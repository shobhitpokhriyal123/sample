<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="language" content="en">
        <?php
        echo Yii::app()->bootstrap->init();
        $path = Yii::app()->baseUrl;
        ?>
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <link rel="stylesheet" href="<?php echo $path; ?>/css/fontAwesome/css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css">
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/custom.css">
    </head>

    <?php
    $controller = $this->id;
    $action = $this->action->id;
    ?>

    <body>
        <nav class="navbar navbar-default" id="top_navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#myNavigation" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo Yii::app()->request->getBaseUrl(true); ?>">
                        <img src='<?php echo Yii::app()->baseUrl; ?>/images/logo.png' alt='R.S Pvt Ltd' />
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="myNavigation">
                    <ul class="nav navbar-nav navbar-right">
                        <?php if (Yii::app()->user->isGuest) { ?>

                            <li class="<?php echo ($controller == "site" && $action == "contact") ? "active" : ""; ?>">
                                <a href="<?php echo Yii::app()->createUrl('/site/contact') ?>">
                                    <i class="glyphicon glyphicon-envelope"></i> Contact <span class="sr-only">(current)</span>
                                </a>
                            </li>
                            <li class="<?php echo ($controller == "site" && $action == "login") ? "active" : ""; ?>">
                                <a href="<?php echo Yii::app()->createUrl('/site/login') ?>">
                                    <i class="glyphicon glyphicon-log-in"></i> Login
                                </a>
                            </li>

                        <?php } if (!Yii::app()->user->isGuest && Yii::app()->user->isCustomer) { ?>
                            <li class="<?php echo ((true) == "") ? "active" : ""; ?>">
                                <a href="<?php echo Yii::app()->createUrl('/admin/leadContacts/sites') ?>">
                                    <i class="fa fa-building"></i>Sites
                                </a>
                            </li>  
    <!--                            <li class="<?php //echo ((true) == "") ? "active" : "";  ?>">
                                <a href="<?php //echo Yii::app()->createUrl('/admin/leadContacts/contracts')  ?>">
                                    <i class="fa fa-wrench"></i>Contracts
                                </a>
                            </li>  -->
                            <li class="<?php echo ((true) == "") ? "active" : ""; ?>">
                                <a href="<?php echo Yii::app()->createUrl('/admin/leadContacts/quotes', array("cid"=>Yii::app()->user->id)) ?>">
                                    <i class="fa fa-money"></i>Quotes
                                </a>
                            </li>  
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-user"></i> My Profile <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li role="separator" class="divider"></li>
                                    <li class="<?php echo ((true) == "") ? "active" : ""; ?>">
                                        <a href="<?php echo Yii::app()->createUrl('/admin/users/view') ?>">
                                            <i class="fa fa-user-plus"></i> Dashboard
                                        </a>
                                    </li>

                                </ul>
                            </li>
                        <?php } if (Yii::app()->user->isAdmin) { ?>

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-truck"></i> Leads<span class="caret"></span>
                                </a>


                                <ul class="dropdown-menu">
                                    <li role="separator" class="divider"></li>
                                    <li class="<?php echo ((true) == "") ? "active" : ""; ?>">
                                        <a href="<?php echo Yii::app()->createUrl('/admin/leadContacts/create') ?>">
                                            <i class="fa fa-plus"></i> New Leads 
                                        </a>
                                    </li>                        
                                    <li role="separator" class="divider"></li>
                                    <li class="<?php echo ($controller == "leadContacts" && $action == "admin") ? "active" : ""; ?>">
                                        <a href="<?php echo Yii::app()->createUrl('/admin/leadContacts/allLeads') ?>">
                                            <i class="fa fa-truck"></i> Manage Leads

                                        </a>
                                    </li>
                                    <?php //} ?>

                                </ul>
                            </li>

                            <li class="<?php echo ($controller == "leadContacts" && $action == "admin") ? "active" : ""; ?>">
                                <a href="<?php echo Yii::app()->createUrl('/admin/news/admin') ?>">
                                    <i class="fa fa-newspaper-o"></i> News
                                </a>
                            </li>

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-user"></i> Agency/Agent <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li role="separator" class="divider"></li>
                                    <li class="<?php echo ((true) == "") ? "active" : ""; ?>">
                                        <a href="<?php echo Yii::app()->createUrl('/admin/users/newUser') ?>">
                                            <i class="fa fa-user-plus"></i> New User
                                        </a>
                                    </li>
                                    <li class="<?php echo ((true) == "") ? "active" : ""; ?>">
                                        <a href="<?php echo Yii::app()->createUrl('/admin/users/allUsers') ?>">
                                            <i class="fa fa-users"></i> Users(Agents)
                                        </a>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <li class="<?php echo ((true) == "") ? "active" : ""; ?>">
                                        <a href="<?php echo Yii::app()->createUrl('/admin/users/newAgency') ?>">
                                            <i class="fa fa-plus"></i> New Agency
                                        </a>
                                    </li>

                                    <li class="<?php echo ((true) == "") ? "active" : ""; ?>">
                                        <a href="<?php echo Yii::app()->createUrl('/admin/users/agencies') ?>">
                                            <i class="fa fa-building-o"></i> Agencies
                                        </a>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <!--                            tickets-->
                                    <li class="<?php echo ($controller == "ticket" && $action == "admin") ? "active" : ""; ?>">
                                        <a href="<?php echo Yii::app()->createUrl('/admin/ticket/admin') ?>">
                                            <i class="fa fa-product-hunt"></i> Tickets
                                        </a>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <li class="<?php echo ((true) == "") ? "active" : ""; ?>">
                                        <a href="<?php echo Yii::app()->createUrl('/admin/users/view') ?>">
                                            <i class="fa fa-building-o"></i> My Agency
                                        </a>
                                    </li>

                                    <li role="separator" class="divider"></li>
                                    <li class="<?php echo ((true) == "") ? "active" : ""; ?>">
                                        <a href="<?php echo Yii::app()->createUrl('/admin/upload/admin') ?>">
                                            <i class="fa fa-building-o"></i> Supplier Documents
                                        </a>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <li class="<?php echo ((true) == "") ? "active" : ""; ?>">
                                        <a href="<?php echo Yii::app()->createUrl('/admin/permissions/admin') ?>">
                                            <i class="fa fa-building-o"></i> Permissions
                                        </a>
                                    </li>
                                </ul>
                            </li>


                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-wrench"></i> Settings <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="<?php echo ((true) == "") ? "active" : ""; ?>">
                                        <a href="<?php echo Yii::app()->createUrl('/admin/paymentType/admin') ?>">
                                            <i class="fa fa-plus"></i> Payment Type
                                        </a>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <li class="<?php echo ((true) == "") ? "active" : ""; ?>">
                                        <a href="<?php echo Yii::app()->createUrl('/admin/meterType/admin') ?>">
                                            <i class="fa fa-plus"></i> Meter Type
                                        </a>
                                    </li>
                                    <li class="<?php echo ((true) == "") ? "active" : ""; ?>">
                                        <a href="<?php echo Yii::app()->createUrl('/admin/meterBase/admin') ?>">
                                            <i class="fa fa-plus"></i> Meter Base
                                        </a>
                                    </li>
                                    <li class="<?php echo ((true) == "") ? "active" : ""; ?>">
                                        <a href="<?php echo Yii::app()->createUrl('/admin/role/admin') ?>">
                                            <i class="fa fa-building-o"></i> Roles
                                        </a>
                                    </li>
                                    <li class="<?php echo ((true) == "") ? "active" : ""; ?>">
                                        <a href="<?php echo Yii::app()->createUrl('/admin/contractStatus/admin') ?>">
                                            <i class="fa fa-building-o"></i> Contract Status
                                        </a>
                                    </li>
                                    <li class="<?php echo ((true) == "") ? "active" : ""; ?>">
                                        <a href="<?php echo Yii::app()->createUrl('/admin/businessType/admin') ?>">
                                            <i class="fa fa-building-o"></i> Business Types
                                        </a>
                                    </li>
                                    <li class="<?php echo ((true) == "") ? "active" : ""; ?>">
                                        <a href="<?php echo Yii::app()->createUrl('/admin/sitetext/admin') ?>">
                                            <i class="fa fa-building-o"></i> Site Text
                                        </a>
                                    </li>
                                    <li class="<?php echo ((true) == "") ? "active" : ""; ?>">
                                        <a href="<?php echo Yii::app()->createUrl('/admin/businessStructure/admin') ?>">
                                            <i class="fa fa-building-o"></i> Business Structure
                                        </a>
                                    </li>

                                    <li class="<?php echo ((true) == "") ? "active" : ""; ?>">
                                        <a href="<?php echo Yii::app()->createUrl('/admin/distributerMap/admin') ?>">
                                            <i class="fa fa-building-o"></i> Distributer Mapping
                                        </a>
                                    </li>
                                    <li class="<?php echo ((true) == "") ? "active" : ""; ?>">
                                        <a href="<?php echo Yii::app()->createUrl('/admin/type/admin') ?>">
                                            <i class="fa fa-building-o"></i> Manage Utilities
                                        </a>
                                    </li>

                                    <li class="<?php echo ((true) == "") ? "active" : ""; ?>">
                                        <a href="<?php echo Yii::app()->createUrl('/admin/ticketType/admin') ?>">
                                            <i class="fa fa-building-o"></i> Ticket Types
                                        </a>
                                    </li>

                                    <li class="<?php echo ((true) == "") ? "active" : ""; ?>">
                                        <a href="<?php echo Yii::app()->createUrl('/admin/ticketStatus/admin') ?>">
                                            <i class="fa fa-building-o"></i> Ticket Status
                                        </a>
                                    </li>
                                    <li class="<?php echo ((true) == "") ? "active" : ""; ?>">
                                        <a href="<?php echo Yii::app()->createUrl('/admin/quoteStatus/admin') ?>">
                                            <i class="fa fa-building-o"></i> Payment Status
                                        </a>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <li class="<?php echo ((true) == "") ? "active" : ""; ?>">
                                        <a href="<?php echo Yii::app()->createUrl('/admin/doctype/admin') ?>">
                                            <i class="fa fa-building-o"></i> Supplier Doc Types
                                        </a>
                                    </li>
                                    <li class="<?php echo ((true) == "") ? "active" : ""; ?>">
                                        <a href="<?php echo Yii::app()->createUrl('/admin/permissionModules/admin') ?>">
                                            <i class="fa fa-building-o"></i> Permission Modules
                                        </a>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <li class="<?php echo ((true) == "") ? "active" : ""; ?>">
                                        <a href="<?php echo Yii::app()->createUrl('/admin/elecMeter/admin') ?>">
                                            <i class="fa fa-building-o"></i> Electric Meter Settings
                                        </a>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <li class="<?php echo ((true) == "") ? "active" : ""; ?>">
                                        <a href="<?php echo Yii::app()->createUrl('/admin/product/startDate') ?>">
                                            <i class="fa fa-building-o"></i> Start Date Settings
                                        </a>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <li class="<?php echo ((true) == "") ? "active" : ""; ?>">
                                        <a href="<?php echo Yii::app()->createUrl('/admin/leadStatus') ?>">
                                            <i class="fa fa-building-o"></i> Lead Status
                                        </a>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <li class="<?php echo ((true) == "") ? "active" : ""; ?>">
                                        <a href="<?php echo Yii::app()->createUrl('/admin/meterStatus') ?>">
                                            <i class="fa fa-building-o"></i> Meter Status
                                        </a>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <li class="<?php echo ((true) == "") ? "active" : ""; ?>">
                                        <a href="<?php echo Yii::app()->createUrl('/admin/contractType') ?>">
                                            <i class="fa fa-building-o"></i> Contract Type
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <!-- Agency -->



                            <!-- Supliers -->
                            <li class="<?php echo ($controller == "users" && $action == "admin") ? "active" : ""; ?>">
                                <a href="<?php echo Yii::app()->createUrl('/admin/users/admin') ?>">
                                    <i class="fa fa-truck"></i> Suppliers
                                </a>
                            </li>
                            <!-- Products -->
                            <li class="<?php echo ($controller == "product" && $action == "admin") ? "active" : ""; ?>">
                                <a href="<?php echo Yii::app()->createUrl('/admin/product/admin') ?>">
                                    <i class="fa fa-product-hunt"></i> Products
                                </a>
                            </li>

                        <?php } ?>

                        <?php if (!Yii::app()->user->isGuest) { ?>
                            <?php if (Yii::app()->user->isAgency || Yii::app()->user->isUser) { ?>
                                <li class="<?php echo ($controller == "leadContacts" && $action == "admin") ? "active" : ""; ?>">
                                    <a href="<?php echo Yii::app()->createUrl('/admin/leadContacts/allLeads') ?>">
                                        <i class="fa fa-product-hunt"></i> Leads
                                    </a>
                                </li>

                                <li class="<?php echo ($controller == "leadContacts" && $action == "admin") ? "active" : ""; ?>">
                                    <a href="<?php echo Yii::app()->createUrl('/admin/news/myNews') ?>">
                                        <i class="fa fa-newspaper-o"></i> News
                                    </a>
                                </li>
                                <?php if (Users::isPermission(Yii::app()->user->id, 6)) {
                                    ?>
                                    <li class="<?php echo ($controller == "ticket" && $action == "admin") ? "active" : ""; ?>">
                                        <a href="<?php echo Yii::app()->createUrl('/admin/ticket/admin') ?>">
                                            <i class="fa fa-product-hunt"></i> Tickets
                                        </a>
                                    </li>
                                    <?php
                                }
                                if (Users::isPermission(Yii::app()->user->id, 7)) {
                                    ?>
                                    <li class="<?php echo ($controller == "upload" && $action == "admin") ? "active" : ""; ?>">
                                        <a href="<?php echo Yii::app()->createUrl('/admin/upload/admin') ?>">
                                            <i class="fa fa-product-hunt"></i> Supplier Documents
                                        </a>
                                    </li>
                                    <?php
                                }
                            }
                            ?>
                            <?php if (!Yii::app()->user->isGuest && !Yii::app()->user->isCustomer) { ?>
                                <?php if (Users::isPermission(Yii::app()->user->id, 2)) { ?>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-gbp"></i> Commission <span class="caret"></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li role="separator" class="divider"></li>
                                            <li class="<?php echo ((true) == "") ? "active" : ""; ?>">
                                                <a href="<?php echo Yii::app()->createUrl('/admin/quote/commission&Quote[is_select]=1') ?>">
                                                    <i class="fa fa-gbp"></i> Commissions
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                <?php } ?>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-building"></i> Customers <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li role="separator" class="divider"></li>
                                        <li class="<?php echo ((true) == "") ? "active" : ""; ?>">
                                            <a href="<?php echo Yii::app()->createUrl('/admin/contractDetail/admin') ?>">
                                                <i class="fa fa-building-o"></i> Contracts
                                            </a>
                                        </li>
                                        <?php //if(Yii::app()->user->isAdmin){    ?>
                                        <?php if (Users::isPermission(Yii::app()->user->id, 3)) { ?>
                                            <li role="separator" class="divider"></li>
                                            <li class="<?php echo ((true) == "") ? "active" : ""; ?>">
            <!--                                                <a href="<?php echo Yii::app()->createUrl('/admin/agencyCustomer/create') ?>">
                                                    <i class="fa fa-plus"></i> New Customer
                                                </a>-->
                                                <a href="<?php echo Yii::app()->createUrl('/admin/leadContacts/addCustomer') ?>">
                                                    <i class="fa fa-plus"></i> New Customer 
                                                </a> 
                                            </li>

                                            <li class="<?php echo ((true) == "") ? "active" : ""; ?>">
                                                <a href="<?php echo Yii::app()->createUrl('/admin/leadContacts/customers') ?>">
                                                    <i class="fa fa-building-o"></i> Customers 
                                                </a>
                                            </li>

                                            <li class="<?php echo ((true) == "") ? "active" : ""; ?>">
                                                <a href="<?php echo Yii::app()->createUrl('/admin/leadTender/admin') ?>">
                                                    <i class="fa fa-building-o"></i> Tenders 
                                                </a>
                                            </li>

                                              <li class="<?php echo ((true) == "") ? "active" : ""; ?>">
                                                <a href="<?php echo Yii::app()->createUrl('/admin/leadContacts/quotes') ?>">
                                                    <i class="fa fa-building-o"></i> Manage Quotes 
                                                </a>
                                            </li>

                                            
                                            
                                        <?php } ?>
                                    </ul>
                                </li>

                                <?php if (Users::isPermission(Yii::app()->user->id, 5)) { ?>
                                    <!-- Tariffs -->
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-money"></i> Tariffs <span class="caret"></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li role="separator" class="divider"></li>
                                            <li class="<?php echo ((true) == "") ? "active" : ""; ?>">
                                                <a href="<?php echo Yii::app()->createUrl('/admin/price/tariff&type=Gas') ?>">
                                                    <i class="glyphicon glyphicon-fire"></i> Gas Tariffs
                                                </a>
                                            </li>
                                            <li class="<?php echo ((true) == "") ? "active" : ""; ?>">
                                                <a href="<?php echo Yii::app()->createUrl('/admin/price/tariff&type=Electricity') ?>">
                                                    <i class="fa fa-bolt"></i> Electricity Tariffs
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                <?php } if (Users::isPermission(Yii::app()->user->id, 4)) { ?>
                                    <!-- Instant Quote -->


                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-money"></i> Live Quote <span class="caret"></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li role="separator" class="divider"></li>
                                            <li class="<?php echo ($controller == "product" && $action == "quote") ? "active" : ""; ?>">
                                                <a href="<?php echo Yii::app()->createUrl('/admin/product/quote') ?>">
                                                    <i class="fa fa-line-chart"></i> Quotes
                                                </a>
                                            </li>
                                            <li class="<?php echo ((true) == "") ? "active" : ""; ?>">
                                                <a href="<?php echo Yii::app()->createUrl('/admin/quoteBaseHistory/admin') ?>">
                                                    <i class="fa fa-bolt"></i> Quote History
                                                </a>
                                            </li>
                                        </ul>
                                    </li>

                                    <?php
                                }
                            }
                            ?>

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-money"></i> <?php echo Yii::app()->user->username; ?>  <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li role="separator" class="divider"></li>
                                    <!-- Logout -->
                                    <li class="<?php echo (CHtml::encode($this->pageTitle) == "Utility - Site") ? "active" : ""; ?>">
                                        <a href="<?php echo Yii::app()->createUrl('/users/profile') ?>">
                                            <i class="glyphicon glyphicon-pencil"></i> Edit Profile 
                                        </a>

                                    </li>

                                    <li class="<?php echo (CHtml::encode($this->pageTitle) == "Utility - Site") ? "active" : ""; ?>">
                                        <a href="<?php echo Yii::app()->createUrl('/site/logout') ?>">
                                            <i class="glyphicon glyphicon-log-out"></i> Logout (<?php echo Yii::app()->user->username; ?>)
                                        </a>
                                    </li>


                                </ul>  
                            </li>

                        <?php }
                        ?>
                    </ul>
                </div>

            </div>
        </nav>

        <?php
//        echo 'Controller : ' .$controller. ' | Action : ' .$action;
        ?>

        <!-- Old Menu -->             



        <!--- Page Content --->
        <?php echo $content; ?>
        <!-------------------->


        <nav class="navbar navbar-default navbar-fixed-bottom">
            <div class="container">
                <p class="navbar-text">
                    Copyright &copy; <?php echo date('Y'); ?> All Rights Reserved.
                </p>
                <p class="navbar-text navbar-right">
                    Utility Cloud
                </p>
            </div>
        </nav>

        <script>
            $(document).ready(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });
        </script>

        <script>

            function paginationControls() {
                $('.table').wrap('<div class="table-responsive"></div>');
                $('.yiiPager').addClass('pagination');
                $('input:not(:checkbox):not(:radio), select').addClass('form-control');
            }
            paginationControls();

        </script>

        <style>
            /*            nav#top_navigation {
                display: none;
            }*/
        </style>
    </body>
</html>
