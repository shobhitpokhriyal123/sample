<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>

	<div id="content">
              <img src="<?php echo Yii::app()->baseUrl; ?>/images/spinner.gif" id="loading_gif" style="display: none">

		<?php echo $content; ?>
	</div><!-- content -->


<?php $this->endContent(); ?>