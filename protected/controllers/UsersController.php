<?php

class UsersController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    public $defaultAction = 'view';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'thumbnail', 'forgot', 'merge'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'admin1', 'profile'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete', 'admin1'),
                'expression' => 'Yii::app()->user->getIsAdmin()',
            //'users' => array('admin'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete', 'letter', 'getUser'),
                'expression' => 'Yii::app()->user->getIsHR()',
            //'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id = null) {
        if (!$id)
            $id = Yii::app()->user->id;
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionAdmin1($id = null) {
        print_r($_GET);
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Users;
        $model->password = '';
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Users'])) {
            $model->attributes = $_POST['Users'];
            $model->password = md5($_POST['Users']['password']);
            $model->saveUploadedFile($model, 'image');
            if ($model->save()) {

                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {

        $model = $this->loadModel($id);
        $model->password = '';
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Users'])) {
            $model->attributes = $_POST['Users'];
            $model->password = md5($_POST['Users']['password']);
            $model->saveUploadedFile($model, 'image');
            if ($model->save()) {

                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Users');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        //  echo '<pre>'; print_r($_GET); die;
        $model = new Users('search');
        $model->unsetAttributes();  // clear any default values

        if (isset($_GET['Users']))
            $model->attributes = $_GET['Users'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Users::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'users-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * 
     * @param type $id
     */
    public function actionGetUser($id) {
        $model = $this->loadModel($id);
        $arr = array();
        $arr['id'] = $model->id;
        $arr['fullname'] = $model->fullname;
        $arr['joining_date'] = date("d-M-Y", strtotime($model->joining_date));
        $arr['role'] = $model->rolename->title;
        echo json_encode($arr);
        die;
    }

    public function actionForgot() {
        $model = new Users;
        $model->scenario = 'forgot';

        if (isset($_POST['Users'])) {
            $email = $_POST['Users']['email'];
            $user = Users::model()->findByAttributes(array('email' => $email));
            // echo '<pre>'; print_r($user); die;
            if ($user && $user->sendPassword()) {

                Yii::app()->user->setFlash('success', "An email sent to you..! Please check your email...! ");
            } else {
                Yii::app()->user->setFlash('error', "Email id is not registered..!");
            }
            $this->redirect(array('/site/login'));
        }
        $this->renderPartial('forgot', array('model' => $model), false, true);
    }

    public function actionProfile() {

        // $this->layout = '//layouts/column2';
        $model = $this->loadModel(Yii::app()->user->id);
        // $model = new Users;
        $pass = $model->password;
        $model->scenario = 'profile_update';
        $model->password = '';
        if (isset($_POST['Users'])) {
            $model->attributes = $_POST['Users'];
            if (isset($_POST['Users']) && $_POST['Users']['password']) {
                $model->password = md5($_POST['Users']['password']);
                $model->confirm_password = md5($_POST['Users']['confirm_password']);
            } else {
                $model->password = $pass;
                $model->confirm_password = $pass;
            }
            $model->saveUploadedFile($model, 'image');
            if ($model->save()) {
                Yii::app()->user->setFlash('success', "Data1 Updated!");
                $this->redirect(array('/admin'));
            }
        }

        $this->render('profile', array(
            'model' => $model,
        ));
    }

}
