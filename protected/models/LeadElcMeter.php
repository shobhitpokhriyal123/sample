<?php

/**
 * This is the model class for table "tbl_lead_elc_meter".
 *
 * The followings are the available columns in table 'tbl_lead_elc_meter':
 * @property integer $id
 * @property integer $create_user_id
 * @property integer $lead_id
 * @property integer $current_supplier
 * @property string $meter_name
 * @property string $pc
 * @property string $mtc
 * @property string $llf
 * @property string $pes
 * @property string $mpan
 * @property string $total_eac
 * @property string $contract_end_date
 * @property string $msn
 * @property string $kva
 * @property string $voltage
 * @property string $ct_wc
 * @property string $mop
 * @property string $meter_status
 * @property string $meter_pricing
 * @property string $created
 */
class LeadElcMeter extends GxActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tbl_lead_elc_meter';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('create_user_id, site_id, corporate_sme, utility,current_supplier, meter_name,  meter_status, meter_pricing, created', 'required'),
            array('create_user_id, serial_no,lead_id,corporate_sme, utility, current_supplier', 'numerical', 'integerOnly' => true),
            array('meter_name, pc, mtc, serial_no, llf, pes, mpan, total_eac, msn, kva, voltage, ct_wc, mop, meter_status, meter_pricing, document', 'length', 'max' => 255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, create_user_id, document,site_id,serial_no, lead_id,site_id,corporate_sme, utility, current_supplier, meter_name, pc, mtc, llf, pes, mpan, total_eac, contract_end_date, msn, kva, voltage, ct_wc, mop, meter_status, meter_pricing, created', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'supplier' => array(self::BELONGS_TO, 'Users', 'current_supplier'),
            'site' => array(self::BELONGS_TO, 'LeadSite', 'site_id'),
            'lead' => array(self::BELONGS_TO, 'LeadContacts', 'lead_id'),
            'status' => array(self::BELONGS_TO, 'MeterStatus', 'meter_satatus'),
                // 'lead'=> array(self::BELONGS_TO, 'LeadContacts', 'lead_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'create_user_id' => 'Create User',
            'lead_id' => 'Lead',
            'serial_no' => 'Serial No',
            'current_supplier' => 'Current Supplier',
            'meter_name' => 'Meter Name',
            'pc' => 'Pc',
            'mtc' => 'Mtc',
            'llf' => 'Llf',
            'pes' => 'Topline',
            'mpan' => 'Mpan',
            'total_eac' => 'Consumption',
            'contract_end_date' => 'Contract End Date',
            'msn' => 'Msn',
            'kva' => 'Kva',
            'voltage' => 'Voltage',
            'ct_wc' => 'Ct Wc',
            'mop' => 'Mop',
            'meter_status' => 'Meter Status',
            'meter_pricing' => 'Smart Meter',
            'created' => 'Created',
            'site_id' => 'Site',
            'lead_id' => 'Choose Contact',
            'document' => 'Document'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('create_user_id', $this->create_user_id);
        $criteria->compare('lead_id', $this->lead_id);

        $criteria->compare('site_id', $this->site_id);

        $criteria->compare('current_supplier', $this->current_supplier);
        $criteria->compare('serial_no', $this->serial_no);
        $criteria->compare('meter_name', $this->meter_name, true);
        $criteria->compare('pc', $this->pc, true);
        $criteria->compare('mtc', $this->mtc, true);
        $criteria->compare('llf', $this->llf, true);
        $criteria->compare('pes', $this->pes, true);
        $criteria->compare('mpan', $this->mpan, true);
        $criteria->compare('total_eac', $this->total_eac, true);
        $criteria->compare('contract_end_date', $this->contract_end_date, true);
        $criteria->compare('msn', $this->msn, true);
        $criteria->compare('kva', $this->kva, true);
        $criteria->compare('voltage', $this->voltage, true);
        $criteria->compare('ct_wc', $this->ct_wc, true);
        $criteria->compare('mop', $this->mop, true);
        $criteria->compare('meter_status', $this->meter_status, true);
        $criteria->compare('meter_pricing', $this->meter_pricing, true);
        $criteria->compare('created', $this->created, true);
        
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function searchByLead($lead_id) {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('create_user_id', $this->create_user_id);
        $criteria->with = array('site');
        $criteria->addCondition('site.lead_id =' . $lead_id);
        // $criteria->compare('site_id', $this->site_id);
        $criteria->compare('current_supplier', $this->current_supplier);
        $criteria->compare('serial_no', $this->serial_no);
        $criteria->compare('meter_name', $this->meter_name, true);
        $criteria->compare('pc', $this->pc, true);
        $criteria->compare('mtc', $this->mtc, true);
        $criteria->compare('llf', $this->llf, true);
        $criteria->compare('pes', $this->pes, true);
        $criteria->compare('mpan', $this->mpan, true);
        $criteria->compare('total_eac', $this->total_eac, true);
        $criteria->compare('contract_end_date', $this->contract_end_date, true);
        $criteria->compare('msn', $this->msn, true);
        $criteria->compare('kva', $this->kva, true);
        $criteria->compare('voltage', $this->voltage, true);
        $criteria->compare('ct_wc', $this->ct_wc, true);
        $criteria->compare('mop', $this->mop, true);
        $criteria->compare('meter_status', $this->meter_status, true);
        $criteria->compare('meter_pricing', $this->meter_pricing, true);
        $criteria->compare('created', $this->created, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return LeadElcMeter the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public static function getCount($id) {
        $criteria = new CDbCriteria;
        $criteria->addCondition('lead_id = ' . $id);
        $sites = LeadSite::model()->findAll($criteria);

        $gasmeters = 0;
        if ($sites) {
            foreach ($sites as $site) {
                $criteria = new CDbCriteria;
                $criteria->addCondition('site_id = ' . $site->id);
                $gasmeters = LeadElcMeter::model()->findAll($criteria);
            }
            return count($gasmeters);
        }
        return $gasmeters;
    }

    public static function getSiteDropdown() {
        $html = '<select id="LeadElcMeter_site_id_" class="form-control" name="LeadElcMeter[site_id]">';
        $sites = LeadSite::model()->findAll();
        foreach ($sites as $site) {
            $html .= '<option value="' . $site->id . '">' . $site->site_name . '</option>';
        }
        $html .= '</select>';
        return $html;
    }

    public static function getSupplierDropdown() {
        $html = '<select id="LeadElcMeter_current_supplier_" class="form-control" name="LeadElcMeter[current_supplier]">';
        $sites = Users::model()->findAllByAttributes(array('role_id' => 2));
        foreach ($sites as $site) {
            $html .= '<option value="' . $site->id . '">' . $site->username . '</option>';
        }
        $html .= '</select>';
        return $html;
    }

    public static function getCorporateDropdown() {
        $html = ' <select id="LeadElcMeter_corporate_sme_" class="form-control" onchange="getUtilities1($(this).val())" name="LeadElcMeter[corporate_sme]">';
        $sites = Type::model()->findAllByAttributes(array('parrent' => 0));
        foreach ($sites as $site) {
            $html .= '<option value="' . $site->id . '">' . $site->title . '</option>';
        }
        $html .= '</select>';
        return $html;
    }

    public static function getUtilityDropdown() {
        $criteria = new CDbCriteria;
        $criteria->condition = 'parrent != 0 && status =1 ';
        $html = '<select id="LeadElcMeter_utility_" class="form-control" name="LeadElcMeter[utility]">';
        $sites = Type::model()->findAll($criteria);
        foreach ($sites as $site) {
            $html .= '<option value="' . $site->id . '">' . $site->title . '</option>';
        }
        $html .= '</select>';
        return $html;
    }

    public static function getStatusDropdown() {
        $html = '<select id="LeadElcMeter_meter_status_" class="form-control" name="LeadElcMeter[meter_status]">';
        $sites = MeterStatus::model()->findAll();
        foreach ($sites as $site) {
            $html .= '<option value="' . $site->id . '">' . $site->title . '</option>';
        }
        $html .= '</select>';
        return $html;
    }

    public function searchGrid($lead_id) {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('create_user_id', $this->create_user_id);
        // $criteria->compare('lead_id', $this->lead_id);
        $criteria->with = array('site');
        $criteria->addCondition('site.lead_id = ' . $lead_id);
        $criteria->compare('current_supplier', $this->current_supplier);
        $criteria->compare('serial_no', $this->serial_no);
        $criteria->compare('meter_name', $this->meter_name, true);
        $criteria->compare('pc', $this->pc, true);
        $criteria->compare('mtc', $this->mtc, true);
        $criteria->compare('llf', $this->llf, true);
        $criteria->compare('pes', $this->pes, true);
        $criteria->compare('mpan', $this->mpan, true);
        $criteria->compare('total_eac', $this->total_eac, true);
        $criteria->compare('contract_end_date', $this->contract_end_date, true);
        $criteria->compare('msn', $this->msn, true);
        $criteria->compare('kva', $this->kva, true);
        $criteria->compare('voltage', $this->voltage, true);
        $criteria->compare('ct_wc', $this->ct_wc, true);
        $criteria->compare('mop', $this->mop, true);
        $criteria->compare('meter_status', $this->meter_status, true);
        $criteria->compare('meter_pricing', $this->meter_pricing, true);
        $criteria->compare('created', $this->created, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function searchBySite($id) {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('create_user_id', $this->create_user_id);
        $criteria->addCondition('site_id =' . $id);
        $criteria->compare('current_supplier', $this->current_supplier);
        $criteria->compare('serial_no', $this->serial_no);
        $criteria->compare('meter_name', $this->meter_name, true);
        $criteria->compare('pc', $this->pc, true);
        $criteria->compare('mtc', $this->mtc, true);
        $criteria->compare('llf', $this->llf, true);
        $criteria->compare('pes', $this->pes, true);
        $criteria->compare('mpan', $this->mpan, true);
        $criteria->compare('total_eac', $this->total_eac, true);
        $criteria->compare('contract_end_date', $this->contract_end_date, true);
        $criteria->compare('msn', $this->msn, true);
        $criteria->compare('kva', $this->kva, true);
        $criteria->compare('voltage', $this->voltage, true);
        $criteria->compare('ct_wc', $this->ct_wc, true);
        $criteria->compare('mop', $this->mop, true);
        $criteria->compare('meter_status', $this->meter_status, true);
        $criteria->compare('meter_pricing', $this->meter_pricing, true);
        $criteria->compare('created', $this->created, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}
