<?php

/**
 * This is the model class for table "tbl_customer_site".
 *
 * The followings are the available columns in table 'tbl_customer_site':
 * @property integer $id
 * @property integer $customer_id
 * @property integer $create_user_id
 * @property string $site_name
 * @property string $site_address1
 * @property string $site_address2
 * @property string $site_address3
 * @property string $postcode
 * @property string $created
 */
class CustomerSite extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tbl_customer_site';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('customer_id, create_user_id, site_name, postcode, created', 'required'),
            array('customer_id, create_user_id', 'numerical', 'integerOnly' => true),
            array('site_name, site_address1, site_address2, site_address3, postcode', 'length', 'max' => 255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, customer_id, create_user_id, site_name, site_address1, site_address2, site_address3, postcode, created', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'customer' => array(self::BELONGS_TO, 'AgencyCustomer', 'customer_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'customer_id' => 'Customer',
            'create_user_id' => 'Create User',
            'site_name' => 'Site Name',
            'site_address1' => 'Site Address1',
            'site_address2' => 'Site Address2',
            'site_address3' => 'Site Address3',
            'postcode' => 'Postcode',
            'created' => 'Created',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search($cid = null) {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        if ($cid)
            $criteria->compare('customer_id', $cid);
        else
            $criteria->compare('customer_id', $this->customer_id);
        $criteria->compare('create_user_id', $this->create_user_id);
        $criteria->compare('site_name', $this->site_name, true);
        $criteria->compare('site_address1', $this->site_address1, true);
        $criteria->compare('site_address2', $this->site_address2, true);
        $criteria->compare('site_address3', $this->site_address3, true);
        $criteria->compare('postcode', $this->postcode, true);
        $criteria->compare('created', $this->created, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CustomerSite the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
