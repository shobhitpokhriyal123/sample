<?php

/**
 * This is the model class for table "tbl_quote_base_history".
 *
 * The followings are the available columns in table 'tbl_quote_base_history':
 * @property integer $id
 * @property integer $create_user_id
 * @property integer $assigned_to
 * @property string $priceType
 * @property string $distributor
 * @property string $postcode
 * @property string $aq
 * @property string $start_date
 * @property string $business_type
 * @property string $meter_type
 * @property string $created
 */
class QuoteBaseHistory extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tbl_quote_base_history';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('create_user_id, assigned_to, aq, start_date,type, created', 'required'),
            array('create_user_id, assigned_to', 'numerical', 'integerOnly' => true),
            array('priceType, distributor', 'length', 'max' => 200),
            array('postcode, aq,profile, start_date, business_type, meter_type', 'length', 'max' => 255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, create_user_id,profile, assigned_to,type, priceType, distributor, postcode, aq, start_date, business_type, meter_type, created', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'user' => array(self::BELONGS_TO, 'Users', 'create_user_id'),
            'supplier' => array(self::BELONGS_TO, 'Users', 'assigned_to'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'create_user_id' => 'Create User',
            'assigned_to' => 'Assigned To',
            'priceType' => 'Price Type',
            'distributor' => 'Distributor',
            'postcode' => 'Postcode',
            'aq' => 'Aq',
            'start_date' => 'Start Date',
            'business_type' => 'Business Type',
            'meter_type' => 'Meter Type',
            'created' => 'Created',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('create_user_id', Yii::app()->user->id);
        $criteria->compare('assigned_to', $this->assigned_to);
        $criteria->compare('priceType', $this->priceType, true);
        $criteria->compare('distributor', $this->distributor, true);
        $criteria->compare('postcode', $this->postcode, true);
        $criteria->compare('aq', $this->aq, true);
        $criteria->compare('profile', $this->profile, true);
        $criteria->compare('type', $this->type, true);
        $criteria->compare('start_date', $this->start_date, true);
        $criteria->compare('business_type', $this->business_type, true);
        $criteria->compare('meter_type', $this->meter_type, true);
        $criteria->compare('created', $this->created, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return QuoteBaseHistory the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public  function getLink() {
       // Product[priceType]=1&Product[postcode]=&Product[distributor]=10&Product[assigned_to]=39&
       // Product[meter_type]=1&Product[profile]=0&Product[aq]=50000&Product[start_date]=2017-11-10
        $params = array(
            "Product[priceType]" =>$this->priceType,
            "Product[postcode]" =>$this->postcode,
            "Product[distributor]" =>$this->distributor,
            "Product[assigned_to]" =>$this->assigned_to,
            "Product[meter_type]" =>$this->meter_type,
            "Product[profile]" =>$this->profile,
            "Product[aq]" =>$this->aq,
            "Product[start_date]" =>$this->start_date,
            "Product[business_type]" =>$this->business_type,
            );
        if($this->type == "Elc"){
        $html  = '<a class="btn btn-primary" target="_blank" href="'.Yii::app()->createAbsoluteUrl("admin/product/searchEQuote",$params ).'"  > '.$this->id.' </a>';
        }else{
        $html  = '<a class="btn btn-primary" target="_blank" href="'.Yii::app()->createAbsoluteUrl("admin/product/searchQuote",$params ).'"  > '.$this->id.' </a>';
            
        }
        return $html;
    }

}
