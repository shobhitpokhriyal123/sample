<?php

/**
 * This is the model class for table "tbl_product".
 *
 * The followings are the available columns in table 'tbl_product':
 * @property integer $id
 * @property string $utility
 * @property string $corporate_sme
 * @property string $product_name
 * @property string $volume_translate_min
 * @property string $volume_translate_max
 * @property string $max_commission
 * @property string $commission_increament
 * @property string $commission_banded
 * @property string $standing_charge
 * @property string $direct_dabit
 * @property string $renewal
 * @property string $renewal_obligation
 * @property string $feed_in_tarriff
 * @property string $contact_for_difference
 * @property string $capacity_machanism
 * @property string $distribution_charge
 * @property string $transmission_charge
 * @property string $balancing_charge
 * @property string $direct_dabit_mandate
 * @property string $t_and_cs
 * @property integer $status
 * @property string $created
 */
class Product extends GxActiveRecord {

    const ELEC_UTILITY = array(17, 19);
    const TYPE_GAS = 1;
    const TYPE_ELEC = 2;
    const BASIS_MPR = 1;
    const BASIS_POSTCODE = 2;

    public $contract_type;
    public $basis;
    public $business_type;
    public $aq;
    public $sdate;
    public $start_date;
    public $mpr;
    public $postcode;
    /////
    public $meter_type;
    public $profile;
    public $duration;
    public $distributor;
    public $priceType;

    // public $gas_mpr;
    //public $phone;
    //  public $sets;
    //public $supplier;
    // public $mtc;
    // public $pc;
    // public $region;
    // public $contract_type;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tbl_product';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('product_name,utility,corporate_sme,valid_from,acc_end,acc_start,valid_till, min_start_time,  assigned_to, created', 'required'),
            array('csv_file', 'required', 'on' => 'create'),
            array('status, volume_translate_min, volume_translate_max, max_end_time ,min_start_time', 'numerical', 'integerOnly' => true),
            array('postcode,contract_type, business_type,aq, start_date', 'required', 'on' => 'gasparams'),
            array('meter_type,profile,aq, start_date', 'required', 'on' => 'elecparams'),
//              array('valid_till','compare','compareAttribute'=>'valid_from',
//          'operator'=>'>',
//          'allowEmpty'=>false,'message'=>'Valid Till must be greater then valid from.'),
            //array('valid_till','compare','compareAttribute'=>'valid_from','operator'=>'>','message'=>'Start Date must be less than End Date'),
            array('utility,sets,csv_file, corporate_sme, product_name, volume_translate_min,valid_till,valid_from, volume_translate_max, max_commission, commission_increament, commission_banded, standing_charge, direct_dabit, renewal, renewal_obligation, feed_in_tarriff, contact_for_difference, capacity_machanism, distribution_charge, transmission_charge, balancing_charge, direct_dabit_mandate, t_and_cs', 'length', 'max' => 255),
            array('csv_file', 'file', 'types' => 'csv', 'allowEmpty' => true,
                'wrongType' => 'Only csv allowed.'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, utility,final_date,dabit_percentage, sets,min_start_time,max_end_time, csv_file,columns_name,valid_till,valid_from, corporate_sme, assigned_to, product_name, volume_translate_min, volume_translate_max, max_commission, commission_increament, commission_banded, standing_charge, direct_dabit, renewal, renewal_obligation, feed_in_tarriff, contact_for_difference, capacity_machanism, distribution_charge, transmission_charge, balancing_charge, direct_dabit_mandate, t_and_cs, status, created', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'modelPrice' => array(self::HAS_MANY, 'Price', 'product_id'),
            'user' => array(self::BELONGS_TO, 'Users', 'assigned_to'),
            'type' => array(self::BELONGS_TO, 'Type', 'utility'),
            'ctype' => array(self::BELONGS_TO, 'Type', 'corporate_sme'),
            'leadGas' => array(self::HAS_MANY, 'leadGasMeter', 'id'),
            'field' => array(self::BELONGS_TO, 'Fields', array('assigned_to' => 'assigned_to', 'utility' => 'utility', 'corporate_sme' => 'corporate_sme')),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'business_type' => 'Business Type',
            'utility' => 'Utility',
            'aq' => 'Consumption KWH/Year',
            'mpr' => 'MPR',
            'postcode' => 'Supply Post Code',
            'start_date' => 'Start date',
            'corporate_sme' => 'Corporate/Sme',
            'product_name' => 'Product Name',
            'volume_translate_min' => 'Volume Translate Min',
            'volume_translate_max' => 'Volume Translate Max',
            'max_commission' => 'Max Commission',
            'commission_increament' => 'Commission Increament',
            'commission_banded' => 'Commission Banded',
            'standing_charge' => 'Standing Charge',
            'direct_dabit' => 'Payment Method',
            'renewal' => 'Acquisition/Renewal',
            'renewal_obligation' => 'Renewal Obligation',
            'feed_in_tarriff' => 'Feed In Tarriff',
            'contact_for_difference' => 'Contact For Difference',
            'capacity_machanism' => 'Capacity Machanism',
            'distribution_charge' => 'Distribution Charge',
            'transmission_charge' => 'Transmission Charge',
            'balancing_charge' => 'Balancing Charge',
            'direct_dabit_mandate' => 'Direct Dabit Mandate',
            't_and_cs' => 'T And Cs',
            'csv_file' => 'Csv File',
            'status' => 'Status',
            'created' => 'Created',
            'columns_name' => 'Columns Name',
            'assigned_to' => 'Current Suppliers',
            'valid_till' => 'Valid From',
            'valid_from' => 'Valid Till',
            'acc_start' => 'Acc. Start',
            'acc_end' => 'Acc. End',
            'sets' => 'Set Name',
            'dm' => 'Multiply/Devide',
            'meter_type' => 'Supply meter Type',
            'priceType' => 'Search By',
            'dabit_percentage' => 'Dabit Percentage'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search($sid = null) {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('corporate_sme', $this->corporate_sme, true);
        $criteria->compare('product_name', $this->product_name, true);
        $criteria->compare('volume_translate_min', $this->volume_translate_min, true);
        $criteria->compare('volume_translate_max', $this->volume_translate_max, true);
        $criteria->compare('max_commission', $this->max_commission, true);
        $criteria->compare('commission_increament', $this->commission_increament, true);
        $criteria->compare('commission_banded', $this->commission_banded, true);
        $criteria->compare('standing_charge', $this->standing_charge, true);
        $criteria->compare('direct_dabit', $this->direct_dabit, true);
        $criteria->compare('renewal', $this->renewal, true);
        $criteria->compare('renewal_obligation', $this->renewal_obligation, true);
        $criteria->compare('feed_in_tarriff', $this->feed_in_tarriff, true);
        $criteria->compare('contact_for_difference', $this->contact_for_difference, true);
        $criteria->compare('capacity_machanism', $this->capacity_machanism, true);
        $criteria->compare('distribution_charge', $this->distribution_charge, true);
        $criteria->compare('transmission_charge', $this->transmission_charge, true);
        $criteria->compare('balancing_charge', $this->balancing_charge, true);
        $criteria->compare('direct_dabit_mandate', $this->direct_dabit_mandate, true);
        $criteria->compare('t_and_cs', $this->t_and_cs, true);
        $criteria->compare('assigned_to', $sid, true);
        //   $criteria->compare('product_name', $this->product_name, true);
        //$criteria->compare('status', 1);
        //  $criteria->condition = 'status =1';
        $criteria->compare('created', $this->created, true);
        if ($sid) {
            $criteria->compare('utility', $this->utility, true);
        } else
            $criteria->compare('utility', $this->utility, true);
        // $criteria->compare('valid_till', $this->valid_till, true);
        //$criteria->compare('valid_from', $this->valid_from, true);
        //  $criteria->compare('sets', $this->sets, true);
        $criteria->order = 'id desc';
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Product the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * 
     * @return boolean
     */
    public function beforeSave() {
        if ($this->isNewRecord) {
            $this->created = new CDbExpression('NOW()');
        }
        return parent::beforeDelete();
    }

    /**
     * 
     * @param type $label
     * @return type
     * @throws CDbException
     */
    public function readCsvContents($label = null, $region = null, $elec = null) {
        if ($elec) {
            $filename = Yii::app()->basePath . '/..' . UPLOAD_PATH . '/postcode_elec.csv';
        } else {
            if ($region) {
                $filename = Yii::app()->basePath . '/..' . UPLOAD_PATH . '/postcode.csv';
            } else {
                $filename = Yii::app()->basePath . '/..' . UPLOAD_PATH . '/' . $this->csv_file;
            }
        }

        // echo $filename; die;
        if (file_exists($filename)) {
            if ($label) {
                $point = ';';
            } else {
                $point = ',';
            }
            $fp = fopen($filename, 'r');
            if ($fp) {
                $line = fgetcsv($fp, 1000, ",");
                $first_time = true;
                do {
                    $csv[] = $line;
                    if ($label)
                        break;
                } while (($line = fgetcsv($fp, 1000, "$point")) != FALSE);
            }
            
            return $csv;
        } else {
            throw new CDbException(Yii::t('yii', 'File not found..!'));
        }
    }

    /**
     * 
     * @param type $id
     * @return string
     */
    public static function getUtilites($id = null) {
        $criteria = new CDbCriteria;
        $criteria->condition = 'parrent != 0 && status =1 ';
        if ($id) {
            return Type::model()->findByPk($id)->title;
        }
        return Type::model()->findAll($criteria);
    }

    /**
     * 
     * @param type $id
     * @return string
     */
    public static function getOthers($id = null) {
        $arr = array('0' => 'NO', '1' => 'YES');
        if ($id != null)
            return $arr[$id];
        return $arr;
    }

    /**
     * 
     * @param type $id
     * @return string
     */
    public static function getAqRe($id = null) {
        $arr = array('0' => 'Acquisition', '1' => 'Renewal');
        if ($id != null)
            return $arr[$id];
        return $arr;
    }

    /**
     * 
     * @param type $id
     * @return string
     */
    public static function getDirectDebit($id = null) {
        $arr = array('0' => 'Cash', '1' => 'Direct Debit');
        if ($id != null)
            return $arr[$id];
        return $arr;
    }

    /**
     * 
     * @param type $id
     * @return string
     */
    public static function getCorporateSME($id = null) {
        $arr = array('' => 'Select', '1' => 'SME', '2' => 'Corporate');
        if ($id != null)
            return $arr[$id];
        return $arr;
    }

    /**
     * 
     * @param type $id
     * @return string
     */
    public static function getOtherCharges($id = null) {
        $arr = array('1' => 'Included and Fixed', '2' => 'Not included');
        if ($id != null)
            return $arr[$id];
        return $arr;
    }

    /**
     * 
     * @param type $id
     * @return string
     */
    public static function getDM($id = null) {
        $arr = array('1' => 'Multiply By 100', '2' => 'Divide By 100');
        if ($id != null)
            return $arr[$id];
        return $arr;
    }

    /**
     * 
     * @return type
     */
    public static function getContracTypes() {
        return array(self::TYPE_GAS => 'GAS', self::TYPE_ELEC => 'Electric');
    }

    /**
     * 
     * @return type
     */
    public static function getBasis() {
        return array(self::BASIS_MPR => 'MPR', self::BASIS_POSTCODE => 'POST CODE');
    }

    /**
     * 
     * @return type
     * 
     */
    public static function getAllSets() {
        $criteria = new CDbCriteria;
        $criteria->condition = "product != ''";
        $criteria->group = "product";
        $prices = Price::model()->findAll($criteria);
        // echo '<pre>'; print_r($prices); die;
        $arr = array();
        if ($prices) {
            foreach ($prices as $price) {
                $arr[$price->product] = $price->product;
            }
        }
        return $arr;
    }

    /**
     * 
     * @return type
     */
    public static function getAllPC() {
        $criteria = new CDbCriteria;
        $criteria->condition = 'profile != ""';
        $criteria->group = "profile";
        $prices = Price::model()->findAll($criteria);
        $arr = array();
        if ($prices) {
            foreach ($prices as $price) {
                $arr[$price->profile] = $price->profile;
            }
        }
        return $arr;
    }

    /**
     * 
     * @return type
     */
    public static function getAllMTC() {
        $criteria = new CDbCriteria;
        $criteria->condition = 'mtc != ""';
        $criteria->group = "mtc";
        $prices = Price::model()->findAll($criteria);
        $arr = array();
        if ($prices) {
            foreach ($prices as $price) {
                $arr[$price->mtc] = $price->mtc;
            }
        }
        return $arr;
    }

    /**
     * 
     * @param type $id
     * @return string
     */
    public static function getStatus($id = null) {
        if ($id == 1) {
            return 'Active';
        } elseif ($id == 0) {
            return 'Inactive';
        } else {
            return array('1' => 'Active', '0' => 'Inactive');
        }
    }

    /**
     * 
     * @return \CActiveDataProvider
     */
    public function searchMap() {
        $criteria1 = new CDbCriteria;
        $criteria1->group = 'product_id';
        $prices = Price::model()->findAll($criteria1);
        $arr = array();
        foreach ($prices as $price) {
            $arr[] = $price->product_id;
        }
        $criteria = new CDbCriteria;
        $criteria->compare('id', $this->id);
        $criteria->compare('utility', $this->utility, true);
        $criteria->compare('corporate_sme', $this->corporate_sme, true);
        $criteria->compare('product_name', $this->product_name, true);
        $criteria->compare('volume_translate_min', $this->volume_translate_min, true);
        $criteria->compare('volume_translate_max', $this->volume_translate_max, true);
        $criteria->compare('max_commission', $this->max_commission, true);
        $criteria->compare('commission_increament', $this->commission_increament, true);
        $criteria->compare('commission_banded', $this->commission_banded, true);
        $criteria->compare('standing_charge', $this->standing_charge, true);
        $criteria->compare('direct_dabit', $this->direct_dabit, true);
        $criteria->compare('renewal', $this->renewal, true);
        $criteria->compare('renewal_obligation', $this->renewal_obligation, true);
        $criteria->compare('feed_in_tarriff', $this->feed_in_tarriff, true);
        $criteria->compare('contact_for_difference', $this->contact_for_difference, true);
        $criteria->compare('capacity_machanism', $this->capacity_machanism, true);
        $criteria->compare('distribution_charge', $this->distribution_charge, true);
        $criteria->compare('transmission_charge', $this->transmission_charge, true);
        $criteria->compare('balancing_charge', $this->balancing_charge, true);
        $criteria->compare('direct_dabit_mandate', $this->direct_dabit_mandate, true);
        $criteria->compare('t_and_cs', $this->t_and_cs, true);
        $criteria->condition = 'status =1';
        $criteria->addNotInCondition('id', $arr);
        $criteria->compare('created', $this->created, true);
        $criteria->compare('assignecd_to', $this->assigned_to, true);
        $criteria->compare('valid_till', $this->valid_till, true);
        $criteria->compare('valid_from', $this->valid_from, true);
        $criteria->compare('sets', $this->sets, true);
         $criteria->order = 'id desc';
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function getRegion() {
        return Region::model()->findAll();
    }

    /**
     * 
     * @return type
     */
    public static function getAllRegions() {
        $criteria = new CDbCriteria;
        $criteria->condition = "region != '' || region_id != '' ";
        $criteria->group = 'region, region_id';
        $regions = Price::model()->findAll($criteria);
        $arr = array();
        if ($regions) {
            foreach ($regions as $region) {
                if (!is_numeric($region->region))
                    $arr[$region->region] = $region->region;
            }
        }
        return $arr;
    }

    /**
     * 
     * @param type $user_id
     * @return type
     */
    public static function getProducts($user_id = null, $type) {
        $criteria = new CDbCriteria;
        $utility = self::ELEC_UTILITY;
        if ($user_id)
            $criteria->addCondition('assigned_to', $user_id);
        if ($type == self::TYPE_GAS)
            $criteria->addNotInCondition('utility', $utility);
        if ($type == self::TYPE_ELEC)
            $criteria->addInCondition('utility', $utility);
        $products = self::model()->findAll($criteria);
        foreach ($products as $product) {
            $pids[] = $product->id;
        }
        if (!empty($pids))
            $pids = self::array_flatten($pids);
        return $pids;
    }

    /**
     * multi array to single array
     * @param type $array
     * @return boolean
     */
    public static function array_flatten($array) {
        if (!is_array($array)) {
            return FALSE;
        }
        $result = array();
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $result = array_merge($result, array_flatten($value));
            } else {
                $result[$key] = $value;
            }
        }
        return $result;
    }

    public function getBusiness() {
        return array('1' => 'catering', '2' => 'software',);
    }

    /**
     * 
     * @return type
     */
//    public function getMeterType() {
//        $criteria = new CDbCriteria;
//        // $criteria->distinct=true;
//      //  $criteria->addCondition('meter_type != ""');
//     //   $criteria->group = 'meter_type';
//      //  $prices = Price::model()->findAll($criteria);
//      //MeterType::model()->findAll(); 
//       
//    }

    /**
     * 
     * @return type
     */
    public function getProfile() {
        $arr = array();
        for ($i = 0; $i <= 8; $i++) {
            $arr[$i] = $i;
        }
        return $arr;
    }

    /**
     * 
     * @return type
     */
    public function getDistributors() {
        return array(
            '10' => '10',
            '11' => '11',
            '12' => '12',
            '13' => '13',
            '14' => '14',
            '15' => '15',
            '16' => '16',
            '17' => '17',
            '18' => '18',
            '19' => '19',
            '20' => '20',
            '21' => '21',
            '22' => '22',
            '23' => '23'
        );
    }

    /**
     * 
     * @param type $label
     * @return type
     * @throws CDbException
     */
    public function readCsvContents2($code) {
        $filename = Yii::app()->basePath . '/..' . UPLOAD_PATH . '/postcode_elec.csv';
        if (file_exists($filename)) {
            $point = ',';
            $fp = fopen($filename, 'r');
            if ($fp) {
                $line = fgetcsv($fp, 1000, ",");
                $first_time = true;
                do {
                    $csv[] = $line;
                } while (($line = fgetcsv($fp, 1000, "$point")) != FALSE);
            }
            // echo '<pre>'; print_r($csv); die;
            return $csv;
        } else {
            throw new CDbException(Yii::t('yii', 'File not found..!'));
        }
    }

    public function beforeDelete() {
//        $criteria = new CDbCriteria;
//        $criteria->addCondition('product_id=' . $this->id);
//        Price::model()->deleteAll($criteria);
//
//        /////////////////
//        $criteria = new CDbCriteria;
//        $criteria->addCondition('utility=' . $this->utility);
//        $criteria->addCondition('corporate_sme=' . $this->corporate_sme);
//        $criteria->addCondition('assigned_to=' . $this->assigned_to);
//        Fields::model()->deleteAll($criteria);
        return true;

//parent::beforeDelete();
    }

    public function getPriceTypes() {
        return array('1' => 'Distributer', '2' => 'Post Code');
    }

    public static function getPName($pname1, $pname2) {
        return $pname1 . "<br> <b>" . $pname2 . "</b>";
    }

    public static function getTrueFalse() {
        if (Yii::app()->user->isCustomer)
            return false;
        //echo Yii::app()->session["ss"]; die;
        if (Yii::app()->session["ss"] == 1)
            return false;
        return true;
    }

    /**
     * 
     * @param type $price_id
     * @return string
     */
    public static function getSupplierName($price_id) {
        $price = Price::model()->findByPk($price_id);
        $product = Product::model()->findByPk($price->product_id);
        if ($product) {
            return $product->user->username;
        } else {
            return "N/A";
        }
    }

    /**
     * 
     * @return int
     */
    public static function getMonths() {
        $arr = array();
        for ($i = 1; $i < 13; $i ++) {
            $arr[$i] = $i;
        }
        return $arr;
    }

}
