<?php

/**
 * This is the model class for table "tbl_fields".
 *
 * The followings are the available columns in table 'tbl_fields':
 * @property integer $id
 * @property integer $utility
 * @property integer $corporate_sme
 * @property integer $assigned_to
 */
class Fields extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tbl_fields';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('utility, corporate_sme, assigned_to, fields', 'required'),
            array('utility, corporate_sme, assigned_to', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, utility, corporate_sme, assigned_to', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'utility' => 'Utility',
            'corporate_sme' => 'Corporate Sme',
            'assigned_to' => 'Assigned To',
            'fields'=>'Fields'
            );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('utility', $this->utility);
        $criteria->compare('corporate_sme', $this->corporate_sme);
        $criteria->compare('assigned_to', $this->assigned_to);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Fields the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
