<?php

/**
 * This is the model class for table "tbl_validation".
 *
 * The followings are the available columns in table 'tbl_validation':
 * @property integer $id
 * @property integer $credit
 * @property integer $user_id
 * @property integer $meter_details
 * @property integer $consumption
 * @property integer $paper_contract
 * @property integer $cot_form
 * @property integer $electorial_check
 * @property integer $verbal_recording
 * @property integer $duplicate_check
 */
class Validation extends GxActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tbl_validation';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array(' user_id, contract_id', 'required'),
            array('credit, user_id, meter_details, consumption, paper_contract, cot_form, electorial_check, verbal_recording, duplicate_check', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, credit, user_id,contract_id, meter_details, consumption, paper_contract, cot_form, electorial_check, verbal_recording, duplicate_check', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
           //  'n_supplier' => array(self::BELONGS_TO, 'Users', 'new_supplier'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'credit' => 'Credit',
            'user_id' => 'User',
            'meter_details' => 'Meter Details',
            'consumption' => 'Consumption',
            'paper_contract' => 'Paper Contract',
            'cot_form' => 'Cot Form',
            'electorial_check' => 'Electorial Check',
            'verbal_recording' => 'Verbal Recording',
            'duplicate_check' => 'Duplicate Check',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('credit', $this->credit);
        $criteria->compare('user_id', $this->user_id);
        $criteria->compare('meter_details', $this->meter_details);
        $criteria->compare('consumption', $this->consumption);
        $criteria->compare('paper_contract', $this->paper_contract);
        $criteria->compare('cot_form', $this->cot_form);
        $criteria->compare('electorial_check', $this->electorial_check);
        $criteria->compare('verbal_recording', $this->verbal_recording);
        $criteria->compare('duplicate_check', $this->duplicate_check);
         $criteria->compare('contract_id', $this->contract_id, true);
        

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Validation the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getPassFail($id=null) {
        $arr = array( '0'=>'N/A','1'=>'Pass', '2'=> 'Fail' );
        if($id)
            return $arr[$arr];
        return $arr;
    }

}
