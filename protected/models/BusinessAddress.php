<?php

/**
 * This is the model class for table "tbl_business_address".
 *
 * The followings are the available columns in table 'tbl_business_address':
 * @property integer $id
 * @property integer $user_id
 * @property integer $create_user_id
 * @property integer $is_site_address
 * @property string $home_postcode
 * @property string $customer_name
 * @property string $home_town
 * @property string $home_building
 * @property string $home_address_no
 * @property string $home_street1
 * @property string $home_street2
 * @property string $home_country
 * @property string $years_of_address
 * @property string $dob
 */
class BusinessAddress extends GxActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_business_address';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, create_user_id,contract_id,  home_postcode, customer_name,   home_address_no', 'required'),
			array('user_id, create_user_id, is_site_address', 'numerical', 'integerOnly'=>true),
			array('home_postcode, customer_name, home_town, home_building, home_address_no, home_street1, home_street2, home_country, years_of_address, dob', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id,contract_id, create_user_id, is_site_address, home_postcode, customer_name, home_town, home_building, home_address_no, home_street1, home_street2, home_country, years_of_address, dob', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'create_user_id' => 'Create User',
			'is_site_address' => 'Is Site Address',
			'home_postcode' => 'Business Postcode',
			'customer_name' => 'Business Name',
			'home_town' => 'Town/City',
			'home_building' => 'Home Building',
			'home_address_no' => 'Business Address No',
			'home_street1' => 'Street1',
			'home_street2' => 'Home Street2',
			'home_country' => 'County',
			'years_of_address' => 'No of Years at Address',
			'dob' => 'Dob',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('create_user_id',$this->create_user_id);
		$criteria->compare('is_site_address',$this->is_site_address);
		$criteria->compare('home_postcode',$this->home_postcode,true);
		$criteria->compare('customer_name',$this->customer_name,true);
		$criteria->compare('home_town',$this->home_town,true);
		$criteria->compare('home_building',$this->home_building,true);
		$criteria->compare('home_address_no',$this->home_address_no,true);
		$criteria->compare('home_street1',$this->home_street1,true);
		$criteria->compare('home_street2',$this->home_street2,true);
		$criteria->compare('home_country',$this->home_country,true);
		$criteria->compare('years_of_address',$this->years_of_address,true);
		$criteria->compare('dob',$this->dob,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BusinessAddress the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
