<?php

/**
 * This is the model class for table "tbl_agency_bank".
 *
 * The followings are the available columns in table 'tbl_agency_bank':
 * @property integer $id
 * @property integer $create_user_id
 * @property integer $agency_id
 * @property string $account_name
 * @property string $account_no
 * @property string $sort_code
 * @property string $bank_name
 * @property string $account_number
 * @property string $payment_date
 */
class AgencyBank extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_agency_bank';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('create_user_id, agency_id, account_name, sort_code, bank_name, account_number, payment_date', 'required'),
			array('create_user_id, agency_id, account_number', 'numerical', 'integerOnly'=>true),
			array('account_name, account_no, sort_code, bank_name, account_number, payment_date', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, create_user_id, agency_id, account_name, account_no, sort_code, bank_name, account_number, payment_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'create_user_id' => 'Create User',
			'agency_id' => 'Agency',
			'account_name' => 'Account Name',
			'account_no' => 'Company Reg No',
			'sort_code' => 'Sort Code',
			'bank_name' => 'Bank Name',
			'account_number' => 'Account Number',
			'payment_date' => 'Payment Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('create_user_id',$this->create_user_id);
		$criteria->compare('agency_id',$this->agency_id);
		$criteria->compare('account_name',$this->account_name,true);
		$criteria->compare('account_no',$this->account_no,true);
		$criteria->compare('sort_code',$this->sort_code,true);
		$criteria->compare('bank_name',$this->bank_name,true);
		$criteria->compare('account_number',$this->account_number,true);
		$criteria->compare('payment_date',$this->payment_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AgencyBank the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
