<?php

/**
 * This is the model class for table "tbl_contract".
 *
 * The followings are the available columns in table 'tbl_contract':
 * @property integer $id
 * @property string $company
 * @property string $telephone
 * @property string $contact_name
 * @property string $contact_position
 * @property string $contact_email
 * @property string $address
 * @property string $postcode
 * @property string $registraion_no
 * @property integer $business_type
 * @property integer $is_micro_business
 * @property integer $no_of_emp
 * @property integer $no_of_years
 * @property integer $turnover
 * @property integer $status
 */
class Contract extends GxActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_contract';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('company, telephone, contact_name, contact_position, contact_email, address, postcode, registraion_no, business_type, is_micro_business, no_of_emp, no_of_years, turnover, status', 'required'),
			array('business_type, is_micro_business, no_of_emp, no_of_years, turnover, status', 'numerical', 'integerOnly'=>true),
			array('company, telephone, contact_name, contact_position, contact_email, address, postcode, registraion_no', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, company, telephone, contact_name, contact_position, contact_email, address, postcode, registraion_no, business_type, is_micro_business, no_of_emp, no_of_years, turnover, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'company' => 'Company',
			'telephone' => 'Telephone',
			'contact_name' => 'Contact Name',
			'contact_position' => 'Contact Position',
			'contact_email' => 'Contact Email',
			'address' => 'Address',
			'postcode' => 'Postcode',
			'registraion_no' => 'Registraion No',
			'business_type' => 'Business Type',
			'is_micro_business' => 'Is Micro Business',
			'no_of_emp' => 'No Of Emp',
			'no_of_years' => 'No Of Years',
			'turnover' => 'Turnover',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('company',$this->company,true);
		$criteria->compare('telephone',$this->telephone,true);
		$criteria->compare('contact_name',$this->contact_name,true);
		$criteria->compare('contact_position',$this->contact_position,true);
		$criteria->compare('contact_email',$this->contact_email,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('postcode',$this->postcode,true);
		$criteria->compare('registraion_no',$this->registraion_no,true);
		$criteria->compare('business_type',$this->business_type);
		$criteria->compare('is_micro_business',$this->is_micro_business);
		$criteria->compare('no_of_emp',$this->no_of_emp);
		$criteria->compare('no_of_years',$this->no_of_years);
		$criteria->compare('turnover',$this->turnover);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Contract the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
