<?php

/**
 * This is the model class for table "tbl_distributer_map".
 *
 * The followings are the available columns in table 'tbl_distributer_map':
 * @property integer $id
 * @property string $dist_network
 * @property string $area
 * @property string $dist_id
 * @property string $ldz
 * @property string $region
 * @property string $region1
 * @property string $region2
 * @property string $region3
 * @property string $region4
 * @property string $region5
 * @property string $region6
 * @property string $region7
 * @property string $created
 */
class DistributerMap extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tbl_distributer_map';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array(' dist_id, ldz, region, created', 'required'),
            array('dist_network,exit_zone, area, dist_id, ldz, region,region1,region2, region3, region4, region5, region6, region7', 'length', 'max' => 255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, dist_network,exit_zone, area, dist_id,postcode, ldz, region,region1,region2, region3, region4, region5, region6, region7,  created', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'dist_network' => 'Dist Network',
            'area' => 'Area',
            'dist_id' => 'Dist',
            'ldz' => 'Ldz',
            'region' => 'Region',
            'region1' => 'Region1',
            'region2' => 'Region2',
            'region3' => 'Region3',
            'region4' => 'Region4',
            'region5' => 'Region5',
            'region6' => 'Region6',
            'region7' => 'Region7',
            'postcode' => 'Post Code',
            'created' => 'Created',
            'exit_zone' => 'Exit Zone'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('dist_network', $this->dist_network, true);
        $criteria->compare('area', $this->area, true);
        $criteria->compare('dist_id', $this->dist_id, true);
        $criteria->compare('ldz', $this->ldz, true);
        $criteria->compare('region', $this->region, true);
        $criteria->compare('postcode', $this->postcode, true);
        $criteria->compare('region7', $this->region7, true);
        $criteria->compare('region6', $this->region6, true);
        $criteria->compare('region5', $this->region5, true);
        $criteria->compare('region4', $this->region4, true);
        $criteria->compare('region3', $this->region3, true);

        $criteria->compare('region2', $this->region2, true);
        $criteria->compare('region1', $this->region1, true);





        $criteria->compare('created', $this->created, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return DistributerMap the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
