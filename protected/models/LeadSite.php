<?php

/**
 * This is the model class for table "tbl_lead_site".
 *
 * The followings are the available columns in table 'tbl_lead_site':
 * @property integer $id
 * @property integer $lead_id
 * @property integer $create_user_id
 * @property string $site_name
 * @property string $client_reference
 * @property string $external_reference
 * @property string $date_occupency
 * @property string $cost_code
 * @property string $change_tenancy
 * @property string $created
 */
class LeadSite extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tbl_lead_site';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('lead_id, create_user_id, site_name,  created', 'required'),
            array('lead_id, create_user_id', 'numerical', 'integerOnly' => true),
            array('site_name, client_reference, external_reference', 'length', 'max' => 255),
            array('cost_code', 'length', 'max' => 60),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, lead_id, create_user_id, site_name, client_reference, external_reference, date_occupency, cost_code, change_tenancy, created', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'lead' => array(self::BELONGS_TO, 'LeadContacts', 'lead_id'),
            'leadElc' => array(self::HAS_MANY, 'LeadElcMeter', 'lead_id'),
            'leadGas' => array(self::HAS_MANY, 'LeadGasMeter', 'lead_id'),
            'address' => array(self::HAS_ONE, 'LeadSiteAddress', 'site_id'),
            'contact' => array(self::HAS_ONE, 'LeadSiteContactDetails', 'site_id'),
            'additional' => array(self::HAS_ONE, 'LeadAdditional', 'site_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'lead_id' => 'Lead',
            'create_user_id' => 'Create User',
            'site_name' => 'Site Name',
            'client_reference' => 'Reference',
            'external_reference' => 'LOA Sent',
            'date_occupency' => 'Date Occupency',
            'cost_code' => 'LOA Registered',
            'change_tenancy' => 'LCA End Date',
            'created' => 'Created',
            'lead_id' => 'Choose Contact'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('lead_id', $this->lead_id);
        $criteria->compare('create_user_id', $this->create_user_id);
        $criteria->compare('site_name', $this->site_name, true);
        $criteria->compare('client_reference', $this->client_reference, true);
        $criteria->compare('external_reference', $this->external_reference, true);
        $criteria->compare('date_occupency', $this->date_occupency, true);
        $criteria->compare('cost_code', $this->cost_code, true);
        $criteria->compare('change_tenancy', $this->change_tenancy, true);
        $criteria->compare('created', $this->created, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function searchByLead($lead_id) {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->addCondition('lead_id = ' . $lead_id);
        $criteria->compare('create_user_id', $this->create_user_id);
        $criteria->compare('site_name', $this->site_name, true);
        $criteria->compare('client_reference', $this->client_reference, true);
        $criteria->compare('external_reference', $this->external_reference, true);
        $criteria->compare('date_occupency', $this->date_occupency, true);
        $criteria->compare('cost_code', $this->cost_code, true);
        $criteria->compare('change_tenancy', $this->change_tenancy, true);
        $criteria->compare('created', $this->created, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return LeadSite the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * 
     * @param type $lead_id
     * @return \CActiveDataProvider
     */
    public function siteSearch() {

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $user_id = Yii::app()->user->id;
        $lead = LeadContacts::model()->findByAttributes(array('user_id' => $user_id));
        if ($lead->id)
            $criteria->compare('lead_id', $lead->id);
        else {
            $criteria->compare('lead_id', $this->lead_id, true);
        }
        $criteria->compare('create_user_id', $this->create_user_id);
        $criteria->compare('site_name', $this->site_name, true);
        $criteria->compare('client_reference', $this->client_reference, true);
        $criteria->compare('external_reference', $this->external_reference, true);
        $criteria->compare('date_occupency', $this->date_occupency, true);
        $criteria->compare('cost_code', $this->cost_code, true);
        $criteria->compare('change_tenancy', $this->change_tenancy, true);
        $criteria->compare('created', $this->created, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

//    protected function beforeDelete() {
//        LeadBusiness::model()->deleteAll(' lead_id = ' . $this->id);
//        parent::beforeDelete();
//    }

    public function searchGrid($lead_id) {
//echo $lead_id; die;
        $criteria = new CDbCriteria;
        $criteria->addCondition('lead_id = ' . $lead_id);
        //$criteria->compare('lead_id', $lead_id);
        $criteria->compare('create_user_id', $this->create_user_id);
        $criteria->compare('site_name', $this->site_name, true);
        $criteria->compare('client_reference', $this->client_reference, true);
        $criteria->compare('external_reference', $this->external_reference, true);
        $criteria->compare('date_occupency', $this->date_occupency, true);
        $criteria->compare('cost_code', $this->cost_code, true);
        $criteria->compare('change_tenancy', $this->change_tenancy, true);
        $criteria->compare('created', $this->created, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * 
     * @param type $id
     */
    public function searchCustomerSite($id) {

        $criteria = new CDbCriteria;
        $criteria->with = array('lead');
        $criteria->addCondition('lead.user_id = ' . $id);
        $criteria->compare('create_user_id', $this->create_user_id);
        $criteria->compare('site_name', $this->site_name, true);
        $criteria->compare('client_reference', $this->client_reference, true);
        $criteria->compare('external_reference', $this->external_reference, true);
        $criteria->compare('date_occupency', $this->date_occupency, true);
        $criteria->compare('cost_code', $this->cost_code, true);
        $criteria->compare('change_tenancy', $this->change_tenancy, true);
        $criteria->compare('created', $this->created, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}
