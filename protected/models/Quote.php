<?php

/**
 * This is the model class for table "tbl_quote".
 *
 * The followings are the available columns in table 'tbl_quote':
 * @property integer $id
 * @property integer $price_id
 * @property integer $is_gas
 * @property string $commission_type
 * @property string $standing_charge
 * @property string $price
 * @property string $commission
 * @property string $annual_cost
 */
class Quote extends GxActiveRecord {

//    public $supplier;
//    public $set_name;
//    public $min_aq;
//    public $max_aq;
//    public $region;
//    public $payment_method;
//    public $contract_length;
//    public $business;
//    public $agency_id;
//    public $agency_name;
//    public $agent;
//    public $business_name;
//    public $live_date;
//    public $date;
//    public $invoice_no;
//    public $net_payment;
//    public $vat;
//    public $total;
    public $product;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tbl_quote';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('price_id, is_gas,create_user_id, commission_type, standing_charge, price, commission, annual_cost', 'required'),
            array('price_id, is_gas, current_supplier,status', 'numerical', 'integerOnly' => true),
            array('commission_type, standing_charge, price, commission,aq, annual_cost', 'length', 'max' => 255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, price_id,invoice_no,status, aq, current_supplier, vat,is_gas,create_user_id, is_select, commission_type, standing_charge, price, commission, annual_cost', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {

        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'priceModel' => array(self::BELONGS_TO, 'Price', 'price_id'),
            'user' => array(self::BELONGS_TO, 'Users', 'create_user_id'),
            'c_supplier' => array(self::BELONGS_TO, 'Users', 'current_supplier'),
            'contract' => array(self::HAS_ONE, 'ContractDetail', 'quote_id'),
                //    'agency1' => array(self::HAS_ONE, 'Agency', 'quote_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'price_id' => 'Price',
            'is_gas' => 'Is Gas',
            'commission_type' => 'Commission Type',
            'standing_charge' => 'Standing Charge',
            'price' => 'Price',
            'commission' => 'Commission',
            'annual_cost' => 'Total Payment',
            'create_user_id' => 'Created By',
            'is_select' => 'select',
            'invoice_no' => 'Invoice Number',
            'vat' => 'Vat',
            'aq' => 'AQ',
            'current_supplier' => 'Current Supplier',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search($is_select = null, $for_email = null) {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;
        $criteria->select = 't.*, a.agency_id, a.agent_id, a.price_id as apid, a.quote_id, a.agency_name,  a.create_user_id as a';
        $criteria->join = 'left JOIN tbl_contract_detail a ON  t.id = a.quote_id';
        //$criteria->join = 'LEFT JOIN tbl_price a ON tbl_quote.id = a.doc_id';
        $criteria->compare('id', $this->id);
        // 
        $criteria->compare('price_id', $this->price_id);
        $criteria->compare('is_gas', $this->is_gas);
        $criteria->compare('commission_type', $this->commission_type, true);
        $criteria->compare('standing_charge', $this->standing_charge, true);
        $criteria->compare('price', $this->price, true);
        $criteria->compare('commission', $this->commission, true);
        $criteria->compare('annual_cost', $this->annual_cost, true);
        $criteria->compare('invoice_no', $this->invoice_no, true);
        if (!Yii::app()->user->isAdmin)
            $criteria->compare('t.create_user_id', Yii::app()->user->id);
//        if (Yii::app()->user->isAgency) {
//            $users = Users::model()->findAllByAttributes(array('agency_id' => Yii::app()->user->id));
//
//            if ($users) {
//
//                foreach ($users as $user) {
//                    $agents[] = $user->id;
//                }
//                $agents = implode(",", $agents);
//                $criteria->condition = "a.agency_id = " . Yii::app()->user->id . " ||  a.agent_id IN ( $agents)";
//            } else {
//                $criteria->condition = 'a.agency_id = ' . Yii::app()->user->id;
//            }
//        }
//        if (Yii::app()->user->isUser) {
//            $criteria->condition = "a.agent_id = " . Yii::app()->user->id;
//        }
        if ($is_select)
            $criteria->addCondition('is_select = 1');
        else
            $criteria->addCondition('is_select = 0');

        $criteria->order = 'id desc';

        if ($for_email) {
            $criteria->limit = 3;
            return Quote::model()->findAll($criteria);
        }
        return $dataProvider = new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
        echo '<pre>';
        print_r($dataProvider->getData());
        die;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Quote the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getCompanyName() {
        $company_name = Company::model()->findByAttributes(array('create_user_id' => Yii::app()->user->id));
        if ($company_name)
            return $company_name = $company_name->company;
        else
            return 'N/A';
    }

    public function getNetPayment() {
        $contract = ContractDetail::model()->findByAttributes(array('quote_id' => $this->id));
        //  echo '<pre>'; print_r($contract); die;
    }

    public function getTotal() {
        echo $this->id;
        $contract = ContractDetail::model()->findByAttributes(array('quote_id' => $this->id));
        //  echo '<pre>'; print_r($contract); die;
    }

    /**
     * 
     * @return type
     */
    public function getAgencyId() {
        $contract = ContractDetail::model()->findByAttributes(array('quote_id' => $this->id));
        if ($contract)
            return ContractDetail::getAgencyId($contract->agency_id);
        return "N/A";
    }

    /**
     * 
     * @return type
     */
    public function getAgencyName() {
        $contract = ContractDetail::model()->findByAttributes(array('quote_id' => $this->id));
        //echo '<pre>'; print_r($contract); die;
        if ($contract) {
            return $contract->agency_name;
        } else {
            return "N/A";
        }
    }

    /**
     * 
     * @return type
     */
    public function getAgent() {
        $contract = ContractDetail::model()->findByAttributes(array('quote_id' => $this->id));
        if ($contract)
            return $contract->agency_name;
        return "N/A";
    }

    /**
     * 
     * @return type
     */
    public function getBusinessName() {
        $contract = ContractDetail::model()->findByAttributes(array('quote_id' => $this->id));
        return ($contract) ? $contract->company->company : "N/A";
    }

    /**
     * 
     * @return type
     */
    public function getLiveDate() {
        $contract = ContractDetail::model()->findByAttributes(array('quote_id' => $this->id));
        return ($contract) ? $contract->live_date : "N/A";
    }

    /**
     * 
     * @return type
     */
    public function getEstimateDate() {
        $contract = ContractDetail::model()->findByAttributes(array('quote_id' => $this->id));
        return ($contract) ? $contract->date_time : "N/A";
    }

    /**
     * 
     * @return type
     */
    public function getContractDate() {
        $contract = ContractDetail::model()->findByAttributes(array('quote_id' => $this->id));
        return ($contract) ? $contract->date_time : "N/A";
    }

}
