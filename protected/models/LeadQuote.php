<?php

/**
 * This is the model class for table "tbl_lead_quote".
 *
 * The followings are the available columns in table 'tbl_lead_quote':
 * @property integer $id
 * @property integer $customer_id
 * @property integer $supplier_id
 * @property integer $meter_ids
 * @property string $product
 * @property string $min_volume
 * @property string $max_volume
 * @property integer $credit_position
 * @property string $supply_contract
 * @property integer $offer_type
 * @property string $consumption
 * @property string $unit_rate
 * @property string $standing_charge
 * @property string $meter_no
 * @property string $created
 */
class LeadQuote extends GxActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tbl_lead_quote';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        return array(
            array('customer_id, create_user_id, supplier_id, meter_ids ,tender_id,  meter_type,  supply_contract, consumption,  standing_charge, created', 'required'),
            array('customer_id, supplier_id,contract_status,  credit_position, tender_id,offer_type', 'numerical', 'integerOnly' => true),
            array('product, min_volume, max_volume, supply_contract, consumption', 'length', 'max' => 255),
            array(' standing_charge', 'length', 'max' => 64),
            array('quote_ref_no', 'unique'),
            // The following rule is used by search().
// @todo Please remove those attributes that should not be searched.
            array('id, customer_id, supplier_id,live_date, start_date,end_date, contract_status, quote_ref_no,tender_id,is_resubmited,meter_type, meter_id, meter_ids, product, min_volume, max_volume, credit_position,create_user_id, supply_contract, offer_type, consumption, unit_rate, standing_charge, meter_no, created', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'customer' => array(self::BELONGS_TO, 'LeadContacts', 'customer_id'),
            'tender' => array(self::BELONGS_TO, 'LeadTender', 'tender_id'),
            'user' => array(self::BELONGS_TO, 'Users', 'supplier_id'),
            'site' => array(self::BELONGS_TO, 'LeadSite', 'meter_ids'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'customer_id' => 'Customer',
            'supplier_id' => 'Supplier',
            'meter_ids' => 'Meters',
            'product' => 'Product',
            'min_volume' => 'Min Volume',
            'max_volume' => 'Max Volume',
            'credit_position' => 'Credit Position',
            'supply_contract' => 'Supply Contract',
            'offer_type' => 'Offer Type',
            'consumption' => 'Consumption',
            'unit_rate' => 'Unit Rate',
            'standing_charge' => 'Standing Charge',
            'meter_no' => 'Meter No',
            'created' => 'Created',
            'meter_id' => "Meter Name",
            'is_resubmited' => 'Submit Contract',
            'tender_id' => 'Tender',
            'quote_ref_no' => 'Quote Refference No'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search($cid = null) {
// @todo Please modify the following code to remove attributes that should not be searched.
//echo $cid; die;
        $criteria = new CDbCriteria;
        $criteria->compare('id', $this->id);
        if (!$cid) {
            $criteria->compare('customer_id', $this->customer_id);
        } else {
            $lead = LeadContacts::model()->findByAttributes(array('user_id' => $cid));
            //     echo '<pre>'; print_r($lead); die;
            if ($lead && $lead->id) {
                $criteria->compare('customer_id', $lead->id);
            } else {
                $criteria->compare('customer_id', "Cust11");
            }
        }
        if (!Yii::app()->user->isAdmin && !Yii::app()->user->isCustomer) {
            $criteria->compare('create_user_id', Yii::app()->user->id);
        }
        $criteria->compare('supplier_id', $this->supplier_id);
        $criteria->compare('meter_ids', $this->meter_ids);
        $criteria->compare('product', $this->product, true);
        $criteria->compare('min_volume', $this->min_volume, true);
        $criteria->compare('max_volume', $this->max_volume, true);
        $criteria->compare('credit_position', $this->credit_position);
        $criteria->compare('supply_contract', $this->supply_contract, true);
        $criteria->compare('offer_type', $this->offer_type);
        $criteria->compare('consumption', $this->consumption, true);
        $criteria->compare('unit_rate', $this->unit_rate, true);
        $criteria->compare('standing_charge', $this->standing_charge, true);
        $criteria->compare('meter_no', $this->meter_no, true);
        $criteria->compare('created', $this->created, true);
        $criteria->compare('quote_ref_no', $this->quote_ref_no, true);
        $criteria->order = "id desc";

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return LeadQuote the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getUnit_rate() {
        if ($this->unit_rate) {
            $unit = unserialize($this->unit_rate);
            return implode(", ", $unit);
        } else {
            return "N/A";
        }
    }

    public static function getMeterName($id, $type) {
        $table = 'Lead' . $type . 'Meter';
        return ($table) ? $table::model()->findByPk($id)->meter_name : "N/A";
    }

    public function getDownload() {
        return '<a href="' . Yii::app()->createAbsoluteUrl("admin/product/download/file/$this->supply_contract") . '"><i class="fa fa-download">Download Contract</a>';
    }

    /**
     * 
     * @param type $meter_ids
     * @param type $type
     */
    public static function getSiteName($meter_ids, $type, $is_meter = null) {
        $class = "Lead" . $type . "Meter";
        $meter_ids = unserialize($meter_ids);
        $criteria = new CDbCriteria;
        $criteria->addInCondition('id', $meter_ids);
        $meters = $class::model()->findAll($criteria);
        $html = '';
        if ($meters)
            foreach ($meters as $meter) {
                if ($is_meter)
                    $html .= $meter->meter_name . ", ";
                else
                    $html .= $meter->site->site_name . ", ";
            }
        return rtrim($html, ", ");
    }

    /**
     * 
     * @param type $id
     */
    public static function quoteStatus($id, $cid) {
        $quote = LeadQuote::model()->findByAttributes(array('tender_id' => $id));
        if (!$quote) {
            return CHtml::link('Send Input Quote', array('/admin/leadContacts/sendQuote', "tender_id" => $id, "cid" => $cid), array("class" => 'btn btn-success send ',));
        } else {
            return '<button class="btn btn-success">Already Sent</button>';
        }
    }

    public function getQuoteStatus() {
        if (Yii::app()->user->isCustomer) {
            return ($this->quote_status == 0) ? CHtml::link('Accept', array('/admin/leadQuote/accept', "id" => $this->id), array("class" => 'btn btn-success accept', 'onclick' => 'accept()',)) : '<button class="btn btn-info" > Accepted </button>';
        } else if (Yii::app()->user->isAdmin) {
            return ($this->quote_status == 0) ? '<button class="btn btn-danger" >Not Accepted </button>' : '<button class="btn btn-info" > Accepted </button>' . CHtml::link('Corporate', array('/admin/agency/corporate', "id" => $this->id), array("class" => 'btn btn-success'));
        }
    }

}
