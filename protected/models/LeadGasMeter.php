<?php

/**
 * This is the model class for table "tbl_lead_gas_meter".
 *
 * The followings are the available columns in table 'tbl_lead_gas_meter':
 * @property integer $id
 * @property integer $lead_id
 * @property integer $create_user_id
 * @property string $meter_name
 * @property string $mpr
 * @property string $total_aq
 * @property string $current_supplier
 * @property string $contract_end_date
 * @property string $meter_status
 * @property string $meter_pricing
 * @property string $created
 */
class LeadGasMeter extends GxActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tbl_lead_gas_meter';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('create_user_id,site_id,corporate_sme, utility, meter_name, current_supplier, contract_end_date, created', 'required'),
            array('lead_id, serial_no, corporate_sme, utility, create_user_id', 'numerical', 'integerOnly' => true),
            array('meter_name, serial_no, smart_meter, mpr,document, total_aq, current_supplier, meter_status, meter_pricing', 'length', 'max' => 255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, lead_id, serial_no, corporate_sme,document, utility, create_user_id,site_id, smart_meter , meter_name, mpr, total_aq, current_supplier, contract_end_date, meter_status, meter_pricing, created', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'site' => array(self::BELONGS_TO, 'LeadSite', 'site_id'),
            'lead' => array(self::BELONGS_TO, 'LeadContacts', 'lead_id'),
            'supplier' => array(self::BELONGS_TO, 'Users', 'current_supplier')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'lead_id' => 'Lead',
            'create_user_id' => 'Create User',
            'serial_no' => 'Serial No',
            'meter_name' => 'Meter Name',
            'corporate_sme' => 'Corporate Sme',
            'utility' => 'Utility',
            'mpr' => 'Mpr',
            'smart_meter' => 'Smart Meter',
            'total_aq' => 'Consumption',
            'current_supplier' => 'Current Supplier',
            'contract_end_date' => 'Contract End Date',
            'meter_status' => 'Meter Status',
            'meter_pricing' => 'Meter Pricing',
            'created' => 'Created',
            'site_id' => 'Site',
            'lead_id' => 'Choose Contact',
            'document' => 'Document'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('lead_id', $this->lead_id);
        $criteria->compare('serial_no', $this->serial_no);
        $criteria->compare('create_user_id', $this->create_user_id);
        $criteria->compare('meter_name', $this->meter_name, true);
        $criteria->compare('mpr', $this->mpr, true);
        $criteria->compare('total_aq', $this->total_aq, true);
        $criteria->compare('current_supplier', $this->current_supplier, true);
        $criteria->compare('contract_end_date', $this->contract_end_date, true);
        $criteria->compare('meter_status', $this->meter_status, true);
        $criteria->compare('meter_pricing', $this->meter_pricing, true);
        $criteria->compare('created', $this->created, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function searchByLead($lead_id) {
        // @todo Please modify the following code to remove attributes that should not be searched.
        $criteria = new CDbCriteria;
        $criteria->compare('id', $this->id);
        $criteria->with = array('site');
        $criteria->addCondition('site.lead_id =' . $lead_id);
        $criteria->compare('serial_no', $this->serial_no);
        $criteria->compare('create_user_id', $this->create_user_id);
        $criteria->compare('meter_name', $this->meter_name, true);
        $criteria->compare('mpr', $this->mpr, true);
        $criteria->compare('total_aq', $this->total_aq, true);
        $criteria->compare('current_supplier', $this->current_supplier, true);
        $criteria->compare('contract_end_date', $this->contract_end_date, true);
        $criteria->compare('meter_status', $this->meter_status, true);
        $criteria->compare('meter_pricing', $this->meter_pricing, true);
        $criteria->compare('created', $this->created, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function searchGrid($lead_id) {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->with = array('site');
        //   $criteria->compare('lead_id', $this->lead_id);
        $criteria->addCondition('site.lead_id = ' . $lead_id);
        $criteria->compare('serial_no', $this->serial_no);
        $criteria->compare('create_user_id', $this->create_user_id);
        $criteria->compare('meter_name', $this->meter_name, true);
        $criteria->compare('mpr', $this->mpr, true);
        $criteria->compare('total_aq', $this->total_aq, true);
        $criteria->compare('current_supplier', $this->current_supplier, true);
        $criteria->compare('contract_end_date', $this->contract_end_date, true);
        $criteria->compare('meter_status', $this->meter_status, true);
        $criteria->compare('meter_pricing', $this->meter_pricing, true);
        $criteria->compare('created', $this->created, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    function searchBySite($id) {
        $criteria = new CDbCriteria;
        $criteria->compare('id', $this->id);
        $criteria->addCondition('site_id = ' . $id);
        $criteria->compare('serial_no', $this->serial_no);
        $criteria->compare('create_user_id', $this->create_user_id);
        $criteria->compare('meter_name', $this->meter_name, true);
        $criteria->compare('mpr', $this->mpr, true);
        $criteria->compare('total_aq', $this->total_aq, true);
        $criteria->compare('current_supplier', $this->current_supplier, true);
        $criteria->compare('contract_end_date', $this->contract_end_date, true);
        $criteria->compare('meter_status', $this->meter_status, true);
        $criteria->compare('meter_pricing', $this->meter_pricing, true);
        $criteria->compare('created', $this->created, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return LeadGasMeter the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * 
     * @param type $id
     * @param type $is_site
     * @return type
     */
    public static function getCount($id, $is_site = null) {
        $criteria = new CDbCriteria;
        $criteria->addCondition('lead_id = ' . $id);
        $sites = LeadSite::model()->findAll($criteria);
//echo '<pre>'; print_r($sites); 
        if ($is_site)
            return count($sites);
        $gasmeters = 0;
        if ($sites) {
            foreach ($sites as $site) {
                $criteria = new CDbCriteria;
                $criteria->addCondition('site_id = ' . $site->id);
                $gasmeters = LeadGasMeter::model()->findAll($criteria);
            }
            return count($gasmeters);
        }
        return $gasmeters;
    }

    public static function getSiteDropdown() {
        $html = '<select id="LeadGasMeter_site_id_" class="form-control" name="LeadGasMeter[site_id]">';
        $html .= '<option value="">Please select site</option>';
        $html .= '</select>';
        return $html;
    }

    public static function getSupplierDropdown() {
        $html = '<select id="LeadGasMeter_current_supplier_" class="form-control" name="LeadGasMeter[current_supplier]">';
        $sites = Users::model()->findAllByAttributes(array('role_id' => 2));
        foreach ($sites as $site) {
            $html .= '<option value="' . $site->id . '">' . $site->username . '</option>';
        }
        $html .= '</select>';
        return $html;
    }

    public static function getCorporateDropdown() {
        $html = ' <select id="LeadGasMeter_corporate_sme_" class="form-control" onchange="getUtilities1($(this).val())" name="LeadGasMeter[corporate_sme]">';
        $sites = Type::model()->findAllByAttributes(array('parrent' => 0));
        foreach ($sites as $site) {
            $html .= '<option value="' . $site->id . '">' . $site->title . '</option>';
        }
        $html .= '</select>';
        return $html;
    }

    public static function getUtilityDropdown() {
        $criteria = new CDbCriteria;
        $criteria->condition = 'parrent != 0 && status =1 ';
        $html = '<select id="LeadGasMeter_utility_" class="form-control" name="LeadGasMeter[utility]">';
        $sites = Type::model()->findAll($criteria);
        foreach ($sites as $site) {
            $html .= '<option value="' . $site->id . '">' . $site->title . '</option>';
        }
        $html .= '</select>';
        return $html;
    }

    public static function getStatusDropdown() {
        $html = '<select id="LeadGasMeter_meter_status_" class="form-control" name="LeadGasMeter[meter_status]">';
        $sites = MeterStatus::model()->findAll();
        foreach ($sites as $site) {
            $html .= '<option value="' . $site->id . '">' . $site->title . '</option>';
        }
        $html .= '</select>';
        return $html;
    }

}
