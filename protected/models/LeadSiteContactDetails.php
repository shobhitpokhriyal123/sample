<?php

/**
 * This is the model class for table "tbl_lead_site_contact_details".
 *
 * The followings are the available columns in table 'tbl_lead_site_contact_details':
 * @property integer $id
 * @property integer $create_user_id
 * @property string $title
 * @property string $full_name
 * @property string $position
 * @property string $email
 * @property string $landline
 * @property string $mobile
 * @property integer $status
 * @property string $created
 */
class LeadSiteContactDetails extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tbl_lead_site_contact_details';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('create_user_id', 'required'),
            array('create_user_id, status', 'numerical', 'integerOnly' => true),
            array('title, full_name, position, email', 'length', 'max' => 255),
            array('landline, site_id, mobile', 'length', 'max' => 11),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, create_user_id, title,site_id, full_name, position, email, landline, mobile, status, created', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            //array(self::)
            'site' => array(self::BELONGS_TO, 'LeadSite', 'site_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'create_user_id' => 'Create User',
            'title' => 'Title',
            'full_name' => 'Site Contact',
            'position' => 'Position',
            'email' => 'Email',
            'landline' => 'Landline',
            'mobile' => 'Mobile',
            'status' => 'Status',
            'created' => 'Created',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('create_user_id', $this->create_user_id);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('full_name', $this->full_name, true);
        $criteria->compare('position', $this->position, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('landline', $this->landline, true);
        $criteria->compare('mobile', $this->mobile, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('created', $this->created, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function searchGrid($lead_id) {
        // @todo Please modify the following code to remove attributes that should not be searched.
        $sql = "select  id from tbl_lead_site where lead_id = $lead_id";
        $sites = Yii::app()->db->createCommand($sql)->queryAll();
        $arr = array();
        if (!empty($sites)) {
            foreach ($sites as $site) {
                $arr[] = $site['id'];
            }
        }
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('create_user_id', $this->create_user_id);
        $criteria->compare('title', $this->title, true);

        $criteria->compare('full_name', $this->full_name, true);
        if (!empty($arr)) {
            $criteria->addInCondition('site_id', $arr);
        }
        $criteria->compare('position', $this->position, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('landline', $this->landline, true);
        $criteria->compare('mobile', $this->mobile, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('created', $this->created, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return LeadSiteContactDetails the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * 
     * @return string
     */
    public static function getSiteDropdown() {

        $html = '<select id="LeadSiteContactDetails_site_id_" class="form-control" name="LeadSiteContactDetails[site_id]">';
        $sites = LeadSite::model()->findAll();
        foreach ($sites as $site) {
            $html .= '<option value="' . $site->id . '">' . $site->site_name . '</option>';
        }
        $html .= '</select>';
        return $html;
    }

    /**
     * 
     * @return string
     */
    public static function getLeadDropdown() {

        $html = '<select id="LeadSite_lead_id_" class="form-control" name="LeadSite[lead_id]">';
        $sites = LeadContacts::model()->findAll();
        foreach ($sites as $site) {
            $html .= '<option value="' . $site->id . '">' . $site->first_name . '</option>';
        }
        $html .= '</select>';
        return $html;
    }

}
