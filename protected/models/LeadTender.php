<?php

/**
 * This is the model class for table "tbl_lead_tender".
 *
 * The followings are the available columns in table 'tbl_lead_tender':
 * @property integer $id
 * @property integer $contract_duration
 * @property string $supplier_ids
 * @property integer $business_name
 * @property integer $meter_id
 * @property integer $meter_type
 * @property integer $contract_type
 * @property integer $bespoke_duration
 * @property integer $commission_discount
 * @property string $tender_return_date
 * @property string $com_reg
 * @property string $commodity_type
 * @property integer $uplift
 * @property string $term_length
 * @property string $fixed_meter
 * @property string $payment
 * @property string $energy
 * @property string $supplier_pref
 * @property string $comments
 * @property string $created
 */
class LeadTender extends GxActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tbl_lead_tender';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        return array(
            array('contract_duration,agent_id,agency_id, supplier_ids, business_name, meter_id,  created', 'required'),
            array('contract_duration,agent_id,agency_id, status,is_accessible, customer_id,  contract_type', 'numerical', 'integerOnly' => true),
            array('supplier_ids', 'length', 'max' => 255),
            array('com_reg, payment, energy', 'length', 'max' => 50),
            array('term_length, fixed_meter,comments', 'length', 'max' => 100),
            array('supplier_pref', 'length', 'max' => 20),
            // The following rule is used by search().
// @todo Please remove those attributes that should not be searched.
            array('id, contract_duration,customer_id,document, status,is_accessible, supplier_ids, business_name, meter_id, meter_type, contract_type, bespoke_duration, commission_discount, tender_return_date, com_reg, commodity_type, uplift, term_length,agent_id,agency_id, fixed_meter, payment, energy, supplier_pref, comments, created', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'uploads' => array(self::HAS_MANY, 'LeadTenderUpload', 'tender_id'),
            'customer' => array(self::BELONGS_TO, 'LeadContacts', 'customer_id'),
            'agency' => array(self::BELONGS_TO, 'Users', 'agency_id'),
            'agent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'contract_duration' => 'Contract Duration',
            'supplier_ids' => 'Suppliers',
            'business_name' => 'Business Name',
            'meter_id' => 'Meters',
            'meter_type' => 'Meter Type',
            'contract_type' => 'Contract Type',
            'bespoke_duration' => 'Bespoke Duration',
            'commission_discount' => 'Commission Discount',
            'tender_return_date' => 'Tender Return Date',
            'com_reg' => 'Com Reg',
            'commodity_type' => 'Commodity Type',
            'uplift' => 'Uplift',
            'term_length' => 'Term Length',
            'fixed_meter' => 'Fixed Meter',
            'payment' => 'Payment',
            'energy' => 'Energy',
            'supplier_pref' => 'Supplier Pref',
            'comments' => 'Comments',
            'created' => 'Created',
            'customer_id' => 'Customer',
            'document' => 'Document',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search($cid = null) {
// @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('contract_duration', $this->contract_duration);

        $criteria->compare('supplier_ids', $this->supplier_ids, true);
        $criteria->compare('business_name', $this->business_name, true);
        $criteria->compare('meter_id', $this->meter_id);
        $criteria->compare('meter_type', $this->meter_type);
        $criteria->compare('contract_type', $this->contract_type);
        $criteria->compare('bespoke_duration', $this->bespoke_duration);
        $criteria->compare('commission_discount', $this->commission_discount);
        $criteria->compare('tender_return_date', $this->tender_return_date, true);
        $criteria->compare('com_reg', $this->com_reg, true);
        $criteria->compare('commodity_type', $this->commodity_type, true);
        $criteria->compare('uplift', $this->uplift);
        if (Yii::app()->user->isAgency) {
            $criteria->addCondition('agency_id = ' . Yii::app()->user->id);
        }
        if (Yii::app()->user->isUser) {
            $criteria->addCondition('agent_id = ' . Yii::app()->user->id);
        }

        if ($cid) {
            $criteria->compare('customer_id', $cid);
        } else {
            $criteria->compare('customer_id', $this->customer_id);
        }
        $criteria->compare('term_length', $this->term_length, true);
        $criteria->compare('fixed_meter', $this->fixed_meter, true);
        $criteria->compare('payment', $this->payment, true);
        $criteria->compare('energy', $this->energy, true);
        $criteria->compare('supplier_pref', $this->supplier_pref, true);
        $criteria->compare('comments', $this->comments, true);
        $criteria->compare('created', $this->created, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return LeadTender the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getSupplierName() {
        $ids = unserialize($this->supplier_ids);
        $names = '';
        foreach ($ids as $id) {
            $user = Users::model()->findByPk($id);
            $names .= $user->username . ", ";
        }
        return rtrim($names, ", ");
    }

    public function getMeterName($is_site = null) {
        $ids = unserialize($this->meter_id);
        $names = '';
        $class = "Lead" . $this->meter_type . "Meter";
        foreach ($ids as $id) {
            $user = $class::model()->findByPk($id);
            if ($is_site && $user)
                $names .= $user->site->site_name . ", ";
            else if ($user)
                $names .= $user->meter_name . ", ";
        }
        $names = rtrim($names, ", ");
        return $names;
    }

    /**
     * 
     * @param type $business
     * @return string
     * 
     */
    public static function getAgent($business) {
        $lead = LeadBusiness::model()->findByAttributes(array('business_name' => $business));
        if ($lead) {
            return $lead->lead->agent->username;
        } else {
            return "N/A";
        }
    }

    /**
     * 
     * @param type $business
     * @return string
     */
    public static function getAgency($business) {
        $lead = LeadBusiness::model()->findByAttributes(array('business_name' => $business));
        if ($lead) {
            return $lead->lead->broker->username;
        } else {
            return "N/A";
        }
    }

    public static function getMeters($meter_id, $type) {
        $meters = unserialize($meter_id);
        $class = "Lead" . $type . 'Meter';
        $criteria = new CDbCriteria;
        $criteria->addInCondition('id', $meters);
        $metsdata = $class::model()->findAll();
        if ($metsdata) {
            foreach ($metsdata as $metsdata) {
                return Product::getUtilites($metsdata->utility);
            }
        } else {
            return "N/A";
        }
    }

    /**
     * 
     * @param type $id
     * @return string
     */
    public function getStatus($id = null) {
        $status = array('1' => 'Accepted', '2' => 'Rejected', '3' => 'In Progress');
        if ($id)
            return $status[$id];
        return $status;
    }

}
