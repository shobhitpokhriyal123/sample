<?php

/**
 * This is the model class for table "tbl_elec_meter".
 *
 * The followings are the available columns in table 'tbl_elec_meter':
 * @property integer $id
 * @property integer $mtype_id
 * @property integer $days
 * @property integer $night
 * @property integer $day_night_weekend
 * @property integer $stdo_6
 */
class ElecMeter extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_elec_meter';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('mtype_id', 'required'),
			array('mtype_id, days, night, day_night_weekend, stdo_6', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, mtype_id, days, night, day_night_weekend, stdo_6', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'base'=>array(self::BELONGS_TO, 'MeterBase', 'mtype_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'mtype_id' => 'Mtype',
			'days' => 'Days %',
			'night' => 'Night %',
			'day_night_weekend' => 'Day Night Weekend %',
			'stdo_6' => 'Stdo 6 %',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('mtype_id',$this->mtype_id);
		$criteria->compare('days',$this->days);
		$criteria->compare('night',$this->night);
		$criteria->compare('day_night_weekend',$this->day_night_weekend);
		$criteria->compare('stdo_6',$this->stdo_6);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ElecMeter the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
