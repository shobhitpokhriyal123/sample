<?php

/**
 * This is the model class for table "tbl_users".
 *
 * The followings are the available columns in table 'tbl_users':
 * @property integer $id
 * @property string $fullname
 * @property string $email
 * @property string $mobile
 * @property string $username
 * @property string $password
 * @property string $image
 * @property integer $role_id
 * @property integer $active
 * @property string $created
 */
class Users extends GxActiveRecord {

    public $supplier_id;

    //public $supplier_id;

    const ROLE_ACTIVE = 1;
    const ROLE_INACTIVE = 0;

    public $confirm_password;

    const ROLEADMIN = 1;
    const ROLEASUPPLIER = 2;
    const ROLEAGENCY = 3;
    const ROLEAGENT = 4;
    const ROLECUSTOMER = 5;

    //public confirm
    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tbl_users';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('fullname,username, role_id', 'required'),
            array('username', 'match', 'pattern' => '/^[A-Za-z0-9_]+$/u'),
//            array('fullname, username, email, password, confirm_password, role_id', 'required', 'on' => 'new_user'),
//            array('confirm_password', 'compare', 'compareAttribute' => 'password', 'on' => 'new_user'),
//            
//            array('confirm_password', 'compare', 'compareAttribute' => 'password', 'on' => 'new_lead'),
//            
//            array('fullname, username, email, password, confirm_password', 'required', 'on' => 'profile_update'),
//            array('confirm_password', 'compare', 'compareAttribute' => 'password', 'on' => 'profile_update'),
//            array('username, email', 'unique', 'on' => 'profile_update'),
//            array('email', 'email', 'on' => 'profile_update'),
            array('username, email', 'unique', 'on' => 'new_user'),
//            array('username, email, password, confirm_password, role_id', 'required', 'on' => 'customer'),
//            array('confirm_password', 'compare', 'compareAttribute' => 'password', 'on' => 'customer'),
            array('username, email', 'unique', 'on' => 'customer'),
//            // array('email, username', 'unique','on' => 'new_user,'),
//            array('username,  password, role_id', 'required', 'on' => 'new_agency'),
            array('username', 'unique', 'on' => 'new_agency'),
//            array('role_id, active', 'numerical', 'integerOnly' => true),
//            array('fullname, email, mobile, username, password', 'length', 'max' => 255),
            array('email', 'email'),
            array('email', 'unique', 'className' => 'Users',
                'attributeName' => 'email',
                'message' => 'This Email is already in use'),
            array('username', 'unique', 'className' => 'Users',
                'attributeName' => 'username',
                'message' => 'This username is already in use'),
            array('mobile', 'numerical', 'integerOnly' => true),
            array('email', 'required', 'on' => 'forgot'),
            //array('supplier_id', 'required', 'on' => 'types'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, fullname,tac,description, auc, agency_id, password_plain, umc, sub_type_id,parrent_type_id, email,permanent_address,current_address,  mobile, username, password, image, role_id, active, created', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'rolename' => array(self::BELONGS_TO, 'Role', 'role_id'),
            'agencies' => array(self::HAS_MANY, 'Agency', 'user_id'),
            'agency' => array(self::BELONGS_TO, 'Agency', 'agency_id'),
            'agency1' => array(self::HAS_ONE, 'Agency', 'agency_id'),
            'broker' => array(self::HAS_ONE, 'LeadContacts', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'fullname' => 'Fullname',
            'email' => 'Email',
            'mobile' => 'Mobile',
            'username' => 'Username',
            'password' => 'Password',
            'confirm_password' => 'Confirm Password',
            'image' => '<i class="fa fa-image"></i> User Logo',
            'role_id' => 'Role',
            'active' => 'Status',
            'created' => 'Created',
            'current_address' => 'Current Address',
            'permanent_address' => 'Permanent Address',
            'parrent_type_id' => 'Product Type',
            'sub_type_id' => 'Product Sub Type',
            'tac' => 'See the total amount of commission',
            'auc' => 'See all of the companies from all users',
            'umc' => 'See the user management screen',
            'agency_id' => 'Select Agency',
            'description' => 'About Supplier'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search($role) {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;
//echo $role; die;
        $criteria->compare('id', $this->id);
        $criteria->compare('fullname', $this->fullname, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('mobile', $this->mobile, true);
        $criteria->compare('username', $this->username, true);
        $criteria->compare('password', $this->password, true);
        $criteria->compare('role_id', $role);
        $criteria->compare('image', $this->image, true);
        $criteria->compare('active', $this->active, true);
        $criteria->compare('created', $this->created, true);
        $criteria->compare('parrent_type_id', $this->created, true);
        $criteria->compare('agency_id', $this->agency_id, true);
        $criteria->compare('description', $this->description, true);


        // $criteria->compare('sub_type_id', $this->created, true);
        //$criteria->compare('active', 1);
        $criteria->order = " id desc";
        // echo '<pre>'; print_r($criteria); die; 
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Users the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * 
     * @return boolean
     */
    public function beforeSave() {

        if (parent::beforeSave()) {
            if (isset($_POST['Users']) && isset($_POST['Users']['password']) && $_POST['Users']['password'] !='' ) {
                $this->password_plain = $_POST['Users']['password'];
            }
            if ($this->isNewRecord) {
                $this->created = new CDbExpression('NOW()');
            }
        }
        return true;
    }

    /**
     * 
     * @param type $status
     * @return string
     */
    public function getStatus($status = null) {
        // echo $status; die;
        $statusArr = array(self::ROLE_ACTIVE => 'Active', self::ROLE_INACTIVE => 'Inactive');
        if ($status != null) {
            $statusArr = $statusArr[$status];
        }

        return $statusArr;
    }

    /**
     * 
     */
    public function getProductTypes() {
        return Type::model()->findAllByAttributes(array('parrent' => 0));
    }

    /**
     * 
     */
    public function getSubProductTypes($id = null) {

        $criteria = new CDbCriteria;
        if ($id) {
            $criteria->addCondition('parrent =' . $id);
        } else
            $criteria->addCondition('parrent != 0');
        $types = Type::model()->findAll($criteria);
        foreach ($types as $type) {
            $title = Type::model()->findByPk($type->parrent)->title;
            $type->title = $type->title . ' - ' . $title;
        }
        return $types;
        // echo '<pre>'; print_r($types); die;
        //return Type::model()->findAllByAttributes(array('parrent !='=>0));
    }

    public static function getAllSuppliers() {
        $criteria = new CDbCriteria;
        $criteria->addCondition('role_id = 2');
        $criteria->addCondition('active = 1');
        // $criteria->active = 1;
        return self::model()->findAll($criteria);
    }

    public function sendMail($user) {
        $pwd = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);

        $to = "$user->email";
        $subject = "Password - Reset";
        $txt = "";
        $txt .= "Hello <br>";
        $txt .= "We are reseting your password of utility cloud... <br><br>";
        $txt .= " Your new password of username " . $user->username;

        $txt .= " &nbsp; Password: <b>" . $pwd . " </b> <br>";
        $txt = htmlentities($txt);
        $headers = "From: noreply@utilitycloudlive.co.uk" . "\r\n";
        $headers .= 'MIME-Version: 1.0' . "\r\n";
        $headers .= "Content-Type: text/html; charset=UTF-8\r\n";


        if (@mail($to, $subject, $txt, $headers)) {
            $user->password = md5($pwd);
            $user->save();
            die('sent');
            return true;
        }
        die('not sent');
        return false;
    }

    public function beforeDelete() {
        // Product::model()->deleteAllByAttributes(array('assigned_to' => $this->id));
        //  Fields::model()->deleteAllByAttributes(array('assigned_to' => $this->id));
        return true;
        parent::beforeDelete();
    }

    public static function getAgencyName($id) {
        return self::model()->findByPk($id)->username;
    }

    public static function isPermission($user_id, $module_id) {
        $permission = Permissions::model()->findByAttributes(array('agency_id' => $user_id));
        if ($permission) {
            $modules = $permission->module_id;
            $modules = explode(",", $modules);
            if (in_array($module_id, $modules)) {
                return true;
            }
            return false;
        } else {
            return true;
        }
    }

    public function getAgency() {
        $criteria = new CDbCriteria;
        $criteria->condition = 'agency_id != ""';
        $agency = Users::model()->findAll($criteria);
        $ag = array();
        if ($agency)
            foreach ($agency as $agency) {
                $ag[$agency->id] = $agency['username'];
            }
        return $ag;
    }

    public function recover() {
        Yii::import('ext-prod.yii-mail.YiiMailMessage');
        $message = new YiiMailMessage ();
        $message->view = 'recover_account';
        $message->setSubject('Password Recovery for: ' . Yii::app()->params ['company']);
        // userModel is passed to the view
        $message->setBody(array(
            'model' => $this
                ), 'text/html');
        // echo $message->body;
        // Yii::app()->end();
        $message->addTo($this->email);
        $message->from = Yii::app()->params ['adminEmail'];
        Yii::app()->mail->send($message);
    }

    public function sendPassword() {


        Yii::import('application.extensions.phpmailer.JPhpMailer');
        $mail = new JPhpMailer(true);
        $password = self::randomPassword();
        $this->setPassword($password, $password);
        $mail->AddReplyTo('noreply@utilitycloudlive.co.uk', 'Utilitycloudlive');
        $mail->AddAddress($this->email, 'Utilitycloudlive');
        $mail->SetFrom('noreply@utilitycloudlive.co.uk', 'Utility Cloud Live..!');
        $mail->Subject = 'Recover Password';
        $mail->AltBody = 'Utility Cloud Live..!'; // optional - MsgHTML will create an alternate automatically
        $body = Yii::app()->controller->renderPartial('//mail/send_password', array('model' => $this, 'password' => $password), true);
        $mail->MsgHTML("$body");
        if ($mail->Send()) {
            return true;
        } else {
            return false;
        }

        //   echo '<pre>'; print_r($this->email); die;
//        $password = self::randomPassword();
//        $this->setPassword($password, $password);
//
//        $body = 'Email ID: ' . $this->email . "<br>\r\n";
//        $body .= 'Password: ' . $password . "\r\n";
////echo $body; die;
//        Yii::import('ext.yii-mail.YiiMailMessage');
//        $message = new YiiMailMessage ();
//        $message->view = 'send_password';
//        $message->setSubject('Your new Password for: ');
//        // userModel is passed to the view
//
//        $message->setBody(array(
//            'model' => $this,
//            'password' => $password,
//            'body' => $body
//                ), 'text/html');
//
////        echo $message->body;
////  die('dsdsd');
//        //  echo $this->email; die;
//        $message->addTo($this->email);
//        $message->from = Yii::app()->params ['adminEmail'];
//        //  echo '<pre>';  print_r($message); die;
//        if (Yii::app()->mail->send($message)) {
//            return true;
//        }
//        return false;
    }

    public static function randomPassword($count = 8, $onlyNum = false) {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        // $alphabet = "abcdefghijklmnopqrstuwxyz0123456789";
        if ($onlyNum) {
            $alphabet = "0123456789";
        }
        $pass = array(); // remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; // put the length -1 in cache
        for ($i = 0; $i < $count; $i ++) {
            $n = rand(0, $alphaLength);
            $pass [] = $alphabet [$n];
        }
        return implode($pass);
        // turn the array into a string
    }

    public function setPassword($password, $password_2) {
        if ($password != '' && $password == $password_2) {
            $this->password = md5($password);
            return $this->save(false, 'password');
        }
        return false;
    }

    public static function getAgencies() {
        $criteria = new CDbCriteria;
        if (!Yii::app()->user->isAdmin) {
            $criteria->addCondition('id = ' . Yii::app()->user->id);
        } else
            $criteria->addInCondition('role_id', array(3, 1));
        $agency = Users::model()->findAll($criteria);
        $ag = array();
        if ($agency)
            foreach ($agency as $agency) {
                $ag[$agency->id] = $agency['username'];
            }
        return $ag;
    }

    
    
}
