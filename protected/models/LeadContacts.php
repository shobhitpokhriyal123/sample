<?php

/**
 * This is the model class for table "tbl_lead_contacts".
 *
 * The followings are the available columns in table 'tbl_lead_contacts':
 * @property integer $id
 * @property integer $create_user_id
 * @property string $title
 * @property string $first_name
 * @property string $sur_name
 * @property string $position
 * @property string $email
 * @property integer $landline
 * @property integer $mobile
 * @property integer $status
 * @property string $created
 */
class LeadContacts extends CActiveRecord {

    public $business;
    public $gas_meter;
    public $corporate_sme;
    public $utility;
    public $electric_meter;
    public $is_user_detail;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tbl_lead_contacts';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('create_user_id, user_id,  is_lead, agency_id, agent_id, status', 'required'),
            array('create_user_id, landline, mobile,is_lead, status', 'numerical', 'integerOnly' => true),
            array('title, first_name, landline, mobile, sur_name, position, email', 'length', 'max' => 255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, create_user_id,user_id , title, first_name,is_lead, sur_name,agency_name, agency_id,agent_id, position,  landline, mobile, status, created', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
            'agent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
            'site' => array(self::HAS_MANY, 'LeadSite', 'site_id'),
            'broker' => array(self::BELONGS_TO, 'Users', 'agency_id'),
            'supplier' => array(self::BELONGS_TO, 'Users', 'current_supplier'),
            'status' => array(self::BELONGS_TO, 'MeterStatus', 'meter_satatus'),
            'leadaddress' => array(self::HAS_ONE, 'LeadAddress', 'lead_id'),
            'business' => array(self::HAS_ONE, 'LeadBusiness', 'lead_id'),
            
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'create_user_id' => 'Create User',
            'title' => 'Title',
            'agency_id' => 'Brocker ID',
            'agency_name' => 'Agency Name',
            'agent_id' => 'Agent ID',
            'first_name' => 'First Name',
            'sur_name' => 'Surname',
            'position' => 'Position',
            'email' => 'Email',
            'landline' => 'Landline',
            'mobile' => 'Mobile',
            'status' => 'Status',
            'created' => 'Created',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('create_user_id', $this->create_user_id);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('first_name', $this->first_name, true);
        $criteria->compare('sur_name', $this->sur_name, true);
        $criteria->compare('position', $this->position, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('landline', $this->landline);
        $criteria->compare('mobile', $this->mobile);
        $criteria->compare('status', $this->status);
        $criteria->compare('created', $this->created, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return LeadContacts the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public static function getYesOrNo() {
        $list[0] = 'No';
        $list[1] = 'Yes';

        return $list;
    }

    /**
     * 
     * @param type $id
     * @return string
     */
    public static function getCommodityType($id = null) {
        $arr = array('1' => "Electric", 2 => 'Gas');
        if ($id)
            return $arr[$id];
        return $arr;
    }

    public static function getTermLength($id = null) {
        $arr = array('12' => "12", '24' => '24', '36' => '36', '48' => '48', '60' => '60');
        if ($id)
            return $arr[$id];
        return $arr;
    }

//      public static function getCorporateSMEE($id = null) {
//        $arr = array('' => 'Select', '1' => 'SME', '2' => 'Corporate');
//        if ($id != null)
//            return $arr[$id];
//        return $arr;
//    }

    public function getBusinessName() {
        $business = LeadBusiness::model()->findByAttributes(array('lead_id' => $this->id));
        if ($business)
            return $business->business_name;
        return "N/A";
    }

    /**
     * 
     * @return string
     */
    public static function getAgency() {
        $users = Users::model()->findAllByAttributes(array('role_id' => 3));
        $html = '';
        $html .= '<select id="LeadContacts_agency_id_" class="form-control" name="LeadContacts[agency_id]">';
        foreach ($users as $user) {
            $html .= '<option value="' . $user->id . '">' . $user->username . '</option>';
        }
        $html .= '</select>';
        return $html;
    }

    /**
     * 
     * @return string
     */
    public static function getAgent() {
        $users = Users::model()->findAllByAttributes(array('role_id' => 4));
        $html = '';
        $html .= '<select id="LeadContacts_agent_id_" class="form-control" name="LeadContacts[agent_id]">';
        foreach ($users as $user) {
            $html .= '<option value="' . $user->id . '">' . $user->username . '</option>';
        }
        $html .= '</select>';
        return $html;
    }

    public static function getStatusDropdown() {
        $users = LeadStatus::model()->findAll();
        $html = '';
        $html .= '<select id="LeadContacts_status_" class="form-control" name="LeadContacts[status]">';
        foreach ($users as $user) {
            $html .= '<option value="' . $user->id . '">' . $user->title . '</option>';
        }
        $html .= '</select>';
        return $html;
    }

    public static function getTotalConsumtion($lead_id) {
        $sites = LeadSite::model()->findAllByAttributes(array('lead_id' => $lead_id));
        $sum = 0;
        if ($sites)
            foreach ($sites as $site) {
                $gases = LeadGasMeter::model()->findAllByAttributes(array('site_id' => $site->id));
                if ($gases)
                    foreach ($gases as $gas) {
                        $sum = $sum + $gas->total_aq;
                    }

                $elecs = LeadGasMeter::model()->findAllByAttributes(array('site_id' => $site->id));
                if ($elecs)
                    foreach ($elecs as $elec) {
                        $sum = $sum + $elec->total_aq;
                    }
            }
        return $sum;
    }

}
