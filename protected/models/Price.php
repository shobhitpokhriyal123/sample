<?php

/**
 * This is the model class for table "tbl_price".
 *
 * The followings are the available columns in table 'tbl_price':
 * @property integer $id
 * @property integer $product_id
 * @property string $product_type
 * @property string $dist_id
 * @property string $region
 * @property string $meter_type
 * @property string $profile
 * @property string $product
 * @property string $standing_charge
 * @property string $day_all_other_day_unit
 * @property string $night_unit_price
 * @property string $weekend_winter_peek
 * @property string $min_aq
 * @property string $max_aq
 * @property string $e_on_id
 * @property string $mtc
 * @property string $llf
 * @property string $contract_length
 * @property string $stand_charge_period
 * @property string $capacity_charge
 * @property string $md_charge
 * @property string $secondary_price
 * @property string $threshold
 * @property string $daily_service_charge
 * @property string $tariff
 */
class Price extends GxActiveRecord {

    const TYPE_GAS = 1;
    const TYPE_ELEC = 2;

    public $val_from;
    public $val_till;
    public $val_end;
    public $acc_start;
    public $database_attr = array();
    public $duration;
    public $cod = 'Y';
    public $smart_mtr = 'Y';
    public $assigned_to;
    public $commission;
    public $anual_cost;
    public $aq;
    public $postcode;
    public $distributer;
    public $region1;
    public $region2;
    public $region3;
    public $region4;
    public $region5;
    public $rates_and_charges;
    public $meter_type1;
    public $meter_type2;
    public $meter_type3;
    public $skip_supplier_aq = array();

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tbl_price';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        $priceMap = PriceMapping::model()->findAll();
        $pri = array();
        foreach ($priceMap as $price) {
            $pri[] = $price->attribute_slug;
        }
        $pri = implode(",", $pri);
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('product_id', 'required'),
            array('product_id', 'numerical', 'integerOnly' => true),
            array('standing_charge_period, fit, start_from_date, end_till_date, start_date,payment_type, end_date, contract_review_date,matrix_id,contract,' . $pri . ' , min_band, max_band,contract_review_date, sap_product,profile_class, credit_score_availablity, reactive_charge,winter_peak, 	invoice_value, retention_value , annual_consumption, unit_rate_desc,	unit_rate_price, unit_rate2_price,region_id,	unit_rate2_desc, product_id, product_type, dist_id,product_type, dist_id, region, meter_type, profile, product, standing_charge, day_all_other_day_unit, night_unit_price, weekend_winter_peek, min_aq, max_aq, e_on_id, mtc, llf, contract_length, stand_charge_period, capacity_charge, md_charge, secondary_price, threshold, daily_service_charge, tariff', 'length', 'max' => 255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id,standing_charge_period, fit , start_from_date, end_till_date, evening,day,night,day_night,region1,region4,region5,payment_type, start_date, end_date, contract_review_date,matrix_id,contract,' . $pri . ' , min_band, max_band, contract_review_date, sap_product,profile_class, credit_score_availablity, reactive_charge,winter_peak, 	invoice_value, retention_value , annual_consumption, unit_rate_desc,	unit_rate_price, unit_rate2_price,region_id,	unit_rate2_desc, product_id, product_type, dist_id, ' . $pri . ', region, meter_type, profile, product, standing_charge, day_all_other_day_unit, night_unit_price,winter_week_day, weekend_winter_peek, min_aq, max_aq, e_on_id, mtc, llf, contract_length, stand_charge_period, capacity_charge, md_charge, secondary_price, threshold, daily_service_charge, max_end_time,tariff', 'safe', 'on' => 'search'),
            array('id,standing_charge_period, fit , start_from_date, end_till_date, evening,region1,region4,region5,payment_type, start_date, end_date, contract_review_date,matrix_id,contract,' . $pri . ' , min_band, max_band, contract_review_date, sap_product,profile_class, credit_score_availablity, reactive_charge,winter_peak, 	invoice_value, retention_value , annual_consumption, unit_rate_desc,	unit_rate_price, unit_rate2_price,region_id,	unit_rate2_desc, product_id, product_type, dist_id, ' . $pri . ', region, meter_type, profile, product, standing_charge, day_all_other_day_unit, night_unit_price,winter_week_day, weekend_winter_peek, min_aq, max_aq, e_on_id, mtc, llf, contract_length, stand_charge_period, capacity_charge, md_charge, secondary_price, threshold, daily_service_charge, tariff,max_end_time', 'safe', 'on' => 'searchByMonth'),
            array('id,standing_charge_period, fit ,start_from_date,  end_till_date, evening,day,night,day_night,region1,region3,region2,region4,region5,payment_type, start_date, end_date, contract_review_date,matrix_id,contract,' . $pri . ' , min_band, max_band, contract_review_date, sap_product,profile_class, credit_score_availablity, reactive_charge,winter_peak, 	invoice_value, retention_value , annual_consumption, unit_rate_desc,	unit_rate_price, unit_rate2_price,region_id,	unit_rate2_desc, product_id, product_type, dist_id, ' . $pri . ', region, meter_type, profile, product, standing_charge, day_all_other_day_unit, night_unit_price,winter_week_day, weekend_winter_peek, min_aq, max_aq, e_on_id, mtc, llf, contract_length, stand_charge_period, capacity_charge, md_charge, secondary_price, threshold, daily_service_charge, tariff,max_end_time', 'safe', 'on' => 'searchEByMonth'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'productName' => array(self::BELONGS_TO, 'Product', 'product_id')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'product_id' => 'Product',
            'product_type' => 'Product Type',
            'dist_id' => 'Dist',
            'region' => 'Region',
            'meter_type' => 'Meter Type',
            'profile' => 'Profile',
            'product' => 'Product',
            'payment_type' => 'Payment Method',
            'standing_charge' => 'Standing Charge',
            'day_all_other_day_unit' => 'Day All Other Day Unit',
            'night_unit_price' => 'Night Unit Price',
            'weekend_winter_peek' => 'Weekend Winter Peek',
            'min_aq' => 'Min Aq',
            'max_aq' => 'Max Aq',
            'e_on_id' => 'E On',
            'mtc' => 'Mtc',
            'llf' => 'Llf',
            'contract_length' => 'Contract Length',
            'stand_charge_period' => 'Stand Charge Period',
            'capacity_charge' => 'Capacity Charge',
            'md_charge' => 'Md Charge',
            'secondary_price' => 'Secondary Price',
            'threshold' => 'Threshold',
            'daily_service_charge' => 'Daily Service Charge',
            'tariff' => 'Tariff',
            'line_description' => 'Line Description',
            'standing_charge_period' => 'Standing Charge Period',
            'unit_rate2_desc' => 'Unit Rate2 Desc',
            'unit_rate2_price' => 'Unit Rate2 Price',
            'unit_rate_price' => 'Unit Rate Price',
            'unit_rate_desc' => 'Unit Rate Desc',
            'annual_consumption' => 'Annual Consumption',
            'retention_value' => 'Retention Value',
            'reactive_charge' => 'Reactive Charge',
            'invoice_value' => 'Invoice Value',
            'winter_peak' => 'Winter Peak',
            'credit_score_availablity' => 'Credit Score Availablity',
            'sap_product' => 'SAP Product',
            'contract_review_date' => 'Contract Review Date',
            'profile_class' => 'Profile Class',
            'matrix_id' => 'Matrix ID',
            'contract_length' => 'Contract Length',
            'winter_week_day' => 'Winter Week Days',
            'contract' => 'Contract',
            'region_id' => 'Region Id',
            'price_book_type' => 'Sets',
            'contract_review_date' => 'contract_review_date',
            'start_date' => 'start_date',
            'end_date' => 'end_date',
            'fit' => 'FiTs'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search($id = null) {
        // @todo Please modify the following code to remove attributes that should not be searched.
        // echo 'fsfs'.$id; die;
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        if ($id) {
            $criteria->compare('product_id', $id);
        } else {
            $criteria->compare('product_id', $this->product_id);
        }
        $criteria->compare('product_type', $this->product_type, true);
        $criteria->compare('dist_id', $this->dist_id, true);
        $criteria->compare('region', $this->region, true);
        $criteria->compare('meter_type', $this->meter_type, true);
        $criteria->compare('profile', $this->profile, true);
        $criteria->compare('product', $this->product, true);
        $criteria->compare('standing_charge', $this->standing_charge, true);
        $criteria->compare('day_all_other_day_unit', $this->day_all_other_day_unit, true);
        $criteria->compare('night_unit_price', $this->night_unit_price, true);
        $criteria->compare('weekend_winter_peek', $this->weekend_winter_peek, true);
        $criteria->compare('min_aq', $this->min_aq, true);
        $criteria->compare('max_aq', $this->max_aq, true);
        $criteria->compare('e_on_id', $this->e_on_id, true);
        $criteria->compare('mtc', $this->mtc, true);
        $criteria->compare('llf', $this->llf, true);
        $criteria->compare('contract_length', $this->contract_length, true);
        $criteria->compare('stand_charge_period', $this->stand_charge_period, true);
        $criteria->compare('capacity_charge', $this->capacity_charge, true);
        $criteria->compare('md_charge', $this->md_charge, true);
        $criteria->compare('secondary_price', $this->secondary_price, true);
        $criteria->compare('threshold', $this->threshold, true);
        $criteria->compare('daily_service_charge', $this->daily_service_charge, true);
        $criteria->compare('tariff', $this->tariff, true);
        $criteria->compare('invoice_value', $this->invoice_value, true);
        $criteria->compare('winter_peak', $this->winter_peak, true);
        $criteria->compare('reactive_charge', $this->reactive_charge, true);
        $criteria->compare('retention_value', $this->retention_value, true);
        $criteria->compare('annual_consumption', $this->annual_consumption, true);
        $criteria->compare('unit_rate_desc', $this->unit_rate_desc, true);
        $criteria->compare('unit_rate_price', $this->unit_rate_price, true);
        $criteria->compare('unit_rate2_price', $this->unit_rate2_price, true);
        $criteria->compare('unit_rate2_desc', $this->unit_rate2_desc, true);
        $criteria->compare('standing_charge_period', $this->standing_charge_period, true);
        $criteria->compare('credit_score_availablity', $this->credit_score_availablity, true);
        $criteria->compare('sap_product', $this->sap_product, true);
        $criteria->compare('contract_review_date', $this->contract_review_date, true);
        $criteria->compare('profile_class', $this->profile_class, true);
        $criteria->compare('matrix_id', $this->matrix_id, true);
        $criteria->compare('contract_length', $this->contract_length, true);
        $criteria->compare('winter_week_day', $this->winter_week_day, true);
        $criteria->compare('contract', $this->contract, true);
        $criteria->compare('region_id', $this->region_id, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function searchGas($user_id = null) {
        $criteria = new CDbCriteria;
        $criteria->with = array('productName');
        $criteria->addNotInCondition('productName.utility', Product::ELEC_UTILITY);
        $criteria->compare('id', $this->id);
        if ($user_id) {
            $criteria->addCondition('productName.assigned_to =' . $user_id);
        }
        $criteria->compare('mtc', $this->mtc, true);
        $criteria->compare('llf', $this->llf, true);
        $criteria->compare('profile', $this->profile, true);
        // $criteria->compare('price_book_type', $this->price_book_type, true);
        $criteria->compare('product', $this->product, true);
        $criteria->compare('region', $this->region, true);
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
                //'pids'=>$pids
        ));
    }

    public function searchElec($user_id = null) {
        $criteria = new CDbCriteria;
        $criteria->with = array('productName');
        $criteria->addInCondition('productName.utility', Product::ELEC_UTILITY);
        if ($user_id) {
            $criteria->addCondition('productName.assigned_to =' . $user_id);
        }
        $criteria->compare('id', $this->id);
        $criteria->compare('mtc', $this->mtc, true);
        $criteria->compare('llf', $this->llf, true);
        $criteria->compare('profile', $this->profile, true);
        $criteria->compare('product', $this->product, true);
        $criteria->compare('region', $this->region, true);
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
                //'pids'=>$pids
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Price the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public static function getMainAttributes() {
        return array(
            'Region', 'Standing Charge', 'Contract Length', 'Profile', 'Product', 'Contract Length', 'Mtc',
        );
    }

    public static function getAllAttributes() {
        $priceMap = PriceMapping::model()->findAll();
        $map = array();
        if ($priceMap) {
            foreach ($priceMap as $price) {
                $map[$price->attribute_slug] = $price->attribute_name;
            }
        }

        $arr = array(
            'region_id' => 'Region Id',
            'product_type' => 'Product Type',
            'dist_id' => 'Dist',
            'region' => 'Region',
            'meter_type' => 'Meter Type',
            'profile' => 'Profile',
            'product' => 'Product',
            'standing_charge' => 'Standing Charge',
            'day_all_other_day_unit' => 'Day All Other Day Unit',
            'night_unit_price' => 'Night Unit Price',
            'weekend_winter_peek' => 'Weekend Winter Peek',
            'min_aq' => 'Min Aq',
            'max_aq' => 'Max Aq',
            'e_on_id' => 'E On ID',
            'mtc' => 'Mtc',
            'llf' => 'Llf',
            'contract_length' => 'Contract Length',
            'stand_charge_period' => 'Stand Charge Period',
            'capacity_charge' => 'Capacity Charge',
            'md_charge' => 'Md Charge',
            'secondary_price' => 'Secondary Price',
            'threshold' => 'Threshold',
            'daily_service_charge' => 'Daily Service Charge',
            'tariff' => 'Tariff',
            'standing_charge_period' => 'Standing Charge Period',
            'unit_rate2_desc' => 'Unit Rate2 Desc',
            'unit_rate2_price' => 'Unit Rate2 Price',
            'unit_rate_price' => 'Unit Rate Price',
            'unit_rate_desc' => 'Unit Rate Desc',
            'annual_consumption' => 'Annual Consumption',
            'retention_value' => 'Retention Value',
            'reactive_charge' => 'Reactive Charge',
            'invoice_value' => 'Invoice Value',
            'winter_peak' => 'Winter Peak',
            'credit_score_availablity' => 'Credit Score Availablity',
            'sap_product' => 'SAP Product',
            'contract_review_date' => 'Contract Review Date',
            'profile_class' => 'Profile Class',
            'matrix_id' => 'Matrix ID',
            'contract_length' => 'Contract Length',
            'winter_week_day' => 'Winter Week Days',
            'contract' => 'Contract',
            'day_night' => 'Day/Night',
            'fit' => 'FiTs'
        );

        return array_merge($arr, $map);
    }

    /**
     * 
     * @param type $field
     * @param type $product
     * @return boolean
     */
    public static function saveCsv($fields, $model) {
        $csv = $model->readCsvContents();
        $to_remove = array("0");
        $result = array_diff_key($csv, array_flip($to_remove));
        //  $attrs = $fields;
        $attrs = unserialize($fields);

        foreach ($result as $k => $attr) {
            //  echo '<pre>';print_r($attr); print_r($attrs); die;
            $final = array_combine($attrs, $attr);
            $price = new Price;
            $price->setAttributes($final);
            $price->product_id = $model->id;
            $price->save();
        }
        return true;
    }

    /**
     * 
     * @param type $month
     * @return \CActiveDataProvider
     */
    public function searchByMonth($month = null, $user = null) {
        $criteria = new CDbCriteria;
        $this->region = trim($this->region);
        $regions = '';
        // echo 'asdgfasdf '. $this->region; die(' dfgdsafg');
        $mapping = DistributerMap::model()->findByAttributes(array('ldz' => $this->region));

        if ($mapping && $mapping->region) {
            $r = trim($mapping->region);
            $regions .= '"' . $r . '"' . ",";
        }
        if ($mapping && $mapping->dist_id) {
            $r = trim($mapping->dist_id);
            $regions .= '"' . $r . '"' . ",";
        }
        if ($mapping && $mapping->region7) {
            $r = trim($mapping->region7);
            $regions .= '"' . $r . '"' . ",";
        }
        if ($mapping && $mapping->region6) {
            $r = trim($mapping->region6);
            $regions .= '"' . $r . '"' . ",";
        }
        if ($mapping && $mapping->region5) {
            $r = trim($mapping->region5);
            $regions .= '"' . $r . '"' . ",";
        }
        if ($mapping && $mapping->region4) {
            $r = trim($mapping->region4);
            $regions .= '"' . $r . '"' . ",";
        }
        if ($mapping && $mapping->region3) {
            $r = trim($mapping->region3);
            $regions .= '"' . $r . '"' . ",";
        }
        if ($mapping && $mapping->region2) {
            $r = trim($mapping->region2);
            $regions .= '"' . $r . '"' . ",";
        }
        if ($mapping && $mapping->region1) {
            $r = trim($mapping->region1);
            $regions .= '"' . $r . '"' . ",";
        }
        if ($mapping && $mapping->ldz) {
            $r = trim($mapping->ldz);
            $regions .= '"' . $r . '"' . ",";
        }
        if ($this->retention_value) {
            $r = trim($this->retention_value);
            $regions .= '"' . $r . '"';
        }
        $aq = $_REQUEST['Price']['max_aq']; //$this->aq; 

        $criteria->select = " t.*, ((($aq * t.unit_charge ) / 100) + (t.standing_charge * 365) / 100) as acost , productName.* ";
        //"12*mrc+otc as aoi";
        $m = date("m", strtotime($this->start_date));

        $start_date_ = date('Y-m-d', strtotime($this->start_date));

        switch ($month) {
            case 12:
                $real_date = date('Y-m-d', strtotime('+364 days', strtotime($this->start_date)));
                $criteria->condition = ' (case when (region != "") then region IN (' . $regions . ')  else 1 end) &&  (  (  (start_date = "") OR  (((start_from_date = "" ) AND (end_till_date = "" ) ) OR    ( DATE(start_from_date) <= DATE("' . trim($start_date_) . '") AND DATE(end_till_date) >= DATE("' . trim($start_date_) . '")  ) ) )  &&     (max_aq >= ' . $this->max_aq . ' && min_aq <= ' . $this->max_aq . ' ) ||  ( max_band >= ' . $this->max_aq . ' && min_band <= ' . $this->max_aq . ' )  || ( min_aq =0 && max_aq =0 )  )   &&  ((contract_length="12") OR (contract_length LIKE "One year%") OR (contract_length="12 months")  OR (contract_length LIKE "12 month%")   OR (contract_length="1")  OR (contract_length LIKE "1 year%")   ||  (term="12") OR (term LIKE "One year%") OR (term="12 months")  OR (term LIKE "12 month%")   OR (term="1")  OR (term LIKE "1 year%")   OR (contract_length = "16")  OR (term = "16")  AND ( (start_date = "")  OR    ( find_in_set("' . $m . '" ,MONTH(DATE(start_date)) ) )         )  AND ( (start_date = "") OR ( proposed_end_date = "" OR DATE(proposed_end_date) <  DATE(' . $real_date . ') )  )) ';
                break;
            case 24:
                $real_date = date('Y-m-d', strtotime('+729 days', strtotime($this->start_date)));
                $criteria->condition = ' (case when (region != "") then region IN (' . $regions . ')  else 1 end) &&   ( (  (start_date = "") OR   (((start_from_date = "" ) AND (end_till_date = "" ) ) OR    ( DATE(start_from_date) <= DATE("' . trim($start_date_) . '") AND DATE(end_till_date) >= DATE("' . trim($start_date_) . '")  )))   &&     (max_aq >= ' . $this->max_aq . ' && min_aq <= ' . $this->max_aq . ' ) ||  ( max_band >= ' . $this->max_aq . ' && min_band <= ' . $this->max_aq . ' )  || ( min_aq =0 && max_aq =0 )  )  &&   ((contract_length="24") OR (contract_length LIKE "Two year%") OR (contract_length="24 months")  OR (contract_length LIKE "24 month%")   OR (contract_length="2")  OR (contract_length LIKE "2 year%")   ||  (term="24") OR (term LIKE "Two year%") OR (term="24 months")  OR (term LIKE "24 month%")   OR (term="2")  OR (term LIKE "2 year%")   OR (contract_length = "30")  OR (term = "30")   OR (contract_length = "25")  OR (term = "25")   AND ( (start_date = "")  OR    ( find_in_set("' . $m . '" ,MONTH(DATE(start_date)) ) )         )  AND ( (start_date = "") OR ( proposed_end_date = "" OR DATE(proposed_end_date) <  DATE(' . $real_date . ') )  )) ';
                break;
            case 36:
                $real_date = date('Y-m-d', strtotime('+1094 days', strtotime($this->start_date)));
                $criteria->condition = ' (case when (region != "") then region IN (' . $regions . ')  else 1 end) &&    ( ( (start_date = "") OR  ( ((start_from_date = "" ) AND (end_till_date = "" ) ) OR    ( DATE(start_from_date) <= DATE("' . trim($start_date_) . '") AND DATE(end_till_date) >= DATE("' . trim($start_date_) . '")  ) ))  &&     (max_aq >= ' . $this->max_aq . ' && min_aq <= ' . $this->max_aq . ' ) ||  ( max_band >= ' . $this->max_aq . ' && min_band <= ' . $this->max_aq . ' )  || ( min_aq =0 && max_aq =0 )  )  &&     ((contract_length="36") OR (contract_length LIKE "three year%") OR (contract_length="36 months")  OR (contract_length LIKE "36 month%")   OR (contract_length="3")  OR (contract_length="3 years")  OR (contract_length LIKE "3 year%")  ||  (term="36") OR (term LIKE "Three year%") OR (term="36 months")  OR (term LIKE "36 month%")   OR (term="3")  OR (term LIKE "3 year%")   OR (contract_length = "39")  OR (term = "39")   AND (    (start_date = "")  OR   ( find_in_set("' . $m . '" ,MONTH(DATE(start_date)) ) )    )  AND ( (start_date = "") OR ( proposed_end_date = "" OR DATE(proposed_end_date) <  DATE(' . $real_date . ') )  ) ) ';
                break;
            case 48:
                $real_date = date('Y-m-d', strtotime('+1459 days', strtotime($this->start_date)));
                $criteria->condition = ' (case when (region != "") then region IN (' . $regions . ')  else 1 end) &&   (  (  (start_date = "") OR ( ((start_from_date = "" ) AND (end_till_date = "" ) ) OR    ( DATE(start_from_date) <= DATE("' . trim($start_date_) . '") AND DATE(end_till_date) >= DATE("' . trim($start_date_) . '")  )))   &&     (max_aq >= ' . $this->max_aq . ' && min_aq <= ' . $this->max_aq . ' ) ||  ( max_band >= ' . $this->max_aq . ' && min_band <= ' . $this->max_aq . ' )  || ( min_aq =0 && max_aq =0 )  )  &&     ((contract_length="48") OR (contract_length LIKE "Four year%") OR (contract_length="48 months")  OR (contract_length LIKE "48 month%")   OR (contract_length="4")  OR (contract_length LIKE "4 year%")   ||  (term="48") OR (term LIKE "four year%") OR (term="48 months")  OR (term LIKE "48 month%")   OR (term="4")  OR (term LIKE "4 year%")   OR (contract_length = "54")  OR (term = "54")  AND ( (start_date = "")  OR    ( find_in_set("' . $m . '" ,MONTH(DATE(start_date)) ) )         )  AND ( (start_date = "") OR ( proposed_end_date = "" OR DATE(proposed_end_date) <  DATE(' . $real_date . ') )  )) ';
                break;
            case 60:
                $real_date = date('Y-m-d', strtotime('+1824 days', strtotime($this->start_date)));
                $criteria->condition = ' (case when (region != "") then region IN (' . $regions . ')  else 1 end) &&      ( ( (start_date = "") OR ( ((start_from_date = "" ) AND (end_till_date = "" ) ) OR    ( DATE(start_from_date) <= DATE("' . trim($start_date_) . '") AND DATE(end_till_date) >= DATE("' . trim($start_date_) . '")  )  )) &&   (max_aq >= ' . $this->max_aq . ' && min_aq <= ' . $this->max_aq . ' ) ||  ( max_band >= ' . $this->max_aq . ' && min_band <= ' . $this->max_aq . ' )  || ( min_aq =0 && max_aq =0 )  )  &&  ((contract_length="60")   OR (contract_length LIKE "five year%") OR (contract_length="60 months")  OR (contract_length LIKE "60 month%")   OR (contract_length="5")  OR (contract_length="5 years")  OR (contract_length LIKE "5 year%")   ||  (term="60") OR (term LIKE "Five year%") OR (term="60 months")  OR (term LIKE "60 month%")   OR (term="5")  OR (term LIKE "5 year%")   AND (   (start_date = "")  OR   ( find_in_set("' . $m . '" ,MONTH(DATE(start_date)) ) )  )  AND ( (start_date = "") OR ( proposed_end_date = "" OR DATE(proposed_end_date) <  DATE(' . $real_date . ') )  ) ) ';
                break;
            default:
                $criteria->condition = ' (case when (region != "") then region IN (' . $regions . ')  else 1 end) &&   ( ( (start_date = "") OR ( ((start_from_date = "" ) AND (end_till_date = "" ) ) OR    ( DATE(start_from_date) <= DATE("' . trim($start_date_) . '") AND DATE(end_till_date) >= DATE("' . trim($start_date_) . '")  ) ))  &&   (max_aq >= ' . $this->max_aq . ' && min_aq <= ' . $this->max_aq . ' ) ||  ( max_band >= ' . $this->max_aq . ' && min_band <= ' . $this->max_aq . ' )  || ( min_aq =0 && max_aq =0 )  ) ';
               break;
        }
        $criteria->with = array('productName');
        $finaldate = date('Y-m-d', strtotime($this->start_date));
        $criteria->addCondition('DATE(productName.final_date) > "' . $finaldate . '" ');
        $criteria->addCondition('productName.status = 1');
        $criteria->addNotInCondition('productName.utility', Product::ELEC_UTILITY);
        $criteria->compare('id', $this->id);
        $criteria->compare('mtc', $this->mtc, true);
        $criteria->compare('llf', $this->llf, true);
        $criteria->compare('standing_charge', $this->standing_charge, true);
        $criteria->compare('unit_charge', $this->unit_charge, true);
        $criteria->compare('product', $this->product, true);
        $criteria->order = "acost ASC";
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * 
     * @param type $month
     * @return \CActiveDataProvider
     */
    public function searchEByMonth($month = null) {

        $criteria = new CDbCriteria;
        $this->region = rtrim($this->region);

        $this->profile = isset($_GET['Price']['profile']) ? $_GET['Price']['profile'] : "";
        $metera = array();

        if ($this->meter_type) {
            $meters = MeterType::model()->findAllByAttributes(array('title' => $this->meter_type));
            foreach ($meters as $meter) {
                $metera[] = $meter->meter_code;
            }
        }
        $aq = $_REQUEST['Price']['max_aq'];
        switch ($this->meter_type) {
            case 1:
                $criteria->select = " t.* , ((($aq * t.unit_charge ) / 100) + (t.standing_charge * 365) / 100) as acost , productName.* ";
                break;
            case 2:
                $basemeter = ElecMeter::model()->findByAttributes(array('mtype_id' => 2));
                if ($basemeter) {
                    $criteria->select = " t.* , ((( ($aq * $basemeter->days) /100 ) * t.unit_charge)/100 + ( ( ($aq*$basemeter->night)/100 ) * t.night )/100  + (t.standing_charge * 365) / 100) as acost , productName.* ";
                } else {
                    $criteria->select = " t.* , ((($aq * t.unit_charge ) / 100) + (t.standing_charge * 365) / 100) as acost , productName.* ";
                }
                break;
            case 3:
                $basemeter = ElecMeter::model()->findByAttributes(array('mtype_id' => 2));
                if ($basemeter) {
                    $criteria->select = " t.* , ((($aq * t.unit_charge ) / 100) + (($aq * t.night ) / 100)  + (t.standing_charge * 365) / 100) as acost , productName.* ";
                } else {
                    $criteria->select = " t.* , ((($aq * t.unit_charge ) / 100) + (t.standing_charge * 365) / 100) as acost , productName.* ";
                }
                break;
            case 4:
                $basemeter = ElecMeter::model()->findByAttributes(array('mtype_id' => 2));
                if ($basemeter) {
                    $criteria->select = " t.* , ((($aq * t.unit_charge ) / 100) + (($aq * t.night ) / 100)  + (t.standing_charge * 365) / 100) as acost , productName.* ";
                } else {
                    $criteria->select = " t.* , ((($aq * t.unit_charge ) / 100) + (t.standing_charge * 365) / 100) as acost , productName.* ";
                }
                break;
            case 5:
                $basemeter = ElecMeter::model()->findByAttributes(array('mtype_id' => 2));
                if ($basemeter) {
                    $criteria->select = " t.* , ((($aq * t.unit_charge ) / 100) + (($aq * t.night ) / 100)  + (t.standing_charge * 365) / 100) as acost , productName.* ";
                } else {
                    $criteria->select = " t.* , ((($aq * t.unit_charge ) / 100) + (t.standing_charge * 365) / 100) as acost , productName.* ";
                }
                break;
            default :
                $criteria->select = " t.* , ((($aq * t.unit_charge ) / 100) + (t.standing_charge * 365) / 100) as acost , productName.* ";
                break;
        }
        $m = date("m", strtotime($this->start_date));
        // $y =   date("Y", strtotime($this->start_date));
        $start_date_ = date('Y-m-d', strtotime($this->start_date));
        switch ($month) {
            case 12:
                $real_date = date('Y-m-d', strtotime('+364 days', strtotime($this->start_date)));
                $criteria->condition = ' ( (start_date = "")  OR ( DATE(start_from_date) <= DATE("' . trim($start_date_) . '") AND DATE(end_till_date) >= DATE("' . trim($start_date_) . '")  ) )   AND (case when (region != "") then region IN (' . $this->region . ')  else 1 end) && (      (max_aq >= ' . $this->max_aq . ' && min_aq <= ' . $this->max_aq . ' ) ||  ( max_band >= ' . $this->max_aq . ' && min_band <= ' . $this->max_aq . ' ) || ( min_aq =0 && max_aq =0 ) )  &&  ((contract_length="12") OR (contract_length LIKE "One year%") OR (contract_length="12 months")  OR (contract_length LIKE "12 month%")   OR (contract_length="1")  OR (contract_length LIKE "1 year%")  ||  (contract_length LIKE "16") || (term="16") ||  (contract_length LIKE "13") || (term="13") ||  (term="12") OR (term LIKE "One year%") OR (term="12 months")  OR (term LIKE "12 month%") OR (contract_length=" 1 Yr")  OR (term=" 1 Yr")  OR (term="1")  OR (term LIKE "1 year%")    ) AND (   (start_date = "") OR ( proposed_end_date = "" OR DATE(proposed_end_date) <  DATE(' . $real_date . ') )  ) && (  ( profile_class = ' . $this->profile . ') || (profile = ' . $this->profile . ') ) ';
                break;
            case 24:
                $real_date = date('Y-m-d', strtotime('+729 days', strtotime($this->start_date)));
                $criteria->condition = ' ( (start_date = "")  OR ( DATE(start_from_date) <= DATE("' . trim($start_date_) . '") AND DATE(end_till_date) >= DATE("' . trim($start_date_) . '")  ) )   AND  (case when (region != "") then region IN (' . $this->region . ')  else 1 end) &&  (    (max_aq >= ' . $this->max_aq . ' && min_aq <= ' . $this->max_aq . ' ) ||  ( max_band >= ' . $this->max_aq . ' && min_band <= ' . $this->max_aq . ' )  || ( min_aq =0 && max_aq =0 )   )  &&  ((contract_length="24") OR (contract_length LIKE "two year%") OR (contract_length="24 months")  OR (contract_length LIKE "24 month%")   OR (contract_length="2")  OR (contract_length="2 years")   OR  (contract_length LIKE "25") || (term="25") || (contract_length LIKE "2 year%")   ||  (term="24") OR (term LIKE "Two year%") OR (term="24 months")  OR (term LIKE "24 month%")   OR (term="2")  OR (term LIKE "2 year%")  OR (contract_length=" 2Yr")  OR (term=" 2Yr")  )   AND  (   (start_date = "") OR ( proposed_end_date = "" OR DATE(proposed_end_date) <  DATE(' . $real_date . ') )  )  && (  ( profile_class = ' . $this->profile . ') || (profile = ' . $this->profile . ')) ';
                break;
            case 36:
                $real_date = date('Y-m-d', strtotime('+1094 days', strtotime($this->start_date)));
                $criteria->condition = ' ( (start_date = "")  OR ( DATE(start_from_date) <= DATE("' . trim($start_date_) . '") AND DATE(end_till_date) >= DATE("' . trim($start_date_) . '")  ) )   AND (case when (region != "") then region IN (' . $this->region . ')  else 1 end) && (  (max_aq >= ' . $this->max_aq . ' && min_aq <= ' . $this->max_aq . ' ) ||  ( max_band >= ' . $this->max_aq . ' && min_band <= ' . $this->max_aq . ' )  || ( min_aq =0 && max_aq =0 )  )  &&  ((contract_length="36") OR (contract_length LIKE "three year%") OR (contract_length="36 months")  OR (contract_length LIKE "36 month%")   OR (contract_length="3")  OR (contract_length="3 years")  OR  (contract_length LIKE "3 year%")  ||  (term="36") OR (term LIKE "Three year%") OR (term="36 months")  OR (term LIKE "36 month%")   OR (term="3")  OR (term LIKE "3 year%") OR (contract_length=" 3Yr")  OR (term=" 3Yr") )   AND  (   (start_date = "") OR ( proposed_end_date = "" OR DATE(proposed_end_date) <  DATE(' . $real_date . ') )  )  && (  ( profile_class = ' . $this->profile . ') || ( profile = ' . $this->profile . ') ) ';
                break;
            case 48:
                $real_date = date('Y-m-d', strtotime('+1459 days', strtotime($this->start_date)));
                $criteria->condition = ' ( (start_date = "")  OR ( DATE(start_from_date) <= DATE("' . trim($start_date_) . '") AND DATE(end_till_date) >= DATE("' . trim($start_date_) . '")  ) )   AND (case when (region != "") then region IN (' . $this->region . ')  else 1 end) && (    (max_aq >= ' . $this->max_aq . ' && min_aq <= ' . $this->max_aq . ' ) ||  ( max_band >= ' . $this->max_aq . ' && min_band <= ' . $this->max_aq . ' )  || ( min_aq =0 && max_aq =0 )  )  &&   ((contract_length="48")  OR (contract_length LIKE "four year%") OR (contract_length="48 months")  OR (contract_length LIKE "48 month%")   OR (contract_length="4")  OR (contract_length="4 years")  OR (contract_length LIKE "4 year%")  ||  (term="48") OR (term LIKE "Four year%") OR (term="48 months")  OR (term LIKE "48 month%")   OR (term="4")  OR (term LIKE "4 year%") OR (contract_length=" 4 Yr")  OR (term=" 4 Yr") )     AND (  (start_date = "") OR ( proposed_end_date = "" OR DATE(proposed_end_date) <  DATE(' . $real_date . ') )  ) && (  ( profile_class = ' . $this->profile . ') || (profile = ' . $this->profile . '))  ';
                break;
            case 60:
                $real_date = date('Y-m-d', strtotime('+1824 days', strtotime($this->start_date)));
                $criteria->condition = '( (start_date = "")  OR ( DATE(start_from_date) <= DATE("' . trim($start_date_) . '") AND DATE(end_till_date) >= DATE("' . trim($start_date_) . '")  ) )   AND (case when (region != "") then region IN (' . $this->region . ')  else 1 end) &&  (   (max_aq >= ' . $this->max_aq . ' && min_aq <= ' . $this->max_aq . ' ) ||  ( max_band >= ' . $this->max_aq . ' && min_band <= ' . $this->max_aq . ' )  || ( min_aq =0 && max_aq =0 )  )  &&  ((contract_length="60")   OR (contract_length LIKE "five year%") OR (contract_length="60 months")  OR (contract_length LIKE "60 month%")   OR (contract_length="5")  OR (contract_length="5 years")  OR (contract_length LIKE "5 year%")   ||  (term="60") OR (term LIKE "Five year%") OR (term="60 months")  OR (term LIKE "60 month%")   OR (term="5")  OR (term LIKE "5 year%")  OR (contract_length=" 5 Yr")  OR (term=" 5 Yr"))   AND (  (start_date = "") OR ( proposed_end_date = "" OR DATE(proposed_end_date) <  DATE(' . $real_date . ') )  )  && (  ( profile_class = ' . $this->profile . ') || (profile = ' . $this->profile . ') ) ';
                break;
            default:
                $criteria->condition = ' ( (start_date = "")  OR ( DATE(start_from_date) <= DATE("' . trim($start_date_) . '") AND DATE(end_till_date) >= DATE("' . trim($start_date_) . '")  ) )   AND (case when (region != "") then region IN (' . $this->region . ')  else 1 end) && (   (max_aq >= ' . $this->max_aq . ' && min_aq <= ' . $this->max_aq . ' ) ||  ( max_band >= ' . $this->max_aq . ' && min_band <= ' . $this->max_aq . ' )  || ( min_aq =0 && max_aq =0 )  )    ';
                break;
        }



        if (!empty($metera))
            $criteria->addInCondition('meter_type', $metera);
        //$criteria->compare('profile_class', $this->profile_class, true);
        $criteria->with = array('productName');
        $criteria->addInCondition('productName.utility', Product::ELEC_UTILITY);
        $finaldate = date('Y-m-d', strtotime($this->start_date));
        $criteria->addCondition('DATE(productName.final_date) > "' . $finaldate . '" ');
        $criteria->addCondition('productName.status = 1');
        $criteria->compare('id', $this->id);
        $criteria->compare('mtc', $this->mtc, true);
        $criteria->compare('llf', $this->llf, true);
        $criteria->compare('standing_charge', $this->standing_charge, true);
        $criteria->compare('unit_charge', $this->unit_charge, true);
        $criteria->order = "acost ASC";
        //    $criteria->compare('region', "$this->region", true);
        // $criteria->compare('profile', $this->profile, true);

        $activedata = new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
        $this->saveHistory($activedata, $month);
        return $activedata;
    }

    public function saveHistory($activedata, $month) {
        $data = $activedata->getData();
        foreach ($data as $datas) {
            $model = new QuoteHistory;
            $model->create_user_id = Yii::app()->user->id;
            $model->assigned_to = $datas->productName->assigned_to;
            $model->price_id = $datas->id;
            $model->created = new CDbExpression("NOW()");
            $model->product = $datas->productName->product_name . ' - ' . $datas->product;
            $model->region = $datas->region;
            $model->payment_type = ($datas->productName->direct_dabit == 0 ) ? "Cash" : "Direct Dabit";
            $model->contract_length = ($datas->contract_length) ? $datas->contract_length : $datas->term;
            $model->start_date = $datas->start_date;
            $model->standing_charge = ($datas->standing_charge) ? $datas->standing_charge : "N/A";
            $model->unit_charge = ($datas->unit_charge) ? $datas->unit_charge : "N/A";
            $model->annual_cost = $datas->getElcAnualCost($month, 6, 1);
            if (!$model->save()) {
                echo '<pre>';
                print_r($model->getErrors());
            }
        }
    }

    /**
     * 
     * @return type
     */
    public static function getRegion($pcode, $type) {
        $product = new Product;
        $pcode_arr = $product->readCsvContents(NULL, 1);
        $res = self::searchCode($pcode_arr, 0, $pcode, 1);
        return $res;
    }

    public static function searchCode($array, $key, $value, $k) {
        $result = '';
        $value = rtrim($value);
        $arr = array();
        for ($i = strlen($value); $i > 0; $i--) {
            foreach ($array as $arr) {
                $char = substr($value, 0, $i);
                if (trim($arr[$key]) == trim($char)) {
                    return $arr;
                }
            }
        }
        return $arr;
    }

    /**
     * 
     * @return string
     */
    public function getCommissionType($month = null) {
        $commission = '';
        if (!Yii::app()->user->getIsAdmin()) {
            $agency = Agency::model()->findByAttributes(array('user_id' => Yii::app()->user->id));
            if ($agency)
                $commission = $agency->agent_split;
            else
                $commission = '0.00';
        } else {
            $commission = '100';
        }

        $html = '
  <div class="inputWrapper">
    <input  id="ct_' . $this->id . '_' . $month . '" type="text"  max=""  min="0.00"  class="txtInput" value="0.00">
    <div class="cusWrap">
      <button class="cusBTN"  onclick="getCalculation(' . $this->id . ' ,1, ' . $this->productName->commission_increament . ',' . $this->productName->max_commission . ',' . $month . ', ' . $commission . ')"  type="button">+</button>
      <button class="cusBTN"   onclick="getCalculation(' . $this->id . ' ,2,' . $this->productName->commission_increament . ',' . $this->productName->max_commission . ',' . $month . ', ' . $commission . ')"  type="button">-</button>
    </div>
  </div>
';
        return $html;
    }

    /**
     * 
     * @return string
     */
    public function getCommission($month) {

        $html = '<input id="comm_' . $this->id . '_' . $month . '" class="fBTN" readonly="true" type="text" placeholder="0.00" max="' . $this->productName->max_commission . '"  min="0.00"  step="' . $this->productName->commission_increament . '" value="0.00">';
        return $html;
    }

    /**
     * 
     * @return string
     */
    public function getStandingCharge($month) {
        if ($this->standing_charge) {
            $sc = $this->standing_charge;
        } else {
            $sc = '0.00';
        }
        $html = '<input  id="sc_' . $this->id . '_' . $month . '" type="text" readonly="true" value="' . $sc . '">';
        return $html;
    }

    /**
     * 
     * @return string
     */
    public function getUnitCharge($month) {
        $html = '';
        if ($this->unit_charge && $this->unit_charge != "-") {
            $sc = $this->unit_charge;
            if ($this->fit == 0) {
                $html .= '<input  id="uc_' . $this->id . '_' . $month . '" readonly="true" type="text" placeholder="0.00" value="' . $sc . '">';
            } else {
                $sc = ($sc + $this->fit);
                $html .= '<input  id="uc_' . $this->id . '_' . $month . '" readonly="true" type="text" placeholder="0.00" value="' . $sc . '"> <a href="#" data-toggle="tooltip" title="FiT included ' . $this->fit . '"><i class="icon-check"></i></a>';
            }
        }
        if ($this->day  && $this->day != "-") {
            if ($this->fit == 0) {
                $html .= '<input  id="uc_day_' . $this->id . '_' . $month . '" readonly="true" type="text" placeholder="0.00" value="' . $this->day . '">';
            } else {
                $this->day = ($this->day + $this->fit);
                $html .= '<input  id="uc_day_' . $this->id . '_' . $month . '" readonly="true" type="text" placeholder="0.00" value="' . $this->day . '">  <a href="#" data-toggle="tooltip" title="FiT included ' . $this->fit . '"><i class="icon-check"></i></a>';
            }
        }
        if ($this->night  && $this->night != "-") {
            if ($this->fit == 0) {
                $html .= '<input  id="uc_night_' . $this->id . '_' . $month . '" readonly="true" type="text" placeholder="0.00" value="' . $this->night . '"> ';
            } else {
                $this->night = ($this->night + $this->fit);
                $html .= '<input  id="uc_night_' . $this->id . '_' . $month . '" readonly="true" type="text" placeholder="0.00" value="' . $this->night . '">  <a href="#" data-toggle="tooltip" title="FiT included ' . $this->fit . '"><i class="icon-check"></i></a>';
            }
        }
        if ($this->evening  && $this->evening != "-" ) {
            if ($this->fit == 0 ) {
                $html .= '<input  id="uc_winter_' . $this->id . '_' . $month . '" readonly="true" type="text" placeholder="0.00" value="' . $this->evening . '">';
            } else {
                $this->evening = ($this->evening + $this->fit);
                $html .= '<input  id="uc_winter_' . $this->id . '_' . $month . '" readonly="true" type="text" placeholder="0.00" value="' . $this->evening . '">  <a href="#" data-toggle="tooltip" title="FiT included ' . $this->fit . '"><i class="icon-check"></i></a>';
            }
        }

        if ($this->day_night && $this->day_night != "-") {
            if ($this->fit == 0) {
                $html .= '<input  id="uc_weekend_' . $this->id . '_' . $month . '" readonly="true" type="text" placeholder="0.00" value="' . $this->day_night . '">';
            } else {
                $this->day_night = ($this->day_night + $this->fit);
                $html .= '<input  id="uc_weekend_' . $this->id . '_' . $month . '" readonly="true" type="text" placeholder="0.00" value="' . $this->day_night . '">  <a href="#" data-toggle="tooltip" title="FiT included ' . $this->fit . '"><i class="icon-check"></i></a>';
            }
        }

        return $html;
    }

    /**
     * 
     * @return string
     */
    public function getAnualCost($month) {
        //   $val = 
        //echo $this->max_aq;
        $aq = $_REQUEST['Price']['max_aq']; //$this->aq; 
        $uc = $this->unit_charge;
        $sc = $this->standing_charge;
        $value = (($aq * $uc) / 100) + ($sc * 365) / 100;
        $html = '<input  id="ac_' . $this->id . '_' . $month . '" readonly="true"  type="text" placeholder="0.00" value="' . $value . '">';
        return $html;
    }

    /**
     * 
     * @return string
     */
    public function getElcAnualCost($month, $meter, $history = null) {
        $aq = $_REQUEST['Price']['max_aq']; //$this->aq; 
        switch ($meter) {
            case 1:
                $uc = $this->unit_charge;
                $sc = $this->standing_charge;
                $value = (($aq * $uc) / 100) + ($sc * 365) / 100;
                break;
            case 2:
                $basemeter = ElecMeter::model()->findByAttributes(array('mtype_id' => 2));
                if ($basemeter) {
                    $value = ((( ($aq * $basemeter->days) / 100 ) * $this->unit_charge) / 100 + ( ( ($aq * $basemeter->night) / 100 ) * $this->night ) / 100 + ($this->standing_charge * 365) / 100);
                } else {
                    $value = (($aq * $uc) / 100) + ($sc * 365) / 100;
                }
                break;
            case 3:

                $basemeter = ElecMeter::model()->findByAttributes(array('mtype_id' => 3));
                if ($basemeter) {
                    $value = ((( ($aq * $basemeter->days) / 100 ) * $this->unit_charge) / 100 + ( ( ($aq * $basemeter->night) / 100 ) * $this->night ) / 100 + ($this->standing_charge * 365) / 100);
                } else {
                    $value = (($aq * $uc) / 100) + ($sc * 365) / 100;
                }
                break;
            case 4:
                $basemeter = ElecMeter::model()->findByAttributes(array('mtype_id' => 4));
                if ($basemeter) {
                    $value = ((( ($aq * $basemeter->days) / 100 ) * $this->unit_charge) / 100 + ( ( ($aq * $basemeter->night) / 100 ) * $this->night ) / 100 + ($this->standing_charge * 365) / 100);
                } else {
                    $value = (($aq * $uc) / 100) + ($sc * 365) / 100;
                }
                break;
            case 5:
                $basemeter = ElecMeter::model()->findByAttributes(array('mtype_id' => 5));
                if ($basemeter) {
                    $value = ((( ($aq * $basemeter->days) / 100 ) * $this->unit_charge) / 100 + ( ( ($aq * $basemeter->night) / 100 ) * $this->night ) / 100 + ($this->standing_charge * 365) / 100);
                } else {
                    $value = (($aq * $uc) / 100) + ($sc * 365) / 100;
                }
                break;
            default :
                $uc = $this->unit_charge;
                $sc = $this->standing_charge;
                $value = (($aq * $uc) / 100) + ($sc * 365) / 100;
                break;
        }
        if ($history)
            return $value;
        $html = '<input  id="ac_' . $this->id . '_' . $month . '" readonly="true"  type="text" placeholder="0.00" value="' . $value . '">';
        return $html;
    }

    /**
     * 
     * @return string
     */
//    public function getElectricAnualCost($month) {
//        //   $val = 
//        //echo $this->max_aq;
//        $aq = $_REQUEST['Price']['max_aq']; //$this->aq; 
//        $uc = $this->unit_charge;
//        $sc = $this->standing_charge;
//        $value = (($aq * $uc) / 100) + ($sc * 365) / 100;
//        $html = '<input  id="ac_' . $this->id . '_' . $month . '" readonly="true"  type="text" placeholder="0.00" value="' . $value . '">';
//        return $html;
//    }

    public function getRates() {
        echo '<div class="ratebox"><p><span class="tag">SC:</span> <span class="value">' . $this->standing_charge . ' p</span>  </p>'
        . '<p><span class="tag">Day:</span> <span class="value">' . $this->tpr_1_price . '</span>  </p>'
        . '<p><span class="tag">Night:</span> <span class="value">' . $this->tpr_2_price . '</span>  </p>'
        . '<p><span class="tag">Winter:</span> <span class="value">' . $this->tpr_3_price . '</span> </p>'
        . '<p><span class="tag">EW:</span> <span class="value">' . $this->tpr_4_price . '</span> </p></div>';
    }

    public static function getElcRegion($pcode, $distributer, $type) {
        $product = new Product;
        $pcode_arr = $product->readCsvContents(NULL, 1, 1);
        $res = array();
        foreach ($pcode_arr as $pcode) {
            if ($pcode[2] == $distributer)
                $res = $pcode;
        }
        //$res = self::searchCode($pcode_arr, 0, $pcode, 1);
        return $res;
    }

    /**
     * 
     * @return type
     */
    public static function getRegion2($pcode) {
        $product = new Product;
        $pcode_arr = $product->readCsvContents2($pcode);
        $pcode = rtrim($pcode);
        foreach ($pcode_arr as $pcode_arr) {
            if ($pcode_arr[3] == $pcode)
                return $pcode_arr;
        }
        //  echo '<pre>'; print_r($pcode_arr); die;
        return false;
    }

    protected function beforeDelete() {
        ContractDetail::model()->deleteAllByAttributes(array('price_id' => $this->id));
        Quote::model()->deleteAllByAttributes(array('price_id' => $this->id));
        return true;
//parent::beforeDelete();
    }

    public function getStartDate($date) {
        $start_date = $this->productName->min_start_time;
        if ($start_date) {
            $tomorrow = date("Y-M-d", time() + 86400);
            $abc = $finalDate = date("Y-M-d", strtotime($tomorrow . " +" . $start_date . "days"));
            $date1 = $date;
            $date = strtotime($date);
            $finalDate = strtotime($finalDate);
            if ($date > $finalDate) {
                $final = $date1;
            } else {
                $final = '<b>' . $abc . '</b>';
            }

            // 
            return $final; //$date = strtotime(date("Y-m-d", strtotime($quoteDate->start_date)) . " +" . $quoteDate->number_of . "$quoteDate->supplier_delays");
        } else
            return "N/A";
    }

}
