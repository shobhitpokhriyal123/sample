<?php

/**
 * This is the model class for table "tbl_ticket".
 *
 * The followings are the available columns in table 'tbl_ticket':
 * @property integer $id
 * @property integer $agent_id
 * @property integer $create_user_id
 * @property integer $corporate_sme
 * @property integer $utility
 * @property integer $ticket_type
 * @property integer $query_type
 * @property string $title
 * @property string $description
 * @property string $created
 */
class Ticket extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tbl_ticket';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('agent_id, create_user_id, corporate_sme, utility, ticket_type, query_type, title, description, created', 'required'),
            array('agent_id, create_user_id,status, corporate_sme, utility,is_replied, ticket_type, query_type', 'numerical', 'integerOnly' => true),
            array('title', 'length', 'max' => 255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, agent_id,status, create_user_id, corporate_sme, is_replied,utility, ticket_type, query_type, title, description, created', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            //'agency' => array(self::BELONGS_TO, 'Agency', 'agency_id'),
            'agent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
            'createUser' => array(self::BELONGS_TO, 'Users', 'create_user_id'),
            'reply' => array(self::HAS_ONE, 'Reply', 'ticket_id'),
            'type' => array(self::BELONGS_TO, 'TicketType', 'ticket_type'),
            'replies'=> array(self::HAS_MANY, 'Reply', 'ticket_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'agent_id' => 'Agent',
            'create_user_id' => 'Create User',
            'corporate_sme' => 'Corporate Sme',
            'utility' => 'Utility',
            'ticket_type' => 'Ticket Type',
            'query_type' => 'Query Type',
            'is_replied' => 'Is Replied',
            'title' => 'Title',
            'description' => 'Description',
            'created' => 'Created',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('agent_id', $this->agent_id);
        if (!Yii::app()->user->isAdmin)
            $criteria->compare('create_user_id', Yii::app()->user->id);
        else
            $criteria->compare('create_user_id', $this->create_user_id);
        $criteria->compare('corporate_sme', $this->corporate_sme);
        $criteria->compare('utility', $this->utility);
        $criteria->compare('ticket_type', $this->ticket_type);
        $criteria->compare('query_type', $this->query_type);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('created', $this->created, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Ticket the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    /**
     * 
     */
   

}
