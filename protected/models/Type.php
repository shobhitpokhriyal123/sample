<?php

/**
 * This is the model class for table "tbl_type".
 *
 * The followings are the available columns in table 'tbl_type':
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $logo
 * @property integer $status
 * @property integer $parrent
 * @property string $created
 */
class Type extends GxActiveRecord {

    const TYPE_GAS = 'gas';
    const TYPE_ELECTRICTY = 'electricty';

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tbl_type';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('title, created', 'required'),
            array('status, parrent', 'numerical', 'integerOnly' => true),
            array('title, logo', 'length', 'max' => 255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, title, description, logo, status, parrent, created', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'title' => 'Utility',
            'description' => 'Description',
            'logo' => 'Logo',
            'status' => 'Status',
            'parrent' => 'Parrent',
            'created' => 'Created',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('logo', $this->logo, true);
       // $criteria->compare('status',1);
        $criteria->condition = 'parrent != 0 && status =1';//compare('parrent !=', '0' );
        $criteria->compare('created', $this->created, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Type the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * 
     * @param type $id
     * @return string
     */
    public function getType($id = null) {
        $arr = array(self::TYPE_GAS => "GAS", self::TYPE_ELECTRICTY => 'Electricity');
        if ($id) {
            return $arr[$id];
        }
        return $arr;
    }

    /**
     * 
     * @param type $id
     * @return type
     */
    public static function getTypes($id = null) {
        $criteria = new CDbCriteria;
        if ($id)
            return self::model()->findByPk($id);
        return self::model()->findAllByAttributes(array('parrent' => 0));
    }

    public function getSubTypes($id = null) {
        
    }

    /**
     * 
     * @param type $id
     * @return string
     */
    public function getStatus($id = null) {
        $arr = array(1 => 'Active', 0 => 'Inactive');
        if ($id)
            return $arr[$id];
        return $arr;
    }
    
    public static function getUtility($id = null){
        if($id){
             return self::model()->findByPk($id)->title; 
        }else{
            return 'N/A';
        }
    }
    
    
      public static function getSME($id = null){
       $criteria = new CDbCriteria;
       $criteria->addCondition('status = 1  && parrent =1');
       return self::model()->findAll($criteria);
       
    }
    
    public static function getUtilityTitle($ids){
        $criteria = new CDbCriteria;
        $ids = explode(",", $ids);
        $criteria->addInCondition('id', $ids);
        $types = Type::model()->findAll($criteria);
        $a = '';
        foreach ($types as $type){
          $a .=  $type->title.' - '. self::getUtility($type->parrent). ', ';
        }
        return rtrim($a,', ');
// echo '<pre>'; print_r( die;
    }

}
