<?php

/**
 * This is the model class for table "tbl_lead_upload".
 *
 * The followings are the available columns in table 'tbl_lead_upload':
 * @property integer $id
 * @property integer $meter_id
 * @property integer $is_gas
 * @property string $document
 */
class LeadUpload extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tbl_lead_upload';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('meter_id', 'required'),
            array('meter_id, is_gas', 'numerical', 'integerOnly' => true),
            array('document', 'length', 'max' => 255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, meter_id, is_gas, document, document_gas', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
                // 'meter' => array(self::BELONGS_TO, 'LeadMeter', 'lead_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'meter_id' => 'Meter',
            'is_gas' => 'Is Gas',
            'document' => 'Document',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('meter_id', $this->meter_id);
        $criteria->compare('is_gas', $this->is_gas);
        $criteria->compare('document', $this->document, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return LeadUpload the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
