<?php

/**
 * This is the model class for table "tbl_lead_additional".
 *
 * The followings are the available columns in table 'tbl_lead_additional':
 * @property integer $id
 * @property integer $site_id
 * @property integer $create_user_id
 * @property string $tel_provider
 * @property string $tel_s_date
 * @property string $tel_e_date
 * @property string $tel_ac_no
 * @property string $tel_ac_pwd
 * @property string $broad_provider
 * @property string $broad_s_date
 * @property string $broad_e_date
 * @property string $broad_ac_no
 * @property string $broad_ac_pwd
 * @property string $tv_provider
 * @property string $tv_s_date
 * @property string $tv_e_date
 * @property string $tv_ac_no
 * @property string $tv_ac_pwd
 * @property string $water_provider
 * @property string $water_s_date
 * @property string $water_e_date
 * @property string $water_ac_no
 * @property string $water_ac_pwd
 */
class LeadAdditional extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tbl_lead_additional';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('site_id, create_user_id', 'required'),
            array('site_id, create_user_id', 'numerical', 'integerOnly' => true),
            array('tel_provider, tel_ac_no, tel_ac_pwd, broad_provider, broad_ac_no, broad_ac_pwd, tv_provider, tv_ac_no, tv_ac_pwd, water_provider, water_ac_no, water_ac_pwd', 'length', 'max' => 255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, site_id, create_user_id, tel_provider, tel_s_date, tel_e_date, tel_ac_no, tel_ac_pwd, broad_provider, broad_s_date, broad_e_date, broad_ac_no, broad_ac_pwd, tv_provider, tv_s_date, tv_e_date, tv_ac_no, tv_ac_pwd, water_provider, water_s_date, water_e_date, water_ac_no, water_ac_pwd', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'site' => array(self::BELONGS_TO, 'LeadSite', 'site_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'site_id' => 'Site',
            'create_user_id' => 'Create User',
            'tel_provider' => 'Tel Provider',
            'tel_s_date' => 'Tel S Date',
            'tel_e_date' => 'Tel E Date',
            'tel_ac_no' => 'Tel Ac No',
            'tel_ac_pwd' => 'Tel Ac Pwd',
            'broad_provider' => 'Broad Provider',
            'broad_s_date' => 'Broad S Date',
            'broad_e_date' => 'Broad E Date',
            'broad_ac_no' => 'Broad Ac No',
            'broad_ac_pwd' => 'Broad Ac Pwd',
            'tv_provider' => 'Tv Provider',
            'tv_s_date' => 'Tv S Date',
            'tv_e_date' => 'Tv E Date',
            'tv_ac_no' => 'Tv Ac No',
            'tv_ac_pwd' => 'Tv Ac Pwd',
            'water_provider' => 'Water Provider',
            'water_s_date' => 'Water S Date',
            'water_e_date' => 'Water E Date',
            'water_ac_no' => 'Water Ac No',
            'water_ac_pwd' => 'Water Ac Pwd',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search($a = null) {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        if (!empty($a)) {
            $criteria->addInCondition('site_id', $a);
        } else {
            $criteria->compare('site_id', $this->site_id);
        }
        $criteria->compare('create_user_id', $this->create_user_id);
        $criteria->compare('tel_provider', $this->tel_provider, true);
        $criteria->compare('tel_s_date', $this->tel_s_date, true);
        $criteria->compare('tel_e_date', $this->tel_e_date, true);
        $criteria->compare('tel_ac_no', $this->tel_ac_no, true);
        $criteria->compare('tel_ac_pwd', $this->tel_ac_pwd, true);
        $criteria->compare('broad_provider', $this->broad_provider, true);
        $criteria->compare('broad_s_date', $this->broad_s_date, true);
        $criteria->compare('broad_e_date', $this->broad_e_date, true);
        $criteria->compare('broad_ac_no', $this->broad_ac_no, true);
        $criteria->compare('broad_ac_pwd', $this->broad_ac_pwd, true);
        $criteria->compare('tv_provider', $this->tv_provider, true);
        $criteria->compare('tv_s_date', $this->tv_s_date, true);
        $criteria->compare('tv_e_date', $this->tv_e_date, true);
        $criteria->compare('tv_ac_no', $this->tv_ac_no, true);
        $criteria->compare('tv_ac_pwd', $this->tv_ac_pwd, true);
        $criteria->compare('water_provider', $this->water_provider, true);
        $criteria->compare('water_s_date', $this->water_s_date, true);
        $criteria->compare('water_e_date', $this->water_e_date, true);
        $criteria->compare('water_ac_no', $this->water_ac_no, true);
        $criteria->compare('water_ac_pwd', $this->water_ac_pwd, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return SiteAdditional the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
