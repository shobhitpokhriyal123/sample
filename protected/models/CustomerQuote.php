<?php

/**
 * This is the model class for table "tbl_customer_quote".
 *
 * The followings are the available columns in table 'tbl_customer_quote':
 * @property integer $id
 * @property integer $create_user_id
 * @property integer $customer_id
 * @property integer $supplier_id
 * @property string $product
 * @property string $min_volume
 * @property string $max_volume
 * @property integer $credit_position
 * @property string $supply_contract
 * @property integer $offer_type
 * @property string $consumption
 * @property string $created
 */
class CustomerQuote extends GxActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tbl_customer_quote';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('create_user_id, site_id, customer_id, supplier_id, product, min_volume, max_volume, credit_position, supply_contract, offer_type, consumption, created', 'required'),
            array('create_user_id, customer_id, supplier_id, site_id, credit_position, offer_type', 'numerical', 'integerOnly' => true),
            array('product, min_volume,unit_rate,meter_no,standing_charge, max_volume, supply_contract, consumption', 'length', 'max' => 255),
            array('supply_contract', 'file', 'allowEmpty' => true, 'types' => 'jpg,jpeg,gif,png'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, create_user_id,unit_rate,standing_charge,meter_no,site_id, customer_id, supplier_id, product, min_volume, max_volume, credit_position, supply_contract, offer_type, consumption, created', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'user' => array(self::BELONGS_TO, 'Users', 'supplier_id'),
            'customer' => array(self::BELONGS_TO, 'AgencyCustomer', 'customer_id'),
             'site' => array(self::BELONGS_TO, 'CustomerSite', 'site_id'),
             'customer1' => array(self::BELONGS_TO, 'Users', 'create_user_id'),
           
            
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'create_user_id' => 'Create User',
            'customer_id' => 'Customer',
            'supplier_id' => 'Supplier',
            'product' => 'Product',
            'min_volume' => 'Min Volume',
            'max_volume' => 'Max Volume',
            'credit_position' => 'Credit Position',
            'supply_contract' => 'Supply Contract',
            'offer_type' => 'Offer Type',
            'consumption' => 'Consumption',
            'created' => 'Created',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search($c = null) {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('create_user_id', $this->create_user_id);
        if ($c) {
            $criteria->compare('customer_id', $c);
        } else {
            $criteria->compare('customer_id', $this->customer_id);
        }
        $criteria->compare('supplier_id', $this->supplier_id);
        $criteria->compare('product', $this->product, true);
        $criteria->compare('min_volume', $this->min_volume, true);
        $criteria->compare('max_volume', $this->max_volume, true);
        $criteria->compare('credit_position', $this->credit_position);
        $criteria->compare('supply_contract', $this->supply_contract, true);
        $criteria->compare('offer_type', $this->offer_type);
        $criteria->compare('consumption', $this->consumption, true);
        $criteria->compare('created', $this->created, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CustomerQuote the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public static function getOfferTypes($id = null) {
        $arr = array('1' => 'Limited Offers', '2' => 'Full Offers');
        if ($id)
            return $arr[$id];
        return $arr;
    }
    
    
    public function searchQuote($create_user_id) {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('create_user_id', $this->create_user_id);
        
              $criteria->with = array('customer1');
        $criteria->addCondition('customer1.id = ' .$create_user_id);
        
        
//        if ($c) {
//            $criteria->compare('customer_id', $c);
//        } else {
//            $criteria->compare('customer_id', $this->customer_id);
//        }
        $criteria->compare('supplier_id', $this->supplier_id);
        $criteria->compare('product', $this->product, true);
        $criteria->compare('min_volume', $this->min_volume, true);
        $criteria->compare('max_volume', $this->max_volume, true);
        $criteria->compare('credit_position', $this->credit_position);
        $criteria->compare('supply_contract', $this->supply_contract, true);
        $criteria->compare('offer_type', $this->offer_type);
        $criteria->compare('consumption', $this->consumption, true);
        $criteria->compare('created', $this->created, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
    

  
    
    }


