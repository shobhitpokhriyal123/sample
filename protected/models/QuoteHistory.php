<?php

/**
 * This is the model class for table "tbl_quote_history".
 *
 * The followings are the available columns in table 'tbl_quote_history':
 * @property integer $id
 * @property integer $create_user_id
 * @property integer $assigned_to
 * @property integer $price_id
 * @property string $product
 * @property string $region
 * @property string $contract_length
 * @property string $start_date
 * @property string $payment_type
 * @property string $standing_charge
 * @property string $unit_charge
 * @property string $annual_cost
 * @property string $created
 */
class QuoteHistory extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_quote_history';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('create_user_id, assigned_to, price_id,  payment_type, standing_charge, unit_charge, annual_cost, created', 'required'),
			array('create_user_id, assigned_to, price_id', 'numerical', 'integerOnly'=>true),
			array('product, region', 'length', 'max'=>200),
			array('contract_length, start_date, payment_type, standing_charge, unit_charge, annual_cost', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, create_user_id, assigned_to, price_id, product, region, contract_length, start_date, payment_type, standing_charge, unit_charge, annual_cost, created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'user' => array(self::BELONGS_TO, 'Users', 'create_user_id'),
                     'supplier' => array(self::BELONGS_TO, 'Users', 'assigned_to'),
                    
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'create_user_id' => 'Create User',
			'assigned_to' => 'Assigned To',
			'price_id' => 'Price',
			'product' => 'Product',
			'region' => 'Region',
			'contract_length' => 'Contract Length',
			'start_date' => 'Start Date',
			'payment_type' => 'Payment Type',
			'standing_charge' => 'Standing Charge',
			'unit_charge' => 'Unit Charge',
			'annual_cost' => 'Annual Cost',
			'created' => 'Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('create_user_id',$this->create_user_id);
		$criteria->compare('assigned_to',$this->assigned_to);
		$criteria->compare('price_id',$this->price_id);
		$criteria->compare('product',$this->product,true);
		$criteria->compare('region',$this->region,true);
		$criteria->compare('contract_length',$this->contract_length,true);
		$criteria->compare('start_date',$this->start_date,true);
		$criteria->compare('payment_type',$this->payment_type,true);
		$criteria->compare('standing_charge',$this->standing_charge,true);
		$criteria->compare('unit_charge',$this->unit_charge,true);
		$criteria->compare('annual_cost',$this->annual_cost,true);
		$criteria->compare('created',$this->created,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return QuoteHistory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
