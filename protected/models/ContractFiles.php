<?php

/**
 * This is the model class for table "tbl_contract_files".
 *
 * The followings are the available columns in table 'tbl_contract_files':
 * @property integer $id
 * @property integer $user_id
 * @property integer $is_paper
 * @property integer $is_copy_of_original
 * @property string $files
 * @property string $contract_signed_date
 */
class ContractFiles extends GxActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tbl_contract_files';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('user_id, is_paper, contract_id,is_copy_of_original, contract_signed_date', 'required'),
            array('user_id, is_paper, is_copy_of_original', 'numerical', 'integerOnly' => true),
            array('files, contract_signed_date', 'length', 'max' => 255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, user_id, is_paper,contract_id, is_copy_of_original, files, contract_signed_date', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'user_id' => 'User',
            'is_paper' => 'Is Paper',
            'is_copy_of_original' => 'Is Copy Of Original',
            'files' => 'Upload Contract Files',
            'contract_signed_date' => 'Contract Signed Date',
            'contract_id' => 'Contract'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('user_id', $this->user_id);
        $criteria->compare('is_paper', $this->is_paper);
        $criteria->compare('is_copy_of_original', $this->is_copy_of_original);
        $criteria->compare('files', $this->files, true);
        $criteria->compare('contract_signed_date', $this->contract_signed_date, true);
        $criteria->compare('contract_id', $this->contract_id, true);
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ContractFiles the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
