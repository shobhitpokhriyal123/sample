<?php

/**
 * This is the model class for table "tbl_role".
 *
 * The followings are the available columns in table 'tbl_role':
 * @property integer $id
 * @property string $title
 * @property integer $role
 * @property integer $active
 * @property string $created
 */
class Role extends GxActiveRecord {

    
    const ROLE_ACTIVE = 1;
    const ROLE_INACTIVE = 0;
    const ROLE_ADMIN = 1;
    const ROLE_CLIENT = 2;

    

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tbl_role';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('title,  description', 'required'),
            array('active', 'numerical', 'integerOnly' => true),
            array('title', 'length', 'max' => 255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, title, role,description, active, created', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'role' => 'Role',
            'active' => 'Status',
            'created' => 'Created',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('role', $this->role);
        $criteria->compare('active', $this->active);
        $criteria->compare('created', $this->created, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Role the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * 
     * @return type
     */
    public function getRolename() {
        return $this->title;
    }

    /**
     * 
     * @return type
     */
    public function beforeSave() {
        if ($this->isNewRecord)
            $this->created = new CDbExpression('NOW()');
//        else
//            $this->modified = new CDbExpression('NOW()');

        return parent::beforeSave();
    }
    
    /**
     * 
     * @param type $status
     * @return string
     */
    public function getStatus($status = null){
        //echo $status; die;
         $statusArr =   array(self::ROLE_ACTIVE=>'Active', self::ROLE_INACTIVE=>'Inactive');
        if($status != null){
             $statusArr =  $statusArr[$status];
        }
      
        return $statusArr;
    }
    
    /**
     * 
     * @param type $role
     * @return type
     */
    public static function getRoles($role = null){
       // echo $role; die;
        if($role)
           return self::model()->findByPk($role)->title;//findById($role)->title;
        return self::model()->findAll();
    }

    
    
    /**
     * 
     * @param type $role
     * @return type
     */
    public static function getRolesForLetter(){
       // echo $role; die;
        $criteria = new CDbCriteria;
        $criteria->addCondition('id != 1');
        return self::model()->findAll($criteria);
    }

}
