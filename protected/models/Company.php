<?php

/**
 * This is the model class for table "tbl_company".
 *
 * The followings are the available columns in table 'tbl_company':
 * @property integer $id
 * @property integer $user_id
 * @property integer $create_user_id
 * @property string $company
 * @property string $telephone
 * @property string $contact_name
 * @property string $contact_position
 * @property string $contact_email
 * @property string $address
 * @property string $postcode
 * @property string $registraion_no
 * @property integer $business_type
 * @property integer $is_micro_business
 * @property integer $no_of_emp
 * @property integer $no_of_years
 * @property integer $turnover
 */
class Company extends GxActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tbl_company';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('user_id, create_user_id,contract_id, company, telephone, contact_name, contact_position, contact_email,  postcode, registraion_no,    no_of_years, turnover', 'required'),
            array('user_id, create_user_id, business_type, is_micro_business, no_of_emp, no_of_years, turnover', 'numerical', 'integerOnly' => true),
            array('company, telephone,contract_id,address_no, gender,contact_name, contact_position, contact_email, address, postcode, registraion_no,street,town_city, county', 'length', 'max' => 255),
            array('contact_email', 'email'),
// The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, user_id,mobile,gender,street,address_no,town_city, county, create_user_id,contract_id, company, telephone, contact_name, contact_position, contact_email, address, postcode, registraion_no, business_type, is_micro_business, no_of_emp, no_of_years, turnover', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
               'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'user_id' => 'User',
            'create_user_id' => 'Create User',
            'company' => 'Business Name',
            'telephone' => 'Landline',
            'mobile'=>'Mobile No',
            'contact_name' => 'First Name',
            'gender' =>'Title',
            'contact_last_name'=>'Last Name',
            'contact_position' => 'Contact Position',
            'contact_email' => 'Contact Email',
            'address' => 'Address',
            'postcode' => 'Postcode',
            'registraion_no' => 'Registraion No',
            'business_type' => 'Business Structure',
            'is_micro_business' => 'Is Micro Business',
            'no_of_emp' => 'No Of Emp',
            'no_of_years' => 'No Of Years Trading',
            'turnover' => 'Turnover',
            'contract_id' => 'Contract',
            'street'=>'Street',
            'town_city'=>'Town/City',
            'county'=>'County',
            'address_no'=>'Business Address No'
            
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('user_id', $this->user_id);
        $criteria->compare('create_user_id', $this->create_user_id);
        $criteria->compare('company', $this->company, true);
        $criteria->compare('telephone', $this->telephone, true);
        $criteria->compare('contact_name', $this->contact_name, true);
        $criteria->compare('contact_position', $this->contact_position, true);
        $criteria->compare('contact_email', $this->contact_email, true);
        $criteria->compare('address', $this->address, true);
        $criteria->compare('postcode', $this->postcode, true);
        $criteria->compare('registraion_no', $this->registraion_no, true);
        $criteria->compare('business_type', $this->business_type);
        $criteria->compare('is_micro_business', $this->is_micro_business);
        $criteria->compare('no_of_emp', $this->no_of_emp);
        $criteria->compare('contract_id', $this->contract_id, true);
        $criteria->compare('no_of_years', $this->no_of_years);
        $criteria->compare('turnover', $this->turnover);
         $criteria->compare('gender', $this->gender);
        

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Company the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public static function getBusinessType($id = null) {
        $businesses = BusinessStructure::model()->findAll(); //array('1' => 'Limited Company', '2' => 'ISO Certified');
      
        
        $arr = array();
        foreach ($businesses as $business){
            $arr[$business->id] = $business->title;
        } 
        
      //echo '<pre>'; print_r($arr); die;
       if($id)
           return $arr[$id];
        return $arr;
    }

    public static function getTrueFalse($id = null) {
        $arr = array('1' => 'Yes', '0' => 'No');
        if ($id)
            return $arr[$id];
        return $arr;
    }

}
