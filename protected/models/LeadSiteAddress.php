<?php

/**
 * This is the model class for table "tbl_lead_site_address".
 *
 * The followings are the available columns in table 'tbl_lead_site_address':
 * @property integer $id
 * @property integer $create_user_id
 * @property integer $lead_id
 * @property integer $site_id
 * @property string $site_post_code
 * @property string $site_address_name
 * @property integer $site_address_no
 * @property string $site_street1
 * @property string $site_street2
 * @property string $site_town
 * @property string $site_country
 * @property string $site_address_same
 * @property string $business_address_same
 * @property string $billing_post_code
 * @property string $billing_name
 * @property integer $billing_no
 * @property string $street_address_1
 * @property string $street_address_2
 * @property string $town
 * @property string $country
 * @property string $created
 */
class LeadSiteAddress extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tbl_lead_site_address';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('create_user_id,  site_id', 'required'),
            array('create_user_id,  site_id', 'numerical', 'integerOnly' => true),
            array('site_post_code, billing_post_code', 'length', 'max' => 64),
            array('site_address_name, site_street1, site_street2, site_town, site_country,site_address_no, billing_name, street_address_1, street_address_2, town, country', 'length', 'max' => 255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, create_user_id, billing_name,  site_id, site_post_code, site_address_name, site_address_no, site_street1, site_street2, site_town, site_country, site_address_same, business_address_same, billing_post_code, billing_name, billing_no, street_address_1, street_address_2, town, country, created', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'site' => array(self::BELONGS_TO, 'LeadSite', 'site_id'),
            'lead' => array(self::BELONGS_TO, 'LeadContacts', 'lead_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'create_user_id' => 'Create User',
            'lead_id' => 'Lead',
            'site_id' => 'Site',
            'site_post_code' => 'Site Post Code',
            'site_address_name' => 'Site Address Name',
            'site_address_no' => 'Site Address No',
            'site_street1' => 'Site Street',
            'site_street2' => 'Site District',
            'site_town' => 'Site Town/City',
            'site_country' => 'Site County',
            'site_address_same' => 'Site Address Same',
            'business_address_same' => 'Business Address Same',
            'billing_post_code' => 'Billing Post Code',
            'billing_name' => 'Billing Name',
            'billing_no' => 'Billing No',
            'street_address_1' => 'Billing Street Name',
            'street_address_2' => 'Billing District',
            'town' => 'Billing Town/City',
            'country' => 'Billing County',
            'created' => 'Created',
            'lead_id' =>'Choose Contact'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('create_user_id', $this->create_user_id);
        $criteria->compare('lead_id', $this->lead_id);
        $criteria->compare('site_id', $this->site_id);
        $criteria->compare('site_post_code', $this->site_post_code, true);
        $criteria->compare('site_address_name', $this->site_address_name, true);
        $criteria->compare('site_address_no', $this->site_address_no);
        $criteria->compare('site_street1', $this->site_street1, true);
        $criteria->compare('site_street2', $this->site_street2, true);
        $criteria->compare('site_town', $this->site_town, true);
        $criteria->compare('site_country', $this->site_country, true);
        $criteria->compare('site_address_same', $this->site_address_same, true);
        $criteria->compare('business_address_same', $this->business_address_same, true);
        $criteria->compare('billing_post_code', $this->billing_post_code, true);
        $criteria->compare('billing_name', $this->billing_name, true);
        $criteria->compare('billing_no', $this->billing_no);
        $criteria->compare('street_address_1', $this->street_address_1, true);
        $criteria->compare('street_address_2', $this->street_address_2, true);
        $criteria->compare('town', $this->town, true);
        $criteria->compare('country', $this->country, true);
        $criteria->compare('created', $this->created, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return LeadSiteAddress the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    
//     protected function beforeDelete() {
//        LeadBusiness::model()->deleteAll(' lead_id = ' . $this->id);
//        parent::beforeDelete();
//    }


}
