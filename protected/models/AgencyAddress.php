<?php

/**
 * This is the model class for table "tbl_agency_address".
 *
 * The followings are the available columns in table 'tbl_agency_address':
 * @property integer $id
 * @property integer $agency_id
 * @property integer $address_number
 * @property string $town
 * @property string $address1
 * @property string $country
 * @property string $address2
 * @property string $telephone
 * @property string $website
 * @property string $email
 * @property string $fax
 */
class AgencyAddress extends CActiveRecord {

    public $mobile;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tbl_agency_address';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('agency_id,postcode, address_number, telephone', 'required'),
            array('agency_id, address_number', 'numerical', 'integerOnly' => true),
            array('town, address1, country, address2, telephone, website, email, fax', 'length', 'max' => 255),
            // array('website', 'url', 'defaultScheme' => 'http'),
// The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, agency_id,postcode, address_number, town, address1, country, address2, telephone, website, email, fax', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'agency_id' => 'Agency',
            'address_number' => 'Address Number/Name',
            'town' => 'Town/City',
            'address1' => 'Address1',
            'country' => 'County',
            'address2' => 'Address2',
            'telephone' => 'Landline',
            'website' => 'Website',
            'mobile' => 'Mobile',
            'email' => 'Business Email',
            'fax' => 'Fax',
            'postcode' => 'Postcode'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('agency_id', $this->agency_id);
        $criteria->compare('address_number', $this->address_number);
        $criteria->compare('town', $this->town, true);
        $criteria->compare('address1', $this->address1, true);
        $criteria->compare('country', $this->country, true);
        $criteria->compare('address2', $this->address2, true);
        $criteria->compare('telephone', $this->telephone, true);
        $criteria->compare('website', $this->website, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('fax', $this->fax, true);
        $criteria->compare('postcode', $this->postcode, true);


        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return AgencyAddress the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
