<?php

/**
 * This is the model class for table "tbl_contract_detail".
 *
 * The followings are the available columns in table 'tbl_contract_detail':
 * @property integer $id
 * @property integer $create_user_id
 * @property string $agency_id
 * @property string $agency_name
 * @property string $mpan_topline
 * @property string $mpan_core
 * @property string $eac
 * @property integer $current_supplier
 * @property integer $new_supplier
 * @property integer $smart_meter
 * @property string $date_time
 * @property integer $renewal
 * @property integer $payment
 * @property integer $payment_type
 * @property integer $contract_period
 * @property string $estimate_date
 * @property string $contract_date
 * @property string $live_date
 * @property string $billing_period
 */
class ContractDetail extends GxActiveRecord{

    public $customer;
    public $company_name;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tbl_contract_detail';
    }

    /*     * ALTER TABLE `tbl_contract_detail` ADD `status` INT(11) NOT NULL AFTER `billing_period`;
     * @return array validation rules for model attributes.
     */

    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('create_user_id, agency_id, agent_id, agency_name, mpan_topline,  eac, current_supplier, new_supplier, smart_meter, date_time, renewal,  payment_type, contract_period, estimate_date, contract_date, live_date, billing_period', 'required'),
            array('create_user_id,is_corporate,price_id,quote_id, current_supplier, new_supplier, smart_meter, renewal, payment, payment_type', 'numerical', 'integerOnly' => true),
            array('agency_id,status, agency_name, mpan_topline, mpan_core, eac, date_time, estimate_date, contract_date, live_date, billing_period', 'length', 'max' => 255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, create_user_id,agent_id, price_id,quote_id, status, agency_id, agency_name, mpan_topline, mpan_core, eac, current_supplier, new_supplier, smart_meter, date_time,is_corporate, renewal, payment, payment_type, contract_period, estimate_date, contract_date, live_date, billing_period', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'price' => array(self::BELONGS_TO, 'Price', 'price_id'),
            'user' => array(self::BELONGS_TO, 'Users', 'create_user_id'),
            'c_supplier' => array(self::BELONGS_TO, 'Users', 'current_supplier'),
            'n_supplier' => array(self::BELONGS_TO, 'Users', 'new_supplier'),
            'company' => array(self::HAS_ONE, 'Company', 'contract_id'),
            'bank' => array(self::HAS_ONE, 'Bank', 'contract_id'),
            'business_address' => array(self::HAS_ONE, 'BusinessAddress', 'contract_id'),
            'contract_files' => array(self::HAS_ONE, 'ContractFiles', 'contract_id'),
            'validation' => array(self::HAS_ONE, 'Validation', 'contract_id'),
            'customer_address' => array(self::HAS_ONE, 'PartnerAddress', 'contract_id'),
            'agency' => array(self::BELONGS_TO, 'Agency', 'agency_id'),
            'agency1' => array(self::BELONGS_TO, 'Users', 'agency_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'create_user_id' => 'Create User',
            'agency_id' => 'Agency ID',
            'agency_name' => 'Agency Name',
            'mpan_topline' => 'Meter No',
            'mpan_core' => 'Mpan Core',
            'eac' => 'Consumption',
            'current_supplier' => 'Current Supplier',
            'new_supplier' => 'New Supplier',
            'smart_meter' => 'Smart Meter',
            'date_time' => 'Date Time',
            'renewal' => ' Acquisition/Renewal',
            // 'payment' => 'Payment',
            'payment_type' => 'Payment Type',
            'contract_period' => 'Contract Period',
            'estimate_date' => 'Start Date',
            'contract_date' => 'End Date',
            'live_date' => 'Live Date',
            'billing_period' => 'Billing Period',
            'status' => 'Status of Contract',
            'price_id' => 'Price',
            'quote_id' => 'Quote id'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        if (Yii::app()->user->isAdmin)
            $criteria->compare('create_user_id', $this->create_user_id);
        else
            $criteria->compare('create_user_id', Yii::app()->user->id);
        $criteria->compare('agency_id', $this->agency_id, true);
        $criteria->compare('agency_name', $this->agency_name, true);
        $criteria->compare('mpan_topline', $this->mpan_topline, true);
        $criteria->compare('mpan_core', $this->mpan_core, true);
        $criteria->compare('eac', $this->eac, true);
        $criteria->compare('current_supplier', $this->current_supplier);
        $criteria->compare('new_supplier', $this->new_supplier);
        $criteria->compare('smart_meter', $this->smart_meter);
        $criteria->compare('date_time', $this->date_time, true);
        $criteria->compare('renewal', $this->renewal);
        $criteria->compare('payment', $this->payment);
        $criteria->compare('payment_type', $this->payment_type);
        $criteria->compare('contract_period', $this->contract_period);
        $criteria->compare('estimate_date', $this->estimate_date, true);
        $criteria->compare('contract_date', $this->contract_date, true);
        $criteria->compare('live_date', $this->live_date, true);
        $criteria->compare('billing_period', $this->billing_period, true);
        $criteria->compare('status', $this->status, true);
        $criteria->compare('quote_id', $this->quote_id, true);

        $criteria->order = 'id desc';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ContractDetail the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public static function getBillingPeriod($id = null) {
        $arr = array('1' => 'Monthly', '2' => 'Quarterly', '3' => 'Yearly');
        if ($id) {
            return $arr[$id];
        }
        return $arr;
    }

    public static function getPaymentType($id = null) {
       // echo $id; die;
        $arr = array('1' => 'Cash', '2' => 'Direct Debit');
        if ($id) {
            return $arr[$id];
        }
        return $arr;
    }

    public static function getAcquisition() {
        $arr = array('1' => 'Acquisition', '2' => 'Renewal');
        return $arr;
    }

    public static function getAgencyId($id = null) {
        $agency = Agency::model()->findByAttributes(array('user_id' => $id));
        return $agency->agency_id;
    }

}
