<?php

/**
 * This is the model class for table "tbl_product_mapping".
 *
 * The followings are the available columns in table 'tbl_product_mapping':
 * @property integer $id
 * @property string $product_id
 * @property string $attribute_name
 * @property string $attribute_slug
 * @property string $attribute_value
 * @property integer $attribute_type
 * @property integer $status
 */
class ProductMapping extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tbl_product_mapping';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('attribute_slug ,attribute_name', 'required'),
            array('attribute_slug', 'unique'),
            array('attribute_type, status', 'numerical', 'integerOnly' => true),
            array('product_id, attribute_name, attribute_slug, attribute_value', 'length', 'max' => 255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, product_id, attribute_name, attribute_slug, attribute_value, attribute_type, status', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'product_id' => 'Product',
            'attribute_name' => 'Attribute Name',
            'attribute_slug' => 'Attribute Slug',
            'attribute_value' => 'Attribute Value',
            'attribute_type' => 'Attribute Type',
            'status' => 'Status',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('product_id', $this->product_id, true);
        $criteria->compare('attribute_name', $this->attribute_name, true);
        $criteria->compare('attribute_slug', $this->attribute_slug, true);
        $criteria->compare('attribute_value', $this->attribute_value, true);
        $criteria->compare('attribute_type', $this->attribute_type);
        $criteria->compare('status', $this->status);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ProductMapping the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * 
     * @return \CActiveDataProvider
     */
    public function searchproduct() {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('product_id', $this->product_id, true);
        $criteria->compare('attribute_name', $this->attribute_name, true);
        $criteria->compare('attribute_slug', $this->attribute_slug, true);
        $criteria->compare('attribute_value', $this->attribute_value, true);
        $criteria->compare('attribute_type', $this->attribute_type);
        $criteria->compare('status', $this->status);
        $criteria->addCondition('product_id != ""');

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}
