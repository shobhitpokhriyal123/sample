<?php

/**
 * This is the model class for table "tbl_quote_date".
 *
 * The followings are the available columns in table 'tbl_quote_date':
 * @property integer $id
 * @property integer $supplier_id
 * @property integer $create_user_id
 * @property string $start_date
 * @property string $supplier_delays
 * @property string $end_date
 * @property integer $status
 * @property string $created
 */
class QuoteDate extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tbl_quote_date';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('supplier_id, create_user_id, start_date, supplier_delays, end_date, created', 'required'),
            array('supplier_id,number_of, create_user_id, status', 'numerical', 'integerOnly' => true),
            array('supplier_id', 'unique'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, supplier_id, number_of,create_user_id, start_date, supplier_delays, end_date, status, created', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'user' => array(self::BELONGS_TO, 'Users', 'supplier_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'supplier_id' => 'Supplier',
            'create_user_id' => 'Create User',
            'start_date' => 'Start Date',
            'supplier_delays' => 'Supplier Delays',
            'end_date' => 'End Date',
            'status' => 'Status',
            'created' => 'Created',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('supplier_id', $this->supplier_id);
        $criteria->compare('create_user_id', $this->create_user_id);
        $criteria->compare('start_date', $this->start_date, true);
        $criteria->compare('supplier_delays', $this->supplier_delays, true);
        $criteria->compare('end_date', $this->end_date, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('created', $this->created, true);
        $criteria->compare('number_of', $this->number_of, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return QuoteDate the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * 
     * @param type $k
     * @return int
     */
    public static function getNumbers($k = null) {
        $arr = array();
        for ($i = 1; $i < 32; $i++) {
            $arr[$i] = $i;
        }
        if ($k)
            $arr[$k];
        return $arr;
    }

    /**
     * 
     * @param type $id
     * @return string
     */
    public static function getStatus($id = null) {
        $arr = array(1 => 'Active', 0 => 'Inactive');
        if ($id)
            return $arr[$id];
        return $arr;
    }

    public static function getTypes($id = null) {
        $arr = array('day' => 'Day', 'days' => 'Days', 'month' => 'Month', 'months' => 'Months', 'year' => 'Year', 'years' => 'Years');
        if ($id)
            return $arr[$id];
        return $arr;
    }

}
