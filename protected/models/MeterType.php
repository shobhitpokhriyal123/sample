<?php

/**
 * This is the model class for table "tbl_meter_type".
 *
 * The followings are the available columns in table 'tbl_meter_type':
 * @property integer $id
 * @property integer $supplier_id
 * @property string $title
 * @property integer $status
 * @property string $created
 */
class MeterType extends GxActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tbl_meter_type';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('supplier_id,meter_code, tanency, validity, energy_per_year, smart_meter,  night, weekday,  title, created', 'required'),
            array('supplier_id,title, status', 'numerical', 'integerOnly' => true),
            //array('', 'length', 'max' => 255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, supplier_id,mapping_title, meter_code, tanency, validity, energy_per_year, smart_meter,  night, weekday, meter_code, title, status, created', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'user' => array(self::BELONGS_TO, 'Users', 'supplier_id'),
            'baseTitle' => array(self::BELONGS_TO, 'MeterBase', 'title'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'supplier_id' => 'Supplier',
            'title' => 'Title',
            'status' => 'Status',
            'created' => 'Created',
            'tanency' => 'Change Of Tanency?',
            'validity' => 'Contract End Date',
            'smart_meter' => 'Is smart meter',
            'energy_per_year' => 'Consumption split',
            'weekday' => 'Day',
            'night' => 'Night',
            'mapping_title' => 'mapping title'
        );
    }

    /**
     * 
     * @return type
     */
    public function getMeters() {
        return array(
            'Day' => 'Day',
            'Day/Night' => 'Day/Night',
            'Day/Night/EWE' => 'Day/Night/EWE',
            'Day/EWE' => 'Day/EWE',
            '1 rate STOD' => '1 rate STOD',
            '2 rate STOD' => '2 rate STOD',
            '3 rate STOD' => '3 rate STOD',
            '4 rate STOD' => '4 rate STOD'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search($sid = null) {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        if ($sid) {
            $criteria->compare('supplier_id', $sid);
        } else {
            $criteria->compare('supplier_id', $this->supplier_id);
        }
        $criteria->compare('title', $this->title, true);
        $criteria->compare('tanency', $this->tanency, true);
        $criteria->compare('validity', $this->validity, true);
        $criteria->compare('smart_meter', $this->smart_meter, true);
        $criteria->compare('energy_per_year', $this->energy_per_year, true);
        $criteria->compare('weekday', $this->weekday, true);
        $criteria->compare('night', $this->night, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('created', $this->created, true);
        $criteria->compare('meter_code', $this->meter_code, true);
        $criteria->compare('mapping_title', $this->mapping_title, true);



        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return MeterType the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * 
     * @param type $id
     * @return string
     */
    public static function getStatus($id = null) {
        if ($id == 1) {
            return 'Yes';
        } elseif ($id == 0) {
            return 'No';
        } else {
            return array('1' => 'Yes', '0' => 'No');
        }
    }

    public static function getMeterTypes($id = null) {
        $arr = array();
        $meterTypes = MeterBase::model()->findAll();
        if ($meterTypes) {
            foreach ($meterTypes as $meterType) {

                $arr[$meterType->id] = $meterType->title;
            }
        }
        if ($id == '0' )
            return "N/A";
        else if($id > 0)
            return $arr[$id];
        
        return $arr;
    }

}
