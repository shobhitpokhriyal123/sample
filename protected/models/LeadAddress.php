<?php

/**
 * This is the model class for table "tbl_lead_address".
 *
 * The followings are the available columns in table 'tbl_lead_address':
 * @property integer $id
 * @property integer $create_user_id
 * @property integer $lead_id
 * @property string $business_postcode
 * @property string $business_name
 * @property integer $business_no
 * @property string $street_address_1
 * @property string $street_address_2
 * @property string $town
 * @property string $country
 * @property string $billling_add
 * @property string $billing_postcode
 * @property string $billing_name
 * @property integer $billing_no
 * @property string $street_address_11
 * @property string $street_address_12
 * @property string $town1
 * @property string $country1
 * @property string $created
 */
class LeadAddress extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tbl_lead_address';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('create_user_id,  created', 'required'),
            array('create_user_id, lead_id', 'numerical', 'integerOnly' => true),
            array('business_postcode, billing_postcode', 'length', 'max' => 50),
            array('street_address_1, business_no, street_address_2,billing_no, town, country, billing_name, street_address_11, street_address_12, town1, country1', 'length', 'max' => 255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, create_user_id,business_name, lead_id, business_postcode,  business_no, street_address_1, street_address_2, town, country, billling_add, billing_postcode, billing_name, billing_no, street_address_11, street_address_12, town1, country1, created', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
                //'site' => array(self::BELONGS_TO, 'LeadSite', 'site_id'),
//            'lead_lead' => array(self::BELONGS_TO, 'LeadContacts', 'lead_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'create_user_id' => 'Create User',
           // 'lead_id' => 'Lead',
            'business_postcode' => 'Business Postcode',
            'business_name' => 'Business Name',
            'business_no' => 'Business Address No/Name',
            'street_address_1' => 'Street Name',
            'street_address_2' => 'District',
            'town' => 'Town/City',
            'country' => 'County',
            'billling_add' => 'Billling Addresses',
            'billing_postcode' => 'Billing Postcode',
            'billing_name' => 'Billing Name',
            'billing_no' => 'Billing Address No',
            'street_address_11' => 'Street Name',
            'street_address_12' => 'District',
            'town1' => 'Billing Town/ City',
            'country1' => 'County',
            'created' => 'Created',
            'lead_id' =>'Business Name'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('create_user_id', $this->create_user_id);
        $criteria->compare('lead_id', $this->lead_id);
//        $criteria->with = array(
//            'lead_lead'
//        );
        $criteria->compare('business_postcode', $this->business_postcode, true);
        $criteria->compare('business_name', $this->business_name, true);
        $criteria->compare('business_no', $this->business_no);
        $criteria->compare('street_address_1', $this->street_address_1, true);
        $criteria->compare('street_address_2', $this->street_address_2, true);
        $criteria->compare('town', $this->town, true);
        $criteria->compare('country', $this->country, true);
        $criteria->compare('billling_add', $this->billling_add, true);
        $criteria->compare('billing_postcode', $this->billing_postcode, true);
        $criteria->compare('billing_name', $this->billing_name, true);
        $criteria->compare('billing_no', $this->billing_no);
        $criteria->compare('street_address_11', $this->street_address_11, true);
        $criteria->compare('street_address_12', $this->street_address_12, true);
        $criteria->compare('town1', $this->town1, true);
        $criteria->compare('country1', $this->country1, true);
        $criteria->compare('created', $this->created, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return LeadAddress the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

  

}
