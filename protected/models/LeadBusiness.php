<?php

/**
 * This is the model class for table "tbl_lead_business".
 *
 * The followings are the available columns in table 'tbl_lead_business':
 * @property integer $id
 * @property integer $create_user_id
 * @property integer $lead_id
 * @property string $business_name
 * @property integer $credit_score
 * @property string $partner_channel
 * @property string $business_level
 * @property integer $charity_no
 * @property string $campaign
 * @property string $year_month_trading
 * @property string $trending_type
 * @property string $external_reference
 * @property string $client_reference
 * @property string $business_type
 * @property string $lead_source
 * @property string $visibility
 * @property string $created
 */
class LeadBusiness extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tbl_lead_business';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('create_user_id, lead_id, business_name,  business_type, created', 'required'),
            array('create_user_id,credit_score, lead_id', 'numerical', 'integerOnly' => true),
            array('business_name, partner_channel,credit_score, business_level, campaign, year_month_trading, trending_type, external_reference, client_reference, business_type, lead_source, visibility', 'length', 'max' => 255),
            //  array('business_name', 'unique'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, create_user_id, lead_id, business_name, credit_score, partner_channel, business_level, charity_no, campaign, year_month_trading, trending_type, external_reference, client_reference, business_type, lead_source, visibility, created', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'site' => array(self::BELONGS_TO, 'LeadSite', 'site_id'),
            'lead' => array(self::BELONGS_TO, 'LeadContacts', 'lead_id'),
            'leadAddress' => array(self::HAS_ONE, 'LeadAddress', 'business_id'),
            'type' => array(self::BELONGS_TO, 'BusinessType', 'business_type'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'create_user_id' => 'Create User',
            'lead_id' => 'Lead',
            'business_name' => 'Business Name',
            'credit_score' => 'Credit Score',
            'partner_channel' => 'Partner Channel',
            'business_level' => 'Business Level',
            'charity_no' => 'Regsitered Company/Charity Number',
            'campaign' => 'Campaign',
            'year_month_trading' => 'Trading Timescale(Years and Months)',
            'trending_type' => 'Business Structure',
            'external_reference' => 'Reference',
            'client_reference' => 'Billing Request/Debit/Cash/Cheque',
            'business_type' => 'Business Type',
            'lead_source' => 'Billing Request(Monthly, Quarterly)',
            'visibility' => 'Active/Inactive',
            'created' => 'Created',
            'lead_id' => 'Choose Contact'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search($customer = null) {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;
//echo $this->business_name; die;
        $criteria->compare('id', $this->id);
        $criteria->compare('create_user_id', $this->create_user_id);
        $criteria->compare('lead_id', $this->lead_id);
        $criteria->with = array('lead');
        if (!Yii::app()->user->isAdmin) {
            $id = Yii::app()->user->id;
            $criteria->condition = "lead.agency_id =  $id OR lead.agent_id = $id";
        }
        if ($customer) {
            $criteria->addCondition('lead.is_lead = 0');
        } else {
            $criteria->addCondition('lead.is_lead = 1');
        }
        $criteria->compare('credit_score', $this->credit_score);
        $criteria->compare('partner_channel', $this->partner_channel, true);
        $criteria->compare('business_level', $this->business_level, true);
        $criteria->compare('charity_no', $this->charity_no);
        $criteria->compare('campaign', $this->campaign, true);
        $criteria->compare('year_month_trading', $this->year_month_trading, true);
        $criteria->compare('trending_type', $this->trending_type, true);
        $criteria->compare('external_reference', $this->external_reference, true);
        $criteria->compare('client_reference', $this->client_reference, true);
        $criteria->compare('business_type', $this->business_type, true);
        $criteria->compare('lead_source', $this->lead_source, true);
        $criteria->compare('visibility', $this->visibility, true);
        $criteria->compare('created', $this->created, true);
//        $a = new CActiveDataProvider($this, array(
//            'criteria' => $criteria,
//        ));
//        echo '<pre>';
//        print_r($a->getData());
//        die;
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return LeadBusiness the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * 
     * @return type
     */
    public static function getLeadName() {
        return $users = Yii::app()->db->createCommand()
                ->select('business_name')
                ->from('tbl_lead_business')
                ->queryAll();
    }

    /**
     * 
     * @return type
     */
    public static function isBusinessName($bname) {
        return $user = Yii::app()->db->createCommand()
                ->select('id, lead_id,  business_name')
                ->from('tbl_lead_business')
                ->where('business_name = "' . $bname . '"')
                ->queryAll();
    }

    /**
     * 
     * @param type $lead_id
     * @return type
     */
    public static function getBusinessName($lead_id) {
        $business = LeadBusiness::model()->findByAttributes(array('lead_id' => $lead_id));
        return isset($business->business_name) ? $business->business_name : "N/A";
    }

    public static function getAgencyName($lead_id) {
        $lead = LeadContacts::model()->findByPk($lead_id);
        $user = Users::model()->findByPk($lead->agency_name);
        return isset($user->username) ? $user->username : "N/A";
    }

    public static function getLeadDropdown() {
        $users = LeadContacts::model()->findAll();
        $html = '';
        $html .= '<select id="LeadBusiness_select_" class="form-control" name="LeadBusiness[lead_id]">';
        foreach ($users as $user) {
            $html .= '<option value="' . $user->id . '">' . $user->first_name . '</option>';
        }
        $html .= '</select>';
        return $html;
    }

    public static function getLevelDropdown() {
        $html = '';
        $html .= '<select id="LeadBusiness_business_level_" class="form-control" name="LeadBusiness[business_level]">';
        $html .= '<option value="1">Micro</option>';
        $html .= '<option value="0">Non Micro</option>';
        $html .= '</select>';
        return $html;
    }

    public static function getTrandingDropdown() {
        $users = BusinessStructure::model()->findAll();
        $html = '';
        $html .= '<select id="LeadBusiness_business_type_" class="form-control" name="LeadBusiness[business_type]">';
        foreach ($users as $user) {
            $html .= '<option value="' . $user->id . '">' . $user->title . '</option>';
        }
        $html .= '</select>';
        return $html;
    }

    public static function getBusinessDropdown() {
        $users = BusinessType::model()->findAll();
        $html = '';
        $html .= '<select id="LeadContacts_trending_type_" class="form-control" name="LeadBusiness[trending_type]">';
        foreach ($users as $user) {
            $html .= '<option value="' . $user->id . '">' . $user->title . '</option>';
        }
        $html .= '</select>';
        return $html;
    }

}
