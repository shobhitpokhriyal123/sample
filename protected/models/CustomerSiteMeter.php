<?php

/**
 * This is the model class for table "tbl_customer_site_meter".
 *
 * The followings are the available columns in table 'tbl_customer_site_meter':
 * @property integer $id
 * @property integer $site_id
 * @property integer $create_user_id
 * @property integer $utility
 * @property string $meter_no
 * @property integer $profile_type
 * @property integer $meter_type
 * @property string $available_capability
 * @property integer $status
 * @property string $created
 */
class CustomerSiteMeter extends GxActiveRecord {

    public $consumption_data;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tbl_customer_site_meter';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('site_id, create_user_id,corporate_sme, utility, meter_no, profile_type, meter_type, status, created', 'required'),
            array('site_id, create_user_id, utility, profile_type, meter_type, status', 'numerical', 'integerOnly' => true),
            array('meter_no, available_capability', 'length', 'max' => 255),
            array('consumption', 'file', 'allowEmpty' => true, 'types' => 'csv, xls, xslx'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, site_id, csv_file,consumption, corporate_sme, create_user_id, utility, meter_no, profile_type, meter_type, available_capability, status, created', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'createUser' => array(self::BELONGS_TO, 'Users', 'create_user_id'),
            'site' => array(self::BELONGS_TO, 'CustomerSite', 'site_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'site_id' => 'Site',
            'create_user_id' => 'Create User',
            'corporate_sme' => 'Corporate SME',
            'utility' => 'Utility',
            'meter_no' => 'Meter No',
            'profile_type' => 'Profile Type',
            'meter_type' => 'Meter Type',
            'available_capability' => 'Available Capability',
            'status' => 'Status',
            'created' => 'Created',
            'consumption' => 'Consumption Data'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search($sid = null) {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        if ($sid)
            $criteria->compare('site_id', $sid);
        else
            $criteria->compare('site_id', $this->site_id);
        $criteria->compare('site_id', $this->site_id);
        $criteria->compare('create_user_id', $this->create_user_id);
        $criteria->compare('utility', $this->utility);
        $criteria->compare('meter_no', $this->meter_no, true);
        $criteria->compare('profile_type', $this->profile_type);
        $criteria->compare('meter_type', $this->meter_type);
        $criteria->compare('available_capability', $this->available_capability, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('created', $this->created, true);
        $criteria->compare('consumption', $this->consumption, true);


        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CustomerSiteMeter the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public static function getMeterType($id = null) {
        $arr = array(
            '1' => 'Day',
            '2' => 'Day/Night',
            '3' => 'Day/Night/EWE',
            '4' => 'Day/EWE',
            '5' => '1 rate STOD',
            '6' => '2 rate STOD',
            '7' => '2 rate STOD',
            '8' => '3 rate STOD'
        );
        if ($id) {
            return $arr[$id];
        }
        return $arr;
    }

    public static function getProfileType($id = null) {
        $arr = array(
            '1' => 'Type 1', '2' => 'Type2'
        );
        if ($id) {
            return $arr[$id];
        }
        return $arr;
    }

    public function getDownloadLink() {
        if ($this->consumption) {
            echo CHtml::link('<i class="fa fa-download">download</i>', array('/admin/users/download', 'file' => $this->consumption), array('class' => 'btn btn-primary'));
        } else {
            echo CHtml::link('<i class="fa fa-upload">Upload</i>', array('#'), array('class' => 'upload_class', 'id' => $this->id), array('class' => 'btn btn-primary'));
        }
    }

    public function searchMeters($cid) {

        $criteria = new CDbCriteria;

//$criteria->join = 'LEFT JOIN tbl_customer_site a ON t.id = a.site_id = $sid'; 
//$aRecords = CustomerSite::model()->findAll($criteria);


        $criteria->compare('id', $this->id);
        $criteria->compare('site_id', $this->site_id);
        $criteria->compare('site_id', $this->site_id);
        $criteria->compare('create_user_id', $this->create_user_id);
        $criteria->compare('utility', $this->utility);
        
        $criteria->with = array('site');
        $criteria->addCondition('site.customer_id = ' . $cid);
        
        
        $criteria->compare('meter_no', $this->meter_no, true);
        $criteria->compare('profile_type', $this->profile_type);
        $criteria->compare('meter_type', $this->meter_type);
        $criteria->compare('available_capability', $this->available_capability, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('created', $this->created, true);
        $criteria->compare('consumption', $this->consumption, true);


        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}

?>