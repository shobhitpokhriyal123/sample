<?php

/**
 * This is the model class for table "tbl_sites".
 *
 * The followings are the available columns in table 'tbl_sites':
 * @property integer $id
 * @property integer $company_id
 * @property integer $create_user_id
 * @property string $site_name
 * @property string $address
 * @property string $telephone
 * @property string $postcode
 * @property integer $no_of_emp
 * @property integer $no_of_years
 */
class Sites extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_sites';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('company_id, create_user_id, site_name, address, telephone, postcode, no_of_emp, no_of_years', 'required'),
			array('company_id, create_user_id, no_of_emp, no_of_years', 'numerical', 'integerOnly'=>true),
			array('site_name, address, telephone, postcode', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, company_id, create_user_id, site_name, address, telephone, postcode, no_of_emp, no_of_years', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'company_id' => 'Company',
			'create_user_id' => 'Create User',
			'site_name' => 'Site Name',
			'address' => 'Address',
			'telephone' => 'Telephone',
			'postcode' => 'Postcode',
			'no_of_emp' => 'No Of Emp',
			'no_of_years' => 'No Of Years',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('company_id',$this->company_id);
		$criteria->compare('create_user_id',$this->create_user_id);
		$criteria->compare('site_name',$this->site_name,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('telephone',$this->telephone,true);
		$criteria->compare('postcode',$this->postcode,true);
		$criteria->compare('no_of_emp',$this->no_of_emp);
		$criteria->compare('no_of_years',$this->no_of_years);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Sites the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
