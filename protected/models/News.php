<?php

/**
 * This is the model class for table "tbl_news".
 *
 * The followings are the available columns in table 'tbl_news':
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property integer $is_publish
 * @property string $image_file
 * @property integer $active
 * @property string $created
 */
class News extends GxActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tbl_news';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('title, heading, agent_agency, supplier_id,  description', 'required'),
            array('is_publish, active', 'numerical', 'integerOnly' => true),
            array('title, image_file', 'length', 'max' => 255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, title, heading,agent_agency, supplier_id, description, is_publish, image_file, active, created', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'supplier' => array(self::BELONGS_TO, 'Users', 'supplier_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'is_publish' => 'Is Publish',
            'image_file' => 'Image',
            'active' => 'Active',
            'created' => 'Created',
            'supplier_id' => 'Supplier',
            'agent_agency' => 'Agent/Agency',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $id = Yii::app()->user->id;

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        if (!Yii::app()->user->isAdmin) {

            $criteria->condition = 'FIND_IN_SET( "' . $id . '" ,agent_agency)';
        } else {
            $criteria->compare('agent_agency', $this->agent_agency);
        }
        $criteria->compare('title', $this->title, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('is_publish', $this->is_publish);
        $criteria->compare('image_file', $this->image_file, true);
        $criteria->compare('active', $this->active);
        $criteria->compare('created', $this->created, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return News the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public static function getAgentNames($agents) {
        $criteria = new CDbCriteria;
        $agents = explode(",", $agents);
        $criteria->addInCondition('id', $agents);
        $agents = Users::model()->findAll($criteria);
        $html = '';
        // echo '<pre>'; print_r($agents); die;
        if ($agents)
            foreach ($agents as $agent) {
                $html .= $agent->username . ", ";
            }
        return rtrim($html, ", ");
    }

    public  function getLink() {
        return '<a href="'.Yii::app()->createAbsoluteUrl("admin/news/view",array("id"=>$this->id)).'" class="news_link" id="news_link_'.$this->id.'">'.$this->title.'</a>';
    }

}
