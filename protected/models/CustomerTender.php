<?php

/**
 * This is the model class for table "tbl_customer_tender".
 *
 * The followings are the available columns in table 'tbl_customer_tender':
 * @property integer $id
 * @property integer $contract_id
 * @property integer $create_user_id
 * @property integer $contract_duration
 * @property string $supplier_ids
 * @property integer $bespoke_duration
 * @property integer $commission_discount
 * @property string $tender_return_date
 * @property string $business_name
 * @property string $com_reg
 * @property string $commodity_type
 * @property integer $site_no
 * @property integer $uplift
 * @property string $term_length
 * @property string $fixed_meter
 * @property string $contract_type
 * @property string $payment
 * @property string $energy
 * @property string $supplier_pref
 * @property string $comments
 * @property string $created
 */
class CustomerTender extends CActiveRecord
{
    public $citysFilter;
    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_customer_tender';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('contract_id, create_user_id, contract_duration, supplier_ids, bespoke_duration, commission_discount, tender_return_date, business_name, com_reg, commodity_type, site_no, uplift, term_length, fixed_meter, contract_type, payment, energy, supplier_pref, comments, created', 'required'),
			array('contract_id, create_user_id, contract_duration, bespoke_duration, commission_discount, site_no, uplift', 'numerical', 'integerOnly'=>true),
			array('supplier_ids', 'length', 'max'=>64),
			array('business_name, term_length, fixed_meter', 'length', 'max'=>100),
			array('com_reg, contract_type, payment, energy', 'length', 'max'=>50),
			array('supplier_pref', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, contract_id, create_user_id, contract_duration, supplier_ids, bespoke_duration, commission_discount, tender_return_date, business_name, com_reg, commodity_type, site_no, uplift, term_length, fixed_meter, contract_type, payment, energy, supplier_pref, comments, created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'contract_id' => 'Contract',
			'create_user_id' => 'Create User',
			'contract_duration' => 'Contract Duration',
			'supplier_ids' => 'Supplier Ids',
			'bespoke_duration' => 'Bespoke Duration',
			'commission_discount' => 'Commission Discount',
			'tender_return_date' => 'Tender Return Date',
			'business_name' => 'Business Name',
			'com_reg' => 'Com Reg',
			'commodity_type' => 'Commodity Type',
			'site_no' => 'Site No',
			'uplift' => 'Uplift',
			'term_length' => 'Term Length',
			'fixed_meter' => 'Fixed Meter',
			'contract_type' => 'Contract Type',
			'payment' => 'Payment',
			'energy' => 'Energy',
			'supplier_pref' => 'Supplier Pref',
			'comments' => 'Comments',
			'created' => 'Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('contract_id',$this->contract_id);
		$criteria->compare('create_user_id',$this->create_user_id);
		$criteria->compare('contract_duration',$this->contract_duration);
		$criteria->compare('supplier_ids',$this->supplier_ids,true);
		$criteria->compare('bespoke_duration',$this->bespoke_duration);
		$criteria->compare('commission_discount',$this->commission_discount);
		$criteria->compare('tender_return_date',$this->tender_return_date,true);
		$criteria->compare('business_name',$this->business_name,true);
		$criteria->compare('com_reg',$this->com_reg,true);
		$criteria->compare('commodity_type',$this->commodity_type,true);
		$criteria->compare('site_no',$this->site_no);
		$criteria->compare('uplift',$this->uplift);
		$criteria->compare('term_length',$this->term_length,true);
		$criteria->compare('fixed_meter',$this->fixed_meter,true);
		$criteria->compare('contract_type',$this->contract_type,true);
		$criteria->compare('payment',$this->payment,true);
		$criteria->compare('energy',$this->energy,true);
		$criteria->compare('supplier_pref',$this->supplier_pref,true);
		$criteria->compare('comments',$this->comments,true);
		$criteria->compare('created',$this->created,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CustomerTender the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
