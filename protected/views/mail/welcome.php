<?php include 'header.php';?>

<p>
	Dear <b><?php echo $model->full_name;?> </b>
</p>
<br />

<p>Your account is activated now. You can enjoy all features of Be
	Employable educational network. If you are searching for a job , dont
	forget to complete your resume.</p>

<?php echo CHtml::link('Your Account',Yii::app()->createAbsoluteUrl('user/home'),array('class'=>"btn btn-primary centred"))?>

<?php include 'footer.php';?>
