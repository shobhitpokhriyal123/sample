
<?php include 'header.php'; ?>
<div class="abc" style="background: white; color: #000; font-family: sans-serif; font-size: 13px;">
    <p style="color: #000; font-family: sans-serif; font-size: 40px;">
        <b><?php echo Yii::app()->user->fullname; ?> </b> 
    </p>
    <br><br>



    <br>
    <p><b>Great News</b> The top three energy contracts available for your business are below.</p>

    <table style="width:100%; font-size: 15px;">

        <tr>
            <th>Supplier</th>
            <th>Product</th>
            <th>Standing Charge</th>
            <th>Anual Cost</th>
            <th>Unit Charge</th>
            <th>Payment Method</th>
            <th>Ref. No</th>
            <th>Duration</th>
        </tr>
        <?php
        if ($model) {
            foreach ($model as $model1) {
                ?>
                <tr style="border: 2px solid #0064cd ; min-height: 40px; font-size: 14px;">
                    <td style="border: 2px #0064cd;"><?php echo $model1->c_supplier->fullname; ?></td>
                    <td><?php echo $model1->priceModel->productName->product_name; ?></td>
                    <td style="box-shadow: #0064cd;"><?php echo $model1->standing_charge; ?></td>
                    <td style="box-shadow: #0064cd;"><?php echo $model1->annual_cost; ?></td>
                    <td style="box-shadow: #0064cd;"><?php echo $model1->priceModel->unit_charge; ?></td>
                    <td style="box-shadow: #0064cd;"><?php echo ($model1->priceModel->productName->direct_dabit) ? "Direct Dabit" : "Cash"; ?></td>
                    <td style="box-shadow: #0064cd;">#<?php echo 'QUOTE-'.$model1->id  ?></td>
                    <td style="box-shadow: #0064cd;"><?php echo ($model1->priceModel->contract_length) ? $model1->priceModel->contract_length : $model1->priceModel->term; ?></td>
                </tr>
                <?php
            }
        } else {
            ?>
            <tr>
                <td colspan="4">No record(s) found...!</td>
            </tr>
        <?php } ?>
    </table> 


    <br />
    <br />
</div>
<?php include 'footer.php'; ?>
