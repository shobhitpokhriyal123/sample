<?php include 'header.php';?>

<p>
	Dear <b><?php echo $model->first_name;?> </b>
</p>
<?php $link = $model->getActivationUrl('recover');?>
<br />
<p>
	We have received your request for password recovery. Below is the link
	to set a new password. <br /> <br /> <a class="btn btn-primary centred"
		href="<?php echo $link;?>">Set New Password</a>
</p>

<p>
	If above link isn't working, please copy and paste it directly in you
	browser's URL field to get started.<br />
	<?php echo $link;?>
</p>

	<?php include 'footer.php';?>