<?php include 'header.php'; ?>

<p>Dear Mr. <?php echo $model2->contact_name; ?></p>

<p>We would like to inform you that your recent application for a supply agreement has been validated and will now be forwarded.</p>
<p>It's important that if any of the details below are incorect you contact us immediatly on (7415212833)</p>
<div class="row">
    <div class="col-md-4"><b>Customer:</b></div>
    <div class="col-md-4"><?php echo Yii::app()->user->fullname; ?></div>
    <div class="col-md-4"><b>MPAN/MPR:</b></div>
    <div class="col-md-4">7415212833</div>

    <div class="col-md-4"><b>Product:</b></div>
    <div class="col-md-4"><?php echo $model->price->productName->product_name; ?></div>

    <div class="col-md-4"><b>Current Supplier:</b></div>
    <div class="col-md-4"><?php echo $model->c_supplier->fullname; ?></div>


    <div class="col-md-4"><b>New Supplier:</b></div>
    <div class="col-md-4"><?php echo $model->n_supplier->fullname; ?></div>
    
    
   



    <div class="col-md-4"><b>Consumption:</b></div>
    <div class="col-md-4"><?php echo $model->eac ?></div>

    <div class="col-md-4"><b>Contract Period:</b></div>
    <div class="col-md-4"><?php $model->contract_period; ?></div>
    <?php
    $quote = Quote::model()->findByAttributes(array('price_id' => $model->price_id, 'current_supplier' => $model->current_supplier));
    ?>
    <div class="col-md-4"><b>Standing Charge:</b></div>
    <div class="col-md-4"><?php echo ($quote) ? $quote->standing_charge : "N/A"; ?></div>
    
     <div class="col-md-4"><b>Quote Refference No:</b></div>
    <div class="col-md-4">#<?php echo 'QUOTE-'.$quote->id; ?></div>
    
    
    <div class="col-md-4"><b>Unit Charge:</b></div>
    <div class="col-md-4"><?php echo ($quote) ? $quote->priceModel->unit_charge : "N/A"; ?></div>

    <div class="col-md-4"><b>Annual Cost:</b></div>
    <div class="col-md-4"><?php echo ($quote) ? $quote->annual_cost : "N/A"; ?></div>

    <div class="col-md-4"><b>Start date :</b></div>
    <div class="col-md-4"><?php echo $model->estimate_date ?></div>

    <div class="col-md-4"><b>End Date:</b></div>
    <div class="col-md-4"><?php echo $model->contract_date ?></div>

    <div class="col-md-4"><b>Live Date:</b></div>
    <div class="col-md-4"><?php echo $model->live_date ?></div>

</div>



<br />
<br />
<?php include 'footer.php'; ?>
