<section id="contact">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                     
                        <?php if (Yii::app()->user->hasFlash('contact')): ?>
                            <div class="flash-success">
                                <?php echo Yii::app()->user->getFlash('contact'); ?>
                            </div>
                        <?php else: ?>
                        </div>

                    </div>



                    <div class="card-header">
                        <div class="card-title">
                            <strong>Contact Us</strong>
                            <p>
                                <small>If you have business inquiries or other questions, please fill out the following form to contact us. Thank you.</small>
                            </p>
                        </div>

                    </div>
                    <div class="card-body">


                        <?php
                        $form = $this->beginWidget('CActiveForm', array(
                            'id' => 'contact-form',
                            'enableClientValidation' => true,
                            'clientOptions' => array(
                                'validateOnSubmit' => true,
                            ),
                        ));
                        ?>

                        <p class="note">Fields with <span class="required">*</span> are required.</p>

                        <?php echo $form->errorSummary($model); ?>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <?php echo $form->labelEx($model, 'name'); ?>
                                    <?php echo $form->textField($model, 'name'); ?>
                                    <?php echo $form->error($model, 'name'); ?>
                                </div>

                                <div class="form-group">
                                    <?php echo $form->labelEx($model, 'email'); ?>
                                    <?php echo $form->textField($model, 'email'); ?>
                                    <?php echo $form->error($model, 'email'); ?>
                                </div>

                                <div class="form-group">
                                    <?php echo $form->labelEx($model, 'subject'); ?>
                                    <?php echo $form->textField($model, 'subject', array('size' => 60, 'maxlength' => 128)); ?>
                                    <?php echo $form->error($model, 'subject'); ?>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <?php echo $form->labelEx($model, 'body'); ?>
                                    <?php echo $form->textArea($model, 'body', array('class' => 'form-control')); ?>
                                    <?php echo $form->error($model, 'body'); ?>
                                </div>

                                <?php if (CCaptcha::checkRequirements()): ?>
                                    <div class="form-group">
                                        <?php echo $form->labelEx($model, 'verifyCode'); ?>
                                        <div class="VCODE">
                                            <?php $this->widget('CCaptcha'); ?>
                                            <?php echo $form->textField($model, 'verifyCode'); ?>
                                        </div>
                                        <div class="hint">Please enter the letters as they are shown in the image above.
                                            <br/>Letters are not case-sensitive.</div>
                                        <?php echo $form->error($model, 'verifyCode'); ?>
                                    </div>
                                <?php endif; ?>
                            </div>

                        </div>



                        <div class="form-group buttons">
                            <?php echo CHtml::submitButton('Submit', array('class' => 'btn btn-success longBtn')); ?>
                        </div>

                        <?php $this->endWidget(); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
/* simulate a click on "refresh captcha" for GET requests */
if (!Yii::app()->request->isPostRequest) {
    Yii::app()->clientScript->registerScript(
            'initCaptcha', '$("#yw0_button").trigger("click");', CClientScript::POS_READY
    );
}
