<section id="manage_productss">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">

                    <div class="card-title">
                        <strong>Errors</strong>
                    </div>


                </div>
                <div class="card-body">
                    <?php
                    /* @var $this SiteController */
                    /* @var $error array */

                    $this->pageTitle = Yii::app()->name . ' - Error';
                    $this->breadcrumbs = array(
                        'Error',
                    );
                    ?>

                    <h2>Error <?php echo $code; ?></h2>

                    <div class="error">
                        <?php echo CHtml::encode($message); ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>