<style>
    #login_background {
        background: url('<?php echo Yii::app()->baseUrl; ?>/images/background.jpg') no-repeat center center;
        background-size: cover;
        position: absolute;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        z-index: -1;
    }
</style>
<section id="login">

    <div class="container">

        <div class="row">
            <div id="login_background">

            </div>

            <div class="col-sm-12" id="login_wrapper">
                <div class="row">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <?php if (Yii::app()->user->hasFlash('error')): ?>
                        <div class="danger">
                            <?php echo Yii::app()->user->getFlash('error'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="col-sm-5" id="login_text">
                        <?php
                        $site = Sitetext::model()->findByPk(1);
                        //  echo '<pre>'; print_r($site);
                        ?>

                        <h3><?php echo $site->title ?></h3>
                        <?php echo $site->description; ?>
                    </div>
                    <div class="col-sm-7" id="login_form">
                
                        <?php
                        $form = $this->beginWidget('CActiveForm', array(
                            'id' => 'login-form',
                            'enableClientValidation' => true,
                            'clientOptions' => array(
                                'validateOnSubmit' => true,
                            ),
                            'htmlOptions' => array('class' => '')
                        ));
                        ?>

                        <div class="form-group">
                            <div class="col-sm-6">
                                <?php echo $form->labelEx($model, 'username'); ?>
                                <?php echo $form->textField($model, 'username', array('class' => 'form-control')); ?>
                                <?php echo $form->error($model, 'username'); ?>  
                            </div>
                            <div class="col-sm-6">
                                <?php echo $form->labelEx($model, 'password'); ?>
                                <?php echo $form->passwordField($model, 'password', array('class' => 'form-control')); ?>
                                <?php echo $form->error($model, 'password'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12" style="padding-top:10px; padding-bottom:5px;">
                                <?php // echo $form->label($model, 'rememberMe', array('class'=>'checkbox'));  ?>
                                <?php echo $form->checkBox($model, 'rememberMe'); ?> Remember me
                                <?php echo $form->error($model, 'rememberMe'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <?php echo CHtml::submitButton('Login', array("class" => 'btn btn-success btn-block')); ?>
                                <br>
                                <a id="fgt_pwd" href="" class="btn-link">Forgotton your username / password</a>
                            </div>
                        </div>
                        <?php $this->endWidget(); ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

<div id="forgot_password" class="modal fade" role="dialog">
    <div class="modal-dialog" id="html_content">

    </div>

</div>

<script>
    $(document).ready(function () {
        $('#fgt_pwd').click(function (e) {
            e.preventDefault();
            var url = '<?php echo Yii::app()->createAbsoluteUrl("users/forgot") ?>';
            $("#forgot_password").modal();
            // 
            $.ajax({
                url: url,
                type: 'POST',
                success: function (res) {
                    $("#html_content").html(res);
                }
            });
        });
    });
</script>
