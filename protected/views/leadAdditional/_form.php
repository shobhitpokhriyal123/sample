<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'lead-additional-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'site_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'create_user_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tel_provider',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'tel_s_date',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tel_e_date',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tel_ac_no',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'tel_ac_pwd',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'broad_provider',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'broad_s_date',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'broad_e_date',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'broad_ac_no',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'broad_ac_pwd',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'tv_provider',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'tv_s_date',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tv_e_date',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tv_ac_no',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'tv_ac_pwd',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'water_provider',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'water_s_date',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'water_e_date',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'water_ac_no',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'water_ac_pwd',array('class'=>'span5','maxlength'=>255)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
