<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('site_id')); ?>:</b>
	<?php echo CHtml::encode($data->site_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_user_id')); ?>:</b>
	<?php echo CHtml::encode($data->create_user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tel_provider')); ?>:</b>
	<?php echo CHtml::encode($data->tel_provider); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tel_s_date')); ?>:</b>
	<?php echo CHtml::encode($data->tel_s_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tel_e_date')); ?>:</b>
	<?php echo CHtml::encode($data->tel_e_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tel_ac_no')); ?>:</b>
	<?php echo CHtml::encode($data->tel_ac_no); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('tel_ac_pwd')); ?>:</b>
	<?php echo CHtml::encode($data->tel_ac_pwd); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('broad_provider')); ?>:</b>
	<?php echo CHtml::encode($data->broad_provider); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('broad_s_date')); ?>:</b>
	<?php echo CHtml::encode($data->broad_s_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('broad_e_date')); ?>:</b>
	<?php echo CHtml::encode($data->broad_e_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('broad_ac_no')); ?>:</b>
	<?php echo CHtml::encode($data->broad_ac_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('broad_ac_pwd')); ?>:</b>
	<?php echo CHtml::encode($data->broad_ac_pwd); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tv_provider')); ?>:</b>
	<?php echo CHtml::encode($data->tv_provider); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tv_s_date')); ?>:</b>
	<?php echo CHtml::encode($data->tv_s_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tv_e_date')); ?>:</b>
	<?php echo CHtml::encode($data->tv_e_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tv_ac_no')); ?>:</b>
	<?php echo CHtml::encode($data->tv_ac_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tv_ac_pwd')); ?>:</b>
	<?php echo CHtml::encode($data->tv_ac_pwd); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('water_provider')); ?>:</b>
	<?php echo CHtml::encode($data->water_provider); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('water_s_date')); ?>:</b>
	<?php echo CHtml::encode($data->water_s_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('water_e_date')); ?>:</b>
	<?php echo CHtml::encode($data->water_e_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('water_ac_no')); ?>:</b>
	<?php echo CHtml::encode($data->water_ac_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('water_ac_pwd')); ?>:</b>
	<?php echo CHtml::encode($data->water_ac_pwd); ?>
	<br />

	*/ ?>

</div>