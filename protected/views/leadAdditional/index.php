<?php
$this->breadcrumbs=array(
	'Lead Additionals',
);

$this->menu=array(
	array('label'=>'Create LeadAdditional','url'=>array('create')),
	array('label'=>'Manage LeadAdditional','url'=>array('admin')),
);
?>

<h1>Lead Additionals</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
