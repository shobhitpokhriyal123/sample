<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'site_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'create_user_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tel_provider',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'tel_s_date',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tel_e_date',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tel_ac_no',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'tel_ac_pwd',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'broad_provider',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'broad_s_date',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'broad_e_date',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'broad_ac_no',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'broad_ac_pwd',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'tv_provider',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'tv_s_date',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tv_e_date',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tv_ac_no',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'tv_ac_pwd',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'water_provider',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'water_s_date',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'water_e_date',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'water_ac_no',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'water_ac_pwd',array('class'=>'span5','maxlength'=>255)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
