<?php
$this->breadcrumbs=array(
	'Lead Additionals'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List LeadAdditional','url'=>array('index')),
	array('label'=>'Manage LeadAdditional','url'=>array('admin')),
);
?>

<h1>Create LeadAdditional</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>