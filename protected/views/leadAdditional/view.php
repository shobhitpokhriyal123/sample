<?php
$this->breadcrumbs=array(
	'Lead Additionals'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List LeadAdditional','url'=>array('index')),
	array('label'=>'Create LeadAdditional','url'=>array('create')),
	array('label'=>'Update LeadAdditional','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete LeadAdditional','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage LeadAdditional','url'=>array('admin')),
);
?>

<h1>View LeadAdditional #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'site_id',
		'create_user_id',
		'tel_provider',
		'tel_s_date',
		'tel_e_date',
		'tel_ac_no',
		'tel_ac_pwd',
		'broad_provider',
		'broad_s_date',
		'broad_e_date',
		'broad_ac_no',
		'broad_ac_pwd',
		'tv_provider',
		'tv_s_date',
		'tv_e_date',
		'tv_ac_no',
		'tv_ac_pwd',
		'water_provider',
		'water_s_date',
		'water_e_date',
		'water_ac_no',
		'water_ac_pwd',
	),
)); ?>
