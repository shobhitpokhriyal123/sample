<?php
$this->breadcrumbs=array(
	'Lead Additionals'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List LeadAdditional','url'=>array('index')),
	array('label'=>'Create LeadAdditional','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('lead-additional-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Lead Additionals</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'lead-additional-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'site_id',
		'create_user_id',
		'tel_provider',
		'tel_s_date',
		'tel_e_date',
		/*
		'tel_ac_no',
		'tel_ac_pwd',
		'broad_provider',
		'broad_s_date',
		'broad_e_date',
		'broad_ac_no',
		'broad_ac_pwd',
		'tv_provider',
		'tv_s_date',
		'tv_e_date',
		'tv_ac_no',
		'tv_ac_pwd',
		'water_provider',
		'water_s_date',
		'water_e_date',
		'water_ac_no',
		'water_ac_pwd',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
