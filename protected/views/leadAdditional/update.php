<?php
$this->breadcrumbs=array(
	'Lead Additionals'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List LeadAdditional','url'=>array('index')),
	array('label'=>'Create LeadAdditional','url'=>array('create')),
	array('label'=>'View LeadAdditional','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage LeadAdditional','url'=>array('admin')),
);
?>

<h1>Update LeadAdditional <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>