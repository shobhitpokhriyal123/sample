<section id="manage_region">
    <div class="container">
        <div class="row">
            <?php
            $this->breadcrumbs = array(
                'Regions' => array('index'),
                'Manage',
            );

            $this->menu = array(
                array('label' => 'List Region', 'url' => array('index')),
                array('label' => 'Create Region', 'url' => array('create')),
            );
            ?>
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
 
                        <strong><img src="<?php echo Yii::app()->createAbsoluteUrl('admin/product/download/file/' . Yii::app()->user->image); ?>" class="img-thumbnail" width="50px" style="margin-bottom:10px;">Edit User Profile</strong>
                    </div>
                    <?php
//                    if (Yii::app()->user->isAdmin) {
//                        echo CHtml::link('Add Region', array('/admin/region/create'), array("class" => 'btn btn-success'));
//                    }
                    ?>
                </div>
                <div class="card-body"><?php
                    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                        'id' => 'users-form',
                        'enableAjaxValidation' => false,
                        'enableClientValidation' => false,
                        'clientOptions' => array(
                            'validateOnSubmit' => true,
                        ),
                        'htmlOptions' => array('enctype' => 'multipart/form-data'),
                    ));
                    ?>

                    <p class="help-block">Fields with <span class="required">*</span> are required.</p>

                    <?php //echo $form->errorSummary($model);  ?>

                    <div class="row">
                        <div class="col-sm-6">
                            <?php if (Yii::app()->user->hasFlash('success')): ?>

                                <div class="flash-success">
                                    <?php echo Yii::app()->user->getFlash('success'); ?>
                                </div>
<?php
//    foreach(Yii::app()->user->getFlashes() as $key => $message) {
//        echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
//    }
?>
                            <?php endif; ?>

                            <div class="control-group">
                                <?php echo $form->textFieldRow($model, 'username', array('class' => 'span5', 'maxlength' => 255)); ?>
                                <?php echo $form->textFieldRow($model, 'fullname', array('class' => 'span5', 'maxlength' => 255)); ?>
                                <?php echo $form->textFieldRow($model, 'email', array('class' => 'span5', 'maxlength' => 255)); ?>
                                <?php //echo $form->textFieldRow($model, 'mobile', array('class' => 'span12', 'maxlength' => 255));  ?>

                                <?php echo $form->passwordFieldRow($model, 'password', array('class' => 'span12', 'maxlength' => 255)); ?>
                                <?php echo $form->passwordFieldRow($model, 'confirm_password', array('class' => 'span5', 'maxlength' => 255)); ?>
                                
 <?php  echo $form->fileFieldRow($model, 'image', array('class' => 'span12 us_image'));  ?>
                                
                                
                            </div>

                        </div>
                    </div>

                    <div class="control-group">
                        <div class="form-actions">
                            <?php
                            $this->widget('bootstrap.widgets.TbButton', array(
                                'buttonType' => 'submit',
                                'type' => 'primary',
                                'label' => $model->isNewRecord ? 'Create' : 'Save',
                            ));
                            ?>
                        </div>
                    </div>


                    <?php $this->endWidget(); ?>
                </div>
            </div>
        </div>
    </div>
</section>

<style>
    .us_image
    {
        margin-bottom: 10px;
        
    }
    </style>
    
    <script>
//        $(document).ready(function()
//  {
//    $("#Users_password").val("");
//    $("#Users_confirm_password").val("");
//  }); 
        </script>