<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'users-form',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php //echo $form->errorSummary($model);  ?>
<div class="control-group">
    <div class="span4">
        <?php echo $form->textFieldRow($model, 'emp_code', array('class' => 'span12', 'maxlength' => 255));
        ?>
    </div>
    <div class="span4">
        <?php echo $form->textFieldRow($model, 'fullname', array('class' => 'span12', 'maxlength' => 255)); ?>
    </div>
    <div class="span4">
        <?php echo $form->textFieldRow($model, 'email', array('class' => 'span12', 'maxlength' => 255)); ?>
    </div>
</div>
<div class="control-group">
    <div class="span4">
        <?php echo $form->textFieldRow($model, 'username', array('class' => 'span12', 'maxlength' => 255)); ?>
    </div>
    <div class="span4">
        <?php echo $form->passwordFieldRow($model, 'password', array('class' => 'span12', 'maxlength' => 255)); ?>
    </div>
    <div class="span4">
        <?php echo $form->textFieldRow($model, 'mobile', array('class' => 'span12', 'maxlength' => 255)); ?>
    </div>

</div>
<div class="control-group">
    <div class="span4">
        <?php echo $form->textFieldRow($model, 'joining_date', array('class' => 'span12', 'maxlength' => 255));
        ?>
    </div>
    <div class="span4">
        <?php echo $form->textFieldRow($model, 'designation', array('class' => 'span12', 'maxlength' => 255));
        ?>
    </div>
    <div class="span4">
        <?php echo $form->fileFieldRow($model, 'image', array('class' => 'span12')); ?>
    </div>

</div>
<div class="clearfix"></div>

<div class="control-group">
    <div class="span4">
        <?php echo $form->textFieldRow($model, 'last_company', array('class' => 'span12', 'maxlength' => 255)); ?>
    </div>
    <div class="span4">
        <?php echo $form->textAreaRow($model, 'current_address', array('class' => 'span12', 'maxlength' => 255)); ?>
    </div>

    <div class="span4">
        <?php echo $form->textAreaRow($model, 'permanent_address', array('class' => 'span12', 'maxlength' => 255)); ?>
    </div>
</div>
<div class="control-group">
    <?php if (Yii::app()->user->isHR || Yii::app()->user->isAdmin) { ?>
        <?php echo $form->dropdownListRow($model, 'role_id', CHtml::listData(Role::getRoles(), 'id', 'title'), array('class' => 'span3', 'empty' => '(Assign Employee Role)')); ?>
    <?php } ?>
</div>

<hr>
<div class="control-group">


    <div class="form-actions">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type' => 'primary',
            'label' => $model->isNewRecord ? 'Create' : 'Save',
        ));
        ?>
    </div>
</div>


<?php $this->endWidget(); ?>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker3.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
<script>
    $('#Users_joining_date').datepicker({
        format: 'yyyy-mm-dd'
    });
</script>
