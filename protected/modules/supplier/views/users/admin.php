<div class="row-fluid">
    <div class="span12 well">

        <h1>Manage Users</h1>
        <div class="right">


            <?php
//            if (Yii::app()->user->isAdmin || Yii::app()->user->isHR) {
             //   echo CHtml::link('New User', array('/admin/users/create'), array("class" => 'btn btn-success'));
          //  }
            ?>
        </div>
        <br>
        <?php
        $this->widget('bootstrap.widgets.TbGridView', array(
            'id' => 'users-grid',
            'dataProvider' => $model->search(),
            'filter' => $model,
            'columns' => array(
                'emp_code',
                'fullname',
                'email',
                'mobile',
                'username',
                'role_id' => array(
                    'filter' => CHtml::listData(Role::model()->getRoles(), 'id', 'title'),
                    'name' => 'role_id',
                    'id' => 'role_id',
                    'value' => '$data->rolename->title'
                ),
                'active' => array(
                    'filter' => $model->getStatus(),
                    'name' => 'active',
                    'id' => 'active',
                    'value' => '$data->getStatus($data->active)'
                ),
                //  'password',
                //'role_id'=>$model->role,
                /*
                  'image',
                  'role_id',
                  ,
                  'created',
                 */
                array(
                    'class' => 'bootstrap.widgets.TbButtonColumn',
                ),
            ),
        ));
        ?>
    </div>
</div>
