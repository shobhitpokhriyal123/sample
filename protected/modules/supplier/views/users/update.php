<div class="row-fluid">
    <div class="span12 well">

        <h1>Update Users <?php echo $model->username; ?></h1>
        <div class="right">
            <?php
            if (Yii::app()->user->isAdmin || Yii::app()->user->isHR) {
                echo CHtml::link('Manage Users', array('/admin/users/admin'), array("class" => 'btn btn-success'));
            } else {
                echo CHtml::link('Profile', array('/admin/users/view'), array("class" => 'btn btn-success'));
            }
            ?>
        </div>
        <br>
        <br>

        <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
    </div>
</div>