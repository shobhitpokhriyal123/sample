<?php

class ProductController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'merge'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('populatedData', 'searchQuote', 'getMonthWiseData', 'changeStatus', 'download', 'tariff', 'filter', 'getUtilities'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('create', 'admin', 'delete', 'getCsvHeader', 'startDate', 'quote', 'getParams', 'searchEQuote', 'calculate', 'products', 'productType', 'update', 'csvMapping', 'csvForm', 'csvView'),
                'expression' => 'Yii::app()->user->getIsAdmin()',
            //  'users' => array('admin'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete', 'getCsvHeader', 'quote', 'getParams', 'searchEQuote', 'calculate'),
                'expression' => 'Yii::app()->user->getIsUser()',
            //  'users' => array('admin'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'quote', 'getParams', 'searchEQuote', 'calculate'),
                'expression' => 'Yii::app()->user->getIsSupplier()',
            //  'users' => array('admin'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete', 'getCsvHeader', 'quote', 'getParams', 'searchEQuote', 'calculate'),
                'expression' => 'Yii::app()->user->getIsAgency()',
            //  'users' => array('admin'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete', 'getCsvHeader', 'quote', 'getParams', 'searchEQuote', 'calculate'),
                'expression' => 'Yii::app()->user->getIsCustomer()',
            //  'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $model = $this->loadModel($id);
        // $price = new Price();
        // $dataProvider = $price->search($id);
        $price = new Price('search', array('id' => $id));
        $price->unsetAttributes();  // clear any default values
        if (isset($_GET['Price']))
            $price->attributes = $_GET['Price'];
        $this->render('view', array(
            'model' => $model,
            //'dataProvider' => $dataProvider,
            'price' => $price
        ));
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionCsvView($id) {


        $model = $this->loadModel($id);
        $file = $model->csv_file;
        $csv = $model->readCsvContents();
        $to_remove = array("0");
        $result = array_diff_key($csv, array_flip($to_remove));
//
        if (isset($_POST['Product'])) {
            $attrs = $_POST['current_attribute'];
            //echo '<pre>'; print_r($attrs); die;
            $attr = serialize($attrs);
            $model->columns_name = $attr;
            $model->csv_file = $file;
            //echo '<pre>'; print_r($result); die;
            $model->save(false);

            /////////////////////////////////////

            $field = Fields::model()->findByAttributes(array(
                'utility' => $model->utility,
                'corporate_sme' => $model->corporate_sme,
                'assigned_to' => $model->assigned_to,
            ));
            if (!$field) {
                $field = new Fields;
                $field->utility = $model->utility;
                $field->corporate_sme = $model->corporate_sme;
                $field->assigned_to = $model->assigned_to;
                // $field->product_id = $model->product_id;
                $field->fields = $model->columns_name;
                $field->save();
            }
            ////////////////////////////////////


            foreach ($result as $k => $attr) {
                $final = array_combine($attrs, $attr);
                $price = new Price;
                $price->setAttributes($final);
                $price->product_id = $model->id;
                if ($price->save()) {
                    Yii::app()->user->setFlash('success', "Price of the product saved successfully..!");
                } else {
                    Yii::app()->user->setFlash('error', "Something went wrong to save csv..!");
                }
            }
            $this->redirect(array('view', 'id' => $model->id));
        }
        $this->render('csvView', array(
            'model' => $model,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Product;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        $model->scenario = 'create';


        if (isset($_POST['Product'])) {

            $model->attributes = $_POST['Product'];
            $model->created = new CDbExpression('NOW()');
            $model->saveUploadedFile($model, 'csv_file');

            if ($model->save()) {
                ////////////////
                $field = Fields::model()->findByAttributes(array(
                    'utility' => $model->utility,
                    'corporate_sme' => $model->corporate_sme,
                    'assigned_to' => $model->assigned_to,
                ));
                if ($field) {
                    $model->columns_name = $field->fields;
                    $model->save();
                    //  $field->product_id = $model->id;
                    $field->save();
                    Price::saveCsv($field->fields, $model);
                    $this->redirect(array('view', 'id' => $model->id));
                } else {
                    //////////////
                    $this->redirect(array('csvView', 'id' => $model->id));
                }
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        if (isset($_POST['Product'])) {
            $file = $model->csv_file;
            $field = Fields::model()->findByAttributes(array(
                'utility' => $model->utility,
                'corporate_sme' => $model->corporate_sme,
                'assigned_to' => $model->assigned_to,
            ));

            if ($field && $field->utility != $_POST['Product']['utility'] || $field->corporate_sme != $_POST['Product']['corporate_sme']) {
                $field->utility = $_POST['Product']['utility'];
                $field->corporate_sme = $_POST['Product']['corporate_sme'];
                $field->save();
            }
            $model->attributes = $_POST['Product'];
            $model->csv_file = $file;
//            echo '<pre>';
//            print_r($model);
//            die;
            if ($model->save(false))
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            $model = $this->loadModel($id);
//            $model->save();
            // we only allow deletion via POST request
            /////////////////////
            $criteria = new CDbCriteria;
            $criteria->addCondition('product_id=' . $id);
            $prices = Price::model()->findAll($criteria);
            foreach ($prices as $price) {
                ContractDetail::model()->deleteAllByAttributes(array('price_id' => $price->id));
                Quote::model()->deleteAllByAttributes(array('price_id' => $price->id));
            }
            Price::model()->deleteAllByAttributes(array('product_id' => $id));
            /////////////////
            $criteria = new CDbCriteria;
            $criteria->addCondition('utility=' . $model->utility);
            $criteria->addCondition('corporate_sme=' . $model->corporate_sme);
            $criteria->addCondition('assigned_to=' . $model->assigned_to);
            Fields::model()->deleteAll($criteria);

            $model->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Product');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Product('search');

        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Product']))
            $model->attributes = $_GET['Product'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Product::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'product-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * 
     */
    public function actionCsvForm() {
        $model = new Product;

        $this->renderPartial("_csvform", array('model' => $model));
    }

    public function actionQuote() {
        $model = new Product;
        $products = Product::model()->findAll();
        foreach ($products as $product) {
            $csv = $product->csv_file;
            $days = $product->max_end_time + 1;
            $product->final_date = new CDbExpression("NOW() + INTERVAL $days DAY");
            //  $finaldate = date('Y-m-d', strtotime("+$product->max_end_time days", strtotime($product->final_date))); //date('Y-m-d', strtotime( $product->final_date));
            // $product->final_date = $finaldate;
            $product->csv_file = $csv;
            //echo '<pre>'; print_r($product); die;
            $product->save(FALSE);
        }
        // $model->scenario = 'qoute';
        $this->render('quote', array('model' => $model));
    }

    public function actionGetUtilities($id, $pid = null) {
        $selected = '';
        $product = '';
        if ($pid) {
            $product = Product::model()->findByPk($pid);
        }

        $utility = Type::model()->findAllByAttributes(array('parrent' => $id, 'status' => 1));
        // echo '<pre>'; print_r($utility); die;
        foreach ($utility as $utility) {
            if ($product && $product->utility == $utility->id)
                $selected = 'selected="selected"';
            else
                $selected = '';
            echo '<option ' . $selected . ' value="' . $utility->id . '">' . $utility->title . '</option>';
        }
        return;
    }

    public function actionCsvMapping() {
        $criteria = new CDbCriteria;
        $criteria->order = "id desc";
        $models = Product::model()->findAll($criteria);
        if (isset($_POST['current_attribute'])) {
            //    echo '<pre>'; print_r($_POST); 
            $current = $_POST['current_attribute'];
            $replaced_with = $_POST['mapped_attribute'];
            $utility = $_POST['p_utility'];
            $c_s = $_POST['p_corporate'];
            $user = $_POST['p_user'];
            $field = Fields::model()->findByAttributes(array('utility' => $utility, 'corporate_sme' => $c_s, 'assigned_to' => $user));
            $fields = unserialize($field->fields);
            //  echo '<pre>';  print_r($fields); 
            $index = array_search($current, $fields);
            $fields[$index] = $replaced_with;
            //  echo '<pre>'; print_r($fields);
            $fields = serialize($fields);
            $field->fields = $fields;
            //   echo $current .'  vvvv  '.$replaced_with ;

            $products = Product::model()->findAllByAttributes(array('utility' => $utility, 'corporate_sme' => $c_s, 'assigned_to' => $user));
            if ($field->save()) {
                foreach ($products as $product) {
                    if ($product->modelPrice) {
                        foreach ($product->modelPrice as $modelPrice) {
                            $curr = $modelPrice->$current;
                            $rep = $modelPrice->$replaced_with;
                            $modelPrice->$current = $rep;
                            $modelPrice->$replaced_with = $curr;
                            if (!$modelPrice->save()) {
                                echo '<pre>';
                                print_r($modelPrice->getErrors());
                                die;
                            }
                        }
                    }
                }
            } else {
                echo '<pre>';
                print_r($field->getErrors());
                die;
            }
        }

        $this->render('csvMapping', array('models' => $models));
    }

    public function actionPopulatedData($id) {
        //  $user = Users::model()->findByPk($id);
        if ($id) {
            $products = Product::model()->findAllByAttributes(array('assigned_to' => $id));
        } else {
            
        }
    }

    public function actionChangeStatus($id) {
        $product = Product::model()->findByPk($id);
        $file = $product->csv_file;
        $product->csv_file = $file;
        if ($product->status == 1) {
            $product->status = 0;
        } else {
            $product->status = 1;
        }

        //echo '<pre>';
        // print_r($product);
        if (!$product->save(false))
            print_r($product->getErrors());
        echo '1';
        die;
    }

    public function actionProducts($sid) {
        $model = Users::model()->findByPk($sid);
        $product = new Product('search', array('sid' => $sid));

        $product->unsetAttributes();  // clear any default values
        if (isset($_GET['Product']))
            $product->attributes = $_GET['Product'];
        //echo '<pre>'; print_r( $_GET); 

        $this->renderPartial('_partial_products', array('model' => $model, 'product' => $product));
    }

    /**
     * 
     * @param type $sid
     */
    public function actionProductType($sid) {
        $model = Users::model()->findByPk($sid);
        $this->renderPartial('_partial', array('model' => $model), false, true);
    }

    /**
     * 
     * @param type $number
     */
    public function actionSearchQuote($number = null) {
        // echo '<pre>'; print_r($_GET); die;
        if (isset($_GET['Product'])) {

            $model = new QuoteBaseHistory;
            //$_GET['QuoteBaseHistory'] =  $_GET['Product'];
            $model->attributes = $_GET['Product'];
            $model->create_user_id = Yii::app()->user->id;
            $model->type = "Gas";
            $model->created = new CDbExpression("NOW()");
            //echo '<pre>'; print_r($model); die;
            if (!$model->save()) {
                echo '<pre>';
                print_r($model->getErrors());
                die;
            }
        }
        if (isset($_GET['ss'])) {

            if (Yii::app()->session['ss'] == 1) {
                Yii::app()->session['ss'] = 0;
            } else {
                Yii::app()->session['ss'] = 1;
            }
            //echo Yii::app()->session['ss']; die;
        }
        $this->render('searchQuote');
    }

    /**
     * 
     * @param type $number
     */
    public function actionSearchEQuote($number = null) {
        //  echo '<pre>'; print_r($_GET); die;
        if (isset($_GET['Product'])) {

            $model = new QuoteBaseHistory;
            //$_GET['QuoteBaseHistory'] =  $_GET['Product'];
            $model->attributes = $_GET['Product'];
            $model->create_user_id = Yii::app()->user->id;
            $model->type = "Elc";
            $model->created = new CDbExpression("NOW()");
            if (!$model->save()) {
                echo '<pre>';
                print_r($model->getErrors());
                die;
            }
        }
        if (isset($_GET['ss'])) {
            if (Yii::app()->session['ss'] == 1) {
                Yii::app()->session['ss'] = 0;
            } else {
                Yii::app()->session['ss'] = 1;
            }
        }
        $this->render('searchEQuote');
    }

    public function actionGetParams($type) {
        if ($type == 'gas') {
            $model = new Product;
            $model->scenario = 'gasparams';
            $this->renderPartial('_gasparams', array('model' => $model), false, true);
        } else {
            $model = new Product;
            $model1 = new Price;
            $model->scenario = 'elecparams';

            $this->renderPartial('_elecparams', array('model' => $model), false, true);
        }
    }

    /**
     * 
     */
    public function actionCalculate($id) {
        $model = $this->loadModel($id);
        if (isset($_POST['Product'])) {
            $dist = $_POST['Product']['distributor'];
            $dm = $_POST['Product']['dm'];
            $prices = Price::model()->findAllByAttributes(array('product_id' => $id));
            foreach ($prices as $price) {
                if ($price->$dist != '') {
                    if ($dm == 1)
                        $price->$dist = ($price->$dist * 100);
                    else {
                        $price->$dist = ($price->$dist / 100);
                    }
                    $price->save();
                }
            }
            $this->redirect(array('admin'));
        }
        $field = Fields::model()->findByAttributes(array(
            'assigned_to' => $model->assigned_to,
            'utility' => $model->utility,
            'corporate_sme' => $model->corporate_sme
        ));
        if ($field) {
            $fields = $field->fields;
            $fields = unserialize($fields);
        } else {
            $fields = array();
        }
        $this->renderPartial('calculate', array('model' => $model, 'fields' => $fields), false, true);
    }

    public function actionStartDate() {
        $criteria = new CDbCriteria;
        $criteria->addCondition('start_date != ""');
        $criteria->group = 'product_id , start_date';
        $models = Price::model()->findAll($criteria);
        if (isset($_POST['Price'])) {
            $start_date = $_POST['Price']['start_from_date'];
            $end_date = $_POST['Price']['end_till_date'];
            $s_date = $_POST['Price']['start_date'];
            $prices = Price::model()->findAllByAttributes(array('start_date' => $s_date));
           // echo '<pre>'; print_r($prices); die;
            if ($prices) {
                foreach ($prices as $price) {
                    $price->start_from_date = $start_date;
                    $price->end_till_date = $end_date;
                    $price->save();
                }
                $this->redirect(array('startDate'));
            }
        }
        $this->render('startDate', array('models' => $models));
    }

}
