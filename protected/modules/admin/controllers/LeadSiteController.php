<?php

class LeadSiteController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('additional', 'saveAddiotional', 'addDropdown', 'deleteAdditional', 'customerMeter', 'getMeters', 'siteDropdown', 'updateSiteDropdown', 'create', 'update', 'siteupdate', 'updateSiteGrid', 'updateSiteContactGrid', 'updateElcGrid', 'updateGasGrid'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionAddDropdown($lead_id) {
        $sites = LeadSite::model()->findAllByAttributes(array('lead_id' => $lead_id));
        $html = '';
        if ($sites) {
            foreach ($sites as $site) {
                $html .= '<option value="' . $site->id . '" > ' . $site->site_name . '  </option>';
            }
        }
        return $html;
    }

    public function actionSaveAddiotional($id = null) {
        $arr = array('status' => 'NOK', "id" => "0", "next_form" => "LeadSiteAddress_site_id", "next_form1" => "LeadGasMeter_site_id", "next_form2" => '0');
        $additional = new LeadAdditional();
        if ($id) {
            $additional = LeadAdditional::model()->findByPk($id);
        }
        if (isset($_POST['LeadAdditional'])) {
            $additional->attributes = $_POST['LeadAdditional'];
            $additional->broad_s_date = $_POST['LeadAdditional']['broad_s_date'];
            $additional->tel_s_date = $_POST['LeadAdditional']['tel_s_date'];
            $additional->tel_e_date = $_POST['LeadAdditional']['tel_e_date'];
            $additional->broad_e_date = $_POST['LeadAdditional']['broad_e_date'];
            $additional->tv_s_date = $_POST['LeadAdditional']['tv_s_date'];
            $additional->tv_e_date = $_POST['LeadAdditional']['tv_e_date'];
            $additional->water_s_date = $_POST['LeadAdditional']['water_s_date'];
            $additional->water_e_date = $_POST['LeadAdditional']['water_e_date'];

            $additional->create_user_id = Yii::app()->user->id;
            if ($additional->save()) {
                $arr['status'] = "OK";
                $arr['id'] = $additional->site->lead_id;
            } else {
                $arr['error'] = $additional->getErrors();
            }
            echo CJSON::encode($arr);
        } else {
            $arr['error'] = "No data posted..!";
        }
        die;
    }

    public function actionAdditional($lead_id) {
        if (Yii::app()->request->isAjaxRequest) {
            $cs = Yii::app()->clientScript;
            $cs->scriptMap['jquery.js'] = false;
            $cs->scriptMap['jquery.min.js'] = false;
            $cs->scriptMap['bootstrap.js'] = false;
            $cs->scriptMap['bootstrap.min.js'] = false;
        }
        $sites = LeadSite::model()->findAllByAttributes(array('lead_id' => $lead_id));
        $a = array();
        if ($sites) {
            foreach ($sites as $site) {
                $a[] = $site->id;
            }
        }

        $model = new LeadAdditional('search', $a);
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['LeadAdditional']))
            $model->attributes = $_GET['LeadAdditional'];

        $this->render('additionalDetails', array(
            'model' => $model,
            'a' => $a
                ), false, true);
    }

    public function actionDeleteAdditional($id) {
        $model = LeadAdditional::model()->findByPk($id);
        $lead_id = $model->lead_id;
        $model->delete();
        $this->redirect(array('leadContacts/view', "id" => $lead_id));
    }

    public function actionCustomerMeter($id) {
        $model = new LeadElcMeter('searchBySite', $id);
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['LeadElcMeter']))
            $model->attributes = $_GET['LeadElcMeter'];

        $model1 = new LeadGasMeter('searchBySite', $id);
        $model1->unsetAttributes();  // clear any default values
        if (isset($_GET['LeadGasMeter']))
            $model1->attributes = $_GET['LeadGasMeter'];


        $sites = array($id);
        //  $leadsite = LeadSite::model()->findAllByPk($id);
        //  foreach ($leadsite as $leadsite) {
        // $sites[] = $leadsite->id;
        // }


        $additioal = new LeadAdditional('search', $sites);
        $additioal->unsetAttributes();  // clear any default values
        if (isset($_GET['LeadAdditional']))
            $additioal->attributes = $_GET['LeadAdditional'];


        $this->render("customer_meters", array('elcmodel' => $model,
            'gasmodel' => $model1,
            'site_id' => $id,
            'additional' => $additioal,
            'sites' => $sites
        ));
    }

    public function actionGetMeters($type, $business) {
        $tender = new LeadTender;
        $meters = array();
        $businesses = LeadBusiness::model()->findByAttributes(array('business_name' => $business));
        if ($businesses) {

            $lead_id = $businesses->lead_id;
            $sites = LeadSite::model()->findAllBYattributes(array('lead_id' => $lead_id));
            $site_ids = array();
            foreach ($sites as $site) {
                $site_ids[] = $site->id;
            }
            $class = "Lead" . $type . "Meter";
            $criteria = new CDbCriteria;
            $criteria->addInCondition('site_id', $site_ids);
            $meters = $class::model()->findAll($criteria);

            if (isset($_POST['LeadTender'])) {
                $tender->attributes = $_POST['LeadTender'];
                $tender->meter_type = $type;
                $tender->created = new CDbExpression("now()");
                $tender->supplier_ids = serialize($_POST['LeadTender']['supplier_ids']);
                $tender->meter_id = serialize($_POST['meter_id']);
                $tender->customer_id = $businesses->lead->id;
                $tender->saveUploadedFile($tender, 'document');
                echo '';
                if (!$tender->save()) {
                    echo '<pre>';
                    print_r($tender->getErrors());
                    die;
                } else {
                    $businesses->lead->is_lead = 0;
                    $businesses->lead->save();
                    $this->redirect(array('leadContacts/addCustomer',
                        "id" => $businesses->id,
                        "update" => 1,
                        "business" => $businesses->business_name,
                        'tender' => 1,
                        'meter_type' => $type,
                        'tender_id' => $tender->id
                    ));
                }
            }
        } else {
            echo '0';
            die;
            //$this->redirect(array('leadContacts/addCustomer'));
        }
        $this->renderPartial('meters', array(
            'meters' => $meters,
            'type' => $type,
            "business" => $business,
            'l_tender' => $tender,
                ), false, true);
    }

    public function actionSiteDropdown($lead_id) {
        $sites = LeadSite::model()->findAllByAttributes(array('lead_id' => $lead_id));
        //   echo '<pre>'; print_r($sites); die;
        $html = '';
        if ($sites)
            foreach ($sites as $site) {
                $html .= '<option value = "' . $site->id . '">' . $site->site_name . '</option>';
            }
        echo $html;
        die;
    }

    public function actionUpdateSiteGrid($lead_id) {
        if (Yii::app()->request->isAjaxRequest) {
            $cs = Yii::app()->clientScript;
            $cs->scriptMap['jquery.js'] = false;
            $cs->scriptMap['jquery.min.js'] = false;
        }
        $model = new LeadSite('searchGrid', $lead_id);
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['LeadSite']))
            $model->attributes = $_GET['LeadSite'];
        $this->renderPartial('site_grid', array('model' => $model, 'lead_id' => $lead_id), false, true);
    }

    public function actionUpdateSiteContactGrid($lead_id) {
        if (Yii::app()->request->isAjaxRequest) {
            $cs = Yii::app()->clientScript;
            $cs->scriptMap['jquery.js'] = false;
            $cs->scriptMap['jquery.min.js'] = false;
        }
        $model = new LeadSiteContactDetails('searchGrid', $lead_id);
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['LeadSiteContactDetails']))
            $model->attributes = $_GET['LeadSiteContactDetails'];
        $this->renderPartial('site_contact_grid', array('model' => $model, 'lead_id' => $lead_id), false, true);
    }

    public function actionUpdateElcGrid($lead_id) {
        if (Yii::app()->request->isAjaxRequest) {
            $cs = Yii::app()->clientScript;
            $cs->scriptMap['jquery.js'] = false;
            $cs->scriptMap['jquery.min.js'] = false;
        }
        $model = new LeadElcMeter('searchGrid', $lead_id);
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['LeadElcMeter']))
            $model->attributes = $_GET['LeadElcMeter'];
        $this->renderPartial('site_elc_grid', array('model' => $model, 'lead_id' => $lead_id), false, true);
    }

    public function actionUpdateGasGrid($lead_id) {
        if (Yii::app()->request->isAjaxRequest) {
            $cs = Yii::app()->clientScript;
            $cs->scriptMap['jquery.js'] = false;
            $cs->scriptMap['jquery.min.js'] = false;
        }
        $model = new LeadGasMeter('searchGrid', $lead_id);
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['LeadGasMeter']))
            $model->attributes = $_GET['LeadGasMeter'];
        $this->renderPartial('site_gas_grid', array('model' => $model, 'lead_id' => $lead_id), false, true);
    }

    public function actionSiteupdate($id) {
        $model = $this->loadModel($id);
        $arr = array();
        $arr['site'] = $model;
        $arr['address'] = $model->address;
        $arr['contact'] = $model->contact;

        if (isset($_POST['LeadSite']) && isset($_POST['LeadSiteContactDetails']) && isset($_POST['LeadSiteAddress'])) {
            
        }
        echo CJSON::encode($arr);
        exit;
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate($id = null) {
        $model = new LeadSite;
        // $model1 = new LeadAdditional;

        if ($id) {
            $model = $this->loadModel($id);
            //$model1 = LeadAdditional::model()->findByAttributes(array('site_id' => $id));
        }
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['LeadSite'])) {
            $model->attributes = $_POST['LeadSite'];
            $model->create_user_id = Yii::app()->user->id;
            $model->created = new CDbExpression('NOW()');
            //  $model1->attributes = $_POST['LeadAdditional'];
            if ($model->save()) {
//                $model1->site_id = $model->id;
//                $model1->save();
                Yii::app()->user->setFlash('success', "Site saved successfully...!!");
                $this->redirect(array('/admin/leadContacts/create'));
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['LeadSite'])) {
            $model->attributes = $_POST['LeadSite'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            LeadGasMeter::model()->deleteAllByAttributes(array('site_id' => $id));
            LeadElcMeter::model()->deleteAllByAttributes(array('site_id' => $id));
            LeadSiteAddress::model()->deleteAllByAttributes(array('site_id' => $id));
            LeadSiteContactDetails::model()->deleteAllByAttributes(array('site_id' => $id));
            LeadAdditional::model()->deleteAllByAttributes(array('site_id' => $id));
            $site = LeadSite::model()->findByPk($id);
            $site->delete();
//            $gas = LeadGasMeter::model()->findAllByAttributes(array('site_id' => $id));
//            $elc = LeadElcMeter::model()->findAllByAttributes(array('site_id' => $id));
//            $address = LeadSiteAddress::model()->findAllByAttributes(array('site_id' => $id));
//            $contact = LeadSiteContactDetails::model()->findAllByAttributes(array('site_id' => $id));
//            $additional = LeadAdditional::model()->findAllByAttributes(array('site_id' => $id));
//
//            //echo '<pre>'; print_r($gas); die;
//            if (!empty($address)) {
//                foreach ($address as $gasa) {
//                    $gasa->delete();
//                }
//            }
//            if (!empty($additional)) {
//                foreach ($additional as $gasa) {
//                    $gasa->delete();
//                }
//            }
//            if (!empty($contact)) {
//                foreach ($contact as $gasa) {
//                    $gasa->delete();
//                }
//            }
//            if (!empty($gas)) {
//                foreach ($gas as $gasa) {
//                    $gasa->delete();
//                }
//            }
//            if (!empty($elc)) {
//                foreach ($elc as $elca) {
//                    $elca->delete();
//                }
//            }
//            $site = LeadSite::model()->findByPk($id);
//            $site->delete();
            // $this->loadModel($id)->delete();
            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('LeadSite');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new LeadSite('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['LeadSite']))
            $model->attributes = $_GET['LeadSite'];

        $this->renderPartial('admin', array(
            'model' => $model,
                ), false, true);
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = LeadSite::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'lead-site-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * 
     * @param type $id
     */
    public function updateSiteDropdown($id) {
        $sites = LeadSite::model()->findAll();
        $html = '';
        foreach ($sites as $site) {
            $html .= '<option value="' . $site->id . '">' . $site->site_name . '</option>';
        }
        return $html;
    }

}
