<?php

class PriceController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'merge'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'tariff', 'filter', 'filterElec'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete', 'getElcMonthData', 'getMonthWiseData'),
                'expression' => 'Yii::app()->user->getIsAdmin()',
            //  'users' => array('admin'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete', 'getElcMonthData', 'getMonthWiseData'),
                'expression' => 'Yii::app()->user->getIsUser()',
            //  'users' => array('admin'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete', 'getElcMonthData', 'getMonthWiseData'),
                'expression' => 'Yii::app()->user->getIsAgency()',
            //  'users' => array('admin'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete', 'getElcMonthData', 'getMonthWiseData'),
                'expression' => 'Yii::app()->user->getIsCustomer()',
            //  'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionTariff($type) {
        $model = new Product;
        $model1 = new Price;
        $this->render('tariff', array('type' => $type,
            'model' => $model,
            'model1' => $model1
        ));
    }

    /**
     * Gas tariff filter
     */
    public function actionFilter() {
        $user_id = '';
        if (isset($_GET['Product']) && isset($_GET['Product']['assigned_to']) && $_GET['Product']['assigned_to']) {
            $user_id = $_GET['Product']['assigned_to'];
        }
        $model = new Price('searchGas', $user_id);
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Price']))
            $model->attributes = $_GET['Price'];
        $this->renderPartial('gas_filter', array('model' => $model, 'user_id' => $user_id), false, true);
    }

    /**
     * Gas tariff filter
     */
    public function actionFilterElec() {
        $user_id = '';
        if (isset($_GET['Product']) && isset($_GET['Product']['assigned_to']) && $_GET['Product']['assigned_to']) {
            $user_id = $_GET['Product']['assigned_to'];
        }
        $model = new Price('searchElec', $user_id);
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Price']))
            $model->attributes = $_GET['Price'];
        $this->renderPartial('elec_filter', array('model' => $model, 'user_id' => $user_id), false, true);
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Price;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Price'])) {
            $model->attributes = $_POST['Price'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Price'])) {
            $model->attributes = $_POST['Price'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Price');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Price('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Price']))
            $model->attributes = $_GET['Price'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Price::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'price-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * 
     * @param type $month
     */
    public function actionGetMonthWiseData($month) {
        $region = array();
        if ($_GET['Price']['postcode'])
            $region = Price::getRegion($_GET['Price']['postcode'], Price::TYPE_GAS);
//        echo '<pre>';
//        print_r($region);
//        die;
        $region2 = '';
        $region1 = '';
        $region3 = '';
        if (!empty($region)) {
            $region1 = rtrim($region[0]);
            $region2 = trim($region[2]);
            $region3 = trim($region[1]);
//            if ($region31 = Price::getRegion2($region2)) {
//                $region3 = trim($region31[0]);
//                $region4 = trim($region31[1]);
//                $region5 = trim($region31[4]);
//                $_GET['Price']['region1'] = $region3;
//                $_GET['Price']['region4'] = $region4;
//                $_GET['Price']['region5'] = $region5;
//            }
        }
//        echo '<pre>';
//        print_r($region[0]);
//        die;
        $_GET['Price']['region'] = trim($region2);
        $_GET['Price']['region1'] = trim($region1);
        $_GET['Price']['retention_value'] = trim($region3);
        $model = new Price('searchByMonth');
        $model->unsetAttributes();  // clear any default values
        
        if (isset($_GET['Price']))
            $model->attributes = $_GET['Price'];

        $user = $_GET['Price']['assigned_to'];

        $this->renderPartial('_searchQuote', array('model' => $model, 'month' => $month, 'user' => $user), false, true);
    }

    /**
     * 
     * @param type $month
     */
    public function actionGetElcMonthData($month) {
        $cs = Yii::app()->clientScript;
        $cs->scriptMap = array(
            'jquery.js' => false,
        );
        $region = '';
        $region2 = '';
        if (isset($_GET['Price']['distributor']) && ($_GET['Price']['distributor'])) {
            $region = DistributerMap::model()->findByAttributes(array('dist_id' => $_GET['Price']['distributor']));
            $region2 = $region->ldz;
        }

        if (isset($_GET['Price']['postcode']) && ($_GET['Price']['postcode'])) {
            $region = Price::getRegion($_GET['Price']['postcode'], Price::TYPE_GAS);
            if (!empty($region)) {
                $region2 = rtrim($region[2]);
            }
        }

        $regions = '';
        $mapping = DistributerMap::model()->findByAttributes(array('ldz' => $region2));
        if ($mapping && $mapping->region) {
            $r = trim($mapping->region);
            $regions .= '"' . $r . '"' . ",";
        }
        if ($mapping && $mapping->dist_id) {
            $r = trim($mapping->dist_id);
            $regions .= '"' . $r . '"' . ",";
        }
        if ($mapping && $mapping->region7) {
            $r = trim($mapping->region7);
            $regions .= '"' . $r . '"' . ",";
        }
        if ($mapping && $mapping->region6) {
            $r = trim($mapping->region6);
            $regions .= '"' . $r . '"' . ",";
        }
        if ($mapping && $mapping->region5) {
            $r = trim($mapping->region5);
            $regions .= '"' . $r . '"' . ",";
        }
        if ($mapping && $mapping->region4) {
            $r = trim($mapping->region4);
            $regions .= '"' . $r . '"' . ",";
        }
        if ($mapping && $mapping->region3) {
            $r = trim($mapping->region3);
            $regions .= '"' . $r . '"' . ",";
        }
        if ($mapping && $mapping->region2) {
            $r = trim($mapping->region2);
            $regions .= '"' . $r . '"' . ",";
        }
        if ($mapping && $mapping->region1) {
            $r = trim($mapping->region1);
            $regions .= '"' . $r . '"' . ",";
        }
        if ($mapping && $mapping->ldz) {
            $r = trim($mapping->ldz);
            $regions .= '"' . $r . '"';
        }
//echo $regions; die;
//echo $region2 . '  '.$regions; die;
//        echo '<pre>';
//        print_r($region);
//        die;
//        $region2 = '';
//        if ($region) {
//            $region2 = $region->region;
//            $region1 = $region->ldz;
//            // $region3 = $region[3];
//            // $region4 = $region[1];
//            $_GET['Price']['region1'] = $region2;
//            $_GET['Price']['region3'] = $region1;
//            $_GET['Price']['region4'] = $region2;
//            // $region5 = $region[5];
//        }
        $_GET['Price']['region'] = $regions;
        //  $_GET['Price']['region1'] = $region5;
        $model = new Price('searchEByMonth');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Price']))
            $model->attributes = $_GET['Price'];

        $this->renderPartial('_searchEQuote', array('model' => $model, 'month' => $month), false, true);
    }

}
