<?php

class LeadGasMeterController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'meterupdate', 'updateGasGrid'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionMeterupdate($id) {
        $model = $this->loadModel($id);
        echo CJSON::encode($model);
        exit;
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate($id = null) {
        $arr = array('status' => 'NOK', "id" => "0", "next_form" => "0", "next_form1" => "0", "next_form2" => '0');
        $model = new LeadGasMeter;
        if ($id) {
            $model = $this->loadModel($id);
        }
        if (isset($_POST['LeadGasMeter'])) {

            $model->attributes = $_POST['LeadGasMeter'];
            $model->create_user_id = Yii::app()->user->id;
            $model->created = new CDbExpression("NOW()");
            // echo '<pre>'; print_r($_FILES); die;
            $model->saveUploadedFile($model, 'document');
            if ($model->save()) {
                $arr['status'] = "OK";
                $arr['id'] = $model->site->lead_id;
            } else {
                $arr['id'] = "0";
                $arr['err'] = $model->getErrors();
            }
            echo CJSON::encode($arr);
            die;
        }

        $this->render('create', array(
            'model' => $model
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['LeadGasMeter'])) {
            $model->attributes = $_POST['LeadGasMeter'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            exit('1');
        //     throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
            
    }

    public function actionUpdateGasGrid($lead_id) {
        if (Yii::app()->request->isAjaxRequest) {
            $cs = Yii::app()->clientScript;
            $cs->scriptMap['jquery.js'] = false;
            $cs->scriptMap['jquery.min.js'] = false;
            //$cs->scriptMap['jquery.yiigridview.js'] = false;
            // $cs->scriptMap['jquery.ba-bbq.js'] = false;
        }
        $model = new LeadGasMeter('searchGrid', $lead_id);
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['LeadGasMeter']))
            $model->attributes = $_GET['LeadGasMeter'];
        $this->renderPartial('site_gas_grid', array('model' => $model, 'lead_id' => $lead_id), false, true);
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('LeadGasMeter');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
        Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
//        Yii::app()->clientScript->scriptMap['jquery.ba-bbq.js'] = false;
//        Yii::app()->clientScript->scriptMap['jquery.yiigridview.js'] = false;

        $model = new LeadGasMeter('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['LeadGasMeter']))
            $model->attributes = $_GET['LeadGasMeter'];

        $this->renderPartial('admin', array(
            'model' => $model,
                ), false, true);
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = LeadGasMeter::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'lead-gas-meter-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
