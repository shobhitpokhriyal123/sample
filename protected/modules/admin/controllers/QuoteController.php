<?php

class QuoteController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'send', 'update', 'commission', 'pdf', 'updateStatus', 'updateDate', 'viewMore'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'expression' => 'Yii::app()->user->getIsAdmin()',
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'expression' => 'Yii::app()->user->getIsAgency()',
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'expression' => 'Yii::app()->user->getIsUser()',
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('quote'),
                'expression' => 'Yii::app()->user->getIsCustomer()',
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * 
     */
    public function actionSend() {
        if (isset($_POST['email']) && $_POST['email']) {
            //  echo '<pre>'; print_r($_POST); die;
            $email = $_POST['email'];
            $quotes = $_POST['quote_id'];
            $quotes = rtrim($quotes, ", ");
            $criteria = new CDbCriteria;
            $quotes = explode(",", $quotes);
            $criteria->addInCondition('id', $quotes);
            $model = Quote::model()->findAll($criteria);
            $model1 = new Quote;
            $to = "$email";
            $from = "noreply@utilitycloudlive.co.uk";
            $sub = "New quotation from Utilitycloudlive";
            $tmplate = "//mail/send_saved_quote";
            $body = "Utility Cloud Live..!";
            if ($model1->sendMailTemplate($to, $from, $sub, $tmplate, $body, $model)) {
                echo '1';
            } else {
                echo '2';
            }
        }

        die;
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * 
     * @param type $id
     */
    public function actionViewMore($id, $month) {
        $model = Price::model()->findByPk($id);
        $this->renderPartial('viewMore', array('model' => $model, 'month' => $month), FALSE, true);
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate($id) {
        $model = new Quote;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        //  echo '<pre>'; print_r($_POST); die;
        if (isset($_POST['Quote'])) {
            $model->attributes = $_POST['Quote'];
            // echo '<pre>'; print_r($model);  die;
            $model->create_user_id = Yii::app()->user->id;
            $model->is_select = $_POST['Quote']['is_select'];
            if ($model->save()) {
                echo $model->id;
                die;
                //$this->redirect(array('view', 'id' => $model->id));
            }
        }
        die('no');
//        $this->render('create', array(
//            'model' => $model,
//        ));
    }

    public function actionUpdateStatus($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        if (Yii::app()->request->isAjaxRequest) {
            $cs = Yii::app()->clientScript;
            $cs->scriptMap['jquery.js'] = false;
            $cs->scriptMap['jquery.min.js'] = false;
        }
        if (isset($_POST['Quote'])) {
            $model->attributes = $_POST['Quote'];
            if ($model->save())
                $this->redirect(array('commission', "Quote[is_select]" => 1));
        }

        $this->renderPartial('updateStatus', array(
            'model' => $model,
                ), false, true);
    }

    public function actionUpdateDate($id) {
        $model = ContractDetail::model()->findByAttributes(array('quote_id' => $id));
        if (Yii::app()->request->isAjaxRequest) {
            $cs = Yii::app()->clientScript;
            $cs->scriptMap['jquery.js'] = false;
            $cs->scriptMap['jquery.min.js'] = false;
        }
        ///  echo '<pre>'; print_r($model); die;
        if (!$model)
            $model = new ContractDetail;

//$this->loadModel($id);
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['ContractDetail'])) {
            $model->attributes = $_POST['ContractDetail'];
            $model->live_date = $_POST['ContractDetail']['live_date'];
            // echo '<pre>'; print_r($model); die;
            if ($model->save())
                $this->redirect(array('commission', "Quote[is_select]" => 1));
        }

        $this->renderPartial('updateDate', array(
            'model' => $model,
                ), false, true);
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Quote'])) {
            $model->attributes = $_POST['Quote'];
            $model->invoice_no = $_POST['Quote']['invoice_no'];
            if ($model->save()) {
                Yii::app()->user->setFlash('success', "Successfully updated...!");
                $this->redirect(array('commission', 'Quote[is_select]' => 1));
            } else {
                Yii::app()->user->setFlash('error', "Error while saving data...!");
                $this->redirect(array('commission', 'Quote[is_select]' => 1));
            }
        }

        $this->renderPartial('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Quote');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin($month) {
        $model = new Quote('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Quote']))
            $model->attributes = $_GET['Quote'];

        $this->renderPartial('admin', array(
            'model' => $model,
            'month' => $month
                ), false, true);
    }

    /**
     * Manages all models.
     */
    public function actionQuote() {
        $model = new Quote('searchQuote');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Quote']))
            $model->attributes = $_GET['Quote'];
        $this->render('quote', array('model' => $model));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Quote::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'quote-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Manages all models.
     */
    public function actionCommission() {
        if (Yii::app()->user->isAdmin) {
            $model = new Quote('search');
        } else {
            $model = new Quote('search', 1);
        }
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Quote']))
            $model->attributes = $_GET['Quote'];

        $this->render('commissions', array(
            'model' => $model,
                //'month' => $month
        ));
    }

    public function actionPdf() {
        //echo '<pre>'; print_r($_POST); die;
        $abc = $_POST['pdf'];
        $mPDF1 = Yii::app()->ePdf->mpdf();
        $mPDF1->SetHtmlHeader("Your header text here");
        # You can easily override default constructor's params
        $mPDF1 = Yii::app()->ePdf->mpdf('', 'A4');
        // $mPDF1->setRotatedText(35, 190, 'U T I L I T Y   E N E R G Y', 45);
        // $model = $this->l oadModel($id);
        $mPDF1->WriteHTML($abc);
        $mPDF1->Output();
    }

}
