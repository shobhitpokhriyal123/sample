<?php

class LeadContactsController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    public $defaultAction = "create";

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('updateQuote', 'myLeads', 'updateDropdown', 'checkMeter', 'getAutofill', 'viewAll', 'newLead', 'addAddress', 'contactDropdown', 'getSites', 'create', 'UpdateLead', 'quotes', 'getMeterDetail', 'getMeters', 'tenderForm', 'sendQuote', 'sites', 'business', 'update', 'viewCustomerSite', 'allLeads', 'leadupdate', 'getAgencyId', 'addCustomer', 'customers', 'isLeadExists'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * 
     * @param type $id
     */
    public function actionCheckMeter($id) {
        if ($id == "Gas") {
            $meter = LeadGasMeter::model()->findAll();
        } else {
            $meter = LeadElcMeter::model()->findAll();
        }
        $html = '';
        foreach ($meter as $mtr) {
            $html .= '<option value="' . $mtr->id . '">' . $mtr->meter_name . ' (' . $mtr->site->site_name . ' )</option>';
        }
        echo $html;
        die;
    }

    public function updateDropdown() {
        $leads = LeadContacts::model()->findAll();
        $html = '';
        foreach ($leads as $lead) {
            $html .= '<option value="' . $lead->id . '">' . $lead->first_name . '</option>';
        }
    }

    public function actionNewLead() {
        $lead = new LeadContacts;
        $user = new Users;
        //  echo '<pre>'; print_r($_POST); die;
        if (isset($_POST['Users']) && isset($_POST['LeadContacts'])) {
            $user->attributes = $_POST['Users'];
            $user->fullname = $_POST['Users']['fullname'];
            $user->email = $_POST['Users']['email'];
            if ($user->save()) {
                $lead->attributes = $_POST['LeadContacts'];
                $lead->create_user_id = Yii::app()->user->id;
                $lead->created = new CDbExpression("now()");
                $lead->user_id = $user->id;
                $lead->first_name = $user->fullname;
                $lead->agency_id = $_POST['LeadContacts']['agency_id'];
                $lead->agent_id = $_POST['LeadContacts']['agent_id'];
                $lead->agency_name = $_POST['LeadContacts']['agency_id'];
                $lead->status = $_POST['LeadContacts']['status'];
                if (!$lead->save()) {

                    echo '<pre>';
                    print_r($lead->getErrors());
                }
            } else {
                echo '<pre>';
                print_r($user->getErrors());
            }
        }
        echo '1';
        die;
    }

    /**
     * 
     * @param type $id
     */
    public function actionGetAgencyId($id) {
        $users = Users::model()->findAllByAttributes(array('role_id' => 4, 'agency_id' => $id));
        $html = '';
        if ($users && !Yii::app()->user->isUser) {
            foreach ($users as $user) {
                $html .= '<option value="' . $user->id . '" >' . $user->username . '</option>';
            }
           
        }
         if (Yii::app()->user->isUser) {
                $html .= '<option value="' . Yii::app()->user->id . '" >' . Yii::app()->user->username . '</option>';
            }
        echo $html;
        die;
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
//    public function actionView($id) {
//        $this->render('view', array(
//            'model' => $this->loadModel($id),
//        ));
//    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionAddCustomer($id = null, $update = null, $business = null, $tender = null, $meter_type = null, $tender_id = null) {
        $l_business = '';
        $business = '';
        if (isset($_GET['business'])) {
            $business = $_GET['business'];
        }
        if (isset($_POST['business'])) {
            $business = $_POST['business'];
        }
        if ($business) {
            $l_business = LeadBusiness::model()->findByAttributes(array('business_name' => $business));
            $id = $l_business->id;
            $update = 1;
        }
        if ($id)
            $l_business = LeadBusiness::model()->findByPk($id);
        if ($l_business) {
            $model = $l_business->lead;
            $l_address = LeadAddress::model()->findByAttributes(array('lead_id' => $model->id));
            $user = $l_business->lead->user;
        } else {
            $model = new LeadContacts;
            $l_address = new LeadAddress;
            $l_business = new LeadBusiness;
            $user = new Users;
        }
        $l_elec_meter = new LeadElcMeter;
        $l_gas_meter = new LeadGasMeter;
        $l_site = new LeadSite;
        $l_site_address = new LeadSiteAddress;
        $l_upload = new LeadUpload;
        $l_site_contact = new LeadSiteContactDetails;
        if ($tender_id) {
            $l_tender = LeadTender::model()->findByPk($tender_id);
            $tender_files = $l_tender->uploads;
            if (empty($tender_files))
                $tender_files = new LeadTenderUpload;
        } else {
            $l_tender = new LeadTender;
            $tender_files = new LeadTenderUpload;
        }
        $lead_names = LeadBusiness::getLeadName();
        $array = array();
        if ($lead_names)
            foreach ($lead_names as $lead) {
                $array[] = $lead['business_name'];
            }
        if (isset($_POST['Users'])) {
            $user->attributes = $_POST['Users'];
            $user->password = md5($_POST['Users']['password']);
            //$user->fullname = $_POST['LeadContacts']['first_name'];
            $user->role_id = 5;
            $user->saveUploadedFile($user, 'image');
            if ($user->save()) {
                if (isset($_POST['LeadContacts'])) {
                    $model->attributes = $_POST['LeadContacts'];
                    $model->user_id = $user->id;
                    $model->is_lead = 0;
                    $model->agency_id = $_POST['LeadContacts']['agency_id'];
                    $model->agency_name = $_POST['LeadContacts']['agency_id'];
                    $model->first_name = $user->fullname;
                    $model->agent_id = $_POST['LeadContacts']['agent_id'];
                    $model->create_user_id = Yii::app()->user->id;
                    $model->created = new CDbExpression("NOW()");
                    if ($model->save()) {
                        if (isset($_POST['LeadBusiness'])) {
                            $l_business->attributes = $_POST['LeadBusiness'];
                            $l_business->create_user_id = Yii::app()->user->id;
                            $l_business->created = new CDbExpression("NOW()");
                            $l_business->lead_id = $model->id;
                            if (!$l_business->save()) {
                                $arr['errors'] = $l_business->getErrors();
                            }
                        }
                        if (isset($_POST['LeadAddress'])) {
                            $l_address->attributes = $_POST['LeadAddress'];
                            $l_address->create_user_id = Yii::app()->user->id;
                            $l_address->created = new CDbExpression("NOW()");
                            $l_address->lead_id = $model->id;
                            if (!$l_address->save()) {
                                $arr['errors'] = $l_address->getErrors();
                            }
                        }


                        $arr['status'] = "OK";
                        $arr['id'] = $model->id;
                    } else {
                        $arr['errors'] = $model->getErrors();
                    }
                }
            } else {
                $arr['errors'] = $user->getErrors();
                $arr['id'] = "0";
            }
            echo CJSON::encode($arr);
            die;
        }
//echo '<pre>'; print_r($tender_files); die;
        $this->render('addCustomer', array(
            'model' => $model,
            'l_address' => $l_address,
            'l_business' => $l_business,
            'l_elec_meter' => $l_elec_meter,
            'l_gas_meter' => $l_gas_meter,
            'l_site' => $l_site,
            'l_site_address' => $l_site_address,
            'l_upload' => $l_upload,
            'l_site_contact' => $l_site_contact,
            'user' => $user,
            'array' => $array,
            'update' => $update,
            'tender' => $tender,
            'meter_type' => $meter_type,
            'l_tender' => $l_tender,
            'tender_files' => $tender_files,
            't_business' => $business
        ));
    }

//    public function actionNewLead(){
//        
//        $this->renderPartial();
//    }

    public function actionCreate($id = null, $update = null) {
        $arr = array("id" => "0", "status" => "NOK");
        $l_business = LeadBusiness::model()->findByPk($id);
        $additional = new LeadAdditional;
        if ($l_business) {
            $model = $l_business->lead;
            $l_address = LeadAddress::model()->findByAttributes(array('lead_id' => $model->id));
            $user = $l_business->lead->user;
            // $additional = $l_business->lead->additional;
            if (!$additional)
                $additional = new LeadAdditional;
        } else {
            $model = new LeadContacts;
            $l_address = new LeadAddress;
            $l_business = new LeadBusiness;
            $user = new Users;
        }
        $l_elec_meter = new LeadElcMeter;
        $l_gas_meter = new LeadGasMeter;
        $l_site = new LeadSite;
        $l_site_address = new LeadSiteAddress;
        $l_upload = new LeadUpload;
        $l_site_contact = new LeadSiteContactDetails;


        $lead_names = LeadBusiness::getLeadName();
        $array = array();
        if ($lead_names)
            foreach ($lead_names as $lead) {
                $array[] = $lead['business_name'];
            }
        if (isset($_POST['Users'])) {
            $user->attributes = $_POST['Users'];
            $user->password = md5($_POST['Users']['password']);
            $user->role_id = 5;
            if (isset($_POST['LeadContacts'])) {
                $model->attributes = $_POST['LeadContacts'];
                $model->is_lead = 1;
                $model->agency_id = $_POST['LeadContacts']['agency_id'];
                $model->agency_name = $_POST['LeadContacts']['agency_id'];
                $model->first_name = $user->fullname;
                $model->agent_id = $_POST['LeadContacts']['agent_id'];
                $model->create_user_id = Yii::app()->user->id;
                $model->created = new CDbExpression("NOW()");
            }
            if (isset($_POST['LeadBusiness'])) {
                $l_business->attributes = $_POST['LeadBusiness'];
                $l_business->create_user_id = Yii::app()->user->id;
                $l_business->created = new CDbExpression("NOW()");
            }
            if (isset($_POST['LeadAdditional'])) {
                $additional->attributes = $_POST['LeadAdditional'];
                $additional->create_user_id = Yii::app()->user->id;
            }
            if (isset($_POST['LeadAddress'])) {
                $l_address->attributes = $_POST['LeadAddress'];
                $l_address->create_user_id = Yii::app()->user->id;
                $l_address->created = new CDbExpression("NOW()");
            }
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $user->saveUploadedFile($user, 'image');
                if (!$user->save())
                    $arr['user_err'] = $user->getErrors();
                $model->user_id = $user->id;
                if (!$model->save())
                    $arr['lead_err'] = $model->getErrors();
                $l_business->lead_id = $model->id;
                if (!$l_business->save())
                    $arr['bus_err'] = $l_business->getErrors();
                $l_address->lead_id = $model->id;
                if (!$l_address->save())
                    $arr['address_err'] = $l_address->getErrors();

//                $additional->lead_id = $model->id;
//                if (!$additional->save()) {
//                    $arr['additional_err'] = $additional->getErrors();
//                }
                $transaction->commit();
                $arr["id"] = $model->id;
            } catch (Exception $e) {
                $transaction->rollBack();
                $arr["id"] = 0;
                $arr["errr"] = $e->getMessage();
                //   Yii::app()->user->setFlash('error', "{$e->getMessage()}");
                $this->refresh();
            }
            echo CJSON::encode($arr);
            die;
        }
        //   echo $update; die;


        $this->render('create', array(
            'model' => $model,
            'l_address' => $l_address,
            'l_business' => $l_business,
            'l_elec_meter' => $l_elec_meter,
            'l_gas_meter' => $l_gas_meter,
            'l_site' => $l_site,
            'l_site_address' => $l_site_address,
            'l_upload' => $l_upload,
            'l_site_contact' => $l_site_contact,
            'l_user' => $user,
            'array' => $array,
            'update' => $update,
            'additional' => $additional
//            'site' => $site,
//            'electric' => $electric,
//            'gas' => $gas,
//            'leadsites' => $leadsites
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate1($id = null) {
        $arr = array('status' => 'NOK', "id" => "0", "next_form" => "LeadBusiness_lead_id", "next_form1" => "LeadAddress_lead_id", "next_form2" => 'LeadSite_lead_id');
        $model = new LeadContacts;
        $l_address = new LeadAddress;
        $l_business = new LeadBusiness;
        $l_elec_meter = new LeadElcMeter;
        $l_gas_meter = new LeadGasMeter;
        $l_site = new LeadSite;
        $l_site_address = new LeadSiteAddress;
        $l_upload = new LeadUpload;
        $l_site_contact = new LeadSiteContactDetails;
        $user = new Users;

        if ($id) {
            $model = LeadContacts::model()->findByPk($id);
            $user = Users::model()->findByPk($model->user_id);
        }
        $user->scenario = 'new_user';
        if (isset($_POST['Users'])) {

            //  echo '<pre>'; print_r($_POST); die;
            $user->attributes = $_POST['Users'];
            $user->saveUploadedFile($user, 'image');
            $user->password = md5($_POST['Users']['password']);
            $user->confirm_password = md5($_POST['Users']['password']);
            $user->role_id = 5;
            if ($user->save()) {
                if (isset($_POST['LeadContacts'])) {
                    $model->attributes = $_POST['LeadContacts'];
                    $model->agency_id = $_POST['LeadContacts']['agency_id'];
                    $model->agency_name = $_POST['LeadContacts']['agency_name'];
                    $model->agent_id = $_POST['LeadContacts']['agent_id'];
                    $model->create_user_id = Yii::app()->user->id;
                    $model->is_lead = 1;
                    $model->created = new CDbExpression("NOW()");
                    $model->first_name = $user->fullname;
                    $model->user_id = $user->id;
                    if ($model->save()) {
                        if (isset($_POST['LeadBusiness']) && isset($_POST['LeadAddress'])) {
                            $l_business->attributes = $_POST['LeadBusiness'];
                            $l_business->lead_id = $model->id;
                            $l_business->business_name = $_POST['LeadBusiness']['business_name'];
                            $l_business->credit_score = $_POST['LeadBusiness']['credit_score'];
                            $l_business->charity_no = $_POST['LeadBusiness']['charity_no'];
                            $l_business->created = new CDbExpression("NOW()");
                            $l_business->create_user_id = Yii::app()->user->id;
                            if (!$l_business->save()) {
                                $arr['error'] = $l_business->getErrors();
                            }
                            $l_address->attributes = $_POST['LeadAddress'];
                            $l_business->lead_id = $model->id;
                            $l_address->created = new CDbExpression("NOW()");
                            $l_address->create_user_id = Yii::app()->user->id;
                            $l_address->business_name = $_POST['LeadAddress']['business_name'];
                            $l_address->business_no = $_POST['LeadAddress']['business_no'];
                            $l_address->billing_no = $_POST['LeadAddress']['billing_no'];
                            if (!$l_address->save()) {
                                $arr['error'] = $l_address->getErrors();
                            }
                        }
                        if (isset($_POST['LeadSite']) && isset($_POST['LeadSiteAddress']) && isset($_POST['LeadSiteContactDetails'])) {
                            $l_site->attributes = $_POST['LeadSite'];
                            $l_site->lead_id = $model->id;
                            $l_site->created = new CDbExpression("NOW()");
                            $l_site->create_user_id = Yii::app()->user->id;
                            if (!$l_site->save()) {
                                $arr['error'] = $l_site->getErrors();
                            }

                            $l_site_address->attributes = $_POST['LeadSiteAddress'];
                            $l_site_address->site_id = $l_site->id;
                            $l_site_address->created = new CDbExpression("NOW()");
                            $l_site_address->create_user_id = Yii::app()->user->id;
                            if (!$l_site_address->save()) {
                                $arr['error'] = $l_site_address->getErrors();
                            }


                            $l_site_contact->attributes = $_POST['LeadSiteContactDetails'];
                            $l_site_contact->site_id = $l_site->id;
                            $l_site_contact->created = new CDbExpression("NOW()");
                            $l_site_contact->create_user_id = Yii::app()->user->id;
                            if (!$l_site_contact->save()) {
                                $arr['error'] = $l_site_contact->getErrors();
                            }
                        }

                        $arr['id'] = $model->id;
                        $arr['status'] = "OK";
                        // Yii::app()->user->setFlash('success', "Lead Contact saved successfully...!!");
                        // $this->redirect(array('/admin/leadContacts/create'));
                    } else {
                        $arr['error'] = $model->getErrors();
                    }
                }
                echo CJSON::encode($arr);
                die;
            }
        }

        $this->render('create', array(
            'model' => $model,
            'l_address' => $l_address,
            'l_business' => $l_business,
            'l_elec_meter' => $l_elec_meter,
            'l_gas_meter' => $l_gas_meter,
            'l_site' => $l_site,
            'l_site_address' => $l_site_address,
            'l_upload' => $l_upload,
            'l_site_contact' => $l_site_contact,
            'l_user' => $user,
                ), false, true);
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['LeadContacts'])) {
            $model->attributes = $_POST['LeadContacts'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $business = LeadBusiness::model()->findByPk($id);
            $lead_id = $business->lead->id;
            $business->lead->user->delete();
            LeadAddress::model()->deleteAllByAttributes(array('lead_id' => $lead_id));
            $sites = LeadSite::model()->findAllByAttributes(array('lead_id' => $lead_id));
            if ($sites)
                foreach ($sites as $site) {
                    LeadSiteAddress::model()->deleteAllByAttributes(array('site_id' => $site->id));
                    LeadSiteContactDetails::model()->deleteAllByAttributes(array('site_id' => $site->id));
                    LeadElcMeter::model()->deleteAllByAttributes(array('site_id' => $site->id));
                    LeadGasMeter::model()->deleteAllByAttributes(array('site_id' => $site->id));
                    // $site->delete();
                }
            LeadSite::model()->deleteAllByAttributes(array('lead_id' => $lead_id));
            $business->lead->delete();
            $business->delete();

            // $this->loadModel($id)->delete();
            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('LeadContacts');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new LeadContacts('search');
        $cs = Yii::app()->clientScript;

        $model->unsetAttributes();
        // clear any default values
        if (isset($_GET['LeadContacts']))
            $model->attributes = $_GET['LeadContacts'];


        $this->renderPartial('admin', array(
            'model' => $model,
                ), false, true);
    }

    public function actionViewCustomerSite($id) {
        $site = LeadSite::model()->findByPk($id);

        $model1 = new LeadElcMeter('search');
        $model1->unsetAttributes();  // clear any default values
        if (isset($_GET['LeadElcMeter']))
            $model1->attributes = $_GET['LeadElcMeter'];

        $model2 = new LeadGasMeter('search');
        $model2->unsetAttributes();  // clear any default values
        if (isset($_GET['LeadGasMeter']))
            $model2->attributes = $_GET['LeadGasMeter'];

        $this->render('view_customer_site', array(
            'site' => $site,
            'elemodel' => $model1,
            'gasmodel' => $model2));
    }

    /**
     * Manages all models.
     */
    public function actionCustomers() {
        $model = new LeadBusiness('search', 1);
        $model->unsetAttributes();
        // clear any default values
        if (isset($_GET['LeadBusiness']))
            $model->attributes = $_GET['LeadBusiness'];

        $this->render('customers', array(
            'model' => $model,
            'customer' => 1
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = LeadContacts::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'lead-contacts-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionLeadupdate($id) {
        $model = $this->loadModel($id);
        $arr = array();
        $arr['user'] = $model->user;
        $arr['lead'] = $model;
        echo CJSON::encode($arr);
        // echo CJSON::encode($model);
        exit;
    }

    /**
     * 
     */
    public function actionIsLeadExists() {
        //   echo '<pre>'; print_r($_POST); die;
        if (isset($_POST['data'])) {
            $business_name = $_POST['data'];
            $a = LeadBusiness::isBusinessName($business_name);
            if (!empty($a)) {
                echo $a[0]['lead_id'];
            } else {
                echo '0';
            }
        }
        die;
    }

    /**
     * 
     * @param type $id
     */
    public function actionGetMeters() {
        if (isset($_POST['LeadAddress'])) {
            $bname = $_POST['LeadAddress']['business_name'];
            $business = Yii::app()->db->createCommand()
                    ->select('id,lead_id, business_name')
                    ->from('tbl_lead_business')
                    ->where('business_name=:business_name', array(':business_name' => $bname))
                    ->queryRow();
            // 
            $html = '';

            if ($business) {
                $lead_id = $business['lead_id'];
                $gas_meters = LeadGasMeter::model()->findAllByAttributes(array('lead_id' => $lead_id));
                $elec_meters = LeadElcMeter::model()->findAllByAttributes(array('lead_id' => $lead_id));
                if (!$gas_meters && !$elec_meters) {
                    echo '1';
                    die;
                } else {
                    $html .= '<h4>Gas Meter</h4>
                    <table class="table table-striped items ">
                    <tr>
                        <th>Please Select</th>
                         <th>Meter Name</th>
                        <th>Mpan</th>
                        <th>Contract End Date</th>
                        <th>Site Name</th>
                        <th>Type</th>
                        <th>Consumption</th>
                        <th>Current Supplier</th>
                    </tr>';
                    if ($gas_meters) {
                        foreach ($gas_meters as $gas_meter) {
                            $html .= ' <tr>
                                        <td><input type="radio" name="select_meter">
                                        <input type="hidden" name="id" value="' . $gas_meter->id . '">
                                          <input type="hidden" name="type" value="Gas">   
                                        </td>
                                        <td>' . $gas_meter->meter_name . '</td>
                                         <td>' . $gas_meter->mpr . '</td>
                                        <td>' . $gas_meter->contract_end_date . '</td>
                                        <td>' . $gas_meter->site->site_name . '</td>
                                        <td>GAS</td>
                                        <td>' . $gas_meter->total_aq . '</td>
                                        <td>' . $gas_meter->supplier->username . '</td>
                                    </tr>';
                        }
                    } else {
                        $html .= '<tr> <td colspan ="8">No record(s) found...!</td> </tr>';
                    }
                    $html .= '</table>';




                    $html .= '<h4>Electric Meter</h4>
                    <table class="table table-striped items ">
                    <tr>
                        <th>Please Select</th>
                        <th>Meter Name</th>
                        <th>Mpan</th>
                        <th>Contract End Date</th>
                        <th>Site Name</th>
                        <th>Type</th>
                        <th>Consumption</th>
                        <th>Current Supplier</th>
                    </tr>';
                    if ($elec_meters) {
                        foreach ($elec_meters as $elec_meter) {
                            $html .= ' <tr>
                                    <td><input type="radio" name="select_meter">
                                    <input type="hidden" name="id" value="' . $elec_meter->id . '">
                                    <input type="hidden" name="type" value="Electric">   
                                    </td>
                                    <td>' . $elec_meter->meter_name . '</td>
                                    <td>' . $elec_meter->mpan . '</td>
                                    <td>' . $elec_meter->contract_end_date . '</td>
                                    <td>' . $elec_meter->site->site_name . '</td>
                                    <td>ELECTRIC</td>
                                    <td>' . $elec_meter->total_aq . '</td>
                                    <td>' . $elec_meter->supplier->username . '</td>
                                </tr>';
                        }
                    } else {
                        $html .= '<tr> <td colspan ="8">No record(s) found...!</td> </tr>';
                    }
                    $html .= '</table>';

                    $html .= '<button class="btn btn-primary" onclick="getTenderForm($(this)); return false;" > Corporate Quote </button>';
                }
                echo $html;
            } else {
                echo '1';
            }
        }

        die;
    }

    public function actionAllLeads() {
        $model = new LeadBusiness('search');
        $model->unsetAttributes();

        // clear any default values
        if (isset($_GET['LeadBusiness']))
            $model->attributes = $_GET['LeadBusiness'];


        $this->render('allLeads', array(
            'model' => $model,
                ), false, true);
    }

    /**
     * 
     */
    public function actionBusiness() {

        $lead_names = LeadBusiness::getLeadName();
        $array = array();
        if ($lead_names)
            foreach ($lead_names as $lead) {
                $array[] = $lead['business_name'];
            }
        $this->renderPartial('business', array('array' => $array), false, true);
    }

    public function actionTenderForm() {
        $tender = new LeadTender();
        if (isset($_POST['id'])) {
            
        }
        $this->renderPartial('tenderForm', array('l_tender' => $tender), false, true);
    }

    /**
     * 
     */
    public function actionSites() {
        $id = Yii::app()->user->id;
        $lead = LeadContacts::model()->findByAttributes(array('user_id' => $id));



        $sites = array();
        $leadsite = LeadSite::model()->findAllByAttributes(array('lead_id' => $lead->id));
        foreach ($leadsite as $leadsite) {
            $sites[] = $leadsite->id;
        }


        $additioal = new LeadAdditional('search', $sites);
        $additioal->unsetAttributes();  // clear any default values
        if (isset($_GET['LeadAdditional']))
            $additioal->attributes = $_GET['LeadAdditional'];

        $model = new LeadSite('searchCustomerSite', $id);
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['LeadSite']))
            $model->attributes = $_GET['LeadSite'];

        $this->render('customer_sites', array(
            'model' => $model,
            'lead' => $lead,
            'additional' => $additioal,
            'sites' => $sites
        ));
    }

    public function actionSendQuote($tender_id, $cid) {
        if (Yii::app()->request->isAjaxRequest) {
            $cs = Yii::app()->clientScript;
            $cs->scriptMap['jquery.js'] = false;
            $cs->scriptMap['jquery.min.js'] = false;
        }
        $tender = LeadTender::model()->findByPk($tender_id);
        // echo '<pre>'; print_r($tender); die;
        $checkedmeters = $tender->meter_id;
        $checkedmeters = unserialize($checkedmeters);
        $criteria = new CDbCriteria;
        $criteria->addInCondition('id', $checkedmeters);
        $type = $tender->meter_type;

        $class = "Lead" . $type . "Meter";
        $checkedmeter = $class::model()->findAll($criteria);
        // echo '<pre>'; print_r($checkedmeters); die;

        $model = new LeadQuote;
        if (isset($_POST['LeadQuote'])) {
            
//            echo '<pre>'; print_r($_POST); die;
//            die(1);
            $unit_rates = $_POST['LeadQuote']['unit_rate'];
            $meter_ids = $_POST['LeadQuote']['meter_ids'];
            $model->attributes = $_POST['LeadQuote'];
            $model->unit_rate = serialize($unit_rates);
            $model->customer_id = $cid;
            $model->meter_ids = serialize($meter_ids);
            $model->tender_id = $tender_id;
            $model->created = new CDbExpression("NOW()");
            $model->create_user_id = Yii::app()->user->id;
            // echo '<pre>'; print_r($_FILES) ; die;
            $model->saveUploadedFile($model, 'supply_contract');
            if ($model->save()) {
                echo '1';
                die; //  $this->redirect(array("leadContacts/quotes"));
            } else {
                echo '<pre>';
                print_r($model->getErrors());
                die;
            }
        }
        $this->renderPartial("sendQuote", array(
            'model' => $model,
            'lead_id' => $cid,
            'tender_id' => $tender_id,
            'checkedmeter' => $checkedmeter,
            'type' => $type
                ), false, true);
    }

    public function actionUpdateQuote($id) {

        $model = LeadQuote::model()->findByPk($id);
        if (Yii::app()->request->isAjaxRequest) {
            $cs = Yii::app()->clientScript;
            $cs->scriptMap['jquery.js'] = false;
            $cs->scriptMap['jquery.min.js'] = false;
        }

        // die($id);
        // echo '<pre>'; print_r($model); 
        $tender = LeadTender::model()->findByPk($model->tender_id);
        // echo '<pre>'; print_r($tender); die;
        $checkedmeters = $tender->meter_id;
        $checkedmeters = unserialize($checkedmeters);
        $criteria = new CDbCriteria;
        $criteria->addInCondition('id', $checkedmeters);
        $type = $tender->meter_type;

        $class = "Lead" . $type . "Meter";
        $checkedmeter = $class::model()->findAll($criteria);
        $tender_id = $model->tender_id;
        $cid = $model->customer_id;
        if (isset($_POST['LeadQuote'])) {
            $unit_rates = $_POST['LeadQuote']['unit_rate'];
            $meter_ids = $_POST['LeadQuote']['meter_ids'];
            $model->attributes = $_POST['LeadQuote'];
            $model->unit_rate = serialize($unit_rates);
            $model->meter_ids = serialize($meter_ids);
            $model->create_user_id = Yii::app()->user->id;
            $model->tender_id = $tender_id;
            $model->created = new CDbExpression("NOW()");
            $model->saveUploadedFile($model, 'supply_contract');
            if ($model->save()) {
                $this->redirect(array("leadContacts/quotes"));
            } else {
                echo '<pre>';
                print_r($model->getErrors());
                die;
            }
        }
        $this->renderPartial("sendQuote", array(
            'model' => $model,
            'lead_id' => $cid,
            'tender_id' => $tender_id,
            'checkedmeter' => $checkedmeter,
            'type' => $type
                ), false, true);
    }

    public function actionQuotes($cid = null) {
        if (Yii::app()->user->isCustomer) {
            $model = new LeadQuote('search', $cid);
        }
        if (Yii::app()->user->isAdmin) {
            $model = new LeadQuote('search');
        }
        if (!Yii::app()->user->isAdmin && !Yii::app()->user->isCustomer) {
            $model = new LeadQuote('search');
        }
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['LeadQuote']))
            $model->attributes = $_GET['LeadQuote'];
        $this->render('quotes', array('model' => $model, 'cid' => $cid));
    }

    /**
     * 
     */
    public function actionGetMeterDetail($id, $site_id) {
        if ($id == "Gas") {
            $meters = LeadGasMeter::model()->findAllByAttributes(array('site_id' => $site_id));
        } else {
            $meters = LeadElcMeter::model()->findAllByAttributes(array('site_id' => $site_id));
        }
        $html = '<option value="">Select Meter</option>';
        if ($meters) {
            foreach ($meters as $meter) {
                $html .= '<option value="' . $meter->id . '">' . $meter->meter_name . '</option>';
            }
        }
        echo $html;
        die;
    }

    public function actionUpdateLead($id) {

        $s_model = new LeadSite('search');
        $s_model->unsetAttributes();  // clear any default values
        if (isset($_GET['LeadSite']))
            $s_model->attributes = $_GET['LeadSite'];

        $l_business = LeadBusiness::model()->findByPk($id);
        $model = $l_business->lead;
        $l_address = LeadAddress::model()->findByAttributes(array('lead_id' => $model->id));
        //echo '<pre>'; print_r($l_address); die;

        $l_elec_meter = new LeadElcMeter;
        $l_gas_meter = new LeadGasMeter;
        $l_site = new LeadSite;
        $l_site_address = new LeadSiteAddress;
        $l_upload = new LeadUpload;
        $l_site_contact = new LeadSiteContactDetails;
        $user = $model->user;
        $user->scenario = 'new_agency';
        if (isset($_POST['Users'])) {

            $user->attributes = $_POST['Users'];
            $user->role_id = 5;
            if ($user->save()) {
                if (isset($_POST['LeadContacts'])) {
                    $model->attributes = $_POST['LeadContacts'];
                    $model->user_id = $user->id;
                    $model->is_lead = 0;
                    $model->agency_id = $_POST['LeadContacts']['agency_name'];
                    $model->agency_name = $_POST['LeadContacts']['agency_name'];
                    $model->agent_id = $_POST['LeadContacts']['agent_id'];
                    $model->first_name = $user->fullname;
                    $model->create_user_id = Yii::app()->user->id;
                    $model->created = new CDbExpression("NOW()");
                    if ($model->save()) {
                        if (isset($_POST['LeadBusiness'])) {
                            $l_business->attributes = $_POST['LeadBusiness'];
                            $l_business->lead_id = $model->id;
                            $l_business->create_user_id = Yii::app()->user->id;
                            $l_business->created = new CDbExpression("NOW()");
                            if (!$l_business->save()) {
                                echo '<pre>';
                                print_r($l_business->getErrors());
                                die;
                            }
                        }
                        if (isset($_POST['LeadAddress'])) {
                            $l_address->attributes = $_POST['LeadAddress'];
                            $l_address->lead_id = $model->id;
                            $l_address->create_user_id = Yii::app()->user->id;
                            $l_address->created = new CDbExpression("NOW()");
                            if (!$l_address->save()) {
                                echo '<pre>';
                                print_r($l_address->getErrors());
                                die;
                            }
                        }
                        if (isset($_POST['LeadSite'])) {
                            $l_site->attributes = $_POST['LeadSite'];
                            $l_site->lead_id = $model->id;
                            $l_site->create_user_id = Yii::app()->user->id;
                            $l_site->created = new CDbExpression("NOW()");
                            if ($l_site->save()) {
                                $l_site_contact->attributes = $_POST['LeadSiteContactDetails'];
                                $l_site_contact->site_id = $l_site->id;
                                $l_site_contact->create_user_id = Yii::app()->user->id;
                                $l_site_contact->created = new CDbExpression("NOW()");
                                if (!$l_site_contact->save()) {
                                    echo '<pre>';
                                    print_r($l_site_contact->getErrors());
                                    die;
                                }

                                $l_site_address->attributes = $_POST['LeadSiteAddress'];
                                $l_site_address->site_id = $l_site->id;
                                $l_site_address->create_user_id = Yii::app()->user->id;
                                $l_site_address->created = new CDbExpression("NOW()");
                                if (!$l_site_address->save()) {
                                    echo '<pre>';
                                    print_r($l_site_address->getErrors());
                                    die;
                                }

                                $l_elec_meter->attributes = $_POST['LeadElcMeter'];
                                $l_elec_meter->site_id = $l_site->id;
                                $l_elec_meter->create_user_id = Yii::app()->user->id;
                                $l_elec_meter->created = new CDbExpression("NOW()");
                                if (!$l_elec_meter->save()) {
                                    echo '<pre>';
                                    print_r($l_elec_meter->getErrors());
                                    die;
                                }

                                $l_gas_meter->attributes = $_POST['LeadGasMeter'];
                                $l_gas_meter->site_id = $l_site->id;
                                $l_gas_meter->create_user_id = Yii::app()->user->id;
                                $l_gas_meter->created = new CDbExpression("NOW()");
                                if (!$l_gas_meter->save()) {
                                    echo '<pre>';
                                    print_r($l_gas_meter->getErrors());
                                    die;
                                }
                            } else {
                                echo '<pre>';
                                print_r($l_site->getErrors());
                                die;
                            }
                        }
                    }
                }
            }
        }
        //echo '<pre>'; print_r($l_business); die;

        $this->render('update_lead', array(
            'model' => $model,
            'l_address' => $l_address,
            'l_business' => $l_business,
            'l_elec_meter' => $l_elec_meter,
            'l_gas_meter' => $l_gas_meter,
            'l_site' => $l_site,
            'l_site_address' => $l_site_address,
            'l_upload' => $l_upload,
            'l_site_contact' => $l_site_contact,
            'user' => $user,
            's_model' => $s_model
        ));
    }

    public function getSites() {
        $sites = LeadSite::model()->findAll();
        $html = '';
        if ($sites) {
            foreach ($sites as $site) {
                $html .= '<option value="' . $site->id . '">' . $site->site_name . '<option>';
            }
        }
        echo $html;
        die;
    }

    public function contactDropdown() {
        $contacts = LeadContacts::model()->findAll();
        $html = '';
        if ($contacts) {
            foreach ($contacts as $contact) {
                $html .= '<option value="' . $contact->id . '">' . $contact->first_name . '<option>';
            }
        }
        echo $html;
        die;
    }

    /**
     * 
     * @param type $id
     */
    public function actionView($id) {
        $model = LeadBusiness::model()->findByPk($id);

        $lead_id = $model->lead->id;

        $sites = array();
        $leadsite = LeadSite::model()->findAllByAttributes(array('lead_id' => $lead_id));
        foreach ($leadsite as $leadsite) {
            $sites[] = $leadsite->id;
        }


        $additioal = new LeadAdditional('search', $sites);
        $additioal->unsetAttributes();  // clear any default values
        if (isset($_GET['LeadAdditional']))
            $additioal->attributes = $_GET['LeadAdditional'];



        $electric = new LeadElcMeter('searchByLead', $id);
        $electric->unsetAttributes();  // clear any default values
        if (isset($_GET['LeadElcMeter']))
            $electric->attributes = $_GET['LeadElcMeter'];


        $gas = new LeadGasMeter('searchByLead', $id);
        $gas->unsetAttributes();  // clear any default values
        if (isset($_GET['LeadGasMeter']))
            $gas->attributes = $_GET['LeadGasMeter'];


        $leadsites = new LeadSite('searchByLead', $id);
        $leadsites->unsetAttributes();  // clear any default values
        if (isset($_GET['LeadSite']))
            $leadsites->attributes = $_GET['LeadSite'];

        $this->render('view', array(
            'model' => $model,
            'gas' => $gas,
            'electric' => $electric,
            'leadsites' => $leadsites,
            'additional' => $additioal,
            'sites' => $sites
        ));
    }

    public function actionGetAutofill($lead_id) {
        $arr = array();
        $site_address = '';
        $site_contact = '';
        $site = '';
        $site = LeadSite::model()->findByAttributes(array('lead_id' => $lead_id));
        if ($site)
            $site_address = LeadSiteAddress::model()->findByAttributes(array('site_id' => $site->id));
        if ($site)
            $site_contact = LeadSiteContactDetails::model()->findByAttributes(array('site_id' => $site->id));
        $arr['site'] = $site;
        $arr['site_address'] = $site_address;
        $arr['site_contact'] = $site_contact;
        echo CJSON::encode($arr);
        die;
    }

    public function actionMyLeads() {
        $id = Yii::app()->user->id;
    }

    public function actionDeleteAdditional($id) {
        $model = LeadAdditional::model()->findByPk($id);
        $lead_id = $model->lead_id;
        $model->delete();
        $this->redirect(array('view', 'id' => $lead_id));
    }

}
