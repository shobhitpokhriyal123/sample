<?php

class AgencyCustomerController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    public $defaultAction = 'admin';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'download'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'dashboard', 'viewAll', 'createNew'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'expression' => 'Yii::app()->user->getIsAdmin()',
            //'users' => array('admin'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'expression' => 'Yii::app()->user->getIsAgency()',
            //'users' => array('admin'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'expression' => 'Yii::app()->user->getIsUser()',
            //'users' => array('admin'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'expression' => 'Yii::app()->user->getIsCustomer()',
            //'users' => array('admin'),
            ),
//            array('allow', // allow admin user to perform 'admin' and 'delete' actions
//                'actions' => array('admin', 'delete'),
//                'users' => array('admin'),
//            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionViewAll($id) {
        $model1 = new CustomerSite('search', $id);
        $model1->unsetAttributes();  // clear any default values
        if (isset($_GET['CustomerSite'])) {
            $model1->attributes = $_GET['CustomerSite'];
        }

        $model2 = new CustomerSiteMeter('searchMeters', $id);
        $model2->unsetAttributes();
        if (isset($_GET['CustomerSiteMeter'])) {
            $model2->attributes = $_GET['CustomerSiteMeter'];
        }

        $model3 = new CustomerContract('searchContracts2', $id);
        $model3->unsetAttributes();
        if (isset($_GET['CustomerContract'])) {
            $model3->attributes = $_GET['CustomerContract'];
        }

        $model4 = new CustomerQuote('searchQuote', $id);
        $model4->unsetAttributes();
        if (isset($_GET['CustomerQuote'])) {
            $model4->attributes = $_GET['CustomerQuote'];
        }
        $model5 = new CustomerTender('searchTender', $id);
        $model5->unsetAttributes();
        if (isset($_GET['CustomerTender'])) {
            $model5->attributes = $_GET['CustomerTender'];
        }
        $this->render('view_all', array(
            'model' => $this->loadModel($id),
            'model1' => $model1,
            'model2' => $model2,
            'model3' => $model3,
            'model4' => $model4,
            'model5' => $model5,
        ));
    }

    public function actionCreateNew($id = null) {
        // $this->layout='column1';
       
        $model = new AgencyCustomer;
        $l_contract = new CustomerContract;
        $l_quote = new CustomerQuote;
        $l_site = new CustomerSite;
        $l_site_meter = new CustomerSiteMeter;
        $l_tender = new CustomerTender;
        $user = new Users;

        //  $l_site_address = new LeadSiteAddress;
        //  $l_upload = new LeadUpload;
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        if ($id) {
            $model = AgencyCustomer::model()->findByPk($id);
          //   echo "<pre>" ; print_r($model); die;
        }
        if (isset($_POST['AgencyCustomer'])) {
          //  echo "<pre>" ; print_r($_POST); die;
            $model->attributes = $_POST['AgencyCustomer'];
            $model->create_user_id = Yii::app()->user->id;
            $model->created = new CDbExpression("NOW()");

            if ($model->save(false)) {

                Yii::app()->user->setFlash('success', "Agency Customer saved successfully...!!");
                $this->redirect(array('/admin/agencyCustomer/createnew'));
            }
            else {
                echo '<pre>';
                print_r($user->getErrors());
                die;
            }
        }

        $this->render('create_new', array(
            'model' => $model,
            'l_contract' => $l_contract,
            'l_quote' => $l_quote,
            'l_site' => $l_site,
            'l_site_meter' => $l_site_meter,
            'l_tender' => $l_tender,
            'user' => $user
          //  'l_site_address' => $l_site_address,
         //   'l_upload' => $l_upload
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new AgencyCustomer;
        $user = new Users;
        $user->scenario = 'customer';
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);




        if (isset($_POST['AgencyCustomer']) && isset($_POST['Users'])) {

            $user->attributes = $_POST['Users'];
            $user->role_id = Users::ROLECUSTOMER;
            $user->fullname = $user->username;
            $user->confirm_password = md5($user->password);
            $user->password = md5($user->password);
            if ($user->save()) {

                $model->attributes = $_POST['AgencyCustomer'];
                $model->create_user_id = Yii::app()->user->id;
                $model->user_id = $user->id;
                $model->created = new CDbExpression('NOW()');
                if ($model->save())
                    $this->redirect(array('view', 'id' => $model->id));
                else {
                    // echo '<pre>'; print_r($model->getErrors()); die;    
                }
            } else {
                echo '<pre>';
                print_r($user->getErrors());
                die;
            }
        }

        $this->render('create', array(
            'model' => $model,
            'user' => $user,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);
        $user = new Users;
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['AgencyCustomer'])) {
            $model->attributes = $_POST['AgencyCustomer'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
            'user' => $user
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            //delete all data regarding agency customer
            //start logic here
            // $model = $this->loadModel($id);

            CustomerQuote::model()->deleteAllByAttributes(array('customer_id' => $id));
            $sites = CustomerSite::model()->findAllByAttributes(array('customer_id' => $id));
            foreach ($sites as $site) {
                CustomerSiteMeter::model()->deleteAllByAttributes(array('site_id' => $site->id));
                $contracts = CustomerContract::model()->findAllByAttributes(array('site_id' => $site->id));
                foreach ($contracts as $contract) {
                    CustomerTender::model()->deleteAllByAttributes(array('contract_id' => $contract->id));
                    $contract->delete();
                }
                $site->delete();
            }
            //endlogic here
            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            // we only allow deletion via POST request
            $this->loadModel($id)->delete();
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('AgencyCustomer');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new AgencyCustomer('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['AgencyCustomer']))
            $model->attributes = $_GET['AgencyCustomer'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = AgencyCustomer::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'agency-customer-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionDashboard() {
        $id = Yii::app()->user->id;
    }

}
