<?php

class LeadBusinessController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'businessupdate', 'leads', 'addAddress'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionAddAddress() {
        $business = new LeadBusiness;
        $address = new LeadAddress;
        if (isset($_POST['LeadBusiness'])) {
            $business->attributes = $_POST['LeadBusiness'];
            $business->create_user_id = Yii::app()->user->id;
            $business->created = new CDbExpression("now()");
            $business->business_name = $_POST['LeadBusiness']['business_name'];
            $business->credit_score = $_POST['LeadBusiness']['credit_score'];
            $business->charity_no = $_POST['LeadBusiness']['charity_no'];
            if ($business->save()) {
                if (isset($_POST['LeadAddress'])) {
                    $address->attributes = $_POST['LeadAddress'];
                    $address->create_user_id = Yii::app()->user->id;
                    $address->created = new CDbExpression("now()");
                    $address->create_user_id = Yii::app()->user->id;
                    $address->business_name = $_POST['LeadAddress']['business_name'];
                    $address->business_no = $_POST['LeadAddress']['business_no'];
                    $address->billing_no = $_POST['LeadAddress']['billing_no'];
                    $address->save();
                }
            } else {
                print_r($business->getErrors());
            }
        }
        echo '1';
        die;
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionUpdateLead($id) {
        $model = $this->loadModel($id);
        $user = $model->user;
        if (isset($_POST['LeadContacts']) && isset($_POST['Users'])) {
            $model->attributes = $_POST['LeadContacts'];
            $user->attributes = $_POST['Users'];
            if ($user->save() && $model->save()) {
                Yii::app()->user->setFlash('success', "Lead Contact saved successfully...!!");
                $this->redirect(array('/admin/leadContacts/allLeads'));
            }
        }
        $this->renderPartial('update_lead', array(
            'model' => $model,
            'user' => $user
                )
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
//    public function actionCreate($id = null) {
//        $arr = array('status' => 'NOK', "id" => "0", "next_form" => "0", "next_form1" => "0", "next_form2" => '0');
//        $model = new LeadBusiness;
//        $address = new LeadAddress;
//        if ($id) {
//            $model = $this->loadModel($id);
//        }
//        if (isset($_POST['LeadBusiness'])) {
//            // echo '<pre>'; print_r($_POST); die;
//            $model->attributes = $_POST['LeadBusiness'];
//            $model->create_user_id = Yii::app()->user->id;
//            $model->created = new CDbExpression("NOW()");
//            if ($model->save()) {
//                $address->attributes = $_POST['LeadAddress'];
//                $address->business_name = $_POST['LeadAddress']['business_name'];
//                $address->business_no = $_POST['LeadAddress']['business_no'];
//                $address->create_user_id = Yii::app()->user->id;
//                $address->lead_id = $model->id;
//                $address->created = new CDbExpression("NOW()");
//                if ($address->save()) {
//                    $arr["status"] = "OK";
//                } else {
//                    $arr['343'] = $address->getErrors();
//                }
//                $arr["id"] = $model->id;
//                echo CJSON::encode($arr);
//                die;
//            } else {
//                $arr['34ss3'] = $model->getErrors();
//            }
//            $arr["id"] = $model->id;
//            echo CJSON::encode($arr);
//            die;
//        }
//
//        $this->render('create', array(
//            'model' => $model,
//            'l_address' => $address
//        ));
//    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['LeadBusiness'])) {
            $model->attributes = $_POST['LeadBusiness'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('LeadBusiness');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
        Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
//        Yii::app()->clientScript->scriptMap['jquery.ba-bbq.js'] = false;
//        Yii::app()->clientScript->scriptMap['jquery.yiigridview.js'] = false;

        $model = new LeadBusiness('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['LeadBusiness']))
            $model->attributes = $_GET['LeadBusiness'];
        // echo '<pre>'; print_r($_POST); die;
        if (isset($_POST['LeadAddress'])) {
            $business_name = $_POST['LeadAddress']['business_name'];
            $a = LeadBusiness::isBusinessName($business_name);
            //  echo '<pre>'; print_r($a); die;
            if (!empty($a)) {
                $this->redirect(array('/admin/leadAdmin/admin', 'bid' => $a[0]['id'], 'lid' => $a[0]['lead_id']));
            } else {
                $this->redirect(array('/admin/leadContacts/create'));
            }
        }
        $this->renderPartial('admin', array(
            'model' => $model,
                ),false, true);
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = LeadBusiness::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'lead-business-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionBusinessupdate($id) {
        $model = $this->loadModel($id);
        echo CJSON::encode($model);
        exit;
    }

    public function actionLeads() {
        $model = new LeadBusiness('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_POST['LeadAddress'])) {
            $business_name = $_POST['LeadAddress']['business_name'];
        }

        if (isset($_GET['LeadBusiness'])) {
            $model->attributes = $_GET['LeadBusiness'];
            $model->business_name = $business_name;
        }
        $this->render('leads', array(
            'model' => $model,
        ));

        // echo '<pre>'; print_r($_POST); die;
    }

}
