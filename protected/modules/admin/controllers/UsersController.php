<?php

class UsersController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    public $defaultAction = 'view';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'thumbnail', 'download', 'merge'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'getSubcategory', 'utilities', 'getUtilities', 'getAgency', 'updateAgency', 'profile'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete', 'types', 'newUser', 'allUsers', 'newAgency', 'agencies', 'updateAgent'),
                'expression' => 'Yii::app()->user->getIsAdmin()',
            //'users' => array('admin'),
            ),
//            array('allow', // allow admin user to perform 'admin' and 'delete' actions
//                'actions' => array('admin', 'delete', 'letter', 'getUser'),
//                'expression' => 'Yii::app()->user->getIsHR()',
//            //'users' => array('admin'),
//            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id = null) {
        if (!$id)
            $id = Yii::app()->user->id;

        if (Yii::app()->user->isCustomer) {
            $this->render('customer_dashboard', array(
                'model' => $this->loadModel($id),
            ));
        } else
            $this->render('view', array(
                'model' => $this->loadModel($id),
            ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Users;
        $model->password = '';
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Users'])) {
            $model->attributes = $_POST['Users'];
//            $model->password = md5($_POST['Users']['password']);
//            $sub = '';
//            if ($_POST['Users']['sub_type_id']) {
//                $sub = implode(",", $_POST['Users']['sub_type_id']);
//            }
//            $model->sub_type_id = $sub;
//            $model->parrent_type_id = $_POST['Users']['parrent_type_id'];
            $model->saveUploadedFile($model, 'image');
            //  echo '<pre>'; print_r($model);
            if ($model->save()) {

                $this->redirect(array('types'));
            } else {
//                print_r($model->getErrors());
//                die;
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {

        $model = $this->loadModel($id);
        $model->password = '';
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Users'])) {
            $model->attributes = $_POST['Users'];
            //echo '<pre>'; print_r($_POST); die;
            $model->description = $_POST['Users']['description'];
            $model->saveUploadedFile($model, 'image');
            if ($model->save()) {

                $this->redirect(array('types'));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            $products = Product::model()->findAllByAttributes(array('assigned_to' => $id));
            foreach ($products as $product) {
                
            }
            Product::model()->deleteAllByAttributes(array('assigned_to' => $id));
            MeterType::model()->deleteAllByAttributes(array('supplier_id' => $id));
            //  Fields::model()->deleteAllByAttributes(array('assigned_to' => $id));
            ContractDetail::model()->deleteAllByAttributes(array('current_supplier' => $id));
            ContractDetail::model()->deleteAllByAttributes(array('new_supplier' => $id));
            CustomerQuote::model()->deleteAllByAttributes(array('supplier_id' => $id));

            // $products = Product::model()->findAllAttributes(array('assigned_to'=>$id))->delete();
            // we only allow deletion via POST request
            $this->loadModel($id)->delete();
//            $model = $this->loadModel($id);
//            $model->active = 0;
//            $model->save();
            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Users');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $role = 2;
        //  echo '<pre>'; print_r($_GET); die;
        $model = new Users('search');
        $model->unsetAttributes();  // clear any default values

        if (isset($_GET['Users']))
            $model->attributes = $_GET['Users'];

        $this->render('admin', array(
            'model' => $model,
            'role' => $role
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Users::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'users-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * 
     * @param type $id
     */
    public function actionGetUser($id) {
        $model = $this->loadModel($id);
        $arr = array();
        $arr['id'] = $model->id;
        $arr['fullname'] = $model->fullname;
        $arr['joining_date'] = date("d-M-Y", strtotime($model->joining_date));
        $arr['role'] = $model->rolename->title;
        echo json_encode($arr);
        die;
    }

    /**
     * 
     * @param type $id
     */
    public function actionGetSubcategory($id) {
        $model = Users::model()->findByPk($id);
        $sub = explode(",", $model->sub_type_id);
        echo json_encode($sub);
        die;
    }

    public function actionTypes() {

        $model = new Users;
        $model->scenario = "types";
        if (isset($_POST['Users'])) {
            $user_id = $_POST['Users']['supplier_id'];
            $model = $this->loadModel($user_id);
            $sub = '';
            if ($_POST['Users']['sub_type_id']) {
                $sub = implode(",", $_POST['Users']['sub_type_id']);
            }
            $model->attributes = $_POST['Users'];
            $model->sub_type_id = $sub;
            // $model->parrent_type_id = $_POST['Users']['parrent_type_id'];
            // echo '<pre>'; print_r($model); die;
            if ($model->save()) {
                $this->redirect(array('utilities', 'id' => $model->id));
            }
        }
        $this->render('types', array(
            'model' => $model,
        ));
    }

    public function actionUtilities($id) {
        if (!$id)
            $id = Yii::app()->user->id;
        $this->render('utilities', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * 
     * @param type $id
     */
    public function actionGetUtilities($id) {
        $products = Product::model()->findAllByAttributes(array('assigned_to' => $id));
        $utilitis = array();
        foreach ($products as $product) {
            $utilitis[] = $product->utility;
        }
        $utilitis = array_unique($utilitis);
        $u = implode(",", $utilitis);
        $html = '';
        if ($utilitis) {
            foreach ($utilitis as $utility) {
                $html .= '<input type="hidden" name="Userss[sub_type_id]" id="p_utility" value="' . $u . '">
           <label class="checkbox">
        <input id=""  value="' . $utility . '" name="Users[sub_type_id][]" type="checkbox" checked="checked" readonly="true">
            <label for="">' . Product::getUtilites($utility) . '</label>
        </label>';
            }
        } else {
            echo '1';
        }
        echo $html;
        die;
    }

    /**
     * 
     */
    public function actionNewUser() {
        $model = new Users;
        $model->scenario = 'new_user';

        if (isset($_POST['Users'])) {
            $model->attributes = $_POST['Users'];

            $model->agency_id = $_POST['Users']['agency_id'];

            $model->confirm_password = md5($_POST['Users']['confirm_password']);
            $model->password = md5($_POST['Users']['password']);
            $model->role_id = 4;
            $model->saveUploadedFile($model, 'image');

            $this->performAjaxValidation($model);
            if ($model->save()) {
                $this->redirect(array('allUsers'));
            }
        }

        $this->render('new_user', array('model' => $model));
    }

    public function actionAllUsers() {
        $role = 4;
        $model = new Users('search', $role);
        $model->unsetAttributes();  // clear any default values
        //echo  '<pre>'; print_r($model); die;
        if (isset($_GET['Users']))
            $model->attributes = $_GET['Users'];
        $this->render('all_users', array('model' => $model, 'role' => $role));
    }

    /**
     * 
     */
    public function actionNewAgency() {

        //echo '<pre>'; print_r($_POST); die;
        $model = new Users;
        $agency = new Agency;
        $agency_address = new AgencyAddress;
        $agency_bank = new AgencyBank;
        $model->scenario = 'new_agency';

        if (isset($_POST['Users']) && isset($_POST['Agency'])) {
            $this->performAjaxValidation($model);

            $model->attributes = $_POST['Users'];
            $model->password = md5($model->password);
            $model->fullname = $model->username;
            // $model->agency_id = $_POST['Users']['agency_id'];
            //  $model->email =    $model->username;
            $model->role_id = 3;

            $model->saveUploadedFile($model, 'image');
            // 
            if ($model->save()) {
                $agency->attributes = $_POST['Agency'];
                $agency->agency_id = 'AGENCY' . $model->id;
                $agency->saveUploadedFile($agency, 'agency_logo');
                $agency->saveUploadedFile($agency, 'agency_file');
                $agency->saveUploadedFile($agency, 'company_pic');
                $agency->saveUploadedFile($agency, 'letter_authority');
                $agency->user_id = $model->id;
                $agency->date_created = new CDbExpression('NOW()');
                $agency->date_modified = new CDbExpression('NOW()');
                //echo '<pre>'; print_r($agency); die;
                if ($agency->save()) {
                    $agency_address->attributes = $_POST['AgencyAddress'];
                    // $self_billing->attributes = $_POST['Agency'];
                    $agency_address->agency_id = $agency->id;

                    $agency_address->save();
                    $agency_bank->attributes = $_POST['AgencyBank'];
                    $agency_bank->agency_id = $agency->id;
                    $agency_bank->create_user_id = $model->id;
                    $agency_bank->save();
                }
                $this->redirect(array('agencies'));
            } else {
                echo 'c<pre>';
                print_r($model->getErrors());
                die;
            }
        }

        $this->render('new_agency', array(
            'model' => $model,
            'agency' => $agency,
            'agency_address' => $agency_address,
            'agency_bank' => $agency_bank
                )
        );
    }

    /**
     * 
     */
    public function actionAgencies() {
        //     $role = 3;
        $model = new Agency('search');
        $model->unsetAttributes();  // clear any default values

        if (isset($_GET['Agency']))
            $model->attributes = $_GET['Agency'];
        $this->render('agencies', array('model' => $model));
    }

    /**
     * 
     * @param type $id
     */
    public function actionGetAgency($id) {
        $user = Users::model()->findByPk($id);
        // echo '<pre>'; print_r($agency); die;
        $this->renderPartial('agency', array('user' => $user), false, true);
    }

    /**
     * 
     * @param type $id
     */
    public function actionUpdateAgent($id) {
        $model = $this->loadModel($id);
        $pwd = $model->password;
        $model->password = '';
        // $model->confirm_password = '';
        $model->scenario = 'new_user';
        if (isset($_POST['Users'])) {
            $file = $model->image;
            $model->attributes = $_POST['Users'];
//            echo '<pre>';
//            print_r($_POST);
//            die;

            // echo "<pre>" ;print_r($_POST); die;
            if (isset($_POST['Users']['password']) && $_POST['Users']['password'] != '') {
                $model->password = md5($_POST['Users']['password']);
                $model->confirm_password = md5($_POST['Users']['password']);
            } else {
                $model->password = $pwd;
            }
            $model->tac = $_POST['Users']['tac'];
            $model->auc = $_POST['Users']['auc'];
            $model->umc = $_POST['Users']['umc'];
            $model->active = $_POST['Users']['active'];
            $model->agency_id = $_POST['Users']['agency_id'];

            $model->role_id = 4;
            //  echo '<pre>'; print_r($_FILES); die;
            if (isset($_FILES['Users']['name']['image'])) {
                $model->image = $file;
            } else
                $model->saveUploadedFile($model, 'image');
            //   echo '<pre>'; print_r($model); die;
            if ($model->save()) {
                $this->redirect(array('view', 'id' => $model->id));
            } else {
                echo "<pre>";
                print_r($model->getErrors());
                die;
            }
        }

        $this->render('new_user', array('model' => $model));
    }

//     $model = new Users;
//        $model->scenario = 'new_user';

    /**
     * 
     * @param type $id
     */
    public function actionUpdateAgency($id) {
        $model = $this->loadModel($id);
        $pwd = $model->password;
        $model->password = '';
        $model->scenario = 'updateAgency';
        $agency = Agency::model()->findByAttributes(array('user_id' => $model->id));
        $agency_address = AgencyAddress::model()->findByAttributes(array('agency_id' => $agency->id));
        if (!$agency_address) {
            $agency_address = new AgencyAddress;
        }


        $agency_bank = AgencyBank::model()->findByAttributes(array('agency_id' => $agency->id));
        if (!$agency_bank) {
            $agency_bank = new AgencyBank;
        }
        $this->performAjaxValidation($model);
        $this->performAjaxValidation($agency);
        $this->performAjaxValidation($agency_address);
        $this->performAjaxValidation($agency_bank);

        if (isset($_POST['Users'])) {
//            echo '<pre>';
//            print_r($_FILES);
//            die;
            $file = $model->image;

            $model->attributes = $_POST['Users'];
            if (isset($_POST['Users']['password']) && $_POST['Users']['password'] != '') {
                $model->password = md5($_POST['Users']['password']);
                $model->confirm_password = md5($_POST['Users']['password']);
            } else {
                $model->password = $pwd;
            }
            if ($_FILES['Users']['name']['image'] == '')
                $model->image = $file;
            else
                $model->saveUploadedFile($model, 'image');

            $model->save();
            if (isset($_POST['Agency'])) {
                $agency_logo = $agency->agency_logo;
                $agency_file = $agency->agency_file;
                $company_pic = $agency->company_pic;
                $letter_authority = $agency->letter_authority;
                $agency->attributes = $_POST['Agency'];
                if ($_FILES['Agency']['name']['agency_logo'] == '') {
                    $agency->agency_logo = $agency_logo;
                } else {
                    $agency->saveUploadedFile($agency, 'agency_logo');
                }
                if ($_FILES['Agency']['name']['agency_file'] == '') {
                    $agency->agency_file = $agency_file;
                } else {
                    $agency->saveUploadedFile($agency, 'agency_file');
                }

                if ($_FILES['Agency']['name']['company_pic'] == '') {
                    $agency->company_pic = $company_pic;
                } else {
                    $agency->saveUploadedFile($agency, 'company_pic');
                }

                if ($_FILES['Agency']['name']['letter_authority'] == '') {
                    $agency->letter_authority = $letter_authority;
                } else {
                    $agency->saveUploadedFile($agency, 'letter_authority');
                }



                $agency->date_modified = new CDbExpression('NOW()');
                if ($agency->save()) {

                    if (isset($_POST['AgencyAddress'])) {
                        $agency_address->attributes = $_POST['AgencyAddress'];
                        $agency_address->agency_id = $agency->id;
                        $agency_address->save();
                    }
                    if (isset($_POST['AgencyBank'])) {
                        $agency_bank->attributes = $_POST['AgencyBank'];
                        $agency_bank->agency_id = $agency->id;
                        $agency_bank->save();
                    }
                    $this->redirect(array('agencies'));
                } else {
                    //echo '<pre>'; print_r($agency->getErrors()); die;
                }
            }
        }



        $this->render('new_agency', array(
            'model' => $model,
            'agency' => $agency,
            'agency_address' => $agency_address,
            'agency_bank' => $agency_bank
        ));
    }

}
