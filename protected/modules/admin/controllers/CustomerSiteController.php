<?php

class CustomerSiteController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    public $defaultAction = 'admin';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update','admin','sites'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate($cid) {
        $model = new CustomerSite;
        $model->customer_id = $cid;
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['CustomerSite'])) {
            $model->attributes = $_POST['CustomerSite'];
            $model->created = new CDbExpression('NOW()');
            $model->create_user_id = Yii::app()->user->id;
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['CustomerSite'])) {
            $model->attributes = $_POST['CustomerSite'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            
            
             CustomerQuote::model()->deleteAllByAttributes(array('customer_id' => $id));
              $sites2 = CustomerSite::model()->findAllByAttributes(array('customer_id'=>$id));
           
             foreach ($sites2 as $contract) {
                CustomerTender::model()->deleteAllByAttributes(array('contract_id' => $contract->id));
              }
            $sites = CustomerSite::model()->findAllByAttributes(array('customer_id'=>$id));
            foreach ($sites as $site) {
                CustomerSiteMeter::model()->deleteAllByAttributes(array('site_id' => $site->id));
                CustomerContract::model()->deleteAllByAttributes(array('site_id' => $site->id));
              
            }
            
              
            
            
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('CustomerSite');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin($cid = null) {
        $model = new CustomerSite('search',$cid);
        $customer = AgencyCustomer::model()->findByPk($cid);
       
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['CustomerSite']))
            $model->attributes = $_GET['CustomerSite'];

        $this->render('admin', array(
            'model' => $model,
            'customer'=>$customer,
            'cid' => $cid
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = CustomerSite::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'customer-site-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
    
    
     /**
     * Manages all models.
     */
    public function actionSites($cid = null) {
        $model = new CustomerSite('search',$cid);
        $customer = AgencyCustomer::model()->findByPk($cid);
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['CustomerSite']))
            $model->attributes = $_GET['CustomerSite'];

        $this->renderPartial('sites', array(
            'model' => $model,
            'customer'=>$customer,
            'cid' => $cid
        ),false,true);
    }


}
