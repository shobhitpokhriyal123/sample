<?php

class AgencyController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'selectQuote', 'corporate'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete', 'selectQuote', 'getAgency', 'updateQuote', 'deleteQuote'),
                'expression' => 'Yii::app()->user->getIsAdmin()',
            //'users' => array('admin'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete', 'selectQuote', 'getAgency'),
                'expression' => 'Yii::app()->user->getIsAdmin()',
            //'users' => array('admin'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete', 'selectQuote', 'getAgency'),
                'expression' => 'Yii::app()->user->getIsAdmin()',
            //'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Agency;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Agency'])) {
            $model->attributes = $_POST['Agency'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Agency'])) {
            $model->attributes = $_POST['Agency'];
            $model->password = '';


            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Agency');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Agency('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Agency']))
            $model->attributes = $_GET['Agency'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Agency::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'agency-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionCorporate($id) {
        $leadQuote = LeadQuote::model()->findByPk($id);
        $contract_details = ContractDetail::model()->findByAttributes(array('quote_id' => $id));
        if (!$contract_details) {
            $contract_details = new ContractDetail();
            $company = new Company();
            $business_address = new BusinessAddress;
            $home_address = new PartnerAddress;
            $bank = new Bank();
            $contract_Files = new ContractFiles();
        } else {
            $company = Company::model()->findByAttributes(array('contract_id' => $contract_details->id));
            $business_address = BusinessAddress::model()->findByAttributes(array('contract_id' => $contract_details->id));
            $home_address = PartnerAddress::model()->findByAttributes(array('contract_id' => $contract_details->id));
            $bank = Bank::model()->findByAttributes(array('contract_id' => $contract_details->id));
            $contract_Files = ContractFiles::model()->findByAttributes(array('contract_id' => $contract_details->id));
        }
        if (isset($_POST['ContractDetail'])) {
            $contract_details->attributes = $_POST['ContractDetail'];
            $contract_details->create_user_id = Yii::app()->user->id;
            $contract_details->is_corporate = 1;
            $contract_details->price_id = $id;
            $contract_details->quote_id = $id;
            if (isset($_POST['Company'])) {
                $company->attributes = $_POST['Company'];
                $company->user_id = Yii::app()->user->id;
                $company->create_user_id = Yii::app()->user->id;
            }

            if (isset($_POST['BusinessAddress'])) {
                $business_address->attributes = $_POST['BusinessAddress'];
                $business_address->user_id = Yii::app()->user->id;
                $business_address->create_user_id = Yii::app()->user->id;
            }

            if (isset($_POST['PartnerAddress'])) {
                $home_address->attributes = $_POST['PartnerAddress'];
                $home_address->user_id = Yii::app()->user->id;
                $home_address->create_user_id = Yii::app()->user->id;
            }

            if (isset($_POST['Bank'])) {
                $bank->attributes = $_POST['Bank'];
                $bank->user_id = Yii::app()->user->id;

                //  $bank->save();
            }
            if (isset($_POST['ContractFiles'])) {
                $contract_Files->attributes = $_POST['ContractFiles'];
                $contract_Files->saveUploadedFile($contract_Files, 'files');
                $contract_Files->user_id = Yii::app()->user->id;

                $contract_Files->save();
            }
            //    die;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $contract_details->save();
                $contract_Files->contract_id = $contract_details->id;
                $bank->contract_id = $contract_details->id;
                $home_address->contract_id = $contract_details->id;
                $business_address->contract_id = $contract_details->id;
                $company->contract_id = $contract_details->id;

                $contract_Files->save();
                $bank->save();
                $home_address->save();
                $business_address->save();
                $company->save();


//                $to = $company->contact_email;
//                $from = "noreply@utilitycloudlive.co.uk";
//                $sub = "Confirmation of your new supply for " . $contract_details->current_supplier;
//                $tmplate = "//mail/send_quotation";
//                $body = "Utility Cloud Live..!";
                //  $contract_details->sendMailTemplate($to, $from, $sub, $tmplate, $body, $contract_details, $business_address, $company);

                $transaction->commit();
                Yii::app()->user->setFlash('success', "Contract Saved Successfully...!");
                $this->redirect(array('/admin/contractDetail/admin'));
            } catch (Exception $e) {
                $transaction->rollBack();
                Yii::app()->user->setFlash('error', "{$e->getMessage()}");
                $this->refresh();
            }
        }
        $this->render('corporate', array(
            'company' => $company,
            'business_address' => $business_address,
            'home_address' => $home_address,
            'contract_details' => $contract_details,
            'bank' => $bank,
            'contract_Files' => $contract_Files,
            'quote' => $leadQuote
                //'price' => $price
                //'validation' => $validation
        ));
    }

    /**
     * 
     * @param type $price_id
     * @param type $quote_id
     */
    public function actionSelectQuote($price_id = null, $quote_id) {

        $company = new Company();
        $business_address = new BusinessAddress;
        $home_address = new PartnerAddress;
        $contract_details = new ContractDetail();
        $bank = new Bank();
        $contract_Files = new ContractFiles();
        $price = Price::model()->findByPk($price_id);
        // $validation = new Validation();

        if (isset($_POST['ContractDetail'])) {
            $contract_details->attributes = $_POST['ContractDetail'];
            $contract_details->create_user_id = Yii::app()->user->id;
            $contract_details->price_id = $price_id;
            $contract_details->quote_id = $quote_id;
            //if ($contract_details->save()) {
            if (isset($_POST['Company'])) {
                $company->attributes = $_POST['Company'];
                $company->user_id = Yii::app()->user->id;

                $company->create_user_id = Yii::app()->user->id;

                // $company->save();
            }

            if (isset($_POST['BusinessAddress'])) {
                $business_address->attributes = $_POST['BusinessAddress'];
                $business_address->user_id = Yii::app()->user->id;
                $business_address->create_user_id = Yii::app()->user->id;

                //  $business_address->save();
            }

            if (isset($_POST['PartnerAddress'])) {
                $home_address->attributes = $_POST['PartnerAddress'];
                $home_address->user_id = Yii::app()->user->id;
                $home_address->create_user_id = Yii::app()->user->id;

                // $home_address->save();
            }

            if (isset($_POST['Bank'])) {
                $bank->attributes = $_POST['Bank'];
                $bank->user_id = Yii::app()->user->id;

                //  $bank->save();
            }
            if (isset($_POST['ContractFiles'])) {
                $contract_Files->attributes = $_POST['ContractFiles'];
                $contract_Files->saveUploadedFile($contract_Files, 'files');
                $contract_Files->user_id = Yii::app()->user->id;

                $contract_Files->save();
            }
//            if (isset($_POST['Validation'])) {
//                $validation->attributes = $_POST['Validation'];
//                $validation->user_id = Yii::app()->user->id;
//                $validation->contract_id = $contract_details->id;
//                //$validation->save();
//            }
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $contract_details->save();
                $contract_Files->contract_id = $contract_details->id;
                $bank->contract_id = $contract_details->id;
                $home_address->contract_id = $contract_details->id;
                $business_address->contract_id = $contract_details->id;
                $company->contract_id = $contract_details->id;
                // $validation->save();
                $contract_Files->save();
                $bank->save();
                $home_address->save();
                $business_address->save();
                $company->save();


                $to = $company->contact_email;
                $from = "noreply@utilitycloudlive.co.uk";
                $sub = "Confirmation of your new supply for " . $contract_details->current_supplier;
                $tmplate = "//mail/send_quotation";
                $body = "Utility Cloud Live..!";

                $contract_details->sendMailTemplate($to, $from, $sub, $tmplate, $body, $contract_details, $business_address, $company);

                $transaction->commit();
                Yii::app()->user->setFlash('success', "Contract Saved Successfully...!");
                $this->redirect(array('/admin/contractDetail/admin'));
            } catch (Exception $e) {
                $transaction->rollBack();
                Yii::app()->user->setFlash('error', "{$e->getMessage()}");
                $this->refresh();
            }
            // }
        }


        $this->render('selectQuote', array(
            'company' => $company,
            'business_address' => $business_address,
            'home_address' => $home_address,
            'contract_details' => $contract_details,
            'bank' => $bank,
            'contract_Files' => $contract_Files,
            'price' => $price
                //'validation' => $validation
        ));
    }

    public function actionGetAgent($id) {
//        $agency = Yii::app()->db->createCommand()
//                ->select('*')
//                ->from('tbl_agency')
//                ->queryRow();
//        echo $agency->user->username;
//        echo '<pre>'; print_r($agency); die;
//        header('Content-Type: application/json');
//        echo json_encode($agency);
        exit;
    }

    public function actionUpdateQuote($id) {
        $contract_details = ContractDetail::model()->findByPk($id);
        $company = Company::model()->findByAttributes(array('contract_id' => $contract_details->id));
        $business_address = BusinessAddress::model()->findByAttributes(array('contract_id' => $contract_details->id));
        $home_address = PartnerAddress::model()->findByAttributes(array('contract_id' => $contract_details->id));
        $price = Price::model()->findByPk($contract_details->price_id);
        $bank = Bank::model()->findByAttributes(array('contract_id' => $contract_details->id));
        $contract_Files = ContractFiles::model()->findByAttributes(array('contract_id' => $contract_details->id));
        if (isset($_POST['ContractDetail'])) {
            $contract_details->attributes = $_POST['ContractDetail'];
            $contract_details->save();


            if (isset($_POST['ContractFiles'])) {
                $contract_Files->attributes = $_POST['ContractFiles'];
                $contract_Files->saveUploadedFile($contract_Files, 'files');
                $contract_Files->save();
            }
            if (isset($_POST['Bank'])) {
                $bank->attributes = $_POST['Bank'];
                $bank->save();
            }
            if (isset($_POST['PartnerAddress'])) {
                $home_address->attributes = $_POST['PartnerAddress'];
                $home_address->save();
            }
            if (isset($_POST['BusinessAddress'])) {
                $business_address->attributes = $_POST['BusinessAddress'];
                $business_address->save();
            }
            if (isset($_POST['Company'])) {
                $company->attributes = $_POST['Company'];
                $company->save();
            }
            Yii::app()->user->setFlash('success', "Contract Saved Successfully...!");
            $this->redirect(array('/admin/contractDetail/admin'));
        }

        $this->render('selectQuote', array(
            'company' => $company,
            'business_address' => $business_address,
            'home_address' => $home_address,
            'contract_details' => $contract_details,
            'bank' => $bank,
            'contract_Files' => $contract_Files,
            'price' => $price
                //'validation' => $validation
        ));



        //$price = Price::model()->findByPk($price_id);
    }

    public function actionDeleteQuote($id) {
        if (Yii::app()->request->isPostRequest) {
            $contract_details = ContractDetail::model()->findByPk($id);
            $company = Company::model()->deleteAllByAttributes(array('contract_id' => $contract_details->id));
            $business_address = BusinessAddress::model()->deleteAllByAttributes(array('contract_id' => $contract_details->id));
            $home_address = PartnerAddress::model()->deleteAllByAttributes(array('contract_id' => $contract_details->id));
            $bank = Bank::model()->deleteAllByAttributes(array('contract_id' => $contract_details->id));
            $contract_Files = ContractFiles::model()->deleteAllByAttributes(array('contract_id' => $contract_details->id));
            $contract_details->delete();
            $this->redirect(array('/admin/contractDetail/admin'));
        } else {
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
        }
    }

}
