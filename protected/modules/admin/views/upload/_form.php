<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'upload-form',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php //echo $form->errorSummary($model); ?>

<?php echo $form->dropdownListRow($model, 'supplier_id', CHtml::listData(Users::model()->findAllByAttributes(array('role_id' => 2)), 'id', 'fullname'), array('empty' => '--- Select Supplier ---')); ?>



<?php echo $form->dropdownListRow($model, 'doctype_id', CHtml::listData(Doctype::model()->findAll(), 'id', 'title')); ?>

<?php echo $form->fileFieldRow($model, 'document', array('class' => 'span5', 'maxlength' => 255)); ?>

<?php echo $form->hiddenField($model, 'status', array('value' => 1)); ?>
<hr>
<div class="form-actions">
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type' => 'primary',
        'label' => $model->isNewRecord ? 'Create' : 'Save',
    ));
    ?>
</div>

<?php $this->endWidget(); ?>
