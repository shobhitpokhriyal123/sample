<section id="manage_products">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="card-title">
                        <strong>Manage Supplier Documents</strong>
                    </div>

                    <?php
                    if (Yii::app()->user->isAdmin) {
                        echo CHtml::link('Upload Document', array('/admin/upload/create'), array("class" => 'btn btn-success'));
                    }
                    ?>

                </div>
                <div class="card-body">
                    <?php
                    $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => 'upload-grid',
                        'dataProvider' => $model->search(),
                        'afterAjaxUpdate' => 'function(id,options){ paginationControls(); }',
                        'filter' => $model,
                        'columns' => array(
                            // 'id',
                            'supplier_id' => array(
                                'name' => 'supplier_id',
                                'type' => 'raw',
                                'filter' => CHtml::listData(Users::model()->findAllByAttributes(array("role_id" => 2)), "id", "fullname"),
                                'value' => '($data->supplier) ? $data->supplier->username : "N/A"',
                            ),
                            'doctype_id' => array(
                                'name' => 'doctype_id',
                                'value' => '($data->type)?$data->type->title : "N/A"',
                                'type' => 'raw',
                                'filter' => CHtml::listData(Doctype::model()->findAll(), "id", "title"),
                            ),
                            'document' => array(
                                'filter' => false,
                                'name' => 'document',
                                'type' => 'raw',
                                'value' => 'CHtml::link("Download",array("upload/download/file/".$data->document))'
                            ),
                            //'status'=>array,
                            array(
                                'class' => 'bootstrap.widgets.TbButtonColumn',
                                'template' => '{view}{update}{delete}',
                                //  'evaluateID'=>true,
                                'header' => 'Actions',
                                'buttons' => array(
                                    'view' => array(
                                        //  'label' => '<button class="btn btn-primary">Save</button>',
                                        // 'url' => 'Yii::app()->createAbsoluteUrl("admin/quote/create", array("id"=>$data->id))',
                                        'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'View'),
                                    ),
                                    'update' => array(
                                        // 'label' => '<button class="btn btn-primary">Save</button>',
                                        // 'url' => 'Yii::app()->createAbsoluteUrl("admin/quote/create", array("id"=>$data->id))',
                                        'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Update'),
                                        'visible' => 'Yii::app()->user->isAdmin'
                                    ),
                                    'delete' => array(
                                        //  'label' => '<button class="btn btn-primary">Save</button>',
                                        // 'url' => 'Yii::app()->createAbsoluteUrl("admin/quote/create", array("id"=>$data->id))',
                                        'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Delete'),
                                        'visible' => 'Yii::app()->user->isAdmin'
                                    ),
                                ),
                            ),
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
