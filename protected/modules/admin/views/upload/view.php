<section id="manage_products">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="card-title">
                        <strong>View Supplier Document</strong>
                    </div>

                    <?php
                    if (Yii::app()->user->isAdmin) {
                        echo CHtml::link('Manage Documents', array('/admin/upload/admin'), array("class" => 'btn btn-success'));
                    }
                    ?>

                </div>
                <div class="card-body">
                    <?php
                    $this->widget('bootstrap.widgets.TbDetailView', array(
                        'data' => $model,
                        'attributes' => array(
                            //  'id',
                            'supplier_id' => array(
                                'name' => 'supplier_id',
                                'value' => $model->supplier->username
                            ),
                            'doctype_id'=> array(
                                'name' => 'doctype_id',
                                'value' => $model->type->title
                            ),
                            'document'=> array(
                                'name' => 'document',
                                'type'=>'raw',
                                'value' => CHtml::link('Download',array('upload/download/file/'.$model->document))
                            ),
                           // 'status',
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
