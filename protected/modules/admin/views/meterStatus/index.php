<?php
$this->breadcrumbs=array(
	'Meter Statuses',
);

$this->menu=array(
	array('label'=>'Create MeterStatus','url'=>array('create')),
	array('label'=>'Manage MeterStatus','url'=>array('admin')),
);
?>

<h1>Meter Statuses</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
