<?php
$this->breadcrumbs=array(
	'Meter Statuses'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List MeterStatus','url'=>array('index')),
	array('label'=>'Create MeterStatus','url'=>array('create')),
	array('label'=>'Update MeterStatus','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete MeterStatus','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MeterStatus','url'=>array('admin')),
);
?>

<h1>View MeterStatus #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'title',
	),
)); ?>
