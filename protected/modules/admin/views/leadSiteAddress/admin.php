
<?php

$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'lead-site-address-grid',
    'dataProvider' => $model->search(),
      'afterAjaxUpdate' => 'function(id,options){ paginationControls(); }',
    'filter' => $model,
    'columns' => array(
        //'id',
        //'create_user_id',
//		'lead_id',
//		'site_id',
        'lead_id' => array(
            'filter' => false, //CHtml::listData(Users::getAllSuppliers(), 'id', 'fullname'), 'type' => 'html',
            'header' => 'Lead',
            'name' => 'lead_id',
            'value' => '$data->site->lead->first_name'
        ),
        'site_id' => array(
            'filter' => false, //CHtml::listData(Users::getAllSuppliers(), 'id', 'fullname'), 'type' => 'html',
            'header' => 'Site',
            'name' => 'site_id',
            'value' => '$data->site->site_name'
        ),
//        'site_post_code',
//        'site_address_name',
//        'site_address_no',
//        'site_street1',
//        'site_street2',
//        'site_town',
//        'site_country',
        /* 'site_address_same',
          'business_address_same',
          'billing_post_code',
          'billing_name',
          'billing_no',
          'street_address_1',
          'street_address_2',
          'town',
          'country',
          'created',
         */
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{update}',
            'header' => 'Actions',
            'buttons' => array(
                'update' => array(
                    'label' => '<button class="btn btn-primary">Update</button>',
                    'url' => 'Yii::app()->createAbsoluteUrl("admin/leadSiteAddress/addressupdate", array("id"=>$data->id))',
                    'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Update'),
                    'imageUrl' => false,
                    'click' => "function(e){
                               e.preventDefault();
                               var url = $(this).attr('href');
                               $.ajax({
                                 type: 'POST',
                                 url: url,
                                 success: function(res){
                                 res = $.parseJSON(res);
                              
                                for (var property in res) {
                                        if (res.hasOwnProperty(property)) { 
                                        
                                         $('#LeadSiteAddress_'+property).val(res[property]);
                                    }
                                }
                                    var current = $('#lead-site-address-form').attr('action');
                                    $('#lead-site-address-form').attr('action',current+'&id='+res.id);
                                 },
                                });
                                
                           
                            }",
                ),
            ),
        ),
    ),
));
?>
