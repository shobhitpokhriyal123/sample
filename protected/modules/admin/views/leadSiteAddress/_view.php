<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_user_id')); ?>:</b>
	<?php echo CHtml::encode($data->create_user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lead_id')); ?>:</b>
	<?php echo CHtml::encode($data->lead_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('site_id')); ?>:</b>
	<?php echo CHtml::encode($data->site_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('site_post_code')); ?>:</b>
	<?php echo CHtml::encode($data->site_post_code); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('site_address_name')); ?>:</b>
	<?php echo CHtml::encode($data->site_address_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('site_address_no')); ?>:</b>
	<?php echo CHtml::encode($data->site_address_no); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('site_street1')); ?>:</b>
	<?php echo CHtml::encode($data->site_street1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('site_street2')); ?>:</b>
	<?php echo CHtml::encode($data->site_street2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('site_town')); ?>:</b>
	<?php echo CHtml::encode($data->site_town); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('site_country')); ?>:</b>
	<?php echo CHtml::encode($data->site_country); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('site_address_same')); ?>:</b>
	<?php echo CHtml::encode($data->site_address_same); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('business_address_same')); ?>:</b>
	<?php echo CHtml::encode($data->business_address_same); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('billing_post_code')); ?>:</b>
	<?php echo CHtml::encode($data->billing_post_code); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('billing_name')); ?>:</b>
	<?php echo CHtml::encode($data->billing_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('billing_no')); ?>:</b>
	<?php echo CHtml::encode($data->billing_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('street_address_1')); ?>:</b>
	<?php echo CHtml::encode($data->street_address_1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('street_address_2')); ?>:</b>
	<?php echo CHtml::encode($data->street_address_2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('town')); ?>:</b>
	<?php echo CHtml::encode($data->town); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('country')); ?>:</b>
	<?php echo CHtml::encode($data->country); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />

	*/ ?>

</div>