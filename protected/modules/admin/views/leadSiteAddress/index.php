<?php
$this->breadcrumbs=array(
	'Lead Site Addresses',
);

$this->menu=array(
	array('label'=>'Create LeadSiteAddress','url'=>array('create')),
	array('label'=>'Manage LeadSiteAddress','url'=>array('admin')),
);
?>

<h1>Lead Site Addresses</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
