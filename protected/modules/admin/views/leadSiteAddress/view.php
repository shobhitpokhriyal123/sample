<?php
$this->breadcrumbs=array(
	'Lead Site Addresses'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List LeadSiteAddress','url'=>array('index')),
	array('label'=>'Create LeadSiteAddress','url'=>array('create')),
	array('label'=>'Update LeadSiteAddress','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete LeadSiteAddress','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage LeadSiteAddress','url'=>array('admin')),
);
?>

<h1>View LeadSiteAddress #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'create_user_id',
		'lead_id',
		'site_id',
		'site_post_code',
		'site_address_name',
		'site_address_no',
		'site_street1',
		'site_street2',
		'site_town',
		'site_country',
		'site_address_same',
		'business_address_same',
		'billing_post_code',
		'billing_name',
		'billing_no',
		'street_address_1',
		'street_address_2',
		'town',
		'country',
		'created',
	),
)); ?>
