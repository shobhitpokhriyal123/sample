<?php
$this->breadcrumbs=array(
	'Lead Site Addresses'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List LeadSiteAddress','url'=>array('index')),
	array('label'=>'Create LeadSiteAddress','url'=>array('create')),
	array('label'=>'View LeadSiteAddress','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage LeadSiteAddress','url'=>array('admin')),
);
?>

<h1>Update LeadSiteAddress <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>