<?php
$this->breadcrumbs=array(
	'Lead Site Addresses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List LeadSiteAddress','url'=>array('index')),
	array('label'=>'Manage LeadSiteAddress','url'=>array('admin')),
);
?>

<h1>Create LeadSiteAddress</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>