<?php
/* @var $this CustomerTenderController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Customer Tenders',
);

$this->menu=array(
	array('label'=>'Create CustomerTender', 'url'=>array('create')),
	array('label'=>'Manage CustomerTender', 'url'=>array('admin')),
);
?>

<h1>Customer Tenders</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
