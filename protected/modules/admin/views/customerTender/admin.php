<?php
/* @var $this CustomerTenderController */
/* @var $model CustomerTender */

$this->breadcrumbs=array(
	'Customer Tenders'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List CustomerTender', 'url'=>array('index')),
	array('label'=>'Create CustomerTender', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#customer-tender-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<!--<h1>Manage Customer Tenders</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div> 

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'customer-tender-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'contract_id',
		'create_user_id',
		'contract_duration',
		'supplier_ids',
		'bespoke_duration',
		/*
		'commission_discount',
		'tender_return_date',
		'business_name',
		'com_reg',
		'commodity_type',
		'site_no',
		'uplift',
		'term_length',
		'fixed_meter',
		'contract_type',
		'payment',
		'energy',
		'supplier_pref',
		'comments',
		'created',
		*/
            
             array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{view}{update}{delete}',
            'header' => 'Tender',
            'buttons' => array(
                'view' => array(
                    'label' => '<button class="btn btn-primary">View Tender</button>',
                    'url' => 'Yii::app()->createAbsoluteUrl("admin/customerTender/view", array("id"=>$data->id))',
                  //  'options' => array('title' => 'Create Contract'),
                    'imageUrl' => false,
                ),
                 'update' => array(
                    'label' => '<button class="btn btn-primary">update Tender</button>',
                    'url' => 'Yii::app()->createAbsoluteUrl("admin/customerTender/update", array("id"=>$data->id))',
                   // 'options' => array('title' => 'Create Contract'),
                    'imageUrl' => false,
                ),
            ),
        ),
            
//		array(
//			'class'=>'CButtonColumn',
//		),
	),
)); ?>
