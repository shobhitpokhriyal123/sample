<?php
/* @var $this CustomerTenderController */
/* @var $model CustomerTender */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'contract_id'); ?>
		<?php echo $form->textField($model,'contract_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'create_user_id'); ?>
		<?php echo $form->textField($model,'create_user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'contract_duration'); ?>
		<?php echo $form->textField($model,'contract_duration'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'supplier_ids'); ?>
		<?php echo $form->textField($model,'supplier_ids',array('size'=>60,'maxlength'=>64)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'bespoke_duration'); ?>
		<?php echo $form->textField($model,'bespoke_duration'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'commission_discount'); ?>
		<?php echo $form->textField($model,'commission_discount'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tender_return_date'); ?>
		<?php echo $form->textField($model,'tender_return_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'business_name'); ?>
		<?php echo $form->textField($model,'business_name',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'com_reg'); ?>
		<?php echo $form->textField($model,'com_reg',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'commodity_type'); ?>
		<?php echo $form->textArea($model,'commodity_type',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'site_no'); ?>
		<?php echo $form->textField($model,'site_no'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'uplift'); ?>
		<?php echo $form->textField($model,'uplift'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'term_length'); ?>
		<?php echo $form->textField($model,'term_length',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fixed_meter'); ?>
		<?php echo $form->textField($model,'fixed_meter',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'contract_type'); ?>
		<?php echo $form->textField($model,'contract_type',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'payment'); ?>
		<?php echo $form->textField($model,'payment',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'energy'); ?>
		<?php echo $form->textField($model,'energy',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'supplier_pref'); ?>
		<?php echo $form->textField($model,'supplier_pref',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'comments'); ?>
		<?php echo $form->textArea($model,'comments',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created'); ?>
		<?php echo $form->textField($model,'created'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->