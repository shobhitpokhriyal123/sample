<?php
/* @var $this CustomerTenderController */
/* @var $model CustomerTender */

$this->breadcrumbs=array(
	'Customer Tenders'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List CustomerTender', 'url'=>array('index')),
	array('label'=>'Create CustomerTender', 'url'=>array('create')),
	array('label'=>'View CustomerTender', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage CustomerTender', 'url'=>array('admin')),
);
?>

<h1>Update CustomerTender <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>