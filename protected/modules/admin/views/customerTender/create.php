<?php
/* @var $this CustomerTenderController */
/* @var $model CustomerTender */

$this->breadcrumbs=array(
	'Customer Tenders'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List CustomerTender', 'url'=>array('index')),
	array('label'=>'Manage CustomerTender', 'url'=>array('admin')),
);
?>

<h1>Create CustomerTender</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>