<?php
/* @var $this CustomerTenderController */
/* @var $model CustomerTender */

$this->breadcrumbs=array(
	'Customer Tenders'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List CustomerTender', 'url'=>array('index')),
	array('label'=>'Create CustomerTender', 'url'=>array('create')),
	array('label'=>'Update CustomerTender', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete CustomerTender', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage CustomerTender', 'url'=>array('admin')),
);
?>

<h1>View CustomerTender #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'contract_id',
		'create_user_id',
		'contract_duration',
		'supplier_ids',
		'bespoke_duration',
		'commission_discount',
		'tender_return_date',
		'business_name',
		'com_reg',
		'commodity_type',
		'site_no',
		'uplift',
		'term_length',
		'fixed_meter',
		'contract_type',
		'payment',
		'energy',
		'supplier_pref',
		'comments',
		'created',
	),
)); ?>
