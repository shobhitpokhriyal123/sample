<?php
/* @var $this CustomerTenderController */
/* @var $model CustomerTender */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'customer-tender-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'contract_id'); ?>
		<?php echo $form->textField($model,'contract_id'); ?>
		<?php echo $form->error($model,'contract_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'create_user_id'); ?>
		<?php echo $form->textField($model,'create_user_id'); ?>
		<?php echo $form->error($model,'create_user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'contract_duration'); ?>
		<?php echo $form->textField($model,'contract_duration'); ?>
		<?php echo $form->error($model,'contract_duration'); ?>
	</div>

	<div class="row">
		<?php //echo $form->labelEx($model,'supplier_ids'); ?>
		<?php //echo $form->textField($model,'supplier_ids',array('size'=>60,'maxlength'=>64)); ?>
		<?php// echo $form->error($model,'supplier_ids'); ?>
            
            <?php
if($model->supplier_ids)
    $model->supplier_ids = explode (",", $model->supplier_ids);
$ab = CHtml::listData(Users::model()->findAllByAttributes(array('role_id' => 2)), 'id', 'fullname');
//echo $form->checkBoxListRow($l_tender, 'supplier_ids', CHtml::listData(Users::model()->findAllByAttributes(array('role_id' => 2)), 'id', 'fullname')); 
 echo $form->dropDownList($model, 'supplier_ids', $ab , array('prompt' => 'Select', 'multiple' => true, 'selected' => 'selected'));
 ?>

	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bespoke_duration'); ?>
		<?php echo $form->textField($model,'bespoke_duration'); ?>
		<?php echo $form->error($model,'bespoke_duration'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'commission_discount'); ?>
		<?php echo $form->textField($model,'commission_discount'); ?>
		<?php echo $form->error($model,'commission_discount'); ?>
	</div>

	<div class="row">
		<?php //echo $form->labelEx($model,'tender_return_date'); ?>
		<?php //echo $form->textField($model,'tender_return_date'); ?>
		<?php //echo $form->error($model,'tender_return_date'); ?>
            <?php echo $form->label($l_tender, 'tender_return_date'); ?>
<?php
$this->widget('zii.widgets.jui.CJuiDatePicker', array(
    'model' => $l_tender,
    'attribute' => 'tender_return_date',
    'options' => array(
        'dateFormat' => 'yy-mm-dd',
        'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
        'changeMonth' => true,
        'changeYear' => true,
    ),
));
?>
    
<?php echo $form->error($l_tender, 'tender_return_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'business_name'); ?>
		<?php echo $form->textField($model,'business_name',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'business_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'com_reg'); ?>
		<?php echo $form->textField($model,'com_reg',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'com_reg'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'commodity_type'); ?>
		<?php echo $form->textArea($model,'commodity_type',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'commodity_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'site_no'); ?>
		<?php echo $form->textField($model,'site_no'); ?>
		<?php echo $form->error($model,'site_no'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'uplift'); ?>
		<?php echo $form->textField($model,'uplift'); ?>
		<?php echo $form->error($model,'uplift'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'term_length'); ?>
		<?php echo $form->textField($model,'term_length',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'term_length'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fixed_meter'); ?>
		<?php echo $form->textField($model,'fixed_meter',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'fixed_meter'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'contract_type'); ?>
		<?php echo $form->textField($model,'contract_type',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'contract_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'payment'); ?>
		<?php echo $form->textField($model,'payment',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'payment'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'energy'); ?>
		<?php echo $form->textField($model,'energy',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'energy'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'supplier_pref'); ?>
		<?php echo $form->textField($model,'supplier_pref',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'supplier_pref'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'comments'); ?>
		<?php echo $form->textArea($model,'comments',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'comments'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created'); ?>
		<?php echo $form->textField($model,'created'); ?>
		<?php echo $form->error($model,'created'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->