<?php
$this->breadcrumbs=array(
	'Lead Statuses',
);

$this->menu=array(
	array('label'=>'Create LeadStatus','url'=>array('create')),
	array('label'=>'Manage LeadStatus','url'=>array('admin')),
);
?>

<h1>Lead Statuses</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
