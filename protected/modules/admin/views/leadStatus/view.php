<section id="create_product">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <strong>View Lead Status</strong>
                    </div>
                    <?php
                    if (Yii::app()->user->isAdmin) {
                        echo CHtml::link('Manage Lead Status', array('/admin/leadStatus'), array("class" => 'btn btn-success'));
                    }
                    ?>
                </div>
                <div class="card-body">

                    <?php
                    $this->widget('bootstrap.widgets.TbDetailView', array(
                        'data' => $model,
                        'attributes' => array(
                            'id',
                            'title',
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
