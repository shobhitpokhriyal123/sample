<?php
$this->breadcrumbs=array(
	'Validations'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Validation','url'=>array('index')),
	array('label'=>'Create Validation','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('validation-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Validations</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'validation-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'credit',
		'user_id',
		'contract_id',
		'meter_details',
		'consumption',
		/*
		'paper_contract',
		'cot_form',
		'electorial_check',
		'verbal_recording',
		'duplicate_check',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
