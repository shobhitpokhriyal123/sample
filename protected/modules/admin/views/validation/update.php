<?php
$this->breadcrumbs=array(
	'Validations'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Validation','url'=>array('index')),
	array('label'=>'Create Validation','url'=>array('create')),
	array('label'=>'View Validation','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Validation','url'=>array('admin')),
);
?>

<h1>Update Validation <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>