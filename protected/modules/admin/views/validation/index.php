<?php
$this->breadcrumbs=array(
	'Validations',
);

$this->menu=array(
	array('label'=>'Create Validation','url'=>array('create')),
	array('label'=>'Manage Validation','url'=>array('admin')),
);
?>

<h1>Validations</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
