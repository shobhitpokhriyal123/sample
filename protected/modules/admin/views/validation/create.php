<?php
$this->breadcrumbs=array(
	'Validations'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Validation','url'=>array('index')),
	array('label'=>'Manage Validation','url'=>array('admin')),
);
?>

<h1>Create Validation</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>