<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('credit')); ?>:</b>
	<?php echo CHtml::encode($data->credit); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contract_id')); ?>:</b>
	<?php echo CHtml::encode($data->contract_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meter_details')); ?>:</b>
	<?php echo CHtml::encode($data->meter_details); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('consumption')); ?>:</b>
	<?php echo CHtml::encode($data->consumption); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('paper_contract')); ?>:</b>
	<?php echo CHtml::encode($data->paper_contract); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('cot_form')); ?>:</b>
	<?php echo CHtml::encode($data->cot_form); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('electorial_check')); ?>:</b>
	<?php echo CHtml::encode($data->electorial_check); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('verbal_recording')); ?>:</b>
	<?php echo CHtml::encode($data->verbal_recording); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('duplicate_check')); ?>:</b>
	<?php echo CHtml::encode($data->duplicate_check); ?>
	<br />

	*/ ?>

</div>