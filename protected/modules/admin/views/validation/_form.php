<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'validation-form',
    'enableAjaxValidation' => false,
        ));
?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->hiddenField($model, 'contract_id', array('class' => 'span5', 'value' => $modelid)); ?>
<?php echo $form->hiddenField($model, 'user_id', array('class' => 'span5', 'value' => Yii::app()->user->id)); ?>
<div class="inlineRadio">
<?php echo $form->radioButtonListRow($model, 'credit', Validation::getPassFail()); ?>
</div>
<div class="inlineRadio">
<?php echo $form->radioButtonListRow($model, 'meter_details', Validation::getPassFail()); ?>
</div>
<div class="inlineRadio">
<?php echo $form->radioButtonListRow($model, 'consumption', Validation::getPassFail()); ?>
</div>
<div class="inlineRadio">
<?php echo $form->radioButtonListRow($model, 'paper_contract', Validation::getPassFail()); ?>
</div>
<div class="inlineRadio">
<?php echo $form->radioButtonListRow($model, 'cot_form', Validation::getPassFail()); ?>
</div>
<div class="inlineRadio">
<?php echo $form->radioButtonListRow($model, 'electorial_check', Validation::getPassFail()); ?>
</div>
<div class="inlineRadio">
<?php echo $form->radioButtonListRow($model, 'verbal_recording', Validation::getPassFail()); ?>
</div>
<div class="inlineRadio">
<?php echo $form->radioButtonListRow($model, 'duplicate_check', Validation::getPassFail()); ?>
</div>

<?php $this->endWidget(); ?>
<span class="success"></span>
<div class="form-actions">
    <button id="save_settings" class="btn btn-primary">Save</button>
    <?php
//    $this->widget('bootstrap.widgets.TbButton', array(
//        'buttonType' => 'submit',
//        'type' => 'primary',
//        'label' => $model->isNewRecord ? 'Create' : 'Save',
//    ));
    ?>
</div>

<script>
    $(document).ready(function () {
        $("#save_settings").click(function (e) {
            var id = '<?php echo $model->id; ?>';
            alert(id);
            var data = $("#validation-form").serialize();
            if (id)
                var url = '<?php echo Yii::App()->createAbsoluteUrl("admin/validation/update&id=") ?>' + id;
            else
                var url = '<?php echo Yii::App()->createAbsoluteUrl("admin/validation/create") ?>';
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                success: function (res) {
                    $('.success').html('Settings saved successfully...!');
                }
            });
        });
    });
</script>
