<?php
$this->breadcrumbs=array(
	'Validations'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Validation','url'=>array('index')),
	array('label'=>'Create Validation','url'=>array('create')),
	array('label'=>'Update Validation','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Validation','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Validation','url'=>array('admin')),
);
?>

<h1>View Validation #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'credit',
		'user_id',
		'contract_id',
		'meter_details',
		'consumption',
		'paper_contract',
		'cot_form',
		'electorial_check',
		'verbal_recording',
		'duplicate_check',
	),
)); ?>
