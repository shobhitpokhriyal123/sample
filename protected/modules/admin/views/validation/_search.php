<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'credit',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'user_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'contract_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'meter_details',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'consumption',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'paper_contract',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'cot_form',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'electorial_check',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'verbal_recording',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'duplicate_check',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
