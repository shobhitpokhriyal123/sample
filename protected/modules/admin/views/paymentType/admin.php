
<section id="manage_products">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="card-title">
                        <strong>Manage Payment Type</strong>
                    </div>

                    <?php
                    if (Yii::app()->user->isAdmin) {
                        echo CHtml::link('Create Payment Base', array('/admin/paymentType/create'), array("class" => 'btn btn-success'));
                    }
                    ?>

                </div>
                <div class="card-body">
                    <?php
                    $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => 'payment-type-grid',
                        'dataProvider' => $model->search(),
                        'filter' => $model,
                        'columns' => array(
                            'id',
                            'title',
                            array(
                                'class' => 'bootstrap.widgets.TbButtonColumn',
                                'template' => '{view}{update}'
                            ),
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
