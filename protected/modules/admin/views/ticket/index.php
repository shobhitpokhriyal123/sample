<?php
//$this->breadcrumbs=array(
//	'Tickets',
//);
//
//$this->menu=array(
//	array('label'=>'Create Ticket','url'=>array('create')),
//	array('label'=>'Manage Ticket','url'=>array('admin')),
//);
?>

<section id="manage_products">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="card-title">
                        <strong>Tickets</strong>
                    </div>

                    <?php
                    if (Yii::app()->user->isAgency || Yii::app()->user->isAgent) {
                        echo CHtml::link('Create Ticket', array('/admin/ticket/create'), array("class" => 'btn btn-success'));
                      echo CHtml::link('Manage Ticket', array('/admin/ticket'), array("class" => 'btn btn-success'));  
                    }
                    ?>

                </div>
                <div class="card-body">

                    <?php
                    $this->widget('bootstrap.widgets.TbListView', array(
                        'dataProvider' => $dataProvider,
                        'itemView' => '_view',
                    ));
                    ?>


                </div>   
            </div> 
        </div>  
    </div>
</section>