<?php
//$this->breadcrumbs=array(
//	'Tickets'=>array('index'),
//	'Create',
//);
//
//$this->menu=array(
//	array('label'=>'List Ticket','url'=>array('index')),
//	array('label'=>'Manage Ticket','url'=>array('admin')),
//);
?>

<section id="manage_products">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="card-title">
                        <strong>Create Ticket</strong>
                    </div>
                     <?php
//                    if (Yii::app()->user->isAgency || Yii::app()->user->isUser) {
                       echo CHtml::link('Manage Tickets', array('/admin/ticket/admin'), array("class" => 'btn btn-success'));
//                          }
                    ?>

                </div>
                <div class="card-body">

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>

                </div> 
            </div>
        </div>
    </div>
</section>
