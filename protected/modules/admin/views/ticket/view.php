
<section id="manage_products">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="card-title">
                        <strong>View Ticket</strong>
                    </div>

                    <?php
                    if (Yii::app()->user->isAgency || Yii::app()->user->isUser) {
                        echo CHtml::link('Create Ticket', array('/admin/ticket/create'), array("class" => 'btn btn-success'));
                        echo CHtml::link('My Tickets', array('/admin/ticket/admin'), array("class" => 'btn btn-success'));
                    }
                    if (Yii::app()->user->isAdmin) {
                        echo CHtml::link('Manage Tickets', array('/admin/ticket/admin'), array("class" => 'btn btn-success'));
                    }
                    ?>

                </div>
                <div class="card-body">
                    <?php
                    $this->widget('bootstrap.widgets.TbDetailView', array(
                        'data' => $model,
                        'attributes' => array(
                            // 'id',
                            'agent_id' => array(
                                'name' => 'agent_id',
                                'value' => $model->agent->username
                            ),
                            'utility' => array(
                                'name' => 'utility',
                                'value' => Product::getUtilites($model->utility)
                            ),
                            'corporate_sme' => array(
                                'name' => 'corporate_sme',
                                'value' => Product::getCorporateSME($model->corporate_sme)
                            ),
                            'ticket_type' => array(
                                'name' => 'ticket_type',
                                'value' => TicketType::getTicketType($model->ticket_type)
                            ),
                            'query_type' => array(
                                'name' => 'query_type',
                                'value' => TicketType::getQueryType($model->query_type)
                            ),
                            'title',
                            'description',
                            'created',
                        ),
                    ));
                    ?>



                    <hr>
                    <div class="row-fluid">

                        <?php
                        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                            'id' => 'reply-form',
                            'action' => $this->createAbsoluteUrl('/admin/reply/create', array("tid"=>$model->id)),
                            'enableAjaxValidation' => false,
                            'enableClientValidation' => true,
                            'clientOptions' => array(
                                'validateOnSubmit' => true,
                            ),
                        ));
                        ?>

                        <p class="help-block">Fields with <span class="required">*</span> are required.</p>

                        <?php
                        $model1 = new Reply;
                        ?>

                        <?php echo $form->hiddenField($model1, 'ticket_id', array('class' => 'span5 form-control', 'value' => $model->id)); ?>

                        <?php echo $form->hiddenField($model1, 'create_user_id', array('class' => 'span5', 'value' => Yii::app()->user->id)); ?>

                        <?php echo $form->textAreaRow($model1, 'description', array('rows' => 6, 'cols' => 50, 'class' => 'span8 form-control')); ?>

                        <div class="form-actions">
                            <?php
                            $this->widget('bootstrap.widgets.TbButton', array(
                                'buttonType' => 'submit',
                                'type' => 'primary',
                                'label' => $model1->isNewRecord ? 'Reply' : 'Save',
                            ));
                            ?>
                        </div>

                        <?php $this->endWidget(); ?>

                    </div>
                    <hr>
                    <h4> Replies</h4>
                    <?php
                    $replies = $model->replies;

                    if ($replies) {
                        ?>
                        <div class="row-fluid">
                            <div class="card-title">

                                <?php foreach ($replies as $replie) { ?> 
                                    <b>
                                        <?php echo $replie->createUser->username ?> Says:
                                    </b>
                                    <p>
                                        <?php
                                        echo $replie->description . '<br>';
                                        ?>
                                    </p>
                                    <?php
                                }
                            } else {
                                echo 'No reply(s) found..!';
                            }
                            ?>

                            <hr>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>