<section id="managse_products">
    <div class="">

        <div class="card">
                <div class="card-header">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="card-title">
                        <strong>Update Status</strong>
                    </div>
                </div>
            <div class="card-body">

                <?php
                $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                    'id' => 'ticket-form',
                    'enableAjaxValidation' => false,
                ));
                ?>

                <p class="help-block">Fields with <span class="required">*</span> are required.</p>

                <?php echo $form->errorSummary($model); ?>


                <?php echo $form->dropDownListRow($model, 'status', CHtml::listData(TicketStatus::model()->findAll(), 'id', 'title'), array('class'=>'form-control')); ?>

                <?php //echo $form->textFieldRow($model, 'created', array('class' => 'span5'));   ?>
                <hr>
                <div class="form-actions">
                    <?php
                    $this->widget('bootstrap.widgets.TbButton', array(
                        'buttonType' => 'submit',
                        'type' => 'primary',
                        'label' => $model->isNewRecord ? 'Create' : 'Save',
                    ));
                    ?>
                </div>

                <?php $this->endWidget(); ?>





                <style>
                    .descc {
                        /* float: left; */
                        width: 100%;
                    }

                </style>    
            </div></div>
        </div>
    </div>
</section>