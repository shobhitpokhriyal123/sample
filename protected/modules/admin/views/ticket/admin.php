<section id="manage_products">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="card-title">
                        <strong>Manage Tickets</strong>
                    </div>

                    <?php
                    if (Yii::app()->user->isAgency || Yii::app()->user->isUser) {
                        echo CHtml::link('Create Ticket', array('/admin/ticket/create'), array("class" => 'btn btn-success'));
                        //echo CHtml::link('Create Ticket', array('/admin/ticket/create'), array("class" => 'btn btn-success'));
                    }
                    ?>

                </div>
                <div class="card-body">
                    <?php
                    $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => 'ticket-grid',
                        'dataProvider' => $model->search(),
                        'filter' => $model,
                        'columns' => array(
                            // 'id',
                            'agent_id' => array(
                                'name' => 'agent_id',
                                'filter' => false,
                                'value' => '$data->agent->username'
                            ),
                            //'create_user_id',
                            'utility' => array(
                                'name' => 'utility',
                                'filter' => false, // Product::getUtilites(),
                                'value' => 'Product::getUtilites($data->utility)',
                            ),
                            'corporate_sme' => array(
                                'name' => 'corporate_sme',
                                'filter' => false, // Product::getCorporateSME(),
                                'value' => 'Product::getCorporateSME($data->corporate_sme)',
                            ),
                            'ticket_type' => array(
                                'name' => 'ticket_type',
                                'value' => 'TicketType::getTicketType($data->ticket_type)'
                            ),
                            'query_type' => array(
                                'name' => 'query_type',
                                'value' => 'TicketType::getQueryType($data->query_type)'
                            ),
                            'title',
                            'description',
                            'reply' => array(
                                'name' => 'reply',
                                'type' => 'raw',
                                'value' => '($data->reply) ? CHtml::link(substr($data->reply->description, 10),array("reply/view","id"=>$data->reply->id), array("target"=>"_blank"))  : "N/A"'
                            ),
                            'status' => array(
                                'header' => 'Ticket Status',
                                'filter' => false,
                                'type' => 'raw',
                                'value' => '(Yii::app()->user->isAdmin)?CHtml::link(TicketStatus::getTicketStatus($data->status), array("/admin/ticket/updateStatus&id=$data->id"), array("class"=>"sdfsdfsasdf")) : TicketStatus::getTicketStatus($data->status)',
                            ),
                            // 'created',
                            array(
                                'class' => 'bootstrap.widgets.TbButtonColumn',
                                'template' => '{view}{update}{delete}{reply}',
                                'buttons' => array(
                                    'update' => array(
                                        'visible' => '(!Yii::app()->user->isAdmin)',
                                    ),
//                                    'delete' => array(
//                                        'visible' => 'Yii::app()->user->isAdmin',
//                                    ),
                                    'reply' => array(
                                        'label' => '&nbsp;&nbsp;<i class="fa fa-arrow-right"></i>',
                                        'url' => 'Yii::app()->createAbsoluteUrl("admin/reply/create", array("tid"=>$data->id))',
                                        'options' => array('title' => 'Reply'),
                                        'imageUrl' => false,
                                        'click' => "function(e){
                                             var url = $(this).attr('href');
                                               $('#myModal').modal('show');
                                                e.preventDefault();
                                                $.ajax({
                                                 type: 'POST',
                                                 url: url,
                                                      success: function(res){
                                                         $('#fasdfsadf1').html(res);
                                               
                                                   },
                                                });

                                                return false;
                                            }",
                                        'visible' => '(Yii::app()->user->isAdmin) && ($data->is_replied == 0)',
                                    ),
                                ),
                            ),
                        ),
                    ));
                    ?>

                </div>
            </div>
        </div> 
    </div>
</section>
<!--<div class="modal fade" id="modal11x" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document" id="fasdfsadf1">
        

    </div>
</div>-->

<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content" id="fasdfsadf1">

        </div>

    </div>
</div>


<script>
    $(document).ready(function () {
        $(".sdfsdfsasdf").click(function (e) {
            e.preventDefault();
            var url = $(this).attr("href");
            $("#myModal1").modal();
            $("#modal-content-status").load(url);
        });
    });
</script>


<!-- Modal -->
<div id="myModal1" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" id="modal-content-status">

        </div>

    </div>
</div>
