
<section id="manage_products">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="card-title">
                        <strong>Update Tickets</strong>
                    </div>

                    <?php
//                    if (Yii::app()->user->isAgency || Yii::app()->user->isAgent) {
                        echo CHtml::link('Manage Tickets', array('/admin/ticket/admin'), array("class" => 'btn btn-success'));
//                    }
                    ?>

                </div>
                <div class="card-body">




                    <?php
//$this->breadcrumbs=array(
//	'Tickets'=>array('index'),
//	$model->title=>array('view','id'=>$model->id),
//	'Update',
//);
//
//$this->menu=array(
//	array('label'=>'List Ticket','url'=>array('index')),
//	array('label'=>'Create Ticket','url'=>array('create')),
//	array('label'=>'View Ticket','url'=>array('view','id'=>$model->id)),
//	array('label'=>'Manage Ticket','url'=>array('admin')),
//);
                    ?>


                    <?php echo $this->renderPartial('_form', array('model' => $model)); ?>

                </div>
            </div>
        </div>
    </div>
</section>