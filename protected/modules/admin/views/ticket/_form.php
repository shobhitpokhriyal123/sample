<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'ticket-form',
    'enableAjaxValidation' => false,
        ));
$model1 = new Product;
?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

<?php
$agents = Users::model()->findAllByAttributes(array('role_id' => 4, 'agency_id' => Yii::app()->user->id));

if (Yii::app()->user->isAdmin) {
    echo $form->dropdownListRow($model, 'agent_id', CHtml::listData(Users::model()->findAllByAttributes(array('role_id' => 4)), 'id', 'username'), array('empty' => '--- Select Agent ---'));
} elseif (Yii::app()->user->isAgency) {
    if ($agents)
        echo $form->dropdownListRow($model, 'agent_id', CHtml::listData($agents, 'id', 'username'), array('empty' => '--- Select Agent ---'));
    else {
        echo $form->dropdownListRow($model, 'agent_id', array(Yii::app()->user->id => Yii::app()->user->username));
    }
} elseif (Yii::app()->user->isUser) {
    echo $form->dropdownListRow($model, 'agent_id', array(Yii::app()->user->id => Yii::app()->user->username));
}
?> 
<?php //echo $form->textFieldRow($model, 'create_user_id', array('class' => 'span5')); ?>

<?php echo $form->dropDownListRow($model, 'corporate_sme', $model1->getCorporateSME(), array('onclick' => 'getUtilities($(this).val())')); ?>

<?php echo $form->dropDownListRow($model, 'utility', CHtml::listData($model1->getUtilites(), 'id', 'title')); ?>


<?php //echo $form->dropDownListRow($model,'corporate_sme', array('empty' => '--- Select ---','onclick' => 'getUtilities($(this).val())'));    ?>

<?php //echo $form->textFieldRow($model,'utility',array('class'=>'span5'));     ?>

<?php $criteria = new CDbCriteria;
$criteria->addCondition('parrent = 0');
echo $form->dropDownListRow($model, 'ticket_type', CHtml::listData(TicketType::model()->findAll($criteria), 'id', 'title'), array('onchange' => 'getQuery($(this).val())')); ?>

<?php echo $form->dropDownListRow($model, 'query_type', array("" => "--Select Query--")); ?>

<?php echo $form->textFieldRow($model, 'title', array('class' => 'span5', 'maxlength' => 255)); ?>

<?php echo $form->textAreaRow($model, 'description', array('rows' => 6, 'cols' => 50, 'class' => 'span8 descc')); ?>

<?php //echo $form->textFieldRow($model, 'created', array('class' => 'span5'));     ?>

<div class="form-actions">
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type' => 'primary',
        'label' => $model->isNewRecord ? 'Create' : 'Save',
    ));
    ?>
</div>

<?php $this->endWidget(); ?>



<script>
    gasParms();
    $("#Ticket_utility").change(function () {
        gasParms();
    });
    getQuery($("#Ticket_ticket_type").val());
    function getUtilities(id) {
        var url = '<?php echo Yii::app()->createAbsoluteUrl("admin/product/getUtilities/id") ?>/' + id;
        $("#Ticket_utility").load(url);
        gasParms();
    }

    function gasParms() {
        return;
        var id = $("#Ticket_utility").val();
        if (id == 17 || id == 19) {
            $(".gas").hide();
        } else {
            $(".gas").show();
        }
    }
    function getQuery(id) {
        var url = '<?php echo Yii::app()->createAbsoluteUrl("admin/ticket/getQuery/id") ?>/' + id;
        $("#Ticket_query_type").load(url);
    }

</script>

<style>
    .descc {
        /* float: left; */
        width: 100%;
    }

</style>    