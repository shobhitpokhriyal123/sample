<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('agent_id')); ?>:</b>
	<?php echo CHtml::encode($data->agent_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_user_id')); ?>:</b>
	<?php echo CHtml::encode($data->create_user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('corporate_sme')); ?>:</b>
	<?php echo CHtml::encode($data->corporate_sme); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('utility')); ?>:</b>
	<?php echo CHtml::encode($data->utility); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ticket_type')); ?>:</b>
	<?php echo CHtml::encode($data->ticket_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('query_type')); ?>:</b>
	<?php echo CHtml::encode($data->query_type); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
	<?php echo CHtml::encode($data->title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />

	*/ ?>

</div>