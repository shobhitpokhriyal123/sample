<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'reply-form',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
        ));
?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php //echo $form->errorSummary($model); ?>

<?php //echo $form->textFieldRow($model, 'ticket_id', array('class' => 'span5 form-control')); ?>

<?php //echo $form->textFieldRow($model,'create_user_id',array('class'=>'span5')); ?>

<?php echo $form->textAreaRow($model, 'description', array('rows' => 6, 'cols' => 50, 'class' => 'span8 form-control')); ?>

    <?php //echo $form->textFieldRow($model,'created',array('class'=>'span5'));  ?>

<div class="form-actions">
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type' => 'primary',
        'label' => $model->isNewRecord ? 'Reply' : 'Save',
    ));
    ?>
</div>

<?php $this->endWidget(); ?>
