<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">Reply for <b><?php echo $ticket->title; ?> </b></h4>
</div>
<div class="modal-body">
    <div class="modal-body">
        <p><b><?php echo $ticket->createUser->username ?></b> Asked : <?php echo $ticket->description ?> </p>
        
        <?php echo $this->renderPartial('_form', array('model' => $model), false, true); ?>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>