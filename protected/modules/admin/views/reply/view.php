<section id="manage_products">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="card-title">
                        <strong>View Reply</strong>
                    </div>

                    <?php
               //     if (Yii::app()->user->isAdmin) {
                        echo CHtml::link('Manage Tickets', array('/admin/ticket/admin'), array("class" => 'btn btn-success'));
                  //  }
                    ?>

                </div>
                <div class="card-body">

                    <?php
                    $this->widget('bootstrap.widgets.TbDetailView', array(
                        'data' => $model,
                        'attributes' => array(
                            'id',
                            'ticket_id',
                           // 'create_user_id',
                            'description',
                            'created',
                        ),
                    ));
                      echo CHtml::link('Resend Reply', array('/admin/ticket/view',"id"=>$model->id), array("class" => 'btn btn-success'));
                    ?>
                    
                </div>
            </div>
        </div>
    </div>
</section>
