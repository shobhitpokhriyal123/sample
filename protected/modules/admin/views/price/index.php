<?php
$this->breadcrumbs=array(
	'Prices',
);

$this->menu=array(
	array('label'=>'Create Price','url'=>array('create')),
	array('label'=>'Manage Price','url'=>array('admin')),
);
?>

<h1>Prices</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
