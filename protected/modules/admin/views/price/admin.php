
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'price-grid',
	'dataProvider'=>$model->modelPrice,
	'filter'=>$model->modelPrice->search(),
	'columns'=>array(
		'id',
		'product_id',
		'product_type',
		'dist_id',
		'region',
		'meter_type',
		/*
		'profile',
		'product',
		'standing_charge',
		'day_all_other_day_unit',
		'night_unit_price',
		'weekend_winter_peek',
		'min_aq',
		'max_aq',
		'e_on_id',
		'mtc',
		'llf',
		'contract_length',
		'stand_charge_period',
		'capacity_charge',
		'md_charge',
		'secondary_price',
		'threshold',
		'daily_service_charge',
		'tariff',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
