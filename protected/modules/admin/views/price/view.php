<?php
$this->breadcrumbs=array(
	'Prices'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Price','url'=>array('index')),
	array('label'=>'Create Price','url'=>array('create')),
	array('label'=>'Update Price','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Price','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Price','url'=>array('admin')),
);
?>

<h1>View Price #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'product_id',
		'product_type',
		'dist_id',
		'region',
		'meter_type',
		'profile',
		'product',
		'standing_charge',
		'day_all_other_day_unit',
		'night_unit_price',
		'weekend_winter_peek',
		'min_aq',
		'max_aq',
		'e_on_id',
		'mtc',
		'llf',
		'contract_length',
		'stand_charge_period',
		'capacity_charge',
		'md_charge',
		'secondary_price',
		'threshold',
		'daily_service_charge',
		'tariff',
	),
)); ?>
