<a style="display: none" target="_blank" href="<?php echo Yii::app()->createAbsoluteUrl("admin/agency/selectQuote") ?>" id="abcdjksfghksdufgh" >sdfsf</a>
<?php if (Users::isPermission(Yii::app()->user->id, 9)) { ?>
    <a href="" id="location" class="btn btn-primary btn-success">Customer View</a>
<?php
}
$date = $_GET['Price']['start_date'];
$meter = $_GET['Price']['meter_type'];
//echo '<pre>'; print_r($_GET); die;
//$d = new DateTime('10-16-2003');
//
//$timestamp = $d->getTimestamp(); // Unix timestamp
//echo $formatted_date = $d->format('Y-m-d');

$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'price-grid1' . $month,
    'dataProvider' => $model->searchEByMonth($month),
    'rowCssClassExpression' => '"myclass_' . $month . '_{$data->id}"',
    'afterAjaxUpdate' => 'function(id,options){ paginationControls(); }',
    'filter' => $model,
    'columns' => array(
        'assigned_to' => array(
            'type' => 'raw',
            'filter' => false, //CHtml::listData(Users::getAllSuppliers(), 'id', 'fullname'), 'type' => 'html',
            'header' => 'Supplier',
            'name' => 'assigned_to',
            //   'value' => '$data->productName->user->fullname'
            'value' => 'CHtml::image(Yii::app()->createAbsoluteUrl("admin/users/thumbnail/file/".$data->productName->user->image))'
        ),
        'product' => array(
            'header' => 'Set Name',
            'filter' => false,
            'type' => 'raw',
            'name' => 'product',
            'value' => 'Product::getPName($data->productName->product_name,$data->product)'
        ),
        // 'start_date',
        'region' => array(
            'header' => 'Region',
            'filter' => false,
            'name' => 'region'
        ),
        'contract_length' => array(
            'header' => 'Term',
            'filter' => false,
            'name' => 'contract_length',
            'value' => '($data->contract_length) ? $data->contract_length : $data->term',
        ),
        'start_date' => array(
            'name' => 'start_date',
            'type' => 'raw',
            'value' => '$data->getStartDate("' . $date . '")', //'$data->start_date',
            'filter' => false


//             'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
//                'model'=>$model, 
//                'attribute'=>'due_date', 
//                'language' => 'ja',
//                // 'i18nScriptFile' => 'jquery.ui.datepicker-ja.js', (#2)
//                'htmlOptions' => array(
//                    'id' => 'datepicker_for_due_date',
//                    'size' => '10',
//                ),
//                'defaultOptions' => array(  // (#3)
//                    'showOn' => 'focus', 
//                    'dateFormat' => 'yy/mm/dd',
//                    'showOtherMonths' => true,
//                    'selectOtherMonths' => true,
//                    'changeMonth' => true,
//                    'changeYear' => true,
//                    'showButtonPanel' => true,
//                )
//            ), 
//            true),
        ),
//        'meter_type' => array(
//            'filter' => false,
//            'name' => 'meter_type'
//        ),
        'payment_type' => array(
            'filter' => false,
            'name' => 'payment_type',
            'value' => '($data->productName->direct_dabit == 0) ? "Cash" : "Direct Debit" '
        ),
//        'rates_and_charges' => array(
//            'name' => 'rates_and_charges',
//            'filter' => false,
//            'type' => 'raw',
//            'value' => '$data->getRates()'
//        ),
        'standing_charge' => array(
            'filter' => false,
            'type' => 'raw',
            'header' => 'S/C p/day',
            'name' => 'standing_charge',
            'value' => '$data->getStandingCharge(' . $month . ')'
        ),
        'unit_charge' => array(
            'filter' => false,
            'type' => 'raw',
            'header' => 'Price ',
            'name' => 'unit_charge',
            'value' => '$data->getUnitCharge(' . $month . ')'
        ),
        'commission' => array(
            'filter' => false,
            'type' => 'raw',
            'header' => 'Commission',
            // 'name' => 'unit_charge',
            'value' => '$data->getCommission(' . $month . ')',
            'visible' => Product::getTrueFalse(),
        ),
        'commission_increament' => array(
            'filter' => false,
            'type' => 'raw',
            'header' => 'Type',
            'name' => 'commission_increament',
            'value' => '$data->getCommissionType(' . $month . ')',
            'visible' => Product::getTrueFalse(),
        ),
        //'min_aq',
        'anual_cost' => array(
            'filter' => false,
            'type' => 'raw',
            'header' => 'Anual Cost',
            'name' => 'anual_cost',
            'value' => '$data->getElcAnualCost(' . $month . ',' . $meter . ')'
        ),
//        'acost' => array(
//            'filter' => false,
//            'type' => 'raw',
//            'header' => 'Anual Cost',
//            'name' => 'acost',
//           'value' => '$data->getElcAnualCost(' . $month . ')'
//        ),
//        'profile' => array(
//            'name' => 'profile',
//            'filter' => false,
//        ),
//        'llf' => array(
//            'filter' => false,
//            'name' => 'llf'
//        ),
//        'product' => array(
//            'header'=>'Set Name',
//            'filter' => false,
//            'name' => 'product'
//        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{Save}{select}{more}',
            //  'evaluateID'=>true,
            'header' => 'Actions',
            'buttons' => array(
                'Save' => array(
                    'label' => '<button class="btn btn-primary">Save</button>',
                    'url' => 'Yii::app()->createAbsoluteUrl("admin/quote/create", array("id"=>$data->id))',
                    'options' => array('month' => $month, 'rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Save'),
                    'imageUrl' => false,
                    'click' => "function(e){
                               e.preventDefault();
                               var url = $(this).attr('href');
                               var month = $(this).attr('month');
                               var price_id =  url.match(/([^\=]*)\/*$/)[1];
                               var test = $('.filter-container').val();
                               var ct_ = $('#ct_'+price_id+'_'+month).val();
                               var sc_ = $('#sc_'+price_id+'_'+month).val();
                               var uc_ = $('#uc_'+price_id+'_'+month).val();
                               var comm_ = $('#comm_'+price_id+'_'+month).val();
                               var ac_ = $('#ac_'+price_id+'_'+month).val();
                               var  data = { 
                               'Quote[price_id]':price_id,
                               'Quote[is_gas]':0,
                                'Quote[is_select]':0,
                                 'Quote[current_supplier]':current_supplier,
                               'Quote[aq]':aqu,
                               'Quote[commission_type]':ct_,
                               'Quote[standing_charge]':sc_,
                               'Quote[price]':uc_,
                               'Quote[commission]':comm_,
                               'Quote[annual_cost]':ac_
                               };
                               $.ajax({
                                 type: 'POST',
                                 url: url,
                                 data:data,
                                 success: function(res){
                                  
                                  $.fn.yiiGridView.update('quote-grid-');
                                  //$('#abcdjksfghksdufgh').click();
                                },
                                });
                                
                           
                            }",
                ),
                'select' => array(
                    'label' => '<button class="btn btn-primary">Select</button>',
                    'url' => 'Yii::app()->createAbsoluteUrl("admin/quote/create", array("id"=>$data->id))',
                    'options' => array('month' => $month, 'target' => '_blank', 'rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Select'
                    //'url2'=>'Yii::app()->createAbsoluteUrl("admin/agency/selectQuote", array("id"=>$data->id))'
                    ),
                    'imageUrl' => false,
                    'click' => "function(e){
                               e.preventDefault();
                               var url = $(this).attr('href');
                               var month = $(this).attr('month');
                               var price_id =  url.match(/([^\=]*)\/*$/)[1];
                               var sdate = $(this).attr('date');
                               var ct_ = $('#ct_'+price_id+'_'+month).val();
                               var sc_ = $('#sc_'+price_id+'_'+month).val();
                               var uc_ = $('#uc_'+price_id+'_'+month).val();
                               var comm_ = $('#comm_'+price_id+'_'+month).val();
                               var ac_ = $('#ac_'+price_id+'_'+month).val();
                              
                              var  data = { 
                               'Quote[price_id]':price_id,
                               'Quote[is_select]':1,
                               'Quote[is_gas]':0,
                               'Quote[commission_type]':ct_,
                               'Quote[standing_charge]':sc_,
                               'Quote[price]':uc_,
                                'Quote[current_supplier]':current_supplier,
                               'Quote[aq]':aqu,
                               'Quote[commission]':comm_,
                               'Quote[annual_cost]':ac_
                               };
                                var ulr =   $('#abcdjksfghksdufgh').attr('href');
                                ulr = ulr+'&price_id='+price_id;
                                $('#abcdjksfghksdufgh').attr('href',ulr);
                               $.ajax({
                                 type: 'POST',
                                 url: url,
                                 data:data,
                                 success: function(res){
                                 if(res !=  'no'){
                                  $.fn.yiiGridView.update('quote-grid-');
                                   window.location.href = $('#abcdjksfghksdufgh').attr('href')+'&quote_id='+res+'&month='+month+'&date='+sdate;
                                 
                                   }
                                },
                                });
                                
                           
                            }",
                ),
                'more' => array(
                    'label' => '<button class="btn btn-primary">View More</button>',
                    'url' => 'Yii::app()->createAbsoluteUrl("admin/quote/viewMore", array("id"=>$data->id))',
                    'options' =>
                    array(
                        'month' => $month,
                        'target' => '_blank',
                        'rel' => 'tooltip',
                        'data-toggle' => 'tooltip',
                        'title' => 'View More'
                    ),
                    'imageUrl' => false,
                    'click' => "function(e){
                           e.preventDefault();
                           jQuery.noConflict();
                           var url1 = $(this).attr('href');
                           var month = $(this).attr('month');
                           var price_id =  url1.match(/([^\=]*)\/*$/)[1];
                           var click = 1;
                           getMoreInfo(url1,month, price_id , click);
                            return false;
                        }",
                ),
            ),
        ),
    ),
));
?>
<hr>
<div id="quote_outer-<?php echo $month ?>">
</div>
<div class="col-sm-12">
    <!--    <a id="" class="btn btn-primary" href="">Download as Pdf</a>-->
    <div class="col-md-4">
        <a id="print" class="btn btn-primary" href="">Print</a>
    </div>

</div>
<hr>

<script type="text/javascript">
    paginationControls();

    function printData()
    {
        var month = '<?php echo $month; ?>';
        var divToPrint = document.getElementById("quote-grid-");
        newWin = window.open("");
        newWin.document.write(divToPrint.outerHTML);
        newWin.print();
        newWin.close();
    }

    $('#print').on('click', function (e) {
        e.preventDefault();
        printData();
    });
</script>
<script type="text/javascript">
    loadQoute();
    function loadQoute() {
        var manth = '<?php echo $month ?>';
        var quote_url = '<?php echo Yii::app()->createAbsoluteUrl("/admin/quote/admin/month/" . $month . "&Quote[is_gas]=0") ?>';
        $("#quote_outer-" + manth).load(quote_url);
    }
    function getCalculation(id, type, inc, max, month, commission) {

        //if(month  == 123)
        var month_cal = (month / 12);

        var aq = '<?php echo $_GET['Price']['max_aq']; ?>';
        var ct_ = parseFloat($('#ct_' + id + '_' + month).val());
        var uc_ = parseFloat($('#uc_' + id + '_' + month).val());
        var sc_ = parseFloat($('#sc_' + id + '_' + month).val());
        var uc_night_ = parseFloat($('#uc_night_' + id + '_' + month).val());
        var uc_winter_ = parseFloat($('#uc_winter_' + id + '_' + month).val());
        var uc_day_ = parseFloat($('#uc_day_' + id + '_' + month).val());
        var min = '0.00';


        if (type == 1) {
            if (parseFloat(max) < parseFloat((+ct_ + +inc)))
                return false;
            $('#ct_' + id + '_' + month).val(parseFloat(+ct_ + +inc).toFixed(4));
            $('#uc_night_' + id + '_' + month).val(parseFloat(+uc_night_ + +inc).toFixed(4));
            $("#uc_" + id + '_' + month).val(parseFloat(uc_ + inc).toFixed(4));
            $("#uc_winter_" + id + '_' + month).val(parseFloat(uc_winter_ + inc).toFixed(4));




        } else {
            if (parseFloat(ct_) <= parseFloat(min))
                return false;
            $('#ct_' + id + '_' + month).val(parseFloat(parseFloat(ct_) - parseFloat(inc)).toFixed(4));
            $("#uc_" + id + '_' + month).val(parseFloat(uc_ - inc).toFixed(4));
            $('#uc_night_' + id + '_' + month).val(parseFloat(+uc_night_ - +inc).toFixed(4));
            $("#uc_day_" + id + '_' + month).val(parseFloat(+uc_day_ - +inc).toFixed(4));
            $("#uc_winter_" + id + '_' + month).val(parseFloat(+uc_winter_ - +inc).toFixed(4));

        }
        // alert(commission);
        var comcal = ((parseFloat(aq).toFixed(4) * parseFloat(parseFloat($('#ct_' + id + '_' + month).val())).toFixed(4)) / 100) * ((parseFloat(commission).toFixed(4)) / 100);
        $("#comm_" + id + '_' + month).val(parseFloat(parseFloat(comcal).toFixed(4) * parseFloat(month_cal).toFixed(4)).toFixed(4));
        var cal = ((parseFloat($("#ct_" + id + '_' + month).val()) * parseFloat(aq)) / 100);
        $("#comm_" + id + '_' + month + '_' + month).val(parseFloat(cal).toFixed(4));
        var anual = ((parseFloat(aq) * parseFloat($("#uc_" + id + '_' + month).val())) / 100) + ((parseFloat(sc_) * 365) / 100);
        $("#ac_" + id + '_' + month).val(parseFloat(anual).toFixed(4));



        //  alert($("#comm_" + id + '_' + month + '_' + month).val());
        // var anual = ( (parseFloat(aq) * parseFloat($("#uc_" + id).val() ) ) /100 + parseFloat((sc_ * 365) / 100));



    }

    $(document).ready(function (e) {
        var url = window.location.href;
        $("#location").click(function (e) {
            e.preventDefault();
            url = url + "&ss=1"
            window.location.href = url;
        });
    });

</script>
<style>
    .spinner {
        width: 100px;
    }
    .spinner input {
        float: left;
        text-align: right;
        width: 62px;
        height: 26px;
    }
    .input-group-btn-vertical {
        position: relative;
        white-space: nowrap;
        width: 1%;
        vertical-align: middle;
        display: table-cell;
    }
    .input-group-btn-vertical > .btn {
        display: block;
        float: none;
        width: 100%;
        max-width: 100%;
        padding: 8px;
        margin-left: -1px;
        position: relative;
        border-radius: 0;
    }
    .input-group-btn-vertical > .btn:first-child {
        border-top-right-radius: 4px;
    }
    .input-group-btn-vertical > .btn:last-child {
        margin-top: -2px;
        border-bottom-right-radius: 4px;
    }
    .input-group-btn-vertical i{
        position: absolute;
        top: 0;
        left: 4px;
    }

    #UniversalMessageWindow {
        width: 90%;
        border: 1px solid #064266;
        background-color: #d3e7f0;
        display: none;
        position: fixed;
        top: calc(20% - 115px);

        left: 90px;
        z-index: 20;
    }

    #UniversalMessageWindow #message {
        padding: 10px;
    }

    #UniversalMessageWindow #message h3 {
        font-weight: 100;
        font-size: 24px;
        font-weight: 100;
        padding: 3px 0 15px 0px;
        border-bottom: 1px solid #064266;
        margin-bottom: 10px;
    }

    #UniversalMessageWindow #message h3 #messageTitle {
        font-weight: bold;
        color: #064266;
    }

    #UniversalMessageWindow #message h3 a {
        float: right;
        cursor: pointer;
    }

    #UniversalMessageWindow #message p {
        font-weight: 600;
        font-size: 15px;
        color: #064266;
    }

    #UniversalMessageWindow #actionControls {
        background-color: #064266;
        height: 69px;
    }

    #UniversalMessageWindow #actionControls a {
        display: inline-block;
        border: 1px solid #000;
        width: 70px;
        padding: 8px 0;
        text-align: center;
        float: right;
        margin: 18px 20px 0 0;
        text-decoration: none;
        color: #000;
        font-weight: 600;
        font-size: 12px;
        background: #fff;
    }

    @media screen and (max-width:390px) {
        #UniversalMessageWindow {
            width: 320px;
            left: calc(50% - 160px);
        }
        #UniversalMessageWindow #message {
            height: 120px;
        }
        #UniversalMessageWindow #message h3 a {
            float: right;
            cursor: pointer;
        }
    }

</style>






<script>
    (function ($) {

        $.confirm = function (params) {

            if ($('#UniversalMessageWindow').length) {
                // A confirm is already shown on the page:
                return false;
            }

            var buttonHTML = '';
            $.each(params.buttons, function (name, obj) {

                // Generating the markup for the buttons:

                buttonHTML += '<a href="#" class="button ' + obj['class'] + '">' + name + '<span></span></a>';

                if (!obj.action) {
                    obj.action = function () {};
                }
            });

            var markup = [

                '<div id="UniversalMessageWindow">',
                '<div id="message">',
                '<h3>',
                '<span id="messageTitle">',
                params.title,
                '</span>',
                '</h3>',
                '<p id="loaded_view">', params.message, '</p>',
                '</div>',
                '<div class="clearfix"></div>',
                '<div id="actionControls">',
                buttonHTML,
                '</div>',
                '</div>'
            ].join('');

            $(markup).hide().appendTo('body').fadeIn();

            var buttons = $('#actionControls .button'),
                    i = 0;

            $.each(params.buttons, function (name, obj) {
                buttons.eq(i++).click(function () {

                    // Calling the action attribute when a
                    // click occurs, and hiding the confirm.

                    obj.action();
                    $.confirm.hide();
                    return false;
                });
            });
        }

        $.confirm.hide = function () {
            $('#confirmOverlay').fadeOut(function () {
                $(this).remove();
            });
        }

    })(jQuery);

    function getMoreInfo(url1, month, price_id, click) {
        url1 = url1 + '&month=' + month
        $.ajax({
            url: url1,
            type: "POST",
            success: function (res) {
                $.confirm({
                    'title': 'Supplier Information',
                    'message': res,
                    'buttons': {
                        'OK': {
                            'action': function () {
                                $('#UniversalMessageWindow').remove();
                            }
                        }
                    }
                });
            }

        });

    }

</script>


<script>
    $(window).load(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>