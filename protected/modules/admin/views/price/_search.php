<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'product_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'product_type',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'dist_id',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'region',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'meter_type',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'profile',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'product',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'standing_charge',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'day_all_other_day_unit',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'night_unit_price',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'weekend_winter_peek',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'min_aq',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'max_aq',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'e_on_id',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'mtc',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'llf',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'contract_length',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'stand_charge_period',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'capacity_charge',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'md_charge',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'secondary_price',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'threshold',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'daily_service_charge',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'tariff',array('class'=>'span5','maxlength'=>255)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
