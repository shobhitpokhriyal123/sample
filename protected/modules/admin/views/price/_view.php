<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('product_id')); ?>:</b>
	<?php echo CHtml::encode($data->product_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('product_type')); ?>:</b>
	<?php echo CHtml::encode($data->product_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dist_id')); ?>:</b>
	<?php echo CHtml::encode($data->dist_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('region')); ?>:</b>
	<?php echo CHtml::encode($data->region); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meter_type')); ?>:</b>
	<?php echo CHtml::encode($data->meter_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('profile')); ?>:</b>
	<?php echo CHtml::encode($data->profile); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('product')); ?>:</b>
	<?php echo CHtml::encode($data->product); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('standing_charge')); ?>:</b>
	<?php echo CHtml::encode($data->standing_charge); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('day_all_other_day_unit')); ?>:</b>
	<?php echo CHtml::encode($data->day_all_other_day_unit); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('night_unit_price')); ?>:</b>
	<?php echo CHtml::encode($data->night_unit_price); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('weekend_winter_peek')); ?>:</b>
	<?php echo CHtml::encode($data->weekend_winter_peek); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('min_aq')); ?>:</b>
	<?php echo CHtml::encode($data->min_aq); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('max_aq')); ?>:</b>
	<?php echo CHtml::encode($data->max_aq); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('e_on_id')); ?>:</b>
	<?php echo CHtml::encode($data->e_on_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mtc')); ?>:</b>
	<?php echo CHtml::encode($data->mtc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('llf')); ?>:</b>
	<?php echo CHtml::encode($data->llf); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contract_length')); ?>:</b>
	<?php echo CHtml::encode($data->contract_length); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('stand_charge_period')); ?>:</b>
	<?php echo CHtml::encode($data->stand_charge_period); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('capacity_charge')); ?>:</b>
	<?php echo CHtml::encode($data->capacity_charge); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('md_charge')); ?>:</b>
	<?php echo CHtml::encode($data->md_charge); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('secondary_price')); ?>:</b>
	<?php echo CHtml::encode($data->secondary_price); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('threshold')); ?>:</b>
	<?php echo CHtml::encode($data->threshold); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('daily_service_charge')); ?>:</b>
	<?php echo CHtml::encode($data->daily_service_charge); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tariff')); ?>:</b>
	<?php echo CHtml::encode($data->tariff); ?>
	<br />

	*/ ?>

</div>