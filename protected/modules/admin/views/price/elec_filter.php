<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'price-grid1',
    'dataProvider' => $model->searchElec($user_id),
    'filter' => $model,
    'afterAjaxUpdate' => 'function(id,options){ paginationControls(); }',
    'columns' => array(
//        'acc_start' => array(
//            'name' => 'acc_start',
//            'filter' => false,
//            'value' => '($data->productName)? date("d-m-Y", strtotime($data->productName->acc_start)) : date("d-m-Y")'
//        ),
//        'acc_end' => array(
//            'name' => 'acc_end',
//            'filter' => false,
//            'value' => '($data->productName)? date("d-m-Y", strtotime($data->productName->acc_end)) : date("d-m-Y")'
//        ),
//        // .'end_date',
//        'valid_from' => array(
//            'name' => 'valid_from',
//            'filter' => false,
//            'value' => '($data->productName)? date("d-m-Y", strtotime($data->productName->valid_from)) : date("d-m-Y")'
//        ),
//        'valid_till' => array(
//            'name' => 'valid_till',
//            'filter' => false,
//            'value' => '($data->productName)? date("d-m-Y", strtotime($data->productName->valid_till)) : date("d-m-Y")'
//        ),
       
        'assigned_to' => array(
            'filter' => false, //CHtml::listData(Users::getAllSuppliers(), 'id', 'fullname'),
            'type' => 'html',
            'header' => 'Supplier',
            'name' => 'assigned_to',
            'value' => 'CHtml::image(Yii::app()->createAbsoluteUrl("admin/users/thumbnail/file/".$data->productName->user->image))'
        ),
        'start_date' => array(
            'filter' => false, //CHtml::listData(Users::getAllSuppliers(), 'id', 'fullname'),
            'type' => 'html',
            'name' => 'start_date',
            'header' => 'Start Date',
        // 'name' => 'assigned_to',
        // 'value' => 'CHtml::image(Yii::app()->createAbsoluteUrl("admin/users/thumbnail/file/".$data->productName->user->image))'
        ),
        'product_id' => array(
            'name' => 'product_id',
            'value' => '$data->productName->product_name',
            'filter' => false
        ),
        'meter_type' => array(
            'filter' => false,
            'name' => 'meter_type'
        ),
        'dist_id' => array(
            'filter' => false,
            'name' => 'dist_id'
        ),
        'region' => array(
            'name' => 'region',
            'filter' => false,
        ),
        'standing_charge' => array(
            'name' => 'standing_charge',
            'filter' => false,
        ),
        'term' => array(
            'name' => 'term',
            'filter' => false,
            'value' => '($data->term) ? $data->term : $data->contract_length'
        ),
        'mtc' => array(
            'name' => 'mtc',
            'filter' => false,
        ),
        'profile' => array(
            'name' => 'profile',
            'filter' => false,
        ),
        'min_aq' => array(
            'name' => 'min_aq',
            'filter' => false,
            'value' => '($data->min_aq) ? $data->min_aq : $data->min_band'
        ),
        'max_aq' => array(
            'name' => 'max_aq',
            'filter' => false,
            'value' => '($data->max_aq) ? $data->max_aq : $data->max_band'
        ),
        'llf' => array(
            'filter' => false,
            'name' => 'llf'
        ),
//        'product' => array(
//            'header'=>'Set Name',
//            'filter' => false,
//            'name' => 'product'
//        ),
        'product' => array(
            'header' => 'Set Name',
            'filter' => false,
            'name' => 'product'
        ),
//        array(
//            'class' => 'bootstrap.widgets.TbButtonColumn',
//        ),
    ),
));
?>

<script>
    paginationControls();
</script>

