<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'price-grid1',
    'dataProvider' => $model->searchGas($user_id),
    'filter' => $model,
    'afterAjaxUpdate' => 'function(id,options){ paginationControls(); }',
    'columns' => array(
        'acc_start' => array(
            'name' => 'acc_start',
            'filter' => false,
            'value' => '($data->productName)? date("d-m-Y", strtotime($data->productName->acc_start)) : date("d-m-Y")'
        ),
        'acc_end' => array(
            'name' => 'acc_end',
            'filter' => false,
            'value' => '($data->productName)? date("d-m-Y", strtotime($data->productName->acc_end)) : date("d-m-Y")'
        ),
    
        // .'end_date',
        'valid_from' => array(
            'name' => 'valid_from',
            'filter' => false,
            'value' => '($data->productName)? date("d-m-Y", strtotime($data->productName->valid_from)) : date("d-m-Y")'
        ),
        'valid_till' => array(
            'name' => 'valid_till',
            'filter' => false,
            'value' => '($data->productName)? date("d-m-Y", strtotime($data->productName->valid_till)) : date("d-m-Y")'
        ),
        'assigned_to' => array(
            'filter' => false, //CHtml::listData(Users::getAllSuppliers(), 'id', 'fullname'),
            'type' => 'html',
            'header' => 'Supplier',
            'name' => 'assigned_to',
            'value' => 'CHtml::image(Yii::app()->createAbsoluteUrl("admin/users/thumbnail/file/".$data->productName->user->image))'
        ),
        'product_id' => array(
            'name' => 'product_id',
            'value' => '$data->productName->product_name',
            'filter' => false
        ),
         'region' => array(
            'name' => 'region',
            'filter' => false,
        ),
        'standing_charge' => array(
            'name' => 'standing_charge',
            'filter' => false,
        ),
        'unit_charge' => array(
            'name' => 'unit_charge',
            'filter' => false,
        ),
        'contract_length' => array(
            'name' => 'term',
            'filter' => false,
        ),
        'min_band' => array(
            'filter' => false,
            'name' => 'min_band',
            'value' => '($data->min_aq) ? $data->min_aq : $data->min_band'
        ),
        'max_band' => array(
            'filter' => false,
            'name' => 'max_band',
            'value' => '($data->max_aq) ? $data->max_aq : $data->max_band'
        ),
        'cod' => array(
            'name' => 'cod',
            //  'value' => 'Y',
            'filter' => false,
        ),
        'smart_mtr' => array(
            'name' => 'smart_mtr',
            //'value' => 'Y',
            'filter' => false,
        ),
//        array(
//            'class' => 'bootstrap.widgets.TbButtonColumn',
//        ),
    ),
));
?>

<script>
    paginationControls();
</script>
