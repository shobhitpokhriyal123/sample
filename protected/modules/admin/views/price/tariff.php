<?php if ($type == "Gas") { ?>


    <section id="gas">
        <div class="container-fluid">
            <div class="row">

                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <strong><?php echo $type; ?> Tariffs</strong>
                        </div>
                    </div>
                    <div class="card-body">
                        <?php
                            $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                                'id' => 'tariff-form',
                                'enableAjaxValidation' => false,
                                'enableClientValidation' => false,
                                'clientOptions' => array(
                                    'validateOnSubmit' => true,
                                ),
                                'htmlOptions' => array('enctype' => 'multipart/form-data'),
                            ));
                        ?>

                        <div class="row">
                            <div class="col-sm-4">
                                <?php echo $form->dropdownListRow($model1, 'price_book_type', Product::getAllSets(), array('class' => 'span8', 'empty' => 'All Sets')); ?>
                            </div>
                            <div class="col-sm-4">
                                <?php echo $form->dropdownListRow($model, 'assigned_to', CHtml::listData(Users::model()->findAllByAttributes(array('role_id' => 2)), 'id', 'fullname'), array('class' => 'span8', 'empty' => 'All Suppliers')); ?>
                            </div>
                            <div class="col-sm-4">
                                <?php echo $form->dropdownListRow($model1, 'region', Product::getAllRegions(), array('class' => 'span11', 'empty' => 'All Regions')); ?>
                            </div>
                        </div>
                        <br>
                        <div class="text-center">
                            <button class="btn btn-primary" id="submit_filters">Submit Filters</button>
                            <button class="btn btn-default" id="clear_filters">Clear Filters</button>
                        </div>

                        <?php $this->endWidget(); ?>

                        <div id="filter_grid" class="">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>
        $(document).ready(function () {
            $("#submit_filters").click(function (e) {
                e.preventDefault();
                var assign_to = $("#Product_assigned_to").val();
                   var set = $("#Price_price_book_type").val();
                set = set.replace(/ /g, "+");
                var region = $("#Price_region").val();
                region = region.replace(/ /g, "+");
                var url = '<?php echo Yii::app()->createAbsoluteUrl("admin/price/filter&Product[assigned_to]="); ?>' + assign_to+"&Price[region]="+region+"&Price[price_book_type]="+set+"&Price[product]="+set;
                $("#filter_grid").load(url);
            });
            $("#clear_filters").click(function (e) {
                e.preventDefault();
                document.getElementById('tariff-form').reset();
                var assign_to = $("#Product_assigned_to").val();
                //var form = document.getElementById('tariff-form').serialize();
                var url = '<?php echo Yii::app()->createAbsoluteUrl("admin/product/filter"); ?>';
                $("#filter_grid").load(url);
            });
        });
    </script>

<?php } else if ($type == 'Electricity') { ?>


    <section id="electricity">
        <div class="container-fluid">
            <div class="row">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <strong><?php echo $type; ?> Tariffs</strong>
                        </div>
                    </div>
                    <div class="card-body">
                        <?php
                            $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                                'id' => 'tariff-form',
                                'enableAjaxValidation' => false,
                                'enableClientValidation' => false,
                                'clientOptions' => array(
                                    'validateOnSubmit' => true,
                                ),
                                'htmlOptions' => array('enctype' => 'multipart/form-data'),
                            ));
                        ?>

                        <div class="row">
                            <div class="col-sm-4">
                                <?php echo $form->dropdownListRow($model1, 'price_book_type', Product::getAllSets(), array('class' => 'span8', 'empty' => 'All Sets')); ?>
                            </div>
                            <div class="col-sm-2">
                                <?php echo $form->dropdownListRow($model, 'assigned_to', CHtml::listData(Users::model()->findAllByAttributes(array('role_id' => 2)), 'id', 'fullname'), array('class' => 'span8', 'empty' => 'All Suppliers')); ?>
                            </div>
                            <div class="col-sm-2">
                                <?php echo $form->dropdownListRow($model1, 'mtc', Product::getAllMTC(), array('class' => 'span8', 'empty' => 'All MTC')); ?>
                            </div>
                            <div class="col-sm-2">
                                <?php echo $form->dropdownListRow($model1, 'profile', Product::getAllPC(), array('class' => 'span8', 'empty' => 'All PC')); ?>
                            </div>
                            <div class="col-sm-2">
                                <?php echo $form->dropdownListRow($model1, 'region', Product::getAllRegions(), array('class' => 'span11', 'empty' => 'All Regions')); ?>
                            </div>
                        </div>
                        <br>
                        <div class="text-center">
                            <button class="btn btn-primary" id="submit_filters">Submit Filters</button>
                            <button class="btn btn-default" id="clear_filters">Clear Filters</button>
                        </div>

                        <?php $this->endWidget(); ?>

                        <div id="filter_grid" class="">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>
        $(document).ready(function () {
            $("#submit_filters").click(function (e) {
                e.preventDefault();
                var assign_to = $("#Product_assigned_to").val();
                var mtc = $("#Price_mtc").val();
                var profile = $("#Price_profile").val();
                profile = profile.replace(/ /g, "+");
                var set = $("#Price_price_book_type").val();
                set = set.replace(/ /g, "+");
                var region = $("#Price_region").val();
                region = region.replace(/ /g, "+");
                var url = '<?php echo Yii::app()->createAbsoluteUrl("admin/price/filterElec&Product[assigned_to]="); ?>' + assign_to + "&Price[mtc]=" + mtc + "&Price[profile]=" + profile + "&Price[region]=" + region + "&Price[product]=" + set + "&Price[price_book_type]=" + set;
                $("#filter_grid").load(url);
            });
            $("#clear_filters").click(function (e) {
                e.preventDefault();
                document.getElementById('tariff-form').reset();
                var assign_to = $("#Product_assigned_to").val();
                //var form = document.getElementById('tariff-form').serialize();
                var url = '<?php echo Yii::app()->createAbsoluteUrl("admin/product/filterElec"); ?>';
                $("#filter_grid").load(url);
            });
        });
    </script>

<?php } ?>
