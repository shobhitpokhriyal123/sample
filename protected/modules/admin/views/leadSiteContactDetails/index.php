<?php
/* @var $this LeadSiteContactDetailsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Lead Site Contact Details',
);

$this->menu=array(
	array('label'=>'Create LeadSiteContactDetails', 'url'=>array('create')),
	array('label'=>'Manage LeadSiteContactDetails', 'url'=>array('admin')),
);
?>

<h1>Lead Site Contact Details</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
