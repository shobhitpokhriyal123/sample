
<div class="card-header">

    <div class="card-title">
        <strong>Site Contact Details </strong>
    </div>
</div>

<form name="site_contact" id="site_contact" method="post">
    <?php
    $this->widget('bootstrap.widgets.TbGridView', array(
        'id' => 'lead-site-contact-details-grid',
        'afterAjaxUpdate' => 'function(id,options){ paginationControls(); }',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'columns' => array(
            // 'id',
//            'lead_id' => array(
//                'name' => 'lead_id',
//                'header' => 'Lead Name',
//                'value' => '$data->site->lead->first_name',
//                'filter' => false,
//                'footer' => ''
//            ),
            'site_id' => array(
                'name' => 'site_id',
                'header' => 'Site Name',
                'value' => '$data->site->site_name',
                'filter' => false,
                'footer' => '<input id="LeadSite_lead_id_" class="span5 form-control" maxlength="255" name="LeadSite[lead_id]" type="hidden"><input id="LeadSite_site_name_" class="span5 form-control" maxlength="255" name="LeadSite[site_name]" type="text">'//LeadSiteContactDetails::getSiteDropdown()
            ),
            'full_name' => array(
                'name' => 'full_name',
                'filter' => false,
                'footer' => '<input id="LeadSiteContactDetails_full_name_" class="form-control" size="60" maxlength="255" name="LeadSiteContactDetails[full_name]" type="text">'
            ),
            'position' => array(
                'name' => 'position',
                'filter' => false,
                'footer' => '<input id="LeadSiteContactDetails_position_" class="form-control" size="60" maxlength="255" name="LeadSiteContactDetails[position]" type="text">',
            ),
            'email' => array(
                'name' => 'email',
                'filter' => false,
                'footer' => '<input id="LeadSiteContactDetails_email_" class="form-control" size="60" maxlength="255" name="LeadSiteContactDetails[email]" type="text">'
            ),
            'landline' => array(
                'name' => 'landline',
                'filter' => false,
                'footer' => '<input id="LeadSiteContactDetails_landline_" class="form-control" size="11" maxlength="11" name="LeadSiteContactDetails[landline]" type="text">'
            ),
            'mobile' => array(
                'name' => 'mobile',
                'filter' => false,
                'footer' => '<input id="LeadSiteContactDetails_mobile_" class="form-control" size="11" maxlength="11" name="LeadSiteContactDetails[mobile]" type="text">'
            ),
            /*
              'status',
              'created',
             */
//		array(
//			'class'=>'CButtonColumn',
//		),
            array(
                'class' => 'bootstrap.widgets.TbButtonColumn',
                'template' => '{view}{update}{delete}',
                'header' => 'Actions',
                'footer' => '<button type="submit" class="btn btn-success" id="site_contact_submit" >Add</button>',
                'buttons' => array(
//               
//                'view' => array(
//                    'label' => '&nbsp;&nbsp;<i class="fa fa-check"></i>',
//                    //'visible' => '$data->status > 0', // <-- SHOW IF ROW INACTIVE
//                    'url' => 'Yii::app()->createAbsoluteUrl("admin/leadSiteContractDetails/view", array("id"=>$data->id))',
//                  //  'options' => array('title' => 'Deactivate product'),
//                    'imageUrl' => false,
//                   
//                ),
//                'update' => array(
//                    'label' => '&nbsp;&nbsp;<i class="fa fa-check"></i>',
//                   // 'visible' => '$data->status == 0', // <-- SHOW IF ROW ACTIVE
//                    'url' => 'Yii::app()->createAbsoluteUrl("admin/leadSiteContractDetails/update", array("id"=>$data->id))',
//                  //  'options' => array('title' => 'Activate product'),
//                    'imageUrl' => false,
//                   
//                ),
                ),
            ),
        ),
    ));
    ?>
</form>

<script>
    $("#site_contact_submit").click(function (e) {
        e.preventDefault();
        var siteurl = '<?php echo Yii::app()->createAbsoluteUrl("admin/leadSiteContactDetails/create") ?>';
        var data = $("#site_contact").serialize();
        $.ajax({
            data: data,
            url: siteurl,
            type: 'POST',
            success: function (res) {
                 $.fn.yiiGridView.update("lead-site-contact-details-grid");
                
            }
        });
    });

</script>