<?php
/* @var $this LeadSiteContactDetailsController */
/* @var $model LeadSiteContactDetails */

$this->breadcrumbs=array(
	'Lead Site Contact Details'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List LeadSiteContactDetails', 'url'=>array('index')),
	array('label'=>'Create LeadSiteContactDetails', 'url'=>array('create')),
	array('label'=>'Update LeadSiteContactDetails', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete LeadSiteContactDetails', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage LeadSiteContactDetails', 'url'=>array('admin')),
);
?>

<h1>View LeadSiteContactDetails #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'create_user_id',
		'title',
		'full_name',
		'position',
		'email',
		'landline',
		'mobile',
		'status',
		'created',
	),
)); ?>
