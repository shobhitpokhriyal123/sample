<?php
/* @var $this LeadSiteContactDetailsController */
/* @var $model LeadSiteContactDetails */

$this->breadcrumbs=array(
	'Lead Site Contact Details'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List LeadSiteContactDetails', 'url'=>array('index')),
	array('label'=>'Create LeadSiteContactDetails', 'url'=>array('create')),
	array('label'=>'View LeadSiteContactDetails', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage LeadSiteContactDetails', 'url'=>array('admin')),
);
?>

<h1>Update LeadSiteContactDetails <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>