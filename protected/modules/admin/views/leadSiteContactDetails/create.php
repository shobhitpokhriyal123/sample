<?php
/* @var $this LeadSiteContactDetailsController */
/* @var $model LeadSiteContactDetails */

$this->breadcrumbs=array(
	'Lead Site Contact Details'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List LeadSiteContactDetails', 'url'=>array('index')),
	array('label'=>'Manage LeadSiteContactDetails', 'url'=>array('admin')),
);
?>

<h1>Create LeadSiteContactDetails</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>