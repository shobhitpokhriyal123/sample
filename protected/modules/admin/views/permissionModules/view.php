<section id="manage_meter">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <strong>View Modules</strong>
                    </div>
                    <?php
                    if (Yii::app()->user->isAdmin) {
                        echo CHtml::link('Manage Permission Modules', array('/admin/permissionModules/admin'), array("class" => 'btn btn-success'));
                    }
                    ?>
                </div>
                <div class="card-body">
                    <?php
                    $this->widget('bootstrap.widgets.TbDetailView', array(
                        'data' => $model,
                        'attributes' => array(
                            'id',
                            'module_name',
                            'created',
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
