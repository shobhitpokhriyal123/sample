<?php
$this->breadcrumbs=array(
	'Permission Modules',
);

$this->menu=array(
	array('label'=>'Create PermissionModules','url'=>array('create')),
	array('label'=>'Manage PermissionModules','url'=>array('admin')),
);
?>

<h1>Permission Modules</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
