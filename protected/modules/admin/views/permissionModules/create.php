<section id="manage_meter">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <strong>Manage Modules</strong>
                    </div>
                    <?php
                    if (Yii::app()->user->isAdmin) {
                        echo CHtml::link('Manage Module', array('/admin/permissionModules/admin'), array("class" => 'btn btn-success'));
                    }
                    ?>
                </div>
                <div class="card-body">

                    <?php
                    
                    echo $this->renderPartial('_form', array('model' => $model)); ?>
                </div>
            </div>
        </div>
    </div>
</section>
