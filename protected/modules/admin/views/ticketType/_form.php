<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'ticket-type-form',
    'enableAjaxValidation' => false,
        ));
?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->dropdownListRow($model, 'parrent', CHtml::listData(TicketType::model()->findAllByAttributes(array('parrent'=>0)), "id", "title" ), array("empty"=>'---Select/Create Parrent ---')); ?>
<?php echo $form->textFieldRow($model, 'title', array('class' => 'span5', 'maxlength' => 255)); ?>

<?php //echo $form->textFieldRow($model,'query',array('class'=>'span5','maxlength'=>255)); ?>

    <?php //echo $form->textFieldRow($model,'created',array('class'=>'span5'));  ?>

<div class="form-actions">
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type' => 'primary',
        'label' => $model->isNewRecord ? 'Create' : 'Save',
    ));
    ?>
</div>

<?php $this->endWidget(); ?>
