<?php

$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'lead-quote-grid1',
    'dataProvider' => $model->search($cid),
    // 'filter' => $model,
    'columns' => array(
        // 'id',
        'customer_id' => array(
            'filter' => false,
            'name' => 'customer_id',
            'value' => '($data->tender && $data->tender->customer) ? $data->tender->customer->first_name: "N/A"'
        ),
        'site_id' => array(
            'name' => 'site_id',
            'header' => 'Sites',
            'filter' => false,
            'value' => 'LeadQuote::getSiteName($data->meter_ids,$data->meter_type )'
        ),
        'supplier_id' => array(
            'filter' => false,
            'name' => 'supplier_id',
            'value' => '$data->user->username'
        ),
         'meter_ids' => array(
            'filter' => false,
            'name' => 'meter_ids',
            'value' => 'LeadQuote::getSiteName($data->meter_ids,$data->meter_type, 1)'
        ),
        
        'product',
        'min_volume',
        'max_volume',
        'credit_position',
////                            'supply_contract' => array(
////                               'type' => 'raw',
////                                'filter' => false,
////                                'name' => 'supply_contract',
////                                'value' => 'CHtml::link(Yii::app()->createAbsoluteUrl("admin/product/download/file/" . $data->supply_contract)',
//                            //       <a class="btn btn-primary" href="<?php echo Yii::app()->createAbsoluteUrl('admin/product/download/file/' . $model->csv_file)>
////                            'offer_type',
        'consumption',
        'unit_rate' => array(
            'name' => 'unit_rate',
            'filter' => false,
            'value' => '$data->getUnit_rate()'
        ),
        'standing_charge',
        'supply_contract' => array(
            'name' => 'supply_contract',
            'filter' => false,
            'type' => 'raw',
            'value' => '$data->getDownload()'
        ),
////   'meter_no',
//                            'created',
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '',
//            'buttons' => array(
//                'upload' => array(
//                    'label' => '<button class="btn btn-primary fa fa-upload">Upload Signed Contract</button>',
//                    'url' => 'Yii::app()->createUrl("/admin/leadQuote/upload", array("id"=>$data->id))',
//                    'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Upload Signed Contract'),
//                    'click' => "function(e){
//                                            e.preventDefault();
//                                            var url = $(this).attr('href');
//                                            $.ajax({
//                                              type: 'POST',
//                                              url: url,
//                                              success: function(res){
//                                                $('#myModal').modal();
//                                                $('#form_').html(res);
//                                                },
//                                             });
//                                         }",
//                ),
//                'viewd' => array(
//                    'label' => '<button class="btn btn-primary fa fa-eye">View Details</button>',
//                    'url' => 'Yii::app()->createUrl("/admin/leadContacts/updateQuote", array("id"=>$data->id))',
//                    'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'View Contract detail'),
//                    'click' => "function(e){
//                                            e.preventDefault();
//                                            var url = $(this).attr('href');
//                                            $.ajax({
//                                              type: 'POST',
//                                              url: url,
//                                              success: function(res){
//                                                $('#myModal1').modal();
//                                                $('#view_').html(res);
//                                                },
//                                             });
//                                         }",
//                ),
//            ),
        ),
    ),
));
?>