<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('customer_id')); ?>:</b>
	<?php echo CHtml::encode($data->customer_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('supplier_id')); ?>:</b>
	<?php echo CHtml::encode($data->supplier_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('site_id')); ?>:</b>
	<?php echo CHtml::encode($data->site_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('product')); ?>:</b>
	<?php echo CHtml::encode($data->product); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('min_volume')); ?>:</b>
	<?php echo CHtml::encode($data->min_volume); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('max_volume')); ?>:</b>
	<?php echo CHtml::encode($data->max_volume); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('credit_position')); ?>:</b>
	<?php echo CHtml::encode($data->credit_position); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('supply_contract')); ?>:</b>
	<?php echo CHtml::encode($data->supply_contract); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('offer_type')); ?>:</b>
	<?php echo CHtml::encode($data->offer_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('consumption')); ?>:</b>
	<?php echo CHtml::encode($data->consumption); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('unit_rate')); ?>:</b>
	<?php echo CHtml::encode($data->unit_rate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('standing_charge')); ?>:</b>
	<?php echo CHtml::encode($data->standing_charge); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meter_no')); ?>:</b>
	<?php echo CHtml::encode($data->meter_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />

	*/ ?>

</div>