<?php
$this->breadcrumbs=array(
	'Lead Quotes'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List LeadQuote','url'=>array('index')),
	array('label'=>'Create LeadQuote','url'=>array('create')),
	array('label'=>'View LeadQuote','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage LeadQuote','url'=>array('admin')),
);
?>

<h1>Update LeadQuote <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>