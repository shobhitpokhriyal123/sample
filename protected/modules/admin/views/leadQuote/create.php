<?php
$this->breadcrumbs=array(
	'Lead Quotes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List LeadQuote','url'=>array('index')),
	array('label'=>'Manage LeadQuote','url'=>array('admin')),
);
?>

<h1>Create LeadQuote</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>