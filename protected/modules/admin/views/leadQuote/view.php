<?php
$this->breadcrumbs=array(
	'Lead Quotes'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List LeadQuote','url'=>array('index')),
	array('label'=>'Create LeadQuote','url'=>array('create')),
	array('label'=>'Update LeadQuote','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete LeadQuote','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage LeadQuote','url'=>array('admin')),
);
?>

<h1>View LeadQuote #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'customer_id',
		'supplier_id',
		'site_id',
		'product',
		'min_volume',
		'max_volume',
		'credit_position',
		'supply_contract',
		'offer_type',
		'consumption',
		'unit_rate',
		'standing_charge',
		'meter_no',
		'created',
	),
)); ?>
