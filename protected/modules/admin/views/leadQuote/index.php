<?php
$this->breadcrumbs=array(
	'Lead Quotes',
);

$this->menu=array(
	array('label'=>'Create LeadQuote','url'=>array('create')),
	array('label'=>'Manage LeadQuote','url'=>array('admin')),
);
?>

<h1>Lead Quotes</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
