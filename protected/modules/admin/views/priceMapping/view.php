<?php
$this->breadcrumbs=array(
	'Price Mappings'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List PriceMapping','url'=>array('index')),
	array('label'=>'Create PriceMapping','url'=>array('create')),
	array('label'=>'Update PriceMapping','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete PriceMapping','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PriceMapping','url'=>array('admin')),
);
?>

<h1>View PriceMapping #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'product_id',
		'attribute_name',
		'attribute_slug',
		'attribute_value',
		'attribute_type',
		'status',
		'created',
	),
)); ?>
