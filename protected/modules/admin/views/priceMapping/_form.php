<hr>
<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'price-mapping-form',
    'enableAjaxValidation' => true,
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
        ));
?>

<?php //echo $form->errorSummary($model);  ?>
<div class="row">
    <div class="col-sm-6">
        <?php echo $form->textFieldRow($model, 'attribute_name', array('class' => 'form-control', 'maxlength' => 255, 'placeholder' => 'Attribute Name')); ?>
    </div>
    <div class="col-sm-5">
        <?php echo $form->textFieldRow($model, 'attribute_slug', array('class' => 'form-control', 'maxlength' => 255, 'placeholder' => 'Attribute Slug', 'readOnly' => true)); ?>
    </div>

    <div class="col-sm-1">
        <div class="">
            <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType' => 'submit',
                'type' => 'primary',
                'label' => $model->isNewRecord ? 'Create' : 'Save',
            ));
            ?>
        </div>
    </div>
</div>
<br>

<?php echo $form->hiddenField($model, 'product_id', array('value' => $pid)); ?>

<?php $this->endWidget(); ?>
<script>
    $("#PriceMapping_attribute_name").keyup(function () {
        var Text = $(this).val();
        Text = Text.toLowerCase();
        var regExp = /\s+/g;
        Text = Text.replace(regExp, '_');
        
        $("#PriceMapping_attribute_slug").val(Text);
        if($("#PriceMapping_attribute_slug").val() == 'id'){
            $("#PriceMapping_attribute_slug").val($("#PriceMapping_attribute_slug").val() + '_')
        }
        
        
    });
</script>
