<?php
$this->breadcrumbs=array(
	'Price Mappings'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List PriceMapping','url'=>array('index')),
	array('label'=>'Create PriceMapping','url'=>array('create')),
	array('label'=>'View PriceMapping','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage PriceMapping','url'=>array('admin')),
);
?>

<h1>Update PriceMapping <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>