<?php
$this->breadcrumbs=array(
	'Price Mappings'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PriceMapping','url'=>array('index')),
	array('label'=>'Manage PriceMapping','url'=>array('admin')),
);
?>

<h1>Create PriceMapping</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>