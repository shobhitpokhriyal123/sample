<?php
$this->breadcrumbs=array(
	'Price Mappings',
);

$this->menu=array(
	array('label'=>'Create PriceMapping','url'=>array('create')),
	array('label'=>'Manage PriceMapping','url'=>array('admin')),
);
?>

<h1>Price Mappings</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
