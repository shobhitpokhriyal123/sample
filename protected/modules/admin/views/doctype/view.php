<section id="manage_products">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="card-title">
                        <strong>view Doctype</strong>
                    </div>

                    <?php
                    if (Yii::app()->user->isAdmin) {
                        echo CHtml::link('Manage Doctype', array('/admin/doctype/admin'), array("class" => 'btn btn-success'));
                    }
                    ?>
                </div>
                <div class="card-body">
                    <?php
                    $this->widget('bootstrap.widgets.TbDetailView', array(
                        'data' => $model,
                        'attributes' => array(
                            'id',
                            'title',
                            'status',
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
