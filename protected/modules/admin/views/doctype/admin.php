<section id="manage_products">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="card-title">
                        <strong>Manage Doctypes</strong>
                    </div>

                    <?php
                    if (Yii::app()->user->isAdmin) {
                        echo CHtml::link('Create Doctype', array('/admin/doctype/create'), array("class" => 'btn btn-success'));
                    }
                    ?>

                </div>
                <div class="card-body">
                    <?php
                    $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => 'doctype-grid',
                        'dataProvider' => $model->search(),
                           'afterAjaxUpdate' => 'function(id,options){ paginationControls(); }',
                        'filter' => $model,
                        'columns' => array(
                            //'id',
                            'title' => array(
                                'name' => 'title',
                                'filter' => false
                            ),
                            // 'status',
                            array(
                                'class' => 'bootstrap.widgets.TbButtonColumn',
                                'template' => '{view}{update}{delete}',
                                //  'evaluateID'=>true,
                                'header' => 'Actions',
                                'buttons' => array(
                                    'view' => array(
                                      //  'label' => '<button class="btn btn-primary">Save</button>',
                                        // 'url' => 'Yii::app()->createAbsoluteUrl("admin/quote/create", array("id"=>$data->id))',
                                        'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'View'),
                                    ),
                                    'update' => array(
                                       // 'label' => '<button class="btn btn-primary">Save</button>',
                                        // 'url' => 'Yii::app()->createAbsoluteUrl("admin/quote/create", array("id"=>$data->id))',
                                        'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Update'),
                                    ),
                                    'delete' => array(
                                      //  'label' => '<button class="btn btn-primary">Save</button>',
                                        // 'url' => 'Yii::app()->createAbsoluteUrl("admin/quote/create", array("id"=>$data->id))',
                                        'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Delete'),
                                    ),
                                ),
                            ),
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
