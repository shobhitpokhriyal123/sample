<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('brocker_id')); ?>:</b>
	<?php echo CHtml::encode($data->brocker_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_user_id')); ?>:</b>
	<?php echo CHtml::encode($data->create_user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('customer_name')); ?>:</b>
	<?php echo CHtml::encode($data->customer_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reg_address1')); ?>:</b>
	<?php echo CHtml::encode($data->reg_address1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reg_address2')); ?>:</b>
	<?php echo CHtml::encode($data->reg_address2); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('reg_address3')); ?>:</b>
	<?php echo CHtml::encode($data->reg_address3); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reg_address4')); ?>:</b>
	<?php echo CHtml::encode($data->reg_address4); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('postcode')); ?>:</b>
	<?php echo CHtml::encode($data->postcode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('company_reg_no')); ?>:</b>
	<?php echo CHtml::encode($data->company_reg_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('company_profile_pic')); ?>:</b>
	<?php echo CHtml::encode($data->company_profile_pic); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('letter_authority')); ?>:</b>
	<?php echo CHtml::encode($data->letter_authority); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('authority_start_date')); ?>:</b>
	<?php echo CHtml::encode($data->authority_start_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('authority_end_date')); ?>:</b>
	<?php echo CHtml::encode($data->authority_end_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('payment_terms')); ?>:</b>
	<?php echo CHtml::encode($data->payment_terms); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('billing_req')); ?>:</b>
	<?php echo CHtml::encode($data->billing_req); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('active')); ?>:</b>
	<?php echo CHtml::encode($data->active); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />

	*/ ?>

</div>