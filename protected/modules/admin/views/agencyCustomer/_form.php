<div class="row">
    <?php
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'agency-customer-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    ));
    ?>

    <p class="help-block">Fields with <span class="required">*</span> are required.</p>

    <?php //echo $form->errorSummary($model); ?>

    <?php //echo $form->dropDownListRow($model,'user_id', Agency::getAgency());  ?>
    <div class="col-md-6">

        <?php echo $form->dropDownListRow($model, 'brocker_id', Agency::getAgency()); ?>
        <?php echo $form->dropDownListRow($model, 'teams', AgencyCustomer::getTeams()); ?>
        <?php //echo $form->textFieldRow($model,'create_user_id',array('class'=>'span5')); ?>

        <?php echo $form->textFieldRow($model, 'customer_name', array('class' => 'span5', 'maxlength' => 255)); ?>

        <?php echo $form->textFieldRow($model, 'reg_address1', array('class' => 'span5', 'maxlength' => 255)); ?>

        <?php echo $form->textFieldRow($model, 'reg_address2', array('class' => 'span5', 'maxlength' => 255)); ?>

        <?php echo $form->textFieldRow($model, 'reg_address3', array('class' => 'span5', 'maxlength' => 255)); ?>

        <?php echo $form->textFieldRow($model, 'reg_address4', array('class' => 'span5', 'maxlength' => 255)); ?>

        <?php echo $form->textFieldRow($model, 'postcode', array('class' => 'span5', 'maxlength' => 255)); ?>


    </div> 
    <div class="col-md-6">
        <?php echo $form->textFieldRow($model, 'company_reg_no', array('class' => 'span5', 'maxlength' => 255)); ?>

        <?php echo $form->fileFieldRow($model, 'company_profile_pic', array('class' => 'span5', 'maxlength' => 255)); ?>
        <?php echo $form->fileFieldRow($model, 'letter_authority', array('class' => 'span5', 'maxlength' => 255)); ?>


        <?php echo $form->label($model, 'authority_start_date'); ?>
        <?php
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model' => $model,
            'attribute' => 'authority_start_date',
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                'changeMonth' => true,
                'changeYear' => true,
            ),
        ));
        ?>

        <?php echo $form->error($model, 'authority_start_date'); ?>


        <?php echo $form->label($model, 'authority_end_date'); ?>
        <?php
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model' => $model,
            'attribute' => 'authority_end_date',
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                'changeMonth' => true,
                'changeYear' => true,
            ),
        ));
        ?>

        <?php echo $form->error($model, 'authority_end_date'); ?>




        <?php //echo $form->textFieldRow($model, 'authority_start_date', array('class' => 'span5', 'maxlength' => 255)); ?>

        <?php //echo $form->textFieldRow($model, 'authority_end_date', array('class' => 'span5', 'maxlength' => 255)); ?>

        <?php echo $form->textFieldRow($model, 'payment_terms', array('class' => 'span5', 'maxlength' => 255)); ?>

        <?php echo $form->textFieldRow($model, 'billing_req', array('class' => 'span5', 'maxlength' => 255)); ?>

        <?php echo $form->dropDownListRow($model, 'active', array('1' => 'Active', '0' => 'Inactive')); ?>

        <?php //echo $form->textFieldRow($model,'created',array('class'=>'span5'));  ?>
    </div>
    <?php if (!$model->id) { ?>
     <div class="clearfix"></div>
        <div class="row-fluid">
            <div class="col-md-6">
                <h4>Login Details</h4>
                <?php echo $form->textFieldRow($user, 'username', array('class' => 'span5', 'maxlength' => 255)); ?>
                <?php echo $form->textFieldRow($user, 'email', array('class' => 'span5', 'maxlength' => 255)); ?>

          
                <?php echo $form->passwordFieldRow($user, 'password', array('class' => 'span5', 'maxlength' => 255)); ?>
                <?php echo $form->passwordFieldRow($user, 'confirm_password', array('class' => 'span5', 'maxlength' => 255)); ?>
            </div>
        </div>
    <?php } ?>

    <div class="clearfix"></div>
    <div class="form-actions col-md-6">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type' => 'primary',
            'label' => $model->isNewRecord ? 'Create' : 'Save',
        ));
        ?>
    </div>

    <?php $this->endWidget(); ?>
</div>