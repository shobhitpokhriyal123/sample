<section id="manage_productss">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="card-title">
                        <strong>View Customer by Username: <?php echo $model->user->username; ?></strong>
                    </div>

                    <?php
                    if (Yii::app()->user->isAdmin || Yii::app()->user->isAgency) {
                        echo CHtml::link('Create Site ', array('/admin/customerSite/create', 'cid' => $model->id), array("class" => 'btn btn-success'));
                        echo CHtml::link('Input quoted price', array('/admin/customerQuote/create', 'cid' => $model->id), array("class" => 'btn btn-success'));
//                        echo CHtml::link('Add Site', array('/admin/customerSite/create', 'cid' => $model->id), array("class" => 'btn btn-success'));
                    }
                    ?>

                </div>
                <div class="card-body">


                    <div class="card-title sub-title">
                        <strong>Agency Customer</strong>
                    </div>
                    <?php
                    $this->widget('bootstrap.widgets.TbDetailView', array(
                        'data' => $model,
                        'attributes' => array(
                            // 'id',
                            // 'user_id',
                            'brocker_id' => array(
                                'name' => 'brocker_id',
                                'value' => $model->broker->username
                            ),
                            //  'create_user_id',
                            'customer_name',
                            'reg_address1',
                            'reg_address2',
                            //  'reg_address3',
                            //  'reg_address4',
                            'postcode',
                            'company_reg_no',
                            //  'company_profile_pic',
                            // 'letter_authority',
                            'authority_start_date',
                            'authority_end_date',
                            'payment_terms',
                            'billing_req',
                        // 'active',
                        //   'created',
                        ),
                    ));
                    ?>

                    <div id="customer_sites">
                        <div class="card-title sub-title">
                            <strong>Customer Site</strong>
                        </div> 

                        <!--                            'dataProvider' => $model1->search($model->id),-->
<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'customer-site-grid',
    'dataProvider' => $model1->search($model->id),
    'filter' => $model1,
    'columns' => array(
        //'id',
        'customer_id' => array(
            'name' => 'customer_id',
            'type' => 'html',
            'value' => 'CHtml::link($data->customer->customer_name,array("/admin/agencyCustomer/view","id"=>$data->customer->id))'
        ),
        //'create_user_id',
        'site_name',
        'site_address1',
        'site_address2',
        'site_address3',
        'postcode',
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{update}{view}{new_meter}{new_contract}',
            'header' => 'Actions',
            'buttons' => array(
                'view' => array(
                    'label' => '<button class="btn btn-primary">View</button>',
                    'url' => 'Yii::app()->createAbsoluteUrl("admin/customerSite/view", array("id"=>$data->id))',
                    'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'View Site'),
                    'imageUrl' => false,
                ),
                'update' => array(
                    'label' => '<button class="btn btn-primary">update</button>',
                    'url' => 'Yii::app()->createAbsoluteUrl("admin/customerSite/update", array("id"=>$data->id))',
                    'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Update Site'),
                    'imageUrl' => false,
                ),
                'new_meter' => array(
                    'label' => '<button class="btn btn-primary">Create Meter</button>',
                    'url' => 'Yii::app()->createAbsoluteUrl("admin/customerSiteMeter/create", array("sid"=>$data->id))',
                    'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Create New Meter'),
                    'imageUrl' => false,
                ),
                'new_contract' => array(
                    'label' => '<button class="btn btn-primary">Create Contract</button>',
                    'url' => 'Yii::app()->createAbsoluteUrl("admin/customerContract/create", array("sid"=>$data->id))',
                    'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Create New Contract'),
                    'imageUrl' => false,
                ),
            ),
        ),
    ),
));
?>
                    </div>
                    <div id="customer_meters">
                        <div class="card-title sub-title">
                            <strong>Customer Site Meter</strong>
                        </div> 

<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'customer-site-meter-grid',
    'dataProvider' => $model2->searchMeters($model->id),
    'filter' => $model2,
    'columns' => array(
        //   'id',
        'site_id' => array(
            'name' => 'site_id',
            'value' => '$data->site->site_name'
        ),
        //   'create_user_id',
        'utility' => array(
            'name' => 'utility',
            'value' => 'Product::getUtilites($data->utility)'
        ),
        'meter_no',
        'profile_type' => array(
            'name' => 'profile_type',
            'value' => 'CustomerSiteMeter::getProfileType($data->profile_type)'
        ),
        'meter_type' => array(
            'name' => 'meter_type',
            'value' => 'CustomerSiteMeter::getMeterType($data->meter_type)',
        ),
        'available_capability',
        'consumption' => array(
            'filter' => false,
            'name' => 'consumption',
            'type' => 'raw',
            'value' => '$data->getDownloadLink()'
        ),
        //'status',
        //'created',
        /**/
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{update}{view}{delete}',
            'header' => 'Actions',
            'buttons' => array(
                'update' => array(
                    'label' => '<button class="btn btn-primary">update</button>',
                    'url' => 'Yii::app()->createAbsoluteUrl("admin/customerSiteMeter/update", array("id"=>$data->id))',
                    'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'update site meter'),
                    'imageUrl' => false,
                ),
                'view' => array(
                    'label' => '<button class="btn btn-primary">view</button>',
                    'url' => 'Yii::app()->createAbsoluteUrl("admin/customerSiteMeter/view", array("id"=>$data->id))',
                    'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'view Site Meter'),
                    'imageUrl' => false,
                ),
                  'delete' => array(
                   // 'label' => '<i class="fa fa-icon-trash">Delete</i>',
                    'url' => 'Yii::app()->createAbsoluteUrl("admin/customerSiteMeter/delete", array("id"=>$data->id))',
                    'options' => array( 'rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Delete'),
                    'imageUrl' => false,
                    'click' => "function(e){
                               e.preventDefault();
                               var url = $(this).attr('href');
                              // alert(url);
                              $.ajax({
                                 type: 'POST',
                                 url: url,
                                  success: function(res){
                                  $.fn.yiiGridView.update('customer-site-meter-grid');
                                },
                                });
                                
                           
                            }",
                ),
                
            ),
        ),
    ),
));
?>


                    </div>


                    <div id="customer_meters">
                        <div class="card-title sub-title">
                            <strong>Customer Contract</strong>
                        </div> 

<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'customer-contract-grid',
    'dataProvider' => $model3->searchContracts2($model->id),
    'filter' => $model3,
    'columns' => array(
        // 'id',
        'customer_id' => array(
            'name' => 'customer_id',
            'filter' => false,
            'value' => '$data->customer->customer_name'
        ),
        'site_id' => array(
            'name' => 'site_id',
            'filter' => false,
            'value' => '$data->site->site_name'
        ),
        //  'create_user_id',
        'corporate_sme' => array(
            'name' => 'corporate_sme',
            'filter' => false, // Product::getCorporateSME(),
            'value' => 'Product::getCorporateSME($data->corporate_sme)',
        ),
        'utility' => array(
            'name' => 'utility',
            'filter' => false, // Product::getUtilites(),
            'value' => 'Product::getUtilites($data->utility)',
        ),
        'start_date' => array(
            'filter' => false,
            'name' => 'start_date'
        ),
        /*
          'created',
         */
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{create}{delete}',
            'header' => 'Tender',
            'buttons' => array(
                'create' => array(
                    'label' => '<button class="btn btn-primary">Create Tender</button>',
                    'url' => 'Yii::app()->createAbsoluteUrl("admin/customerTender/create", array("contract_id"=>$data->id))',
                    //'visible' => '$data->status > 0', // <-- SHOW IF ROW INACTIVE
                    // 'url' => 'Yii::app()->createAbsoluteUrl("admin/product/changeStatus", array("id"=>$data->id))',
                    'options' => array('title' => 'Create Contract'),
                    'imageUrl' => false,
                ),
                     'delete' => array(
                   // 'label' => '<i class="fa fa-icon-trash">Delete</i>',
                    'url' => 'Yii::app()->createAbsoluteUrl("admin/customerContract/delete", array("id"=>$data->id))',
                    'options' => array( 'rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Delete'),
                    'imageUrl' => false,
                    'click' => "function(e){
                               e.preventDefault();
                               var url = $(this).attr('href');
                              
                               $.ajax({
                                 type: 'POST',
                                 url: url,
                                  success: function(res){
                                  $.fn.yiiGridView.update('customer-contract-grid');
                                },
                                });
                                
                           
                            }",
                ),
            ),
        ),
    ),
));
?>

                    </div>

                    <div id="customer_meters">
                        <div class="card-title sub-title">
                            <strong>Customer Quote</strong>
                        </div> 
   <?php
                    $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => 'customer-quote-grid',
                        'dataProvider' => $model4->searchQuote($model->id),
                        'filter' => $model4,
                        'columns' => array(
//                            'id',
                            //  'create_user_id',
                            'customer_id' => array(
                                'name' => 'customer_id',
                                'filter' => false,
                                'value' => '$data->customer->customer_name'
                            ),
                            'supplier_id' => array(
                                'name' => 'supplier_id',
                                'filter' => false,
                                'type' => 'raw',
                                'value' => 'CHtml::image(Yii::app()->createAbsoluteUrl("admin/users/thumbnail/file/".$data->user->image))',
                                'htmlOptions' => array('class' => 'table-images')
                            ),
//                           
                            'product',
                            'min_volume',
                            'max_volume',
                            'credit_position',
                            'supply_contract',
                            'offer_type' => array(
                                'name' => 'offer_type',
                                'filter' => false,
                                'value' => 'CustomerQuote::getOfferTypes($data->offer_type)'
                            ),
                            'consumption',
                            /*  'created',
                             */
                            array(
                                'class' => 'bootstrap.widgets.TbButtonColumn',
                                'template' => '{view}{update}{delete}{print_quote}',
                                'header' => 'Tender',
                                'buttons' => array(
                                    'print_quote' => array(
                                        'label' => '<button class="btn btn-primary">Print Pdf Quote</button>',
                                        'url' => 'Yii::app()->createAbsoluteUrl("admin/customerQuote/pdf", array("id"=>$data->id))',
                                        //'visible' => '$data->status > 0', // <-- SHOW IF ROW INACTIVE
                                        // 'url' => 'Yii::app()->createAbsoluteUrl("admin/product/changeStatus", array("id"=>$data->id))',
                                        'options' => array('title' => 'Print Pdf Quote', 'target' => '_blank'),
                                        'imageUrl' => false,
                                    ),
                                ),
                            ),
                        ),
                    ));
                    ?>
                    </div>


                    <div id="customer_meters">
                        <div class="card-title sub-title">
                            <strong>Customer Tender</strong>
                        </div> 
               <?php
                    $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => 'customer-tender-grid',
                        'dataProvider' => $model5->searchTender($model->id),
                        'filter' => $model5,
                        'columns' => array(
                            // 'id',
//                            'customer',
//                            'utility',
//                            'start_date',
//                            'end_date',


                            'contract_id' => array(
                                'name' => 'contract_id',
                                'filter' => false,
                            ),
                            // 'create_user_id',
                            'contract_duration'=>array(
                                'name'=>'contract_duration',
                                'filter'=>false,
                            ),
                            'supplier_ids' => array(
                                'type' => 'raw',
                                'name' => 'supplier_ids',
                                'filter' => false,
                                'value' => 'CustomerTender::getSuppliers($data->supplier_ids)',
                            ),
                            'bespoke_duration'=>array(
                                'name'=>'bespoke_duration',
                                'filter'=>false,
                            ),
                            'commission_discount'=>array(
                                'name'=>'commission_discount',
                                'filter'=>false,
                            ),
                            'tender_return_date'=>array(
                                'name'=>'tender_return_date',
                                'filter'=>false,
                            ),
                            'comments'=>array(
                                'name'=>'comments',
                                'filter'=> false,
                            ),
                            /*  'created',
                             */
                            
                               array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{view}{update}{delete}',
            'header' => 'Tender',
            'buttons' => array(
                'view' => array(
                    'label' => '<button class="btn btn-primary">view </button>',
                    'url' => 'Yii::app()->createAbsoluteUrl("admin/customerTender/view", array("id"=>$data->id))',
                    'options' => array('title' => 'Create Contract'),
                    'imageUrl' => false,
                ),
                'update' => array(
                    'label' => '<button class="btn btn-primary">update </button>',
                    'url' => 'Yii::app()->createAbsoluteUrl("admin/customerTender/update", array("id"=>$data->id))',
                    'options' => array('title' => 'Create Contract'),
                    'imageUrl' => false,
                ),
                
                
                 'delete' => array(
                   // 'label' => '<i class="fa fa-icon-trash">Delete</i>',
                    'url' => 'Yii::app()->createAbsoluteUrl("admin/customerTender/delete", array("id"=>$data->id))',
                    'options' => array( 'rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Delete'),
                    'imageUrl' => false,
                    'click' => "function(e){
                               e.preventDefault();
                               var url = $(this).attr('href');
                              
                               $.ajax({
                                 type: 'POST',
                                 url: url,
                                  success: function(res){
                                  $.fn.yiiGridView.update('customer-tender-grid');
                                },
                                });
                                
                           
                            }",
                ),
            ),
        ),
                            
                           
                        ),
                    ));
                    ?>

                    </div>
                </div>



            </div>
        </div>
    </div>
</section>

<script>
    // var site_url = '<?php // echo Yii::app()->createAbsoluteUrl("admin/customerSite/sites",array("cid"=>$model->id))     ?>';
    // $("#customer_sites").load(site_url);
    //$("#customer_meters").load(meter_url);

</script>



<style>
    .sub-title {
        font-size: 15px;
        text-align: center;
        color: green;
        margin: 10px 0 0 0;
    }
</style>



<script>
    $(document).ready(function (e) {
        $(".upload_class").click(function (e) {
            // alert('sdfsdf');
            e.preventDefault();
            var sid = $(this).attr('id');
            $('#myModalddd').modal();
            $.ajax({
                url: '<?php echo Yii::app()->createAbsoluteUrl("admin/customerSiteMeter/upload/sid/") ?>/' + sid,
                type: 'POST',
            }).done(function (res) {
                $("#moddal_id").html(res);
            });
        });

    })


</script>
<div id="myModalddd" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content" id="moddal_id">


        </div>

    </div>

</div>
