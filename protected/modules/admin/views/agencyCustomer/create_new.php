<section id="manage_products">
    
    
    <div class="container-fluid">
        <div class="row">
            <div class="card">
                <div class="card-header">

                    <div class="card-title">
                        <strong>Customer Management</strong>
                    </div>

                    <?php
                    if (Yii::app()->user->isAdmin) {
                        echo CHtml::link('Details', array('/admin/agencyCustomer/admin'), array("class" => 'btn btn-success'));
                    }
                    ?>

                </div>
                <div class="card-body">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="container-fluid">
                        <?php
                        
                        echo $this->renderPartial('_form2', array(
                            'model' => $model,
                            'l_contract' => $l_contract,
                            'l_quote' => $l_quote,
                            'l_site' => $l_site,
                            'l_site_meter' => $l_site_meter,
                            'l_tender' => $l_tender,
                            'user' => $user
                        ));
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
     var contact_url = '<?php echo Yii::app()->createAbsoluteUrl("/admin/agencyCustomer/admin") ?>';
    $("#agency_customer").load(contact_url);
    


    var address_details = '<?php echo Yii::app()->createAbsoluteUrl("/admin/customerContract/admin") ?>';
    $("#customer_contract").load(address_details);
   

    var gas_detail = '<?php echo Yii::app()->createAbsoluteUrl("/admin/customerQuote/admin") ?>';
    $("#customer_quote").load(gas_detail);

    var business_detail = '<?php echo Yii::app()->createAbsoluteUrl("/admin/customerSite/admin") ?>';
    $("#customer_site").load(business_detail);

    var url = '<?php echo Yii::app()->createAbsoluteUrl("/admin/customerSiteMeter/admin") ?>';
  //y alert(url);
    $("#site_meter").load(url);

   
    var site_detail = '<?php echo Yii::app()->createAbsoluteUrl("/admin/customerTender/admin") ?>';
    $("#customer_tender").load(site_detail);


</script>


<!-- Trigger the modal with a button -->

