<!--customer agency data  form -->

<div class="card-header">

    <div class="card-title">
        <strong>Customer Details</strong>
    </div>
</div>


<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'agency-customer-form',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
?>
<!--
<div class="col-md-12">
    <p class="help-block">Fields with <span class="required">*</span> are required.</p>
    <div class="col-lg-6">
<?php echo $form->dropDownListRow($model, 'brocker_id', Agency::getAgency()); ?>
    </div>
    <div class="col-lg-6">
<?php echo $form->dropDownListRow($model, 'teams', AgencyCustomer::getTeams()); ?>
    </div>
    <div class="col-lg-6">
<?php echo $form->textFieldRow($model, 'create_user_id', array('class' => 'span5')); ?>
    </div>
    <div class="col-lg-6">
<?php echo $form->textFieldRow($model, 'customer_name', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div> 
    <div class="col-lg-6">
<?php echo $form->textFieldRow($model, 'reg_address1', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div> 
    <div class="col-lg-6">
<?php echo $form->textFieldRow($model, 'reg_address2', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>
    <div class="col-lg-6">
<?php echo $form->textFieldRow($model, 'reg_address3', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div> 
    <div class="col-lg-6">
<?php echo $form->textFieldRow($model, 'reg_address4', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div> 
    <div class="col-lg-6">
<?php echo $form->textFieldRow($model, 'postcode', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>  
    <div class="col-lg-6">
<?php echo $form->textFieldRow($model, 'company_reg_no', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div> 
    <div class="col-lg-6">
<?php echo $form->fileFieldRow($model, 'company_profile_pic', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div> 
    <div class="col-lg-6">
<?php echo $form->fileFieldRow($model, 'letter_authority', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div> 
    <div class="col-lg-6">

<?php echo $form->label($model, 'authority_start_date'); ?>
        <?php
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model' => $model,
            'attribute' => 'authority_start_date',
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                'changeMonth' => true,
                'changeYear' => true,
            ),
        ));
        ?>
    </div> 
    <div class="col-lg-6">
<?php echo $form->error($model, 'authority_start_date'); ?>
    </div> 
    <div class="col-lg-6">
<?php echo $form->label($model, 'authority_end_date'); ?>
        <?php
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model' => $model,
            'attribute' => 'authority_end_date',
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                'changeMonth' => true,
                'changeYear' => true,
            ),
        ));
        ?>

        <?php echo $form->error($model, 'authority_end_date'); ?>
    </div>
    <div class="col-lg-6">
<?php echo $form->textFieldRow($model, 'payment_terms', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div> 
    <div class="col-lg-6">
<?php echo $form->textFieldRow($model, 'billing_req', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>
    <div class="col-lg-6">
<?php echo $form->dropDownListRow($model, 'active', array('1' => 'Active', '0' => 'Inactive')); ?>
    </div> 
    <div class="col-lg-6">
<?php //echo $form->textFieldRow($model,'created',array('class'=>'span5'));   ?>
    </div>
</div> -->
<div class="col-md-12"> 

<?php if (!$model->id) { ?>
        <div class="clearfix"></div>
        <div class="row-fluid">

<!--
            <div class="card-header">

                <div class="card-title">
                    <strong>Customer Login Details </strong>
                </div>
            </div>-->
            <div class="col-lg-6">
    <?php echo $form->textFieldRow($user, 'username', array('class' => 'span5', 'maxlength' => 255)); ?>
            </div>
            <div class="col-lg-6">
    <?php echo $form->textFieldRow($user, 'email', array('class' => 'span5', 'maxlength' => 255)); ?>
            </div>
            <div class="col-lg-6">
    <?php echo $form->passwordFieldRow($user, 'password', array('class' => 'span5', 'maxlength' => 255)); ?>
            </div>
            <div class="col-lg-6">
    <?php echo $form->passwordFieldRow($user, 'confirm_password', array('class' => 'span5', 'maxlength' => 255)); ?>

            </div>
<?php } ?>



        <div class="form-actions col-lg-12 create">
            <br>
<?php
$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'submit',
    'type' => 'primary',
    'label' => $model->isNewRecord ? 'Create New User' : 'Save',
));
?>

        </div>

    </div>
</div>
<?php $this->endWidget(); ?>
<div class="col-md-12" id="agency_customer"></div>


<div class="card-header">

    <div class="card-title">

        <strong>Customer Tender Details </strong>
    </div>
</div>

<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'customer-tender-form',
    'action' => $this->createAbsoluteUrl('LeadTender/create'),
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        )
);
?>
<div class="col-md-12">
    <p class="note">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($l_tender); ?>

    <div class="col-lg-6">
<?php echo $form->labelEx($l_tender, 'contract_id'); ?>
        <?php echo $form->textField($l_tender, 'contract_id'); ?>
        <?php //echo $form->error($l_tender,'contract_id'); ?>
    </div>

    <div class="col-lg-6">
<?php echo $form->labelEx($l_tender, 'create_user_id'); ?>
        <?php echo $form->textField($l_tender, 'create_user_id'); ?>
        <?php //echo $form->error($l_tender,'create_user_id'); ?>
    </div>

    <div class="col-lg-6">
<?php echo $form->labelEx($l_tender, 'contract_duration'); ?>
        <?php echo $form->textField($l_tender, 'contract_duration'); ?>
        <?php //echo $form->error($l_tender,'contract_duration'); ?>
    </div>

    <div class="col-lg-6">
<?php //echo $form->labelEx($model,'supplier_ids');  ?>
        <?php //echo $form->textField($model,'supplier_ids',array('size'=>60,'maxlength'=>64));  ?>
        <?php // echo $form->error($model,'supplier_ids'); ?>

        <?php
        if ($l_tender->supplier_ids)
            $l_tender->supplier_ids = explode(",", $model->supplier_ids);
        $ab = CHtml::listData(Users::model()->findAllByAttributes(array('role_id' => 2)), 'id', 'fullname');
//echo $form->checkBoxListRow($l_tender, 'supplier_ids', CHtml::listData(Users::model()->findAllByAttributes(array('role_id' => 2)), 'id', 'fullname')); 
        echo $form->dropDownList($l_tender, 'supplier_ids', $ab, array('prompt' => 'Select', 'multiple' => true, 'selected' => 'selected'));
        ?>

    </div>

    <div class="col-lg-6">
        <?php echo $form->labelEx($l_tender, 'bespoke_duration'); ?>
        <?php echo $form->textField($l_tender, 'bespoke_duration'); ?>
        <?php echo $form->error($l_tender, 'bespoke_duration'); ?>
    </div>

    <div class="col-lg-6">
        <?php echo $form->labelEx($l_tender, 'commission_discount'); ?>
        <?php echo $form->textField($l_tender, 'commission_discount'); ?>
        <?php echo $form->error($l_tender, 'commission_discount'); ?>
    </div>

    <div class="col-lg-6">
        <?php //echo $form->labelEx($model,'tender_return_date');  ?>
        <?php //echo $form->textField($model,'tender_return_date'); ?>
        <?php //echo $form->error($model,'tender_return_date'); ?>
        <?php echo $form->label($l_tender, 'tender_return_date'); ?>
        <?php
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model' => $l_tender,
            'attribute' => 'tender_return_date',
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                'changeMonth' => true,
                'changeYear' => true,
            ),
        ));
        ?>

        <?php echo $form->error($l_tender, 'tender_return_date'); ?>
    </div>

    <div class="col-lg-6">
        <?php echo $form->labelEx($l_tender, 'business_name'); ?>
        <?php echo $form->textField($l_tender, 'business_name', array('size' => 60, 'maxlength' => 100)); ?>
        <?php echo $form->error($l_tender, 'business_name'); ?>
    </div>

    <div class="col-lg-6">
        <?php echo $form->labelEx($l_tender, 'com_reg'); ?>
        <?php echo $form->textField($l_tender, 'com_reg', array('size' => 50, 'maxlength' => 50)); ?>
        <?php echo $form->error($l_tender, 'com_reg'); ?>
    </div>

    <div class="col-lg-6">
        <?php echo $form->labelEx($l_tender, 'commodity_type'); ?>
        <?php
        echo $form->radioButton($l_tender, 'commodity_type', array('value' => 'Electric')) . 'Electric';
        ?>
        <?php
        echo $form->radioButton($l_tender, 'commodity_type', array('value' => 'Gas')) . 'Gas';
        ?>

    </div>

    <div class="col-lg-6">
        <?php echo $form->labelEx($l_tender, 'site_no'); ?>
        <?php echo $form->textField($l_tender, 'site_no'); ?>
        <?php echo $form->error($l_tender, 'site_no'); ?>
    </div>

   
    <div class="col-lg-6">
        <?php echo $form->labelEx($l_tender, 'uplift'); ?>
        <?php echo $form->textField($l_tender, 'uplift'); ?>
        <?php echo $form->error($l_tender, 'uplift'); ?>
    </div>

    <div class="col-lg-6">
        <?php echo $form->labelEx($l_tender, 'term_length'); ?>
          <?php
        echo $form->radioButton($l_tender, 'term_length', array('value' => '12')) . '12';
        ?>
        <?php
        echo $form->radioButton($l_tender, 'term_length', array('value' => '24')) . '24';
        ?>
         <?php
        echo $form->radioButton($l_tender, 'term_length', array('value' => '36')) . '36';
        ?>
         <?php echo $form->textField($l_tender, 'term_length', array('size' => 60, 'maxlength' => 100, 'placeholder' => 'others')); ?>
        <?php //echo $form->textField($l_tender, 'term_length', array('size' => 60, 'maxlength' => 100)); ?>
        <?php echo $form->error($l_tender, 'term_length'); ?>
    </div>

    <div class="col-lg-6">
        <?php echo $form->labelEx($l_tender, 'fixed_meter'); ?>
        <?php echo $form->textField($l_tender, 'fixed_meter', array('size' => 60, 'maxlength' => 100)); ?>
        <?php echo $form->error($l_tender, 'fixed_meter'); ?>
    </div>

    <div class="col-lg-6">
        <?php echo $form->labelEx($l_tender, 'contract_type'); ?> 
      
        <?php //echo $form->textField($l_tender, 'contract_type', array('size' => 50, 'maxlength' => 50)); ?>
           <?php
        echo $form->radioButton($l_tender, 'contract_type', array('value' => 'Fully Fixed')) . 'Fully Fixed';
        ?>
        <?php
        echo $form->radioButton($l_tender, 'contract_type', array('value' => 'Flexible Offerings')) . 'Flexible Offerings';
        ?>
         <?php
        echo $form->radioButton($l_tender, 'contract_type', array('value' => 'Part Fixed')) . 'Part Fixed';
        ?>
        <?php echo $form->error($l_tender, 'contract_type'); ?>
    </div>

    <div class="col-lg-6">
        <?php echo $form->labelEx($l_tender, 'payment'); ?>
        <?php //echo $form->textField($l_tender, 'payment', array('size' => 50, 'maxlength' => 50)); ?>
         <?php
        echo $form->radioButton($l_tender, 'payment', array('value' => 'DD')) . 'DD';
        ?>
         <?php
        echo $form->radioButton($l_tender, 'payment', array('value' => 'Cash/Cheque')) . 'Cash/Cheque';
        ?>
        <?php
        echo $form->radioButton($l_tender, 'payment', array('value' => 'BACs')) . 'BACs';
        ?>
        <?php echo $form->error($l_tender, 'payment'); ?>
    </div>

    <div class="col-lg-6">
        <?php echo $form->labelEx($l_tender, 'energy'); ?>
         <?php
        echo $form->radioButton($l_tender, 'energy', array('value' => 'Green')) . 'Green';
        ?>
         <?php
        echo $form->radioButton($l_tender, 'energy', array('value' => 'Brown')) . 'Brown';
        ?>
        <?php
        echo $form->radioButton($l_tender, 'energy', array('value' => 'Both')) . 'Both';
        ?>
        <?php //echo $form->textField($l_tender, 'energy', array('size' => 50, 'maxlength' => 50)); ?>
        <?php echo $form->error($l_tender, 'energy'); ?>
    </div>

    <div class="col-lg-6">
        <?php echo $form->labelEx($l_tender, 'supplier_pref'); ?>
        <?php echo $form->textField($l_tender, 'supplier_pref', array('size' => 20, 'maxlength' => 20)); ?>
        <?php echo $form->error($l_tender, 'supplier_pref'); ?>
    </div>

    <div class="col-lg-6">
        <?php echo $form->labelEx($l_tender, 'comments'); ?>
        <?php echo $form->textArea($l_tender, 'comments', array('rows' => 6, 'cols' => 50)); ?>
        <?php echo $form->error($l_tender, 'comments'); ?>
    </div>

    <div class="col-lg-6">
        <?php echo $form->labelEx($l_tender, 'created'); ?>
        <?php echo $form->textField($l_tender, 'created'); ?>
        <?php echo $form->error($l_tender, 'created'); ?>
    </div>

    <!--	<div class="row buttons">
    <?php echo CHtml::submitButton($l_tender->isNewRecord ? 'Create' : 'Save'); ?>
            </div>-->
    <div class="form-actions col-lg-12">
        <br>
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type' => 'primary',
            'label' => $l_tender->isNewRecord ? 'Create' : 'Save',
        ));
        ?>

    </div>

    <?php $this->endWidget(); ?>

</div>


</div>


</div>
<div class="col-md-12" id="customer_tender" ></div>

<script>
    function getUtilities(id) {
        var url = '<?php echo Yii::app()->createAbsoluteUrl("admin/product/getUtilities/id") ?>/' + id;
        $("#CustomerContract_utility").load(url);
        //gasParms();
    }
</script>


<script>
    var input = document.getElementById('CustomerSite_postcode');
    input.onkeyup = function () {
        this.value = this.value.toUpperCase();
    }
</script>

<script>
    function fillAddress1() {
        // alert('chzbCMbnxm');
        if ($('#BusinessAddress_is_site_address').is(":checked")) {
            $('#BusinessAddress_home_postcode').val($('#Company_postcode').val());
            $('#BusinessAddress_home_street1').val($('#Company_address').val());
            $('#BusinessAddress_home_street2').val($('#Company_address').val());

        } else {
            $('#BusinessAddress_home_postcode').val('');
            $('#BusinessAddress_home_street1').val('');
            $('#BusinessAddress_home_street2').val('');
        }
    }
</script>
<style>
    .container-fluid nav#top_navigation {
        display: none;
    }
    a.btn.btn-success {
        display: none;
    }
    label.required {
        float: left;
        /* width: 100%; */
    }
    .create button.btn.btn-primary {
    float: right;
    margin-right: 20px;
}
</style>