<section id="manage_productss">
    <div class="container">
        <div class="row">
            <div class="card">

                <div class="card-header">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="card-title">
                        <strong>Manage Customer</strong>
                    </div>

               

                    <?php
                    if (Yii::app()->user->isAdmin || Yii::app()->user->isAgency) {
                        echo CHtml::link('Create Customers', array('/admin/agencyCustomer/create'), array("class" => 'btn btn-success'));
                        echo CHtml::link('Manage Sites', array('/admin/customerSite/admin'), array("class" => 'btn btn-success'));
                        echo CHtml::link('Manage Meters', array('/admin/customerSiteMeter/admin'), array("class" => 'btn btn-success'));
                        echo CHtml::link('Manage Contracts', array('/admin/customerContract/admin'), array("class" => 'btn btn-success'));
                        echo CHtml::link('Manage Tenders', array('/admin/customerTender/admin'), array("class" => 'btn btn-success'));
                        echo CHtml::link('Manage Quotes', array('/admin/customerQuote/admin'), array("class" => 'btn btn-success'));
                    }
                    ?>

                </div>
                <div class="card-body">
                    <div class="container-fluid">
                        <div class="col-md-5">
                            <form method="post" id="lead-contacts-form11" name="sfdsdf" action="<?php echo Yii::app()->createUrl('/admin/leadContacts/admin') ?>">
                                <div class="col-lg-9">
                                    <input type="text" required="required" name="LeadContacts[business]" placeholder="Search By Business">
                                </div>
                                <div class="col-lg-3">
                                    <button type="submit" class="btn btn-success">Submit </button>

                                </div>
                            </form>
                        </div>
                    </div>
                    <?php
                    $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => 'agency-customer-grid',
                        'dataProvider' => $model->search(),
                        'filter' => $model,
                        'columns' => array(
                            //  'id',
                            // 'user_id',
                            'brocker_id' => array(
                                'name' => 'brocker_id',
                                'filter' => false,
                                'value' => '$data->broker->username'
                            ),
                            //  'create_user_id',
                            //'customer_name',
                            'customer_name' => array(
                                'name' => 'customer_name',
                                'type' => 'html',
                                'value' => 'CHtml::link($data->customer_name,array("/admin/agencyCustomer/viewAll","id"=>$data->id))'
                            ),
                            'reg_address1',
                            'reg_address2',
                            // 'reg_address3',
                            //  'reg_address4',
                            'postcode',
                            'company_reg_no',
                            //'company_profile_pic',
                            //'letter_authority',
                            'authority_start_date',
                            'authority_end_date',
                            'payment_terms',
                            'billing_req',
                            //  'active',
                            //   'created',
                            /*  */
                            array(
                                'class' => 'bootstrap.widgets.TbButtonColumn',
                                'template' => '{update}{view}{new_site}{new_quote}',
                                'header' => 'Actions',
                                'buttons' => array(
                                    'new_site' => array(
                                        'label' => '<button class="btn btn-primary">Create Site</button>',
                                        'url' => 'Yii::app()->createAbsoluteUrl("admin/customerSite/create", array("cid"=>$data->id))',
                                        'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Create Site'),
                                        'imageUrl' => false,
                                    ),
                                    'new_quote' => array(
                                        'label' => '<button class="btn btn-primary">Input quoted price</button>',
                                        'url' => 'Yii::app()->createAbsoluteUrl("admin/customerQuote/create", array("cid"=>$data->id))',
                                        'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Create Quote'),
                                        'imageUrl' => false,
                                        'visible' => 'Yii::app()->user->isAdmin'
                                    ),
                                ),
                            ),
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
