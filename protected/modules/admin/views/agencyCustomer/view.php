<section id="manage_productss">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="card-title">
                        <strong>View Customer by Username: <?php echo $model->user->username; ?></strong>
                    </div>

                    <?php
                    if (Yii::app()->user->isAdmin || Yii::app()->user->isAgency) {
                        echo CHtml::link('Manage Customers', array('/admin/agencyCustomer/admin'), array("class" => 'btn btn-success'));
                        echo CHtml::link('Manage Sites for ' . $model->customer_name, array('/admin/customerSite/admin', 'cid' => $model->id), array("class" => 'btn btn-success'));
                        echo CHtml::link('Add Site', array('/admin/customerSite/create', 'cid' => $model->id), array("class" => 'btn btn-success'));
                    }
                    ?>

                </div>
                <div class="card-body">


                    <?php
                    $this->widget('bootstrap.widgets.TbDetailView', array(
                        'data' => $model,
                        'attributes' => array(
                            // 'id',
                            // 'user_id',
                            'brocker_id' => array(
                                'name' => 'brocker_id',
                                'value' => $model->broker->username
                            ),
                            //  'create_user_id',
                            'customer_name',
                            'reg_address1',
                            'reg_address2',
                            //  'reg_address3',
                            //  'reg_address4',
                            'postcode',
                            'company_reg_no',
                            //  'company_profile_pic',
                            // 'letter_authority',
                            'authority_start_date',
                            'authority_end_date',
                            'payment_terms',
                            'billing_req',
                        // 'active',
                        //   'created',
                        ),
                    ));
                    ?>

                    <div id="customer_sites">

                    </div>
                    <div id="customer_meters">

                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<script>
//    var site_url = '<?php //echo Yii::app()->createAbsoluteUrl("")   ?>';
//      $("#customer_sites").load(site_url);
    // $("#customer_meters").load(meter_url);

</script>