<?php
$this->breadcrumbs=array(
	'Agency Customers',
);

$this->menu=array(
	array('label'=>'Create AgencyCustomer','url'=>array('create')),
	array('label'=>'Manage AgencyCustomer','url'=>array('admin')),
);
?>

<h1>Agency Customers</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
