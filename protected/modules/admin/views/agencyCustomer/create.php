<section id="manage_productss">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="card-title">
                        <strong>Create Customer</strong>
                    </div>

                    <?php
                    if (Yii::app()->user->isAdmin  || Yii::app()->user->isAgency ) {
                       // echo CHtml::link('New Customer', array('/admin/agencyCustomer/create'), array("class" => 'btn btn-success'));
                         echo CHtml::link('Manage Customers', array('/admin/agencyCustomer/admin'), array("class" => 'btn btn-success'));
                        echo CHtml::link('Manage Sites', array('/admin/customerSite/admin'), array("class" => 'btn btn-success'));
                        echo CHtml::link('Manage Meters', array('/admin/customerSiteMeter/admin'), array("class" => 'btn btn-success'));
                        echo CHtml::link('Manage Contracts', array('/admin/customerContract/admin'), array("class" => 'btn btn-success'));
                        echo CHtml::link('Manage Tenders', array('/admin/customerTender/admin'), array("class" => 'btn btn-success'));
                    }
                    ?>

                </div>
                <div class="card-body">
                    <?php echo $this->renderPartial('_form', array('model' => $model, 'user' => $user)); ?>
                </div>
            </div>
        </div>
    </div>
</section>


<script>
    $(document).ready(function(){
       // alert('saassdasds');
    $('#AgencyCustomer_postcode').keyup(function(){
    this.value = this.value.toUpperCase();
});
    });
    
    </script>