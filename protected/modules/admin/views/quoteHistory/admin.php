<section id="create_product">
    <div class="container-fluid">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <strong>Quote History</strong>
                    </div>
                    <?php
                    // if (Yii::app()->user->isAdmin) {
                    //echo CHtml::link('Print Invoices', array('/admin/quote/pdf'), array("class" => 'btn btn-success', 'id' => 'invoices'));
                    //  }
                    ?>
                </div>
                <div class="card-body">
                    <?php
                    $criteria = new CDbCriteria;
                    $criteria->addInCondition('role_id', array(1, 3, 4));
                    $users = Users::model()->findAll($criteria);
                    $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => 'quote-history-grid',
                        'dataProvider' => $model->search(),
                        'afterAjaxUpdate' => 'function(id,options){ paginationControls(); }',
                        'filter' => $model,
                        'columns' => array(
                            //'id',
                            'create_user_id' => array(
                                'name' => 'create_user_id',
                                'header' => 'Agent/Agency',
                                'filter' => CHtml::listData($users, 'id', 'username'),
                                'value' => '$data->user->username',
                            ),
                            'assigned_to' => array(
                                'name' => 'assigned_to',
                                'header' => 'Supplier',
                                'filter' => CHtml::listData(Users::model()->findAllByAttributes(array('role_id' => 2)), 'id', 'fullname'),
                                'value' => '$data->supplier->username',
                            ),
                            'product' => array(
                                'name' => 'product',
                                'filter' => false
                            ),
                            'region' => array(
                                'name' => 'region',
                                'filter' => false
                            ),
                            'contract_length' => array(
                                'name' => 'contract_length',
                                'filter' => false
                            ),
                            //'start_date',
                            'payment_type' => array(
                                'name' => 'payment_type',
                                'filter' => false
                            ),
                            'standing_charge' => array(
                                'name' => 'standing_charge',
                                'filter' => false
                            ),
                            'unit_charge' => array(
                                'name' => 'unit_charge',
                                'filter' => false
                            ),
                            'annual_cost' => array(
                                'name' => 'annual_cost',
                                'filter' => false,
                            ),
                            'created' => array(
                                'name' => 'created',
                                'filter' => false,
                            ),
                        /**/
//		array(
//			'class'=>'bootstrap.widgets.TbButtonColumn',
//		),
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>