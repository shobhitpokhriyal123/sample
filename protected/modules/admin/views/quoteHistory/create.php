<?php
$this->breadcrumbs=array(
	'Quote Histories'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List QuoteHistory','url'=>array('index')),
	array('label'=>'Manage QuoteHistory','url'=>array('admin')),
);
?>

<h1>Create QuoteHistory</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>