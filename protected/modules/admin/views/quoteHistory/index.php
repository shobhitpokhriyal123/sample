<?php
$this->breadcrumbs=array(
	'Quote Histories',
);

$this->menu=array(
	array('label'=>'Create QuoteHistory','url'=>array('create')),
	array('label'=>'Manage QuoteHistory','url'=>array('admin')),
);
?>

<h1>Quote Histories</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
