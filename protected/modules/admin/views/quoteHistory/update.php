<?php
$this->breadcrumbs=array(
	'Quote Histories'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List QuoteHistory','url'=>array('index')),
	array('label'=>'Create QuoteHistory','url'=>array('create')),
	array('label'=>'View QuoteHistory','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage QuoteHistory','url'=>array('admin')),
);
?>

<h1>Update QuoteHistory <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>