<?php
$this->breadcrumbs=array(
	'Quote Histories'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List QuoteHistory','url'=>array('index')),
	array('label'=>'Create QuoteHistory','url'=>array('create')),
	array('label'=>'Update QuoteHistory','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete QuoteHistory','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage QuoteHistory','url'=>array('admin')),
);
?>

<h1>View QuoteHistory #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'create_user_id',
		'assigned_to',
		'price_id',
		'product',
		'region',
		'contract_length',
		'start_date',
		'payment_type',
		'standing_charge',
		'unit_charge',
		'annual_cost',
		'created',
	),
)); ?>
