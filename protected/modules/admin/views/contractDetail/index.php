<?php
$this->breadcrumbs=array(
	'Contract Details',
);

$this->menu=array(
	array('label'=>'Create ContractDetail','url'=>array('create')),
	array('label'=>'Manage ContractDetail','url'=>array('admin')),
);
?>

<h1>Contract Details</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
