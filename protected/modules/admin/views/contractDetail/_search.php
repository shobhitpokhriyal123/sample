<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'create_user_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'agency_id',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'agency_name',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'mpan_topline',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'mpan_core',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'eac',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'current_supplier',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'new_supplier',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'smart_meter',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'date_time',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'renewal',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'payment',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'payment_type',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'contract_period',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'estimate_date',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'contract_date',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'live_date',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'billing_period',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'status',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
