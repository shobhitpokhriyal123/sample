
<section id="manage_supliers">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <strong>Contract</strong>
                    </div>
                    <?php
//                    if (Yii::app()->user->isAdmin) {
//                        echo CHtml::link('New Agency', array('/admin/users/newAgency'), array("class" => 'btn btn-success'));
//                    }
                    ?>
                </div>
                <div class="card-body .table-responsive">

                    <?php
//                    $this->breadcrumbs = array(
//                        'Contract Details' => array('index'),
//                        $model->id,
//                    );
//
//                    $this->menu = array(
//                        array('label' => 'List ContractDetail', 'url' => array('index')),
//                        array('label' => 'Create ContractDetail', 'url' => array('create')),
//                        array('label' => 'Update ContractDetail', 'url' => array('update', 'id' => $model->id)),
//                        array('label' => 'Delete ContractDetail', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Are you sure you want to delete this item?')),
//                        array('label' => 'Manage ContractDetail', 'url' => array('admin')),
//                    );
                    ?>





                    <div id="accordion" class="panel-group">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseZero">Contract Details</a>
                                </h4>
                            </div>
                            <div id="collapseZero" class="panel-collapse collapse  in">
                                <div class="panel-body">

                                    <h1>View ContractDetail #<?php echo $model->id; ?></h1>

                                    <?php
                                    // echo '<pre>'; print_r($model->agency1); die;
                                    $this->widget('bootstrap.widgets.TbDetailView', array(
                                        'data' => $model,
                                        'attributes' => array(
                                             'id',
                                            'create_user_id' => array(
                                                'name' => 'create_user_id',
                                                'value' => $model->user->username
                                            ),
                                             'agency_id',
                                            'agency_id' => array(
                                                'name' => 'agency_id',
                                                'value' => Agency::getAgencyId($model->agency_id)
                                            ),
                                            'agency_name',
                                         //   'mpan_topline',
                                           // 'mpan_core',
                                            'eac',
                                            'current_supplier' => array(
                                                'name' => 'current_supplier',
                                                'value' => $model->c_supplier->username,
                                            ),
                                            'new_supplier' => array(
                                                'name' => 'new_supplier',
                                                'value' => $model->n_supplier->username,
                                            ),
                                            'smart_meter'=> array(
                                                'name' => 'smart_meter',
                                               // 'value' => ($model->smart_meter == 1) ? "Yes" : "NO"
                                                'value' => Company::getTrueFalse($model->smart_meter),
                                            ),
                                            'date_time',
                                            'renewal'=>array(
                                                'name' => 'renewal',
                                                'value' => ($model->renewal == 1) ? "Renewal" : "Acquisition"
                                            ),
                                            // 'payment',
                                            'payment_type' => array(
                                                'name' => 'payment_type',
                                                'value' => ($model->payment_type == 0) ? "Cash": "Direct Debit"//Product::getDirectDebit($model->payment_type),
                                            ),
//                                          // 'contract_period',
                                              'contract_period' => array(
                                                'name' => 'contract_period',
                                                //'value' => CustomerTender::getContractDuration($model->contract_period),
                                            ),
//                                            
                                            
                                            'estimate_date',
                                            'contract_date',
                                            'live_date',
                                            'billing_period'=> array(
                                                'name' => 'billing_period',
                                                   'value' => ContractDetail::getBillingPeriod($model->billing_period),
                                            ),
                                            'status'=> array(
                                                'name' => 'status',
                                                'value' => ContractStatus::getContractStatus($model->status),
                                            ),
                                        ),
                                    ));
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Company Details</a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse">
                                <div class="panel-body">
<?php $this->renderPartial('application.modules.admin.views.company.view', array('model' => ($model->company) ? $model->company : new Company), null, true); ?>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapsex">Customer Details</a>
                                </h4>
                            </div>
                            <div id="collapsex" class="panel-collapse collapse">
                                <div class="panel-body">

<?php $this->renderPartial('application.modules.admin.views.partnerAddress.view', array('model' => ($model->customer_address) ? $model->customer_address : new PartnerAddress), null, true); ?>


                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Business Address</a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse">
                                <div class="panel-body">
<?php $this->renderPartial('application.modules.admin.views.businessAddress.view', array('model' => ($model->customer_address ) ? $model->customer_address : new PartnerAddress), null, true); ?>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Bank Details</a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse">
                                <div class="panel-body">
<?php $this->renderPartial('application.modules.admin.views.bank.view', array('model' => ($model->bank) ? $model->bank : new Bank()), null, true); ?>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapsesix">Aditional Docs</a>
                                </h4>
                            </div>
                            <div id="collapsesix" class="panel-collapse collapse">
                                <div class="panel-body">
<?php $this->renderPartial('application.modules.admin.views.contractFiles.view', array('model' => ($model->contract_files) ? $model->contract_files : new ContractFiles()), null, true); ?>
                                </div>
                            </div>
                        </div>
                                    <?php if (Yii::app()->user->getIsAdmin() && $model->id) { ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">Utility Cloud Validation</a>
                                    </h4>
                                </div>
                                <div id="collapseFour" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="col-md-5">
    <?php $this->renderPartial('application.modules.admin.views.validation._form', array('model' => ($model->validation) ? $model->validation : new Validation, 'modelid' => $model->id), null, true); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
<?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<style>
    .panel-default > .panel-heading {
        /*        background-color: #000000;
                border-color: #ddd;
                color: #fff;*/
    }
</style>
