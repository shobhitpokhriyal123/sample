<?php
$this->breadcrumbs=array(
	'Contract Details'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ContractDetail','url'=>array('index')),
	array('label'=>'Manage ContractDetail','url'=>array('admin')),
);
?>

<h1>Create ContractDetail</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>