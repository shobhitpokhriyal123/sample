<section id="manage_products">
    <div class="container-fluid">
        <div class="row">
            <div class="card">

                <div class="card-header">


                    <div class="card-title">
                        <strong>Manage Contract Details</strong>
                    </div>
                </div>
                <div class="card-body">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <?php
                    //  echo '<pre>'; print_r($model);die;
                    $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => 'contract-detail-grid',
                        'afterAjaxUpdate' => 'function(id,options){ paginationControls(); }',
                        'dataProvider' => $model->search(),
                        'filter' => $model,
                        'columns' => array(
                            //'id',
                            'agency_id' => array(
                                'name' => 'agency_id',
                                'filter' => false,
                                'value' => 'ContractDetail::getAgencyId($data->agency_id)'
                            ),
                            'agency_name' => array(
                                'header' => 'Agency Name',
                                'filter' => false,
                                'value' => '$data->agency_name'
                            ),
//                            'agent_name' => array(
//                                'name' => 'agent_name',
//                                'header' => 'Agent Name',
//                                'filter' => false,
//                            //  'value' => '($data->company) ? $data->company->company : "N/A"'
//                            ),
                            'company_name' => array(
                                'header' => 'Business Name',
                                'filter' => false,
                                'value' => '($data->company) ? $data->company->company : "N/A"'
                            ),
                            'customer' => array(
                                'filter' => false,
                                'header' => 'Customer Name',
                                'value' => '($data->customer_address) ? $data->customer_address->customer_name : "N/A"'
                            ),
                            'postcode' => array(
                                'filter' => false,
                                'name' => 'postcode',
                                'value' => '$data->business_address->home_postcode'
                            // 'value' => 'N/A',
                            ),
                            'mpan_topline' => array(
                                'filter' => false,
                                'name' => 'mpan_topline',
                                'value' => '$data->mpan_topline',
                                'header'=>'Meter-MPAN/MPR',
                            ),
//                            'meter' => array(
//                                'filter' => false,
//                                'name' => 'meter',
//                                'value' => '$data->price->meter_type',
//                            ),
//                            
                            'utility' => array(
                                'filter' => false,
                                'name' => 'utility',
                                'value' => '($data->is_corporate == 1 ) ? "Corporate" : Product::getUtilites($data->price->productName->utility)',
                            ),
                            'corporate_sme' => array(
                                'filter' => false,
                                'header' => 'corporate/SME',
                                'name' => 'corporate_sme',
                                'value' => '($data->is_corporate == 1 ) ? "Corporate" : Product::getCorporateSME($data->price->productName->corporate_sme)',
                            ),
                            'start_date' => array(
                                'filter' => false,
                                'header' => 'Start Date',
                                'name' => 'start_date',
                                //'value' => '$data->price->productName->valid_from',
                            ),
                            'end_date' => array(
                                'filter' => false,
                                'header' => 'End Date',
                                'name' => 'end_date',
                               // 'value' => '$data->price->productName->valid_till',
                            ),
//                            'contract_date' => array(
//                                'filter' => false,
//                                'value' => '$data->contract_date',
//                                'header' => 'Live Date'
//                                
//                            ),
                            'contract_date' => array(
                                'header' => 'Live Status',
                                'filter' => false,
                                'type' => 'raw',
                                'value' => '(Yii::app()->user->isAdmin)?CHtml::link($data->contract_date, array("/admin/contractDetail/updateDate&id=$data->id"), array("class"=>"sdfasdf")) : $data->contract_date',
                            ),
                            'eac' => array(
                                'filter' => false,
                                'header' => 'Consumption',
                                'name' => 'eac',
                                'value' => '$data->eac',
                            ),
                            'new_supplier' => array(
                                'name' => 'new_supplier',
                                'filter' => false,
                                'value' => '($data->n_supplier) ? $data->n_supplier->username : "N/A"'
                            ),
                            'status' => array(
                                'header' => 'Contract Status',
                                'filter' => false,
                                'type' => 'raw',
                                'value' => '(Yii::app()->user->isAdmin)?CHtml::link(ContractStatus::getContractStatus($data->status), array("/admin/contractDetail/update&id=$data->id"), array("class"=>"sdfasdf")) : ContractStatus::getContractStatus($data->status)',
                            ),
                            array(
                                'class' => 'bootstrap.widgets.TbButtonColumn',
                                'template' => '{view}{update}{delete}',
                                'buttons' => array(
                                    'update' => array(
                                        'label' => '<i class="fa fa-pencil"></i>',
                                        'url' => 'Yii::app()->createAbsoluteUrl("admin/agency/updateQuote", array("id"=>$data->id))',
                                        'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Update'),
                                        'imageUrl' => false,
                                        'visible' => 'Yii::app()->user->isAdmin'
                                    ),
                                    'delete' => array(
                                        'label' => '<i class="fa fa-trash"></i>',
                                        'url' => 'Yii::app()->createAbsoluteUrl("admin/agency/deleteQuote", array("id"=>$data->id))',
                                        'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Update'),
                                        'imageUrl' => false,
                                        'visible' => 'Yii::app()->user->isAdmin'
                                    ),
                                ),
                            ),
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<!--<div class="modal fade" id="myModal34344" role="dialog">
    <div class="modal-dialog" id="response1234">

         Modal content

    </div>
</div>-->
<script>
    $(document).ready(function () {
        $(".sdfasdf").click(function (e) {
            e.preventDefault();
            var url = $(this).attr("href");
            $("#myModal").modal();
            $("#modal-content-status").load(url);
        });
    });
</script>


<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" id="modal-content-status">

        </div>

    </div>
</div>