<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_user_id')); ?>:</b>
	<?php echo CHtml::encode($data->create_user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('agency_id')); ?>:</b>
	<?php echo CHtml::encode($data->agency_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('agency_name')); ?>:</b>
	<?php echo CHtml::encode($data->agency_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mpan_topline')); ?>:</b>
	<?php echo CHtml::encode($data->mpan_topline); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mpan_core')); ?>:</b>
	<?php echo CHtml::encode($data->mpan_core); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('eac')); ?>:</b>
	<?php echo CHtml::encode($data->eac); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('current_supplier')); ?>:</b>
	<?php echo CHtml::encode($data->current_supplier); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('new_supplier')); ?>:</b>
	<?php echo CHtml::encode($data->new_supplier); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('smart_meter')); ?>:</b>
	<?php echo CHtml::encode($data->smart_meter); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_time')); ?>:</b>
	<?php echo CHtml::encode($data->date_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('renewal')); ?>:</b>
	<?php echo CHtml::encode($data->renewal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('payment')); ?>:</b>
	<?php echo CHtml::encode($data->payment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('payment_type')); ?>:</b>
	<?php echo CHtml::encode($data->payment_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contract_period')); ?>:</b>
	<?php echo CHtml::encode($data->contract_period); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('estimate_date')); ?>:</b>
	<?php echo CHtml::encode($data->estimate_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contract_date')); ?>:</b>
	<?php echo CHtml::encode($data->contract_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('live_date')); ?>:</b>
	<?php echo CHtml::encode($data->live_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('billing_period')); ?>:</b>
	<?php echo CHtml::encode($data->billing_period); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	*/ ?>

</div>