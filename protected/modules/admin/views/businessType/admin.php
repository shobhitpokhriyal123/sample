<section id="manage_products_">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="card-title">
                        <strong>Manage Business Types</strong>
                    </div>

                    <?php
                    if (Yii::app()->user->isAdmin) {
                        echo CHtml::link('Create Business Type', array('/admin/businessType/create'), array("class" => 'btn btn-success'));
                    }
                    ?>

                </div>
                <div class="card-body">
                    <?php
                    $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => 'business-type-grid',
                        'dataProvider' => $model->search(),
                        'filter' => $model,
                        'columns' => array(
                            'id',
                            'title',
                            array(
                                'class' => 'bootstrap.widgets.TbButtonColumn',
                                'template' => '{view}{update}'
                            ),
                        ),
                    ));
                    ?>
                </div></div></div>
    </div>
</section>