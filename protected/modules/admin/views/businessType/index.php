<?php
$this->breadcrumbs=array(
	'Business Types',
);

$this->menu=array(
	array('label'=>'Create BusinessType','url'=>array('create')),
	array('label'=>'Manage BusinessType','url'=>array('admin')),
);
?>

<h1>Business Types</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
