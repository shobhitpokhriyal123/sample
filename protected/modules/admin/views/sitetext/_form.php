<?php
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'role-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    ));
?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php //echo $form->errorSummary($model); ?>
<div class="control-group">
    <?php echo $form->textFieldRow($model, 'title', array('class' => 'span5', 'maxlength' => 255)); ?>
</div>
<?php //echo $form->textFieldRow($model, 'role', array('class' => 'span5')); ?>
<?php //echo $form->textAreaRow($model, 'description', array('class' => 'span5')); ?>
<?php //echo $form->label($model, 'description', array("class"=>'span5')); ?>
<br>
<div class="control-group">
    <label for="Role_title" class="required">Description <span class="required">*</span></label>
    <?php
        $this->widget('application.extensions.ckeditor.CKEditor', array(
            'model' => $model,
            'attribute' => 'description',
            //x 'language' => 'ru',
            'editorTemplate' => 'basic',
        ));
    ?>
    <?php echo $form->error($model, 'description'); ?>
</div>
<br>
<div class="control-group">
    <?php //echo $form->dropdownListRow($model, 'active', $model->getStatus(), array('class' => 'form-control')); ?>
</div>
<?php //echo $form->textFieldRow($model, 'created', array('class' => 'span5'));     ?>
<br>
<div class="control-group">
    <div class="form-actions">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type' => 'primary',
            'label' => $model->isNewRecord ? 'Create' : 'Update',
            'htmlOptions' => array('class' => 'longBtn'),
        ));
        ?>
    </div>
</div>
<?php $this->endWidget(); ?>



