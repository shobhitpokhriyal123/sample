<section id="update_role">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <strong>Update Role <?php echo $model->title; ?></strong>
                    </div>
                    <?php
                        echo CHtml::link('Manage', array('/admin/sitetext/admin'), array("class" => 'btn btn-success'));
                        echo CHtml::link('Detail', array('/admin/sitetext/view/id/'.$model->id), array("class" => 'btn btn-success'));
                    ?>
                </div>
                <div class="card-body">
                    <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
                </div>
            </div>
        </div>
    </div>
</section>
