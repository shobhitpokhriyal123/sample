<section id="view_role">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <strong>View Customer Detail<?php //echo $model->rolename;    ?></strong>
                    </div>
                    <?php
                    //echo CHtml::link('Manage', array('/admin/role/admin'), array("class" => 'btn btn-success'));
                    // echo CHtml::link('Update', array('/admin/role/update/id/' . $model->id), array("class" => 'btn btn-success'));
                    ?>
                </div>
                <div class="card-body">
                    <?php
                    $this->widget('bootstrap.widgets.TbDetailView', array(
                        'data' => $site,
                        'attributes' => array(
                            'id',
                            'lead_id' => array(
                                'header' => 'Customer',
                                'name' => 'lead_id',
                                'value' => $site->lead->first_name
                            ),
                            //  'create_user_id',
                            'site_name',
                            'client_reference',
                            'external_reference',
                            'date_occupency',
                            'cost_code',
                            'change_tenancy',
                        //   'created',
                        ),
                    ));
                    ?>

                    <hr>
                    <div class="card-title">
                        <h4>Lead Gas Meter Details</h4>
                    </div>
                    <?php
                    $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => 'lead-gas-meter-grid',
                        'dataProvider' => $gasmodel->search(),
                        'afterAjaxUpdate' => 'function(id,options){ paginationControls(); }',
                        'filter' => $gasmodel,
                        'columns' => array(
                            //'id',
                            'site_id' => array(
                                'filter' => false,
                                'header' => 'Site',
                                'name' => 'site_id',
                                'value' => '$data->site->site_name'
                            ),
                            'current_supplier' => array(
                                'type' => 'raw',
                                'filter' => false, //CHtml::listData(Users::getAllSuppliers(), 'id', 'fullname'), 'type' => 'html',
                                'name' => 'current_supplier',
                                'value' => ''//'CHtml::image(Yii::app()->createAbsoluteUrl("admin/users/thumbnail/file/".$data->supplier->image))'
                            ),
                            'meter_name' => array(
                                'filter' => false,
                                'name' => 'meter_name'
                            ),
                            'mpr' => array(
                                'filter' => false,
                                'name' => 'mpr'
                            ),
                            'total_aq' => array(
                                'name' => 'total_aq',
                                'filter' => false,
                            ),
                            'contract_end_date' => array(
                                'name' => 'contract_end_date',
                                'filter' => false,
                            ),
                            'meter_status' => array(
                                'name' => 'meter_status',
                                'filter' => false,
                                'value' => 'LeadStatus::getStatus($data->meter_status)'
                            ),
                        //  'meter_pricing',
                        // 'created',
                        ),
                    ));
                    ?>

                    <hr>
                    <div class="card-title">
                        <h4>Lead Electric Meter Details</h4>
                    </div>

                    <?php
                    $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => 'lead-elc-meter-grid',
                        'dataProvider' => $elemodel->search(),
                        'afterAjaxUpdate' => 'function(id,options){ paginationControls(); }',
                        'filter' => $elemodel,
                        'columns' => array(
                            // 'id',
                            // 'create_user_id',
//        'lead_id' => array(
//              'filter' => false, //CHtml::listData(Users::getAllSuppliers(), 'id', 'fullname'), 'type' => 'html',
//           // 'header' => 'Lead',
//            'name' => 'lead_id',
//            'value'=> '$data->lead->first_name'
//        ),
                            'site_id' => array(
                                'filter' => false, //CHtml::listData(Users::getAllSuppliers(), 'id', 'fullname'), 'type' => 'html',
                                'header' => 'Site',
                                'name' => 'site_id',
                                'value' => '$data->site->site_name'
                            ),
                            'current_supplier' => array(
                                'type' => 'raw',
                                'filter' => false, //CHtml::listData(Users::getAllSuppliers(), 'id', 'fullname'), 'type' => 'html',
                                // 'header' => 'Supplier',
                                'name' => 'current_supplier',
                                //   'value' => '$data->productName->user->fullname'
                                'value' => ''//'CHtml::image(Yii::app()->createAbsoluteUrl("admin/users/thumbnail/file/".$data->supplier->image))'
                            ),
                            'meter_name' => array(
                                'filter' => false,
                                'name' => 'meter_name'
                            ),
                            'mpan' => array(
                                'filter' => false,
                                'name' => 'mpan'
                            ),
//        'pc'=>array(
//            'filter' => false, 
//            'name'=>'pc'
//        ),
//        'mtc'=>array(
//            'filter' => false, 
//            'name'=>'mtc'
//        ),
//        'llf',
//        'pes',
//        'mpan',
                            'total_eac' => array(
                                'filter' => false,
                                'name' => 'total_eac'
                            ),
                            'contract_end_date' => array(
                                'filter' => false,
                                'name' => 'contract_end_date'
                            ),
//        'msn',
//        'kva',
//        'voltage',
//        'ct_wc',
//        'mop',
                            'meter_status' => array(
                                'filter' => false,
                                'name' => 'meter_status',
                                'value' => 'LeadStatus::getStatus($data->meter_status)'
                            ),
                            'meter_pricing' => array(
                                'filter' => false,
                                'name' => 'meter_pricing'
                            ), /**/
                            //  'created',
//                            array(
//                                'class' => 'bootstrap.widgets.TbButtonColumn',
//                                'template' => '{update}{delete}',
//                                'header' => 'Actions',
//            'buttons' => array(
//                'update' => array(
//                    'label' => '<button class="btn btn-primary">Update</button>',
//                    'url' => 'Yii::app()->createAbsoluteUrl("admin/leadElcMeter/meterupdate", array("id"=>$data->id))',
//                    'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Update'),
//                    'imageUrl' => false,
//                    'click' => "function(e){
//                               e.preventDefault();
//                               var url = $(this).attr('href');
//                               $.ajax({
//                                 type: 'POST',
//                                 url: url,
//                                 success: function(res){
//                                 res = $.parseJSON(res);
//                              
//                                for (var property in res) {
//                                        if (res.hasOwnProperty(property)) { 
//                                        
//                                         $('#LeadElcMeter_'+property).val(res[property]);
//                                    }
//                                }
//                                    var current = $('#lead-elc-meter-form').attr('action');
//                                    $('#lead-elc-meter-form').attr('action',current+'&id='+res.id);
//                                 },
//                                });
//                                
//                           
//                            }",
//                ),
//            ),
                          //  ),
                        ),
                    ));
                    ?>

                </div>
            </div>
        </div>
    </div>
</section>