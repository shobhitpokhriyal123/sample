<section id="manage_products">
    <div class="container-fluid">
        <div class="row">
            <div class="card">

                <div class="card-header">


                    <div class="card-title">
                        <strong>Manage Customer Leads</strong>
                    </div>
                </div>
                <div class="card-body">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <?php
                    $buttonPlus = CHtml::ajaxLink('<i class="fa fa-icon-plus">Add</i>', $this->createUrl('newLead'), array(
                                'type' => 'POST',
                                'data' => array(
                                    'LeadContacts[agency_id]' => 'js:$("#LeadContacts_agency_id_").val()',
                                    'LeadContacts[agent_id]' => 'js:$("#LeadContacts_agent_id_").val()',
                                    'LeadContacts[position]' => 'js:$("#LeadContacts_position_").val()',
                                    'Users[email]' => 'js:$("#Users_email_").val()',
                                    'Users[fullname]' => 'js:$("#Users_fullname_").val()',
                                    'LeadContacts[landline]' => 'js:$("#LeadContacts_landline_").val()',
                                    'LeadContacts[mobile]' => 'js:$("#LeadContacts_mobile_").val()',
                                    'LeadContacts[status]' => 'js:$("#LeadContacts_status_").val()',
                                    
                                ),
                                'success' => 'function(html){  $.fn.yiiGridView.update("lead-contacts-grid");  gridLoaded = true;}'
                    ));

                    //echo '<pre>'; print_r($model); die;
                    $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => 'lead-contacts-grid',
                        'dataProvider' => $model->search(),
                        'afterAjaxUpdate' => 'function(id,options){ paginationControls(); }',
                        //'filter' => $model,
                        'columns' => array(
                            'agency_id' => array(
                                'name' => 'agency_id',
                                'header' => 'Agency Name',
                                'type' => 'raw',
                                'value' => '$data->broker->fullname',
                                'footer' => LeadContacts::getAgency(),
                            ),
                            'agent_id' => array(
                                'name' => 'agent_id',
                                'header' => 'Agent Name',
                                'value'=>'$data->agent->username',
                                'type' => 'raw',
                                'footer' => LeadContacts::getAgent(),
                            ),
                            'position' => array(
                                'name' => 'position',
                                'type' => 'raw',
                                'footer' => '<input id="LeadContacts_position_" class="span5 form-control" maxlength="255" name="LeadContacts[position]" type="text">',
                            ),
                            'email' => array(
                                'name' => 'email',
                                'type' => 'raw',
                                'value' => '$data->user->email',
                                'footer' => '<input  id="Users_email_" class="span5 form-control" maxlength="255" name="Users[email]" type="text">',
                            ),
                            'username' => array(
                                'name' => 'username',
                                'type' => 'raw',
                                'value' => '$data->user->username',
                                'footer' => '<input  id="Users_username_" class="span5 form-control" maxlength="255" name="Users[username]" type="text">',
                            ),
                            'password' => array(
                                'name' => 'password',
                                'type' => 'raw',
                                'value' => '$data->user->password_plain',
                                'footer' => '<input  id="Users_password_" class="span5 form-control" maxlength="255" name="Users[password]" type="text">',
                            ),
                            'fullname' => array(
                                'name' => 'fullname',
                                'type' => 'raw',
                                'value' => '$data->user->fullname',
                                'footer' => '<input  id="Users_fullname_" class="span5 form-control" maxlength="255" name="Users[fullname]" type="text">',
                            ),
                            'landline' => array(
                                'name' => 'landline',
                                'type' => 'raw',
                                'footer' => '<input id="LeadContacts_landline_" class="span5 form-control" name="LeadContacts[landline]" maxlength="255" type="text">',
                            ),
                            'mobile' => array(
                                'name' => 'mobile',
                                'type' => 'raw',
                                'footer' => '<input id="LeadContacts_mobile_" class="span5 form-control" name="LeadContacts[mobile]" maxlength="255" type="text">',
                            ),
                            'status' => array(
                                'header' => 'Status',
                                'filter' => false,
                                'type' => 'raw',
                                'value' => 'LeadStatus::getStatus($data->status)',
                                'footer' => LeadContacts::getStatusDropdown()
                            ),
                            array(
                                'class' => 'bootstrap.widgets.TbButtonColumn',
                                'footer' => $buttonPlus,
                                'template' => '{view}{update}{delete}',
                                'header' => 'Actions',
                                'buttons' => array(
                                    'update' => array(
                                        'label' => '<button class="btn btn-primary">Update</button>',
                                        'url' => 'Yii::app()->createAbsoluteUrl("admin/LeadContacts/leadupdate", array("id"=>$data->id))',
                                        'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Update'),
                                        'imageUrl' => false,
                                        'click' => "function(e){
                                           
                                            e.preventDefault();
                                            var url = $(this).attr('href');
                                           // alert(url);
                                            $.ajax({
                                              type: 'POST',
                                              url: url,
                                              success: function(res){
                                                
                                              res = $.parseJSON(res);
                                             var id = res.lead.id;
                                             var current = $('#lead-contacts-form').attr('action');
                                             $('#lead-contacts-form').attr('action',current+'&id='+ id);
                                             for (var property in res.lead) {
                                                     if ( res.lead.hasOwnProperty(property)) { 
                                                    // alert(res[property]);
                                                      $('#LeadContacts_'+property).val( res.lead[property]);
                                                 }
                                             }
                                              for (var property in res.user) {
                                                     if ( res.user.hasOwnProperty(property)) { 
                                                    // alert(res[property]);
                                                      $('#Users_'+property).val(res.user[property]);
                                                 }
                                             }
                                                 
                                                
                                              },
                                             });


                                         }",
                                    ),
                                ),
                            ),
                        ),
                            // 'rowHtmlOptionsExpression' => '[ "data-animalclass" => $data->animal_class, ]',
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>