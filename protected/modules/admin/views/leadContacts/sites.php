<section id="manage_products">
    <div class="container-fluid">
        <div class="row">
            <div class="card">

                <div class="card-header">


                    <div class="card-title">
                        <strong>Manage Customer Leads</strong>
                    </div>
                </div>
                <div class="card-body">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <?php
                    $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => 'lead-contacts-grid',
                        'dataProvider' => $sites->search(),
                        'afterAjaxUpdate' => 'function(id,options){ paginationControls(); }',
                        //  'filter' => $model,
                        'columns' => array(
                            'id',
                            array(
                                'class' => 'bootstrap.widgets.TbButtonColumn',
                                'template' => '{view}{update}{delete}',
                                'header' => 'Actions',
                               
                            ),
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>