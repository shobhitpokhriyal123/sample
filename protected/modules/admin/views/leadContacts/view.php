<section id="manage_products">
    <div class="container-fluid">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <strong>View Lead Details</strong>
                    </div>
                    <?php
                    if (Yii::app()->user->isAdmin) {
                        echo CHtml::link('Manage Lead', array('/admin/leadContacts/allLeads'), array("class" => 'btn btn-success right'));
                        //   echo CHtml::link('Send Input Quote', array('/admin/leadContacts/sendQuote'), array("class" => 'btn btn-success right', 'id' => 'send_quote'));
                    }
                    //
                    ?>
                </div>

                <div class="card-body"> 
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>

                    <?php
                    $this->widget('bootstrap.widgets.TbDetailView', array(
                        'data' => $model,
                        'attributes' => array(
                            'first_name' => array(
                                'name' => 'first_name',
                                'value' => $model->lead->first_name
                            ),
                            'business_name' => array(
                                'name' => 'business_name',
                                'value' => $model->business_name
                            ),
                            'position' => array(
                                'name' => 'position',
                                'value' => $model->lead->position
                            ),
                            'email' => array(
                                'name' => 'email',
                                'value' => $model->lead->user->email
                            ),
                            'landline' => array(
                                'name' => 'landline',
                                'value' => $model->lead->landline
                            ),
                            'mobile' => array(
                                'name' => 'mobile',
                                'value' => $model->lead->mobile
                            ),
                            'charity_no' => array(
                                'name' => 'charity_no',
                                'value' => $model->charity_no
                            ),
                            'trending_type' => array(
                                'name' => 'trending_type',
                                'value' => Agency::getTrendingType($model->trending_type)
                            ),
                            'credit_score' => array(
                                'name' => 'credit_score',
                                'value' => $model->credit_score
                            ),
                            'client_reference' => array(
                                'name' => 'client_reference',
                                'value' => $model->client_reference
                            ),
                            'partner_channel' => array(
                                'name' => 'partner_channel',
                                'value' => $model->partner_channel
                            ),
                            'campaign' => array(
                                'name' => 'campaign',
                                'value' => $model->campaign
                            ),
                            'lead_source' => array(
                                'name' => 'lead_source',
                                'value' => $model->lead_source
                            ),
                            'business_no' => array(
                                'name' => 'business_no',
                                'value' => $model->lead->leadaddress->business_no
                            ),
                            'street_address_1' => array(
                                'name' => 'street_address_1',
                                'value' => $model->lead->leadaddress->street_address_1
                            ),
                            'town' => array(
                                'name' => 'town',
                                'value' => $model->lead->leadaddress->town
                            ),
                            'partner_channel' => array(
                                'name' => 'partner_channel',
                                'value' => $model->lead->leadaddress->street_address_2
                            ),
                            'country' => array(
                                'name' => 'country',
                                'value' => $model->lead->leadaddress->country
                            ),
                            'billing_name' => array(
                                'name' => 'billing_name',
                                'value' => $model->lead->leadaddress->billing_name
                            ),
                            'billing_no' => array(
                                'name' => 'billing_no',
                                'value' => $model->lead->leadaddress->billing_no
                            ),
                            'street_address_11' => array(
                                'name' => 'street_address_11',
                                'value' => $model->lead->leadaddress->street_address_11
                            ),
                            'town1' => array(
                                'name' => 'town1',
                                'value' => $model->lead->leadaddress->town1
                            ),
                            'street_address_12' => array(
                                'name' => 'street_address_12',
                                'value' => $model->lead->leadaddress->street_address_12
                            ),
                            'country1' => array(
                                'name' => 'country1',
                                'value' => $model->lead->leadaddress->country1
                            ),
                            'partner_channel' => array(
                                'name' => 'partner_channel',
                                'value' => $model->partner_channel
                            ),
                        ),
                    ));
                    ?>
                </div>



                <div class="card-body">

                    <div class="card-header">
                        <div class="card-title">
                            <strong>Site Details</strong>
                        </div>
                    </div>
                    <?php
                    $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => 'lead-site-grid',
                        'dataProvider' => $leadsites->searchByLead($model->lead_id),
                        'afterAjaxUpdate' => 'function(id,options){ paginationControls(); }',
                        //'filter' => $model,
                        'columns' => array(
                            //'id',
                            'lead_id' => array(
                                'name' => 'lead_id',
                                'header' => 'Business Contact',
                                'value' => '$data->lead->first_name',
                            ),
                            'site_name',
                            'full_name' => array(
                                'name' => 'full_name',
                                'filter' => false,
                                'value' => '($data->contact) ? $data->contact->full_name : "N/A"'
                            //  'footer' => '<input id="LeadSiteContactDetails_full_name_" class="form-control" size="60" maxlength="255" name="LeadSiteContactDetails[full_name]" type="text">'
                            ),
                            'position' => array(
                                'name' => 'position',
                                'filter' => false,
                                'value' => '($data->contact) ? $data->contact->position : "N/A"'
                            //  'footer' => '<input id="LeadSiteContactDetails_position_" class="form-control" size="60" maxlength="255" name="LeadSiteContactDetails[position]" type="text">',
                            ),
                            'email' => array(
                                'name' => 'email',
                                'filter' => false,
                                'value' => '($data->contact) ? $data->contact->email : "N/A"'
                            // 'footer' => '<input id="LeadSiteContactDetails_email_" class="form-control" size="60" maxlength="255" name="LeadSiteContactDetails[email]" type="text">'
                            ),
                            'landline' => array(
                                'name' => 'landline',
                                'filter' => false,
                                'value' => '($data->contact) ? $data->contact->landline : "N/A"'
                            //  'footer' => '<input id="LeadSiteContactDetails_landline_" class="form-control" size="11" maxlength="11" name="LeadSiteContactDetails[landline]" type="text">'
                            ),
                            'mobile' => array(
                                'name' => 'mobile',
                                'filter' => false,
                                'value' => '($data->contact) ? $data->contact->mobile : "N/A"'
                            //  'footer' => '<input id="LeadSiteContactDetails_mobile_" class="form-control" size="11" maxlength="11" name="LeadSiteContactDetails[mobile]" type="text">'
                            ),
                            'site_street2' => array(
                                'name' => 'site_street2',
                                'filter' => false,
                                'value' => '($data->contact) ? $data->address->site_street2 : "N/A"'
                            //  'footer' => '<input id="LeadSiteContactDetails_mobile_" class="form-control" size="11" maxlength="11" name="LeadSiteContactDetails[mobile]" type="text">'
                            ),
                            'site_street1' => array(
                                'name' => 'site_street1',
                                'filter' => false,
                                'value' => '($data->contact) ? $data->address->site_street1 : "N/A"'
                            //  'footer' => '<input id="LeadSiteContactDetails_mobile_" class="form-control" size="11" maxlength="11" name="LeadSiteContactDetails[mobile]" type="text">'
                            ),
                            'site_address_no' => array(
                                'name' => 'site_address_no',
                                'filter' => false,
                                'value' => '($data->address) ? $data->address->site_address_no : "N/A"'
                            //  'footer' => '<input id="LeadSiteContactDetails_mobile_" class="form-control" size="11" maxlength="11" name="LeadSiteContactDetails[mobile]" type="text">'
                            ),
                            'site_address_name' => array(
                                'name' => 'site_address_name',
                                'filter' => false,
                                'value' => '($data->address) ? $data->address->site_address_name : "N/A"'
                            //  'footer' => '<input id="LeadSiteContactDetails_mobile_" class="form-control" size="11" maxlength="11" name="LeadSiteContactDetails[mobile]" type="text">'
                            ),
                            'site_town' => array(
                                'name' => 'site_town',
                                'filter' => false,
                                'value' => '($data->address) ? $data->address->site_town : "N/A"'
                            //  'footer' => '<input id="LeadSiteContactDetails_mobile_" class="form-control" size="11" maxlength="11" name="LeadSiteContactDetails[mobile]" type="text">'
                            ),
                            'site_country' => array(
                                'name' => 'site_country',
                                'filter' => false,
                                'value' => '($data->address) ? $data->address->site_country : "N/A"'
                            //  'footer' => '<input id="LeadSiteContactDetails_mobile_" class="form-control" size="11" maxlength="11" name="LeadSiteContactDetails[mobile]" type="text">'
                            ),
                            'site_post_code' => array(
                                'name' => 'site_post_code',
                                'filter' => false,
                                'value' => '($data->address) ? $data->address->site_post_code : "N/A"'
                            //  'footer' => '<input id="LeadSiteContactDetails_mobile_" class="form-control" size="11" maxlength="11" name="LeadSiteContactDetails[mobile]" type="text">'
                            ),
                            'street_address_2' => array(
                                'name' => 'street_address_2',
                                'filter' => false,
                                'value' => '($data->address) ? $data->address->street_address_2 : "N/A"'
                            //  'footer' => '<input id="LeadSiteContactDetails_mobile_" class="form-control" size="11" maxlength="11" name="LeadSiteContactDetails[mobile]" type="text">'
                            ),
                            'billing_no' => array(
                                'name' => 'billing_no',
                                'filter' => false,
                                'value' => '($data->address) ? $data->address->billing_no : "N/A"'
                            //  'footer' => '<input id="LeadSiteContactDetails_mobile_" class="form-control" size="11" maxlength="11" name="LeadSiteContactDetails[mobile]" type="text">'
                            ),
                            'billing_name' => array(
                                'name' => 'billing_name',
                                'filter' => false,
                                'value' => '($data->address) ? $data->address->billing_name : "N/A"'
                            //  'footer' => '<input id="LeadSiteContactDetails_mobile_" class="form-control" size="11" maxlength="11" name="LeadSiteContactDetails[mobile]" type="text">'
                            ),
                        ),
                    ));
                    ?>


                    <div class="card-header">
                        <div class="card-title">
                            <strong>Additional Details</strong>
                        </div>
                    </div>


                    <div class="col-md-12">
                        <?php
                        // if ($model)
                        echo $this->renderPartial('additional', array('model' => $additional, 'sites' => $sites));
                        ?>   
                    </div>



                </div>




                <div class="card-body">
                    <div class="card-header">
                        <div class="card-title">
                            <strong>Electric Meter</strong>
                        </div>
                    </div>

                    <?php
                    $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => 'lead-elc-meter-grid',
                        'dataProvider' => $electric->searchByLead($model->lead_id),
                        'afterAjaxUpdate' => 'function(id,options){ paginationControls(); }',
                        // 'filter' => $model,
                        'columns' => array(
                            'site_id' => array(
                                'filter' => false, //CHtml::listData(Users::getAllSuppliers(), 'id', 'fullname'), 'type' => 'html',
                                'header' => 'Site',
                                'name' => 'site_id',
                                'value' => '$data->site->site_name',
                            //  'footer' => LeadElcMeter::getSiteDropdown()
                            ),
                            'current_supplier' => array(
                                'type' => 'raw',
                                'filter' => false, //CHtml::listData(Users::getAllSuppliers(), 'id', 'fullname'), 'type' => 'html',
                                'name' => 'current_supplier',
                                'value' => 'CHtml::image(Yii::app()->createAbsoluteUrl("admin/users/thumbnail/file/".$data->supplier->image))',
                            //  'footer' => LeadElcMeter::getSupplierDropdown()
                            ),
                            'corporate_sme' => array(
                                'type' => 'raw',
                                'filter' => false, //CHtml::listData(Users::getAllSuppliers(), 'id', 'fullname'), 'type' => 'html',
                                'name' => 'corporate_sme',
                                'value' => 'Product::getCorporateSME($data->corporate_sme)',
                            //  'footer' => LeadElcMeter::getCorporateDropdown()
                            ),
                            'utility' => array(
                                'type' => 'raw',
                                'filter' => false, //CHtml::listData(Users::getAllSuppliers(), 'id', 'fullname'), 'type' => 'html',
                                'name' => 'utility',
                                'value' => 'Product::getUtilites($data->utility)',
                            //  'footer' => LeadElcMeter::getUtilityDropdown()
                            ),
                            'meter_name' => array(
                                'filter' => false,
                                'name' => 'meter_name',
                            //  'footer' => '<input id="LeadElcMeter_meter_name_" class="span5 form-control" maxlength="255" name="LeadElcMeter[meter_name]" type="text">'
                            ),
                            'mpan' => array(
                                'filter' => false,
                                'name' => 'mpan',
                            //  'footer' => '<input id="LeadElcMeter_mpan_" class="span5 form-control" maxlength="255" name="LeadElcMeter[mpan]" type="text">'
                            ),
                            'total_eac' => array(
                                'filter' => false,
                                'name' => 'total_eac',
                            //  'footer' => '<input id="LeadElcMeter_total_eac_" class="span5 form-control" maxlength="255" name="LeadElcMeter[total_eac]" type="text">'
                            ),
                            'contract_end_date' => array(
                                'filter' => false,
                                'name' => 'contract_end_date',
                            // 'footer' => '<input id="LeadElcMeter_contract_end_date_" class="form-control " name="LeadElcMeter[contract_end_date]" type="text">'
                            ),
                            'meter_status' => array(
                                'filter' => false,
                                'name' => 'meter_status',
                                'value' => 'MeterStatus::getStatus($data->meter_status)',
                            // 'footer' => LeadElcMeter::getStatusDropdown()
                            ),
                        ),
                    ));
                    ?>

                </div>
                <div class="card-body">
                    <div class="card-header">
                        <div class="card-title">
                            <strong>Gas Meter</strong>
                        </div>
                    </div>
                    <?php
                    $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => 'lead-gas-meter-grid',
                        'dataProvider' => $gas->searchByLead($model->lead_id),
                        'afterAjaxUpdate' => 'function(id,options){ paginationControls(); }',
                        //  'filter' => $model,
                        'columns' => array(
                            'site_id' => array(
                                'name' => 'site_id',
                                'value' => '$data->site->site_name',
                                'filter' => false,
                            ),
                            'current_supplier' => array(
                                'type' => 'raw',
                                'filter' => false, //CHtml::listData(Users::getAllSuppliers(), 'id', 'fullname'), 'type' => 'html',
                                'name' => 'current_supplier',
                                'value' => 'CHtml::image(Yii::app()->createAbsoluteUrl("admin/users/thumbnail/file/".$data->supplier->image))',
                            ),
                            'corporate_sme' => array(
                                'type' => 'raw',
                                'filter' => false, //CHtml::listData(Users::getAllSuppliers(), 'id', 'fullname'), 'type' => 'html',
                                'name' => 'corporate_sme',
                                'value' => 'Product::getCorporateSME($data->corporate_sme)',
                            ),
                            'utility' => array(
                                'type' => 'raw',
                                'filter' => false, //CHtml::listData(Users::getAllSuppliers(), 'id', 'fullname'), 'type' => 'html',
                                'name' => 'utility',
                                'value' => 'Product::getUtilites($data->utility)',
                            ),
                            'meter_name' => array(
                                'filter' => false,
                                'name' => 'meter_name',
                            ),
                            'mpr' => array(
                                'filter' => false,
                                'name' => 'mpr',
                            ),
                            'total_aq' => array(
                                'name' => 'total_aq',
                                'filter' => false,
                            ),
                            'contract_end_date' => array(
                                'name' => 'contract_end_date',
                                'filter' => false,
                            ),
                            'meter_status' => array(
                                'name' => 'meter_status',
                                'value' => 'MeterStatus::getStatus($data->meter_status)',
                                'filter' => false,
                            ),
                        ),
                    ));
                    ?>

                </div>
            </div>
        </div>
    </div>
</section>
