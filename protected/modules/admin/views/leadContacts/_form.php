

<div class="card-header">

    <div class="card-title">
        <strong>Lead Details</strong>
    </div>
</div>
<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'lead-contacts-form',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
        ));
?>

<div class="col-md-12 border">
    <p class="help-block">Fields with <span class="required">*</span> are required.</p>

    <div class="col-lg-3">
        <?php echo $form->dropdownListRow($model, 'agency_id', Users::getAgencies(), array('onchange' => 'getAgencyId($(this).val())')); ?>
    </div>


    <div class="col-lg-3 ">
        <?php echo $form->dropdownListRow($model, 'agent_id', CHtml::listdata(Users::model()->findAllByAttributes(array('role_id' => 4)), "id", "username")); ?>
    </div>
    <div class="col-lg-3">
        <?php echo $form->dropdownListRow($model, 'is_user_detail', array("1" => "Yes", '2' => "No"), array('onchange' => 'getsowHide($(this).val())')); ?>
    </div>
    <div class="col-lg-3">
        <?php
        $user->password = '';
        echo $form->textFieldRow($user, 'fullname', array('class' => 'span5', 'maxlength' => 255,));
        ?>
    </div>
    <div class="col-lg-3">
        <?php echo $form->textFieldRow($user, 'username', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>
    <div class="col-lg-3 user_detail">
        <?php echo $form->textFieldRow($user, 'email', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>
    <div class="col-lg-2 user_detail">
        <?php echo $form->passwordFieldRow($user, 'password', array('class' => 'span5', 'maxlength' => 255)); ?>

    </div>
    <div class="col-lg-1 user_detail">
        <?php if (Yii::app()->user->isAdmin && $user->id) { ?>
            <a style="position:relative; top: 40px;" href="#" data-toggle="tooltip" title="<?php echo $user->password_plain; ?>"><i class="fa fa-eye"></i></a>
            <br>
        <?php }
        ?>
    </div>
    <div class = "col-lg-3 user_detail">
        <?php echo $form->passwordFieldRow($user, 'confirm_password', array('class' => 'span5', 'maxlength' => 255));
        ?>
    </div>
    <div class="col-lg-3">
        <?php echo $form->textFieldRow($model, 'position', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>
    <div class="col-lg-3">
        <?php echo $form->fileFieldRow($user, 'image'); ?>
    </div>

    <div class="col-lg-3">
        <?php echo $form->textFieldRow($model, 'landline', array('class' => 'span5')); ?>
    </div>
    <div class="col-lg-3">
        <?php echo $form->textFieldRow($model, 'mobile', array('class' => 'span5')); ?>
    </div>
    <div class="col-lg-3">
        <?php echo $form->dropdownListRow($model, 'status', CHtml::listData(LeadStatus::model()->findAll(), "id", "title")); ?>
    </div>
</div>





<div class="card-header">
    <div class="card-title">
        <strong>Business Details</strong>
    </div>
</div>

<div class="col-md-12">
    <p class="help-block">Fields with <span class="required">*</span> are required.</p>
    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_business, 'business_name', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>

    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_business, 'charity_no', array('class' => 'span5')); ?>
    </div>

    <div class="col-lg-3">
        <?php echo $form->dropdownListRow($l_business, 'trending_type', Agency::getTrendingType()); ?>
    </div>
    <div class="col-lg-3">
        <?php echo $form->dropdownListRow($l_business, 'business_type', CHtml::listData(BusinessType::model()->findAll(), "id", "title")); ?>
    </div>
    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_business, 'year_month_trading', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>
    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_business, 'credit_score', array('class' => 'span5')); ?>
    </div>
    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_business, 'partner_channel', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>


    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_business, 'campaign', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>


    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_business, 'external_reference', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>
    <div class="col-lg-3">
        <?php echo $form->dropdownListRow($l_business, 'client_reference', CHtml::listdata(PaymentType::model()->findAll(), "id", "title")); ?>

        <?php //echo $form->textFieldRow($l_business, 'client_reference', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>

    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_business, 'lead_source', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>

    <div class="col-lg-3">
        <?php echo $form->dropdownListRow($l_business, 'visibility', array('1' => 'Active', '0' => 'Inactive')); ?>
    </div>

    <div class="col-lg-3">
        <?php
        //echo $form->textFieldRow($l_business, 'business_level', array('class' => 'span5', 'maxlength' => 255));
        echo $form->labelEx($l_business, 'business_level');
        ?>
        <?php
        echo $form->radioButtonList($l_business, 'business_level', array('Micro' => 'Micro', 'Non Micro' => 'Non Micro'), array(
            'labelOptions' => array('style' => 'display:inline'),
            'separator' => '  ',
                )
        );
        ?>
        <?php //echo $form->radioButton($l_business, 'business_level', ) . 'Non Micro';  ?>

    </div>

</div>
<div class="card-header">
    <div class="card-title">
        <strong>Business Address</strong>
    </div>
</div>
<div class="col-md-12">
    <p class="help-block">Fields with <span class="required">*</span> are required.</p>
    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_address, 'business_name', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>
    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_address, 'business_no', array('class' => 'span5')); ?>
    </div>
    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_address, 'street_address_1', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>
    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_address, 'town', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>
    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_address, 'street_address_2', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>

    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_address, 'country', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>
    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_address, 'business_postcode', array('class' => 'span5', 'maxlength' => 50)); ?>
    </div>
    <div class="col-lg-12"><br/><br/>
        <input type="checkbox" id="bill_checkbox">
        <b>Billing address same as Business Address</b><br/><br/>
    </div>
    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_address, 'billing_name', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>
    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_address, 'billing_no', array('class' => 'span5')); ?>
    </div>
    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_address, 'street_address_11', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>
    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_address, 'town1', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>
    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_address, 'street_address_12', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>
    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_address, 'country1', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>
    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_address, 'billing_postcode', array('class' => 'span5', 'maxlength' => 50)); ?>
    </div>

    <!--    <div class="col-md-7">
            <br>
            <div class="form-actions">
                <button  class="submit_lead_button btn btn-primary" form_id="lead-business-form" type="submit" name="yt0">Create </button>
            </div>
        </div>-->
    <div class="col-md-12">
        <br>  
        <div class="form-actions">
            <button  class="submit_lead_button btn btn-primary" form_id="lead-contacts-form" type="submit" name="yt0">Create </button>      
        </div>
    </div>

</div>

<?php $this->endWidget(); ?>


<!--<div id="business_detail1" class="col-md-12">
<?php //echo $this->renderPartial('/leadBusiness/admin', array('model' => $business))     ?>

</div>-->

<div class="card-header">
    <div class="card-title">
        <strong>Site Contact and Site Details</strong>
    </div>
</div>

<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'lead-site-details-form',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'action' => $this->createAbsoluteUrl('leadSiteContactDetails/create'),
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
        ));
?>
<div class="col-lg-12">
    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_site, 'site_name', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>
    <?php echo $form->hiddenField($l_site, 'lead_id', array('value' => $model->id)); ?>
    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_site_contact, 'full_name', array('size' => 60, 'maxlength' => 255)); ?>
    </div>

    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_site_contact, 'position', array('size' => 60, 'maxlength' => 255)); ?>
    </div>

    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_site_contact, 'email', array('size' => 60, 'maxlength' => 255)); ?>
    </div>

    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_site_contact, 'landline', array('size' => 11, 'maxlength' => 11)); ?>
    </div>

    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_site_contact, 'mobile', array('size' => 11, 'maxlength' => 11)); ?>
    </div>



    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_site, 'client_reference', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>
    <div class="col-lg-3">
        <?php
        echo $form->dropDownListRow($l_site, 'external_reference', LeadContacts::getYesOrNo(), array('prompt' => 'Select', 'class' => 'span5', 'maxlength' => 255));
        ?>
    </div>
    <!--    <div class="col-lg-3">
            
    <?php //echo $form->textFieldRow($l_site, 'date_occupency', array('class' => 'span5'));   ?>
        </div>-->
    <div class="col-lg-3">
        <?php echo $form->label($l_site, 'date_occupency'); ?>
        <?php
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model' => $l_site,
            'attribute' => 'date_occupency',
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                'changeMonth' => true,
                'changeYear' => true,
            ),
        ));
        ?>
    </div>
    <div class="col-lg-3">
        <?php echo $form->label($l_site, 'cost_code'); ?>
        <?php
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model' => $l_site,
            'attribute' => 'cost_code',
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                'changeMonth' => true,
                'changeYear' => true,
            ),
        ));
        ?>
    </div>

</div>


<div class="card-header">
    <div class="card-title">
        <strong>Site Address </strong>
    </div>
</div>
<input type="checkbox" class="checkbox_2" id="site_address_autofill" value="bill_two" ><b>Site Address same as Business Address</b><br>
<input type="checkbox" class="checkbox_2" id="bill_address_autofill" value="bill_two" ><b>Billing Address same as Site Address</b><br>
<div class="col-md-12">
    <p class="help-block">Fields with <span class="required">*</span> are required.</p>
    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_site_address, 'site_address_name', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>
    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_site_address, 'site_address_no', array('class' => 'span5')); ?>
    </div>
    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_site_address, 'site_street1', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>
    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_site_address, 'site_street2', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>
    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_site_address, 'site_town', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>
    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_site_address, 'site_country', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>
    <!--    <div class="col-lg-3">
    <?php echo $form->textFieldRow($l_site_address, 'site_address_same', array('rows' => 6, 'cols' => 50, 'class' => 'span8')); ?>
    </div>-->
    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_site_address, 'site_post_code', array('class' => 'span5', 'maxlength' => 64)); ?>
    </div>
    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_site_address, 'billing_name', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>
    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_site_address, 'billing_no', array('class' => 'span5')); ?>
    </div>
    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_site_address, 'street_address_1', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>
    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_site_address, 'street_address_2', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>
    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_site_address, 'town', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>
    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_site_address, 'country', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>
    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_site_address, 'billing_post_code', array('class' => 'span5', 'maxlength' => 64)); ?>
    </div>


</div>

<div class="col-md-12">
    <div class="form-actions">
        <button class=" submit_lead_button btn btn-primary" form_id="lead-site-details-form" type="submit" name="yt0">Create </button>
    </div>
</div>
<?php $this->endWidget(); ?>

<div id="site_contact_SITE1" class="col-md-12">
</div>


<div class="card-header">
    <div class="card-title">
        <button  id="additional_info" class="btn btn-success"><i class="fa fa-plus"></i> Additonal Details</button>
    </div>
</div>

<div class="col-md-12" id="additional_info_" style="display: none">
    <?php
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'lead-additional-form',
        'action' => $this->createAbsoluteUrl('leadSite/saveAddiotional'),
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
    ));
    ?>
    <div class="col-lg-12">
        <?php echo $form->dropdownListRow($additional, 'site_id', CHtml::listdata(LeadSite::model()->findAllByAttributes(array('lead_id' => $model->id)), "id", "site_name")); ?>
    </div>
    <div class="card-header">
        <div class="card-title">
            <strong>Telecom Details</strong>
        </div>
    </div>
    <div class="col-md-12">

        <br><br>
        <div class="col-lg-2">
            <?php echo $form->textFieldRow($additional, 'tel_provider', array('class' => 'span5', 'maxlength' => 255)); ?>
        </div>
        <div class="col-lg-2">
            <?php
            echo $form->label($additional, 'tel_s_date');
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $additional,
                'attribute' => 'tel_s_date',
                'options' => array(
                    'dateFormat' => 'yy-mm-dd',
                    'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                    'changeMonth' => true,
                    'changeYear' => true,
                ),
            ));
            ?> 
        </div>
        <div class="col-lg-2">
            <?php
            echo $form->label($additional, 'tel_e_date');
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $additional,
                'attribute' => 'tel_e_date',
                'options' => array(
                    'dateFormat' => 'yy-mm-dd',
                    'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                    'changeMonth' => true,
                    'changeYear' => true,
                ),
            ));
            ?> 
        </div>
        <div class="col-lg-2">
            <?php echo $form->textFieldRow($additional, 'tel_ac_no', array('class' => 'span5', 'maxlength' => 255)); ?>
        </div>
        <div class="col-lg-2">
            <?php echo $form->textFieldRow($additional, 'tel_ac_pwd', array('class' => 'span5', 'maxlength' => 255)); ?>
        </div>
    </div>

    <div class="card-header">
        <div class="card-title">
            <strong>Broadband Details</strong>
        </div>
    </div>
    <div class="col-md-12">
        <div class="col-lg-2">
            <?php echo $form->textFieldRow($additional, 'broad_provider', array('class' => 'span5', 'maxlength' => 255)); ?>
        </div>
        <div class="col-lg-2">
            <?php
            echo $form->label($additional, 'broad_s_date');
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $additional,
                'attribute' => 'broad_s_date',
                'options' => array(
                    'dateFormat' => 'yy-mm-dd',
                    'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                    'changeMonth' => true,
                    'changeYear' => true,
                ),
            ));
            ?> 
        </div>
        <div class="col-lg-2">
            <?php
            echo $form->label($additional, 'broad_e_date');
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $additional,
                'attribute' => 'broad_e_date',
                'options' => array(
                    'dateFormat' => 'yy-mm-dd',
                    'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                    'changeMonth' => true,
                    'changeYear' => true,
                ),
            ));
            ?> 
        </div>
        <div class="col-lg-2">
            <?php echo $form->textFieldRow($additional, 'broad_ac_no', array('class' => 'span5', 'maxlength' => 255)); ?>
        </div>
        <div class="col-lg-2">
            <?php echo $form->textFieldRow($additional, 'broad_ac_pwd', array('class' => 'span5', 'maxlength' => 255)); ?>
        </div>
    </div>
    <div class="card-header">
        <div class="card-title">
            <strong>TV Details</strong>
        </div>
    </div>
    <div class="col-md-12">
        <div class="col-lg-2">
            <?php echo $form->textFieldRow($additional, 'tv_provider', array('class' => 'span5', 'maxlength' => 255)); ?>
        </div>
        <div class="col-lg-2">
            <?php
            echo $form->label($additional, 'tv_s_date');
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $additional,
                'attribute' => 'tv_s_date',
                'options' => array(
                    'dateFormat' => 'yy-mm-dd',
                    'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                    'changeMonth' => true,
                    'changeYear' => true,
                ),
            ));
            ?> 
        </div>
        <div class="col-lg-2">
            <?php
            echo $form->label($additional, 'tv_e_date');
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $additional,
                'attribute' => 'tv_e_date',
                'options' => array(
                    'dateFormat' => 'yy-mm-dd',
                    'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                    'changeMonth' => true,
                    'changeYear' => true,
                ),
            ));
            ?> 
        </div>
        <div class="col-lg-2">
            <?php echo $form->textFieldRow($additional, 'tv_ac_no', array('class' => 'span5', 'maxlength' => 255)); ?>
        </div>
        <div class="col-lg-2">
            <?php echo $form->textFieldRow($additional, 'tv_ac_pwd', array('class' => 'span5', 'maxlength' => 255)); ?>
        </div>
    </div>
    <div class="card-header">
        <div class="card-title">
            <strong>Water Details</strong>
        </div>
    </div>
    <div class="col-md-12">
        <div class="col-lg-2">
            <?php echo $form->textFieldRow($additional, 'water_provider', array('class' => 'span5', 'maxlength' => 255)); ?>
        </div>
        <div class="col-lg-2">
            <?php
            echo $form->label($additional, 'water_s_date');
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $additional,
                'attribute' => 'water_s_date',
                'options' => array(
                    'dateFormat' => 'yy-mm-dd',
                    'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                    'changeMonth' => true,
                    'changeYear' => true,
                ),
            ));
            ?> 
        </div>
        <div class="col-lg-2">
            <?php
            echo $form->label($additional, 'water_e_date');
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $additional,
                'attribute' => 'water_e_date',
                'options' => array(
                    'dateFormat' => 'yy-mm-dd',
                    'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                    'changeMonth' => true,
                    'changeYear' => true,
                ),
            ));
            ?> 
        </div>
        <div class="col-lg-2">
            <?php echo $form->textFieldRow($additional, 'water_ac_no', array('class' => 'span5', 'maxlength' => 255)); ?>
        </div>
        <div class="col-lg-2">
            <?php echo $form->textFieldRow($additional, 'water_ac_pwd', array('class' => 'span5', 'maxlength' => 255)); ?>
        </div>
    </div>

    <div class="col-md-12">
        <br>  
        <div class="form-actions">
            <button  class="submit_lead_button btn btn-primary" form_id="lead-additional-form" type="submit" name="yt0">Create </button>      
        </div>
    </div>

    <?php $this->endWidget(); ?>

</div>
<div id="site_contact_additional" class="col-md-12">
</div>



<div class="card-header">

    <div class="card-title">
        <strong>Electric Meter Details </strong>
    </div>
</div>


<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'lead-elc-meter-form',
    'action' => $this->createAbsoluteUrl('leadElcMeter/create'),
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
        ));
?>

<div class="col-md-12">
    <p class="help-block">Fields with <span class="required">*</span> are required.</p>

    <?php //echo $form->errorSummary($model);          ?>
    <div class="col-lg-3">
        <?php echo $form->dropdownListRow($l_elec_meter, 'site_id', array("" => "Please Select Site")); ?>
    </div>
    <div class="col-lg-3">
        <?php echo $form->dropDownListRow($l_elec_meter, 'corporate_sme', Product::getCorporateSME(), array('onchange' => 'getUtilities($(this).val())')); ?>
    </div>
    <div class="col-lg-3">

        <?php echo $form->dropDownListRow($l_elec_meter, 'utility', CHtml::listData(Product::getUtilites(), 'id', 'title')); ?>
    </div>
    <div class="col-lg-3">
        <?php echo $form->dropdownListRow($l_elec_meter, 'current_supplier', CHtml::listData(Users::getAllSuppliers(), 'id', 'username')); ?>
    </div>
    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_elec_meter, 'meter_name', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>
    <div class="col-lg-3">
        <?php echo $form->dropdownListRow($l_elec_meter, 'meter_status', CHtml::listData(MeterStatus::model()->findAll(), "id", "title")); ?>
    </div>
    <div class="col-lg-3">
        <?php //echo $form->textFieldRow($l_elec_meter, 'meter_pricing', array('class' => 'span5', 'maxlength' => 255));       ?>

        <?php echo $form->label($l_elec_meter, 'meter_pricing'); ?>
        <?php
        echo $form->dropDownList($l_elec_meter, 'meter_pricing', LeadContacts::getYesOrNo(), array('prompt' => 'Select', 'class' => 'span5', 'maxlength' => 255));
        ?>
    </div>

    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_elec_meter, 'pes', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>
    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_elec_meter, 'mpan', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>

    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_elec_meter, 'serial_no', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>

    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_elec_meter, 'total_eac', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>
    <div class="col-lg-3">

        <?php echo $form->label($l_elec_meter, 'contract_end_date'); ?>
        <?php
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model' => $l_elec_meter,
            'attribute' => 'contract_end_date',
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                'changeMonth' => true,
                'changeYear' => true,
            ),
            'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));

        //echo $form->textFieldRow($l_elec_meter, 'contract_end_date', array('class' => 'span5'));
        ?>
        <?php echo $form->error($l_elec_meter, 'contract_end_date'); ?>
    </div>
    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_elec_meter, 'msn', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>
    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_elec_meter, 'kva', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>
    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_elec_meter, 'voltage', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>
    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_elec_meter, 'ct_wc', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>
    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_elec_meter, 'mop', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>
    <div class="col-lg-3">
        <?php echo $form->fileFieldRow($l_elec_meter, 'document', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>

    <div class="col-md-12">
        <div class="form-actions">
            <button class=" submit_lead_button btn btn-primary" form_id="lead-elc-meter-form" type="submit" name="yt0">Create </button>
        </div>
    </div>
</div>
<?php $this->endWidget(); ?>

<div class="col-md-12" id="electric_meter">
    <?php //echo $this->renderPartial('/leadElcMeter/admin', array('model' => $electric))     ?>

</div>

<div class="card-header">

    <div class="card-title">
        <strong>Gas Meter Details </strong>
    </div>
</div>

<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'lead-gas-meter-form',
    'action' => $this->createAbsoluteUrl('leadGasMeter/create'),
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
?>
<div class="col-md-12">
    <p class="help-block">Fields with <span class="required">*</span> are required.</p>
    <div class="col-lg-3">
        <?php echo $form->dropdownListRow($l_gas_meter, 'site_id', array("" => "Please Select Site")); ?>
    </div>

    <div class="col-lg-3">
        <?php echo $form->dropDownListRow($l_gas_meter, 'corporate_sme', Product::getCorporateSME(), array('onchange' => 'getUtilities1($(this).val())')); ?>
    </div>
    <div class="col-lg-3">

        <?php echo $form->dropDownListRow($l_gas_meter, 'utility', CHtml::listData(Product::getUtilites(), 'id', 'title')); ?>
    </div>
    <?php //echo $form->dropdownListRow($l_gas_meter, 'lead_id', CHtml::listData(LeadContacts::model()->findAll(), "id", "first_name"));        ?>

    <div class="col-lg-3">
        <?php echo $form->dropdownListRow($l_gas_meter, 'current_supplier', CHtml::listData(Users::getAllSuppliers(), 'id', 'username')); ?>
    </div>
    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_gas_meter, 'meter_name', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>
    <div class="col-lg-3">
        <?php echo $form->dropdownListRow($l_gas_meter, 'meter_status', CHtml::listData(MeterStatus::model()->findAll(), "id", "title")); ?>
    </div>
    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_gas_meter, 'smart_meter', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>

    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_gas_meter, 'mpr', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>

    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_gas_meter, 'serial_no', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>


    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_gas_meter, 'total_aq', array('class' => 'span5', 'maxlength' => 255));
        ?>
    </div>
    <div class="col-lg-3">
        <?php echo $form->label($l_gas_meter, 'contract_end_date'); ?>
        <?php
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model' => $l_gas_meter,
            'attribute' => 'contract_end_date',
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                'changeMonth' => true,
                'changeYear' => true,
            ),
        ));
        ?>
        <?php echo $form->error($l_gas_meter, 'contract_end_date'); ?>
    </div>

    <div class="col-lg-3">
        <?php echo $form->textFieldRow($l_gas_meter, 'meter_pricing', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>
    <div class="col-lg-3">
        <?php //echo $form->label($l_upload, 'document');  ?>
        <?php
//        $this->widget('CMultiFileUpload', array(
//            'model' => $l_upload,
//            'attribute' => 'document',
//            'accept' => 'xsl|csv|xsls|jpg|jpeg|png',
//            'options' => array(
//            // 'onFileSelect'=>'function(e, v, m){ alert("onFileSelect - "+v) }',
//            // 'afterFileSelect'=>'function(e, v, m){ alert("afterFileSelect - "+v) }',
//            // 'onFileAppend'=>'function(e, v, m){ alert("onFileAppend - "+v) }',
//            // 'afterFileAppend'=>'function(e, v, m){ alert("afterFileAppend - "+v) }',
//            // 'onFileRemove'=>'function(e, v, m){ alert("onFileRemove - "+v) }',
//            // 'afterFileRemove'=>'function(e, v, m){ alert("afterFileRemove - "+v) }',
//            ),
//            'denied' => 'File is not allowed',
//            'max' => 10, // max 10 files
//        ));
//        
        ?>
        <?php //echo $form->error($l_upload, 'document');  ?>
        <?php echo $form->fileFieldRow($l_gas_meter, 'document', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>

    <div class="col-md-12">
        <div class="form-actions">
            <button class=" submit_lead_button btn btn-primary" form_id="lead-gas-meter-form" type="submit" name="yt0">Create </button>
        </div>
    </div>
</div>
<?php $this->endWidget(); ?>

<div id="gas_detail" class="col-md-12">
</div>

<?php //echo $update ;    ?>
<script>
    function getUtilities(id) {
        var url = '<?php echo Yii::app()->createAbsoluteUrl("admin/product/getUtilities/id") ?>/' + id;
        $("#LeadElcMeter_utility").load(url);
    }
    function getUtilities1(id) {
        var url = '<?php echo Yii::app()->createAbsoluteUrl("admin/product/getUtilities/id") ?>/' + id;
        $("#LeadGasMeter_utility").load(url);
    }

    function autofill(lead_id) {
        var url_ = '<?php echo Yii::app()->createAbsoluteUrl("admin/leadContacts/getAutofill/lead_id/") ?>/' + lead_id;
        $.ajax({
            url: url_,
            type: 'POST',
            success: function (res) {
                res = $.parseJSON(res);
                for (var property in res.site) {
                    if (res.site.hasOwnProperty(property)) {
                        $('#LeadSite_' + property).val(res.site[property]);
                    }
                }
                for (var property in res.site_address) {
                    if (res.site_address.hasOwnProperty(property)) {
                        $('#LeadSiteAddress_' + property).val(res.site_address[property]);
                    }
                }
                for (var property in res.site_contact) {
                    if (res.site_contact.hasOwnProperty(property)) {
                        $('#LeadSiteContactDetails_' + property).val(res.site_contact[property]);
                    }
                }
            }
        });
    }
    $(".submit_lead_button").click(function (e) {
        e.preventDefault();
        var form_id = $(this).attr("form_id");
        var data = new FormData($("#" + form_id)[0]);
        var url = $("#" + form_id).attr('action');
        var contact_url_save = '<?php echo Yii::app()->createAbsoluteUrl("admin/leadSiteContactDetails/create"); ?>';
        var elc_url_save = '<?php echo Yii::app()->createAbsoluteUrl("admin/leadElcMeter/create"); ?>';
        var gas_url_save = '<?php echo Yii::app()->createAbsoluteUrl("admin/leadGasMeter/create"); ?>';
        //return;
        $.ajax({
            url: url,
            data: data,
            type: 'POST',
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                res = JSON.parse(res);
                if (res.id == 0) {
                    // $("#" + form_id).submit();
                } else {
                    $("#myModalmessage").modal();
                    document.getElementById("lead-site-details-form").reset();
                    $("#lead-site-details-form").attr("action", contact_url_save);
                    document.getElementById("lead-elc-meter-form").reset();
                    document.getElementById("lead-gas-meter-form").reset();
                    $("#lead-elc-meter-form").attr("action", elc_url_save);
                    $("#lead-gas-meter-form").attr("action", gas_url_save);
                    if (form_id == "lead-contacts-form") {
                        $("#LeadSite_lead_id").val(res.id);
                        $("#LeadSite_lead_id_").val(res.id);
                    }
                    if ((form_id == 'lead-gas-meter-form')) {
                        updateGasMeter(res.id);
                        var url_dropdown2 = '<?php echo Yii::app()->createAbsoluteUrl("admin/leadSite/siteDropdown/lead_id/"); ?>/' + res.id;
                        $("#LeadGasMeter_site_id_").load(url_dropdown2);

                    }
                    if (form_id == 'lead-elc-meter-form') {
                        updateElcMeter(res.id);
                        //var url_dropdown1 = '<?php echo Yii::app()->createAbsoluteUrl("admin/leadSite/siteDropdown/lead_id/"); ?>/' + res.id;
                        // $("#LeadElcMeter_site_id_").load(url_dropdown1);

                    }
                    if (form_id == 'lead-additional-form') {
                        updateAdditionalGrid(res.id);
                    }
                    if (form_id == 'lead-site-details-form') {
                        updateDropdown(res.id);
                        updateSiteDropdown(res.id);
                        updateAdditionalDD(res.id);
                    } else {
                        $.fn.yiiGridView.update('lead-site-contact-details-grid');
                        $.fn.yiiGridView.update('lead-elc-meter-grid');
                        $.fn.yiiGridView.update('lead-gas-meter-grid');
                        $.fn.yiiGridView.update('lead-site-grid');
                        $.fn.yiiGridView.update('lead-additional-grid');
                    }
                }
                return false;
            }
        });
    });
    function updateDropdown(site_id) {
        var url_dropdown = '<?php echo Yii::app()->createAbsoluteUrl("admin/leadSite/siteDropdown/lead_id/"); ?>/' + site_id;
        $("#LeadElcMeter_site_id").load(url_dropdown);
        $("#LeadGasMeter_site_id").load(url_dropdown);
        $("#LeadAdditional_site_id").load(url_dropdown);

        //  $("#LeadElcMeter_site_id_").load(url_dropdown);
        //$("#LeadGasMeter_site_id_").load(url_dropdown);

    }
    function updateGasMeter(id) {
        var url_gas = '<?php echo Yii::app()->createAbsoluteUrl("admin/leadGasMeter/updateGasGrid/lead_id/") ?>/' + id;
        $("#gas_detail").load(url_gas);
    }

    function updateElcMeter(id) {
        var url_electric = '<?php echo Yii::app()->createAbsoluteUrl("admin/leadElcMeter/updateElcGrid/lead_id/") ?>/' + id;
        $("#electric_meter").load(url_electric);
    }
    function updateSiteDropdown(id) {
        var url_site = '<?php echo Yii::app()->createAbsoluteUrl("admin/leadSite/updateSiteGrid/lead_id/") ?>/' + id;
        $("#site_contact_SITE1").load(url_site);

    }

    function updateAdditionalGrid(id) {
        var url_site = '<?php echo Yii::app()->createAbsoluteUrl("admin/leadSite/additional/lead_id/") ?>/' + id;
        $("#site_contact_additional").load(url_site);

    }

    var update = '<?php echo $update ?>';
    if (update) {
        var lead_ids = '<?php echo $model->id ?>';

        updateSiteDropdown(lead_ids);
        updateAdditionalGrid(lead_ids);
        updateGasMeter(lead_ids);
        updateElcMeter(lead_ids);
        updateDropdown(lead_ids);

    }

    getAgencyId($("#LeadContacts_agency_id").val());
    function getAgencyId(id) {
        var siteagency = '<?php echo Yii::app()->createAbsoluteUrl("/admin/leadContacts/getAgencyId") ?>/id/' + id;
        $.ajax({
            url: siteagency,
            type: 'POST',
            success: function (res) {
                $("#LeadContacts_agent_id").html(res);
            }
        });
    }
    function updateDropdown(site_id) {
        // alert(site_id);
        var url_dropdown = '<?php echo Yii::app()->createAbsoluteUrl("admin/leadSite/siteDropdown/lead_id/"); ?>/' + site_id;
        $("#LeadElcMeter_site_id").load(url_dropdown);
        $("#LeadGasMeter_site_id").load(url_dropdown);
          $("#LeadAdditional_site_id").load(url_dropdown);
        // $("#LeadElcMeter_site_id_").load(url_dropdown);
        // $("#LeadGasMeter_site_id_").load(url_dropdown);


    }


    function updateAdditionalDD(lead_id) {
        var url_dropdown = '<?php echo Yii::app()->createAbsoluteUrl("admin/leadSite/addDropdown/lead_id/"); ?>/' + lead_id;
       // $("#LeadAdditional_site_id").load(url_dropdown);
    }
    //  functio
//    $(window).load(function(){
//         var lead_ids = '<?php echo $model->id ?>';
//         if(lead_ids)
//             updateDropdown(lead_ids);
//    });

</script>
<style>
    label[for="LeadBusiness_business_level"] {
        margin-right: 10px;
    }
    input#LeadBusiness_business_level {
        margin-left: 10px;
    }
    .form-actions button {
        float: right;
    }
</style>
<style>
    .container-fluid nav#top_navigation {
        display: none;
    }
    .topp {
        float: left;
        width: 100%;
    }
    input.checkbox, input.checkbox_2 {
        float: left;
        display: inline-block;
        margin-right: 7px !important;
    }
</style>
<style>
    .error{
        position: absolute;
    }
    .col-lg-3{
        margin-bottom: 16px;
    }
</style>
<script>
    function getsowHide(id) {
        if (id == 1) {
            $(".user_detail").show();
        } else {
            $(".user_detail").hide();
        }
    }


    $("#additional_info").click(function (e) {
        e.preventDefault();
        $("#additional_info_").slideToggle("slow");
    });
</script>