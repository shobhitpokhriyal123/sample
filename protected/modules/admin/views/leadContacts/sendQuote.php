<div class="container-fluid">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Send Quote</h4>
    </div>
    <div id="" class="modal-body">
        <?php
        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'customer-quote-form',
            'enableAjaxValidation' => false,
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
            ),
            'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
//echo '<pre>'; print_r($model);die;
        $cid = '';
        if ($model && $model->customer_id) {
            $cid = $model->customer_id;
        }
        ?>
        <div class="col-md-12">
            <div class="col-md-6 common_height">
                <?php
                if (Yii::app()->user->isCustomer) {
                    echo $form->dropdownListRow($model, 'supplier_id', CHtml::listData(Users::model()->findAllByAttributes(array('role_id' => 2, 'active' => 1)), 'id', 'fullname'), array('empty' => '--- Select Supplier ---', 'disabled' => 'disabled', 'class' => 'form-control'));
                } else {
                    ?>
                    <?php echo $form->dropdownListRow($model, 'supplier_id', CHtml::listData(Users::model()->findAllByAttributes(array('role_id' => 2, 'active' => 1)), 'id', 'fullname'), array('empty' => '--- Select Supplier ---', 'class' => 'form-control')); ?>
                <?php } ?>
            </div>

            <?php
            echo $form->hiddenField($model, 'customer_id', array('value' => $lead_id));
            ?>
            <div class="col-md-6 common_height">
                <?php
                echo $form->dropdownListRow($model, 'contract_status', CHtml::listData(ContractStatus::model()->findAll(), 'id', 'title'), array('empty' => '--- Select Status ---', 'class' => 'form-control'));
                ?>
            </div>
            <div class="col-md-6 common_height">
                <?php echo $form->label($model, "meter_ids"); ?>

                <?php
                echo CHtml::activeListBox($model, 'meter_ids', CHtml::listData($checkedmeter, "id", "meter_name"), array('class' => 'chosen form-control', 'multiple' => true, 'data-placeholder' => 'Select'))
                ?>
                <?php
                $this->widget('ext.chosen.EChosenWidget', array(
                    'selector' => '.chosen',
                ));
                ?>
                <?php echo $form->error($model, "meter_ids"); ?>
            </div>
            <?php echo $form->hiddenField($model, 'meter_type', array('value' => $type)); ?>

            <div class="col-md-6 common_height">
                <?php echo $form->textFieldRow($model, 'product', array('class' => 'form-control')); ?>
            </div> 


            <div class="col-lg-6">
                <?php echo $form->label($model, 'live_date'); ?>
                <?php
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $model,
                    'attribute' => 'live_date',
                    'options' => array(
                        'dateFormat' => 'yy-mm-dd',
                        'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                        'changeMonth' => true,
                        'changeYear' => true,
                    ),
                    'htmlOptions' => array(
                        'class' => 'form-control'
                    ),
                ));
                ?>
                <?php echo $form->error($model, 'live_date'); ?>
            </div>

            <div class="col-lg-6">
                <?php echo $form->label($model, 'start_date'); ?>
                <?php
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $model,
                    'attribute' => 'start_date',
                    'options' => array(
                        'dateFormat' => 'yy-mm-dd',
                        'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                        'changeMonth' => true,
                        'changeYear' => true,
                    ),
                    'htmlOptions' => array(
                        'class' => 'form-control'
                    ),
                ));
                ?>
                <?php echo $form->error($model, 'start_date'); ?>
            </div>


            <div class="col-lg-6">
                <?php echo $form->label($model, 'end_date'); ?>
                <?php
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $model,
                    'attribute' => 'end_date',
                    'options' => array(
                        'dateFormat' => 'yy-mm-dd',
                        'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                        'changeMonth' => true,
                        'changeYear' => true,
                    ),
                    'htmlOptions' => array(
                        'class' => 'form-control'
                    ),
                ));
                ?>
                <?php echo $form->error($model, 'end_date'); ?>
            </div>

            <div class="col-md-6 common_height">
                <?php echo $form->textFieldRow($model, 'min_volume', array('class' => 'form-control', 'maxlength' => 255)); ?>
            </div> 
            <div class="col-md-6 common_height">
                <?php echo $form->textFieldRow($model, 'max_volume', array('class' => 'form-control', 'maxlength' => 255)); ?>
            </div> 
            <div class="col-md-6 common_height">
                <?php echo $form->textFieldRow($model, 'credit_position', array('class' => 'form-control')); ?>
            </div> 
            <div class="col-md-6 common_height">
                <?php echo $form->fileFieldRow($model, 'supply_contract', array('class' => 'form-control', 'maxlength' => 255)); ?>
            </div> 
            <div class="col-md-6 common_height">
                <?php //echo $form->dropdownListRow($model, 'offer_type', CustomerQuote::getOfferTypes());     ?>

                <?php echo $form->textFieldRow($model, 'consumption', array('class' => 'form-control', 'maxlength' => 255)); ?>
            </div> 
            <div class="col-md-6 common_height">
                <?php echo $form->textFieldRow($model, 'standing_charge', array('class' => 'form-control', 'maxlength' => 255)); ?>
            </div> 
            <div class="col-md-6 common_height clone-this ">
                <?php echo $form->label($model, 'unit_rate'); ?>
                <?php echo $form->textField($model, 'unit_rate[]', array('class' => 'form-control ', 'maxlength' => 255)); ?>
                <?php echo $form->error($model, 'unit_rate'); ?>
                <?php
                $this->widget('ext.reCopy.ReCopyWidget', array(
                    'targetClass' => 'clone-this',
                    'addButtonLabel' => '<i class="fa fa-plus"></i>',
                    'addButtonCssClass' => 'add-clone',
                    'removeButtonLabel' => '<i class="fa fa-remove"></i>',
                    'removeButtonCssClass' => 'remove-clone',
                    'limit' => 5,
                ));
                ?>
            </div> 



        </div>
        <?php $this->endWidget(); ?>
        <div class="col-md-6 common_height"  >
            <?php //echo $form->textFieldRow($model, 'created', array('class' => 'span5'));       ?>
            <?php if (Yii::app()->user->isAdmin) { ?>
                <div class="form-actions">
                    <div class="form-actions">
                        <button class="btn submit_qotess" type="submit" name="yt0">Create</button>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="card-header">
        <div class="card-title">
            <strong>Quotes</strong>
        </div>
    </div>
    <div class="col-md-12" id="quotes_id">

    </div>

</div>

<div class="clearfix"></div>
<br><br>
<?php $ld = LeadContacts::model()->findByPk($lead_id); ?>
<script>
    $(".submit_qotess").click(function (e) {
      //  alert('dsgfsdfg');
        e.preventDefault();
       // e.stopPropagation();
        var formData = new FormData($("#customer-quote-form")[0]);
        var url = $("#customer-quote-form").attr("action");
        $.ajax({
            url: url,
            data: formData,
            type: 'POST',
            processData: false,
            contentType: false,
            success: function (res) {
                $.fn.yiiGridView.update("lead-quote-grid1");
                return false;
            }
        });
        return false;
    });


    var url_q = '<?php echo Yii::app()->createAbsoluteUrl("admin/leadQuote/quotes", array('cid' => ($ld) ? $ld->user_id : $ld)) ?>';
    $("#quotes_id").load(url_q);

</script>

<style>
    .common_height{
        height: 76px;
    }

    .chosen-container {
        width: 100%!important;
    }
</style>




