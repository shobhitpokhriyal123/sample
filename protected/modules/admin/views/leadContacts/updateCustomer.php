<section id="manage_products">
    <div class="container-fluid">
        <div class="row">
            <div class="card">
                <div class="card-header">

                    <div class="card-title">
                        <strong>Customer Management</strong>
                    </div>
                    <?php
                    if (Yii::app()->user->isAdmin) {
                        //echo CHtml::link('Details', array('/admin/leadContacts/admin'), array("class" => 'btn btn-success'));
                    }
                    ?>

                </div>
                <div class="card-body">
                    <div class="container-fluid">
                        <div class="col-md-6">
                            <form name="fdgsdfg" action="<?php echo Yii::app()->createUrl('/admin/leadContacts/addCustomer') ?>" method="post" >
                                <div class="col-md-12">
                                    <div class="col-md-9">
                                        <?php
                                        $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                                            'name' => 'LeadAddress[business_name]',
                                            'source' => $array,
                                            // additional javascript options for the autocomplete plugin
                                            'options' => array(
                                                'minLength' => '1',
                                            ),
                                            'htmlOptions' => array(
                                                // 'style' => 'height:20px;',
                                                'placeholder' => 'Search by business'
                                            ),
                                        ));
                                        ?>
                                    </div>
                                    <div  class="col-md-3">
                                        <button id="iodfghsdogfihsdaofgi" class="btn btn-success" type="submit">Submit</button>    
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-6">
                            <?php
                            if (Yii::app()->user->isAdmin) {
                                echo CHtml::link('Manage Quote', array('/admin/leadContacts/quotes'), array("class" => 'btn btn-success right'));
                                echo CHtml::link('Send Input Quote', array('/admin/leadContacts/sendQuote'), array("class" => 'btn btn-success right', 'id' => 'send_quote'));
                            }
                            //
                            ?>
                        </div>
                    </div>
                    <div class="card-header">

                        <div class="card-title">
                            <strong>Customers</strong>
                        </div>
                    </div>
                    <div class="container-fluid">
                        <?php
                        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                            'id' => 'lead-contacts-form',
                            'enableAjaxValidation' => false,
                            'enableClientValidation' => true,
                            'clientOptions' => array(
                                'validateOnSubmit' => true,
                            ),
                        ));
                        ?>

                        <div class="col-md-12 border">
                            <p class="help-block">Fields with <span class="required">*</span> are required.</p>

                            <div class="col-lg-3">
                                <?php echo $form->dropdownListRow($model, 'agency_name', CHtml::listdata(Users::model()->findAllByAttributes(array('role_id' => 3)), "id", "username"), array('onchange' => 'getAgencyId($(this).val())')); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($model, 'agency_id', CHtml::listdata(Users::model()->findAllByAttributes(array('role_id' => 3)), "id", "username")); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->dropdownListRow($model, 'agent_id', CHtml::listdata(Users::model()->findAllByAttributes(array('role_id' => 4)), "id", "username")); ?>
                            </div>

                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($model, 'first_name', array('class' => 'span5', 'maxlength' => 255)); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($user, 'email', array('class' => 'span5', 'maxlength' => 255)); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($user, 'username', array('class' => 'span5', 'maxlength' => 255)); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->passwordFieldRow($user, 'password', array('class' => 'span5', 'maxlength' => 255)); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->passwordFieldRow($user, 'confirm_password', array('class' => 'span5', 'maxlength' => 255)); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->fileFieldRow($user, 'image'); ?>
                            </div>

                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($model, 'landline', array('class' => 'span5')); ?>
                            </div>


                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($model, 'mobile', array('class' => 'span5')); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->dropdownListRow($model, 'status', CHtml::listData(LeadStatus::model()->findAll(), "id", "title")); ?>
                            </div>
                            <div class="col-lg-12">
                                <?php //echo $form->textFieldRow($model,'created',array('class'=>'span5'));    ?>
                                <br>  
                                <div class="form-actions">
                                    <button class="submit_lead_button btn btn-primary" form_id = 'lead-contacts-form' type="submit" name="yt0">Create </button>      
                                    <?php
//                                    $this->widget('bootstrap.widgets.TbButton', array(
//                                        'buttonType' => 'submit',
//                                        'type' => 'primary',
//                                      //  'form_id' => 'lead-contacts-form',
//                                        'class' => 'submit_lead_button',
//                                        'label' => $model->isNewRecord ? 'Create ' : 'Save',
//                                    ));
                                    ?>
                                </div>
                            </div>
                        </div>

                        <?php $this->endWidget(); ?>


                        <!--                    <div class="col-md-12" id="contact_details"></div>-->

                        <div class="card-header">

                            <div class="card-title">
                                <strong>Business Details</strong>
                            </div>
                        </div>

                        <?php
                        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                            'id' => 'lead-business-form',
                            'action' => $this->createAbsoluteUrl('leadBusiness/create'),
                            'enableAjaxValidation' => false,
                            'enableClientValidation' => true,
                            'clientOptions' => array(
                                'validateOnSubmit' => true,
                            ),
                        ));
                        ?>

                        <div class="col-md-12">
                            <p class="help-block">Fields with <span class="required">*</span> are required.</p>
                            <?php echo $form->hiddenField($l_business, 'lead_id', array('value' => '1')); ?>
                            <?php //echo $form->errorSummary($model);   ?>
                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_business, 'business_name', array('class' => 'span5', 'maxlength' => 255)); ?>
                            </div>

                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_business, 'charity_no', array('class' => 'span5')); ?>
                            </div>

                            <div class="col-lg-3">
                                <?php echo $form->dropdownListRow($l_business, 'trending_type', Agency::getTrendingType()); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->dropdownListRow($l_business, 'business_type', CHtml::listData(BusinessType::model()->findAll(), "id", "title")); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_business, 'year_month_trading', array('class' => 'span5', 'maxlength' => 255)); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_business, 'credit_score', array('class' => 'span5')); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_business, 'partner_channel', array('class' => 'span5', 'maxlength' => 255)); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_business, 'campaign', array('class' => 'span5', 'maxlength' => 255)); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_business, 'external_reference', array('class' => 'span5', 'maxlength' => 255)); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_business, 'client_reference', array('class' => 'span5', 'maxlength' => 255)); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_business, 'lead_source', array('class' => 'span5', 'maxlength' => 255)); ?>
                            </div>


                            <div class="col-lg-3">
                                <?php echo $form->dropdownListRow($l_business, 'visibility', array('1' => 'Active', '0' => 'Inactive')); ?>
                            </div>

                            <div class="col-lg-3">
                                <?php
                                //echo $form->textFieldRow($l_business, 'business_level', array('class' => 'span5', 'maxlength' => 255)); 
                                echo $form->labelEx($l_business, 'business_level');
                                ?>
                                <?php echo $form->radioButton($l_business, 'business_level', array('value' => 'Micro')) . 'Micro'; ?>
                                <?php echo $form->radioButton($l_business, 'business_level', array('value' => 'Non Micro')) . 'Non Micro'; ?>

                            </div>

                            <div class="col-lg-12">
                                <?php //echo $form->textFieldRow($model,'created',array('class'=>'span5'));      ?>
                                <br><br>
                                <div class="form-actions">
                                    <button class="submit_lead_button btn btn-primary" form_id = 'lead-business-form' type="submit" name="yt0">Create </button>   
                                    <?php
//                                    $this->widget('bootstrap.widgets.TbButton', array(
//                                        'buttonType' => 'submit',
//                                        'type' => 'primary',
//                                        'form_id' => 'lead-business-form',
//                                        'class' => 'submit_lead_button',
//                                        'label' => $l_business->isNewRecord ? 'Create' : 'Save',
//                                    ));
                                    ?>
                                </div>
                            </div>
                        </div>
                        <?php $this->endWidget(); ?>


                        <!--                    <div class="col-md-12" id="business_detail"></div>-->

                        <div class="card-header">
                            <div class="card-title">
                                <strong>Business Address</strong>
                            </div>
                        </div>

                        <?php
                        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                            'id' => 'lead-address-form',
                            'action' => $this->createAbsoluteUrl('leadAddress/create'),
                            'enableAjaxValidation' => false,
                            'enableClientValidation' => true,
                            'clientOptions' => array(
                                'validateOnSubmit' => true,
                            ),
                        ));
                        ?>


                        <div class="col-md-12">
                            <p class="help-block">Fields with <span class="required">*</span> are required.</p>

                            <div class="col-lg-3">
                                <?php echo $form->hiddenField($l_address, 'lead_id'); ?>
                            </div>

                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_address, 'business_name', array('class' => 'span5', 'maxlength' => 255)); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_address, 'business_no', array('class' => 'span5')); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_address, 'street_address_1', array('class' => 'span5', 'maxlength' => 255)); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_address, 'street_address_2', array('class' => 'span5', 'maxlength' => 255)); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_address, 'town', array('class' => 'span5', 'maxlength' => 255)); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_address, 'country', array('class' => 'span5', 'maxlength' => 255)); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_address, 'business_postcode', array('class' => 'span5', 'maxlength' => 50)); ?>
                            </div>

                            <div class="col-lg-12"><br/><br/>
                                <input type="checkbox" id="bill_checkbox">
                                <!--   <em>Check this box if Shipping Address and Billing Address are the same.</em>-->
                                <b>Billing address same as Business Address</b><br/><br/>

                            </div>


                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_address, 'billing_name', array('class' => 'span5', 'maxlength' => 255)); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_address, 'billing_no', array('class' => 'span5')); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_address, 'street_address_11', array('class' => 'span5', 'maxlength' => 255)); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_address, 'town1', array('class' => 'span5', 'maxlength' => 255)); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_address, 'street_address_12', array('class' => 'span5', 'maxlength' => 255)); ?>
                            </div>

                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_address, 'country1', array('class' => 'span5', 'maxlength' => 255)); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_address, 'billing_postcode', array('class' => 'span5', 'maxlength' => 50)); ?>
                            </div>

                            <div class="col-lg-12">

                                <?php // echo $form->textFieldRow($model, 'created', array('class' => 'span5'));     ?>
                                <br>  
                                <div class="form-actions">
                                    <button class=" submit_lead_button btn btn-primary" form_id = 'lead-address-form' type="submit" name="yt0">Create </button>   

                                    <?php
//                                    $this->widget('bootstrap.widgets.TbButton', array(
//                                        'buttonType' => 'submit',
//                                        'type' => 'primary',
//                                        'form_id' => 'lead-address-form',
//                                        'class' => 'submit_lead_button',
//                                        'label' => $l_address->isNewRecord ? 'Create' : 'Save',
//                                    ));
                                    ?>
                                </div>
                            </div>
                        </div>

                        <?php $this->endWidget(); ?>

                        <!--                    <div class="col-md-12" id="address_details"></div>-->

                        <div class="card-header">

                            <div class="card-title">
                                <strong>Site Contact and Site Details</strong>
                            </div>
                        </div>


                        <?php
                        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                            'id' => 'lead-site-contact-details-form',
                            'action' => $this->createAbsoluteUrl('leadSiteContactDetails/create'),
                            'enableAjaxValidation' => false,
                            'enableClientValidation' => true,
                            'clientOptions' => array(
                                'validateOnSubmit' => true,
                            ),
                            'htmlOptions' => array('enctype' => 'multipart/form-data'),
                        ));
                        ?>
                        <div class="col-lg-12">
                            <p class="note">Fields with <span class="required">*</span> are required.</p>


                            <!--                            <div class="col-lg-3">-->
                            <?php echo $form->hiddenField($l_site, 'lead_id', array('value' => '1')); ?>
                            <!--                            </div>-->
                            <div class="col-lg-3">
                                <?php echo $form->labelEx($l_site_contact, 'title'); ?>
                                <?php echo $form->textField($l_site_contact, 'title', array('size' => 60, 'maxlength' => 255)); ?>
                                <?php //echo $form->error($model,'title');    ?>
                            </div>

                            <div class="col-lg-3">
                                <?php echo $form->labelEx($l_site_contact, 'full_name'); ?>
                                <?php echo $form->textField($l_site_contact, 'full_name', array('size' => 60, 'maxlength' => 255)); ?>
                                <?php //echo $form->error($model,'full_name');    ?>
                            </div>

                            <div class="col-lg-3">
                                <?php echo $form->labelEx($l_site_contact, 'position'); ?>
                                <?php echo $form->textField($l_site_contact, 'position', array('size' => 60, 'maxlength' => 255)); ?>
                                <?php //echo $form->error($model,'position');    ?>
                            </div>

                            <div class="col-lg-3">
                                <?php echo $form->labelEx($l_site_contact, 'email'); ?>
                                <?php echo $form->textField($l_site_contact, 'email', array('size' => 60, 'maxlength' => 255)); ?>
                                <?php //echo $form->error($model,'email');    ?>
                            </div>

                            <div class="col-lg-3">
                                <?php echo $form->labelEx($l_site_contact, 'landline'); ?>
                                <?php echo $form->textField($l_site_contact, 'landline', array('size' => 11, 'maxlength' => 11)); ?>
                                <?php //echo $form->error($model,'landline');    ?>
                            </div>

                            <div class="col-lg-3">
                                <?php echo $form->labelEx($l_site_contact, 'mobile'); ?>
                                <?php echo $form->textField($l_site_contact, 'mobile', array('size' => 11, 'maxlength' => 11)); ?>
                                <?php //echo $form->error($model,'mobile');    ?>
                            </div>


                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_site, 'site_name', array('class' => 'span5', 'maxlength' => 255)); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_site, 'client_reference', array('class' => 'span5', 'maxlength' => 255)); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->label($l_site, 'external_reference'); ?>
                                <?php
                                echo $form->dropDownList($l_site, 'external_reference', LeadContacts::getYesOrNo(), array('prompt' => 'Select', 'class' => 'span5', 'maxlength' => 255));
                                ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_site, 'date_occupency', array('class' => 'span5')); ?>
                            </div>
                            <?php //echo $form->textFieldRow($l_site, 'cost_code', array('class' => 'span5', 'maxlength' => 60));    ?>
                            <div class="col-lg-3">

                                <?php echo $form->label($l_site, 'cost_code'); ?>
                                <?php
                                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                    'model' => $l_site,
                                    'attribute' => 'cost_code',
                                    'options' => array(
                                        'dateFormat' => 'yy-mm-dd',
                                        'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                                        'changeMonth' => true,
                                        'changeYear' => true,
                                    ),
                                    'htmlOptions' => array('enctype' => 'multipart/form-data'),
                                ));

//echo $form->textFieldRow($l_elec_meter, 'contract_end_date', array('class' => 'span5')); 
                                ?>

                            </div>


                            <br><br>
                            <div class="form-actions col-lg-12">
                                <button class="submit_lead_button btn btn-primary" form_id = 'lead-site-contact-details-form' type="submit" name="yt0">Create </button>   

                                <?php
//                                $this->widget('bootstrap.widgets.TbButton', array(
//                                    'buttonType' => 'submit',
//                                    'type' => 'primary',
//                                    'form_id' => 'lead-site-contact-details-form',
//                                    'class' => 'submit_lead_button',
//                                    'label' => $l_site_contact->isNewRecord ? 'Create' : 'Save',
//                                ));
                                ?>
                            </div>
                        </div>
                        <?php $this->endWidget(); ?>
                        <!--
                                            <div id="site_contact_detail" class="col-md-12"></div>-->


                        <?php
//$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
//    'id' => 'lead-site-form',
//    'action' => $this->createAbsoluteUrl('leadSite/create'),
//    'enableAjaxValidation' => false,
//    'enableClientValidation' => true,
//    'clientOptions' => array(
//        'validateOnSubmit' => true,
//    ),
//        ));
                        ?>


                        <?php
//        $this->widget('bootstrap.widgets.TbButton', array(
//            'buttonType' => 'submit',
//            'type' => 'primary',
//            'label' => $l_site->isNewRecord ? 'Create' : 'Save',
//        ));
                        ?>


                        <!--
                        
                        <div class="col-md-12" id="site_detail"></div>-->

                        <div class="card-header">
                            <div class="card-title">
                                <strong>Site Address </strong>
                            </div>
                        </div>
                        <br><br>
                        <input type="checkbox" name="check" > Same as Business Address
                        <br><br>
                        <?php
                        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                            'id' => 'lead-site-address-form',
                            'action' => $this->createAbsoluteUrl('leadSiteAddress/create'),
                            'enableAjaxValidation' => false,
                            'enableClientValidation' => true,
                            'clientOptions' => array(
                                'validateOnSubmit' => true,
                            ),
                        ));
                        ?>

                        <div class="col-md-12">
                            <p class="help-block">Fields with <span class="required">*</span> are required.</p>

                            <?php //echo $form->errorSummary($model);       ?>
                            <!--    <div class="col-lg-3">
                            
                            <?php echo $form->dropdownListRow($l_site_address, 'lead_id', CHtml::listData(LeadContacts::model()->findAll(), "id", "first_name")); ?>
                            
                                </div>-->
                            <div class="col-lg-3">
                                <?php echo $form->dropdownListRow($l_site_address, 'site_id', CHtml::listData(LeadSite::model()->findAll(), "id", 'site_name')); ?>
                            </div>

                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_site_address, 'site_address_name', array('class' => 'span5', 'maxlength' => 255)); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_site_address, 'site_address_no', array('class' => 'span5')); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_site_address, 'site_street1', array('class' => 'span5', 'maxlength' => 255)); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_site_address, 'site_street2', array('class' => 'span5', 'maxlength' => 255)); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_site_address, 'site_town', array('class' => 'span5', 'maxlength' => 255)); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_site_address, 'site_country', array('class' => 'span5', 'maxlength' => 255)); ?>
                            </div>
                            <!--    <div class="col-lg-3">
                            <?php echo $form->textFieldRow($l_site_address, 'site_address_same', array('rows' => 6, 'cols' => 50, 'class' => 'span8')); ?>
                                </div>-->
                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_site_address, 'site_post_code', array('class' => 'span5', 'maxlength' => 64)); ?>
                            </div>

                            <?php //echo $form->textFieldRow($l_site_address, 'business_address_same', array('rows' => 6, 'cols' => 50, 'class' => 'span8'));     ?>


                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_site_address, 'billing_name', array('class' => 'span5', 'maxlength' => 255)); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_site_address, 'billing_no', array('class' => 'span5')); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_site_address, 'street_address_1', array('class' => 'span5', 'maxlength' => 255)); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_site_address, 'street_address_2', array('class' => 'span5', 'maxlength' => 255)); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_site_address, 'town', array('class' => 'span5', 'maxlength' => 255)); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_site_address, 'country', array('class' => 'span5', 'maxlength' => 255)); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_site_address, 'billing_post_code', array('class' => 'span5', 'maxlength' => 64)); ?>
                            </div>

                            <div class="col-lg-12">
                                <br/><br/>
                                <input type="checkbox" class="checkbox" name="bill_address" value="bill"> <b>Billing Address same as Site Address</b> 
                                <br/>
                                <input type="checkbox" class="checkbox_2" vname="bill_address" value="bill_two" ><b> Billing Address same as Business Address</b> <br/>

                            </div>

                            <div class="col-lg-12">

                                <br><br>
                                <div class="form-actions">
                                    <button class="submit_lead_button btn btn-primary" form_id = 'lead-site-address-form' type="submit" name="yt0">Create </button>   

                                    <?php
//                                    $this->widget('bootstrap.widgets.TbButton', array(
//                                        'buttonType' => 'submit',
//                                        'type' => 'primary',
//                                        'form_id' => 'lead-site-address-form',
//                                        'class' => 'submit_lead_button',
//                                        'label' => $l_site_address->isNewRecord ? 'Create' : 'Save',
//                                    ));
                                    ?>
                                </div>
                            </div>
                        </div>

                        <?php $this->endWidget(); ?>

                        <!--                    <div class="col-md-12" id="site_add_detail"></div>-->

                        <div class="card-header">

                            <div class="card-title">
                                <strong>Electric Meter Details </strong>
                            </div>
                        </div>


                        <?php
                        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                            'id' => 'lead-elc-meter-form',
                            'action' => $this->createAbsoluteUrl('leadElcMeter/create'),
                            'enableAjaxValidation' => false,
                            'enableClientValidation' => true,
                            'clientOptions' => array(
                                'validateOnSubmit' => true,
                            ),
                        ));
                        ?>

                        <div class="col-md-12">
                            <p class="help-block">Fields with <span class="required">*</span> are required.</p>

                            <?php //echo $form->errorSummary($model);        ?>
                            <div class="col-lg-3">
                                <?php echo $form->dropdownListRow($l_elec_meter, 'site_id', CHtml::listData(LeadSite::model()->findAll(), "id", "site_name")); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->dropDownListRow($l_elec_meter, 'corporate_sme', Product::getCorporateSME(), array('onchange' => 'getUtilities($(this).val())')); ?>
                            </div>   
                            <div class="col-lg-3">

                                <?php echo $form->dropDownListRow($l_elec_meter, 'utility', CHtml::listData(Product::getUtilites(), 'id', 'title')); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->dropdownListRow($l_elec_meter, 'current_supplier', CHtml::listData(Users::getAllSuppliers(), 'id', 'username')); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_elec_meter, 'meter_name', array('class' => 'span5', 'maxlength' => 255)); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->dropdownListRow($l_elec_meter, 'meter_status', CHtml::listData(MeterStatus::model()->findAll(), "id", "title")); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php //echo $form->textFieldRow($l_elec_meter, 'meter_pricing', array('class' => 'span5', 'maxlength' => 255));     ?>

                                <?php echo $form->label($l_elec_meter, 'meter_pricing'); ?>
                                <?php
                                echo $form->dropDownList($l_elec_meter, 'meter_pricing', LeadContacts::getYesOrNo(), array('prompt' => 'Select', 'class' => 'span5', 'maxlength' => 255));
                                ?>   


                            </div>
                            <!--                            <div class="col-lg-3">
                            <?php //echo $form->textFieldRow($l_elec_meter, 'pc', array('class' => 'span5', 'maxlength' => 255));     ?>
                                                        </div>
                                                        <div class="col-lg-3">
                            <?php //echo $form->textFieldRow($l_elec_meter, 'mtc', array('class' => 'span5', 'maxlength' => 255));     ?>
                                                        </div>
                                                        <div class="col-lg-3">
                            <?php //echo $form->textFieldRow($l_elec_meter, 'llf', array('class' => 'span5', 'maxlength' => 255));     ?>
                                                        </div>-->
                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_elec_meter, 'pes', array('class' => 'span5', 'maxlength' => 255)); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_elec_meter, 'mpan', array('class' => 'span5', 'maxlength' => 255)); ?>
                            </div>

                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_elec_meter, 'serial_no', array('class' => 'span5', 'maxlength' => 255)); ?>
                            </div>

                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_elec_meter, 'total_eac', array('class' => 'span5', 'maxlength' => 255)); ?>
                            </div>
                            <div class="col-lg-3">

                                <?php echo $form->label($l_elec_meter, 'contract_end_date'); ?>
                                <?php
                                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                    'model' => $l_elec_meter,
                                    'attribute' => 'contract_end_date',
                                    'options' => array(
                                        'dateFormat' => 'yy-mm-dd',
                                        'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                                        'changeMonth' => true,
                                        'changeYear' => true,
                                    ),
                                    'htmlOptions' => array('enctype' => 'multipart/form-data'),
                                ));

//echo $form->textFieldRow($l_elec_meter, 'contract_end_date', array('class' => 'span5')); 
                                ?>
                                <?php echo $form->error($l_elec_meter, 'contract_end_date'); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_elec_meter, 'msn', array('class' => 'span5', 'maxlength' => 255)); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_elec_meter, 'kva', array('class' => 'span5', 'maxlength' => 255)); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_elec_meter, 'voltage', array('class' => 'span5', 'maxlength' => 255)); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_elec_meter, 'ct_wc', array('class' => 'span5', 'maxlength' => 255)); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_elec_meter, 'mop', array('class' => 'span5', 'maxlength' => 255)); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->label($l_upload, 'document'); ?>
                                <?php
                                $this->widget('CMultiFileUpload', array(
                                    'model' => $l_upload,
                                    'attribute' => 'document',
                                    'accept' => 'xsl|csv|xsls|jpg|jpeg|png',
                                    'options' => array(
                                    // 'onFileSelect'=>'function(e, v, m){ alert("onFileSelect - "+v) }',
                                    // 'afterFileSelect'=>'function(e, v, m){ alert("afterFileSelect - "+v) }',
                                    // 'onFileAppend'=>'function(e, v, m){ alert("onFileAppend - "+v) }',
                                    // 'afterFileAppend'=>'function(e, v, m){ alert("afterFileAppend - "+v) }',
                                    // 'onFileRemove'=>'function(e, v, m){ alert("onFileRemove - "+v) }',
                                    // 'afterFileRemove'=>'function(e, v, m){ alert("afterFileRemove - "+v) }',
                                    ),
                                    'denied' => 'File type is not allowed',
                                    'max' => 10, // max 10 files
                                ));
                                ?>
                                <?php echo $form->error($l_upload, 'document'); ?>
                                <?php //echo $form->fileFieldRow($l_upload, 'document', array('class' => 'span5', 'maxlength' => 255));    ?>
                            </div>

                            <div class="col-lg-12">
                                <div class="form-actions">
                                    <br><br>
                                    <button class=" submit_lead_button btn btn-primary" form_id = 'lead-elc-meter-form' type="submit" name="yt0">Create </button>   

                                    <?php
//                                    $this->widget('bootstrap.widgets.TbButton', array(
//                                        'buttonType' => 'submit',
//                                        'type' => 'primary',
//                                        'form_id' => 'lead-elc-meter-form',
//                                        'class' => 'submit_lead_button',
//                                        'label' => $l_elec_meter->isNewRecord ? 'Create' : 'Save',
//                                    ));
                                    ?>

                                </div>
                            </div>
                        </div>
                        <?php $this->endWidget(); ?>

                        <!--                    <div class="col-md-12" id="electric_meter"></div>-->

                        <div class="card-header">

                            <div class="card-title">
                                <strong>Gas Meter Details </strong>
                            </div>
                        </div>

                        <?php
                        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                            'id' => 'lead-gas-meter-form',
                            'action' => $this->createAbsoluteUrl('leadGasMeter/create'),
                            'enableAjaxValidation' => false,
                            'enableClientValidation' => true,
                            'clientOptions' => array(
                                'validateOnSubmit' => true,
                            ),
                            'htmlOptions' => array('enctype' => 'multipart/form-data'),
                        ));
                        ?>
                        <div class="col-md-12">
                            <p class="help-block">Fields with <span class="required">*</span> are required.</p>
                            <div class="col-lg-3">
                                <?php echo $form->dropdownListRow($l_gas_meter, 'site_id', CHtml::listData(LeadSite::model()->findAll(), "id", "site_name")); ?>
                            </div>

                            <div class="col-lg-3">
                                <?php echo $form->dropDownListRow($l_gas_meter, 'corporate_sme', Product::getCorporateSME(), array('onchange' => 'getUtilities1($(this).val())')); ?>
                            </div>   
                            <div class="col-lg-3">

                                <?php echo $form->dropDownListRow($l_gas_meter, 'utility', CHtml::listData(Product::getUtilites(), 'id', 'title')); ?>
                            </div>
                            <?php //echo $form->dropdownListRow($l_gas_meter, 'lead_id', CHtml::listData(LeadContacts::model()->findAll(), "id", "first_name"));     ?>

                            <div class="col-lg-3">
                                <?php echo $form->dropdownListRow($l_gas_meter, 'current_supplier', CHtml::listData(Users::getAllSuppliers(), 'id', 'username')); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_gas_meter, 'meter_name', array('class' => 'span5', 'maxlength' => 255)); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->dropdownListRow($l_gas_meter, 'meter_status', CHtml::listData(MeterStatus::model()->findAll(), "id", "title")); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_gas_meter, 'smart_meter', array('class' => 'span5', 'maxlength' => 255)); ?>
                            </div>

                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_gas_meter, 'mpr', array('class' => 'span5', 'maxlength' => 255)); ?>
                            </div>

                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_gas_meter, 'serial_no', array('class' => 'span5', 'maxlength' => 255)); ?>
                            </div>


                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_gas_meter, 'total_aq', array('class' => 'span5', 'maxlength' => 255));
                                ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->label($l_gas_meter, 'contract_end_date'); ?>
                                <?php
                                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                    'model' => $l_gas_meter,
                                    'attribute' => 'contract_end_date',
                                    'options' => array(
                                        'dateFormat' => 'yy-mm-dd',
                                        'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                                        'changeMonth' => true,
                                        'changeYear' => true,
                                    ),
                                ));
                                ?>
                                <?php echo $form->error($l_gas_meter, 'contract_end_date'); ?>
                            </div>

                            <div class="col-lg-3">
                                <?php echo $form->textFieldRow($l_gas_meter, 'meter_pricing', array('class' => 'span5', 'maxlength' => 255)); ?>
                            </div>
                            <div class="col-lg-3">
                                <?php echo $form->label($l_upload, 'document'); ?>
                                <?php
                                $this->widget('CMultiFileUpload', array(
                                    'model' => $l_upload,
                                    'attribute' => 'document_gas',
                                    'accept' => 'xsl|csv|xsls|jpg|jpeg|png',
                                    'options' => array(
                                    // 'onFileSelect'=>'function(e, v, m){ alert("onFileSelect - "+v) }',
                                    // 'afterFileSelect'=>'function(e, v, m){ alert("afterFileSelect - "+v) }',
                                    // 'onFileAppend'=>'function(e, v, m){ alert("onFileAppend - "+v) }',
                                    // 'afterFileAppend'=>'function(e, v, m){ alert("afterFileAppend - "+v) }',
                                    // 'onFileRemove'=>'function(e, v, m){ alert("onFileRemove - "+v) }',
                                    // 'afterFileRemove'=>'function(e, v, m){ alert("afterFileRemove - "+v) }',
                                    ),
                                    'denied' => 'File is not allowed',
                                    'max' => 10, // max 10 files
                                ));
                                ?>
                                <?php echo $form->error($l_upload, 'document'); ?>
                                <?php //echo $form->fileFieldRow($l_upload, 'document', array('class' => 'span5', 'maxlength' => 255));    ?>
                            </div>
                            <div class="col-md-12">

                                <div class="form-actions">
                                    <button class=" submit_lead_button btn btn-primary" form_id = 'lead-gas-meter-form' type="submit" name="yt0">Create </button>   

                                    <?php
//                                    $this->widget('bootstrap.widgets.TbButton', array(
//                                        'buttonType' => 'submit',
//                                        'type' => 'primary',
//                                        'form_id' => 'lead-gas-meter-form',
//                                        'class' => 'submit_lead_button',
//                                        'label' => $l_gas_meter->isNewRecord ? 'Create' : 'Save',
//                                    ));
                                    ?>
                                </div>
                            </div>
                        </div>
                        <?php $this->endWidget(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    getAgencyId($("#LeadContacts_agency_name").val());
    function getAgencyId(id) {
        var siteagency = '<?php echo Yii::app()->createAbsoluteUrl("/admin/leadContacts/getAgencyId") ?>/id/' + id;
        $.ajax({
            url: siteagency,
            type: 'POST',
            success: function (res) {
                $("#LeadContacts_agency_id").val(res);
            }
        });

    }

    function getUtilities(id) {

        var url = '<?php echo Yii::app()->createAbsoluteUrl("admin/product/getUtilities/id") ?>/' + id;
        $("#LeadElcMeter_utility").load(url);


    }


    function getUtilities1(id) {

        var url = '<?php echo Yii::app()->createAbsoluteUrl("admin/product/getUtilities/id") ?>/' + id;
        $("#LeadGasMeter_utility").load(url);


    }


    $("#bill_checkbox").click(function () {
        if ($(this).is(":checked")) {
            $("#LeadAddress_billing_no").val($("#LeadAddress_business_no").val());
            $("#LeadAddress_street_address_11").val($("#LeadAddress_street_address_1").val());
            $("#LeadAddress_town1").val($("#LeadAddress_town").val());
            $("#LeadAddress_country1").val($("#LeadAddress_country").val());
            $("#LeadAddress_street_address_12").val($("#LeadAddress_street_address_2").val());
        }
    });


</script>
<!--<style>
    label[for="LeadBusiness_business_level"] {
        margin-right: 10px;
    }
    input#LeadBusiness_business_level {
        margin-left: 10px;
    }
    .form-actions button {
        float: right;
    }
</style>
-->
<script>
    // function getMeters(id) {
    //var url = '<?php //echo Yii::app()->createAbsoluteUrl("/admin/leadContacts/getMeters/id")                             ?>/' + id;
////            $.ajax({
////                url: url,
////                type: "POST",
////                success: function (res) {
////                    $("#meters").html(res);
////                }
////            });
////    }
//
////    function getMeters() {
////        alert();
////        $("#select_meter").show();
////        
////    }

    $(document).ready(function () {
        $("#iodfghsdogfihsdaofgi").click(function (e) {
            e.preventDefault();
            var url = '<?php echo Yii::app()->createAbsoluteUrl("/admin/leadContacts/isLeadExists") ?>';
            var data = $("#LeadAddress_business_name").val();
            $.ajax({
                url: url,
                type: "POST",
                data: {data: data},
                success: function (res) {
                    if (res == 0) {

                    } else {

                    }
                }
            });
        });
    });
    $("#send_quote").click(function (e) {
        e.preventDefault();
        var url = '<?php echo Yii::app()->createAbsoluteUrl("admin/leadContacts/sendQuote") ?>';
        $("#modal_bodys").load(url);
        $("#myModalcd").modal();

//        $.ajax({
//            url: url,
//            type: 'POST',
//            succees: function (res) {
//                $("#myModalcd").modal();
//                $("#modal_bodys").html(res);
//               
//            }
//        });
    });

</script>


<!-- Modal -->
<div id="myModalcd" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width:90%">
        <div class="modal-content" id="modal_bodys">

        </div>
    </div>
</div>


<div id="myModalmessage" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title success">Data successfully saved..! Please proceed..</h4>
            </div>
            <div class="modal-body">
                <p>Data successfully saved...!.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<style>
    .error{
        position: absolute;
    }
    .col-lg-3{
        margin-bottom: 16px;
    }
</style>
<script>
    $(".submit_lead_button").click(function (e) {
        e.preventDefault();
        var form_id = $(this).attr("form_id");
        var data = $("#" + form_id).serialize();
        var url = $("#" + form_id).attr('action');
        $.ajax({
            url: url,
            data: data,
            type: 'POST',
            success: function (res) {
                res = JSON.parse(res);
                if (res.id == 0) {
                    $("#" + form_id).submit();
                } else {
                    if (res.next_form != 0) {
                        $("#" + res.next_form).val(res.id);
                    }
                    if (res.next_form1 != 0) {
                        $("#" + res.next_form1).val(res.id);
                    }
                    if (res.next_form2 != 0) {
                        $("#" + res.next_form2).val(res.id);
                    }
                    $("#myModalmessage").modal();
                    
                    return false;
                }
            }
        });
    });
</script>