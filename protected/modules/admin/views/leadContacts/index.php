<?php
$this->breadcrumbs=array(
	'Lead Contacts',
);

$this->menu=array(
	array('label'=>'Create LeadContacts','url'=>array('create')),
	array('label'=>'Manage LeadContacts','url'=>array('admin')),
);
?>

<h1>Lead Contacts</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
