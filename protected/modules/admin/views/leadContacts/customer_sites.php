<section id="manage_products">
    <div class="container-fluid">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <strong>Manage Sites</strong>
                    </div>
                </div>
                <div class="card-body">
                    <?php
                    $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => 'lead-site-grid',
                        'dataProvider' => $model->searchCustomerSite(Yii::app()->user->id),
                        'afterAjaxUpdate' => 'function(id,options){ paginationControls(); }',
                        'filter' => $model,
                        'columns' => array(
                            'site_name' => array(
                                'name' => 'site_name',
                                'filter' => false,
                            ),
//                            'lead_id' => array(
//                                'name' => 'lead_id',
//                                'filter' => false,
//                                'header' => 'Business',
//                                'value' => '$data->lead->business->business_name'
//                            ),
                            'landline' => array(
                                'name' => 'landline',
                                'filter' => false,
                                'value' => '$data->contact->landline'
                            ),
                            'mobile' => array(
                                'name' => 'mobile',
                                'filter' => false,
                                'value' => '$data->contact->mobile'
                            ),
                            'site_street1' => array(
                                'name' => 'site_street1',
                                'filter' => false,
                                'value' => '$data->address->site_street1'
                            ),
                            'site_street2' => array(
                                'name' => 'site_street2',
                                'filter' => false,
                                'value' => '$data->address->site_street2'
                            ),
                            'site_town' => array(
                                'name' => 'site_town',
                                'filter' => false,
                                'value' => '$data->address->site_town'
                            ),
                            'site_country' => array(
                                'name' => 'site_country',
                                'filter' => false,
                                'value' => '$data->address->site_country'
                            ),
                            'site_post_code' => array(
                                'name' => 'site_post_code',
                                'filter' => false,
                                'value' => '$data->address->site_post_code'
                            ),
                            'site_post_code' => array(
                                'name' => 'site_post_code',
                                'filter' => false,
                                'value' => '$data->address->site_post_code'
                            ),
                            'billing_no' => array(
                                'name' => 'billing_no',
                                'filter' => false,
                                'value' => '$data->address->billing_no'
                            ),
                            'street_address_1' => array(
                                'name' => 'street_address_1',
                                'filter' => false,
                                'value' => '$data->address->street_address_1'
                            ),
                            'town' => array(
                                'name' => 'town',
                                'filter' => false,
                                'value' => '$data->address->town'
                            ),
                            'country' => array(
                                'name' => 'country',
                                'filter' => false,
                                'value' => '$data->address->country'
                            ),
                            'billing_post_code' => array(
                                'name' => 'billing_post_code',
                                'filter' => false,
                                'value' => '$data->address->billing_post_code'
                            ),
                            'street_address_1' => array(
                                'name' => 'street_address_1',
                                'filter' => false,
                                'value' => '$data->address->street_address_1'
                            ),
                            array(
                                'class' => 'bootstrap.widgets.TbButtonColumn',
                                'template' => '{view}',
                                'header' => 'Actions',
                                'buttons' => array(
                                    'view' => array(
                                        'label' => '<button class="btn btn-primary">View</button>',
                                        'url' => 'Yii::app()->createAbsoluteUrl("admin/LeadSite/customerMeter", array("id"=>$data->id))',
                                        'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'View'),
                                        'imageUrl' => false,
                                    ),
                                ),
                            ),
                        ),
                    ));
                    ?>



                    <div class="card">

                        <div class="card-header">
                            <div class="card-title">
                                <strong>Additional Details</strong>
                            </div>
                        </div>
                        <?php
                        echo $this->renderPartial('additional', array('model' => $additional, 'sites' => $sites));
                        ?>
                    </div>

                </div> 
            </div>  
        </div>
    </div>
</section>