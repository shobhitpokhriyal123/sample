<section id="manage_products">
    <div class="container-fluid">
        <div class="row">
            <div class="card">
                <div class="card-header">

                    <div class="card-title">
                        <strong>Lead Management</strong>
                    </div>
                    <?php
                    if (Yii::app()->user->isAdmin) {
                        echo CHtml::link('Manage Leads', array('/admin/leadContacts/allLeads'), array("class" => 'btn btn-success'));
                    }
                    ?>
                </div>
                <div class="card-body">

                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="container-fluid">
                        <?php
                        echo $this->renderPartial('_form', array(
                            'model' => $model,
                            'user' => $l_user,
                            'l_address' => $l_address,
                            'l_business' => $l_business,
                            'l_elec_meter' => $l_elec_meter,
                            'l_gas_meter' => $l_gas_meter,
                            'l_site' => $l_site,
                            'l_site_address' => $l_site_address,
                            'l_upload' => $l_upload,
                            'l_site_contact' => $l_site_contact,
                            'update'=>$update,
                            'additional'=>$additional
//                            'site' => $site,
//                            'electric' => $electric,
//                            'gas' => $gas,
//                            'leadsites' => $leadsites
                                //  'l_user' => $user,
                                //  'l_agency' => $agencyy
                        ));
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div id="myModalmessage" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title success">Data successfully saved..! Please proceed..</h4>
            </div>
            <div class="modal-body">
                <p>Data successfully saved...!.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<script type="text/javascript">

    $("#bill_checkbox").click(function () {
        if ($(this).is(":checked")) {
            $("#LeadAddress_billing_no").val($("#LeadAddress_business_no").val());
            $("#LeadAddress_street_address_11").val($("#LeadAddress_street_address_1").val());
            $("#LeadAddress_town1").val($("#LeadAddress_town").val());
            $("#LeadAddress_country1").val($("#LeadAddress_country").val());
            $("#LeadAddress_street_address_12").val($("#LeadAddress_street_address_2").val());
            $("#LeadAddress_billing_name").val($("#LeadAddress_business_name").val());
            $("#LeadAddress_billing_postcode").val($("#LeadAddress_business_postcode").val());
        }
    });
    $("#bill_address_autofill").click(function () {
        if ($(this).is(":checked")) {
            $("#LeadSiteAddress_billing_name").val($("#LeadSiteAddress_site_address_name").val());
            $("#LeadSiteAddress_billing_no").val($("#LeadSiteAddress_site_address_no").val());
            $("#LeadSiteAddress_street_address_1").val($("#LeadSiteAddress_site_street1").val());
            $("#LeadSiteAddress_street_address_2").val($("#LeadSiteAddress_site_street2").val());
            $("#LeadSiteAddress_town").val($("#LeadSiteAddress_site_town").val());
            $("#LeadSiteAddress_country").val($("#LeadSiteAddress_site_country").val());
            $("#LeadSiteAddress_billing_post_code").val($("#LeadSiteAddress_site_post_code").val());
        }
    });
    $("#site_address_autofill").click(function () {
        if ($(this).is(":checked")) {
            $("#LeadSiteAddress_site_address_name").val($("#LeadAddress_business_name").val());
            $("#LeadSiteAddress_site_address_no").val($("#LeadAddress_business_no").val());
            $("#LeadSiteAddress_site_street1").val($("#LeadAddress_street_address_1").val());
            $("#LeadSiteAddress_site_street2").val($("#LeadAddress_street_address_2").val());
            $("#LeadSiteAddress_town").val($("#LeadAddress_town").val());
            $("#LeadSiteAddress_site_town").val($("#LeadAddress_town").val());
            $("#LeadSiteAddress_site_country").val($("#LeadAddress_country").val());
            $("#LeadSiteAddress_site_post_code").val($("#LeadAddress_business_postcode").val());
        }
    });

</script>

