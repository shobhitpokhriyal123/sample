<section id="manage_products">
    <div class="container-fluid">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <strong>Manage Customers</strong>
                    </div>
                    <?php
                    if (Yii::app()->user->isAdmin) {
                        echo CHtml::link('New Customer', array('/admin/leadContacts/addCustomer'), array("class" => 'btn btn-success'));
                    }
                    ?>
                </div>
                <div class="card-body">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <?php
                    $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => 'lead-Businessx-grid',
                        'dataProvider' => $model->search(1),
                        'afterAjaxUpdate' => 'function(id,options){ paginationControls(); }',
                        //  'filter' => $model,
                        'columns' => array(
                            //  'id',

                            'agency_name' => array(
                                'name' => 'agency_name',
                                'filter' => false,
                                'value' => 'LeadBusiness::getAgencyName($data->lead_id)'
                            ),
                            'agent_id' => array(
                                'name' => 'agent_id',
                                'filter' => false,
                                'value' => '$data->lead->agent->username'
                            ),
//                            'password_plain' => array(
//                                'name' => 'password_plain',
//                                'header' => 'Password',
//                                'filter' => false,
//                                'value' => '$data->lead->user->password_plain'
//                            ),
                            'business_name' => array(
                                'filter' => false, //CHtml::listData(Users::getAllSuppliers(), 'id', 'fullname'), 'type' => 'html',
                                'name' => 'business_name',
                            //  'header' => 'No of site',
                            // 'value' => '$data->id)'
                            ),
                            'corporate_sme' => array(
                                'name' => 'corporate_sme',
                                'value' => 'LeadElcMeter::getCount($data->lead->id)'
                            ),
                            'utility' => array(
                                'name' => 'utility',
                                'value' => 'LeadElcMeter::getCount($data->lead->id)'
                            ),
                            'site_id' => array(
                                'filter' => false, //CHtml::listData(Users::getAllSuppliers(), 'id', 'fullname'), 'type' => 'html',
                                'name' => 'site_id',
                                'header' => 'No of site',
                                'value' => 'LeadGasMeter::getCount($data->lead_id,1)'
                            ),
                            'gas_meter' => array(
                                'name' => 'gas_meter',
                                'header' => 'No Of gas Meters',
                                'value' => 'LeadGasMeter::getCount($data->lead_id)'
                            ),
                            'electric_meter' => array(
                                'name' => 'electric_meter',
                                'header' => 'No Of Elec Meters',
                                'value' => 'LeadElcMeter::getCount($data->lead_id)'
                            ),
                            'consumption' => array(
                                'name' => 'consumption',
                                'value' => 'LeadContacts::getTotalConsumtion($data->lead_id)'
                            ),
//                            
//                            'id',
//                            //  'create_user_id',
//                            //  'title',
//                            'title' => array(
//                                'name' => 'title',
//                                'type' => 'html',
//                                'value' => 'CHtml::link($data->title,array("/admin/leadContacts/view","id"=>$data->id))'
//                            ),
//                            'first_name',
//                            'sur_name',
//                            'position',
//                            'email',
//                            'landline',
//                            'mobile',
//                            'gasmeter',
//                            'elecmeter',
                            'status' => array(
                                'header' => 'Status',
                                'filter' => false,
                                'type' => 'raw',
                                'value' => 'LeadStatus::getStatus($data->lead->status)',
                            ),
                            array(
                                'class' => 'bootstrap.widgets.TbButtonColumn',
                                'template' => '{view}{update}{delete}{tenders}{contract}',
                                'header' => 'Actions',
                                'buttons' => array(
                                    'update' => array(
                                        'label' => '<button class="btn btn-primary">Update</button>',
                                        'url' => 'Yii::app()->createAbsoluteUrl("admin/LeadContacts/addCustomer", array("id"=>$data->id, "update"=>1))',
                                        'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Update'),
                                    ),
                                    'tenders' => array(
                                        'label' => '<button class="btn btn-primary">Tenders</button>',
                                        'url' => 'Yii::app()->createAbsoluteUrl("admin/LeadTender/admin", array("LeadTender[business_name]"=>$data->business_name))',
                                        'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Tenders'),
                                    ),
                                     'contract' => array(
                                        'label' => '<button class="btn btn-primary">Contracts</button>',
                                        'url' => 'Yii::app()->createAbsoluteUrl("admin/leadContacts/quotes", array("cid"=>$data->lead->user_id))',
                                        'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Contracts'),
                                    ),
                                   
                                ),
                            ),
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>