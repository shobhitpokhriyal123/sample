<section id="manage_products">
    <div class="container-fluid">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <strong>Manage Lead Details</strong>
                    </div>
                       <?php
                    if (Yii::app()->user->isAdmin || Yii::app()->user->isAgency || Yii::app()->user->isUser) {
                        echo CHtml::link('New Lead', array('/admin/leadContacts/create'), array("class" => 'btn btn-success'));
                    }
                    ?>
                </div>
                <div class="card-body">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <?php
                   

                    $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => 'lead-Businessx-grid',
                        'dataProvider' => $model->search(),
                        'afterAjaxUpdate' => 'function(id,options){ paginationControls(); }',
                        //  'filter' => $model,
                        'columns' => array(
                            //  'id',
                           
                            'agency_name' => array(
                                'name' => 'agency_name',
                                'filter' => false,
                                'value' => 'LeadBusiness::getAgencyName($data->lead_id)'
                            ),
                             'agent_id'=>array(
                                'name' => 'agent_id',
                                'filter'=>false,
                                'value' => '$data->lead->agent->username'
                            ),
//                            'password_plain' => array(
//                                'name' => 'password_plain',
//                                'header' => 'Password',
//                                'filter' => false,
//                                'value' => '$data->lead->user->password_plain'
//                            ),
                            'business_name' => array(
                                'filter' => false, //CHtml::listData(Users::getAllSuppliers(), 'id', 'fullname'), 'type' => 'html',
                                'name' => 'business_name',
                            //  'header' => 'No of site',
                            // 'value' => '$data->id)'
                            ),
                            'corporate_sme' => array(
                                'name' => 'corporate_sme',
                                'value' => 'LeadElcMeter::getCount($data->lead->id)'
                            ),
                            'utility' => array(
                                'name' => 'utility',
                                'value' => 'LeadElcMeter::getCount($data->lead->id)'
                            ),
                            'site_id' => array(
                                'filter' => false, //CHtml::listData(Users::getAllSuppliers(), 'id', 'fullname'), 'type' => 'html',
                                'name' => 'site_id',
                                'header' => 'No of site',
                                'value' => 'LeadGasMeter::getCount($data->lead_id,1)'
                            ),
                            'gas_meter' => array(
                                'name' => 'gas_meter',
                                'header' => 'No Of gas Meters',
                                'value' => 'LeadGasMeter::getCount($data->lead_id)'
                            ),
                            'electric_meter' => array(
                                'name' => 'electric_meter',
                                'header' => 'No Of Elec Meters',
                                'value' => 'LeadElcMeter::getCount($data->lead_id)'
                            ),
                            'consumption' => array(
                                'name' => 'consumption',
                                'value' => 'LeadContacts::getTotalConsumtion($data->lead_id)'
                            ),
//                            
//                            'id',
//                            //  'create_user_id',
//                            //  'title',
//                            'title' => array(
//                                'name' => 'title',
//                                'type' => 'html',
//                                'value' => 'CHtml::link($data->title,array("/admin/leadContacts/view","id"=>$data->id))'
//                            ),
//                            'first_name',
//                            'sur_name',
//                            'position',
//                            'email',
//                            'landline',
//                            'mobile',
//                            'gasmeter',
//                            'elecmeter',
                            'status' => array(
                                'header' => 'Status',
                                'filter' => false,
                                'type' => 'raw',
                                'value' => 'LeadStatus::getStatus($data->lead->status)',
                            ),
                            array(
                                'class' => 'bootstrap.widgets.TbButtonColumn',
                                'template' => '{view}{update}{delete}',
                                'header' => 'Actions',
                                'buttons' => array(
                                    'update' => array(
                                        'label' => '<button class="btn btn-primary">Update</button>',
                                        'url' => 'Yii::app()->createAbsoluteUrl("admin/LeadContacts/create", array("id"=>$data->id, "update"=>1))',
                                        'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Update'),
                                    //  'imageUrl' => false,
//                                        'click' => "function(e){
//                                            var url = $(this).attr('href');
//                                            e.preventDefault();
//                                            $('#myModalupdate').modal();
//                                            $('#update_lead').load(url);  
//                                            return false;
//                                         }",
                                    ),
                                ),
                            ),
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>

<div id="myModalupdate" class="modal fade" role="dialog">
    <div class="modal-dialog"  style="width: 80%; ">
        <div class="modal-content" id="update_lead">



        </div>
    </div>
</div>

<script>
    getAgencyId($("#LeadContacts_agency_name").val());
    function getAgencyId(id) {
        //  var id = obj.val();
        var siteagency = '<?php echo Yii::app()->createAbsoluteUrl("/admin/leadContacts/getAgencyId") ?>/id/' + id;
        $.ajax({
            url: siteagency,
            type: 'POST',
            success: function (res) {
                $("#LeadContacts_agency_id").val(res);
            }
        });

    }
</script>