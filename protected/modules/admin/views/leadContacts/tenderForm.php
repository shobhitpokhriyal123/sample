
<div class="card-header">
    <div class="card-title">
        <strong>Meter Overview</strong>
    </div>
    <?php
    if (isset($_GET['id'])) {
        $cuid = $_GET['id'];
    } else {
        $cuid = 0;
    }
//    if($l_tender->id){
//    echo CHtml::link('Send Input Quote', array('/admin/leadContacts/sendQuote', "tender_id" => $l_tender->id, "cid" => $c_id), array("class" => 'btn btn-success right', 'id' => 'send_quote'));
//    } ?>
</div>
<div class="col-md-12" >
    <?php
    if($l_tender->id){
        $tender_files =  LeadTenderUpload::model()->findByAttributes(array('tender_id'=>$l_tender->id));
    }else{
    $tender_files = new LeadTenderUpload;
    }
    if ($l_tender->id) {
        $meter_ids = unserialize($l_tender->meter_id);
        $class = "Lead" . $meter_type . "Meter";
        $criteria = new CdbCriteria;
        $criteria->addInCondition('id', $meter_ids);
        $meters = $class::model()->findAll($criteria);
        ?>
        <table class="table table-striped table-condensed">
            <tr>
                <th>Select</th>
                <th>Meter Name</th>
                <th>Site</th>
                <th>Business Name</th>
                <th>MPR/MPAN</th>
                <th>Contract End Date</th>
                <th>Consumption</th>
            </tr>
            <?php
            if (!empty($meters)) {
                foreach ($meters as $meter) {
                    ?>
                    <tr>
                        <td><input class="roles" checked="checked" type="checkbox" name="meter_id[]" value="<?php echo $meter->id ?>" ></td>
                        <td><?php echo $meter->meter_name; ?></td>
                        <td><?php echo $meter->site->site_name; ?></td>
                        <td><?php //echo $business;       ?></td>
                        <td><?php echo ($meter_type == "Gas") ? $meter->mpr : $meter->mpan; ?></td>
                        <td><?php echo $meter->contract_end_date; ?></td>
                        <td><?php echo ($meter_type == "Gas") ? $meter->total_aq : $meter->total_eac; ?></td>
                    </tr>
                    <?php
                }
            } else {
                ?>
                <tr>
                    <td colspan="7">No Meter found for business...!</td>
                </tr>
            <?php } ?>
        </table> 
    <?php }
    ?>
</div>


<div class="card-header">
    <div class="card-title">
        <strong>Customer Tender Details </strong>
    </div>
</div>
<?php
if ($l_tender) {
    $l_tender->supplier_ids = unserialize($l_tender->supplier_ids);
}
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'customer-tender-form',
    'action' => ($l_tender->id) ? $this->createAbsoluteUrl('leadTender/update',array("id"=>$l_tender->id )) : $this->createAbsoluteUrl('leadTender/update', array("id" => $l_tender->id)),
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        )
);
?>
<div class="col-md-12" id="tender_part">
    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <div class="col-md-6">
        <?php
        if (!empty($meters)) {
            foreach ($meters as $meter) {
                ?>
                <input class="roles"  type="hidden" name="meter_id[]" value="<?php echo $meter->id ?>" >
            <?php }
        }
        ?>
<?php echo $form->textFieldRow($l_tender, 'contract_duration', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>

    <div class="col-md-6">
        <?php echo $form->label($l_tender, "supplier_ids"); ?>
        <?php
        $ab = CHtml::listData(Users::model()->findAllByAttributes(array('role_id' => 2)), 'id', 'fullname');
        ?>

        <?php echo CHtml::activeListBox($l_tender, 'supplier_ids', $ab, array('class' => 'chosen form-control', 'multiple' => true, 'data-placeholder' => 'Select')) ?>
        <?php
        $this->widget('ext.chosen.EChosenWidget', array(
            'selector' => '.chosen',
        ));
        ?>
<?php echo $form->error($l_tender, "supplier_ids"); ?>
    </div>

    <div class="col-md-6">


        <?php
        echo $form->hiddenField($l_tender, 'meter_type', array('value' => $meter_type));
        echo $form->textFieldRow($l_tender, 'bespoke_duration', array('class' => 'span5', 'maxlength' => 255));
        ?>
    </div>

    <div class="col-md-6">
<?php echo $form->textFieldRow($l_tender, 'commission_discount', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>

    <div class="col-md-6">

        <?php echo $form->label($l_tender, 'tender_return_date'); ?>
        <?php
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model' => $l_tender,
            'attribute' => 'tender_return_date',
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                'changeMonth' => true,
                'changeYear' => true,
            ),
        ));
        ?>

        <?php echo $form->error($l_tender, 'tender_return_date'); ?>

<?php echo $form->hiddenField($l_tender, 'customer_id', array("value" => $c_id)); ?>
    </div>

    <div class="col-md-6">
<?php echo $form->textFieldRow($l_tender, 'business_name', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>

    <div class="col-md-6">
<?php echo $form->textFieldRow($l_tender, 'com_reg', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>

    <div class="col-md-6">
        <?php
        echo $form->dropdownListRow($l_tender, 'commodity_type', LeadContacts::getCommodityType(), array('class' => 'span5', 'maxlength' => 255));
        ?>

    </div>

    <!--        <div class="col-md-6">
<?php //echo $form->textFieldRow($l_tender, 'site_no', array('class' => 'span5', 'maxlength' => 255));      ?>
            </div>-->


    <div class="col-md-6">
<?php echo $form->textFieldRow($l_tender, 'uplift', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>

    <div class="col-md-6">
        <?php
        echo $form->dropdownListRow($l_tender, 'term_length', LeadContacts::getTermLength(), array('class' => 'span5', 'maxlength' => 255));
        ?>

    </div>
    <div class="col-md-6">
<?php echo $form->textFieldRow($l_tender, 'term_length', array('class' => 'span5', 'maxlength' => 255)); ?>

    </div>

    <div class="col-md-6">
<?php echo $form->textFieldRow($l_tender, 'fixed_meter', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>

    <div class="col-md-6">

        <?php
        echo $form->dropdownListRow($l_tender, 'contract_type', CHtml::listData(ContractType::model()->findAll(), 'id', 'title'));
        ?>

    </div>

    <div class="col-md-6">
        <?php
        echo $form->dropdownListRow($l_tender, 'payment', PaymentType::getPayment());
        ?>

    </div>


    <div class="col-md-6">
        <?php
        echo $form->dropdownListRow($l_tender, 'energy', ContractType::getEnergy(), array());
        ?>

    </div>

    <div class="col-md-6">
<?php echo $form->textFieldRow($l_tender, 'supplier_pref', array('size' => 20, 'maxlength' => 20)); ?>
    </div>

    <div class="col-md-6">
<?php echo $form->textAreaRow($l_tender, 'comments', array('rows' => 6, 'cols' => 50, "class" => "form-control")); ?>
    </div>
    <hr>
    <div class="col-md-12">
        <div class="card-header">
            <div class="card-title">
                <strong>Upload Tender Files  </strong>
            </div>
        </div>
        <?php echo $form->fileFieldRow($tender_files, 'document', array( "class" => "form-control")); ?>

        <?php
       // echo $form->label($tender_files, 'document');
        ?>
        <?php
//        $this->widget('CMultiFileUpload', array(
//            'model' => $tender_files,
//            'attribute' => 'document',
//            'accept' => 'xsl|csv|xsls|jpg|jpeg|png|pdf',
//            'options' => array(
//            // 'onFileSelect'=>'function(e, v, m){ alert("onFileSelect - "+v) }',
//            // 'afterFileSelect'=>'function(e, v, m){ alert("afterFileSelect - "+v) }',
//            // 'onFileAppend'=>'function(e, v, m){ alert("onFileAppend - "+v) }',
//            // 'afterFileAppend'=>'function(e, v, m){ alert("afterFileAppend - "+v) }',
//            // 'onFileRemove'=>'function(e, v, m){ alert("onFileRemove - "+v) }',
//            // 'afterFileRemove'=>'function(e, v, m){ alert("afterFileRemove - "+v) }',
//            ),
//            'denied' => 'File type is not allowed',
//            'max' => 10, // max 10 files
//        ));
        ?>
        <?php
      //  echo $form->error($tender_files, 'document');
        ?>

    </div>

    <!--	<div class="row buttons">
<?php //echo CHtml::submitButton($l_tender->isNewRecord ? 'Create' : 'Save');       ?>
            </div>-->
    <div class="form-actions col-lg-12">
        <br>
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type' => 'primary',
            'label' => $l_tender->isNewRecord ? 'Create' : 'Save',
        ));
        ?>

    </div>
</div>
<?php $this->endWidget(); ?>
<style>
    .chosen-container{
        width: 100%;
        height: 37px;
    }
</style>
