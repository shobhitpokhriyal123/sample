<?php
$this->breadcrumbs=array(
	'Lead Contacts'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List LeadContacts','url'=>array('index')),
	array('label'=>'Create LeadContacts','url'=>array('create')),
	array('label'=>'View LeadContacts','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage LeadContacts','url'=>array('admin')),
);
?>

<h1>Update LeadContacts <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>