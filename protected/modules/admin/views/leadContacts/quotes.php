<section id="manage_supliers">
    <div class="container-fluid">
        <div class="row">

            <div class="card">
                <div class="card-header">
                    <div class="card-title text-primary">
                        <strong>Input Quote</strong>
                    </div>
                    <?php
                    if (Yii::app()->user->isAdmin) {
                        echo CHtml::link('New Customer', array('/admin/leadContacts/addCustomer'), array("class" => 'btn btn-success'));
                        echo CHtml::link('Customers', array('/admin/leadContacts/customers'), array("class" => 'btn btn-success'));
                    }
                    ?>
                </div>
                <div class="card-body .table-responsive">

                    <?php
                    $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => 'lead-quote-grid',
                        'dataProvider' => $model->search($cid),
                        'afterAjaxUpdate' => 'function(id,options){ paginationControls(); }',
                        // 'filter' => $model,
                        'columns' => array(
                            // 'id',
                            'customer_id' => array(
                                'filter' => false,
                                'name' => 'customer_id',
                                'value' => '($data->customer_id && $data->customer ) ? $data->customer->first_name : "N/A"'
                            ),
                            'site_id' => array(
                                'name' => 'site_id',
                                'header' => 'Sites',
                                'filter' => false,
                                'value' => 'LeadQuote::getSiteName($data->meter_ids,$data->meter_type )'
                            ),
                            'supplier_id' => array(
                                'filter' => false,
                                'name' => 'supplier_id',
                                'value' => '$data->user->username'
                            ),
                            'meter_ids' => array(
                                'filter' => false,
                                'name' => 'meter_ids',
                                'value' => 'LeadQuote::getSiteName($data->meter_ids,$data->meter_type, 1)'
                            ),
                            'product',
                            'min_volume',
                            'max_volume',
                            'credit_position',
////                            'supply_contract' => array(
////                               'type' => 'raw',
////                                'filter' => false,
////                                'name' => 'supply_contract',
////                                'value' => 'CHtml::link(Yii::app()->createAbsoluteUrl("admin/product/download/file/" . $data->supply_contract)',
//                            //       <a class="btn btn-primary" href="<?php echo Yii::app()->createAbsoluteUrl('admin/product/download/file/' . $model->csv_file)>
////                            'offer_type',
                            'consumption',
                            'unit_rate' => array(
                                'name' => 'unit_rate',
                                'filter' => false,
                                'value' => '$data->getUnit_rate()'
                            ),
                            'standing_charge',
                            'created' => array(
                                'name' => 'created',
                                'value' => '$data->created',
                                'header' => 'Quote date'
                            ),
                            'updated' => array(
                                'name' => 'updated',
                                'value' => '$data->updated',
                                'header' => 'Accepted Date'
                            ),
                            'quote_status' => array(
                                'name' => 'quote_status',
                                'value' => '$data->getQuoteStatus()',
                                'type' => 'raw',
                                'header' => 'Quote Status'
                            ),
                            'supply_contract' => array(
                                'name' => 'supply_contract',
                                'filter' => false,
                                'type' => 'raw',
                                'value' => '$data->getDownload()'
                            ),
//                            'created',
                            array(
                                'class' => 'bootstrap.widgets.TbButtonColumn',
                                'template' => '{upload}{viewd}',
                                'buttons' => array(
                                    'upload' => array(
                                        'label' => '<button class="btn btn-primary fa fa-upload">Upload Signed Contract</button>',
                                        'url' => 'Yii::app()->createUrl("/admin/leadQuote/upload", array("id"=>$data->id))',
                                        'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Upload Signed Contract'),
                                        'click' => "function(e){
                                            e.preventDefault();
                                            var url = $(this).attr('href');
                                            $.ajax({
                                              type: 'POST',
                                              url: url,
                                              success: function(res){
                                                $('#myModal').modal();
                                                $('#form_').html(res);
                                                },
                                             });
                                         }",
                                    ),
                                    'viewd' => array(
                                        'label' => '<button class="btn btn-primary fa fa-eye">View Details</button>',
                                        'url' => 'Yii::app()->createUrl("/admin/leadContacts/updateQuote", array("id"=>$data->id))',
                                        'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'View Contract detail'),
                                        'click' => "function(e){
                                            e.preventDefault();
                                            var url = $(this).attr('href');
                                            $.ajax({
                                              type: 'POST',
                                              url: url,
                                              success: function(res){
                                                $('#myModal1').modal();
                                                $('#view_').html(res);
                                                },
                                             });
                                         }",
                                    ),
                                ),
                            ),
                        ),
                    ));
                    ?>

                </div>
            </div>
        </div>
    </div>

</section>

<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog"  style="width:90%">

        <!-- Modal content-->
        <div class="modal-content" id="form_">

        </div>

    </div>
</div>
<div id="myModal1" class="modal fade" role="dialog">
    <div class="modal-dialog"  style="width:90%">

        <!-- Modal content-->
        <div class="modal-content" id="view_">

        </div>

    </div>
</div>

<script>
    $(".accept").click(function (e) {
        e.preventDefault();
        var url = $(this).attr("href");
        $.ajax({
            url: url,
            type: 'POST',
            success: function (res) {
                if (res == 1) {
                    $.fn.yiiGridView.update('lead-quote-grid');
                }
                return false;
            }
        });

    });
</script>