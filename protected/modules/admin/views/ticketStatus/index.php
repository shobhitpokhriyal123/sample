<?php
$this->breadcrumbs=array(
	'Ticket Statuses',
);

$this->menu=array(
	array('label'=>'Create TicketStatus','url'=>array('create')),
	array('label'=>'Manage TicketStatus','url'=>array('admin')),
);
?>

<h1>Ticket Statuses</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
