<?php
$this->breadcrumbs=array(
	'Lead Gas Meters'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List LeadGasMeter','url'=>array('index')),
	array('label'=>'Create LeadGasMeter','url'=>array('create')),
	array('label'=>'View LeadGasMeter','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage LeadGasMeter','url'=>array('admin')),
);
?>

<h1>Update LeadGasMeter <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>