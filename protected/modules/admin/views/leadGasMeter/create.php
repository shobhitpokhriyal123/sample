<?php
$this->breadcrumbs=array(
	'Lead Gas Meters'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List LeadGasMeter','url'=>array('index')),
	array('label'=>'Manage LeadGasMeter','url'=>array('admin')),
);
?>

<h1>Create LeadGasMeter</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>