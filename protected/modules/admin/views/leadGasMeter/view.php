<?php
$this->breadcrumbs=array(
	'Lead Gas Meters'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List LeadGasMeter','url'=>array('index')),
	array('label'=>'Create LeadGasMeter','url'=>array('create')),
	array('label'=>'Update LeadGasMeter','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete LeadGasMeter','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage LeadGasMeter','url'=>array('admin')),
);
?>

<h1>View LeadGasMeter #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'lead_id',
		'create_user_id',
		'meter_name',
		'mpr',
		'total_aq',
		'current_supplier',
		'contract_end_date',
		'meter_status',
		'meter_pricing',
		'created',
	),
)); ?>
