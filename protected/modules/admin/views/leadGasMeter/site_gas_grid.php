<form id="gas_meter_frm" name="gas_meter_frm" method="post"> 
    <?php
    $this->widget('bootstrap.widgets.TbGridView', array(
        'id' => 'lead-gas-meter-grid',
        'dataProvider' => $model->searchGrid($lead_id),
        'afterAjaxUpdate' => 'function(id,options){ paginationControls(); }',
        'filter' => $model,
        'columns' => array(
            'site_id' => array(
                'name' => 'site_id',
                'value' => '$data->site->site_name',
                'filter' => false,
            // 'footer' => LeadGasMeter::getSiteDropdown()
            ),
            'current_supplier' => array(
                'type' => 'raw',
                'filter' => false, //CHtml::listData(Users::getAllSuppliers(), 'id', 'fullname'), 'type' => 'html',
                'name' => 'current_supplier',
                'value' => 'CHtml::image(Yii::app()->createAbsoluteUrl("admin/users/thumbnail/file/".$data->supplier->image))',
            //  'footer' => LeadGasMeter::getSupplierDropdown()
            ),
            'corporate_sme' => array(
                'type' => 'raw',
                'filter' => false, //CHtml::listData(Users::getAllSuppliers(), 'id', 'fullname'), 'type' => 'html',
                'name' => 'corporate_sme',
                'value' => 'Product::getCorporateSME($data->corporate_sme)',
            // 'footer' => LeadGasMeter::getCorporateDropdown()
            ),
            'utility' => array(
                'type' => 'raw',
                'filter' => false, //CHtml::listData(Users::getAllSuppliers(), 'id', 'fullname'), 'type' => 'html',
                'name' => 'utility',
                'value' => 'Product::getUtilites($data->utility)',
            //'footer' => LeadGasMeter::getUtilityDropdown()
            ),
            'meter_name' => array(
                'filter' => false,
                'name' => 'meter_name',
            // 'footer' => '<input id="LeadGasMeter_meter_name_" class="span5 form-control" maxlength="255" name="LeadGasMeter[meter_name]" type="text">'
            ),
            'mpr' => array(
                'filter' => false,
                'name' => 'mpr',
            //'footer' => '<input id="LeadGasMeter_mpr_" class="span5 form-control" maxlength="255" name="LeadGasMeter[mpr]" type="text">'
            ),
            'total_aq' => array(
                'name' => 'total_aq',
                'filter' => false,
            // 'footer' => '<input id="LeadGasMeter_total_aq_" class="span5 form-control" maxlength="255" name="LeadGasMeter[total_aq]" type="text">'
            ),
            'contract_end_date' => array(
                'name' => 'contract_end_date',
                'filter' => false,
            //   'footer' => '<input id="LeadGasMeter_contract_end_date_" class="form-control hasDatepicker" name="LeadGasMeter[contract_end_date]" type="text">'
            ),
            'document' => array(
                'filter' => false,
                'name' => 'document',
                'value' => '($data->document) ? CHtml::link("Download",Yii::app()->createAbsoluteUrl("admin/users/thumbnail/file/".$data->document)) : "N/A"',
                'type' => 'raw'
            ),
            'meter_status' => array(
                'name' => 'meter_status',
                'value' => 'MeterStatus::getStatus($data->meter_status)',
                'filter' => false,
            //   'footer' => LeadGasMeter::getStatusDropdown()
            ),
            //  'meter_pricing',
            // 'created',
            array(
                'class' => 'bootstrap.widgets.TbButtonColumn',
                'template' => '{update}{deleteMeter}',
                'header' => 'Actions',
                'footer' => '<button class="btn btn-success"  id="gasmeterSubmit">Add</button>',
                'buttons' => array(
                    'update' => array(
                        'label' => '<button class="btn btn-primary">Update</button>',
                        'url' => 'Yii::app()->createAbsoluteUrl("admin/leadGasMeter/meterupdate", array("id"=>$data->id))',
                        'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Update'),
                        'imageUrl' => false,
                        'click' => "function(e){
                               e.preventDefault();
                               var url = $(this).attr('href');
                               $.ajax({
                                 type: 'POST',
                                 url: url,
                                 success: function(res){
                                 res = $.parseJSON(res);
                              
                                for (var property in res) {
                                        if (res.hasOwnProperty(property)) { 
                                        
                                         $('#LeadGasMeter_'+property).val(res[property]);
                                    }
                                }
                                    var current = $('#lead-gas-meter-form').attr('action');
                                    $('#lead-gas-meter-form').attr('action',current+'&id='+res.id);
                                 },
                                });
                                
                           
                            }",
                    ),
                    'deleteMeter' => array(
                        'label' => '<i class="fa fa-remove"></i>',
                        'url' => 'Yii::app()->createAbsoluteUrl("admin/leadGasMeter/delete", array("id"=>$data->id))',
                        'options' => array('class'=>'deleteMeter','rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Delete'),
                        'imageUrl' => false,
                        'click' => "function(e){
                               e.preventDefault();
                               if(!confirm('Are you sure you wants to delete..?'))
                                  return false;
                               var url = $(this).attr('href');
                               $.ajax({
                                 type: 'POST',
                                 url: url,
                                 success: function(res){
                                    $.fn.yiiGridView.update('lead-gas-meter-grid');
                                    return false;
                                 },
                                });
                                
                                return false;
                            }",
                    ),
                ),
            ),
        ),
    ));
    ?>

</form>
<script>
//    getUtilities($("#CustomerSiteMeter_corporate_sme").val());
//    function getUtilities(id) {
//        var url = '<?php //echo Yii::app()->createAbsoluteUrl("admin/product/getUtilities/id")           ?>/' + id;
//        $("#CustomerSiteMeter_utility").load(url);
//        // gasParms();
//    }

    $("#gasmeterSubmit").click(function (e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: $('#lead-gas-meter-form').offset().top
        }, 500);
        $('#LeadGasMeter_site_id').focus();
        return false;
//        var gas_url = '<?php //echo Yii::app()->createAbsoluteUrl("admin/leadGasMeter/create")         ?>';
//        var data = $("#gas_meter_frm").serialize();
//        $.ajax({
//            url: gas_url,
//            type: "POST",
//            data: data,
//            success: function (res) {
//                $.fn.yiiGridView.update("lead-gas-meter-grid");
//
//            }
//        });
    });
    paginationControls();
</script>