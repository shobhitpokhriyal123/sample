<?php
$this->breadcrumbs=array(
	'Lead Gas Meters',
);

$this->menu=array(
	array('label'=>'Create LeadGasMeter','url'=>array('create')),
	array('label'=>'Manage LeadGasMeter','url'=>array('admin')),
);
?>

<h1>Lead Gas Meters</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
