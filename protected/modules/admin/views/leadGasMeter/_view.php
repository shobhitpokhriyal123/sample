<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lead_id')); ?>:</b>
	<?php echo CHtml::encode($data->lead_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_user_id')); ?>:</b>
	<?php echo CHtml::encode($data->create_user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meter_name')); ?>:</b>
	<?php echo CHtml::encode($data->meter_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mpr')); ?>:</b>
	<?php echo CHtml::encode($data->mpr); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('total_aq')); ?>:</b>
	<?php echo CHtml::encode($data->total_aq); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('current_supplier')); ?>:</b>
	<?php echo CHtml::encode($data->current_supplier); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('contract_end_date')); ?>:</b>
	<?php echo CHtml::encode($data->contract_end_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meter_status')); ?>:</b>
	<?php echo CHtml::encode($data->meter_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meter_pricing')); ?>:</b>
	<?php echo CHtml::encode($data->meter_pricing); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />

	*/ ?>

</div>