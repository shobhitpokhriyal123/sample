<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'news-form',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
?>
<div class="row">
    <p class="help-block">Fields with <span class="required">*</span> are required.</p>
    <div class="col-md-6">
        <?php echo $form->dropdownListRow($model, 'supplier_id', CHtml::listData(Users::model()->findAllByAttributes(array('role_id' => 2)), 'id', 'fullname'), array('empty' => '--- Select Supplier ---')); ?>
    </div>
    <div class="col-md-6">

        <?php echo $form->textFieldRow($model, 'title', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>

    <div class="col-md-6">
        <?php echo $form->label($model, "agent_agency"); ?>
        <a href="#" class="btn btn-primary" id="select_all" >Select All</a>
        <a href="#" class="btn btn-primary" id="de_select_all" >Deselect All</a>
        <?php
        $criteria = new CDbCriteria;
        $criteria->addInCondition("role_id", array(3, 4));
        $users = Users::model()->findAll($criteria);
        if ($model->id)
            $model->agent_agency = explode(",", $model->agent_agency);
        echo CHtml::activeListBox($model, 'agent_agency', CHtml::listData($users, "id", "username"), array('class' => 'chosen form-control', 'multiple' => true, 'data-placeholder' => 'Select'))
        ?>
        <?php
        $this->widget('ext.chosen.EChosenWidget', array(
            'selector' => '.chosen',
        ));
        ?>


        <?php echo $form->error($model, "agent_agency"); ?>
    </div>
    <div class="col-md-6">
        <?php echo $form->textFieldRow($model, 'heading', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>

    <div class="col-md-6">
        <?php echo $form->textAreaRow($model, 'description', array('class' => 'form-control', 'rows' => 7)); ?>
    </div>
    <!--    <div class="col-md-6">
    <?php //echo $form->textFieldRow($model, 'is_publish', array('class' => 'span5')); ?>
        </div>-->
    <div class="col-md-6">
        <?php echo $form->fileFieldRow($model, 'image_file', array('class' => 'span5', 'maxlength' => 255)); ?>
    </div>
    <!--    <div class="col-md-6">
    <?php //echo $form->textFieldRow($model, 'active', array('class' => 'span5')); ?>
        </div>
        <div class="col-md-6">
    <?php //echo $form->textFieldRow($model, 'created', array('class' => 'span5')); ?>
        </div>-->
    <div class="col-md-6">
        <div class="form-actions">
            <br><br>
            <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType' => 'submit',
                'type' => 'primary',
                'label' => $model->isNewRecord ? 'Create' : 'Save',
            ));
            ?>
        </div>
    </div>
</div>



<?php $this->endWidget(); ?>
<style>
    .chosen-container{
        width: 100%;
        height: 37px;
    }
</style>
<script>
    // Deselect All
    $('#select_all').click(function () {
        $('#News_agent_agency option').prop('selected', true);
        $('#News_agent_agency').trigger('chosen:updated');
    });

    $('#de_select_all').click(function () {
        $('#News_agent_agency option:selected').removeAttr('selected');
        $('#News_agent_agency').trigger('chosen:updated');

    });


// Select All

</script>


