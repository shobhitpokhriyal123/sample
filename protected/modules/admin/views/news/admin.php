<section id="manage_products">
    <div class="container-fluid">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="card-title">
                        <strong>Manage News</strong>
                    </div>

                    <?php
                    if (Yii::app()->user->isAdmin) {
                        echo CHtml::link('Create News', array('/admin/news/create'), array("class" => 'btn btn-success'));
                    }
                    ?>

                </div>
                <div class="card-body">
                    <?php
                    $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => 'news-grid',
                        'dataProvider' => $model->search(),
                        //   'filter' => $model,
                        'columns' => array(
                            // 'id',
                            'created' => array(
                                'name' => 'created',
                                'header' => 'Date',
                                'value' => 'date("Y-m-d", strtotime($data->created))',
                            ),
                            'title' => array(
                                'name' => 'title',
                                'value' => '$data->getLink()',
                                'type' => 'raw'
                            ),
                            'heading',
                            'description',
                            'supplier_id' => array(
                                'name' => 'supplier_id',
                                'value' => '$data->supplier->username'
                            ),
                            'agent_agency' => array(
                                'name' => 'agent_agency',
                                'value' => 'News::getAgentNames($data->agent_agency)'
                            ),
                            array(
                                'class' => 'bootstrap.widgets.TbButtonColumn',
                                'template' => '{update}{delete}'
                            ),
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(".news_link").click(function (e) {
        e.preventDefault();
        $("#myModalNews").modal();
        var news_url = $(this).attr("href");
        $("#news_content").load(news_url);
        return false;
    });
</script>
<div id="myModalNews" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 90%">

        <!-- Modal content-->
        <div class="modal-content" id="news_content">
            
        </div>

    </div>
</div>