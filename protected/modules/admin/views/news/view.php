<section id="manage_products">
    <div class="container-fluid">

        <div class="row">
            <div class="card">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="card-header">

                    <div class="card-title">
                        <strong><?php echo $model->title; ?></strong>
                    </div>
                </div>
                <div class="card-body">

                    <div class="row">
                        <div class="col-md-10">Published On <?php echo date("Y-m-d", strtotime($model->created)); ?></div>
                        <div class="col-md-2"></div>

                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <?php echo CHtml::image(Yii::app()->createAbsoluteUrl("admin/news/download/file/$model->image_file"), "Image File", array("max-width" => '800px')); ?>
<!--                            <img src='<?php //echo Yii::app()->createAbsoluteUrl("admin/news/download/file/".$model->image_file);    ?>'>-->
                        </div>
                    </div>
                    <div class="card-header">
                        <div class="card-title">
                            <strong><?php echo $model->heading; ?></strong>
                        </div>
                    </div>
                    <div class="row">
                        <?php echo $model->description; ?>
                    </div>
                    <hr>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</section>