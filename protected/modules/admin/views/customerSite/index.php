<?php
$this->breadcrumbs=array(
	'Customer Sites',
);

$this->menu=array(
	array('label'=>'Create CustomerSite','url'=>array('create')),
	array('label'=>'Manage CustomerSite','url'=>array('admin')),
);
?>

<h1>Customer Sites</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
