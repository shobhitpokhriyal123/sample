<section id="manage_productss">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="card-title">
                        <strong>Update Site </strong>
                    </div>

                    <?php
                     if (Yii::app()->user->isAdmin  || Yii::app()->user->isAgency ) {
                        echo CHtml::link('Manage Site for ' . $model->customer->customer_name, array('/admin/customerSite/admin', 'cid' => $model->customer_id), array("class" => 'btn btn-success'));
                        echo CHtml::link('Manage Meters', array('/admin/customerSiteMeter/admin', 'sid' => $model->id), array("class" => 'btn btn-success'));
                        echo CHtml::link('Manage Contract', array('/admin/customerContract/admin', 'sid' => $model->id), array("class" => 'btn btn-success'));
                        echo CHtml::link('Add Contract', array('/admin/customerContract/create', 'sid' => $model->id), array("class" => 'btn btn-success'));
                        echo CHtml::link('Add Meter', array('/admin/customerSiteMeter/create', 'sid' => $model->id), array("class" => 'btn btn-success'));
                    }
                    ?>

                </div>
                <div class="card-body">
                    <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
                </div>
            </div>
        </div>
    </div>
</section>
