<section id="manage_productss">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="card-title">
                        <strong><?php //echo $customer->customer_name   ?>  Sites</strong>
                    </div>

                    <?php
                    if (Yii::app()->user->isAdmin  || Yii::app()->user->isAgency ) {
                        // echo CHtml::link('New Customer', array('/admin/agencyCustomer/create'), array("class" => 'btn btn-success'));
                        echo CHtml::link('Manage Customers', array('/admin/agencyCustomer/admin'), array("class" => 'btn btn-success'));
                        echo CHtml::link('Manage Sites', array('/admin/customerSite/admin'), array("class" => 'btn btn-success'));
                        echo CHtml::link('Manage Quotes', array('/admin/customerQuote/admin'), array("class" => 'btn btn-success'));
                        echo CHtml::link('Manage Meters', array('/admin/customerSiteMeter/admin'), array("class" => 'btn btn-success'));
                        echo CHtml::link('Manage Contracts', array('/admin/customerContract/admin'), array("class" => 'btn btn-success'));
                        echo CHtml::link('Manage Tenders', array('/admin/customerTender/admin'), array("class" => 'btn btn-success'));
                    }
                    ?>

                </div>
                <div class="card-body">
                    <?php
                    $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => 'customer-site-grid',
                        'dataProvider' => $model->search($cid),
                        'filter' => $model,
                        'columns' => array(
                            //'id',
                            'customer_id' => array(
                                'name' => 'customer_id',
                                'type' => 'html',
                                'value' => 'CHtml::link($data->customer->customer_name,array("/admin/agencyCustomer/view","id"=>$data->customer->id))'
                            ),
                            //'create_user_id',
                            'site_name',
                            'site_address1',
                            'site_address2',
                            'site_address3',
                            'postcode',
                            array(
                                'class' => 'bootstrap.widgets.TbButtonColumn',
                                'template' => '{update}{view}{new_meter}{new_contract}',
                                'header' => 'Actions',
                                'buttons' => array(
                                    'new_meter' => array(
                                        'label' => '<button class="btn btn-primary">Create Meter</button>',
                                        'url' => 'Yii::app()->createAbsoluteUrl("admin/customerSiteMeter/create", array("sid"=>$data->id))',
                                        'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Create Site Meter'),
                                        'imageUrl' => false,
                                    ),
                                    'new_contract' => array(
                                        'label' => '<button class="btn btn-primary">Create Contract</button>',
                                        'url' => 'Yii::app()->createAbsoluteUrl("admin/customerContract/create", array("sid"=>$data->id))',
                                        'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Create Contract'),
                                        'imageUrl' => false,
                                    ),
                                ),
                            ),
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>