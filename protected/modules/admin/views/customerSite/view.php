<section id="manage_productss">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="card-title">
                        <strong>View Site : <?php echo $model->site_name ?></strong>
                    </div>

                    <?php
                    if (Yii::app()->user->isAdmin  || Yii::app()->user->isAgency ) {
                        echo CHtml::link('Manage Site for '.$model->customer->customer_name, array('/admin/customerSite/admin', 'cid' => $model->customer_id), array("class" => 'btn btn-success'));
                        echo CHtml::link('Manage Meters', array('/admin/customerSiteMeter/admin', 'sid' => $model->id), array("class" => 'btn btn-success'));
                        echo CHtml::link('Manage Contract', array('/admin/customerContract/admin', 'sid' => $model->id), array("class" => 'btn btn-success'));
                        echo CHtml::link('Add Contract', array('/admin/customerContract/create', 'sid' => $model->id), array("class" => 'btn btn-success'));
                        echo CHtml::link('Add Meter', array('/admin/customerSiteMeter/create', 'sid' => $model->id), array("class" => 'btn btn-success'));
                    }
                    ?>
                </div>
                <div class="card-body">
                    <?php
                    $this->widget('bootstrap.widgets.TbDetailView', array(
                        'data' => $model,
                        'attributes' => array(
                            //    'id',
                            'customer_id' => array(
                                'name' => 'customer_id',
                                'type' => 'html',
                                'value' => CHtml::link($model->customer->customer_name, array('/admin/agencyCustomer/view', 'id' => $model->customer->id))
                            ),
                            //'create_user_id',
                            'site_name',
                            'site_address1' => array(
                                'name' => 'site_address1'
                            ),
                            'site_address2',
                            'site_address3',
                            'postcode',
                            'created',
                        ),
                    ));
                    ?>
                </div>
            </div>

        </div>

    </div>
</section>

