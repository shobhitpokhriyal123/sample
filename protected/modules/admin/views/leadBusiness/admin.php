<form id="business_frm" method="post" name="business_frm">
    <?php 
    $this->widget('bootstrap.widgets.TbGridView', array(
        'id' => 'lead-business-grid',
        'dataProvider' => $model->search(),
        'afterAjaxUpdate' => 'function(id,options){ paginationControls(); }',
        // 'filter' => $model,
        'columns' => array(
            'lead_id' => array(
                'header' => 'Lead',
                'name' => 'lead_id',
                'value' => '$data->lead->first_name',
                'footer' => LeadBusiness::getLeadDropdown()
            ),
            'business_name' => array(
                'name' => 'business_name',
                'footer' => '<input id="LeadBusiness_business_name_" class="span5 form-control" maxlength="255" name="LeadBusiness[business_name]" type="text">'
            ),
            'credit_score' => array(
                'name' => 'credit_score',
                'footer' => '<input id="LeadBusiness_credit_score_" class="span5 form-control" maxlength="255" name="LeadBusiness[credit_score]" type="text">'
            ),
            'partner_channel' => array(
                'name' => 'partner_channel',
                'footer' => '<input id="LeadBusiness_partner_channel_" class="span5 form-control" maxlength="255" name="LeadBusiness[partner_channel]" type="text">'
            ),
            'business_level' => array(
                'name' => 'business_level',
                'footer' => LeadBusiness::getLevelDropdown(),
                'value' => '($data->business_level) ? "Micro" : "Non-Micro"'
            ),
            'charity_no' => array(
                'name' => 'charity_no',
                'footer' => '<input id="LeadBusiness_charity_no_" class="span5 form-control" maxlength="255" name="LeadBusiness[charity_no]" type="text">'
            ),
            'campaign' => array(
                'name' => 'campaign',
                'footer' => '<input id="LeadBusiness_campaign_" class="span5 form-control" maxlength="255" name="LeadBusiness[campaign]" type="text">'
            ),
            'year_month_trading' => array(
                'name' => 'year_month_trading',
                'footer' => '<input id="LeadBusiness_year_month_trading_" class="span5 form-control" maxlength="255" name="LeadBusiness[year_month_trading]" type="text">'
            ),
            'trending_type' => array(
                'name' => 'lead_id',
                'footer' => LeadBusiness::getTrandingDropdown()
            ),
            'business_type' => array(
                'name' => 'business_type',
                'footer' => LeadBusiness::getBusinessDropdown(),
                'value' => '$data->type->title'
            ),
//        'external_reference' => array(
//            'header' => 'Lead',
//            'name' => 'lead_id',
//            'value' => '$data->lead->first_name',
//            'footer' => LeadBusiness::getLeadDropdown()
//        ),

            /* 'client_reference',
              'business_type',
              'lead_source',
              'visibility',
              'created',
             */
            array(
                'class' => 'bootstrap.widgets.TbButtonColumn',
                'footer' => '<button class="btn btn-success" id="business_submit">Add</button>',
                'template' => '{update}',
                'header' => 'Actions',
                'buttons' => array(
                    'update' => array(
                        'label' => '<button class="btn btn-primary">Update</button>',
                        'url' => 'Yii::app()->createAbsoluteUrl("admin/leadBusiness/businessupdate", array("id"=>$data->id))',
                        'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Update'),
                        'imageUrl' => false,
                        'click' => "function(e){
                               e.preventDefault();
                               var url = $(this).attr('href');
                               $.ajax({
                                 type: 'POST',
                                 url: url,
                                 success: function(res){
                                 res = $.parseJSON(res);
                              
                                for (var property in res) {
                                        if (res.hasOwnProperty(property)) { 
                                        
                                         $('#LeadBusiness_'+property).val(res[property]);
                                    }
                                }
                                    var current = $('#lead-business-form').attr('action');
                                    $('#lead-business-form').attr('action',current+'&id='+res.id);
                                 },
                                });
                                
                           
                            }",
                    ),
                ),
            ),
        ),
    ));
    ?>
</form>
<script>
    $(document).ready(function (e) {
        $("#business_submit").click(function (e) {
            e.preventDefault();
            var data = $("#business_frm").serialize();
            console.log(data);
            var business_url = '<?php echo Yii::app()->createAbsoluteUrl("admin/leadBusiness/AddAddress") ?>';
            $.ajax({
                url: business_url,
                data: data,
                type: "POST",
                success: function (res) {
                    $.fn.yiiGridView.update("lead-business-grid");
                }
            });
        });
    });

</script>