<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'lead-business-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'create_user_id',array('class'=>'span5')); ?>

	<?php //echo $form->textFieldRow($model,'lead_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'business_name',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'credit_score',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'partner_channel',array('class'=>'span5','maxlength'=>255)); ?>

	<?php //echo $form->textFieldRow($model,'business_level',array('class'=>'span5','maxlength'=>255));?>

	<?php echo $form->textFieldRow($model,'charity_no',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'campaign',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'year_month_trading',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'trending_type',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'external_reference',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'client_reference',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'business_type',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'lead_source',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'visibility',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'created',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>

      