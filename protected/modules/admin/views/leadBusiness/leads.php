<section id="manage_products">
    <div class="container-fluid">
        <div class="row">
            <div class="card">
                <div class="card-header">

                    <div class="card-title">
                        <strong>Customer Management</strong>
                    </div>
                    <?php
                    if (Yii::app()->user->isAdmin) {
                        //echo CHtml::link('Details', array('/admin/leadContacts/admin'), array("class" => 'btn btn-success'));
                    }
                    ?>

                </div>
                <div class="card-body">
<?php

$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'lead-business-grid',
    'dataProvider' => $model->search(),
    'afterAjaxUpdate' => 'function(id,options){ paginationControls(); }',
    'filter' => $model,
    'columns' => array(
        //'id',
        //'create_user_id',
//        'lead_id' => array(
//            'filter' => false, //CHtml::listData(Users::getAllSuppliers(), 'id', 'fullname'), 'type' => 'html',
//            'header' => 'Lead',
//            'name' => 'lead_id',
//            'value' => '$data->lead->first_name'
//        ),
        'business_name',
        'credit_score',
        'partner_channel',
        /**/
          'business_level',
          'charity_no',
          'campaign',
          'year_month_trading',
          'trending_type',
          'external_reference',
          'client_reference',
          'business_type',
          'lead_source',
          'visibility',
          'created',
       
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{update}',
            'header' => 'Actions',
            'buttons' => array(
                'update' => array(
                    'label' => '<button class="btn btn-primary">Update</button>',
                    'url' => 'Yii::app()->createAbsoluteUrl("admin/leadBusiness/businessupdate", array("id"=>$data->id))',
                    'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Update'),
                    'imageUrl' => false,
                    'click' => "function(e){
                               e.preventDefault();
                               var url = $(this).attr('href');
                               $.ajax({
                                 type: 'POST',
                                 url: url,
                                 success: function(res){
                                 res = $.parseJSON(res);
                              
                                for (var property in res) {
                                        if (res.hasOwnProperty(property)) { 
                                        
                                         $('#LeadBusiness_'+property).val(res[property]);
                                    }
                                }
                                    var current = $('#lead-business-form').attr('action');
                                    $('#lead-business-form').attr('action',current+'&id='+res.id);
                                 },
                                });
                                
                           
                            }",
                ),
            ),
        ),
    ),
));
?>
                </div></div></div></div></section>