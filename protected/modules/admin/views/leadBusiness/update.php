<?php
$this->breadcrumbs=array(
	'Lead Businesses'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List LeadBusiness','url'=>array('index')),
	array('label'=>'Create LeadBusiness','url'=>array('create')),
	array('label'=>'View LeadBusiness','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage LeadBusiness','url'=>array('admin')),
);
?>

<h1>Update LeadBusiness <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>