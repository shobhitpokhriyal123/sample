<?php
$this->breadcrumbs=array(
	'Lead Businesses'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List LeadBusiness','url'=>array('index')),
	array('label'=>'Create LeadBusiness','url'=>array('create')),
	array('label'=>'Update LeadBusiness','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete LeadBusiness','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage LeadBusiness','url'=>array('admin')),
);
?>

<h1>View LeadBusiness #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'create_user_id',
		'lead_id',
		'business_name',
		'credit_score',
		'partner_channel',
		'business_level',
		'charity_no',
		'campaign',
		'year_month_trading',
		'trending_type',
		'external_reference',
		'client_reference',
		'business_type',
		'lead_source',
		'visibility',
		'created',
	),
)); ?>
