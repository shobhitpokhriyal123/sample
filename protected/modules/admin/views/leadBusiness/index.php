<?php
$this->breadcrumbs=array(
	'Lead Businesses',
);

$this->menu=array(
	array('label'=>'Create LeadBusiness','url'=>array('create')),
	array('label'=>'Manage LeadBusiness','url'=>array('admin')),
);
?>

<h1>Lead Businesses</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
