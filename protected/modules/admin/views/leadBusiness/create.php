<?php
$this->breadcrumbs=array(
	'Lead Businesses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List LeadBusiness','url'=>array('index')),
	array('label'=>'Manage LeadBusiness','url'=>array('admin')),
);
?>

<h1>Create LeadBusiness</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>