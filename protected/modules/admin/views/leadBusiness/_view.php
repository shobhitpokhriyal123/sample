<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_user_id')); ?>:</b>
	<?php echo CHtml::encode($data->create_user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lead_id')); ?>:</b>
	<?php echo CHtml::encode($data->lead_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('business_name')); ?>:</b>
	<?php echo CHtml::encode($data->business_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('credit_score')); ?>:</b>
	<?php echo CHtml::encode($data->credit_score); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('partner_channel')); ?>:</b>
	<?php echo CHtml::encode($data->partner_channel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('business_level')); ?>:</b>
	<?php echo CHtml::encode($data->business_level); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('charity_no')); ?>:</b>
	<?php echo CHtml::encode($data->charity_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('campaign')); ?>:</b>
	<?php echo CHtml::encode($data->campaign); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('year_month_trading')); ?>:</b>
	<?php echo CHtml::encode($data->year_month_trading); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('trending_type')); ?>:</b>
	<?php echo CHtml::encode($data->trending_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('external_reference')); ?>:</b>
	<?php echo CHtml::encode($data->external_reference); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('client_reference')); ?>:</b>
	<?php echo CHtml::encode($data->client_reference); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('business_type')); ?>:</b>
	<?php echo CHtml::encode($data->business_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lead_source')); ?>:</b>
	<?php echo CHtml::encode($data->lead_source); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('visibility')); ?>:</b>
	<?php echo CHtml::encode($data->visibility); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />

	*/ ?>

</div>