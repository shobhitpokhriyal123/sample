<section id="manage_meter">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <strong>Manage Agency Permissions</strong>
                    </div>
                    <?php
                    if (Yii::app()->user->isAdmin) {
                        echo CHtml::link('Add Permissions', array('/admin/permissions/create'), array("class" => 'btn btn-success'));
                    }
                    ?>
                </div>
                <div class="card-body">

                    <?php
                    $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => 'permissions-grid',
                        'dataProvider' => $model->search(),
                        'filter' => $model,
                        'columns' => array(
                            'id',
                            'module_id' => array(
                                'name' => 'module_id',
                                'value' => 'PermissionModules::getModules($data->module_id )' //$model->module->title
                            ),
                            'agency_id' => array(
                                'name' => 'agency_id',
                                'value' => '$data->agency->username'
                            ),
                            'created',
                            array(
                                'class' => 'bootstrap.widgets.TbButtonColumn',
                                'template' => '{view}'
                            ),
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
