<section id="manage_meter">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <strong>View Agency Permissions</strong>
                    </div>
                    <?php
                    if (Yii::app()->user->isAdmin) {
                        echo CHtml::link('Manage Permissions', array('/admin/permissions/admin'), array("class" => 'btn btn-success'));
                    }
                    ?>
                </div>
                <div class="card-body">
                    <?php
                    $this->widget('bootstrap.widgets.TbDetailView', array(
                        'data' => $model,
                        'attributes' => array(
                            //  'id',
                            'module_id' => array(
                                'name' => 'module_id',
                                'value' => PermissionModules::getModules($model->module_id ) //$model->module->title
                            ),
                            'agency_id' => array(
                                'name' => 'agency_id',
                                'value' => $model->agency->username
                            ),
                            'created',
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>