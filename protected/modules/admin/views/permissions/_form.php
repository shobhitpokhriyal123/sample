<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'permissions-form',
    'enableClientValidation' => true,
    'enableAjaxValidation' => false,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php
if ($model->module_id) {
    $model->module_id = explode(",", $model->module_id);
}

//echo $form->errorSummary($model); 
?>


<?php echo $form->dropDownListRow($model, 'agency_id', Agency::getAgency(), array("onchange" => 'getPermission($(this).val())')); ?>

<?php echo $form->checkboxListRow($model, 'module_id', CHtml::listData(PermissionModules::model()->findAll(), 'id', 'module_name')); ?>

<?php //echo $form->textFieldRow($model, 'created', array('class' => 'span5'));  ?>

<div class="form-actions">
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type' => 'primary',
        'label' => $model->isNewRecord ? 'Create' : 'Save',
    ));
    ?>
</div>

<?php $this->endWidget(); ?>

<script>
    
    getPermission($("#Permissions_agency_id").val());
    function getPermission(id) {
         $('[id^="Permissions_module_id_"]').each(function() {
             $(this).removeAttr("checked", false);
             
         });
        var url = '<?php echo Yii::app()->createAbsoluteUrl("/admin/permissions/getPermission/id") ?>/' + id;
        
    $.ajax({
            type: "POST",
            url: url,
            success: function (res) {
                var obj = jQuery.parseJSON(res);
                $( obj ).each(function( index , v) {
                    $("input#Permissions_module_id_"+ (v - 1)).prop( "checked" , true);
                });
            
            },
        });
    }
</script>
