<div class="row-fluid">
    <div class="span12 well">
        <?php if (Yii::app()->user->hasFlash('success')): ?>
            <div class="info">
                <?php echo Yii::app()->user->getFlash('success'); ?>
            </div>
        <?php endif; ?>
        <h1>Manage Sets</h1>
        <div class="right">
            <?php
            if (Yii::app()->user->isAdmin) {
                echo CHtml::link('Create Set', array('/admin/sets/create'), array("class" => 'btn btn-success'));
            }
            ?>
        </div>
        <br>




        <h1>Manage Sets</h1>



        <!-- search-form -->

        <?php
        $this->widget('bootstrap.widgets.TbGridView', array(
            'id' => 'sets-grid',
            'dataProvider' => $model->search(),
            'filter' => $model,
            'columns' => array(
                'id',
                'title',
                'status',
             //   'created',
                array(
                    'class' => 'bootstrap.widgets.TbButtonColumn',
                ),
            ),
        ));
        ?>
    </div>
</div>