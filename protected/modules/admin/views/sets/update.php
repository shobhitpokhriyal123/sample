<?php
$this->breadcrumbs=array(
	'Sets'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Sets','url'=>array('index')),
	array('label'=>'Create Sets','url'=>array('create')),
	array('label'=>'View Sets','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Sets','url'=>array('admin')),
);
?>

<h1>Update Sets <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>