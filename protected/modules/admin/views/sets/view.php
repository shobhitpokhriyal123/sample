<div class="row-fluid">
    <div class="span12 well">
        <h1>View Set <?php echo $model->title; ?></h1>
        <div class="right">
            <?php
            if (Yii::app()->user->isAdmin) {
                echo CHtml::link('Add Set', array('/admin/sets/create'), array("class" => 'btn btn-success'));

                echo CHtml::link('Manage Sets', array('/admin/sets/admin'), array("class" => 'btn btn-success'));
            }
            ?>
        </div>
        <br>

        <?php
        $this->widget('bootstrap.widgets.TbDetailView', array(
            'data' => $model,
            'attributes' => array(
                'id',
                'title',
                'status',
                'created',
            ),
        ));
        ?>
    </div>
</div>