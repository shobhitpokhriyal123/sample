<?php
$this->breadcrumbs=array(
	'Sets',
);

$this->menu=array(
	array('label'=>'Create Sets','url'=>array('create')),
	array('label'=>'Manage Sets','url'=>array('admin')),
);
?>

<h1>Sets</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
