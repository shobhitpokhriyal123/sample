<div class="row-fluid">
    <div class="span12 well">

      

        <h1>Create Sets</h1>

        <div class="right">
            <?php
            if (Yii::app()->user->isAdmin) {
                echo CHtml::link('Manage Sets', array('/admin/sets/admin'), array("class" => 'btn btn-success'));
            }
            ?>
        </div>
        <br>



        <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
    </div>
</div>