<?php
$this->breadcrumbs=array(
	'Distributer Maps',
);

$this->menu=array(
	array('label'=>'Create DistributerMap','url'=>array('create')),
	array('label'=>'Manage DistributerMap','url'=>array('admin')),
);
?>

<h1>Distributer Maps</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
