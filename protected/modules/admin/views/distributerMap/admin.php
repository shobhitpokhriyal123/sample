<section id="manage_productss">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="card-title">
                        <strong>Manage distributer Mapping </strong>
                    </div>
                    <?php
                    if (Yii::app()->user->isAdmin) {
                        echo CHtml::link('Add Mapping', array('/admin/distributerMap/create'), array("class" => 'btn btn-success'));
                    }
                    ?>
                </div>
                <div class="card-body">
                    <?php
                    $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => 'distributer-map-grid',
                        'afterAjaxUpdate' => 'function(id,options){ paginationControls(); }',
                        'dataProvider' => $model->search(),
                        'filter' => $model,
                        'columns' => array(
//                            'id',
//                            'dist_network',
//                            'area',
                            'postcode',
                            'dist_id',
                            'ldz',
                            'exit_zone',
//                            'area',
                            'region',
                            'region1',
                            'region2',
                            'region3',
                            'region4',
                            'region5',
                            'region6',
                            'region7',
                            /*
                              'created',
                             */
                            array(
                                'class' => 'bootstrap.widgets.TbButtonColumn',
                                'template' => '{view}{update}'
                            ),
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
