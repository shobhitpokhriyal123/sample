<section id="manage_productss">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="card-title">
                        <strong>View Mapping </strong>
                    </div>

                    <?php
                    if (Yii::app()->user->isAdmin) {
                        echo CHtml::link('Manage Mapping', array('/admin/distributerMap/admin'), array("class" => 'btn btn-success'));
                    }
                    ?>

                </div>
                <div class="card-body">

                    <?php
                    $this->widget('bootstrap.widgets.TbDetailView', array(
                        'data' => $model,
                        'attributes' => array(
                            'id',
                            'dist_network',
                            'exit_zone',
//                            'area',
                             'region1',
                            'region2',
                            'region3',
                            'region4',
                            'region5',
                            'region6',
                            'region7',
                            'dist_id',
                            'ldz',
                            'region',
                            'created',
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>

