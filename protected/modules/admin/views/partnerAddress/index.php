<?php
$this->breadcrumbs=array(
	'Partner Addresses',
);

$this->menu=array(
	array('label'=>'Create PartnerAddress','url'=>array('create')),
	array('label'=>'Manage PartnerAddress','url'=>array('admin')),
);
?>

<h1>Partner Addresses</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
