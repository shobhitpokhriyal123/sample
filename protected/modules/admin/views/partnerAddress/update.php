<?php
$this->breadcrumbs=array(
	'Partner Addresses'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List PartnerAddress','url'=>array('index')),
	array('label'=>'Create PartnerAddress','url'=>array('create')),
	array('label'=>'View PartnerAddress','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage PartnerAddress','url'=>array('admin')),
);
?>

<h1>Update PartnerAddress <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>