<?php
$this->breadcrumbs=array(
	'Partner Addresses'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List PartnerAddress','url'=>array('index')),
	array('label'=>'Create PartnerAddress','url'=>array('create')),
	array('label'=>'Update PartnerAddress','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete PartnerAddress','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PartnerAddress','url'=>array('admin')),
);
?>

<h1>View Customer Details #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		//'user_id',
		//'create_user_id',
		'contract_id',
                'customer_name',
		'home_postcode',
		'home_address_no',
		'home_town',
		'home_building',
		
		'home_street1',
		'home_street2',
		'home_country',
		'years_of_address',
		'dob',
	),
)); ?>
