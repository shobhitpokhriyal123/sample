<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_user_id')); ?>:</b>
	<?php echo CHtml::encode($data->create_user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contract_id')); ?>:</b>
	<?php echo CHtml::encode($data->contract_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('home_postcode')); ?>:</b>
	<?php echo CHtml::encode($data->home_postcode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('customer_name')); ?>:</b>
	<?php echo CHtml::encode($data->customer_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('home_town')); ?>:</b>
	<?php echo CHtml::encode($data->home_town); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('home_building')); ?>:</b>
	<?php echo CHtml::encode($data->home_building); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('home_address_no')); ?>:</b>
	<?php echo CHtml::encode($data->home_address_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('home_street1')); ?>:</b>
	<?php echo CHtml::encode($data->home_street1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('home_street2')); ?>:</b>
	<?php echo CHtml::encode($data->home_street2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('home_country')); ?>:</b>
	<?php echo CHtml::encode($data->home_country); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('years_of_address')); ?>:</b>
	<?php echo CHtml::encode($data->years_of_address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dob')); ?>:</b>
	<?php echo CHtml::encode($data->dob); ?>
	<br />

	*/ ?>

</div>