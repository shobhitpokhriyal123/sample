<?php
$this->breadcrumbs=array(
	'Partner Addresses'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List PartnerAddress','url'=>array('index')),
	array('label'=>'Create PartnerAddress','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('partner-address-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Partner Addresses</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'partner-address-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'user_id',
		'create_user_id',
		'contract_id',
		'home_postcode',
		'customer_name',
		/*
		'home_town',
		'home_building',
		'home_address_no',
		'home_street1',
		'home_street2',
		'home_country',
		'years_of_address',
		'dob',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
