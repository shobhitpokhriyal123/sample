<?php
$this->breadcrumbs=array(
	'Partner Addresses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PartnerAddress','url'=>array('index')),
	array('label'=>'Manage PartnerAddress','url'=>array('admin')),
);
?>

<h1>Create PartnerAddress</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>