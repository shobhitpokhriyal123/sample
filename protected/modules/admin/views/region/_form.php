<?php
    $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
        'id'=>'region-form',
        'enableAjaxValidation'=>false,
    ));
?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'title',array('class'=>'form-control','maxlength'=>255)); ?>
	<br>

	<?php echo $form->textAreaRow($model,'description',array('class' => 'form-control', 'rows'=>6, 'cols'=>50)); ?>

	<?php //echo $form->textFieldRow($model,'status',array('class'=>'span5')); ?>

	<?php //echo $form->textFieldRow($model,'created',array('class'=>'span5')); ?>
    <br>
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
            'htmlOptions' => array('class' => 'longBtn')
		)); ?>
	</div>

<?php $this->endWidget(); ?>
