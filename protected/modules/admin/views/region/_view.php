
<div class="view">

    <div class="panel panel-default">
        <div class="panel-heading ">
            <h3 class="panel-title">
                <strong><?php echo CHtml::encode($data->getAttributeLabel('title')); ?></strong> -
                <?php echo CHtml::encode($data->title); ?>
            </h3>
        </div>
        <div class="panel-body">
            <strong><?php echo CHtml::encode($data->getAttributeLabel('id')); ?></strong> - <?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?> <br>
            <strong><?php echo CHtml::encode($data->getAttributeLabel('description')); ?></strong> - <?php echo CHtml::encode($data->description); ?>
        </div>
        <div class="panel-footer">

            <strong><?php echo CHtml::encode($data->getAttributeLabel('created')); ?></strong> - <?php echo CHtml::encode($data->created); ?>
            <div class="pull-right">
                <strong><?php echo CHtml::encode($data->getAttributeLabel('status')); ?></strong> - <?php echo CHtml::encode($data->status); ?>
            </div>

        </div>
    </div>
</div>
