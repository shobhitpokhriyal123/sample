<section id="region">
    <div class="container">
        <div class="row">
            <?php
                $this->breadcrumbs=array(
                    'Regions',
                );

                $this->menu=array(
                    array('label'=>'Create Region','url'=>array('create')),
                    array('label'=>'Manage Region','url'=>array('admin')),
                );
            ?>
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <strong>Regions</strong>
                    </div>
                </div>
                <div class="card-body">
                    <?php
                        $this->widget('bootstrap.widgets.TbListView',array(
                            'dataProvider'=>$dataProvider,
                            'afterAjaxUpdate' => 'function(id,options){ paginationControls(); }',
                            'itemView'=>'_view',
                        ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
