<section id="update_region">
    <div class="container">
        <div class="row">
            <?php
                $this->breadcrumbs = array(
                    'Regions' => array('index'),
                    $model->title => array('view', 'id' => $model->id),
                    'Update',
                );

                $this->menu = array(
                    array('label' => 'List Region', 'url' => array('index')),
                    array('label' => 'Create Region', 'url' => array('create')),
                    array('label' => 'View Region', 'url' => array('view', 'id' => $model->id)),
                    array('label' => 'Manage Region', 'url' => array('admin')),
                );
            ?>
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <strong>Update Region <?php echo $model->id; ?></strong>
                    </div>
                    <?php
                        if (Yii::app()->user->isAdmin) {
                            echo CHtml::link('Manage Regions', array('/admin/region/admin'), array("class" => 'btn btn-success'));
                        }
                    ?>
                </div>
                <div class="card-body">
                    <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
                </div>
            </div>
        </div>
    </div>
</section>
