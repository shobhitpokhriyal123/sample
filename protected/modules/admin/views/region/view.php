<section id="view_region">
    <div class="container">
        <div class="row">
            <?php
                $this->breadcrumbs = array(
                    'Regions' => array('index'),
                    $model->title,
                );

                $this->menu = array(
                    array('label' => 'List Region', 'url' => array('index')),
                    array('label' => 'Create Region', 'url' => array('create')),
                    array('label' => 'Update Region', 'url' => array('update', 'id' => $model->id)),
                    array('label' => 'Delete Region', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Are you sure you want to delete this item?')),
                    array('label' => 'Manage Region', 'url' => array('admin')),
                );
            ?>
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <strong>View Region <?php echo $model->title; ?></strong>
                    </div>
                    <?php
                        if (Yii::app()->user->isAdmin) {
                            echo CHtml::link('Add Region', array('/admin/region/create'), array("class" => 'btn btn-primary'));

                            echo CHtml::link('Manage Regions', array('/admin/region/admin'), array("class" => 'btn btn-success'));
                        }
                    ?>
                </div>
                <div class="card-body">
                    <?php
                        $this->widget('bootstrap.widgets.TbDetailView', array(
                            'data' => $model,
                            'attributes' => array(
                                'id',
                                'title',
                                'description',
                                'status',
                                'created',
                            ),
                        ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
