<section id="create_region">
    <div class="container">
        <div class="row">
            <?php
                $this->breadcrumbs = array(
                    'Regions' => array('index'),
                    'Create',
                );

                $this->menu = array(
                    array('label' => 'List Region', 'url' => array('index')),
                    array('label' => 'Manage Region', 'url' => array('admin')),
                );
            ?>
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <strong>Create Region</strong>
                    </div>
                    <?php
                        if (Yii::app()->user->isAdmin) {
                            echo CHtml::link('Manage Regions', array('/admin/region/admin'), array("class" => 'btn btn-success'));
                        }
                    ?>
                </div>
                <div class="card-body">
                    <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
                </div>
            </div>
        </div>
    </div>
</section>
