<section id="manage_region">
    <div class="container">
        <div class="row">
            <?php
                $this->breadcrumbs = array(
                    'Regions' => array('index'),
                    'Manage',
                );

                $this->menu = array(
                    array('label' => 'List Region', 'url' => array('index')),
                    array('label' => 'Create Region', 'url' => array('create')),
                );
            ?>
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <strong>Manage Regions</strong>
                    </div>
                    <?php
                        if (Yii::app()->user->isAdmin) {
                            echo CHtml::link('Add Region', array('/admin/region/create'), array("class" => 'btn btn-success'));
                        }
                    ?>
                </div>
                <div class="card-body">
                    <?php //echo CHtml::link('Advanced Search', '#', array('class' => 'search-button btn')); ?>
                    <div class="search-form" style="display:none">
                        <?php
            //            $this->renderPartial('_search', array(
            //                'model' => $model,
            //            ));
                        ?>
                    </div><!-- search-form -->

                    <?php
                        $this->widget('bootstrap.widgets.TbGridView', array(
                            'id' => 'region-grid',
                            'dataProvider' => $model->search(),
                            'afterAjaxUpdate' => 'function(id,options){ paginationControls(); }',
                            'filter' => $model,
                            'columns' => array(
                                'id',
                                'title',
                                'description',
                                'status',
                                'created',
                                array(
                                    'class' => 'bootstrap.widgets.TbButtonColumn',
                                ),
                            ),
                        ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
