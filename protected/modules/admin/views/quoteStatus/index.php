<?php
$this->breadcrumbs=array(
	'Quote Statuses',
);

$this->menu=array(
	array('label'=>'Create QuoteStatus','url'=>array('create')),
	array('label'=>'Manage QuoteStatus','url'=>array('admin')),
);
?>

<h1>Quote Statuses</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
