<?php
$this->breadcrumbs=array(
	'Business Addresses'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List BusinessAddress','url'=>array('index')),
	array('label'=>'Create BusinessAddress','url'=>array('create')),
	array('label'=>'View BusinessAddress','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage BusinessAddress','url'=>array('admin')),
);
?>

<h1>Update BusinessAddress <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>