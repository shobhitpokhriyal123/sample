<?php
$this->breadcrumbs=array(
	'Business Addresses'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List BusinessAddress','url'=>array('index')),
	array('label'=>'Create BusinessAddress','url'=>array('create')),
	array('label'=>'Update BusinessAddress','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete BusinessAddress','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage BusinessAddress','url'=>array('admin')),
);
?>

<h1>View BusinessAddress #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		//'user_id',
		//'create_user_id',
		'is_site_address',
		'home_postcode',
		'customer_name',
		'home_town',
		'home_building',
		'home_address_no',
		'home_street1',
		'home_street2',
		'home_country',
		'years_of_address',
		'dob',
	),
)); ?>
