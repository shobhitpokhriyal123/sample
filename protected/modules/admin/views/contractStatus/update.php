<section>
    <div class="container">
        <div class="row">

            <?php
                $this->breadcrumbs=array(
                    'Contract Statuses'=>array('index'),
                    $model->title=>array('view','id'=>$model->id),
                    'Update',
                );

                $this->menu=array(
                    array('label'=>'List ContractStatus','url'=>array('index')),
                    array('label'=>'Create ContractStatus','url'=>array('create')),
                    array('label'=>'View ContractStatus','url'=>array('view','id'=>$model->id)),
                    array('label'=>'Manage ContractStatus','url'=>array('admin')),
                );
            ?>

            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <strong>
                            Update ContractStatus <?php echo $model->id; ?>
                        </strong>
                    </div>
                </div>
                <div class="card-body">
                    <?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
                </div>
            </div>
        </div>
    </div>
</section>



