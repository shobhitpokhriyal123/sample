<section id="manage_products">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="card-title">
                        <strong>Manage Contract status</strong>
                    </div>
                    <?php
                    if (Yii::app()->user->isAdmin) {
                        echo CHtml::link('Create Contract Status', array('/admin/contractStatus/create'), array("class" => 'btn btn-success'));
                    }
                    ?>
                </div>
                <div class="card-body">
                    <?php
                    $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => 'contract-status-grid',
                        'afterAjaxUpdate' => 'function(id,options){ paginationControls(); }',
                        'dataProvider' => $model->search(),
                        'filter' => $model,
                        'columns' => array(
                            'id',
                            'title',
                            array(
                                'class' => 'bootstrap.widgets.TbButtonColumn',
                                'template' => '{view}{update}'
                            ),
                        ),
                    ));
                    ?>
                </div>
            </div></div>
    </div></section>
