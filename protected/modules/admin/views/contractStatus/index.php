<?php
$this->breadcrumbs=array(
	'Contract Statuses',
);

$this->menu=array(
	array('label'=>'Create ContractStatus','url'=>array('create')),
	array('label'=>'Manage ContractStatus','url'=>array('admin')),
);
?>

<h1>Contract Statuses</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
