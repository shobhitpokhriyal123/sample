<section id="contractView">
    <div class="container">
        <div class="row">

            <?php
                $this->breadcrumbs=array(
                    'Contract Statuses'=>array('index'),
                    $model->title,
                );

                $this->menu=array(
                    array('label'=>'List ContractStatus','url'=>array('index')),
                    array('label'=>'Create ContractStatus','url'=>array('create')),
                    array('label'=>'Update ContractStatus','url'=>array('update','id'=>$model->id)),
                    array('label'=>'Delete ContractStatus','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
                    array('label'=>'Manage ContractStatus','url'=>array('admin')),
                );
            ?>

            <div class="card">

                <div class="card-header">
                    <div class="card-title">
                        <strong>
                            View ContractStatus #<?php echo $model->id; ?>
                        </strong>
                    </div>
                           <?php if (Yii::app()->user->isAdmin) {
                        echo CHtml::link('Create Contract Status', array('/admin/contractStatus/admin'), array("class" => 'btn btn-success'));
                    }
                    ?>
                </div>
                 
                <div class="card-body">
                    <?php $this->widget('bootstrap.widgets.TbDetailView',array(
                        'data'=>$model,
                        'attributes'=>array(
                            'id',
                            'title',
                        ),
                    )); ?>
                </div>

            </div>

        </div>
    </div>
</section>
