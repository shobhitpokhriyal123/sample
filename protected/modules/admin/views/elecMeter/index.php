<?php
$this->breadcrumbs=array(
	'Elec Meters',
);

$this->menu=array(
	array('label'=>'Create ElecMeter','url'=>array('create')),
	array('label'=>'Manage ElecMeter','url'=>array('admin')),
);
?>

<h1>Elec Meters</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
