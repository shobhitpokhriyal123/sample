
<div class="container">
    <?php
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'elec-meter-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <p class="help-block">Fields with <span class="required">*</span> are required.</p>
    <div class="col-md-6">
        <?php echo $form->errorSummary($model); ?>

        <?php echo $form->dropdownListRow($model, 'mtype_id', CHtml::listData(MeterBase::model()->findAll(), 'id', 'title'), array('class' => 'span5')); ?>

        <?php echo $form->textFieldRow($model, 'days', array('class' => 'span5')); ?>

        <?php echo $form->textFieldRow($model, 'night', array('class' => 'span5')); ?>
    </div>
    <div class="col-md-6">
        <?php echo $form->textFieldRow($model, 'day_night_weekend', array('class' => 'span5')); ?>

        <?php echo $form->textFieldRow($model, 'stdo_6', array('class' => 'span5')); ?>


    </div>
 
    <div class="col-md-12">
           <hr>
    <div class="form-actions">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type' => 'primary',
            'label' => $model->isNewRecord ? 'Create' : 'Save',
        ));
        ?>
    </div>
        </div>
    <?php $this->endWidget(); ?>

</div>