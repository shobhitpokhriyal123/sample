<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'mtype_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'days',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'night',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'day_night_weekend',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'stdo_6',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
