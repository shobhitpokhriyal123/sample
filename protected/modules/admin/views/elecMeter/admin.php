<section id="manage_products">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">

                    <div class="card-title">
                        <strong>Manage Electric Meter </strong>
                    </div>

                    <?php
                    if (Yii::app()->user->isAdmin) {
                        echo CHtml::link('Create Settings', array('/admin/elecMeter/create'), array("class" => 'btn btn-success'));
                    }
                    ?>

                </div>
                <div class="card-body">
                    <?php
                    $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => 'elec-meter-grid',
                        'dataProvider' => $model->search(),
                        'afterAjaxUpdate' => 'function(id,options){ paginationControls(); }',
                        'filter' => $model,
                        'columns' => array(
                            'id',
                            'mtype_id' => array(
                                'name' => 'mtype_id',
                                'value' => '$data->base->title'
                            ),
                            'days',
                            'night',
                            'day_night_weekend',
                            'stdo_6',
                            array(
                                'class' => 'bootstrap.widgets.TbButtonColumn',
                            ),
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>