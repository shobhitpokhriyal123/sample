<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mtype_id')); ?>:</b>
	<?php echo CHtml::encode($data->mtype_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('days')); ?>:</b>
	<?php echo CHtml::encode($data->days); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('night')); ?>:</b>
	<?php echo CHtml::encode($data->night); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('day_night_weekend')); ?>:</b>
	<?php echo CHtml::encode($data->day_night_weekend); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('stdo_6')); ?>:</b>
	<?php echo CHtml::encode($data->stdo_6); ?>
	<br />


</div>