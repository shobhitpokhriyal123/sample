<section id="manage_products">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">

                    <div class="card-title">
                        <strong>View Electric Meter </strong>
                    </div>

                    <?php
                    if (Yii::app()->user->isAdmin) {
                        echo CHtml::link('Manage Settings', array('/admin/elecMeter/admin'), array("class" => 'btn btn-success'));
                    }
                    ?>

                </div>
                <div class="card-body">
                    <?php
                    $this->widget('bootstrap.widgets.TbDetailView', array(
                        'data' => $model,
                        'attributes' => array(
                            'id',
                            'mtype_id'=>array(
                                'name'=>'mtype_id',
                                'value'=>$model->base->title
                            ),
                            'days',
                            'night',
                            'day_night_weekend',
                            'stdo_6',
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
