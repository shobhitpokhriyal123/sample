<section id="create_meter">
    <div class="container">
        <div class="row">
            <?php
                $this->breadcrumbs=array(
                    'Meter Types'=>array('index'),
                    'Create',
                );

                $this->menu=array(
                    array('label'=>'List MeterType','url'=>array('index')),
                    array('label'=>'Manage MeterType','url'=>array('admin')),
                );
            ?>
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <strong>Create MeterType</strong>
                    </div>
                    <?php
                        if (Yii::app()->user->isAdmin) {
                            echo CHtml::link('Manage', array('/admin/meterType/admin'), array("class" => 'btn btn-success'));
                         //   echo CHtml::link('Create', array('/admin/meterType/create'), array("class" => 'btn btn-success'));
                        }
                    ?>
                </div>
                <div class="card-body">
                    <?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
                </div>
            </div>
        </div>
    </div>
</section>
