<section id="view_meter">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <strong>View MeterType #<?php echo $model->title; ?></strong>
                    </div>
                    <?php
                    if (Yii::app()->user->isAdmin) {
                        echo CHtml::link('Manage', array('/admin/meterType/admin'), array("class" => 'btn btn-success'));
                        echo CHtml::link('Create', array('/admin/meterType/create'), array("class" => 'btn btn-success'));
                    }
                    ?>
                </div>
                <div class="card-body">
                    <?php
                    $this->widget('bootstrap.widgets.TbDetailView', array(
                        'data' => $model,
                        'attributes' => array(
                            'id',
                            'supplier_id' => array(
                                'name' => 'supplier_id',
                                'value' => $model->user->username //MeterType::getStatus($model->supplier_id)
                            ),
                            'title' => array(
                                'name' => 'title',
                                'value' => $model->baseTitle->title
                            ),
                            
                            'tanency' => array(
                                'name' => 'tanency',
                                'value' => MeterType::getStatus($model->tanency)
                            ),
                            'smart_meter' => array(
                                'name' => 'smart_meter',
                                'value' => MeterType::getStatus($model->smart_meter)
                            ),
                            'validity',
                            'energy_per_year',
                            'night',
                            'weekday',
//                            'status' => array(
//                                'name' => 'status',
//                                'value' => Product::getStatus($model->status)
//                            ),
                            'created',
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
