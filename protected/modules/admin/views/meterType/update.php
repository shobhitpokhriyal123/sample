<section id="update_meter">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <strong>Update MeterType <?php echo $model->id; ?></strong>
                    </div>
                    <?php
                        if (Yii::app()->user->isAdmin) {
                            echo CHtml::link('Manage', array('/admin/meterType/admin'), array("class" => 'btn btn-success'));
                            echo CHtml::link('Create', array('/admin/meterType/create'), array("class" => 'btn btn-success'));
                        }
                    ?>
                </div>
                <div class="card-body">
                    <?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
                </div>
            </div>
        </div>
    </div>
</section>
