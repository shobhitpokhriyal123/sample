<style>
    label {
        margin: 5px 0;
    }
</style>
<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'meter-type-form',
    'enableAjaxValidation' => false,
        ));
?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<div class="row">
    <div class="col-sm-6">
        <?php //echo $form->errorSummary($model); ?>
        <?php echo $form->dropdownListRow($model, 'supplier_id', CHtml::listData(Users::model()->findAllByAttributes(array('role_id' => 2)), 'id', 'fullname'), array('empty' => '--- Select Supplier ---')); ?>

        <?php //echo $form->textFieldRow($model,'supplier_id',array('class'=>'span5')); ?>
         <?php echo $form->dropdownListRow($model, 'title', CHtml::listData(MeterBase::model()->findAll(), 'id', 'title'), array('empty' => '--- Select Title ---')); ?>
        <?php echo $form->textFieldRow($model, 'meter_code', array('class' => 'form-control', 'maxlength' => 255)); ?>
       
        <?php echo $form->dropdownListRow($model, 'tanency', array('1' => 'YES', '0' => 'NO')); ?>
        <?php echo $form->dropdownListRow($model, 'smart_meter', array('1' => 'YES', '0' => 'NO')); ?>
    </div>
    <div class="col-sm-6">
        <label for="Product_valid_till">Valid Till*</label>
        <?php
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model' => $model,
            'attribute' => 'validity',
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                'changeMonth' => true,
                'changeYear' => true,
            ),
        ));
        ?>

        <?php echo $form->textFieldRow($model, 'energy_per_year', array('class' => 'form-control', 'maxlength' => 255)); ?>
        <?php echo $form->textFieldRow($model, 'weekday', array('class' => 'form-control', 'maxlength' => 255)); ?>
        <?php echo $form->textFieldRow($model, 'night', array('class' => 'form-control', 'maxlength' => 255)); ?>
        <?php //echo $form->textFieldRow($model, 'status', array('class' => 'span5')); ?>

        <?php //echo $form->textFieldRow($model, 'created', array('class' => 'span5')); ?>

    </div>
</div>
<br>
<div class="form-actions">
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type' => 'primary',
        'label' => $model->isNewRecord ? 'Create' : 'Save',
        'htmlOptions' => array('class' => 'longBtn')
    ));
    ?>
</div>

<?php $this->endWidget(); ?>
