<?php
$this->breadcrumbs=array(
	'Meter Types',
);

$this->menu=array(
	array('label'=>'Create MeterType','url'=>array('create')),
	array('label'=>'Manage MeterType','url'=>array('admin')),
);
?>

<h1>Meter Types</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
