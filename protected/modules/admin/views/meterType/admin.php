<section id="manage_meter">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <strong>Manage Meter Types</strong>
                    </div>
                    <?php
                    if (Yii::app()->user->isAdmin) {
                        echo CHtml::link('Create Meter Type', array('/admin/meterType/create'), array("class" => 'btn btn-success'));
                    }
                    ?>
                </div>
                <div class="card-body">
                    <?php
                    $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => 'meter-type-grid',
                        'dataProvider' => $model->search($sid),
                        'afterAjaxUpdate' => 'function(id,options){ paginationControls(); }',
                        'filter' => $model,
                        'columns' => array(
                            //'id',

//                            'role_id' => array(
//                                'filter' => false, //CHtml::listData(Role::model()->getRoles(), 'id', 'title'),
//                                'name' => 'role_id',
//                                'id' => 'role_id',
//                                'value' => '$data->rolename->title'
//                            ),
                            'supplier_id' => array(
                                'filter' => CHtml::listData(Users::getAllSuppliers(), 'id', 'fullname'),
                                'type' => 'html',
                                'name' => 'supplier_id',
                                'value' => '$data->user->username', //'CHtml::image(Yii::app()->createAbsoluteUrl("admin/users/thumbnail/file/".$data->user->image))',
                            //  'htmlOptions' => array('class' => 'table-images')
                            ),
                            'meter_code',
                            'title' => array(
                                'name' => 'title',
                                'value' => '$data->baseTitle->title'
                            ),
                            'validity',
                            'energy_per_year',
                            'night',
                            'weekday',
//                            'status' => array(
//                                'filter' => false,
//                                'name' => 'status',
//                                'value' => 'Product::getStatus($data->status)'
//                            ),
                            'tanency' => array(
                                'filter' => false,
                                'name' => 'tanency',
                                'value' => 'MeterType::getStatus($data->tanency)'
                            ),
                            'smart_meter' => array(
                                'filter' => false,
                                'name' => 'smart_meter',
                                'value' => 'MeterType::getStatus($data->smart_meter)'
                            ),
                            //'created',
                            array(
                                'class' => 'bootstrap.widgets.TbButtonColumn',
                            ),
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
