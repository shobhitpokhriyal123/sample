<?php
$this->breadcrumbs=array(
	'Companies'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Company','url'=>array('index')),
	array('label'=>'Create Company','url'=>array('create')),
	array('label'=>'Update Company','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Company','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Company','url'=>array('admin')),
);
?>

<h1>View Company #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'user_id'=>array(
                    'name' => 'user_id',
                    'value' =>$model->user->username
                ),
		//'create_user_id',
		//'contract_id',
		'company',
		'telephone',
		'contact_name',
		'contact_position',
		'contact_email',
		'address',
		'postcode',
		'registraion_no',
		//'business_type',
                'business_type'=> array(
                      'name' => 'business_type',
                    'value' => Company::getBusinessType($model->business_type),
                 ),
		'is_micro_business'=> array(
                      'name' => 'is_micro_business',
                    'value' => Company::getTrueFalse($model->is_micro_business),
                 ),
            
            
            
            
		'no_of_emp',
		'no_of_years',
		'turnover',
	),
)); ?>
