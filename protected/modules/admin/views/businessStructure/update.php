<?php
$this->breadcrumbs=array(
	'Business Structures'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List BusinessStructure','url'=>array('index')),
	array('label'=>'Create BusinessStructure','url'=>array('create')),
	array('label'=>'View BusinessStructure','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage BusinessStructure','url'=>array('admin')),
);
?>

<h1>Update BusinessStructure <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>