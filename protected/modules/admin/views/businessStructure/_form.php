<section id="manage_products">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="card-title">
                        <strong>Create Business Structure</strong>
                    </div>
                    <?php
                    if (Yii::app()->user->isAdmin) {
                        echo CHtml::link('Create Business Structure', array('/admin/businessStructure/create'), array("class" => 'btn btn-success'));
                    }
                    ?>
                </div>
                <div class="card-body">

                    <?php
                    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                        'id' => 'business-structure-form',
                        'enableAjaxValidation' => false,
                    ));
                    ?>

                    <p class="help-block">Fields with <span class="required">*</span> are required.</p>

                    <?php echo $form->errorSummary($model); ?>

                    <?php echo $form->textFieldRow($model, 'title', array('class' => 'span5', 'maxlength' => 255)); ?>

                    <?php echo $form->textAreaRow($model, 'description', array('rows' => 6, 'cols' => 50, 'class' => 'span8 description')); ?>

                    <?php //echo $form->textFieldRow($model, 'created', array('class' => 'span5')); ?>

                    <div class="form-actions">
                        <?php
                        $this->widget('bootstrap.widgets.TbButton', array(
                            'buttonType' => 'submit',
                            'type' => 'primary',
                            'label' => $model->isNewRecord ? 'Create' : 'Save',
                        ));
                        ?>
                    </div>

                    <?php $this->endWidget(); ?>

                </div>
            </div>  
        </div>
    </div>
</section>

<style>
    .description {
    float: left;
    width: 100%;
    margin-bottom: 10px;
}
    
    </style>