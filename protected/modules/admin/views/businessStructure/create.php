<?php
$this->breadcrumbs=array(
	'Business Structures'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List BusinessStructure','url'=>array('index')),
	array('label'=>'Manage BusinessStructure','url'=>array('admin')),
);
?>

<h1>Create BusinessStructure</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>