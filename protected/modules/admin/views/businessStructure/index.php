<?php
$this->breadcrumbs=array(
	'Business Structures',
);

$this->menu=array(
	array('label'=>'Create BusinessStructure','url'=>array('create')),
	array('label'=>'Manage BusinessStructure','url'=>array('admin')),
);
?>

<h1>Business Structures</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
