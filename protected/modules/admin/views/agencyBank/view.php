<?php
$this->breadcrumbs=array(
	'Agency Banks'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List AgencyBank','url'=>array('index')),
	array('label'=>'Create AgencyBank','url'=>array('create')),
	array('label'=>'Update AgencyBank','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete AgencyBank','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AgencyBank','url'=>array('admin')),
);
?>

<h1>View AgencyBank #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'create_user_id',
		'agency_id',
		'account_name',
		'account_no',
		'sort_code',
		'bank_name',
		'account_number',
		'payment_date',
	),
)); ?>
