<?php
$this->breadcrumbs=array(
	'Agency Banks'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AgencyBank','url'=>array('index')),
	array('label'=>'Manage AgencyBank','url'=>array('admin')),
);
?>

<h1>Create AgencyBank</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>