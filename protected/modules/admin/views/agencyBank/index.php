<?php
$this->breadcrumbs=array(
	'Agency Banks',
);

$this->menu=array(
	array('label'=>'Create AgencyBank','url'=>array('create')),
	array('label'=>'Manage AgencyBank','url'=>array('admin')),
);
?>

<h1>Agency Banks</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
