<?php
$this->breadcrumbs=array(
	'Agency Banks'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AgencyBank','url'=>array('index')),
	array('label'=>'Create AgencyBank','url'=>array('create')),
	array('label'=>'View AgencyBank','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage AgencyBank','url'=>array('admin')),
);
?>

<h1>Update AgencyBank <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>