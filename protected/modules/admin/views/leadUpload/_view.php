<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meter_id')); ?>:</b>
	<?php echo CHtml::encode($data->meter_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('is_gas')); ?>:</b>
	<?php echo CHtml::encode($data->is_gas); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('document')); ?>:</b>
	<?php echo CHtml::encode($data->document); ?>
	<br />


</div>