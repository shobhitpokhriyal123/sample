<?php
$this->breadcrumbs=array(
	'Lead Uploads'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List LeadUpload','url'=>array('index')),
	array('label'=>'Create LeadUpload','url'=>array('create')),
	array('label'=>'View LeadUpload','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage LeadUpload','url'=>array('admin')),
);
?>

<h1>Update LeadUpload <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>