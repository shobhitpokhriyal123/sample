<?php
$this->breadcrumbs=array(
	'Lead Uploads',
);

$this->menu=array(
	array('label'=>'Create LeadUpload','url'=>array('create')),
	array('label'=>'Manage LeadUpload','url'=>array('admin')),
);
?>

<h1>Lead Uploads</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
