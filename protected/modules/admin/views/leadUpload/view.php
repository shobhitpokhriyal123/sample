<?php
$this->breadcrumbs=array(
	'Lead Uploads'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List LeadUpload','url'=>array('index')),
	array('label'=>'Create LeadUpload','url'=>array('create')),
	array('label'=>'Update LeadUpload','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete LeadUpload','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage LeadUpload','url'=>array('admin')),
);
?>

<h1>View LeadUpload #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'meter_id',
		'is_gas',
		'document',
	),
)); ?>
