<?php
$this->breadcrumbs=array(
	'Lead Uploads'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List LeadUpload','url'=>array('index')),
	array('label'=>'Manage LeadUpload','url'=>array('admin')),
);
?>

<h1>Create LeadUpload</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>