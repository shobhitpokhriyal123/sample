<?php
$this->breadcrumbs=array(
	'Lead Addresses'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List LeadAddress','url'=>array('index')),
	array('label'=>'Create LeadAddress','url'=>array('create')),
	array('label'=>'Update LeadAddress','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete LeadAddress','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage LeadAddress','url'=>array('admin')),
);
?>

<h1>View LeadAddress #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'create_user_id',
		'lead_id',
		'business_postcode',
		'business_name',
		'business_no',
		'street_address_1',
		'street_address_2',
		'town',
		'country',
		'billling_add',
		'billing_postcode',
		'billing_name',
		'billing_no',
		'street_address_11',
		'street_address_12',
		'town1',
		'country1',
		'created',
	),
)); ?>
