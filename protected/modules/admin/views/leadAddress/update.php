<?php
$this->breadcrumbs=array(
	'Lead Addresses'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List LeadAddress','url'=>array('index')),
	array('label'=>'Create LeadAddress','url'=>array('create')),
	array('label'=>'View LeadAddress','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage LeadAddress','url'=>array('admin')),
);
?>

<h1>Update LeadAddress <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>