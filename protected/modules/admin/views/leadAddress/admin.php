
<?php

$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'lead-address-grid',
    'dataProvider' => $model->search(),
    'afterAjaxUpdate' => 'function(id,options){ paginationControls(); }',
    'filter' => $model,
    'columns' => array(
        //'id',
        //'create_user_id',
//        'lead_id' => array(
//            'filter' => false, //CHtml::listData(Users::getAllSuppliers(), 'id', 'fullname'), 'type' => 'html',
//            'header' => 'Lead',
//            'name' => 'lead_id',
//            'value' => '$data->lead->first_name'
//        ),
        'business_postcode',
        'business_name',
        'business_no',
       
          'street_address_1',
          'street_address_2',
          'town',
          'country',
          'billling_add',
          'billing_postcode',
          'billing_name',
          'billing_no',
          /* 'street_address_11',
          'street_address_12',
          'town1',
          'country1',
          'created',
         */
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{update}',
            'header' => 'Actions',
            'buttons' => array(
                'update' => array(
                    'label' => '<button class="btn btn-primary">Update</button>',
                    'url' => 'Yii::app()->createAbsoluteUrl("admin/leadAddress/addressupdate", array("id"=>$data->id))',
                    'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Update'),
                    'imageUrl' => false,
                    'click' => "function(e){
                               e.preventDefault();
                               var url = $(this).attr('href');
                               $.ajax({
                                 type: 'POST',
                                 url: url,
                                 success: function(res){
                                 res = $.parseJSON(res);
                              
                                for (var property in res) {
                                        if (res.hasOwnProperty(property)) { 
                                        
                                         $('#LeadAddress_'+property).val(res[property]);
                                    }
                                }
                                    var current = $('#lead-address-form').attr('action');
                                    $('#lead-address-form').attr('action',current+'&id='+res.id);
                                 },
                                });
                                
                           
                            }",
                ),
            ),
        ),
    ),
));
?>
