<?php
$this->breadcrumbs=array(
	'Lead Addresses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List LeadAddress','url'=>array('index')),
	array('label'=>'Manage LeadAddress','url'=>array('admin')),
);
?>

<h1>Create LeadAddress</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>