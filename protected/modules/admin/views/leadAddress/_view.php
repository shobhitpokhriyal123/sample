<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_user_id')); ?>:</b>
	<?php echo CHtml::encode($data->create_user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lead_id')); ?>:</b>
	<?php echo CHtml::encode($data->lead_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('business_postcode')); ?>:</b>
	<?php echo CHtml::encode($data->business_postcode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('business_name')); ?>:</b>
	<?php echo CHtml::encode($data->business_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('business_no')); ?>:</b>
	<?php echo CHtml::encode($data->business_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('street_address_1')); ?>:</b>
	<?php echo CHtml::encode($data->street_address_1); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('street_address_2')); ?>:</b>
	<?php echo CHtml::encode($data->street_address_2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('town')); ?>:</b>
	<?php echo CHtml::encode($data->town); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('country')); ?>:</b>
	<?php echo CHtml::encode($data->country); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('billling_add')); ?>:</b>
	<?php echo CHtml::encode($data->billling_add); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('billing_postcode')); ?>:</b>
	<?php echo CHtml::encode($data->billing_postcode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('billing_name')); ?>:</b>
	<?php echo CHtml::encode($data->billing_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('billing_no')); ?>:</b>
	<?php echo CHtml::encode($data->billing_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('street_address_11')); ?>:</b>
	<?php echo CHtml::encode($data->street_address_11); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('street_address_12')); ?>:</b>
	<?php echo CHtml::encode($data->street_address_12); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('town1')); ?>:</b>
	<?php echo CHtml::encode($data->town1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('country1')); ?>:</b>
	<?php echo CHtml::encode($data->country1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />

	*/ ?>

</div>