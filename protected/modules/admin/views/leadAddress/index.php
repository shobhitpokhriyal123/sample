<?php
$this->breadcrumbs=array(
	'Lead Addresses',
);

$this->menu=array(
	array('label'=>'Create LeadAddress','url'=>array('create')),
	array('label'=>'Manage LeadAddress','url'=>array('admin')),
);
?>

<h1>Lead Addresses</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
