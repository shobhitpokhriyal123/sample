<section id="manage_products">
    <div class="container-fluid">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="card-title">
                        <strong>Manage Products</strong>
                    </div>

                    <?php
                    if (Yii::app()->user->isAdmin) {
                        echo CHtml::link('Create product', array('/admin/product/create'), array("class" => 'btn btn-success'));
                    }
                    ?>

                </div>
                <div class="card-body">
                    <?php
                    $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => 'product-grid',
                        'dataProvider' => $model->search(),
                        'afterAjaxUpdate' => 'function(id,options){ paginationControls(); }',
                        'filter' => $model,
                        'columns' => array(
                            'assigned_to' => array(
                                'filter' => false,//CHtml::listData(Users::getAllSuppliers(), 'id', 'fullname'),
                                'type' => 'html',
                                'name' => 'assigned_to',
                                'value' => '$data->user->username',//'CHtml::image(Yii::app()->createAbsoluteUrl("admin/users/thumbnail/file/".$data->user->image))',
                                'htmlOptions' => array('class' => 'table-images'),
                               
                                
                            ),
                            'utility' => array(
                                'name' => 'utility',
                                'filter' => false, // Product::getUtilites(),
                                'value' => 'Product::getUtilites($data->utility)',
                            ),
                            'corporate_sme' => array(
                                'name' => 'corporate_sme',
                                'filter' => false, // Product::getCorporateSME(),
                                'value' => 'Product::getCorporateSME($data->corporate_sme)',
                            ),
                            'product_name' => array(
                                'name' => 'product_name',
                                'type' => 'raw',
                                'value' => 'CHtml::link($data->product_name,array("product/view","id"=>$data->id), array("target"=>"_blank"))',
                                'filter' => false
                            ),
                            'valid_till' => array(
                                'name' => 'valid_till',
                                'type' => 'raw',
                                'value' => 'date("d-m-Y", strtotime($data->valid_till))',
                                'filter' => false
                            ),
                            'valid_from' => array(
                                'name' => 'valid_from',
                                'type' => 'raw',
                                'value' => 'date("d-m-Y", strtotime($data->valid_from))',
                                'filter' => false
                            ),
                            //
                            array(
                                'class' => 'bootstrap.widgets.TbButtonColumn',
                                'template' => '{activate}{deactivate}{calculate}',
                                'header' => 'Active',
                                'buttons' => array(
                                    'calculate' => array(
                                        'label' => '&nbsp;&nbsp;<i class="fa fa-wrench"></i>',
                                        'url' => 'Yii::app()->createAbsoluteUrl("admin/product/calculate", array("id"=>$data->id))',
                                        'options' => array('title' => 'Calculate'),
                                        'imageUrl' => false,
                                        'click' => "function(e){
                                                var url = $(this).attr('href');
                                                  $('#modal11').modal('show');
                                                e.preventDefault();
                                                $.ajax({
                                                 type: 'POST',
                                                 url: url,
                                                 success: function(res){
                                                      $('#fasdfsadf').html(res);
                                                   },
                                                });

                                                return false;
                                            }",
                                    ),
                                    'activate' => array(
                                        'label' => '&nbsp;&nbsp;<i class="fa fa-check"></i>',
                                        'visible' => '$data->status > 0', // <-- SHOW IF ROW INACTIVE
                                        'url' => 'Yii::app()->createAbsoluteUrl("admin/product/changeStatus", array("id"=>$data->id))',
                                        'options' => array('title' => 'Deactivate product'),
                                        'imageUrl' => false,
                                        'click' => "function(e){
                                                var url = $(this).attr('href');
                                                e.preventDefault();
                                                $.ajax({
                                                 type: 'POST',
                                                 url: url,
                                                 success: function(res){
                                                  $.fn.yiiGridView.update('product-grid');

                                                },
                                                });

                                                return false;
                                            }",
                                    ),
                                    'deactivate' => array(
                                        'label' => '&nbsp;&nbsp;<i class="fa fa-remove"></i>',
                                        'visible' => '$data->status == 0', // <-- SHOW IF ROW ACTIVE
                                        'url' => 'Yii::app()->createAbsoluteUrl("admin/product/changeStatus", array("id"=>$data->id))',
                                        'options' => array('title' => 'Activate product'),
                                        'imageUrl' => false,
                                        'click' => "function(e){

                                                 e.preventDefault();
                                                var url = $(this).attr('href');

                                                $.ajax({
                                                 type: 'POST',
                                                 url: url,
                                                 success: function(res){
                                                   $.fn.yiiGridView.update('product-grid');
                                                },
                                                });

                                                return false;
                                            }",
                                    ),
                                ),
                            ),
                            array(
                                'class' => 'bootstrap.widgets.TbButtonColumn',
                                'template' => '{update}{delete}{view}',
                                'header' => 'Update/Delete',
                            ),
                        ),
                    ));
                    ?>
                </div>
            </div>

        </div>
    </div>
</section>

<div class="modal fade" id="modal11" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document" id="fasdfsadf">

    </div>
</div>


