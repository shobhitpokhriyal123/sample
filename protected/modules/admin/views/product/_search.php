<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'utility',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'corporate_sme',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'product_name',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'volume_translate_min',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'volume_translate_max',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'max_commission',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'commission_increament',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'commission_banded',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'standing_charge',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'direct_dabit',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'renewal',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'renewal_obligation',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'feed_in_tarriff',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'contact_for_difference',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'capacity_machanism',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'distribution_charge',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'transmission_charge',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'balancing_charge',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'direct_dabit_mandate',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'t_and_cs',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'status',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'created',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
