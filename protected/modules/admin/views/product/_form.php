<style>
    label {
        margin: 5px 0;
    }

    .btn {
        margin: 5px 5px 5px 0;
    }
</style>
<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'product-form',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>
<hr>
<?php //echo $form->errorSummary($model); ?>
<?php echo $form->checkBoxRow($model, 'status', array('1' => 'Active', '0' => 'Inactive')); ?>
<div class="row">
    <div class="col-sm-6">

        <?php echo $form->dropdownListRow($model, 'assigned_to', CHtml::listData(Users::model()->findAllByAttributes(array('role_id' => 2)), 'id', 'fullname'), array('empty' => '--- Select Supplier ---')); ?>

        <?php echo $form->dropDownListRow($model, 'corporate_sme', $model->getCorporateSME(), array('onclick' => 'getUtilities($(this).val())')); ?>

        <?php echo $form->dropDownListRow($model, 'utility', array('0' => '--- Select Utility --- ')); ?>

        <?php echo $form->textFieldRow($model, 'product_name', array('class' => 'form-control', 'maxlength' => 255)); ?>

        <?php echo $form->textFieldRow($model, 'volume_translate_min', array('class' => 'form-control', "prepend" => '<i class="">%</i>')); ?>

        <?php echo $form->textFieldRow($model, 'volume_translate_max', array('class' => 'form-control', "prepend" => '<i class="">%</i>')); ?>

        <?php echo $form->textFieldRow($model, 'max_commission', array('class' => 'form-control', "append" => '<i class="">p/kWh</i>')); ?>

        <?php echo $form->textFieldRow($model, 'commission_increament', array('class' => 'form-control', "append" => '<i class="">p/kWh</i>')); ?>

        <?php echo $form->dropDownListRow($model, 'commission_banded', $model->getOthers()); ?>

        <?php echo $form->dropDownListRow($model, 'standing_charge', $model->getOthers()); ?>

        <?php echo $form->dropDownListRow($model, 'direct_dabit', $model->getDirectDebit()); ?>
        <div id="dsfasfd" style="display:none">
            <?php echo $form->textFieldRow($model, 'dabit_percentage', array('class' => 'form-control', "append" => '<i class="">%</i>')); ?>
        </div>

        <label for="Product_valid_from">Valid From*</label>
        <?php
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model' => $model,
            'attribute' => 'valid_from',
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                'changeMonth' => true,
                'changeYear' => true,
            ),
        ));
        ?>

        <label for="Product_valid_till">Valid Till*</label>
        <?php
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model' => $model,
            'attribute' => 'valid_till',
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                'changeMonth' => true,
                'changeYear' => true,
            ),
        ));
        ?>



    </div>
    <div class="col-sm-6">
        <?php echo $form->dropDownListRow($model, 'renewal', $model->getAqRe()); ?>

        <?php //echo $form->label($model, 'min_start_time'); ?>
        <?php
//        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
//            'model' => $model,
//            'attribute' => 'min_start_date',
//            'options' => array(
//                'dateFormat' => 'yy-mm-dd',
//                'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
//                'changeMonth' => true,
//                'changeYear' => true,
//            ),
//                //'language' => Yii::app()->language,
//        ));
        ?>
        <?php echo $form->textFieldRow($model, 'min_start_time', array('placeholder' => "In Days", 'class' => 'form-control', "prepend" => '<i class="fa fa-calendar-check-o">In days</i>')); ?>
        <?php echo $form->textFieldRow($model, 'max_end_time', array('placeholder' => "In Days", 'class' => 'form-control', "prepend" => '<i class="fa fa-calendar-check-o">In days</i>')); ?>


        <?php echo $form->dropDownListRow($model, 'renewal_obligation', $model->getOtherCharges()); ?>
        <div class="gas">
            <?php echo $form->dropDownListRow($model, 'feed_in_tarriff', $model->getOtherCharges()); ?>

            <?php echo $form->textFieldRow($model, 'contact_for_difference', $model->getOtherCharges()); ?>

            <?php echo $form->dropDownListRow($model, 'capacity_machanism', $model->getOthers()); ?>

            <?php echo $form->dropDownListRow($model, 'distribution_charge', $model->getOtherCharges()); ?>

            <?php echo $form->dropDownListRow($model, 'transmission_charge', $model->getOtherCharges()); ?>

            <?php echo $form->dropDownListRow($model, 'balancing_charge', $model->getOtherCharges()); ?>
        </div>
        <?php echo $form->fileFieldRow($model, 'direct_dabit_mandate'); ?>

        <?php echo $form->fileFieldRow($model, 't_and_cs'); ?>
        <?php if (!$model->id) { ?>
            <?php echo $form->fileFieldRow($model, 'csv_file'); ?>
        <?php } else { ?>
            <br>
            <?php  echo $form->fileFieldRow($model, 'csv_file', array('disabled' => 'disabled')); ?>
            <a class="btn btn-primary" href="<?php echo Yii::app()->createAbsoluteUrl('admin/product/download/file/' . $model->csv_file) ?>"><?php echo $model->csv_file ?><i class="icon-download"></i></a>
        <?php } ?>

        <label for="Product_acc_start">Acc. Start*</label>
        <?php
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model' => $model,
            'attribute' => 'acc_start',
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                'changeMonth' => true,
                'changeYear' => true,
            ),
                //'language' => Yii::app()->language,
        ));
        ?>

        <label for="Product_acc_end">Acc. End*</label>
        <?php
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model' => $model,
            'attribute' => 'acc_end',
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                'changeMonth' => true,
                'changeYear' => true,
            ),
                //'language' => Yii::app()->language,
        ));
        ?>
    </div>
</div>

<?php //echo $form->textFieldRow($model, 'created', array('class' => 'span5')); ?>
<br>
<div class="form-actions">
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type' => 'primary',
        'label' => $model->isNewRecord ? 'Next' : 'Save',
        'htmlOptions' => array('class' => 'longBtn')
    ));
    ?>
</div>

<?php $this->endWidget(); ?>

<script>

    var pid = '<?php echo $model->id ?>';
    $(document).ready(function () {
        var idd = $("#Product_utility").val();
        if (idd)
            getUtilities($("#Product_corporate_sme").val(), pid);

    });


</script>   

<script>


    $("#Product_direct_dabit").change(function () {
        var id = $(this).val();
        if (id == 1) {
            $("#dsfasfd").show();
        } else {
            $(this).val('');
            $("#dsfasfd").hide();
        }
    });

    gasParms();

    $("#Product_utility").change(function () {
        gasParms();
    });

    function getUtilities(id, pid = null) {

        var url = '<?php echo Yii::app()->createAbsoluteUrl("admin/product/getUtilities/id") ?>/' + id + "/pid/" + pid;
        $("#Product_utility").load(url);

        gasParms();
    }

    function gasParms() {
        return;
        var id = $("#Product_utility").val();
        if (id == 17 || id == 19) {
            $(".gas").hide();
        } else {
            $(".gas").show();
        }
    }


</script>
