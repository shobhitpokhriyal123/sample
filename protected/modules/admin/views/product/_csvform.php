<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Csv Form</h3>
</div>
<div class="modal-body">

    <?php
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'product-form',
        'action'=>$this->createUrl("product/admin"),
        'enableAjaxValidation' => false,
        'enableClientValidation' => false,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    ));
    ?>


    <p class="help-block">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->fileFieldRow($model, 'csvfile', array("required" => "required")); ?>

    <?php /* echo $form->errorSummary($model); ?>

      <?php echo $form->textFieldRow($model, 'utility', array('class' => 'span5', 'maxlength' => 255)); ?>

      <?php echo $form->textFieldRow($model, 'corporate_sme', array('class' => 'span5', 'maxlength' => 255)); ?>

      <?php echo $form->textFieldRow($model, 'product_name', array('class' => 'span5', 'maxlength' => 255)); ?>

      <?php echo $form->textFieldRow($model, 'volume_translate_min', array('class' => 'span5', 'maxlength' => 255)); ?>

      <?php echo $form->textFieldRow($model, 'volume_translate_max', array('class' => 'span5', 'maxlength' => 255)); ?>

      <?php echo $form->textFieldRow($model, 'max_commission', array('class' => 'span5', 'maxlength' => 255)); ?>

      <?php echo $form->textFieldRow($model, 'commission_increament', array('class' => 'span5', 'maxlength' => 255)); ?>

      <?php echo $form->textFieldRow($model, 'commission_banded', array('class' => 'span5', 'maxlength' => 255)); ?>

      <?php echo $form->textFieldRow($model, 'standing_charge', array('class' => 'span5', 'maxlength' => 255)); ?>

      <?php echo $form->textFieldRow($model, 'direct_dabit', array('class' => 'span5', 'maxlength' => 255)); ?>

      <?php echo $form->textFieldRow($model, 'renewal', array('class' => 'span5', 'maxlength' => 255)); ?>

      <?php echo $form->textFieldRow($model, 'renewal_obligation', array('class' => 'span5', 'maxlength' => 255)); ?>

      <?php echo $form->textFieldRow($model, 'feed_in_tarriff', array('class' => 'span5', 'maxlength' => 255)); ?>

      <?php echo $form->textFieldRow($model, 'contact_for_difference', array('class' => 'span5', 'maxlength' => 255)); ?>

      <?php echo $form->textFieldRow($model, 'capacity_machanism', array('class' => 'span5', 'maxlength' => 255)); ?>

      <?php echo $form->textFieldRow($model, 'distribution_charge', array('class' => 'span5', 'maxlength' => 255)); ?>

      <?php echo $form->textFieldRow($model, 'transmission_charge', array('class' => 'span5', 'maxlength' => 255)); ?>

      <?php echo $form->textFieldRow($model, 'balancing_charge', array('class' => 'span5', 'maxlength' => 255)); ?>

      <?php echo $form->textFieldRow($model, 'direct_dabit_mandate', array('class' => 'span5', 'maxlength' => 255)); ?>

      <?php echo $form->textFieldRow($model, 't_and_cs', array('class' => 'span5', 'maxlength' => 255)); ?>

      <?php echo $form->textFieldRow($model, 'status', array('class' => 'span5')); ?>

      <?php echo $form->textFieldRow($model, 'created', array('class' => 'span5')); ?>

      <div class="form-actions">
      <?php
      $this->widget('bootstrap.widgets.TbButton', array(
      'buttonType' => 'submit',
      'type' => 'primary',
      'label' => $model->isNewRecord ? 'Create' : 'Save',
      ));
      ?>
      </div>
      <?php */ ?>
    <?php $this->endWidget(); ?>

</div>
<div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    <button id="submit" class="btn btn-primary">Get Header </button>
</div>


<script>

    $(document).ready(function () {
        $("#submit").click(function (e) {
            e.preventDefault();
            if($("#Product_csvfile"))
            $("#product-form").submit();
        });

    });

    //   alert('sadfsfd');
    function getCsvHeader() {
        
        var data = $("#product-form").serialize();
        var url = '<?php echo Yii::app()->createAbsoluteUrl("/admin/product/getCsvHeader")   ?>';
        $.ajax({
            url: url,
            type: "POST",
            data: data,
            //  dataType:json,
            beforeSend: function () {

            },
            success: function (res) {
                console.log(res);
            },
        });
    }
</script>