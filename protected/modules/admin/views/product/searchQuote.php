<style>
    .container-fluid {
        margin-left: 20px;
        margin-right: 20px;
    }

    /* Input field with increment/decrement */
    .inputWrapper {
        position: relative;
        width: 90px;
        height: 37px;
        border-radius: 3px;
        overflow: hidden;
    }
    .inputWrapper .txtInput {
        display: block;
        width: 80%;
        height: 100%;
        position: absolute;
        left: 0;
        padding-left: 5px;
    }
    .cusWrap {
        position: absolute;
        width: 20%;
        height: 100%;
        right: 0;
    }
    .cusWrap .cusBTN {
        display: block;
        width: 100%;
        height: 50%;
        padding: 0;
        line-height: 4px;
        border: 1px solid #ccc;
    }

    table input[type="text"] {
        width: 65px;
        height: 36px;
    }
    .fBTN {
        float: right;
    }
    .ratebox {
        width: 170px;
    }
    .ratebox p {
        margin: 0;
    }
    .ratebox span {
        display: inline-block;
        font-size: 0.8em;
    }
    .ratebox .tag {
        width: 100px;
        font-weight: bold;
    }
    table th, table td {
        font-size: 0.8em;
    }
</style>


<div class="row-fluid">
    <div class="span12 well">
        <div class="span12" style="border: 1px solid #dfdfdf; background: #FFF" >
            <?php
            $get = $_GET['Product'];
            $supplier = $user_id = $get['assigned_to'];
            $supplier = Users::model()->findByPk($supplier);
            // echo '<pre>'; print_r($get); die;
            $mpr = '';
            $pcode = '';
            if (isset($get['postcode']) && $get['postcode']) {
                ?>
                <div class="span3"><span>Postcode:</span> <?php echo $pcode = $get['postcode'] ?></div> 
            <?php } else if (isset($get['mpr']) && $get['mpr']) { ?>
                <div class="span3"><span>MPR:</span> <?php echo $mpr = $get['mpr'] ?></div> 
            <?php }
            ?>

            <div class="span3"><span>Start Date:</span> <?php echo $date = $get["start_date"]; ?></div> 
            <div class="span3"><span>AQ:</span> <?php echo $aq = $get['aq']; ?></div>
            <div class="span3"><span>Current Supplier:</span> <?php echo ($supplier) ? $supplier->fullname : 'N/A'; ?></div> 
        </div>
        <script>
            var current_supplier = '<?php echo $supplier->id; ?>';
            var aqu = '<?php echo $aq; ?>';
        </script>
        <div class="clearfix"></div>
        <hr>
        <?php
        $url12 = $this->createAbsoluteUrl("/admin/price/getMonthWiseData/month/12&Price[start_date]=$date&Price[max_aq]=$aq&Price[min_kwh]=$mpr&Price[postcode]=$pcode&Price[assigned_to]=$user_id");
        $url24 = $this->createAbsoluteUrl("/admin/price/getMonthWiseData/month/24&Price[start_date]=$date&Price[max_aq]=$aq&Price[min_kwh]=$mpr&Price[assigned_to]=$user_id&Price[postcode]=$pcode");
        $url36 = $this->createAbsoluteUrl("/admin/price/getMonthWiseData/month/36&Price[start_date]=$date&Price[max_aq]=$aq&Price[min_kwh]=$mpr&Price[assigned_to]=$user_id&Price[postcode]=$pcode");
        $url48 = $this->createAbsoluteUrl("/admin/price/getMonthWiseData/month/48&Price[start_date]=$date&Price[max_aq]=$aq&Price[min_kwh]=$mpr&Price[assigned_to]=$user_id&Price[postcode]=$pcode");
        $url60 = $this->createAbsoluteUrl("/admin/price/getMonthWiseData/month/60&Price[start_date]=$date&Price[max_aq]=$aq&Price[min_kwh]=$mpr&Price[assigned_to]=$user_id&Price[postcode]=$pcode");
        $all = $this->createAbsoluteUrl("/admin/price/getMonthWiseData/month/123&Price[start_date]=$date&Price[max_aq]=$aq&Price[min_kwh]=$mpr&Price[assigned_to]=$user_id&Price[postcode]=$pcode");
        $this->widget('zii.widgets.jui.CJuiTabs', array(
            'tabs' => array(
                '12 Months' => array('ajax' => $url12),
                '24 Months' => array('ajax' => $url24),
                '36 Months' => array('ajax' => $url36),
                '48 Months' => array('ajax' => $url48),
                '60 Months' => array('ajax' => $url60),
                'All Terms' => array('ajax' => $all),
            ),
            // additional javascript options for the tabs plugin
            'options' => array(
                'collapsible' => true,
                'event' => "click",
            ),
        ));
        ?>

    </div>
</div>
<script>
    paginationControls();
</script>
