<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>


<section id="csvView_product">
    <div class="container">
        <div class="row">
            <?php if (Yii::app()->user->hasFlash('success')): ?>
                <div class="info">
                    <?php echo Yii::app()->user->getFlash('success'); ?>
                </div>
            <?php endif; ?>
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <strong>Map Your Csv <?php echo $model->csv_file; ?></strong>
                    </div>
                    <?php
                        if (Yii::app()->user->isAdmin) {
                            echo CHtml::link('Add Fields', array('/admin/priceMapping/create' . $model->id), array("class" => 'btn btn-success ', 'id' => 'add_fields'));
            //                /echo CHtml::link('Previous', array('/admin/product/update/id/' . $model->id), array("class" => 'btn btn-success'));
                            echo CHtml::link('Submit', array("#"), array("class" => 'btn btn-success ', 'id' => 'submit_csv'));
                        }
                    ?>
                </div>
                <div class="card-body">


                    <div id="dynamic_field">

                    </div>


                    <?php
                        $csv_attr = array();
                        $attributes_array = Price::getAllAttributes();
                        // echo '<pre>'; print_r($attributes_array); die;
                        $csv = $model->readCsvContents(1);
                        if (!empty($csv) && !empty($csv[0]))
                            $csv_attr = $csv[0];
                    ?>
                    <?php
                        $count = 0;
                        if (!empty($csv_attr)) {
                            $count = count($csv_attr);
                    ?>
                    <?php
                        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                            'id' => 'product-price-form',
                            'enableAjaxValidation' => false,
                            'enableClientValidation' => false,
                            'clientOptions' => array(
                                'validateOnSubmit' => true,
                            ),
                            'htmlOptions' => array('enctype' => 'multipart/form-data'),
                        ));
                    ?>
                    <table class="table-striped table-bordered table">
                        <tr>
                            <th>CSV Headers</th>
                            <th>Database Attributes</th>
                        </tr>
                        <?php for ($i = 0; $i < $count; $i++) { ?>
                            <tr>
                                <td>
                                    <?php echo $csv_attr[$i]; ?>
                                    <input type="hidden" name="Product[csv_attr][<?php echo $i ?>]" value="<?php echo $i; ?>"/>
                                </td>
                                <td>

                                    <?php
            //                            $this->widget('ext.select2.ESelect2', array(
            //                                'model' => $model,
            //                                'attribute' => 'current_attribute[]',
            //                                'data' => $attributes_array,
            //                                 'options' => ['placeholder' => 'Select database attribute','width'=>'100px'],
            //                            ));
                                    ?>
                                    <select  id="current_attribute_<?php echo $i ?>"  name="current_attribute[<?php echo $i ?>]" onchange="getCurrentAttr($(this))" >
                                        <option  value="">Select Database Attributes</option>
                                        <?php foreach ($attributes_array as $key => $attributes_arr) { ?>
                                            <option  value="<?php echo $key; ?>" ><?php echo $attributes_arr ?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                            </tr>
                        <?php } ?>
                    </table>
                    <?php $this->endWidget(); ?>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>



<script>
   $('select').select2();
    function getCurrentAttr(obj) {
        var val = obj.val();
        var count = '<?php echo $count ?>';
        var c = 0;

    }

    $(document).ready(function () {
        $("#submit_csv").click(function (e) {
            e.preventDefault();
            var count = '<?php echo $count ?>';
            for (i = 0; i < count; i++) {
                if (!$("#current_attribute_" + i).val()) {
                    $("#current_attribute_" + i).css("border", "1px solid red");
                    return false;
                } else {
                    $("#current_attribute_" + i).css("border", "1px solid #cccccc");
                }
            }
            $("#product-price-form").submit();
        });

        $("#add_fields").click(function (e) {
            e.preventDefault();
            var url = '<?php echo Yii::app()->createAbsoluteUrl("admin/priceMapping/create/pid/" . $model->id) ?>';
            $("#dynamic_field").load(url);
        });

    });
</script>
