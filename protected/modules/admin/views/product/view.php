<section id="view_product">
    <div class="container-fluid">
        <div class="row">
            <?php if (Yii::app()->user->hasFlash('success')): ?>
                <div class="info">
                    <?php echo Yii::app()->user->getFlash('success'); ?>
                </div>
            <?php endif; ?>

            <?php
            $this->breadcrumbs = array(
                'Products' => array('index'),
                $model->id,
            );

            $this->menu = array(
                array('label' => 'List Product', 'url' => array('index')),
                array('label' => 'Create Product', 'url' => array('create')),
                array('label' => 'Update Product', 'url' => array('update', 'id' => $model->id)),
                array('label' => 'Delete Product', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Are you sure you want to delete this item?')),
                array('label' => 'Manage Product', 'url' => array('admin')),
            );
            ?>
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <strong>View Product #<?php echo $model->product_name; ?></strong>
                    </div>
                    <?php
                    if (Yii::app()->user->isAdmin) {
                        echo CHtml::link('Manage product', array('/admin/product/admin'), array("class" => 'btn btn-success'));
                    }
                    ?>
                </div>
                <div class="card-body">
                    <?php
                    $this->widget('bootstrap.widgets.TbDetailView', array(
                        'data' => $model,
                        'attributes' => array(
                            // 'id',
                            'utility' => array(
                                'name' => 'utility',
                                'value' => Product::getUtilites($model->utility)
                            ),
                            'corporate_sme' => array(
                                'name' => 'corporate_sme',
                                'value' => Product::getCorporateSME($model->corporate_sme)
                            ),
                            'assigned_to' => array(
                                'name' => 'assigned_to',
                                'type' => 'raw',
                                'value' => CHtml::image(Yii::app()->createAbsoluteUrl("admin/users/thumbnail/file/" . $model->user->image))
                            ),
                            'product_name',
                            'volume_translate_min',
                            'volume_translate_max',
                            'max_commission',
                            'commission_increament',
                        //                'commission_banded',
                        //                'standing_charge',
                        //                'direct_dabit',
                        //                'renewal',
                        //                'renewal_obligation',
                        //                'feed_in_tarriff',
                        //                'contact_for_difference',
                        //                'capacity_machanism',
                        //                'distribution_charge',
                        //                'transmission_charge',
                        //                'balancing_charge',
                        //                'direct_dabit_mandate',
                        //                't_and_cs',
                        //                'status',
                        //                'created',
                        ),
                    ));
                    ?>

                    <hr>

                    <?php
                    if (in_array($model->type->id, Product::ELEC_UTILITY)) { //electric NHH
                        $this->widget('bootstrap.widgets.TbGridView', array(
                            'id' => 'price-grid1',
                            'dataProvider' => $price->search($model->id),
                            'filter' => $price,
                            'afterAjaxUpdate' => 'function(id,options){ paginationControls(); }',
                            'columns' => array(
                                'assigned_to' => array(
                                    'filter' => false, //CHtml::listData(Users::getAllSuppliers(), 'id', 'fullname'),
                                    'type' => 'html',
                                    'header' => 'Supplier',
                                    'name' => 'assigned_to',
                                    'value' => 'CHtml::image(Yii::app()->createAbsoluteUrl("admin/users/thumbnail/file/".$data->productName->user->image))',
                                    'htmlOptions' => array('class' => 'table-images')
                                ),
                                'product_id' => array(
                                    'name' => 'product_id',
                                    'value' => '$data->productName->product_name',
                                    'filter' => false
                                ),
                                'region' => array(
                                    'name' => 'region',
                                    'filter' => false,
                                ),
                                'mtc' => array(
                                    'name' => 'mtc',
                                    'filter' => false,
                                ),
                                'profile' => array(
                                    'name' => 'profile',
                                    'filter' => false,
                                ),
                                'llf' => array(
                                    'filter' => false,
                                    'name' => 'llf'
                                ),
                                //  'unit_charge',
                                'product' => array(
                                    'header' => 'Set Name',
                                    'filter' => false,
                                    'name' => 'product'
                                ),
                            //        array(
                            //            'class' => 'bootstrap.widgets.TbButtonColumn',
                            //        ),
                            ),
                        ));
                    } else {
                        $this->widget('bootstrap.widgets.TbGridView', array(
                            'id' => 'price-grid',
                            'dataProvider' => $price->search($model->id),
                              'afterAjaxUpdate' => 'function(id,options){ paginationControls(); }',
                            'filter' => $price,
                            'columns' => array(
                                'acc_start' => array(
                                    'name' => 'acc_start',
                                    'filter' => false,
                                    'value' => '($data->productName)? date("d-m-Y", strtotime($data->productName->acc_start)) : date("d-m-Y")'
                                ),
                                'acc_end' => array(
                                    'name' => 'acc_end',
                                    'filter' => false,
                                    'value' => '($data->productName)? date("d-m-Y", strtotime($data->productName->acc_end)) : date("d-m-Y")'
                                ),
                                // .'end_date',
                                'valid_from' => array(
                                    'name' => 'valid_from',
                                    'filter' => false,
                                    'value' => '($data->productName)? date("d-m-Y", strtotime($data->productName->valid_from)) : date("d-m-Y")'
                                ),
                                'valid_till' => array(
                                    'name' => 'valid_till',
                                    'filter' => false,
                                    'value' => '($data->productName)? date("d-m-Y", strtotime($data->productName->valid_till)) : date("d-m-Y")'
                                ),
                                //        'product_id' => array(
                                //            'name' => 'product_id',
                                //            'value' => '$data->productName->product_name',
                                //            'filter'=>false
                                //        ),
                                'region' => array(
                                    'name' => 'region',
                                    'filter' => false,
                                ),
                                'standing_charge' => array(
                                    'name' => 'standing_charge',
                                    'filter' => false,
                                ),
                                'unit_charge' => array(
                                    'name' => 'unit_charge',
                                    'filter' => false,
                                ),
                                'contract_length' => array(
                                    'name' => 'contract_length',
                                    'filter' => false,
                                    'value' => '($data->contract_length) ? $data->contract_length : $data->term'
                                ),
                                'cod' => array(
                                    'name' => 'cod',
                                    //  'value' => 'Y',
                                    'filter' => false,
                                ),
                                'smart_mtr' => array(
                                    'name' => 'smart_mtr',
                                    //'value' => 'Y',
                                    'filter' => false,
                                ),
                            )
                        ));
                    }
                    /**
                     * Please do not delete below commented code. It is for future use. Thank you
                     */
                    //        $coulmns = unserialize($model->columns_name);
                    //        if (!empty($coulmns)) {
                    //            $this->widget('bootstrap.widgets.TbGridView', array(
                    //                'id' => 'price-grid',
                    //                'dataProvider' => $price->search($model->id),
                    //                'filter' => $price,
                    //                'columns' => $coulmns,
                    //            ));
                    //        }
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    paginationControls();
</script>