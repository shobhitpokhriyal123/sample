<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('utility')); ?>:</b>
	<?php echo CHtml::encode($data->utility); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('corporate_sme')); ?>:</b>
	<?php echo CHtml::encode($data->corporate_sme); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('product_name')); ?>:</b>
	<?php echo CHtml::encode($data->product_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('volume_translate_min')); ?>:</b>
	<?php echo CHtml::encode($data->volume_translate_min); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('volume_translate_max')); ?>:</b>
	<?php echo CHtml::encode($data->volume_translate_max); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('max_commission')); ?>:</b>
	<?php echo CHtml::encode($data->max_commission); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('commission_increament')); ?>:</b>
	<?php echo CHtml::encode($data->commission_increament); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('commission_banded')); ?>:</b>
	<?php echo CHtml::encode($data->commission_banded); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('standing_charge')); ?>:</b>
	<?php echo CHtml::encode($data->standing_charge); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('direct_dabit')); ?>:</b>
	<?php echo CHtml::encode($data->direct_dabit); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('renewal')); ?>:</b>
	<?php echo CHtml::encode($data->renewal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('renewal_obligation')); ?>:</b>
	<?php echo CHtml::encode($data->renewal_obligation); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('feed_in_tarriff')); ?>:</b>
	<?php echo CHtml::encode($data->feed_in_tarriff); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contact_for_difference')); ?>:</b>
	<?php echo CHtml::encode($data->contact_for_difference); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('capacity_machanism')); ?>:</b>
	<?php echo CHtml::encode($data->capacity_machanism); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('distribution_charge')); ?>:</b>
	<?php echo CHtml::encode($data->distribution_charge); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('transmission_charge')); ?>:</b>
	<?php echo CHtml::encode($data->transmission_charge); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('balancing_charge')); ?>:</b>
	<?php echo CHtml::encode($data->balancing_charge); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('direct_dabit_mandate')); ?>:</b>
	<?php echo CHtml::encode($data->direct_dabit_mandate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('t_and_cs')); ?>:</b>
	<?php echo CHtml::encode($data->t_and_cs); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />

	*/ ?>

</div>