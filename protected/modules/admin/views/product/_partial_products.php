<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'product-filter-forms',
    'enableAjaxValidation' => false,
    //'action' => $this->createAbsoluteUrl("users/types"),
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
?>

<div class="control-group" id="">
    <label for="Users_sub_type_id">Utility</label>
    <?php
    $model->sub_type_id = explode(",", $model->sub_type_id);
    echo $form->dropDownList($product, 'utility', CHtml::listData(
                    $model->getSubProductTypes(), 'id', 'title'), array("empty" => 'All')
    );
    ?>
</div>
<?php $this->endWidget(); ?>
<div class="control-group">
    <button type="button" class="btn btn-primary" id="filter_submit">Save</button>
</div>
<div id="p_grid">
    <?php
    $this->widget('bootstrap.widgets.TbGridView', array(
        'id' => 'product-grid',
        'dataProvider' => $product->search($model->id),
        'filter' => $product,
        'columns' => array(
            //  'id',
//                'valid_from',
//                'valid_till',
//                'valid_from'=> array(
//                    'name' => 'valid_from',
//                    'value' => 'date("Y-m-d", strtotime($data->valid_from))',
//                ),
//                'valid_till'=> array(
//                    'name' => 'valid_from',
//                    'value' => 'date("Y-m-d", strtotime($data->valid_from))',
//                ),
//                'assigned_to' => array(
//                    'filter' => CHtml::listData(Users::getAllSuppliers(), 'id', 'fullname'),
//                    'type' => 'html',
//                    'name' => 'assigned_to',
//                    'value' => 'CHtml::image(Yii::app()->createAbsoluteUrl("admin/users/thumbnail/file/".$data->user->image))'
//                ),
            'utility' => array(
                'name' => 'utility',
                'filter' => false, // Product::getUtilites(),
                'value' => 'Product::getUtilites($data->utility)',
            ),
            'corporate_sme' => array(
                'name' => 'corporate_sme',
                'filter' => false, // Product::getCorporateSME(),
                'value' => 'Product::getCorporateSME($data->corporate_sme)',
            ),
            'product_name' => array(
                'name' => 'product_name',
                'filter' => false
            ),
//                'status' => array(
//                    'name' => 'status',
//                    'value' => 'Product::getStatus($data->status)',
//                    'filter' => false,
//                ),
            /* 'volume_translate_min',
              'volume_translate_max',
              'max_commission',
              'commission_increament',

              'commission_banded',
              'standing_charge',
              'direct_dabit',
              'renewal',
              'renewal_obligation',
              'feed_in_tarriff',
              'contact_for_difference',
              'capacity_machanism',
              'distribution_charge',
              'transmission_charge',
              'balancing_charge',
              'direct_dabit_mandate',
              't_and_cs',
              'status',
              'created',
             */
//          array(
//                    'class' => 'bootstrap.widgets.TbButtonColumn',
//                    'template' => '{activate}{deactivate}',
//                    'header' => 'Active',
//                    'buttons' => array(
//                        'activate' => array(
//                            'label' => '&nbsp;&nbsp;<i class="icon-ok"></ok>',
//                            'visible' => '$data->status > 0', // <-- SHOW IF ROW INACTIVE
//                            'url' => 'Yii::app()->createAbsoluteUrl("admin/product/changeStatus", array("id"=>$data->id))',
//                            'options' => array('title' => 'Deactivate product'),
//                            'imageUrl' => false,
//                            'click' => "function(e){
//                                var url = $(this).attr('href');
//                                e.preventDefault();
//                                $.ajax({
//                                 type: 'POST',
//                                 url: url,
//                                 success: function(res){
//                                  $.fn.yiiGridView.update('product-grid');
//                                  
//                                },
//                                });
//                           
//                                return false;
//                            }",
//                        ),
//                        'deactivate' => array(
//                            'label' => '&nbsp;&nbsp;<i class="icon-remove"></i>',
//                            'visible' => '$data->status == 0', // <-- SHOW IF ROW ACTIVE
//                            'url' => 'Yii::app()->createAbsoluteUrl("admin/product/changeStatus", array("id"=>$data->id))',
//                            'options' => array('title' => 'Activate product'),
//                            'imageUrl' => false,
//                            'click' => "function(e){
//                                 e.preventDefault();
//                                var url = $(this).attr('href');
//                                $.ajax({
//                                 type: 'POST',
//                                 url: url,
//                                 success: function(res){
//                                   $.fn.yiiGridView.update('product-grid');
//                                },
//                                });
//                               
//                                return false;
//                            }",
//                        ),
//                    ),
//                ),
                array(
                    'class' => 'bootstrap.widgets.TbButtonColumn',
                    'template' => '{update}',
                    'header' => 'Update',
   
                ),
        ),
    ));
    ?>
</div>

<script>
    $(document).ready(function () {
        $("#filter_submit").click(function (e) {
            e.preventDefault();
            var val = $("#Product_utility").val();
            var url = '<?php echo Yii::app()->createAbsoluteUrl("admin/product/products/sid/" . $model->id); ?>';
            if (val) {
                var url = '<?php echo Yii::app()->createAbsoluteUrl("admin/product/products/sid/" . $model->id); ?>/&Product[utility]=' + val;
            }
            $("#partial_part").load(url);

            return false;
        });
    });
</script>





