<section id="update_product">
    <div class="container">
        <?php
            $this->breadcrumbs = array(
                'Products' => array('index'),
                $model->id => array('view', 'id' => $model->id),
                'Update',
            );

            $this->menu = array(
                array('label' => 'List Product', 'url' => array('index')),
                array('label' => 'Create Product', 'url' => array('create')),
                array('label' => 'View Product', 'url' => array('view', 'id' => $model->id)),
                array('label' => 'Manage Product', 'url' => array('admin')),
            );
        ?>
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <strong>Update Product <?php echo $model->id; ?></strong>
                    </div>
                    <?php
                        if (Yii::app()->user->isAdmin) {
                            echo CHtml::link('Manage product', array('/admin/product/admin'), array("class" => 'btn btn-success'));
                        }
                    ?>
                </div>
                <div class="card-body">
                    <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
                </div>
            </div>
        </div>
    </div>
</section>
