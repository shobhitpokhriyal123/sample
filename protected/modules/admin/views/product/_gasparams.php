<?php


    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'quote-form',
        'enableAjaxValidation' => false,
        'method' => 'get',
        'enableClientValidation' => true,
        'action' => $this->createUrl("product/searchQuote"),
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    ));
?>


<div class="row">

        <div id="mpr_id" class="">
            <?php // echo $form->textFieldRow($model, 'mpr', array('class' => 'span4')); ?>
        </div>

        <div id="postcode_id" class="col-sm-2">
            <?php echo $form->textFieldRow($model, 'postcode', array('class' => 'form-control')); ?>
        </div>


        <div class="col-sm-2">
            <?php echo $form->dropdownListRow($model, 'assigned_to', CHtml::listData(Users::model()->findAllByAttributes(array('role_id' => 2)), 'id', 'fullname'), array('empty' => '--- Select Supplier ---', 'class' => 'form-control')); ?>
        </div>

        <div class="col-sm-3">
            <?php echo $form->dropdownListRow($model, 'business_type',CHtml::listData(BusinessType::model()->findAll(),'id','title') , array('empty' => '--- Select Business Type ---', 'class' => 'form-control')); ?>
        </div>



        <div class="col-sm-3">
            <?php echo $form->textFieldRow($model, 'aq', array('class' => 'form-control','required'=>'required')); ?>
        </div>

        <div class="col-sm-2">
            <?php echo $form->label($model, 'start_date'); ?>
      
            <?php
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $model,
                    'attribute' => 'start_date',
                    'options' => array(
                        'dateFormat' => 'yy-mm-dd',
                        'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                        'changeMonth' => true,
                        'changeYear' => true,
                         'minDate'=>'new Date() +1 day'
                    ),
                    'htmlOptions'=>array('class'=>'form-control')
                ));
            ?>
              <?php echo $form->error($model, 'start_date'); ?>
        </div>

</div>




<br>
<button type="submit" name="Search Tariff" class="btn btn-primary longBtn" >
    Search Tariff
</button>

<?php $this->endWidget(); ?>

<script>
    $(function () {
        $('#Product_postcode').keyup(function () {
            this.value = this.value.toUpperCase();
        });
    });
</script>
