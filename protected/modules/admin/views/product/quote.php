<style>
    .radio_Box {
        padding: 10px 15px;
        background: #d5d5d5;

    }
    #conetnts_ {
        padding: 15px;
    }

    #conetnts_ label {
        padding-top: 10px;
    }
</style>

<section id="quote_product">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <strong id="u_types">Utility Types</strong>
                    </div>
                </div>
                <div class="card-body">

                    <div class="btn-group">

                        <button type="button"  required="required"   class=" btn btn-success" id="Product_corporate_2" value="2" >SME</button>
                        <?php if (Users::isPermission(Yii::app()->user->id, 8)) { ?>
                            <a  id="corporate_business" class="btn btn-success" href="<?php echo Yii::app()->createAbsoluteUrl('/admin/leadContacts/create'); ?>" value="1" >Corporate</a>
                        <?php } ?>
                    </div> 

                    <hr>
                    <div id="sme_id" class="btn-group" style="display: none">
                        <button type="button"  required="required"  onclick="getContract($(this).val())"  class="btn btn-success" id="Product_contract_type_1" value="2" name="Product[contract_type]" >Electricity</button>

                        <button type="button"  onclick="getContract($(this).val())" required="required"  class="btn btn-success" id="Product_contract_type_0" value="1" name="Product[contract_type]" >Gas</button>
                    </div> 


                    <div id="conetnts_">

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<script type="text/javascript">
    $("#Product_corporate_2").click(function () {
        $("#u_types").html("SME Live Market Rates");
        $("#sme_id").show();
        $("#business_id").hide();
    });
    $('.abc').hide();
    $("#mpr_id").hide();
    $("#postcode_id").hide();
    function getContract(val) {
        $(".second").show();
        if (val == 1) {
            var gasurl = '<?php echo Yii::app()->createAbsoluteUrl("admin/product/getParams/type/gas"); ?>';
            $("#conetnts_").load(gasurl);
            $("#dfgdfg").show();
            $(".second").html("Gas");
        } else {
            var gasurl1 = '<?php echo Yii::app()->createAbsoluteUrl("admin/product/getParams/type/elec"); ?>';
            $("#conetnts_").load(gasurl1);
            $("#dfgdfg").show();

            $(".second").html("Electric");
        }
    }

    function getBasis(id) {
        $('.abc').show();
        $(".btn-success").show();
        if (id == 1) {
            $("#mpr_id").show();
            $("#postcode_id").hide();
            $("#Product_postcode").val("");
        } else {
            $("#Product_mpr").val("");
            $("#postcode_id").show();
            $("#mpr_id").hide();
        }

    }
    $("#corporate_business").click(function (e) {
        e.preventDefault();
        var business_url = '<?php echo Yii::app()->createAbsoluteUrl("admin/leadContacts/business"); ?>';
        $("#conetnts_").load(business_url);
        $("#business_id").show();
        $('#sme_id').hide();

    });


    function   getTenderForm(obj) {
        console.log();
        var url = '<?php echo Yii::app()->createAbsoluteUrl("/admin/leadContacts/tenderForm") ?>';
        $.ajax({
            url: url,
            type: 'POST',
            data: $("#meter_selection").serialize(),
            success: function (res) {
                $("#tender_part").html(res);
            }
        });
    }
</script>
