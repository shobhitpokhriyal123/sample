<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'quote-form',
    'enableAjaxValidation' => false,
    'method' => 'get',
    'enableClientValidation' => true,
    'action' => $this->createUrl("product/searchEQuote"),
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
?>

<hr>

<div class="row">
    <div class="btn-group">
        <?php echo $form->dropDownListRow($model, 'priceType', $model->getPriceTypes(), array('class' => 'form-control', 'onchange' => 'showHide($(this).val())')); ?>
    </div> 
    <hr>
    <div class="col-sm-2"  id="pcode_id" style="display: none">
        <div>
            <?php echo $form->textFieldRow($model, 'postcode', array('class' => 'form-control')); ?>
        </div>
    </div>
    <div class="col-sm-2" id="dist_id" >
        <div>
            <?php echo $form->dropDownListRow($model, 'distributor', $model->getDistributors(), array('class' => 'form-control')); ?>
        </div>
    </div>
    <div class="col-sm-2">
        <div>
            <?php echo $form->dropdownListRow($model, 'assigned_to', CHtml::listData(Users::model()->findAllByAttributes(array('role_id' => 2)), 'id', 'fullname'), array('empty' => '--- Select Supplier ---', 'class' => 'form-control')); ?>
        </div>
    </div>
    <div class="col-sm-2">
        <div>
            <?php echo $form->dropDownListRow($model, 'meter_type', MeterType::getMeterTypes(), array('class' => 'form-control')); ?>
        </div>
    </div>

    <div class="col-sm-1">
        <div>
            <?php echo $form->dropDownListRow($model, 'profile', $model->getProfile(), array('class' => 'form-control')); ?>
        </div>
    </div>
    <div class="col-sm-3">
        <div>
            <?php echo $form->textFieldRow($model, 'aq', array('class' => 'form-control', 'required' => 'required')); ?>
        </div>
    </div>
    <div class="col-sm-2">
        <div id="postcode_xid">
          <?php echo $form->label($model, 'start_date'); ?>
      
            <?php
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $model,
                    'attribute' => 'start_date',
                    'options' => array(
                        'dateFormat' => 'yy-mm-dd',
                        'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                        'changeMonth' => true,
                        'changeYear' => true,
                         'minDate'=>'new Date() +1 day'
                    ),
                    'htmlOptions'=>array('class'=>'form-control')
                ));
            ?>
              <?php echo $form->error($model, 'start_date'); ?>
        </div>
    </div>

</div>


<br>
<button type="submit" name="Search Tariff" class="btn btn-primary longBtn" >
    Search Tariff
</button>

<?php $this->endWidget(); ?>

<script>
    showHide(1);
    function showHide(val) {
        if (val == 1) {
            $("#pcode_id").hide();
            $("#dist_id").show();
            $("#Product_distributor").attr("required", "required");
            $("#Product_postcode").removeAttr("required");
            $("#Product_postcode").val("");
        }
        if (val == 2) {
            $("#pcode_id").show();
            $("#dist_id").hide();
            $("#Product_postcode").attr("required", "required");
            $("#Product_distributor").removeAttr("required");
            $("#Product_distributor").val("");
        }
    }
    $(function () {
        $('#Product_postcode').keyup(function () {
            this.value = this.value.toUpperCase();
        });
    });

    function getMeterType(sid) {
        var url = '<?php echo Yii::app()->createAbsoluteUrl("admin/meterType/getMeters") ?>&sid=' + sid;
        //  alert(url);
        $("#Product_meter_type").load(url);
    }
</script>
