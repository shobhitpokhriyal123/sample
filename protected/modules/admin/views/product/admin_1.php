<div class="row-fluid">
    <div class="span12 well">
        <?php if (Yii::app()->user->hasFlash('success')): ?>
            <div class="info">
                <?php echo Yii::app()->user->getFlash('success'); ?>
            </div>
        <?php endif; ?>
        <h1>Manage Products</h1>
        <div class="right">
            <?php
            if (Yii::app()->user->isAdmin) {
                echo CHtml::link('Create product', array('/admin/product/create'), array("class" => 'btn btn-success'));
            }
            ?>
        </div>
        <br>

        <?php
        $this->widget('bootstrap.widgets.TbGridView', array(
            'id' => 'product-grid',
            'dataProvider' => $model->search(),
            'filter' => $model,
            'columns' => array(
                //  'id',
                'valid_from',
                'valid_till',   
                'assigned_to' => array(
                    'filter' => false,
                    'type' => 'html',
                    'name' => 'assigned_to',
                    'value' => 'CHtml::image(Yii::app()->createAbsoluteUrl("admin/users/thumbnail/file/".$data->user->image))'
                ),
                'product_name',
                 'status'=>array(
                     'name'=> 'status',
                     'value'=> 'Product::getStatus($data->status)',
                      'filter'=>false,
                 ),
//                 'utility' => array(
//                    'name' => 'utility',
//                    'filter' => Product::getUtilites(),
//                    'value' => 'Product::getUtilites($data->utility)',
//                ),
//                'corporate_sme' => array(
//                    'name' => 'corporate_sme',
//                    'filter' => Product::getCorporateSME(),
//                    'value' => 'Product::getCorporateSME($data->corporate_sme)',
//                ),
                /* 'volume_translate_min',
                  'volume_translate_max',
                  'max_commission',
                  'commission_increament',

                  'commission_banded',
                  'standing_charge',
                  'direct_dabit',
                  'renewal',
                  'renewal_obligation',
                  'feed_in_tarriff',
                  'contact_for_difference',
                  'capacity_machanism',
                  'distribution_charge',
                  'transmission_charge',
                  'balancing_charge',
                  'direct_dabit_mandate',
                  't_and_cs',
                  'status',
                  'created',
                 */
                array(
                    'class' => 'bootstrap.widgets.TbButtonColumn',
                   'template'=>'{view}{delete}',
                ),
            ),
        ));
        ?>

    </div>      
</div>
<?php //print_r($csv);      ?>

