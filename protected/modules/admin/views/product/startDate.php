
<section id="create_product"  style="min-height: 2000px !important">
    <div class="container-fluid">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <strong>Update Start Date</strong>
                    </div>
                </div>
                <div class="card-body">
                    <table style="width:100%">
                        <tr>

                            <th>Utility</th>
                            <th>Supplier</th>
                            <th>Corporate/SME</th>
                            <th>Start Date</th>
                            <th>Update Start date</th>
                              <th>Update End date</th>
                            <th>Action</th>
                        </tr>
                        <?php
                        // echo '<pre>'; print_r($models); die;

                        foreach ($models as $model) {
                            // echo '<pre>'; print_r($model->productName); die;
                            $supplier = Product::getSupplierName($model->id);
                            ?>
                            <?php
                            $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                                'id' => 'product-fssorm' . $model->id,
                                'enableAjaxValidation' => false,
                                'enableClientValidation' => true,
                                'clientOptions' => array(
                                    'validateOnSubmit' => true,
                                ),
                                'htmlOptions' => array('enctype' => 'multipart/form-data'),
                            ));

//                            if ($model->m_setting == '0') {
//                                $model->m_setting = '';
//                            }
                            ?>

                            <tr>

                                <td><?php echo (!$model->productName) ? "N/A" : $model->productName->type->title ?></td>
                                <td><?php echo $supplier; ?></td>
                                <td><?php echo (!$model->productName) ? "N/A" : $model->productName->ctype->title ?></td>
                                <td><?php echo $model->start_date; ?></td>
                                <td><?php //echo $model->start_from_date;
                                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                        'model' => $model,
                                        'attribute' => 'start_from_date',
                                         'id' => 'start_from_date'.$model->id,
                                        'options' => array(
                                            'dateFormat' => 'yy-mm-dd',
                                            'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                                            'changeMonth' => true,
                                            'changeYear' => true,
                                        ),
                                    ));
                                    ?></td>
                                <td><?php
                                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                        'model' => $model,
                                        'attribute' => 'end_till_date',
                                         'id' => 'end_till_date_'.$model->id,
                                        'options' => array(
                                            'dateFormat' => 'yy-mm-dd',
                                            'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                                            'changeMonth' => true,
                                            'changeYear' => true,
                                        ),
                                    ));
                                    echo $form->hiddenField($model, 'start_date', array('value' => $model->start_date)); ?>
                                    
                                </td>
<!--                                <td> -->
                                    <?php
                                    // echo $model->m_setting;
//                                    if ($model->id)
//                                        $model->m_setting = explode(",", $model->m_setting);
//                                    echo CHtml::activeListBox($model, 'm_setting', Product::getMonths(), array('class' => 'chosen form-control', 'multiple' => true, 'data-placeholder' => 'Select'))
//                                    ?>
                                    <?php
                                    //  echo '<pre>'; print_r($model->m_setting); 

                                   // echo $form->hiddenField($model, 'start_date', array('value' => $model->start_date));
                                    ?>

<!--                                </td>  -->
                                <td><button type="submit" class="btn btn-success"> Save</button></td>
                            </tr>
                            <?php $this->endWidget(); ?>
                        <?php } ?>
                    </table> 
                </div>
            </div>
        </div>
    </div>
</section>
<?php
//$this->widget('ext.chosen.EChosenWidget', array(
//    'selector' => '.chosen',
//));
?>

<style>
    .chosen-container {
        width: 80% !important;
    }
    /*    body{
            min-height:200%;
        }*/
</style>