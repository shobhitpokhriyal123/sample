
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<section id="csv_mapping">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="card-title">
                        <strong>Update Mapping</strong>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table-bordered table-striped table" style="width:100%">
                        <tr>
                             <th>Product Name</th>
                            <th>Supplier</th>
                            <th>Utility</th>
                            <th>Corporate/SME</th>
                            <th>Curr DB Attr</th>
                            <th>Update Mapping with</th>
                            <th>Actions</th>
                        </tr>

                        <?php
                        $model1 = new Product;
                        if ($models) {
                        //    echo '<pre>'; print_r($models);  die;
                            foreach ($models as $model) {
                                if($model->field){
                                $fields = unserialize($model->field->fields);

                                $attributes_array = Price::getAllAttributes();
                                //echo '<pre>'; print_r($fields); die;
                                ?>

                                <tr>
                                <form method="post" name="mapping_form" action="" id="mapping_form_<?php echo $model->id ?>">
                                    <td><?php echo $model->product_name ?></td>
                                    <td class="table-images"><?php
                                        $user = Users::model()->findByPk($model->assigned_to);
                                        echo $user->username; 
                                       // echo CHtml::image(Yii::app()->createAbsoluteUrl("/admin/users/thumbnail/file/" . $user->image)); //
                                        ?></td>
                                    <td><input type="hidden" name="p_utility" value="<?php echo $model->utility; ?>"/>
                                        <input type="hidden" name="p_model" value="<?php echo $model->id; ?>"/>
                                        <input type="hidden" name="p_corporate" value="<?php echo $model->corporate_sme; ?>"/>
                                        <input type="hidden" name="p_user" value="<?php echo $model->assigned_to; ?>"/>
                                        <?php echo $model1->getUtilites($model->utility) ?></td>
                                    <td><?php echo $model1->getCorporateSME($model->corporate_sme) ?></td>
                                    <td>
                                      
                                        
                                        <select required="required"  id="current_attribute_<?php echo $model->id ?>"  name="current_attribute"  >
                                            <option  value="">Select Database Attributes</option>
                                            <?php foreach ($fields as $key => $attributes_arr) { ?>
                                                <option  value="<?php echo $attributes_arr; ?>" ><?php echo $attributes_arr ?></option>
                                            <?php } ?>
                      
                                        </select>
                                    </td>
                                                                      <td>
                                        <select  required="required"   id="pammped_attribute_<?php echo $model->id ?>"  name="mapped_attribute"  >
                                            <option  value="">Select Database Attributes</option>
                                            <?php foreach ($attributes_array as $key => $attributes_arr) { ?>
                                                <option  value="<?php echo $key; ?>" ><?php echo $attributes_arr ?></option>
                                            <?php } ?>
                                        </select>
                                    </td>
                                    <td><button class="btn btn-primary">Update</button></td>
                                </form>
                                </tr>

                            <?php
                                }
                                }
                        } else {
                            echo '<tr><td colspan="5">No record(s) found..!</td></tr>';
                        }
                        ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $('select').select2();
</script>
