<style>
    .container-fluid {
        margin-left: 20px;
        margin-right: 20px;
    }

    /* Input field with increment/decrement */
    .inputWrapper {
        position: relative;
        width: 90px;
        height: 37px;
        border-radius: 3px;
        overflow: hidden;
    }
    .inputWrapper .txtInput {
        display: block;
        width: 80%;
        height: 100%;
        position: absolute;
        left: 0;
        padding-left: 5px;
    }
    .cusWrap {
        position: absolute;
        width: 20%;
        height: 100%;
        right: 0;
    }
    .cusWrap .cusBTN {
        display: block;
        width: 100%;
        height: 50%;
        padding: 0;
        line-height: 4px;
        border: 1px solid #ccc;
    }

    table input[type="text"] {
        width: 65px;
        height: 36px;
    }
    .fBTN {
        float: right;
    }
    .ratebox {
        width: 170px;
    }
    .ratebox p {
        margin: 0;
    }
    .ratebox span {
        display: inline-block;
        font-size: 0.8em;
    }
    .ratebox .tag {
        width: 100px;
        font-weight: bold;
    }
    table th, table td {
        font-size: 0.8em;
    }
</style>
<section id="searchQuote">
    <div class="container-fluid">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <strong>Quote</strong>
                    </div>
                </div>
                <div class="card-body">

                    <div class="well">
                    <?php
                        $get = $_GET['Product'];
                          $supplier = $user_id = $get['assigned_to'];
                          $supplier = Users::model()->findByPk($supplier);
                          //echo '<pre>'; print_r($get); die;
                        $mpr = '';
                        $pcode = '';
                        if (isset($get['postcode']) && $get['postcode']) {
                    ?>
                        <strong>Postcode:</strong> <?php echo $pcode = $get['postcode'] ?> <br>
                    <?php } else if (isset($get['mpr']) && $get['mpr']) { ?>
                        <strong>MPR:</strong> <?php echo $mpr = $get['mpr'] ?> <br>
                    <?php } ?>

                        <strong>Start Date:</strong> <?php echo $date = $get["start_date"]; ?> <br>
                        <strong>AQ:</strong> <?php echo $aq = $get['aq']; ?> <br>
                        <strong>Meter Type:</strong> <?php  $meter = $get["meter_type"]; 
                        echo MeterType::getMeterTypes($meter);
                        ?> <br>
                        <strong>Profile:</strong> <?php echo $profile = $get["profile"]; ?> <br>
                        <strong>Distributer:</strong> <?php echo $dist = isset($get["distributor"]) ? $get["distributor"] : ""; ?> <br>
                    </div>
     <script>
            var current_supplier = '<?php echo $supplier->id; ?>';
            var aqu = '<?php echo $aq; ?>';
            
        </script>
                    <hr>

                    <?php 
                        $url12 = $this->createAbsoluteUrl("/admin/price/getElcMonthData/month/12&Price[distributor]=$dist&Price[start_date]=$date&Price[max_aq]=$aq&Price[min_kwh]=$mpr&Price[profile]=$profile&Price[postcode]=$pcode&Price[meter_type]=$meter");
                        $url24 = $this->createAbsoluteUrl("/admin/price/getElcMonthData/month/24&Price[distributor]=$dist&Price[start_date]=$date&Price[max_aq]=$aq&Price[min_kwh]=$mpr&Price[profile]=$profile&Price[postcode]=$pcode&Price[meter_type]=$meter");
                        $url36 = $this->createAbsoluteUrl("/admin/price/getElcMonthData/month/36&Price[distributor]=$dist&Price[start_date]=$date&Price[max_aq]=$aq&Price[min_kwh]=$mpr&Price[profile]=$profile&Price[postcode]=$pcode&Price[meter_type]=$meter");
                        $url48 = $this->createAbsoluteUrl("/admin/price/getElcMonthData/month/48&Price[distributor]=$dist&Price[start_date]=$date&Price[max_aq]=$aq&Price[min_kwh]=$mpr&Price[profile]=$profile&Price[postcode]=$pcode&Price[meter_type]=$meter");
                        $url60 = $this->createAbsoluteUrl("/admin/price/getElcMonthData/month/60&Price[distributor]=$dist&Price[start_date]=$date&Price[max_aq]=$aq&Price[min_kwh]=$mpr&Price[profile]=$profile&Price[postcode]=$pcode&Price[meter_type]=$meter");
                        $all = $this->createAbsoluteUrl("/admin/price/getElcMonthData/month/123&Price[distributor]=$dist&Price[start_date]=$date&Price[max_aq]=$aq&Price[min_kwh]=$mpr&Price[profile]=$profile&Price[postcode]=$pcode&Price[meter_type]=$meter");
                        $this->widget('zii.widgets.jui.CJuiTabs', array(
                            'tabs' => array(
                                '12 Months' => array('ajax' => $url12),
                                '24 Months' => array('ajax' => $url24),
                                '36 Months' => array('ajax' => $url36),
                                '48 Months' => array('ajax' => $url48),
                                '60 Months' => array('ajax' => $url60),
                                'All Terms' => array('ajax' => $all),
                            ),
                            // additional javascript options for the tabs plugin
                            'options' => array(
                                'collapsible' => true,
                            ),
                        ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    paginationControls();

</script>

