<section id="create_product">
    <div class="container">
        <div class="row">
            <?php
                $this->breadcrumbs = array(
                    'Products' => array('index'),
                    'Create',
                );

                $this->menu = array(
                    array('label' => 'List Product', 'url' => array('index')),
                    array('label' => 'Manage Product', 'url' => array('admin')),
                );
            ?>
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <strong>Create Product</strong>
                    </div>
                    <?php
                        if (Yii::app()->user->isAdmin) {
                            echo CHtml::link('Manage product', array('/admin/product/admin'), array("class" => 'btn btn-success'));
                        }
                    ?>
                </div>
                <div class="card-body">
                    <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
                </div>
            </div>
        </div>
    </div>
</section>


<script>
    $('.btn').each(function () {
        if($(this).text() == 'Next') {
            $(this).addClass('longBtn').css('margin-top','10px');
        }
    });
</script>
