<div class="modal-content">

    <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

        <h4 class="modal-title">Choose the fields to multiply by 100</h4>

    </div>

    <div class="modal-body">

        <?php
        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'calculation-form',
            'enableAjaxValidation' => false,
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
            ),
            'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
        ?>

        <p class="help-block">Fields with <span class="required">*</span> are required.</p>
        <hr>
        <?php //echo $form->errorSummary($model); ?>
        <div class="">
            <?php echo '<b>Utility:</b> ' . $model->getUtilites($model->utility) . ''; ?>
            <?php echo '<b>Corporate/SME: </b>' . $model->getCorporateSME($model->corporate_sme) . ''; ?>
            <?php echo '<b>Supplier: </b>' . $model->user->fullname . ''; ?>
        </div>
        <hr>
        <?php  echo $form->dropDownListRow($model, 'dm', $model->getDM(), array('class' => 'form-control')); ?>
        <br>
        <?php $fields = array_combine($fields, $fields);
        echo $form->dropDownListRow($model, 'distributor', $fields, array('class' => 'form-control')); ?>
        <div class="clearfix"></div>
        <br>
        <div class="form-actions">
            <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType' => 'submit',
                'type' => 'primary',
                'label' => $model->isNewRecord ? 'Next' : 'Save',
                'htmlOptions' => array('class' => 'longBtn'),
            ));
            ?>
        </div>

        <?php $this->endWidget(); ?>

    </div>

    <!--    <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
        </div>-->

</div>

<style>


    .select2-container, .select2-drop, .select2-search, .select2-search input {
        box-sizing: border-box;
        min-width: 100px;
    }
</style>
