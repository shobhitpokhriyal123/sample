<section id="manage_productss">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="card-title">
                        <strong>Update Quote </strong>
                    </div>

                       <?php
                    if (Yii::app()->user->isCustomer) {
                        echo CHtml::link('Dashboard', array('/admin/users/view'), array("class" => 'btn btn-success'));
                    }
                    ?>

                </div>
                <div class="card-body">

                    <?php echo $this->renderPartial('_form', array('model' => $model)); ?>

                </div>
            </div>
        </div>
    </div>
</section>