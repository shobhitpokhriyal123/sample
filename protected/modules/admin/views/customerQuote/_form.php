<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'customer-quote-form',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
//echo '<pre>'; print_r($model);die;
$cid = '';;
if($model && $model->customer_id){
    $cid = $model->customer_id;
}
?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php //echo $form->errorSummary($model); ?>

<?php //echo $form->textFieldRow($model,'create_user_id',array('class'=>'span5')); ?>

<?php //echo $form->hiddenField($model,'customer_id',array('val'=>'span5')); ?>

<?php //echo $form->textFieldRow($model,'supplier_id',array('class'=>'span5')); ?>

<?php
if (Yii::app()->user->isCustomer) {
    //echo '<strong>'. $model->user->fullname.'</strong> <br>';
    //  echo CHtml::link('Dashboard', array('/admin/users/view'), array("class" => 'btn btn-success'));
    echo $form->dropdownListRow($model, 'supplier_id', CHtml::listData(Users::model()->findAllByAttributes(array('role_id' => 2, 'active' => 1)), 'id', 'fullname'), array('empty' => '--- Select Supplier ---', 'disabled' => 'disabled'));
} else {
    ?>
    <?php echo $form->dropdownListRow($model, 'supplier_id', CHtml::listData(Users::model()->findAllByAttributes(array('role_id' => 2, 'active' => 1)), 'id', 'fullname'), array('empty' => '--- Select Supplier ---')); ?>
<?php } ?>

<?php
if (Yii::app()->user->isCustomer) {
    //echo '<strong>'. $model->user->fullname.'</strong> <br>';
    //  echo CHtml::link('Dashboard', array('/admin/users/view'), array("class" => 'btn btn-success'));
   echo $form->dropdownListRow($model, 'site_id', CHtml::listData(CustomerSite::model()->findAllByAttributes(array('customer_id' =>$cid )), 'id', 'site_name'), array('empty' => '--- Select Site ---', 'readonly'=> true));
} else {
    ?>
    <?php echo $form->dropdownListRow($model, 'site_id', CHtml::listData(CustomerSite::model()->findAllByAttributes(array('customer_id' =>$cid )), 'id', 'site_name'), array('empty' => '--- Select Site ---')); ?>
<?php } ?>
<?php echo $form->textFieldRow($model, 'meter_no', array('class' => 'span5', 'maxlength' => 255)); ?>

<?php echo $form->textFieldRow($model, 'product', array('class' => 'span5', 'maxlength' => 255)); ?>

<?php echo $form->textFieldRow($model, 'min_volume', array('class' => 'span5', 'maxlength' => 255)); ?>

<?php echo $form->textFieldRow($model, 'max_volume', array('class' => 'span5', 'maxlength' => 255)); ?>

<?php echo $form->textFieldRow($model, 'credit_position', array('class' => 'span5')); ?>

<?php echo $form->fileFieldRow($model, 'supply_contract', array('class' => 'span5', 'maxlength' => 255)); ?>

<?php echo $form->dropdownListRow($model, 'offer_type', CustomerQuote::getOfferTypes()); ?>

<?php echo $form->textFieldRow($model, 'consumption', array('class' => 'span5', 'maxlength' => 255)); ?>
<?php echo $form->textFieldRow($model, 'standing_charge', array('class' => 'span5', 'maxlength' => 255)); ?>
<?php echo $form->textFieldRow($model, 'unit_rate', array('class' => 'span5', 'maxlength' => 255)); ?>

<?php //echo $form->textFieldRow($model, 'created', array('class' => 'span5'));  ?>

<div class="form-actions">
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type' => 'primary',
        'label' => $model->isNewRecord ? 'Create' : 'Save',
    ));
    ?>
</div>

<?php $this->endWidget(); ?>
