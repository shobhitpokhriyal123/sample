<section id="manage_productss">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="card-title">
                        <strong>Manage Quotes</strong>
                    </div>

                    <?php
                    if (Yii::app()->user->isAdmin  || Yii::app()->user->isAgency ) {
                        // echo CHtml::link('New Customer', array('/admin/agencyCustomer/create'), array("class" => 'btn btn-success'));
                        echo CHtml::link('Manage Customers', array('/admin/agencyCustomer/admin'), array("class" => 'btn btn-success'));
                        echo CHtml::link('Manage Sites', array('/admin/customerSite/admin'), array("class" => 'btn btn-success'));
                        echo CHtml::link('Manage Quotes', array('/admin/customerQuote/admin'), array("class" => 'btn btn-success'));

                        echo CHtml::link('Manage Meters', array('/admin/customerSiteMeter/admin'), array("class" => 'btn btn-success'));
                        echo CHtml::link('Manage Contracts', array('/admin/customerContract/admin'), array("class" => 'btn btn-success'));
                        echo CHtml::link('Manage Tenders', array('/admin/customerTender/admin'), array("class" => 'btn btn-success'));
                    }
                    ?>

                </div>
                <div class="card-body">
                    <?php
                    $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => 'customer-quote-grid',
                        'dataProvider' => $model->search(),
                        'filter' => $model,
                        'columns' => array(
//                            'id',
                            //  'create_user_id',
                            'customer_id' => array(
                                'name' => 'customer_id',
                                'filter' => false,
                                'value' => '$data->customer->customer_name'
                            ),
                            'supplier_id' => array(
                                'name' => 'supplier_id',
                                'filter' => false,
                                'type' => 'raw',
                                'value' => 'CHtml::image(Yii::app()->createAbsoluteUrl("admin/users/thumbnail/file/".$data->user->image))',
                                'htmlOptions' => array('class' => 'table-images')
                            ),
//                           
                            'product',
                            'min_volume',
                            'max_volume',
                            'credit_position',
                            'supply_contract',
                            'offer_type' => array(
                                'name' => 'offer_type',
                                'filter' => false,
                                'value' => 'CustomerQuote::getOfferTypes($data->offer_type)'
                            ),
                            'consumption',
                            /*  'created',
                             */
                            array(
                                'class' => 'bootstrap.widgets.TbButtonColumn',
                                'template' => '{view}{update}{delete}{print_quote}',
                                'header' => 'Tender',
                                'buttons' => array(
                                    'print_quote' => array(
                                        'label' => '<button class="btn btn-primary">Print Pdf Quote</button>',
                                        'url' => 'Yii::app()->createAbsoluteUrl("admin/customerQuote/pdf", array("id"=>$data->id))',
                                        //'visible' => '$data->status > 0', // <-- SHOW IF ROW INACTIVE
                                        // 'url' => 'Yii::app()->createAbsoluteUrl("admin/product/changeStatus", array("id"=>$data->id))',
                                        'options' => array('title' => 'Print Pdf Quote', 'target' => '_blank'),
                                        'imageUrl' => false,
                                    ),
                                ),
                            ),
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>