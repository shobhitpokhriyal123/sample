<section id="manage_productss">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="card-title">
                        <strong>Manage Quotes</strong>
                    </div>

                    <?php
                    if (Yii::app()->user->isCustomer) {
                        echo CHtml::link('Dashboard', array('/admin/users/view'), array("class" => 'btn btn-success'));
                    }
                    ?>

                </div>
                <div class="card-body">
                    <?php
                    $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => 'customer-quote-grid',
                        'dataProvider' => $model->search($c),
                        'afterAjaxUpdate' => 'function(id,options){ paginationControls(); }',
                        'filter' => $model,
                        'columns' => array(
//                            'id',
                            //  'create_user_id',
                            'customer_id' => array(
                                'name' => 'customer_id',
                                'filter' => false,
                                'value' => '$data->customer->customer_name'
                            ),
                            'supplier_id' => array(
                                'name' => 'supplier_id',
                                'filter' => false,
                                'type' => 'raw',
                                'value' => 'CHtml::image(Yii::app()->createAbsoluteUrl("admin/users/thumbnail/file/".$data->user->image))',
                                'htmlOptions' => array('class' => 'table-images')
                            ),
//                           
                            'product' => array(
                                'name' => 'product',
                                'filter' => false,
                            ),
                            'min_volume' => array(
                                'name' => 'min_volume',
                                'filter' => false,
                            ),
                            'max_volume' => array(
                                'name' => 'max_volume',
                                'filter' => false,
                            ),
                            'credit_position' => array(
                                'name' => 'credit_position',
                                'filter' => false,
                            ),
                            //'supply_contract',
                            'offer_type' => array(
                                'name' => 'offer_type',
                                'filter' => false,
                                'value' => 'CustomerQuote::getOfferTypes($data->offer_type)'
                            ),
                            'consumption' => array(
                                'name' => 'consumption',
                                'filter' => false,
                            ),
                            /*  'created',
                             */
                            array(
                                'class' => 'bootstrap.widgets.TbButtonColumn',
                                'template'=>'{view}{update}{delete}{print_quote}',
                                
                                 'header' => 'Tender',
                                'buttons' => array(
                                    'print_quote' => array(
                                        'label' => '<button class="btn btn-primary">Print Pdf Quote</button>',
                                        'url' => 'Yii::app()->createAbsoluteUrl("admin/customerQuote/pdf", array("id"=>$data->id))',
                                        //'visible' => '$data->status > 0', // <-- SHOW IF ROW INACTIVE
                                        // 'url' => 'Yii::app()->createAbsoluteUrl("admin/product/changeStatus", array("id"=>$data->id))',
                                        'options' => array('title' => 'Print Pdf Quote', 'target'=>'_blank'),
                                        'imageUrl' => false,
                                    ),
                                    'update'=>array(
                                      'visible'=>'Yii::app()->user->isAdmin'
                                    ),
                                    'delete'=>array(
                                      'visible'=>'Yii::app()->user->isAdmin'
                                    ),
                                ),
                                
                            ),
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>