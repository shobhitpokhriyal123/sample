<section id="manage_productss">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="card-title">
                        <strong>View Quote</strong>
                    </div>

                    <?php
                    if (Yii::app()->user->isCustomer) {
                        echo CHtml::link('Dashboard', array('/admin/users/view'), array("class" => 'btn btn-success'));
                    }
                    ?>

                </div>
                <div class="card-body">

                    <?php
                    $this->widget('bootstrap.widgets.TbDetailView', array(
                        'data' => $model,
                        'attributes' => array(
                            // 'id',
                            // 'create_user_id',
                            'customer_id' => array(
                                'name' => 'customer_id',
                                'value' => $model->customer->customer_name
                            ),
                            'supplier_id' => array(
                                'name' => 'supplier_id',
                                'value' => $model->user->username
                            ),
                           
                           
                            'site_id' => array(
                                'name' => 'site_id',
                                'value' => ($model->site) ? $model->site->site_name : "N/A"
                            ),
                            'meter_no',
                            'product',
                            'min_volume',
                            'max_volume',
                            'credit_position',
                            'supply_contract',
                            'offer_type',
                             'consumption',
                             'standing_charge',
                            'unit_rate',
                            'created',
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
