<?php
$this->breadcrumbs=array(
	'Customer Quotes',
);

$this->menu=array(
	array('label'=>'Create CustomerQuote','url'=>array('create')),
	array('label'=>'Manage CustomerQuote','url'=>array('admin')),
);
?>

<h1>Customer Quotes</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
