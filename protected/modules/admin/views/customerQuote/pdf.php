<section id="manage_productss">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <strong> Quote >>>   <?php echo $model->customer->customer_name. ' : '. $model->user->fullname ;  ?></strong>
                    </div>
                </div>
                <div class="card-body">
                    <?php
                    $this->widget('bootstrap.widgets.TbDetailView', array(
                        'data' => $model,
                        'attributes' => array(
//                            'id',
//                            'create_user_id',
                            'customer_id' => array(
                                'name' => 'customer_id',
                                'value' => $model->customer->customer_name
                            ),
                            'supplier_id' => array(
                                'name' => 'supplier_id',
                                'value' => $model->user->fullname
                            ),
                            'product' => array(
                                'name' => 'product',
                            //'value'=>$model->supplier->fullname
                            ),
                            'min_volume',
                            'max_volume',
                            'credit_position',
                          // 'supply_contract',
                            'offer_type'=>array(
                                'name'=>'offer_type',
                                'value'=>CustomerQuote::getOfferTypes($model->offer_type)
                            ),
                            'consumption',
                            'created',
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
