<?php
$this->breadcrumbs = array(
    'Contract Files' => array('index'),
    $model->id,
);

$this->menu = array(
    array('label' => 'List ContractFiles', 'url' => array('index')),
    array('label' => 'Create ContractFiles', 'url' => array('create')),
    array('label' => 'Update ContractFiles', 'url' => array('update', 'id' => $model->id)),
    array('label' => 'Delete ContractFiles', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Are you sure you want to delete this item?')),
    array('label' => 'Manage ContractFiles', 'url' => array('admin')),
);
?>

<h1>View ContractFiles #<?php echo $model->id; ?></h1>

<?php
$this->widget('bootstrap.widgets.TbDetailView', array(
    'data' => $model,
    'attributes' => array(
        'id',
//        'user_id' => array(
//            'name' => 'user_id',
//            //'filter'=>false,
//            'value' => $model->user->username
//        ),
        'contract_id' => array(
            'name' => 'contract_id',
        //'filter'=>false,
        //'value'=>$model->user->username
        ),
        'is_paper' => array(
            'name' => 'is_paper',
            'value' => ($model->is_paper == 1) ? "Paper" : "Audio"
        ),
       // 'is_copy_of_original',
        
        'is_copy_of_original'=> array(
                      'name' => 'is_copy_of_original',
                    'value' => Company::getTrueFalse($model->is_copy_of_original),
                 ),
        
        'files' => array(
            'type'=>'raw',
            'name'=>'files',
            'value'=> CHtml::link('Download',array('users/download/file/'.$model->files))
        ),
        'contract_signed_date',
    ),
));
?>
