<?php
$this->breadcrumbs=array(
	'Contract Files'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ContractFiles','url'=>array('index')),
	array('label'=>'Manage ContractFiles','url'=>array('admin')),
);
?>

<h1>Create ContractFiles</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>