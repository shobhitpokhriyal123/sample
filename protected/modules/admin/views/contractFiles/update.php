<?php
$this->breadcrumbs=array(
	'Contract Files'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ContractFiles','url'=>array('index')),
	array('label'=>'Create ContractFiles','url'=>array('create')),
	array('label'=>'View ContractFiles','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage ContractFiles','url'=>array('admin')),
);
?>

<h1>Update ContractFiles <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>