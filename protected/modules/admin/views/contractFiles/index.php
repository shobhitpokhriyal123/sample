<?php
$this->breadcrumbs=array(
	'Contract Files',
);

$this->menu=array(
	array('label'=>'Create ContractFiles','url'=>array('create')),
	array('label'=>'Manage ContractFiles','url'=>array('admin')),
);
?>

<h1>Contract Files</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
