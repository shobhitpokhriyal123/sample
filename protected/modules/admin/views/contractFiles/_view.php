<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contract_id')); ?>:</b>
	<?php echo CHtml::encode($data->contract_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('is_paper')); ?>:</b>
	<?php echo CHtml::encode($data->is_paper); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('is_copy_of_original')); ?>:</b>
	<?php echo CHtml::encode($data->is_copy_of_original); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('files')); ?>:</b>
	<?php echo CHtml::encode($data->files); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contract_signed_date')); ?>:</b>
	<?php echo CHtml::encode($data->contract_signed_date); ?>
	<br />


</div>