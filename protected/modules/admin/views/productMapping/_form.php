<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'product-mapping-form',
    'enableAjaxValidation' => false,
        ));
?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

<?php //echo $form->textFieldRow($model,'product_id',array('class'=>'span5','maxlength'=>255)); ?>

<?php echo $form->textFieldRow($model, 'attribute_name', array('class' => 'span5', 'maxlength' => 255)); ?>

<?php echo $form->textFieldRow($model, 'attribute_slug', array('class' => 'span5', 'maxlength' => 255,"readOnly"=>true)); ?>

<?php echo $form->textFieldRow($model, 'attribute_value', array('class' => 'span5', 'maxlength' => 255)); ?>

<?php //echo $form->textFieldRow($model, 'attribute_type', array('class' => 'span5')); ?>

    <?php //echo $form->textFieldRow($model,'status',array('class'=>'span5'));  ?>

<div class="form-actions">
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type' => 'primary',
        'label' => $model->isNewRecord ? 'Create' : 'Save',
    ));
    ?>
</div>

<?php $this->endWidget(); ?>


<script>
    $("#ProductMapping_attribute_name").keyup(function () {
        var Text = $(this).val();
        Text = Text.toLowerCase();
        var regExp = /\s+/g;
        Text = Text.replace(regExp, '_');
        $("#ProductMapping_attribute_slug").val(Text);
    });
</script>
