<div class="row-fluid">
    <div class="span12 well">


        <?php
//$this->breadcrumbs=array(
//	'Product Mappings'=>array('index'),
//	$model->id,
//);
//
//$this->menu=array(
//	array('label'=>'List ProductMapping','url'=>array('index')),
//	array('label'=>'Create ProductMapping','url'=>array('create')),
//	array('label'=>'Update ProductMapping','url'=>array('update','id'=>$model->id)),
//	array('label'=>'Delete ProductMapping','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
//	array('label'=>'Manage ProductMapping','url'=>array('admin')),
//);
        ?>

        <h1>View Product Mapping #<?php echo $model->id; ?></h1>
        <div class="right">
            <?php
            if (Yii::app()->user->isAdmin) {
                echo CHtml::link('manage Product Fields', array('/admin/productMapping/admin'), array("class" => 'btn btn-success'));
            }
            ?>
        </div>

        <?php
        $this->widget('bootstrap.widgets.TbDetailView', array(
            'data' => $model,
            'attributes' => array(
                'id',
                'product_id',
                'attribute_name',
                'attribute_slug',
                'attribute_value',
                'attribute_type',
                'status',
            ),
        ));
        ?>

    </div>
</div>
