<?php
$this->breadcrumbs=array(
	'Product Mappings',
);

$this->menu=array(
	array('label'=>'Create ProductMapping','url'=>array('create')),
	array('label'=>'Manage ProductMapping','url'=>array('admin')),
);
?>

<h1>Product Mappings</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
