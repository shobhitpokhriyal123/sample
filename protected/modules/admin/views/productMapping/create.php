<div class="row-fluid">
    <div class="span12 well">
        <?php
//        $this->breadcrumbs = array(
//            'Product Mappings' => array('index'),
//            'Create',
//        );
//
//        $this->menu = array(
//            array('label' => 'List ProductMapping', 'url' => array('index')),
//            array('label' => 'Manage ProductMapping', 'url' => array('admin')),
//        );
        ?>

        <h1>Create ProductMapping</h1>
        <div class="right">
            <?php
            if (Yii::app()->user->isAdmin) {
                echo CHtml::link('manage Product Fields', array('/admin/productMapping/admin'), array("class" => 'btn btn-success'));
            }
            ?>
        </div>

        <?php echo $this->renderPartial('_form', array('model' => $model)); ?>

    </div>
</div>