<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('product_id')); ?>:</b>
	<?php echo CHtml::encode($data->product_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('attribute_name')); ?>:</b>
	<?php echo CHtml::encode($data->attribute_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('attribute_slug')); ?>:</b>
	<?php echo CHtml::encode($data->attribute_slug); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('attribute_value')); ?>:</b>
	<?php echo CHtml::encode($data->attribute_value); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('attribute_type')); ?>:</b>
	<?php echo CHtml::encode($data->attribute_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />


</div>