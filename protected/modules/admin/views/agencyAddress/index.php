<?php
$this->breadcrumbs=array(
	'Agency Addresses',
);

$this->menu=array(
	array('label'=>'Create AgencyAddress','url'=>array('create')),
	array('label'=>'Manage AgencyAddress','url'=>array('admin')),
);
?>

<h1>Agency Addresses</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
