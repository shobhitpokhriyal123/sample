<?php
$this->breadcrumbs=array(
	'Agency Addresses'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AgencyAddress','url'=>array('index')),
	array('label'=>'Create AgencyAddress','url'=>array('create')),
	array('label'=>'View AgencyAddress','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage AgencyAddress','url'=>array('admin')),
);
?>

<h1>Update AgencyAddress <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>