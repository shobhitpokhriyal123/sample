<?php
$this->breadcrumbs=array(
	'Agency Addresses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AgencyAddress','url'=>array('index')),
	array('label'=>'Manage AgencyAddress','url'=>array('admin')),
);
?>

<h1>Create AgencyAddress</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>