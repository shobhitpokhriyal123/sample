<?php
$this->breadcrumbs=array(
	'Agency Addresses'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List AgencyAddress','url'=>array('index')),
	array('label'=>'Create AgencyAddress','url'=>array('create')),
	array('label'=>'Update AgencyAddress','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete AgencyAddress','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AgencyAddress','url'=>array('admin')),
);
?>

<h1>View AgencyAddress #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'agency_id',
		'address_number',
		'town',
		'address1',
		'country',
		'address2',
		'telephone',
		'website',
		'email',
		'fax',
	),
)); ?>
