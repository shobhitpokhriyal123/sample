<section id="role">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <strong>Manage Roles</strong>
                    </div>
                    <?php echo CHtml::link('Create Role', array('/admin/role/create'), array("class" => 'btn btn-success')); ?>
                </div>
                <div class="card-body">
                    <?php
                    $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => 'role-grid',
                        'dataProvider' => $model->search(),
                        'filter' => $model,
                        'afterAjaxUpdate' => "function() {
                                jQuery('#created').datepicker();
                             }",
                        'columns' => array(
                            'id',
                            'title',
                            'description:html',
                            //'role',
                            'active' => array(
                                'filter' => $model->getStatus(),
                                'name' => 'active',
                                'id' => 'active',
                                'value' => '$data->getStatus($data->active)'
                            ),
                            'created' => array(
                                'id' => 'created',
                                'name' => 'created',
                                'value' => '$data->created'
                            ),
                            array(
                                'class' => 'bootstrap.widgets.TbButtonColumn',
                                'template' => '{view},{update}'
                            ),
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    jQuery('#created').datepicker();
</script>
