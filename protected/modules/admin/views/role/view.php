<section id="view_role">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <strong>View Role Detail<?php //echo $model->rolename; ?></strong>
                    </div>
                    <?php
                        echo CHtml::link('Manage', array('/admin/role/admin'), array("class" => 'btn btn-success'));
                        echo CHtml::link('Update', array('/admin/role/update/id/' . $model->id), array("class" => 'btn btn-success'));
                    ?>
                </div>
                <div class="card-body">
                    <?php
                        $this->widget('bootstrap.widgets.TbDetailView', array(
                            'data' => $model,
                            'attributes' => array(
                                'id',
                                'title',
                                // 'role',
                                'active' => array(
                                    'name' => 'active',
                                    'id' => 'active',
                                    'value' => $model->getStatus($model->active)
                                ),
                                'created',
                            ),
                        ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
