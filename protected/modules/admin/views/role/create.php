<section id="create_role">
    <div class="container">
        <div class="row">
            <?php
                $this->breadcrumbs = array(
                    'Manage Roles' => array('admin'),
                    'Create',
                );

                $this->menu=array(
                //	array('label'=>'List Role','url'=>array('index')),
                    array('label'=>'Manage Role','url'=>array('admin')),
                );
            ?>
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <strong>Create Role</strong>
                    </div>
                    <?php echo CHtml::link('Manage', array('/admin/role/admin'), array("class" => 'btn btn-success')); ?>
                </div>
                <div class="card-body">
                    <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
                </div>
            </div>
        </div>
    </div>
</section>
