
            <div class="card">
                <div class="card-header">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="card-title">
                        <strong id="contract">Manage Cotracts  <?php //echo  CustomerSite::model()->findByPk($sid)->site_name     ?></strong>
                    </div>

                </div>
                <div class="card-body">
                    <?php
                    $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => 'customer-contract-grid',
                        'dataProvider' => $model->searchContracts($cid),
                        'filter' => $model,
                        'columns' => array(
                            // 'id',
                            'customer_id' => array(
                                'name' => 'customer_id',
                                'filter' => false,
                                'value' => '$data->customer->customer_name'
                            ),
                            'site_id' => array(
                                'name' => 'site_id',
                                'filter' => false,
                                'value' => '$data->site->site_name'
                            ),
                            //  'create_user_id',
                            'corporate_sme' => array(
                                'name' => 'corporate_sme',
                                'filter' => false, // Product::getCorporateSME(),
                                'value' => 'Product::getCorporateSME($data->corporate_sme)',
                            ),
                            'utility' => array(
                                'name' => 'utility',
                                'filter' => false, // Product::getUtilites(),
                                'value' => 'Product::getUtilites($data->utility)',
                            ),
                            'start_date' => array(
                                'filter' => false,
                                'name' => 'start_date'
                            ),
                            /*
                              'created',
                             */
                            array(
                                'class' => 'bootstrap.widgets.TbButtonColumn',
                                'template' => '{create}',
                                'header' => 'Tender',
                                'buttons' => array(
                                    'create' => array(
                                        'label' => '<button class="btn btn-primary">Create Tender</button>',
                                        'url' => 'Yii::app()->createAbsoluteUrl("admin/customerTender/create", array("contract_id"=>$data->id))',
                                        //'visible' => '$data->status > 0', // <-- SHOW IF ROW INACTIVE
                                        // 'url' => 'Yii::app()->createAbsoluteUrl("admin/product/changeStatus", array("id"=>$data->id))',
                                        'options' => array('title' => 'Create Contract'),
                                        'imageUrl' => false,
                                    ),
                                ),
                            ),
                        ),
                    ));
                    ?>
                </div>
            </div>
     