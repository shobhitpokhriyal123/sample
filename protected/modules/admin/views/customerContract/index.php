<?php
$this->breadcrumbs=array(
	'Customer Contracts',
);

$this->menu=array(
	array('label'=>'Create CustomerContract','url'=>array('create')),
	array('label'=>'Manage CustomerContract','url'=>array('admin')),
);
?>

<h1>Customer Contracts</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
