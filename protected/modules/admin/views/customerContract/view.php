<section id="manage_productss">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="card-title">
                        <strong>View Cotract <?php //echo CustomerSite::model()->findByPk($sid)->site_name   ?></strong>
                    </div>

                    <?php
                   if (Yii::app()->user->isAdmin  || Yii::app()->user->isAgency ) {
                        echo CHtml::link('Manage Contracts', array('/admin/customerContract/admin', 'sid' => $model->site_id), array("class" => 'btn btn-success'));
                        echo CHtml::link('Manage Customers', array('/admin/agencyCustomer/admin'), array("class" => 'btn btn-success'));
                        echo CHtml::link('New Tender for '.$model->site->site_name, array('/admin/customerTender/create', 'contract_id' => $model->id), array("class" => 'btn btn-success'));
                        // echo CHtml::link('Add users', array('/admin/agencyCustomer/admin'), array("class" => 'btn btn-success'));
                    }
                    ?>

                </div>
                <div class="card-body">

                    <?php
                    $this->widget('bootstrap.widgets.TbDetailView', array(
                        'data' => $model,
                        'attributes' => array(
                            // 'id',
                            'customer_id' => array(
                                'name' => 'customer_id',
                                'filter' => false,
                                'value' => $model->customer->customer_name
                            ),
                            'site_id' => array(
                                'name' => 'customer_id',
                                'filter' => false,
                                'value' => $model->site->site_name
                            ),
                            //  'create_user_id',
                            'corporate_sme' => array(
                                'name' => 'corporate_sme',
                                'filter' => false, // Product::getCorporateSME(),
                                'value' => Product::getCorporateSME($model->corporate_sme),
                            ),
                            'utility' => array(
                                'name' => 'utility',
                                'filter' => false, // Product::getUtilites(),
                                'value' => Product::getUtilites($model->utility),
                            ),
                            'start_date' => array(
                                'filter' => false,
                                'name' => 'start_date'
                            ),
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div></div>
</section>

