<section id="manage_productss">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="card-title">
                        <strong>Manage Contracts </strong>
                    </div>

                    <?php
                   if (Yii::app()->user->isAdmin  || Yii::app()->user->isAgency ) {
                        echo CHtml::link('Manage Customers', array('/admin/agencyCustomer/admin'), array("class" => 'btn btn-success'));
                        echo CHtml::link('Manage Sites', array('/admin/customerSite/admin'), array("class" => 'btn btn-success'));
                        echo CHtml::link('Manage Meters', array('/admin/customerSiteMeter/admin'), array("class" => 'btn btn-success'));
                        echo CHtml::link('Manage Contracts', array('/admin/customerContract/admin'), array("class" => 'btn btn-success'));
                        echo CHtml::link('Manage Tenders', array('/admin/customerTender/admin'), array("class" => 'btn btn-success'));
                        echo CHtml::link('Manage Quotes', array('/admin/customerQuote/admin'), array("class" => 'btn btn-success'));
                    }
                    ?>

                </div>
                <div class="card-body">
<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'customer-contract-grid',
    'dataProvider' => $model->search($sid),
    'filter' => $model,
    'columns' => array(
        // 'id',
        'customer_id' => array(
            'name' => 'customer_id',
            'filter' => false,
            'value' => '$data->customer->customer_name'
        ),
        'site_id' => array(
            'name' => 'site_id',
            'filter' => false,
            'value' => '$data->site->site_name'
        ),
        //  'create_user_id',
        'corporate_sme' => array(
            'name' => 'corporate_sme',
            'filter' => false, // Product::getCorporateSME(),
            'value' => 'Product::getCorporateSME($data->corporate_sme)',
        ),
        'utility' => array(
            'name' => 'utility',
            'filter' => false, // Product::getUtilites(),
            'value' => 'Product::getUtilites($data->utility)',
        ),
        'start_date' => array(
            'filter' => false,
            'name' => 'start_date'
        ),
        /*
          'created',
         */
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{create}{delete}',
            'header' => 'Tender',
            'buttons' => array(
                'create' => array(
                    'label' => '<button class="btn btn-primary">Create Tender</button>',
                    'url' => 'Yii::app()->createAbsoluteUrl("admin/customerTender/create", array("contract_id"=>$data->id))',
                    //'visible' => '$data->status > 0', // <-- SHOW IF ROW INACTIVE
                    // 'url' => 'Yii::app()->createAbsoluteUrl("admin/product/changeStatus", array("id"=>$data->id))',
                    'options' => array('title' => 'Create Contract'),
                    'imageUrl' => false,
                ),
            ),
        ),
    ),
));
?>
                </div>
            </div>
        </div>
    </div>
</section>