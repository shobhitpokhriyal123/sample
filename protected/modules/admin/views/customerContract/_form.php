<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'customer-contract-form',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php //echo $form->errorSummary($model); ?>

<?php //echo $form->hiddenField($model,'customer_id', );  ?>
<?php //echo $form->dropdownListRow($model, 'assigned_to', CHtml::listData(Users::model()->findAllByAttributes(array('role_id' => 2)), 'id', 'fullname'), array( 'empty' => '--- Select Supplier ---')); ?>

<?php echo $form->hiddenField($model, 'create_user_id', array('value' => Yii::app()->user->id)); ?>

<?php echo $form->dropdownListRow($model, 'corporate_sme', Product::getCorporateSME(), array('onclick' => 'getUtilities($(this).val())')); ?>

<?php echo $form->dropdownListRow($model, 'utility', CHtml::listData(Product::getUtilites(), 'id', 'title')); ?>

<?php //echo $form->textFieldRow($model,'start_date',array('class'=>'span5')); ?>
<?php echo $form->label($model, 'start_date'); ?>
<?php
$this->widget('zii.widgets.jui.CJuiDatePicker', array(
    'model' => $model,
    'attribute' => 'start_date',
    'options' => array(
        'dateFormat' => 'yy-mm-dd',
        'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
        'changeMonth' => true,
        'changeYear' => true,
    ),
));
?>
<?php echo $form->error($model, 'start_date'); ?>

<?php //echo $form->hiddenField($model, 'created', array('value' => new CDbExpression('NOW()'))); ?>

<div class="form-actions">
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type' => 'primary',
        'label' => $model->isNewRecord ? 'Create' : 'Save',
    ));
    ?>
</div>

<?php $this->endWidget(); ?>

<script>
    function getUtilities(id) {
        var url = '<?php echo Yii::app()->createAbsoluteUrl("admin/product/getUtilities/id") ?>/' + id;
        $("#CustomerContract_utility").load(url);
        //gasParms();
    }
</script>
