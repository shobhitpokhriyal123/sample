<section id="create_product">
    <div class="container">
        <div class="row">
            <?php
            $this->breadcrumbs = array(
                'Products' => array('index'),
                'Create',
            );

            $this->menu = array(
                array('label' => 'List Product', 'url' => array('index')),
                array('label' => 'Manage Product', 'url' => array('admin')),
            );
            ?>
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <strong>Invoices</strong>
                    </div>
                    <?php
                    // if (Yii::app()->user->isAdmin) {
                    echo CHtml::link('Print Invoices', array('/admin/quote/pdf'), array("class" => 'btn btn-success', 'id' => 'invoices'));
                    //  }
                    ?>
                </div>
                <div class="card-body">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <?php if (Yii::app()->user->hasFlash('error')): ?>
                        <div class="error">
                            <?php echo Yii::app()->user->getFlash('error'); ?>
                        </div>
                    <?php endif; ?>
                    <?php
                    $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => 'quote-grid-invoices',
                        'dataProvider' => $model->search(1),
                        'afterAjaxUpdate' => 'function(id,options){ paginationControls(); }',
                        'filter' => $model,
                        'columns' => array(
                            'agency_id' => array(
                                'name' => 'agency_id',
                                'filter' => false,
                                'value' => '$data->getAgencyId()'
                            ),
                            'agency_name' => array(
                                'name' => 'agency_name',
                                'filter' => false,
                                'value' => '$data->getAgencyName()',
                            ),
                            'agent' => array(
                                'name' => 'agent',
                                'header' => 'Agent',
                                'filter' => false,
                                'value' => '$data->getAgent()',
                            ),
                            'business_name' => array(
                                'name' => 'business_name',
                                'header' => 'Business Name',
                                'filter' => false,
                                'value' => '$data->getBusinessName()',
                            ),
                            'live_date' => array(
                                'name' => 'live_date',
                                'header' => 'Live Date',
                                'filter' => false,
                                'type'=>'raw',
                                'value' => '(Yii::app()->user->isAdmin)?CHtml::link($data->getLiveDate(), array("/admin/quote/updateDate&id=$data->id"), array("class"=>"sdfsdfsasdf")) : $data->getLiveDate()'//'$data->getLiveDate()',
                            ),
                            'date' => array(
                                'name' => 'date',
                                'filter' => false,
                                'header' => 'Date',
                                'value' => '$data->getEstimateDate()',
                            ),
                            'invoice_no' => array(
                                'name' => 'invoice_no',
                                'filter' => false,
                            ),
                            'net_payment' => array(
                                'name' => 'net_payment',
                                'filter' => false,
                                'value' => 'number_format((float)$data->commission, 2, ".", "")', //'($data->annual_cost) ? $data->annual_cost : "N/A"',
                            ),
//                            array(
//                                'class' => 'ext.editable.EditableColumn',
//                                'name' => 'commission',
//                               // 'headerHtmlOptions' => array('style' => 'width: 30px'),
//                                'editable' => array(
//                                    'url' => $this->createUrl('readingbooks/update'),
//                                    'placement' => 'right',
//                                    //'inputclass' => 'span3',
//                                    'params' => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken),
//                                )
//                            ),
                            'vat' => array(
                                'name' => 'vat',
                                'filter' => false,
                                'value' => '$data->vat',
                            ),
                            'total' => array(
                                'name' => 'total',
                                'filter' => false,
                                'value' => '(number_format((float)$data->commission, 2, ".", "") + $data->vat)', // '($data->annual_cost) ? $data->annual_cost : "N/A"',
                            ),
                            'status' => array(
                                'header' => 'Payment Status',
                                'filter' => false,
                                'type' => 'raw',
                                'value' => '(Yii::app()->user->isAdmin)?CHtml::link(QuoteStatus::getQuoteStatus($data->status), array("/admin/quote/updateStatus&id=$data->id"), array("class"=>"sdfsdfsasdf")) : QuoteStatus::getQuoteStatus($data->status)',
                            ),
                            array(
                                'class' => 'bootstrap.widgets.TbButtonColumn',
                                'template' => '{update}{delete}',
                                'buttons' => array(
                                    'update' => array(
                                        // 'label' => '<button class="btn btn-primary">Update</button>',
                                        'url' => 'Yii::app()->createAbsoluteUrl("admin/quote/update", array("id"=>$data->id))',
                                        'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'update'),
                                        'imageUrl' => false,
                                        'click' => "function(e){
                                                e.preventDefault();
                                                var url = $(this).attr('href');
                                                $('#myModal').modal('show');
                                                $('#content_page').load(url);
                                
                           
                            }",
                                        'visible' => 'Yii::app()->user->isAdmin',
                                    ),
                                    'delete' => array('visible' => 'Yii::app()->user->isAdmin'),
                                ),
                            ),
                        ),
                    ));
                    ?>

                </div>
            </div>
        </div>
    </div>
</section>

<form id="invoice_form" name="invoice_form" method="post" action="<?php echo Yii::app()->createAbsoluteUrl('/admin/quote/pdf') ?>">
    <input type="hidden" name="pdf" value="" id="input_invoice" >
</form>


<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="card">

            <!-- Modal content-->
            <div class="modal-content" id="content_page">
                

            </div>

        </div>


    </div>
</div>

<script>
    $(document).ready(function () {
        $("#invoices").click(function (e) {
            e.preventDefault();
            $("#input_invoice").val($("#quote-grid-invoices").html());
            $("#invoice_form").submit();
        });
    });
</script>



<script>
    $(document).ready(function () {
        $(".sdfsdfsasdf").click(function (e) {
            e.preventDefault();
            var url = $(this).attr("href");
            $("#myModal1").modal();
            $("#modal-content-status").load(url);
        });
        
        
    });
</script>


<!-- Modal -->
<div id="myModal1" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" id="modal-content-status">

        </div>

    </div>
</div>
