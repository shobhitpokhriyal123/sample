<div class="container-fluid">

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12"><strong><?php echo $model->productName->user->fullname; ?></strong></div>
                <div class="clearfix"></div>
                <div class="col-md-2">
                    <b>Supplier:</b> 
                </div>                    
                <div class="col-md-2">
                    <?php echo $model->productName->user->fullname; ?>
                </div>      
                <div class="col-md-2">
                    <b>Product Name:</b> 
                </div>   
                <div class="col-md-2">
                    <?php echo Product::getPName($model->productName->product_name, $model->product); ?>
                </div>   
                <div class="col-md-2">
                    <b>Min AQ:</b>
                </div>   
                <div class="col-md-2">
                    <?php echo $model->min_aq; ?>
                </div> 
                <div class="clearfix"></div>
                <div class="col-md-2">
                    <b>Max AQ:</b>
                </div>   
                <div class="col-md-2">
                    <?php echo $model->max_aq; ?>
                </div> 
                <div class="col-md-2">
                    <b>Payment Type</b>
                </div>   
                <div class="col-md-2">
                    <?php echo ($model->productName->direct_dabit == 0) ? "Cash" : "Direct Debit"; ?>
                </div> 
                <div class="col-md-2">
                    <b>Contract Duration</b>
                </div>   
                <div class="col-md-2">
                    <?php echo ($model->term) ? $model->term : $model->contract_length ?>
                </div> 
                <div class="clearfix"></div>
                <div class="card-header">
                    <div class="card-title">
                        <strong>Tariff Information</strong>
                    </div>
                </div>
                <div class="col-md-3">
                    <b>Distribution Charge:</b>
                </div>   
                <div class="col-md-3">
                    <?php echo Product::getOtherCharges($model->productName->distribution_charge); ?>
                </div> 
                <div class="col-md-3">
                    <b>Distribution Charge:</b>
                </div>   
                <div class="col-md-3">
                    <?php echo Product::getOtherCharges($model->productName->distribution_charge) ?>
                </div> 
                <div class="col-md-3">
                    <b>Transmission Charge:</b>
                </div>   
                <div class="col-md-3">
                    <?php echo Product::getOtherCharges($model->productName->transmission_charge); ?>
                </div> 

                <div class="col-md-3">
                    <b>Renewal Obligation:</b>
                </div>   
                <div class="col-md-3">
                    <?php echo Product::getOtherCharges($model->productName->renewal_obligation); ?>
                </div> 
                <div class="col-md-3">
                    <b>Feed in Tariff:</b>
                </div>   
                <div class="col-md-3">
                    <?php echo Product::getOtherCharges($model->productName->feed_in_tarriff) ?>
                </div> 
                <div class="col-md-3">
                    <b>Utility:</b>
                </div>   
                <div class="col-md-3">
                    <?php echo Product::getUtilites($model->productName->utility); ?>
                </div> 

                <div class="clearfix"></div>
                <div class="card-header">
                    <div class="card-title">
                        <strong>About Supplier</strong>
                    </div>
                </div>
                <div class="col-md-3">
                    <p> <?php echo $model->productName->user->description; ?> </p>
                </div> 
            </div>
        </div>
    </div>
</div>
