<section id="manage_products">
    <div class="container-fluid">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="card-title">
                        <strong>Quote Detail</strong>
                    </div>

                  

                </div>
                <div class="card-body">

                    <?php
                    $this->widget('bootstrap.widgets.TbDetailView', array(
                        'data' => $model,
                        'attributes' => array(
                            'id',
                            'price_id',
                         //   'is_gas',
                            'commission_type',
                            'standing_charge',
                            'price',
                            //'commission',
                            'annual_cost',
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
