<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">Change Live Date</h4>
</div>
<div class="modal-body">

    <?php
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'ticket-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <p class="help-block">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>


    <?php echo $form->dropDownListRow($model, 'status', CHtml::listData(QuoteStatus::model()->findAll(), 'id', 'title'), array('class' => 'form-control')); ?>

    <?php //echo $form->textFieldRow($model, 'created', array('class' => 'span5'));   ?>
    <hr>
    <div class="form-actions">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type' => 'primary',
            'label' => $model->isNewRecord ? 'Create' : 'Save',
        ));
        ?>
    </div>

    <?php $this->endWidget(); ?>





    <style>
        .descc {
            /* float: left; */
            width: 100%;
        }

    </style>    
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>