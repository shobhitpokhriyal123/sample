<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('price_id')); ?>:</b>
	<?php echo CHtml::encode($data->price_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('is_gas')); ?>:</b>
	<?php echo CHtml::encode($data->is_gas); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('commission_type')); ?>:</b>
	<?php echo CHtml::encode($data->commission_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('standing_charge')); ?>:</b>
	<?php echo CHtml::encode($data->standing_charge); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('price')); ?>:</b>
	<?php echo CHtml::encode($data->price); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('commission')); ?>:</b>
	<?php echo CHtml::encode($data->commission); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('annual_cost')); ?>:</b>
	<?php echo CHtml::encode($data->annual_cost); ?>
	<br />

	*/ ?>

</div>