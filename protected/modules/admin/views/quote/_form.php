<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'quote-form',
    'enableAjaxValidation' => false,
        ));
?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php //echo $form->errorSummary($model); ?>

<?php //echo $form->textFieldRow($model, 'price_id', array('class' => 'span5')); ?>

<?php //echo $form->textFieldRow($model, 'is_gas', array('class' => 'span5')); ?>

<?php //echo $form->textFieldRow($model, 'commission_type', array('class' => 'span5', 'maxlength' => 255)); ?>

<?php echo $form->textFieldRow($model, 'invoice_no', array('class' => 'form-control', 'maxlength' => 255)); ?>

<?php echo $form->textFieldRow($model, 'vat', array('class' => 'form-control', 'maxlength' => 255)); ?>

<?php echo $form->textFieldRow($model, 'commission', array('class' => 'form-control', 'maxlength' => 255)); ?>

    <?php echo $form->textFieldRow($model, 'annual_cost', array('class' => 'form-control', 'maxlength' => 255, 'value'=>($model->commission+$model->vat))); ?>

<div class="form-actions">
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type' => 'primary',
        'label' => $model->isNewRecord ? 'Create' : 'Save',
    ));
    ?>
</div>

<?php $this->endWidget(); ?>
