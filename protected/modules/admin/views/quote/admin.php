<?php $quotes = $model->search(NULL, 1) ?>
<div class="row">
    <div class="col-md-12">
        <div class="col-md-6">
            <form id="send_mail" action='<?php echo Yii::app()->createAbsoluteUrl("admin/quote/send") ?>' name="send_mail" method="post">
                <input  id="email_id" name="email" required="required" >
                <?php
                if (!empty($quotes)) {
                    $arr = "";
                    foreach ($quotes as $quote) {
                        $arr .= $quote->id .", " ;
                        ?>
                        <input id="quote_id" type="hidden" name="quote_id" value="<?php echo $arr ?>" >
                        <?php
                    }
                } else {
                    ?>
                    <input  id="quote_id" type="hidden" name="quote_id" >
                  
                <?php } ?>
                     
            </form>
        </div>
        <div class="col-md-2">   
            <button id="send_mail_btn" type="button" class="btn btn-primary">Send </button>
        </div>
    </div>
</div>
<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'quote-grid-',
    'dataProvider' => $model->search(NULL, null),
    'afterAjaxUpdate' => 'function(id,options){ paginationControls(); }',
    'filter' => $model,
    'columns' => array(
        //'id',
        'supplier' => array(
            'name' => 'supplier',
            'type' => 'html',
            'value' => '($data->priceModel) ? CHtml::image(Yii::app()->createAbsoluteUrl("admin/users/thumbnail/file/".$data->priceModel->productName->user->image)) : "N/A"',
            'filter' => false,
        ),
        'product' => array(
            'header' => 'Set Name',
            'filter' => false,
            'type' => 'raw',
            'name' => 'product',
            'value' => 'Product::getPName($data->priceModel->productName->product_name,$data->priceModel->product)'
        ),
        'region' => array(
            'name' => 'region',
            'filter' => false,
            'value' => '($data->priceModel) ?  $data->priceModel->region : "N/A"',
        ),
        'contract_length' => array(
            'name' => 'contract_length',
            'header' => 'Terms',
            'filter' => false,
            'value' => '($data->priceModel && $data->priceModel->contract_length)? $data->priceModel->contract_length :$data->priceModel->term',
        ),
//        'min_aq' => array(
//            'name' => 'min_aq',
//            'value' => '($data->priceModel) ? $data->priceModel->min_aq : "N/A"',
//            'filter' => false,
//        ),
//        'max_aq' => array(
//            'name' => 'max_aq',
//            'value' => '($data->priceModel)?$data->priceModel->max_aq : "N/A"',
//            'filter' => false,
//        ),
        'start_date' => array(
            'name' => 'start_date',
            'value' => '$data->priceModel->start_date',
            'filter' => false
        ),
        'payment_type' => array(
            'filter' => false,
            'name' => 'payment_type',
            'value' => '($data->priceModel->productName->direct_dabit == 0) ? "Direct Debit" : "Cash" '
        ),
        'commission_type' => array(
            'name' => 'commission_type',
            'filter' => false,
            'value' => '$data->commission_type',
        ),
        //'price_id'=,
        //'is_gas',
        'standing_charge' => array(
            'name' => 'standing_charge',
            'filter' => false,
            'value' => '$data->standing_charge',
        ),
        'price' => array(
            'name' => 'price',
            'filter' => false,
            'value' => '$data->price',
        ),
        'commission' => array(
            'name' => 'commission',
            'filter' => false,
            'value' => 'number_format((float)$data->commission, 2, ".", "")',
        ),
        'annual_cost' => array(
            'name' => 'annual_cost',
            'filter' => false,
            'value' => '$data->annual_cost',
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{delete}'
        ),
    ),
));
?>


<script>
    $(document).ready(function (e) {
        $("#send_mail_btn").on("click", function (e) {
            e.preventDefault();
            if ($("#email_id").val() == "") {
                alert("Please enter email id");
                return false;
            }
            if ($("#quote_id").val() == "") {
                alert("No quote(s) found...!");
                return false;
            }
           
            //$("#html1").val($("#quote-grid-").html());
        //     alert($("#html1").val()); return;
            var url = $("#send_mail").attr("action");
            var data = $("#send_mail").serialize();
         
            $.ajax({
                url: url,
                type: "POST",
                data: data,
                success: function (res) {
                   alert(res);
                   if(res == '1'){
                       alert("Qutation sent...!");
                   }else{
                       alert("Error while quatation sending...!");
                   }
                   return false;
                }
            });
        });
    });

    paginationControls();


</script>
