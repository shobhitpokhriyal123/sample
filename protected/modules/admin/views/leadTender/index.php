<?php
$this->breadcrumbs=array(
	'Lead Tenders',
);

$this->menu=array(
	array('label'=>'Create LeadTender','url'=>array('create')),
	array('label'=>'Manage LeadTender','url'=>array('admin')),
);
?>

<h1>Lead Tenders</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
