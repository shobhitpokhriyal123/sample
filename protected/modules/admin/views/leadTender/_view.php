<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contract_duration')); ?>:</b>
	<?php echo CHtml::encode($data->contract_duration); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('supplier_ids')); ?>:</b>
	<?php echo CHtml::encode($data->supplier_ids); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('business_name')); ?>:</b>
	<?php echo CHtml::encode($data->business_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meter_id')); ?>:</b>
	<?php echo CHtml::encode($data->meter_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meter_type')); ?>:</b>
	<?php echo CHtml::encode($data->meter_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contract_type')); ?>:</b>
	<?php echo CHtml::encode($data->contract_type); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('bespoke_duration')); ?>:</b>
	<?php echo CHtml::encode($data->bespoke_duration); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('commission_discount')); ?>:</b>
	<?php echo CHtml::encode($data->commission_discount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tender_return_date')); ?>:</b>
	<?php echo CHtml::encode($data->tender_return_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('com_reg')); ?>:</b>
	<?php echo CHtml::encode($data->com_reg); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('commodity_type')); ?>:</b>
	<?php echo CHtml::encode($data->commodity_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('uplift')); ?>:</b>
	<?php echo CHtml::encode($data->uplift); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('term_length')); ?>:</b>
	<?php echo CHtml::encode($data->term_length); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fixed_meter')); ?>:</b>
	<?php echo CHtml::encode($data->fixed_meter); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('payment')); ?>:</b>
	<?php echo CHtml::encode($data->payment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('energy')); ?>:</b>
	<?php echo CHtml::encode($data->energy); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('supplier_pref')); ?>:</b>
	<?php echo CHtml::encode($data->supplier_pref); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('comments')); ?>:</b>
	<?php echo CHtml::encode($data->comments); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />

	*/ ?>

</div>