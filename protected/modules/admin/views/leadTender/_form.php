<div class="row">

    <?php
    if ($l_tender) {
        $l_tender->supplier_ids = unserialize($l_tender->supplier_ids);
    }
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'customer-tender-form',
        //'action' => $this->createAbsoluteUrl('customerTender/update', array("id" => $l_tender->id)),
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
            )
    );
    ?>
    <div class="col-md-12" id="tender_part">

        <div class="col-md-6">
            <?php echo $form->dropdownListRow($l_tender, 'agency_id', Users::getAgencies(), array('onchange' => 'getAgencyId($(this).val())', 'class' => 'form-control')); ?>
        </div>


        <div class="col-md-6">
            <?php echo $form->dropdownListRow($l_tender, 'agent_id', CHtml::listdata(Users::model()->findAllByAttributes(array('role_id' => 4)), "id", "username"), array('class' => 'form-control')); ?>
        </div>
        <div class="col-md-6">
            <?php echo $form->textFieldRow($l_tender, 'contract_duration', array('class' => 'span5', 'maxlength' => 255)); ?>
        </div>

        <div class="col-md-6">
            <?php echo $form->label($l_tender, "supplier_ids"); ?>
            <?php
            $ab = CHtml::listData(Users::model()->findAllByAttributes(array('role_id' => 2)), 'id', 'fullname');
            ?>

            <?php echo CHtml::activeListBox($l_tender, 'supplier_ids', $ab, array('class' => 'chosen form-control', 'multiple' => true, 'data-placeholder' => 'Select')) ?>
            <?php
            $this->widget('ext.chosen.EChosenWidget', array(
                'selector' => '.chosen',
            ));
            ?>
            <?php echo $form->error($l_tender, "supplier_ids"); ?>
        </div>

        <div class="col-md-6">
            <?php echo $form->textFieldRow($l_tender, 'bespoke_duration', array('class' => 'span5', 'maxlength' => 255)); ?>
        </div>

        <div class="col-md-6">
            <?php echo $form->textFieldRow($l_tender, 'commission_discount', array('class' => 'span5', 'maxlength' => 255)); ?>
        </div>

        <div class="col-md-6">

            <?php echo $form->label($l_tender, 'tender_return_date'); ?>
            <?php
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $l_tender,
                'attribute' => 'tender_return_date',
                'options' => array(
                    'dateFormat' => 'yy-mm-dd',
                    'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                    'changeMonth' => true,
                    'changeYear' => true,
                ),
            ));
            ?>

            <?php echo $form->error($l_tender, 'tender_return_date'); ?>
            <?php if ($l_tender->id) { ?>
                <?php echo $form->hiddenField($l_tender, 'customer_id'); ?>
            <?php } ?>
        </div>

        <div class="col-md-6">
            <?php echo $form->textFieldRow($l_tender, 'business_name', array('class' => 'span5', 'maxlength' => 255)); ?>
        </div>

        <div class="col-md-6">
            <?php echo $form->textFieldRow($l_tender, 'com_reg', array('class' => 'span5', 'maxlength' => 255)); ?>
        </div>

        <div class="col-md-6">
            <?php
            echo $form->dropdownListRow($l_tender, 'commodity_type', LeadContacts::getCommodityType(), array('class' => 'span5', 'maxlength' => 255));
            ?>

        </div>

        <!--        <div class="col-md-6">
        <?php //echo $form->textFieldRow($l_tender, 'site_no', array('class' => 'span5', 'maxlength' => 255));     ?>
                </div>-->


        <div class="col-md-6">
            <?php echo $form->textFieldRow($l_tender, 'uplift', array('class' => 'span5', 'maxlength' => 255)); ?>
        </div>

        <div class="col-md-6">
            <?php
            echo $form->dropdownListRow($l_tender, 'term_length', LeadContacts::getTermLength(), array('class' => 'span5', 'maxlength' => 255));
            ?>

        </div>
        <div class="col-md-6">
            <?php echo $form->textFieldRow($l_tender, 'term_length', array('class' => 'span5', 'maxlength' => 255)); ?>

        </div>

        <div class="col-md-6">
            <?php echo $form->textFieldRow($l_tender, 'fixed_meter', array('class' => 'span5', 'maxlength' => 255)); ?>
        </div>

        <div class="col-md-6">

            <?php
            echo $form->dropdownListRow($l_tender, 'contract_type', CHtml::listData(ContractType::model()->findAll(), 'id', 'title'));
            ?>

        </div>

        <div class="col-md-6">
            <?php
            echo $form->dropdownListRow($l_tender, 'payment', PaymentType::getPayment());
            ?>

        </div>


        <div class="col-md-6">
            <?php
            echo $form->dropdownListRow($l_tender, 'energy', ContractType::getEnergy(), array());
            ?>

        </div>

        <div class="col-md-6">
            <?php echo $form->textFieldRow($l_tender, 'supplier_pref', array('size' => 20, 'maxlength' => 20)); ?>
        </div>

        <div class="col-md-6">
            <?php echo $form->textAreaRow($l_tender, 'comments', array('rows' => 6, 'cols' => 50, "class" => "form-control")); ?>
        </div>
        <hr>
        <div class="col-md-12">
            <div class="card-header">
                <div class="card-title">
                    <strong>Upload Tender Files </strong>
                </div>
            </div>
            <?php echo $form->fileFieldRow($l_tender, 'document', array("class" => "form-control")); ?>

            <?php
//            echo $form->label($tender_files, 'document');
//            $this->widget('CMultiFileUpload', array(
//                'model' => $tender_files,
//                'attribute' => 'document',
//                'accept' => 'xsl|csv|xsls|jpg|jpeg|png|pdf',
//                'options' => array(
//                // 'onFileSelect'=>'function(e, v, m){ alert("onFileSelect - "+v) }',
//                // 'afterFileSelect'=>'function(e, v, m){ alert("afterFileSelect - "+v) }',
//                // 'onFileAppend'=>'function(e, v, m){ alert("onFileAppend - "+v) }',
//                // 'afterFileAppend'=>'function(e, v, m){ alert("afterFileAppend - "+v) }',
//                // 'onFileRemove'=>'function(e, v, m){ alert("onFileRemove - "+v) }',
//                // 'afterFileRemove'=>'function(e, v, m){ alert("afterFileRemove - "+v) }',
//                ),
//                'denied' => 'File type is not allowed',
//                'max' => 10, // max 10 files
//            ));
//            echo $form->error($tender_files, 'document');
            ?>

        </div>
        <div class="col-md-12">
            <div class="form-actions">
                <?php
                $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType' => 'submit',
                    'type' => 'primary',
                    'label' => $l_tender->isNewRecord ? 'Create' : 'Save',
                ));
                ?>
            </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>
<style>
    .chosen-container{
        width: 100%;
        height: 37px;
    }
</style>
<script>
    getAgencyId($("#LeadTender_agency_id").val());
    function getAgencyId(id) {
        var siteagency = '<?php echo Yii::app()->createAbsoluteUrl("/admin/leadContacts/getAgencyId") ?>/id/' + id;
        $.ajax({
            url: siteagency,
            type: 'POST',
            success: function (res) {
                $("#LeadTender_agent_id").html(res);
            }
        });
    }
</script>
