<section id="manage_products">
    <div class="container-fluid">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <strong>View Tender</strong>
                    </div>
                    <?php
                    if (Yii::app()->user->isAdmin) {
                        echo CHtml::link('Tenders', array('/admin/leadTender/admin'), array("class" => 'btn btn-success'));
                        echo CHtml::link('Customers', array('/admin/leadContacts/customers'), array("class" => 'btn btn-success'));
                    }
                    ?>
                </div>
                <div class="card-body">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <?php
                    $this->widget('bootstrap.widgets.TbDetailView', array(
                        'data' => $model,
                        'attributes' => array(
                            //   'id',
                            'contract_duration',
                            'supplier_ids' => array(
                                'name' => 'supplier_ids',
                                'value' => $model->getSupplierName()
                            ),
                            'business_name',
                            'meter_id' => array(
                                'name' => 'meter_id',
                                'value' => $model->getMeterName()
                            ),
                            'document' => array(
                                'name' => 'document',
                                'type' => 'raw',
                                'value' => ($model->document) ? CHtml::link("Download", Yii::app()->createAbsoluteUrl("admin/users/thumbnail/file/" . $model->document)) : "N/A",
                            ),
                            'meter_type',
                            'contract_type',
                            'bespoke_duration',
                            'commission_discount',
                            'tender_return_date',
                            'com_reg',
                            'commodity_type',
                            'uplift',
                            'term_length',
                            'fixed_meter',
                            'payment',
                            'energy',
                            'supplier_pref',
                            'comments',
                            'created',
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
