<section id="manage_products">
    <div class="container-fluid">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <strong>Manage Tenders</strong>
                    </div>
                    <?php
                    if (Yii::app()->user->isAdmin) {
                        echo CHtml::link('Create Customer', array('/admin/leadContacts/addCustomer'), array("class" => 'btn btn-success'));
                        echo CHtml::link('Customers', array('/admin/leadContacts/customers'), array("class" => 'btn btn-success'));
                    }
                    ?>
                </div>
                <div class="card-body">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <?php 
                    $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => 'lead-tender-grid',
                        'dataProvider' => $model->search(),
                        'afterAjaxUpdate' => 'function(id,options){ paginationControls(); }',
                        //     'filter' => $model,
                        'columns' => array(
                            'id' => array(
                                'name' => 'id',
                                'header' => 'Tender No',
                            ),
                            'agency_id' => array(
                                'name' => 'agency_id',
                                'header'=>'Agency',
                                'value' => '($data->agency) ? $data->agency->username : "N/A"'//'LeadTender::getAgency($data->business_name)'
                            ),
                            'agent_id' => array(
                                'name' => 'agent_id',
                                    'header'=>'Agent',
                                'value' => '($data->agent) ? $data->agent->username: "N/A"', // 'LeadTender::getAgent($data->business_name)'
                            ),
                            'utility' => array(
                                'name' => 'utility',
                                'value' => 'LeadTender::getMeters($data->meter_id, $data->meter_type)'
                            ),
                            'customer_id' => array(
                                'name' => 'customer_id',
                                'value' => '($data->customer) ? $data->customer->user->fullname : "N/A"',
                            ),
                            'created' => array(
                                'name' => 'created'
                            ),
                            'contract_duration' => array(
                                'name' => 'contract_duration',
                                'filter' => false,
                            ),
//                            'agency' => array(
//                                'name' => 'agency',
//                                'filter' => false,
//                                'value' => '$data->'
//                            ),
                            'supplier_ids' => array(
                                'name' => 'supplier_ids',
                                'filter' => false,
                                'value' => '$data->getSupplierName()'
                            ),
                            'business_name' => array(
                                'name' => 'business_name',
                                'filter' => false,
                            ),
                            'meter_id' => array(
                                'name' => 'meter_id',
                                'filter' => false,
                                'value' => '$data->getMeterName()'
                            ),
                            'site' => array(
                                'name' => 'site',
                                'filter' => false,
                                'value' => '$data->getMeterName(1)'
                            ),
                            'meter_type' => array(
                                'name' => 'meter_type',
                                'filter' => false,
                            ),
                            'contract_type' => array(
                                'name' => 'contract_type',
                                'filter' => false,
                                'value' => 'ContractType::getContract($data->contract_type)'
                            ),
//                            'status'=>array(
//                                'name'=>'status'
//                            ),
//                            'bespoke_duration',
//                            'commission_discount',
//                            'tender_return_date',
//                            'com_reg',
//                            'commodity_type',
//                            'uplift',
//                            'term_length',
//                            'fixed_meter',
//                            'payment' => array(
//                                'name' => 'payment',
//                                'value' => 'PaymentType::getPayment($data->payment)'
//                            ),
                            /* 'energy',
                              'supplier_pref',
                              'comments',
                              //'created',
                             */
                            'document' => array(
                                'name' => 'document',
                                'type' => 'raw',
                                'value' => '($data->document) ? CHtml::link("Download",Yii::app()->createAbsoluteUrl("admin/users/thumbnail/file/".$data->document)) : "N/A"',
                            ),
                            'send_quote' => array(
                                'name' => 'send_quote',
                                'header' => "Input quote status",
                                'value' => 'LeadQuote::quoteStatus($data->id, $data->customer_id)',
                                'type' => 'raw'
                            ),
                            array(
                                'class' => 'bootstrap.widgets.TbButtonColumn',
                                'template' => '{view}{update}',
//                                'buttons' => array(
//                                    'send' => array(
//                                        'label' => '<button class="btn btn-primary">Send Input Quote</button>',
//                                        'url' => 'Yii::app()->createAbsoluteUrl("admin/leadContacts/sendQuote", array("tender_id"=>$data->id, "cid"=>1))',
//                                        'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Send Input Quote'),
//                                        'imageUrl' => false,
//                                        'click' => "function(e){
//                                            e.preventDefault();
//                                            var url = $(this).attr('href');
//                                            $('#quote_modal').modal();
//                                            $('#modal_con').load(url);
//                                         }",
//                                    ),
//                                ),
                            ),
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
<div id="myModalcd" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content" id="modal_bodys">

        </div>

    </div>
</div>

<style>
    .modal-dialog {
        margin: 30px auto;
        width: 90%;
    }
</style>

<script>
    $(".send").click(function (e) {
        e.preventDefault();
        var url = $(this).attr("href");
        $("#myModalcd").modal();
        $("#modal_bodys").load(url);

    });
</script>