<section id="manage_products">
    <div class="container-fluid">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <strong>Update Tender</strong>
                    </div>
                    <?php
                    if (Yii::app()->user->isAdmin) {
                        echo CHtml::link('Tenders', array('/admin/leadTender/admin'), array("class" => 'btn btn-success'));
                        echo CHtml::link('Customers', array('/admin/leadContacts/customers'), array("class" => 'btn btn-success'));
                    }
                    ?>
                </div>
                <div class="card-body">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <?php
                    echo $this->renderPartial('_form', array(
                        'l_tender' => $l_tender,
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
