<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'contract_duration',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'supplier_ids',array('class'=>'span5','maxlength'=>64)); ?>

	<?php echo $form->textFieldRow($model,'business_name',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'meter_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'meter_type',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'contract_type',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'bespoke_duration',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'commission_discount',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tender_return_date',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'com_reg',array('class'=>'span5','maxlength'=>50)); ?>

	<?php echo $form->textAreaRow($model,'commodity_type',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($model,'uplift',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'term_length',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'fixed_meter',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'payment',array('class'=>'span5','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'energy',array('class'=>'span5','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'supplier_pref',array('class'=>'span5','maxlength'=>20)); ?>

	<?php echo $form->textAreaRow($model,'comments',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($model,'created',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
