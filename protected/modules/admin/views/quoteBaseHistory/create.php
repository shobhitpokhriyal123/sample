<?php
$this->breadcrumbs=array(
	'Quote Base Histories'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List QuoteBaseHistory','url'=>array('index')),
	array('label'=>'Manage QuoteBaseHistory','url'=>array('admin')),
);
?>

<h1>Create QuoteBaseHistory</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>