<?php
$this->breadcrumbs=array(
	'Quote Base Histories'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List QuoteBaseHistory','url'=>array('index')),
	array('label'=>'Create QuoteBaseHistory','url'=>array('create')),
	array('label'=>'View QuoteBaseHistory','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage QuoteBaseHistory','url'=>array('admin')),
);
?>

<h1>Update QuoteBaseHistory <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>