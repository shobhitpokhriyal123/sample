<section id="create_product">
    <div class="container-fluid">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <strong>Quote History</strong>
                    </div>
                    <?php
                    // if (Yii::app()->user->isAdmin) {
                    //echo CHtml::link('Print Invoices', array('/admin/quote/pdf'), array("class" => 'btn btn-success', 'id' => 'invoices'));
                    //  }
                    ?>
                </div>
                <div class="card-body">
                    <?php
                    $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => 'quote-base-history-grid',
                        'dataProvider' => $model->search(),
                        'afterAjaxUpdate' => 'function(id,options){ paginationControls(); }',
                        'filter' => $model,
                        'columns' => array(
                            'id' => array(
                                'name' => 'id',
                                'header' => 'Quote ID',
                                'value' => '$data->getLink()',
                                'filter' => false,
                                'type' => 'raw'
                            ),
                            'create_user_id' => array(
                                'name' => 'create_user_id',
                                'value' => '$data->user->username',
                                'filter' => false,
                                'header' => 'Searched By',
                                
                            ),
                            'assigned_to' => array(
                                'name' => 'assigned_to',
                                'header' => 'Supplier',
                                'filter' => CHtml::listData(Users::model()->findAllByAttributes(array('role_id' => 2)), 'id', 'fullname'),
                                'value' => '$data->supplier->username',
                            ),
                            'priceType' => array(
                                'name' => 'priceType',
                                'filter' => false,
                                'value' => '( $data->priceType && $data->priceType == 1) ? "Distributer" : "Post Code"'
                            ),
//                            'distributor' => array(
//                                'filter' => false,
//                                'name' => 'distributor'
//                            ),
                            'type' => array(
                                'name' => 'type',
                                'filter' => false,
                            ),
                            'postcode'=>array(
                                'name'=>'postcode',
                                'value' => '($data->postcode) ? $data->postcode : $data->distributor',
                                'header' => 'Dist / Postcode',
                                'filter' => false,
                            ),
                            'aq'=>array(
                                'name'=>'aq',
                                'filter' =>false,
                            ),
                            'start_date'=>array(
                                'name'=>'start_date',
                                'filter' => false
                            ),
//                            'business_type' => array(
//                                'name' => 'business_type',
//                                
//                                //'value' => ''
//                            ),
//                            'meter_type'=>array(
//                                'name'=>'meter_type',
//                                'value'=> 'MeterType::getMeterTypes($data->meter_type)',
//                            ),
                            'created',
//                            array(
//                                'class' => 'bootstrap.widgets.TbButtonColumn',
//                                'template' => '{view}',
//                                'buttons' => array(
//                                    'view' => array(
//                                        'label' => '<button class="btn btn-primary">View</button>',
//                                        'url' => 'Yii::app()->createAbsoluteUrl("admin/quote/create", array("id"=>$data->id))',
//                                        'options' => array( 'rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'View'),
//                                        'imageUrl' => false,
//                                        'click' => "function(e){
//                                            e.preventDefault();
//                                            var url = $(this).attr('href');
//                                            
//                                         }",
//                                    ),
//                                ),
//                            ),
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>