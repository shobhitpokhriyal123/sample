<?php
$this->breadcrumbs=array(
	'Quote Base Histories'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List QuoteBaseHistory','url'=>array('index')),
	array('label'=>'Create QuoteBaseHistory','url'=>array('create')),
	array('label'=>'Update QuoteBaseHistory','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete QuoteBaseHistory','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage QuoteBaseHistory','url'=>array('admin')),
);
?>

<h1>View QuoteBaseHistory #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'create_user_id',
		'assigned_to',
		'priceType',
		'distributor',
		'postcode',
		'aq',
		'start_date',
		'business_type',
		'meter_type',
		'created',
	),
)); ?>
