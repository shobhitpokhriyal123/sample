<?php
$this->breadcrumbs=array(
	'Quote Base Histories',
);

$this->menu=array(
	array('label'=>'Create QuoteBaseHistory','url'=>array('create')),
	array('label'=>'Manage QuoteBaseHistory','url'=>array('admin')),
);
?>

<h1>Quote Base Histories</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
