<section id="manage_products">
    <div class="container-fluid">
        <div class="row">
            <div class="card">

                <div class="card-header">
                    <div class="card-title">
                        <strong>Site Electric Meters</strong>
                    </div>

                    <?php
                    echo CHtml::link('Manage Sites', array('/admin/leadContacts/sites'), array("class" => 'btn btn-success'));
                    ?>
                </div>
                <div class="card-body">
                    <?php
                    $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => 'lead-elc-meter-grid',
                        'dataProvider' => $elcmodel->searchBySite($site_id),
                        'afterAjaxUpdate' => 'function(id,options){ paginationControls(); }',
                        // 'filter' => $model,
                        'columns' => array(
                            'site_id' => array(
                                'filter' => false, //CHtml::listData(Users::getAllSuppliers(), 'id', 'fullname'), 'type' => 'html',
                                'header' => 'Site',
                                'name' => 'site_id',
                                'value' => '$data->site->site_name',
                            //'footer' => LeadElcMeter::getSiteDropdown()
                            ),
                            'current_supplier' => array(
                                'type' => 'raw',
                                'filter' => false, //CHtml::listData(Users::getAllSuppliers(), 'id', 'fullname'), 'type' => 'html',
                                'name' => 'current_supplier',
                                'value' => 'CHtml::image(Yii::app()->createAbsoluteUrl("admin/users/thumbnail/file/".$data->supplier->image))',
                            //'footer' => LeadElcMeter::getSupplierDropdown()
                            ),
                            'corporate_sme' => array(
                                'type' => 'raw',
                                'filter' => false, //CHtml::listData(Users::getAllSuppliers(), 'id', 'fullname'), 'type' => 'html',
                                'name' => 'corporate_sme',
                                'value' => 'Product::getCorporateSME($data->corporate_sme)',
                            //'footer' => LeadElcMeter::getCorporateDropdown()
                            ),
                            'utility' => array(
                                'type' => 'raw',
                                'filter' => false, //CHtml::listData(Users::getAllSuppliers(), 'id', 'fullname'), 'type' => 'html',
                                'name' => 'utility',
                                'value' => 'Product::getUtilites($data->utility)',
                            // 'footer' => LeadElcMeter::getUtilityDropdown()
                            ),
                            'meter_name' => array(
                                'filter' => false,
                                'name' => 'meter_name',
                            // 'footer' => '<input id="LeadElcMeter_meter_name_" class="span5 form-control" maxlength="255" name="LeadElcMeter[meter_name]" type="text">'
                            ),
                            'mpan' => array(
                                'filter' => false,
                                'name' => 'mpan',
                            //  'footer' => '<input id="LeadElcMeter_mpan_" class="span5 form-control" maxlength="255" name="LeadElcMeter[mpan]" type="text">'
                            ),
                            'total_eac' => array(
                                'filter' => false,
                                'name' => 'total_eac',
                            //'footer' => '<input id="LeadElcMeter_total_eac_" class="span5 form-control" maxlength="255" name="LeadElcMeter[total_eac]" type="text">'
                            ),
                            'contract_end_date' => array(
                                'filter' => false,
                                'name' => 'contract_end_date',
                            // 'footer' => '<input id="LeadElcMeter_contract_end_date_" class="form-control " name="LeadElcMeter[contract_end_date]" type="text">'
                            ),
                            'meter_status' => array(
                                'filter' => false,
                                'name' => 'meter_status',
                                'value' => 'MeterStatus::getStatus($data->meter_status)',
                            // 'footer' => LeadElcMeter::getStatusDropdown()
                            ),
                        ),
                    ));
                    ?>






                </div> 
                <div class="card-header">
                    <div class="card-title">
                        <strong>Site Gas Meters</strong>
                    </div>
                </div>
                <div class="card-body">
                    <?php
                    $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => 'lead-gas-meter-grid',
                        'dataProvider' => $gasmodel->searchBySite($site_id),
                        'afterAjaxUpdate' => 'function(id,options){ paginationControls(); }',
                        //'filter' => $model,
                        'columns' => array(
                            'site_id' => array(
                                'name' => 'site_id',
                                'value' => '$data->site->site_name',
                                'filter' => false,
                            //  'footer' => LeadGasMeter::getSiteDropdown()
                            ),
                            'current_supplier' => array(
                                'type' => 'raw',
                                'filter' => false, //CHtml::listData(Users::getAllSuppliers(), 'id', 'fullname'), 'type' => 'html',
                                'name' => 'current_supplier',
                                'value' => 'CHtml::image(Yii::app()->createAbsoluteUrl("admin/users/thumbnail/file/".$data->supplier->image))',
                            //  'footer' => LeadGasMeter::getSupplierDropdown()
                            ),
                            'corporate_sme' => array(
                                'type' => 'raw',
                                'filter' => false, //CHtml::listData(Users::getAllSuppliers(), 'id', 'fullname'), 'type' => 'html',
                                'name' => 'corporate_sme',
                                'value' => 'Product::getCorporateSME($data->corporate_sme)',
                            // 'footer' => LeadGasMeter::getCorporateDropdown()
                            ),
                            'utility' => array(
                                'type' => 'raw',
                                'filter' => false, //CHtml::listData(Users::getAllSuppliers(), 'id', 'fullname'), 'type' => 'html',
                                'name' => 'utility',
                                'value' => 'Product::getUtilites($data->utility)',
                            //  'footer' => LeadGasMeter::getUtilityDropdown()
                            ),
                            'meter_name' => array(
                                'filter' => false,
                                'name' => 'meter_name',
                            // 'footer' => '<input id="LeadGasMeter_meter_name_" class="span5 form-control" maxlength="255" name="LeadGasMeter[meter_name]" type="text">'
                            ),
                            'mpr' => array(
                                'filter' => false,
                                'name' => 'mpr',
                            // 'footer' => '<input id="LeadGasMeter_mpr_" class="span5 form-control" maxlength="255" name="LeadGasMeter[mpr]" type="text">'
                            ),
                            'total_aq' => array(
                                'name' => 'total_aq',
                                'filter' => false,
                            //  'footer' => '<input id="LeadGasMeter_total_aq_" class="span5 form-control" maxlength="255" name="LeadGasMeter[total_aq]" type="text">'
                            ),
                            'contract_end_date' => array(
                                'name' => 'contract_end_date',
                                'filter' => false,
                            //  'footer' => '<input id="LeadGasMeter_contract_end_date_" class="form-control hasDatepicker" name="LeadGasMeter[contract_end_date]" type="text">'
                            ),
                            'meter_status' => array(
                                'name' => 'meter_status',
                                'value' => 'MeterStatus::getStatus($data->meter_status)',
                                'filter' => false,
                            // 'footer' => LeadGasMeter::getStatusDropdown()
                            ),
                        ),
                    ));
                    ?>
                </div>


                <div class="card-header">
                    <div class="card-title">
                        <strong>Additional Details</strong>
                    </div>
                </div>


                <div class="col-md-12">
                    <?php
                    //   $site_id = array($site->id);
                    echo $this->renderPartial('additional', array('model' => $additional, 'sites' => $sites));
                    ?>   
                </div>
            </div>  
        </div>
    </div>
</section>


