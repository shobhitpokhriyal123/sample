<div class="card-header">
    <div class="card-title">
        <strong> Site Contact Details</strong>
    </div>
</div>
<?php

$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'lead-site-grid',
    'dataProvider' => $model->searchGrid($lead_id),
    'afterAjaxUpdate' => 'function(id,options){ paginationControls(); }',
    'filter' => $model,
    'columns' => array(
        //'id',
//        'lead_id' => array(
//            'name' => 'lead_id',
//            'value' => '$data->lead->first_name',
//            'header' => 'Site Contact',
//            'filter' => CHtml::listData(LeadContacts::model()->findAll(), 'id', 'first_name'),
//        ),
        //'create_user_id',
        'site_name' => array(
            'name' => 'site_name',
            'filter' => false
        ),
        'site_contact' => array(
            'name' => 'site_contact',
            'value' => '$data->contact->full_name',
            'filter' => false
        ),
        'site_address_name' => array(
            'header' => 'Address',
            'value' => '$data->address->site_address_name',
            'filter' => false,
        ),
        'address_no' => array(
            'header' => 'Address No',
            'value' => '$data->address->site_address_no',
            'filter' => false,
        ),
        'site_street1' => array(
            'header' => 'Site Street',
            'value' => '$data->address->site_street1',
            'filter' => false,
        ),
        'site_street2' => array(
            'header' => 'Site District',
            'value' => '$data->address->site_street2',
            'filter' => false,
        ),
        'site_town' => array(
            'header' => 'Town/City',
            'value' => '$data->address->site_town',
            'filter' => false,
        ),
        'site_country' => array(
            'header' => 'Site County',
            'value' => '$data->address->site_country',
            'filter' => false,
        ),
        'site_post_code' => array(
            'header' => 'Post Code',
            'value' => '$data->address->site_post_code',
            'filter' => false,
        ),
        'site_post_code' => array(
            'header' => 'Post Code',
            'value' => '$data->address->site_post_code',
            'filter' => false,
        ),
        'billing_name' => array(
            'header' => 'Billing Name',
            'value' => '$data->address->billing_name',
            'filter' => false,
        ),
        'billing_no' => array(
            'header' => 'Billing No',
            'value' => '$data->address->billing_no',
            'filter' => false,
        ),
        'street_address_1' => array(
            'header' => 'Billing St.Name',
            'value' => '$data->address->street_address_1',
            'filter' => false,
        ),
        'street_address_1' => array(
            'header' => 'Billing St.Name',
            'value' => '$data->address->street_address_1',
            'filter' => false,
        ),
        'street_address_2' => array(
            'header' => 'District',
            'value' => '$data->address->street_address_2',
            'filter' => false,
        ),
        'town' => array(
            'header' => 'Town',
            'value' => '$data->address->town',
            'filter' => false,
        ),
        'country' => array(
            'header' => 'Billing Country',
            'value' => '$data->address->country',
            'filter' => false,
        ),
        'billing_post_code' => array(
            'header' => 'Billing Post Code',
            'value' => '$data->address->billing_post_code',
            'filter' => false,
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{update}{delete}',
            'header' => 'Actions',
            'buttons' => array(
                'update' => array(
                    'label' => '<button class="btn btn-primary">Update</button>',
                    'url' => 'Yii::app()->createAbsoluteUrl("admin/leadSite/siteupdate", array("id"=>$data->id))',
                    'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Update'),
                    'imageUrl' => false,
                    'click' => "function(e){
                               e.preventDefault();
                               var url = $(this).attr('href');
                               $.ajax({
                                 type: 'POST',
                                 url: url,
                                 success: function(res){
                                 res = $.parseJSON(res);
                              console.log(res.site);
                                for (var property in res.site) {
                                        if (res.site.hasOwnProperty(property)) { 
                                        
                                         $('#LeadSite_'+property).val(res.site[property]);
                                    }
                                }
                                for (var property in res.contact) {
                                        if (res.contact.hasOwnProperty(property)) { 
                                         $('#LeadSiteContactDetails_'+property).val(res.contact[property]);
                                    }
                                }
                                for (var property in res.address) {
                                        if (res.address.hasOwnProperty(property)) { 
                                         $('#LeadSiteAddress_'+property).val(res.address[property]);
                                    }
                                }
                                    var current = $('#lead-site-details-form').attr('action');
                                    $('#lead-site-details-form').attr('action',current+'&id='+res.site.id);
                                 },
                                });
                                
                           
                            }",
                ),
            ),
        ),
    ),
));
?>
