<?php
$this->breadcrumbs=array(
	'Lead Sites'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List LeadSite','url'=>array('index')),
	array('label'=>'Create LeadSite','url'=>array('create')),
	array('label'=>'Update LeadSite','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete LeadSite','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage LeadSite','url'=>array('admin')),
);
?>

<h1>View LeadSite #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'lead_id',
		'create_user_id',
		'site_name',
		'client_reference',
		'external_reference',
		'date_occupency',
		'cost_code',
		'change_tenancy',
		'created',
	),
)); ?>
