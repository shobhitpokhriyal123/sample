<?php
$this->breadcrumbs=array(
	'Lead Sites',
);

$this->menu=array(
	array('label'=>'Create LeadSite','url'=>array('create')),
	array('label'=>'Manage LeadSite','url'=>array('admin')),
);
?>

<h1>Lead Sites</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
