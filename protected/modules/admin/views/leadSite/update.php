<?php
$this->breadcrumbs=array(
	'Lead Sites'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List LeadSite','url'=>array('index')),
	array('label'=>'Create LeadSite','url'=>array('create')),
	array('label'=>'View LeadSite','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage LeadSite','url'=>array('admin')),
);
?>

<h1>Update LeadSite <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>