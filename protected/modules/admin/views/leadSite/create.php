<?php
$this->breadcrumbs=array(
	'Lead Sites'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List LeadSite','url'=>array('index')),
	array('label'=>'Manage LeadSite','url'=>array('admin')),
);
?>

<h1>Create LeadSite</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>