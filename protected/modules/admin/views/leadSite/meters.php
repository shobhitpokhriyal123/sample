<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'customer-tender-form',
    'action' => $this->createAbsoluteUrl("leadSite/getMeters/type/$type/business/$business"),
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        )
);
?>
<section id="manage_products">
    <div class="container-fluid">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <strong>Meter Overview of <?php echo $type ?> for business <?php echo $business ?></strong>
                    </div>

                </div>
                <div class="card-body">
                    <table class="table table-striped table-condensed">
                        <tr>
                            <th>Select</th>
                            <th>Meter Name</th>
                            <th>Site</th>
                            <th>Business Name</th>
                            <th>MPR/MPAN</th>
                            <th>Contract End Date</th>
                            <th>Consumption</th>
                        </tr>
                        <?php
                        if (!empty($meters)) {
                            foreach ($meters as $meter) {
                                ?>
                                <tr>
                                    <td><input type="checkbox" name="meter_id[]" value="<?php echo $meter->id ?>" ></td>
                                    <td><?php echo $meter->meter_name; ?></td>
                                    <td><?php echo $meter->site->site_name; ?></td>
                                    <td><?php echo $business; ?></td>
                                    <td><?php echo ($type == "Gas") ? $meter->mpr : $meter->mpan; ?></td>
                                    <td><?php echo $meter->contract_end_date; ?></td>
                                    <td><?php echo ($type == "Gas") ? $meter->total_aq : $meter->total_eac; ?></td>
                                </tr>
                                <?php
                            }
                        } else {
                            ?>
                            <tr>
                                <td colspan="7">No Meter found for business...!</td>
                            </tr>
                        <?php } ?>
                    </table> 
                </div>
            </div>
        </div>
    </div>
</section>





<div class="card-header">
    <div class="card-title">
        <strong>Customer Tender & Meter Details </strong>
    </div>
</div>

<div class="col-md-12" id="tender_part">
    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <div class="col-md-6">
        <?php echo $form->dropdownListRow($l_tender, 'agency_id', Users::getAgencies(), array('onchange' => 'getAgencyId($(this).val())', 'class'=>'form-control')); ?>
    </div>


    <div class="col-md-6">
        <?php echo $form->dropdownListRow($l_tender, 'agent_id', CHtml::listdata(Users::model()->findAllByAttributes(array('role_id' => 4)), "id", "username"), array('class'=>'form-control')); ?>
    </div>

    <div class="col-md-6">
        <?php echo $form->textFieldRow($l_tender, 'contract_duration', array('class' => 'form-control', 'maxlength' => 255)); ?>
    </div>

    <div class="col-md-6">
        <?php echo $form->label($l_tender, "supplier_ids"); ?>
        <?php
        if ($l_tender->supplier_ids)
            $l_tender->supplier_ids = explode(",", $model->supplier_ids);
        $ab = CHtml::listData(Users::model()->findAllByAttributes(array('role_id' => 2)), 'id', 'fullname');
        ?>

        <?php echo CHtml::activeListBox($l_tender, 'supplier_ids', $ab, array('class' => 'chosen form-control', 'multiple' => true, 'data-placeholder' => 'Select')) ?>
        <?php
        $this->widget('ext.chosen.EChosenWidget', array(
            'selector' => '.chosen',
        ));
        ?>
        <?php echo $form->error($l_tender, "supplier_ids"); ?>
    </div>

    <div class="col-md-6">
        <?php echo $form->textFieldRow($l_tender, 'bespoke_duration', array('class' => 'form-control', 'maxlength' => 255)); ?>
    </div>

    <div class="col-md-6">
        <?php echo $form->textFieldRow($l_tender, 'commission_discount', array('class' => 'form-control', 'maxlength' => 255)); ?>
    </div>

    <div class="col-md-6">

        <?php echo $form->label($l_tender, 'tender_return_date'); ?>
        <?php
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model' => $l_tender,
            'attribute' => 'tender_return_date',
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                'changeMonth' => true,
                'changeYear' => true,
            ),
            'htmlOptions' => array(
                'class' => "form-control"
            ),
        ));
        ?>

        <?php echo $form->error($l_tender, 'tender_return_date'); ?>
    </div>

    <div class="col-md-6">
        <?php echo $form->textFieldRow($l_tender, 'business_name', array('class' => 'form-control', 'maxlength' => 255, 'value' => $business, 'readOnly' => true)); ?>
    </div>

    <div class="col-md-6">
        <?php echo $form->textFieldRow($l_tender, 'com_reg', array('class' => 'form-control', 'maxlength' => 255)); ?>
    </div>

    <div class="col-md-6">
        <?php
        echo $form->hiddenField($l_tender, 'commodity_type', array('value' => ($type == "Gas" ) ? 2 : 1));
        ?>
    </div>

    <div class="col-md-6">
        <?php echo $form->textFieldRow($l_tender, 'uplift', array('class' => 'form-control', 'maxlength' => 255)); ?>
    </div>

    <div class="col-md-6">
        <?php
        echo $form->dropdownListRow($l_tender, 'term_length', LeadContacts::getTermLength(), array('class' => 'form-control', 'maxlength' => 255));
        ?>

    </div>
    <div class="col-md-6">
        <?php echo $form->textFieldRow($l_tender, 'term_length', array('class' => 'form-control', 'maxlength' => 255)); ?>
    </div>

    <div class="col-md-6">
        <?php echo $form->textFieldRow($l_tender, 'fixed_meter', array('class' => 'form-control', 'maxlength' => 255)); ?>
    </div>

    <div class="col-md-6">

        <?php
        echo $form->dropdownListRow($l_tender, 'contract_type', CHtml::listData(ContractType::model()->findAll(), 'id', 'title'), array("class" => "form-control"));
        ?>

    </div>

    <div class="col-md-6">
        <?php
        echo $form->dropdownListRow($l_tender, 'payment', PaymentType::getPayment(), array("class" => "form-control"));
        ?>

    </div>


    <div class="col-md-6">
        <?php
        echo $form->dropdownListRow($l_tender, 'energy', ContractType::getEnergy(), array("class" => "form-control"));
        ?>

    </div>

    <div class="col-md-6">
        <?php echo $form->textFieldRow($l_tender, 'supplier_pref', array("class" => "form-control", 'size' => 20, 'maxlength' => 20)); ?>
    </div>

    <div class="col-md-6">
        <?php echo $form->textAreaRow($l_tender, 'comments', array('rows' => 6, 'cols' => 50, "class" => "form-control")); ?>
    </div>


    <hr>
    <div  class="col-md-12">

        <div class="card-header">
            <div class="card-title">
                <strong>Upload Tender Files </strong>
            </div>
        </div>
        <?php echo $form->fileFieldRow($l_tender, 'document', array("class" => "form-control")); ?>



    </div>

    <!--	<div class="row buttons">
    <?php //echo CHtml::submitButton($l_tender->isNewRecord ? 'Create' : 'Save');  ?>
            </div>-->
    <div class="form-actions col-lg-12">
        <br>
        <button type="submit" class="btn btn-success" id="sdfgfsadfg">Submit Tender</button>
    </div>
</div>
<?php $this->endWidget(); ?>


<style>
    .chosen-container{
        width: 100%;
        height: 37px;
    }
</style>

<script>
    getAgencyId($("#LeadTender_agency_id").val());
    function getAgencyId(id) {
        var siteagency = '<?php echo Yii::app()->createAbsoluteUrl("/admin/leadContacts/getAgencyId") ?>/id/' + id;
        $.ajax({
            url: siteagency,
            type: 'POST',
            success: function (res) {
                $("#LeadTender_agent_id").html(res);
            }
        });
    }
</script>



<!--  <button id="sdfgfsadfg" name="tender" class="btn btn-success" type="submit">Submit Meter</button>    -->