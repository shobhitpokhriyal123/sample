
<?php

$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'lead-additional-grid',
    'dataProvider' => $model->search($a),
    //'filter' => $model,
    'columns' => array(
        //'id',
        'site_id'=>array(
            'name'=>'site_id',
            'value' => '$data->site->site_name'
        ),
      //  'create_user_id',
        'tel_provider',
        'tel_s_date',
        'tel_e_date',
        'tel_ac_no',
        'tel_ac_pwd',
        'broad_provider',
        'broad_s_date',
        'broad_e_date',
        'broad_ac_no',
        'broad_ac_pwd',
        'tv_provider',
        'tv_s_date',
        'tv_e_date',
        'tv_ac_no',
        'tv_ac_pwd',
        'water_provider',
        'water_s_date',
        'water_e_date',
        'water_ac_no',
        'water_ac_pwd',
//        array(
//            'class' => 'bootstrap.widgets.TbButtonColumn',
//            'template' => '{update}'
//        ),
    ),
));
?>
