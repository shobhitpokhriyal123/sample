<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lead_id')); ?>:</b>
	<?php echo CHtml::encode($data->lead_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_user_id')); ?>:</b>
	<?php echo CHtml::encode($data->create_user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('site_name')); ?>:</b>
	<?php echo CHtml::encode($data->site_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('client_reference')); ?>:</b>
	<?php echo CHtml::encode($data->client_reference); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('external_reference')); ?>:</b>
	<?php echo CHtml::encode($data->external_reference); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_occupency')); ?>:</b>
	<?php echo CHtml::encode($data->date_occupency); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('cost_code')); ?>:</b>
	<?php echo CHtml::encode($data->cost_code); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('change_tenancy')); ?>:</b>
	<?php echo CHtml::encode($data->change_tenancy); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />

	*/ ?>

</div>