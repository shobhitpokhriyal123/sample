<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'agency-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'user_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'agency_logo',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'agency_file',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'agency_id',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'company_pic',array('class'=>'span5','maxlength'=>255)); ?>

	<?php  echo $form->textFieldRow($model,'letter_authority',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'current_users',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'max_users',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'trending_type',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'principal',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'vat_no',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'date_created',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'date_modified',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'self_billing',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'od_split',array('class'=>'span5','maxlength'=>64)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
