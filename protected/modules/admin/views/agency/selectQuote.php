
<section id="manage_products">
    <div class="container">
        <div class="row">
            <div class="card">

                <?php
                if (isset($_GET['quote_id'])) {
                    $quote_id = $_GET['quote_id'];
                } else {
                    $quote_id = $contract_details->quote_id;
                }
                //   echo $quote_id ; die;
                $quote = Quote::model()->findByPk($quote_id);
                //echo '<pre>';print_r($quote); die;
                $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                    'id' => 'agency-form',
                    'enableAjaxValidation' => false,
                    'enableClientValidation' => true,
                    'clientOptions' => array(
                        'validateOnSubmit' => true,
                    ),
                    'htmlOptions' => array('enctype' => 'multipart/form-data'),
                ));
                ?>

                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="formBlocks">
                                <h3  class="heading">Contact Details</h3>
                                <hr>
                                <?php //echo $form->errorSummary($model);  ?>


                                <?php echo $form->textFieldRow($company, 'gender', array('class' => 'span5')); ?>
                                <?php echo $form->textFieldRow($company, 'contact_name', array('class' => 'span5', 'maxlength' => 255)); ?>
                                <?php echo $form->textFieldRow($company, 'contact_last_name', array('class' => 'span5', 'maxlength' => 255)); ?>

                                <?php echo $form->textFieldRow($company, 'telephone', array('class' => 'span5', 'maxlength' => 255)); ?>
                                <?php echo $form->textFieldRow($company, 'mobile', array('class' => 'span5', 'maxlength' => 255)); ?>
                                <?php echo $form->textFieldRow($company, 'contact_email', array('class' => 'span5', 'maxlength' => 255)); ?>


                                <?php echo $form->textFieldRow($company, 'contact_position', array('class' => 'span5', 'maxlength' => 255)); ?>

                                <h3  class="heading">Business Details : Registered Business Address</h3>
                                <hr>

                                <?php echo $form->textFieldRow($company, 'company', array('class' => 'span5')); ?>
                                <?php echo $form->textFieldRow($company, 'registraion_no', array('class' => 'span5', 'maxlength' => 255)); ?>
                                <?php echo $form->textFieldRow($company, 'address_no', array('class' => 'span5', 'maxlength' => 255)); ?>

                                <?php echo $form->textFieldRow($company, 'street', array('class' => 'span5', 'maxlength' => 255)); ?>
                                <?php echo $form->textFieldRow($company, 'town_city', array('class' => 'span5', 'maxlength' => 255)); ?>
                                <?php echo $form->textFieldRow($company, 'county', array('class' => 'span5', 'maxlength' => 255)); ?>

                                <?php //echo $form->textFieldRow($company, 'address', array('class' => 'span5', 'maxlength' => 255));  ?>
                                <?php echo $form->textFieldRow($company, 'postcode', array('class' => 'span5', 'maxlength' => 255)); ?>
                                <?php echo $form->dropDownListRow($company, 'business_type', CHtml::listData(BusinessStructure::model()->findAll(), 'id', 'title')); ?>


                                <hr>
                                <?php //echo $form->errorSummary($model);  ?>

                                <?php echo $form->checkBoxRow($business_address, 'is_site_address', array('onclick' => 'fillAddress()', 'class' => 'span5')); ?>
                                <h3 class="heading">Supply Address</h3>
                                <hr>
                                <?php echo $form->textFieldRow($business_address, 'customer_name', array('class' => 'span5')); ?>
                                <?php echo $form->textFieldRow($business_address, 'home_address_no', array('class' => 'span5')); ?>

                                <?php echo $form->textFieldRow($business_address, 'home_street1', array('class' => 'span5')); ?>
                                <?php //echo $form->textFieldRow($business_address, 'home_street2', array('class' => 'span5')); ?>


                                <?php echo $form->textFieldRow($business_address, 'home_town', array('class' => 'span5')); ?>
                                <?php //echo $form->textFieldRow($business_address, 'home_building', array('class' => 'span5')); ?>
                                <?php echo $form->textFieldRow($business_address, 'home_country', array('class' => 'span5')); ?>
                                <?php echo $form->textFieldRow($business_address, 'home_postcode', array('class' => 'span5')); ?>
                                <?php //echo $form->textFieldRow($business_address, 'years_of_address', array('class' => 'span5')); ?>
                                <!--                                <label for="Product_valid_till">DOB*</label>-->
                                <?php
//                                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
//                                    'model' => $business_address,
//                                    'attribute' => 'dob',
//                                    'options' => array(
//                                        'dateFormat' => 'yy-mm-dd',
//                                        'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
//                                        'changeMonth' => true,
//                                        'changeYear' => true,
//                                    ),
//                                ));
                                ?>




                                <h3  class="heading">Business Activity</h3>
                                <hr>
                                <?php echo $form->checkBoxRow($company, 'is_micro_business', array('class' => 'span5', 'maxlength' => 255)); ?>
                                <?php echo $form->textFieldRow($company, 'no_of_emp', array('class' => 'span5', 'maxlength' => 255)); ?>
                                <?php echo $form->textFieldRow($company, 'no_of_years', array('class' => 'span5', 'maxlength' => 255)); ?>
                                <?php echo $form->textFieldRow($company, 'turnover', array('class' => 'span5', 'maxlength' => 255)); ?>


                                <?php //echo $form->textFieldRow($company, 'payment_date', array('class' => 'span5', 'maxlength' => 255));  ?>


                                <?php //echo $form->textFieldRow($company, 'status', array('class' => 'span5', 'maxlength' => 255));  ?>

                            </div>

                            <div class="formBlocks">

                                <h3>Bank Details</h3>
                                <hr>
                                <?php echo $form->textFieldRow($bank, 'bank_name', array('class' => 'span5')); ?>

                                <?php echo $form->textFieldRow($bank, 'account_no', array('class' => 'span5')); ?>

                                <?php echo $form->textFieldRow($bank, 'sort_code', array('class' => 'span5')); ?>
                                <?php echo $form->textFieldRow($bank, 'account_number', array('class' => 'span5')); ?>
                                <?php //echo $form->labelEx($bank, 'payment_date'); ?>
                                <?php
//                                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
//                                    'model' => $bank,
//                                    'attribute' => 'payment_date',
//                                    'options' => array(
//                                        'dateFormat' => 'yy-mm-dd',
//                                        'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
//                                        'changeMonth' => true,
//                                        'changeYear' => true,
//                                        'yearRange' => '1950:2099',
//                                    ),
//                                        // 'language' => Yii::app()->language,
//                                ));
                                ?>
                                <?php //echo $form->error($bank, 'payment_date'); ?>

                                <?php echo $form->dropDownListRow($bank, 'days', QuoteDate::getNumbers()); ?>

                            </div>

                            <div class="formBlocks">

                                <h3>Additional Docs</h3>
                                <hr>
                                <div class="inlineRadio">
                                    <?php echo $form->radioButtonListRow($contract_Files, 'is_paper', Company::getTrueFalse()); ?>
                                </div>

                                <?php echo $form->dropDownListRow($contract_Files, 'is_copy_of_original', Company::getTrueFalse()); ?>

                                <?php
                                echo $form->fileFieldRow($contract_Files, 'files', array('class' => 'span5'));
                                if ($contract_Files->files) {
                                    ?>
                                    <a class="btn btn-primary" href="<?php echo Yii::app()->createAbsoluteUrl('admin/product/download/file/' . $contract_Files->files) ?>"><?php echo $contract_Files->files ?><i class="icon-download"></i></a>

                                <?php }
                                ?>
                                <br>
                                <label for="Product_valid_till">Contract signed date*</label>
                                <?php
                                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                    'model' => $contract_Files,
                                    'attribute' => 'contract_signed_date',
                                    'options' => array(
                                        'dateFormat' => 'yy-mm-dd',
                                        'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                                        'changeMonth' => true,
                                        'changeYear' => true,
                                        'yearRange' => '1950:2099',
                                    ),
                                ));
                                ?>


                            </div>

                        </div>

                        <div class="col-sm-6">
                            <div class="formBlocks">
                                <h3>Quote/Contract Details</h3>
                                <hr>
                                <?php
                                if (Yii::app()->user->isAgency || Yii::app()->user->isAdmin) {
                                    echo $form->dropDownListRow($contract_details, 'agency_id', array(Yii::app()->user->id => Yii::app()->user->username), array('onchange' => 'getAgencyDetail($(this))', 'readonly' => true));
                                } else if (Yii::app()->user->isUser) {

                                    $user1 = Users::model()->findByPk(Yii::app()->user->agency_id);
                                    echo $form->dropDownListRow($contract_details, 'agency_id', array($user1->id => $user1->username), array('onchange' => 'getAgencyDetail($(this))', 'readonly' => true));
                                }
                                ?>
                                <?php
                                if (Yii::app()->user->isAgency || Yii::app()->user->isAdmin) {
                                    echo $form->textFieldRow($contract_details, 'agency_name', array('class' => 'span5', 'readonly' => true, 'value' => Yii::app()->user->username));
                                } else if (Yii::app()->user->isUser) {
                                    echo $form->textFieldRow($contract_details, 'agency_name', array('class' => 'span5', 'readonly' => true, 'value' => $user1->username));
                                }
                                ?>
                                <?php
                                if (Yii::app()->user->isAgency || Yii::app()->user->isAdmin) {
                                    echo $form->dropDownListRow($contract_details, 'agent_id', Agency::getAgents(), array('empty' => '----Select Agent----'));
                                } else if (Yii::app()->user->isUser) {
                                    echo $form->dropDownListRow($contract_details, 'agent_id', array(Yii::app()->user->id => Yii::app()->user->username));
                                }
                                ?>
                                <?php echo $form->textFieldRow($contract_details, 'mpan_topline', array('class' => 'span5')); ?>
                                <?php //echo $form->textFieldRow($contract_details, 'mpan_core', array('class' => 'span5')); ?>
                                <?php echo $form->textFieldRow($contract_details, 'eac', array('class' => 'span5', 'value' => ($quote ) ? $quote->aq : "", 'readonly' => true)); ?>
                                <?php echo $form->dropDownListRow($contract_details, 'current_supplier', array(($quote ) ? $quote->c_supplier->id : '' => ($quote ) ? $quote->c_supplier->username : "")); ?>
                                <?php
                                if ($contract_details->is_corporate == 1) {
                                    echo $form->dropDownListRow($contract_details, 'new_supplier', array($contract_details->new_supplier => $contract_details->n_supplier->username));
                                } else {
                                    echo $form->dropDownListRow($contract_details, 'new_supplier', array($price->productName->user->id => $price->productName->user->username));
                                }
                                ?>
                                <?php echo $form->dropDownListRow($contract_details, 'smart_meter', Company::getTrueFalse()); ?>
                                <?php // echo $form->textFieldRow($contract_details, 'date_time', array('class' => 'span5')); ?>
                                <?php
                                if (Yii::app()->user->isAdmin) {
                                    echo $form->dropDownListRow($contract_details, 'status', CHtml::listData(ContractStatus::model()->findAll(), 'id', 'title'));
                                } else {
                                    echo $form->dropDownListRow($contract_details, 'status', array(2 => 'New'), array('readonly' => true));
                                }
                                ?>


                                <?php
                                echo $form->textFieldRow($contract_details, 'date_time', array('class' => 'span5', 'readonly' => true, 'value' => date("Y-m-d h:m:s")));

//                                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
//                                    'model' => $contract_details,
//                                    'attribute' => 'date_time',
//                                    'options' => array(
//                                        'dateFormat' => 'yy-mm-dd',
//                                        'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
//                                        'changeMonth' => true,
//                                        'changeYear' => true,
//                                        'yearRange' => '1950:2099',
//                                    ),
//                                ));
                                ?>



                                <?php
                                if ($contract_details->is_corporate == 1) {
                                    echo $form->dropDownListRow($contract_details, 'renewal', array($contract_details->renewal => ($contract_details->renewal == 0 ) ? "Acquistion" : "Renewal"), array("readonly" => true));
                                } else {
                                    echo $form->dropDownListRow($contract_details, 'renewal', array($price->productName->renewal => ($price->productName->renewal == 0 ) ? "Acquistion" : "Renewal"), array("readonly" => true));
                                }
                                ?>
                                <?php
                                if ($contract_details->is_corporate == 1) {
                                    echo $form->dropDownListRow($contract_details, 'payment_type', array($contract_details->payment_type => ($contract_details->payment_type == 0 ) ? "Cash" : "Direct Debit"), array("readonly" => true));
                                } else {
                                    echo $form->dropDownListRow($contract_details, 'payment_type', array($price->productName->direct_dabit => ($price->productName->direct_dabit == 0 ) ? "Cash" : "Direct Debit"), array("readonly" => true));
                                }
                                ?>
                                <?php
                                if ($contract_details->is_corporate == 0) {
                                    echo $form->textFieldRow($contract_details, 'contract_period', array("value" => ($price->term) ? $price->term : $price->contract_length, 'readonly' => true));
                                } else {
                                    echo $form->textFieldRow($contract_details, 'contract_period', array('readonly' => true));
                                }
                                ?>

                                <!--                                <label for="Product_valid_till">Start date*</label>-->
                                <?php
                                $end_date = $contract_details->contract_date;
                                $date2 = date("Y-m-d H:i:s");
                                if (isset($_GET['date'])) {
                                    $date2 = $_GET['date'];
                                    $months = $_GET['month'];
                                    if ($months == '123') {
                                        $months = '12';
                                    }
                                    $end_date = date('Y-m-d', strtotime("+$months months", strtotime($date2)));
                                }
                                //echo $date2; die;
                                $end_date = date('Y-m-d', strtotime("-1 day", strtotime($end_date)));

//                                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
//                                    'model' => $contract_details,
//                                    'attribute' => 'estimate_date',
//                                    //'value'=>$date2,
//                                    'options' => array(
//                                        'dateFormat' => 'yy-mm-dd',
//                                        'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
//                                        'changeMonth' => true,
//                                        'changeYear' => true,
//                                        'yearRange' => '1950:2099',
//                                    ),
//                                ));
                                ?>
                                <?php
                                echo $form->textFieldRow($contract_details, 'estimate_date', array("value" => $date2, 'readonly' => true));

                                echo $form->textFieldRow($contract_details, 'contract_date', array("value" => $end_date, 'readonly' => true));
                                ?>

                                <!--                                <label for="Product_valid_till">End date*</label>-->
                                <?php
//                                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
//                                    'model' => $contract_details,
//                                    'attribute' => 'contract_date',
//                                    'options' => array(
//                                        'dateFormat' => 'yy-mm-dd',
//                                        'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
//                                        'changeMonth' => true,
//                                        'changeYear' => true,
//                                        'yearRange' => '1950:2099',
//                                    ),
//                                ));
                                ?>
                                <label for="Product_valid_till">Live date*</label>
                                <?php
                                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                    'model' => $contract_details,
                                    'attribute' => 'live_date',
                                    'options' => array(
                                        'dateFormat' => 'yy-mm-dd',
                                        'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                                        'changeMonth' => true,
                                        'changeYear' => true,
                                        'yearRange' => '1950:2099',
                                    ),
                                ));
                                ?>

                                <?php echo $form->dropDownListRow($contract_details, 'billing_period', ContractDetail::getBillingPeriod()); ?>



                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="formBlocks">

                                <h3>Sole Trader LTD - Home Address Details</h3>
                                <hr>
                                <?php echo $form->textFieldRow($home_address, 'customer_name', array('class' => 'span5')); ?>



                                <?php //echo $form->textFieldRow($home_address, 'home_building', array('class' => 'span5'));        ?>
                                <?php echo $form->textFieldRow($home_address, 'home_address_no', array('class' => 'span5')); ?>
                                <?php echo $form->textFieldRow($home_address, 'home_street1', array('class' => 'span5')); ?>
                                <?php echo $form->textFieldRow($home_address, 'home_town', array('class' => 'span5')); ?>
                                <?php // echo $form->textFieldRow($home_address, 'home_street2', array('class' => 'span5'));  ?>
                                <?php echo $form->textFieldRow($home_address, 'home_country', array('class' => 'span5')); ?>

                                <?php echo $form->textFieldRow($home_address, 'home_postcode', array('class' => 'span5')); ?>

                                <?php echo $form->textFieldRow($home_address, 'years_of_address', array('class' => 'span5')); ?>

                                <label for="Product_valid_till">DOB*</label>
                                <?php
                                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                    'model' => $home_address,
                                    'attribute' => 'dob',
                                    'options' => array(
                                        'dateFormat' => 'yy-mm-dd',
                                        'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                                        'changeMonth' => true,
                                        'changeYear' => true,
                                        'yearRange' => '1950:2099',
                                    ),
                                ));
                                ?>

                            </div>
                        </div>





                        <!--
                                                <div class="col-sm-6">
                                                   
                                                </div>-->
                        <!--                        <div class="col-sm-6">-->
                        <!--
                        <div class="formBlocks">

                            <h3>Online Direct Validation</h3>
                            <hr>
                        <?php //echo $form->radioButtonListRow($validation, 'credit', Validation::getPassFail());         ?>
                        <?php //echo $form->radioButtonListRow($validation, 'meter_details', Validation::getPassFail());     ?>
                        <?php //echo $form->radioButtonListRow($validation, 'consumption', Validation::getPassFail());  ?>
                        <?php //echo $form->radioButtonListRow($validation, 'paper_contract', Validation::getPassFail());  ?>
                        <?php //echo $form->radioButtonListRow($validation, 'cot_form', Validation::getPassFail()); ?>
                        <?php //echo $form->radioButtonListRow($validation, 'electorial_check', Validation::getPassFail()); ?>
                        <?php //echo $form->radioButtonListRow($validation, 'verbal_recording', Validation::getPassFail());  ?>
                        <?php //echo $form->radioButtonListRow($validation, 'duplicate_check', Validation::getPassFail());   ?>

                        </div>-->
                        <!--                        </div>-->
                    </div>

                    <div class="form-actions">
                        <?php
                        $this->widget('bootstrap.widgets.TbButton', array(
                            'buttonType' => 'submit',
                            'type' => 'primary',
                            'label' => $company->isNewRecord ? 'Create' : 'Save',
                            'htmlOptions' => array('class' => 'longBtn')
                        ));
                        ?>
                    </div>
                </div>
                <?php $this->endWidget(); ?>
            </div>
        </div>
    </div>
</section>

<script>
    $(function () {
        var data3 = '<?php echo $date2 ?>';
        $("#ContractDetail_estimate_date").val(data3);
        $('#BusinessAddress_home_postcode').keyup(function () {
            this.value = this.value.toUpperCase();
        });
        $('#PartnerAddress_home_postcode').keyup(function () {
            this.value = this.value.toUpperCase();
        });
        $('#Company_postcode').keyup(function () {
            this.value = this.value.toUpperCase();
        });

    });
    $(document).ready(function () {
        $('#BusinessAddress_is_site_address').click(function () {
            if ($('#BusinessAddress_is_site_address').is(":checked")) {
                $('#BusinessAddress_customer_name').val($("#Company_company").val());
                $('#BusinessAddress_home_street1').val($("#Company_street").val());
                $('#BusinessAddress_home_postcode').val($("#Company_postcode").val());
                $('#BusinessAddress_home_address_no').val($("#Company_address_no").val());
                $('#BusinessAddress_home_town').val($("#Company_town_city").val());
                $('#BusinessAddress_home_country').val($("#Company_county").val());


                // $('#BusinessAddress_customer_name').val($("#Company_company").val());
            }
        });

    });
    function fillAddress() {
        if ($('#BusinessAddress_is_site_address').is(":checked")) {
            $('#BusinessAddress_home_postcode').val($('#Company_postcode').val());
            $('#BusinessAddress_home_street1').val($('#Company_address').val());
            $('#BusinessAddress_home_street2').val($('#Company_address').val());

        } else {
            $('#BusinessAddress_home_postcode').val('');
            $('#BusinessAddress_home_street1').val('');
            $('#BusinessAddress_home_street2').val('');
        }
    }
    $(document).ready(function () {
        $("#BusinessAddress_customer_name").keyup(function () {
            $("#PartnerAddress_customer_name").val($(this).val());
        });
    });
    getAgencyDetail($('#ContractDetail_agency_id'));
    function getAgencyDetail(obj) {
        var id = obj.val();
        var url = '<?php echo Yii::app()->createAbsoluteUrl("admin/agency/getAgent&id=") ?>' + id;
        $.ajax({
            url: url,
            type: 'POST',
            success: function (res) {
                $("#ContractDetail_agency_name").val(res);
                // console.log(res.id);
            },
        });
    }

</script>
