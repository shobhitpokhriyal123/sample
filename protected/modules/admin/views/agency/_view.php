<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('agency_logo')); ?>:</b>
	<?php echo CHtml::encode($data->agency_logo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('agency_file')); ?>:</b>
	<?php echo CHtml::encode($data->agency_file); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('agency_id')); ?>:</b>
	<?php echo CHtml::encode($data->agency_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('company_pic')); ?>:</b>
	<?php echo CHtml::encode($data->company_pic); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('letter_authority')); ?>:</b>
	<?php echo CHtml::encode($data->letter_authority); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('current_users')); ?>:</b>
	<?php echo CHtml::encode($data->current_users); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('max_users')); ?>:</b>
	<?php echo CHtml::encode($data->max_users); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('trending_type')); ?>:</b>
	<?php echo CHtml::encode($data->trending_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('principal')); ?>:</b>
	<?php echo CHtml::encode($data->principal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vat_no')); ?>:</b>
	<?php echo CHtml::encode($data->vat_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_created')); ?>:</b>
	<?php echo CHtml::encode($data->date_created); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_modified')); ?>:</b>
	<?php echo CHtml::encode($data->date_modified); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('self_billing')); ?>:</b>
	<?php echo CHtml::encode($data->self_billing); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('od_split')); ?>:</b>
	<?php echo CHtml::encode($data->od_split); ?>
	<br />

	*/ ?>

</div>