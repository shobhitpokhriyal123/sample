<?php
$this->breadcrumbs=array(
	'Agencies'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Agency','url'=>array('index')),
	array('label'=>'Create Agency','url'=>array('create')),
	array('label'=>'Update Agency','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Agency','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Agency','url'=>array('admin')),
);
?>

<h1>View Agency details #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'user_id'=>array(
                    'header'=>'Agency',
                    'name' =>'user_id',
                    'value' => Yii::app()->user->username,
                ),
                'od_split',
                'agent_split',
		//'agency_logo',
		//'agency_file',
		'agency_id',
		//'company_pic',
		//'letter_authority',
		'current_users',
		'max_users',
		'trending_type',
             'trending_type' => array(
                                'filter' => false,
                               'name' => 'trending_type',
                               'id' => 'trending_type',
                                'value' => 'Agency::getTrendingType($data->trending_type)'
                           ),
		'principal',
		'vat_no',
		'date_created',
		'date_modified',
		'self_billing',
            
//                            'self_billing' => array(
//                            'filter' => false,
//                               'name' => 'self_billing',
//                              'id' => 'self_billing',
//                                'value' => '$data->self_billing'
//                         ),
		
	),
)); ?>
