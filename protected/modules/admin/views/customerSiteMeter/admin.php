<section id="manage_productss">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="card-title">
                        <strong>  Manage Meters</strong>
                    </div>

                    <?php
                     if (Yii::app()->user->isAdmin  || Yii::app()->user->isAgency ) {
                        echo CHtml::link('Manage Customers', array('/admin/agencyCustomer/admin'), array("class" => 'btn btn-success'));
                        echo CHtml::link('Manage Sites', array('/admin/customerSite/admin'), array("class" => 'btn btn-success'));
                        echo CHtml::link('Manage Meters', array('/admin/customerSiteMeter/admin'), array("class" => 'btn btn-success'));
                        echo CHtml::link('Manage Contracts', array('/admin/customerContract/admin'), array("class" => 'btn btn-success'));
                        echo CHtml::link('Manage Tenders', array('/admin/customerTender/admin'), array("class" => 'btn btn-success'));
                    }
                    ?>

                </div>
                <div class="card-body">

                    <?php
                    $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => 'customer-site-meter-grid',
                        'dataProvider' => $model->search($sid),
                        'filter' => $model,
                        'columns' => array(
                            //   'id',
                            'site_id' => array(
                                'name' => 'site_id',
                                'value' => '$data->site->site_name'
                            ),
                            //   'create_user_id',
                            'utility' => array(
                                'name' => 'utility',
                                'value' => 'Product::getUtilites($data->utility)'
                            ),
                            'meter_no',
                            'profile_type' => array(
                                'name' => 'profile_type',
                                'value' => 'CustomerSiteMeter::getProfileType($data->profile_type)'
                            ),
                            'meter_type' => array(
                                'name' => 'meter_type',
                                'value' => 'CustomerSiteMeter::getMeterType($data->meter_type)',
                            ),
                            'available_capability',
                            'consumption' => array(
                                'filter' => false,
                                'name' => 'consumption',
                                'type' => 'raw',
                                'value' => '$data->getDownloadLink()'
                            ),
                            //'status',
                            //'created',
                            /**/
                            array(
                                'class' => 'bootstrap.widgets.TbButtonColumn',
                                 'template' => '{update}{view}{delete}',
                                'header' => 'Actions',
//                                'buttons' => array(
//                                    'new_meter_price' => array(
//                                        'label' => '<button class="btn btn-primary">Create Meter</button>',
//                                        'url' => 'Yii::app()->createAbsoluteUrl("admin/customerSiteMeter/create", array("sid"=>$data->id))',
//                                        'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Create Site Meter'),
//                                        'imageUrl' => false,
//                                    ),
//                                    
//                                ),
                            ),
                        ),
                    ));
                    ?>

                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(document).ready(function (e) {
        $(".upload_class").click(function (e) {
            // alert('sdfsdf');
            e.preventDefault();
            var sid = $(this).attr('id');
            $('#myModalddd').modal();
            $.ajax({
                url: '<?php echo Yii::app()->createAbsoluteUrl("admin/customerSiteMeter/upload/sid/") ?>/' + sid,
                type: 'POST',
            }).done(function (res) {
                $("#moddal_id").html(res);
            });
        });

    })


</script>
<div id="myModalddd" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content" id="moddal_id">


        </div>

    </div>

</div>
