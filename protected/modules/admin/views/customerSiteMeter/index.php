<?php
$this->breadcrumbs=array(
	'Customer Site Meters',
);

$this->menu=array(
	array('label'=>'Create CustomerSiteMeter','url'=>array('create')),
	array('label'=>'Manage CustomerSiteMeter','url'=>array('admin')),
);
?>

<h1>Customer Site Meters</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
