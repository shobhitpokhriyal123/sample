<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('site_id')); ?>:</b>
	<?php echo CHtml::encode($data->site_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_user_id')); ?>:</b>
	<?php echo CHtml::encode($data->create_user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('utility')); ?>:</b>
	<?php echo CHtml::encode($data->utility); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meter_no')); ?>:</b>
	<?php echo CHtml::encode($data->meter_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('profile_type')); ?>:</b>
	<?php echo CHtml::encode($data->profile_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meter_type')); ?>:</b>
	<?php echo CHtml::encode($data->meter_type); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('available_capability')); ?>:</b>
	<?php echo CHtml::encode($data->available_capability); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />

	*/ ?>

</div>