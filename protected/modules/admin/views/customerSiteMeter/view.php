<section id="manage_productss">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="card-title">
                        <strong>View Meter</strong>
                    </div>

                    <?php
                     if (Yii::app()->user->isAdmin  || Yii::app()->user->isAgency ) {
                        echo CHtml::link('Update Meter', array('/admin/customerSiteMeter/update', 'id' => $model->id), array("class" => 'btn btn-success'));
                        echo CHtml::link('Manage Meters', array('/admin/customerSiteMeter/admin', 'sid'=>$model->site_id), array("class" => 'btn btn-success'));
                        echo CHtml::link('Manage Site', array('/admin/customerSite/admin'), array("class" => 'btn btn-success'));
                    }
                    ?>

                </div>
                <div class="card-body">

                    <?php
                    $this->widget('bootstrap.widgets.TbDetailView', array(
                        'data' => $model,
                        'attributes' => array(
                            //'id',
                            'site_id' => array(
                                'name' => 'site_id',
                                'value' => $model->site->site_name
                            ),
                            'create_user_id' => array(
                                'name' => 'create_user_id',
                                'value' => $model->createUser->username,
                            ),
                            'utility' => array(
                                'name' => 'utility',
                                'value' => Product::getUtilites($model->utility),
                            ),
                            'meter_no',
                            'profile_type' => array(
                                'name' => 'profile_type',
                                'value' => ''
                            ),
                            'meter_type',
                            'available_capability',
                            'consumption'=>array(
                                'name'=>'consumption',
                                'value'=>CHtml::link('<i class="fa fa-download">download</i>',array('/admin/users/download', 'file'=>$model->consumption)),
                                'type'=>'raw'
                                ),
                            'status',
                            'created',
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
