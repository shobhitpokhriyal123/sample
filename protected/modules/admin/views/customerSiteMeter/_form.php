<div class="row">
    <?php
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'customer-site-meter-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    ));
    ?>

    <p class="help-block">Fields with <span class="required">*</span> are required.</p>

    <?php //echo $form->errorSummary($model); ?>
    <div class="col-md-6">
        <?php echo $form->hiddenField($model, 'site_id', array('class' => 'span5')); ?>

        <?php //echo $form->textFieldRow($model,'create_user_id',array('class'=>'span5')); ?>
        <?php echo $form->dropDownListRow($model, 'corporate_sme', Product::getCorporateSME(), array('onchange' => 'getUtilities($(this).val())')); ?>


        <?php echo $form->dropDownListRow($model, 'utility', CHtml::listData(Product::getUtilites(), 'id', 'title')); ?>

        <?php echo $form->textFieldRow($model, 'meter_no', array('class' => 'span5', 'maxlength' => 255)); ?>

        <?php echo $form->dropDownListRow($model, 'profile_type', CustomerSiteMeter::getProfileType()); ?>

        <?php echo $form->dropDownListRow($model, 'meter_type', CustomerSiteMeter::getMeterType()); ?>

        <?php echo $form->textFieldRow($model, 'available_capability', array('class' => 'span5', 'maxlength' => 255)); ?>

        <?php echo $form->dropDownListRow($model, 'status', array('1' => 'Active', '0' => 'Inactive')); ?>

        <?php //echo $form->textFieldRow($model,'created',array('class'=>'span5'));  ?>
        <div class="form-actions">
            <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType' => 'submit',
                'type' => 'primary',
                'label' => $model->isNewRecord ? 'Create' : 'Save',
            ));
            ?>
        </div>
    </div>


    <?php $this->endWidget(); ?>
</div>

<script>
    getUtilities($("#CustomerSiteMeter_corporate_sme").val());
    function getUtilities(id) {
        var url = '<?php echo Yii::app()->createAbsoluteUrl("admin/product/getUtilities/id") ?>/' + id;
        $("#CustomerSiteMeter_utility").load(url);
        // gasParms();
    }
</script>