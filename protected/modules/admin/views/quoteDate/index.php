<?php
$this->breadcrumbs=array(
	'Quote Dates',
);

$this->menu=array(
	array('label'=>'Create QuoteDate','url'=>array('create')),
	array('label'=>'Manage QuoteDate','url'=>array('admin')),
);
?>

<h1>Quote Dates</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
