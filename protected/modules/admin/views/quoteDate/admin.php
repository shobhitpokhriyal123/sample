<section id="manage_products">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="card-title">
                        <strong>Manage Quote Dates</strong>
                    </div>

                    <?php
                    if (Yii::app()->user->isAdmin) {
                        echo CHtml::link('Create Quote Date', array('/admin/quoteDate/create'), array("class" => 'btn btn-success'));
                        //echo CHtml::link('Create Ticket', array('/admin/ticket/create'), array("class" => 'btn btn-success'));
                    }
                    ?>

                </div>
                <div class="card-body">
                    <?php
                    $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => 'quote-date-grid',
                        'dataProvider' => $model->search(),
                        'filter' => $model,
                        'columns' => array(
                            'id',
                            // 'supplier_id',
                            'supplier_id' => array(
                                'filter' => false, //CHtml::listData(Users::getAllSuppliers(), 'id', 'fullname'),
                                'type' => 'html',
                                'header' => 'Supplier',
                                'name' => 'supplier_id',
                                //    'value' => '$data->productName->user->username'
                                'value' => 'CHtml::image(Yii::app()->createAbsoluteUrl("admin/users/thumbnail/file/".$data->user->image))'
                            ),
                          //  'create_user_id',
                            'start_date',
                            'supplier_delays',
                            'end_date',
                            /*
                              'status',
                              'created',
                             */
                            array(
                                'class' => 'bootstrap.widgets.TbButtonColumn',
                            ),
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
