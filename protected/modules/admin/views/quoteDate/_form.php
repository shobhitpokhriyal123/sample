<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'quote-date-form',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
        ));
?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

<?php //echo $form->textFieldRow($model,'supplier_id',array('class'=>'span5'));  ?>
<?php echo $form->dropdownListRow($model, 'supplier_id', CHtml::listData(Users::model()->findAllByAttributes(array('role_id' => 2)), 'id', 'username'), array('empty' => '--- Select Supplier ---')); ?>


<?php //echo $form->textFieldRow($model, 'create_user_id', array('class' => 'span5')); ?>

<?php //echo $form->textFieldRow($model, 'start_date', array('class' => 'span5')); ?>

<?php echo $form->label($model, 'start_date'); ?>

<?php
$this->widget('zii.widgets.jui.CJuiDatePicker', array(
    'model' => $model,
    'attribute' => 'start_date',
    'options' => array(
        'dateFormat' => 'yy-mm-dd',
        'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
        'changeMonth' => true,
        'changeYear' => true,
    ),
));
?>
<div class="col-md-12">
    <div class="col-md-2">
        <?php echo $form->dropdownListRow($model, 'number_of', QuoteDate::getNumbers()); ?>
    </div>
    <div class="col-md-2">
        <?php echo $form->dropdownListRow($model, 'supplier_delays', QuoteDate::getTypes()); ?>
    </div>
    <div class="col-md-6"></div>
</div>
<?php //echo $form->textFieldRow($model, 'supplier_delays', array('class' => 'span5')); ?>

<?php echo $form->label($model, 'end_date'); ?>

<?php
$this->widget('zii.widgets.jui.CJuiDatePicker', array(
    'model' => $model,
    'attribute' => 'end_date',
    'options' => array(
        'dateFormat' => 'yy-mm-dd',
        'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
        'changeMonth' => true,
        'changeYear' => true,
    ),
));
?>
<?php //echo $form->textFieldRow($model, 'end_date', array('class' => 'span5')); ?>

<?php //echo $form->dropdownListRow($model, 'status', QuoteDate::getStatus()); ?>

<?php //echo $form->textFieldRow($model, 'created', array('class' => 'span5')); ?>
<hr>
<div class="form-actions">
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type' => 'primary',
        'label' => $model->isNewRecord ? 'Create' : 'Save',
    ));
    ?>
</div>

<?php $this->endWidget(); ?>
