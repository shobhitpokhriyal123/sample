<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contract_id')); ?>:</b>
	<?php echo CHtml::encode($data->contract_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_user_id')); ?>:</b>
	<?php echo CHtml::encode($data->create_user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contract_duration')); ?>:</b>
	<?php echo CHtml::encode($data->contract_duration); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('supplier_ids')); ?>:</b>
	<?php echo CHtml::encode($data->supplier_ids); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bespoke_duration')); ?>:</b>
	<?php echo CHtml::encode($data->bespoke_duration); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('commission_discount')); ?>:</b>
	<?php echo CHtml::encode($data->commission_discount); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('tender_return_date')); ?>:</b>
	<?php echo CHtml::encode($data->tender_return_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('comments')); ?>:</b>
	<?php echo CHtml::encode($data->comments); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />

	*/ ?>

</div>