<section id="manage_productss">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="card-title">
                        <strong>Create Tender</strong>
                    </div>

                    <?php
                   if (Yii::app()->user->isAdmin  || Yii::app()->user->isAgency ) {
                        echo CHtml::link('Manage Customers', array('/admin/agencyCustomer/admin'), array("class" => 'btn btn-success'));
                        //echo CHtml::link('Add Site', array('/admin/customerSite/create', 'cid' => $model->id), array("class" => 'btn btn-success'));
                        //echo CHtml::link('Meters', array('/admin/agencyCustomer/admin'), array("class" => 'btn btn-success'));
                        // echo CHtml::link('Add users', array('/admin/agencyCustomer/admin'), array("class" => 'btn btn-success'));
                    }
                    ?>

                </div>
                <div class="card-body">

                    <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
                </div>
            </div>
        </div>
    </div>
</section>
