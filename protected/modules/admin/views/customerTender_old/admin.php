<section id="manage_productss">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="card-title">
                        <strong>Manage Tenders</strong>
                    </div>

                    <?php
                   if (Yii::app()->user->isAdmin  || Yii::app()->user->isAgency ) {
                       echo CHtml::link('Manage Customers', array('/admin/agencyCustomer/admin'), array("class" => 'btn btn-success'));
                        echo CHtml::link('Manage Sites', array('/admin/customerSite/admin'), array("class" => 'btn btn-success'));
                        echo CHtml::link('Manage Meters', array('/admin/customerSiteMeter/admin'), array("class" => 'btn btn-success'));
                        echo CHtml::link('Manage Contracts', array('/admin/customerContract/admin'), array("class" => 'btn btn-success'));
                        echo CHtml::link('Manage Tenders', array('/admin/customerTender/admin'), array("class" => 'btn btn-success'));
                     }
                    ?>

                </div>
                <div class="card-body">

                    <?php
                    $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => 'customer-tender-grid',
                        'dataProvider' => $model->search(),
                        'filter' => $model,
                        'columns' => array(
                            // 'id',
//                            'customer',
//                            'utility',
//                            'start_date',
//                            'end_date',


                            'contract_id' => array(
                                'name' => 'contract_id',
                                'filter' => false,
                            ),
                            // 'create_user_id',
                            'contract_duration'=>array(
                                'name'=>'contract_duration',
                                'filter'=>false,
                            ),
                            'supplier_ids' => array(
                                'type' => 'raw',
                                'name' => 'supplier_ids',
                                'filter' => false,
                                'value' => 'CustomerTender::getSuppliers($data->supplier_ids)',
                            ),
                            'bespoke_duration'=>array(
                                'name'=>'bespoke_duration',
                                'filter'=>false,
                            ),
                            'commission_discount'=>array(
                                'name'=>'commission_discount',
                                'filter'=>false,
                            ),
                            'tender_return_date'=>array(
                                'name'=>'tender_return_date',
                                'filter'=>false,
                            ),
                            'comments'=>array(
                                'name'=>'comments',
                                'filter'=> false,
                            ),
                            /*  'created',
                             */
                            array(
                                'class' => 'bootstrap.widgets.TbButtonColumn',
                            ),
                        ),
                    ));
                    ?>

                </div>
            </div>
        </div>
    </div>
</section>
