<section id="manage_productss">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="card-title">
                        <strong>View Tender</strong>
                    </div>

                    <?php
                    
                      if (Yii::app()->user->isAdmin  || Yii::app()->user->isAgency ) {
                        echo CHtml::link('Manage Customers', array('/admin/agencyCustomer/admin'), array("class" => 'btn btn-success'));
                      //  echo CHtml::link('Add Site', array('/admin/customerSite/create', 'cid' => $model->id), array("class" => 'btn btn-success'));
                        echo CHtml::link('Manage Tenders', array('/admin/customerTender/admin','contract_id'=>$model->contract_id), array("class" => 'btn btn-success'));
                        // echo CHtml::link('Add users', array('/admin/agencyCustomer/admin'), array("class" => 'btn btn-success'));
                    }
                    ?>

                </div>
                <div class="card-body">

                    <?php
                    $this->widget('bootstrap.widgets.TbDetailView', array(
                        'data' => $model,
                        'attributes' => array(
                            'id',
                            'contract_id',
                         //   'create_user_id',
                            'contract_duration',
                            'supplier_ids'=>array(
                                'name'=>'supplier_ids',
                                'type'=>'raw',
                                'filter'=>false,
                                'value'=> CustomerTender::getSuppliers($model->supplier_ids),
                            ),
                            'bespoke_duration',
                            'commission_discount',
                            'tender_return_date',
                            'comments',
                            'created',
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>

