<?php
$this->breadcrumbs=array(
	'Customer Tenders',
);

$this->menu=array(
	array('label'=>'Create CustomerTender','url'=>array('create')),
	array('label'=>'Manage CustomerTender','url'=>array('admin')),
);
?>

<h1>Customer Tenders</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
