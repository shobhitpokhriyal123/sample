<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'customer-tender-form',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        )
);
?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php //echo $form->errorSummary($model); ?>

<?php //echo $form->textFieldRow($model, 'contract_id', array('class' => 'span5')); ?>

<?php //echo $form->textFieldRow($model, 'create_user_id', array('class' => 'span5')); ?>

<?php echo $form->dropDownListRow($model, 'contract_duration', CustomerTender::getContractDuration()); ?>

<?php
if($model->supplier_ids)
    $model->supplier_ids = explode (",", $model->supplier_ids);

echo $form->checkBoxListRow($model, 'supplier_ids', CHtml::listData(Users::model()->findAllByAttributes(array('role_id' => 2)), 'id', 'fullname')); ?>

<?php echo $form->textFieldRow($model, 'bespoke_duration', array('class' => 'span5')); ?>

<?php echo $form->textFieldRow($model, 'commission_discount', array('class' => 'span5')); ?>

<?php //echo $form->textFieldRow($model, 'tender_return_date', array('class' => 'span5')); ?>

<?php echo $form->label($model, 'tender_return_date'); ?>
<?php
$this->widget('zii.widgets.jui.CJuiDatePicker', array(
    'model' => $model,
    'attribute' => 'tender_return_date',
    'options' => array(
        'dateFormat' => 'yy-mm-dd',
        'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
        'changeMonth' => true,
        'changeYear' => true,
    ),
));
?>
<?php echo $form->error($model, 'tender_return_date'); ?>
<hr>
<?php echo $form->textAreaRow($model, 'comments', array('rows' => 6, 'cols' => 50, 'class' => 'span8')); ?>

<?php //echo $form->textFieldRow($model, 'created', array('class' => 'span5')); ?>

<div class="form-actions">
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type' => 'primary',
        'label' => $model->isNewRecord ? 'Create' : 'Save',
    ));
    ?>
</div>

<?php $this->endWidget(); ?>
