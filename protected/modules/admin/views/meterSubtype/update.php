<?php
$this->breadcrumbs=array(
	'Meter Subtypes'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List MeterSubtype','url'=>array('index')),
	array('label'=>'Create MeterSubtype','url'=>array('create')),
	array('label'=>'View MeterSubtype','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage MeterSubtype','url'=>array('admin')),
);
?>

<h1>Update MeterSubtype <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model,'supplier'=>$supplier)); ?>