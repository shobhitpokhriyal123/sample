<?php
$this->breadcrumbs=array(
	'Meter Subtypes'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List MeterSubtype','url'=>array('index')),
	array('label'=>'Create MeterSubtype','url'=>array('create')),
	array('label'=>'Update MeterSubtype','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete MeterSubtype','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MeterSubtype','url'=>array('admin')),
);
?>

<h1>View MeterSubtype #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'supplier_id',
		'title',
		'meter_code',
		'meter_type',
		'parent',
		'created',
	),
)); ?>
