<?php
$this->breadcrumbs=array(
	'Meter Subtypes',
);

$this->menu=array(
	array('label'=>'Create MeterSubtype','url'=>array('create')),
	array('label'=>'Manage MeterSubtype','url'=>array('admin')),
);
?>

<h1>Meter Subtypes</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
