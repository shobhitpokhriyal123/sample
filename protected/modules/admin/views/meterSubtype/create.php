<section id="manage_supliers">
    <div class="container">
        <div class="row">

            <div class="card">
                <div class="card-header">
                    <div class="card-title text-primary">
                        <strong>Create Meter Type : <?php echo $supplier->fullname ?></strong>
                    </div>
                    <?php
//                    if (Yii::app()->user->isAdmin) {
//                        echo CHtml::link('New Supplier', array('/admin/users/create'), array("class" => 'btn btn-success'));
//                        echo CHtml::link('Map CSV', array('/admin/product/csvMapping'), array("class" => 'btn btn-default'));
//                        echo CHtml::link('Product Utilities', array('/admin/users/types'), array("class" => 'btn btn-success'));
//                    }
                    ?>
                </div>
                <div class="card-body ">

                    <?php echo $this->renderPartial('_form', array('model' => $model,'supplier'=>$supplier)); ?>
                </div>
            </div>
        </div>
    </div>
</section>