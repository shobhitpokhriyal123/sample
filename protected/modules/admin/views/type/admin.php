<section id="manage_products">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="card-title">
                        <strong>Manage Utilities</strong>
                    </div>

                    <?php
                    if (Yii::app()->user->isAdmin) {
                        echo CHtml::link('Create types', array('/admin/type/create'), array("class" => 'btn btn-success'));
                    }
                    ?>

                </div>
                <div class="card-body">

                    <?php
                    $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => 'type-grid',
                        'dataProvider' => $model->search(),
                        'filter' => $model,
                        'columns' => array(
                            'id',
                            'title',
                            // 'description',
//                'logo' => array(
//                    'filter' => false,
//                    'type' => 'html',
//                    'name' => 'logo',
//                    'value' => 'CHtml::image(Yii::app()->createAbsoluteUrl("/admin/type/thumbnail/file/".$data->logo))'
//                ),
                            'status' => array(
                                'filter' => $model->getStatus(),
                                'name' => 'status',
                                'value' => 'Region::getStatus($data->status)'
                            ),
                            'parrent' => array(
                                'name' => 'parrent',
                                'value' => 'Type::getUtility($data->parrent)'
                            ),
                            /*
                              'created',
                             */
                            array(
                                'class' => 'bootstrap.widgets.TbButtonColumn',
                                'template' => '{view},{update}'
                            ),
                        ),
                    ));
                    ?>

                </div>
            </div>
        </div>
    </div>
</section>