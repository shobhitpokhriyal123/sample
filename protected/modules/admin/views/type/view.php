<section id="manage_products">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="info">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="card-title">
                        <strong>Manage Utilities</strong>
                    </div>

                    <?php
                    if (Yii::app()->user->isAdmin) {
                        echo CHtml::link('Manage Utilities', array('/admin/type/admin'), array("class" => 'btn btn-success'));
                    }
                    ?>

                </div>
                <div class="card-body">

                    <?php
                    $this->widget('bootstrap.widgets.TbDetailView', array(
                        'data' => $model,
                        'attributes' => array(
                            'id',
                            'title',
                            'description',
                            'logo' => array(
                                'type' => 'html',
                                'name' => 'logo',
                                'value' => CHtml::image(Yii::app()->createAbsoluteUrl("/admin/type/thumbnail/file/" . $model->logo))
                            ),
                          'status' => array(
                                        'name' => 'status',
                                        'value' => $model->getStatus($model->status),
                                    ),
                           
                            'parrent' => array(
                                'name' => 'parrent',
                                'value' => Type::getUtility($model->parrent)
                            ),
                            'created',
                        ),
                    ));
                    ?>

                </div>
            </div>
        </div></div>
</section>