<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'type-form',
    'enableAjaxValidation' => false,
    'enableClientValidation' => false,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->textFieldRow($model, 'title', array('class' => 'span5', 'maxlength' => 255)); ?>

<?php echo $form->textAreaRow($model, 'description', array('rows' => 6, 'cols' => 50, 'class' => 'span8')); ?>

<?php echo $form->fileFieldRow($model, 'logo'); ?>

<?php // echo $form->textFieldRow($model, 'status', array('class' => 'span5')); ?>

<?php //echo $form->textFieldRow($model, 'parrent', array('class' => 'span5')); ?>
<?php echo $form->dropdownListRow($model, 'parrent', CHtml::listData(Type::getTypes(), 'id', 'title'), array('class' => 'span5', 'empty' => '(Select Type)')); ?>
<?php //echo $form->textFieldRow($model, 'created', array('class' => 'span5')); ?>

<div class="form-actions">
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type' => 'primary',
        'label' => $model->isNewRecord ? 'Create' : 'Save',
    ));
    ?>
</div>

<?php $this->endWidget(); ?>
