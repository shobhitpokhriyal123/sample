<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_user_id')); ?>:</b>
	<?php echo CHtml::encode($data->create_user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lead_id')); ?>:</b>
	<?php echo CHtml::encode($data->lead_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('current_supplier')); ?>:</b>
	<?php echo CHtml::encode($data->current_supplier); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meter_name')); ?>:</b>
	<?php echo CHtml::encode($data->meter_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pc')); ?>:</b>
	<?php echo CHtml::encode($data->pc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mtc')); ?>:</b>
	<?php echo CHtml::encode($data->mtc); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('llf')); ?>:</b>
	<?php echo CHtml::encode($data->llf); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pes')); ?>:</b>
	<?php echo CHtml::encode($data->pes); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mpan')); ?>:</b>
	<?php echo CHtml::encode($data->mpan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('total_eac')); ?>:</b>
	<?php echo CHtml::encode($data->total_eac); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contract_end_date')); ?>:</b>
	<?php echo CHtml::encode($data->contract_end_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('msn')); ?>:</b>
	<?php echo CHtml::encode($data->msn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kva')); ?>:</b>
	<?php echo CHtml::encode($data->kva); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('voltage')); ?>:</b>
	<?php echo CHtml::encode($data->voltage); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ct_wc')); ?>:</b>
	<?php echo CHtml::encode($data->ct_wc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mop')); ?>:</b>
	<?php echo CHtml::encode($data->mop); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meter_status')); ?>:</b>
	<?php echo CHtml::encode($data->meter_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meter_pricing')); ?>:</b>
	<?php echo CHtml::encode($data->meter_pricing); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />

	*/ ?>

</div>