<?php
$this->breadcrumbs=array(
	'Lead Elc Meters',
);

$this->menu=array(
	array('label'=>'Create LeadElcMeter','url'=>array('create')),
	array('label'=>'Manage LeadElcMeter','url'=>array('admin')),
);
?>

<h1>Lead Elc Meters</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
