<?php
$this->breadcrumbs=array(
	'Lead Elc Meters'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List LeadElcMeter','url'=>array('index')),
	array('label'=>'Create LeadElcMeter','url'=>array('create')),
	array('label'=>'View LeadElcMeter','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage LeadElcMeter','url'=>array('admin')),
);
?>

<h1>Update LeadElcMeter <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>