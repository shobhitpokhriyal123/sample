<?php
$this->breadcrumbs=array(
	'Lead Elc Meters'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List LeadElcMeter','url'=>array('index')),
	array('label'=>'Create LeadElcMeter','url'=>array('create')),
	array('label'=>'Update LeadElcMeter','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete LeadElcMeter','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage LeadElcMeter','url'=>array('admin')),
);
?>

<h1>View LeadElcMeter #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'create_user_id',
		'lead_id',
		'current_supplier',
		'meter_name',
		'pc',
		'mtc',
		'llf',
		'pes',
		'mpan',
		'total_eac',
		'contract_end_date',
		'msn',
		'kva',
		'voltage',
		'ct_wc',
		'mop',
		'meter_status',
		'meter_pricing',
		'created',
	),
)); ?>
