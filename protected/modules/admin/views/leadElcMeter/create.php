<?php
$this->breadcrumbs=array(
	'Lead Elc Meters'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List LeadElcMeter','url'=>array('index')),
	array('label'=>'Manage LeadElcMeter','url'=>array('admin')),
);
?>

<h1>Create LeadElcMeter</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>