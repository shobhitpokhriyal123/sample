<form id="elec_meter_frm" name="elec_meter_frm" method="post">
    <?php
    $this->widget('bootstrap.widgets.TbGridView', array(
        'id' => 'lead-elc-meter-grid',
        'dataProvider' => $model->search(),
        'afterAjaxUpdate' => 'function(id,options){ paginationControls(); }',
        // 'filter' => $model,
        'columns' => array(
            'site_id' => array(
                'filter' => false, //CHtml::listData(Users::getAllSuppliers(), 'id', 'fullname'), 'type' => 'html',
                'header' => 'Site',
                'name' => 'site_id',
                'value' => '$data->site->site_name',
                'footer' => LeadElcMeter::getSiteDropdown()
            ),
            'current_supplier' => array(
                'type' => 'raw',
                'filter' => false, //CHtml::listData(Users::getAllSuppliers(), 'id', 'fullname'), 'type' => 'html',
                'name' => 'current_supplier',
                'value' => 'CHtml::image(Yii::app()->createAbsoluteUrl("admin/users/thumbnail/file/".$data->supplier->image))',
                'footer' => LeadElcMeter::getSupplierDropdown()
            ),
            'corporate_sme' => array(
                'type' => 'raw',
                'filter' => false, //CHtml::listData(Users::getAllSuppliers(), 'id', 'fullname'), 'type' => 'html',
                'name' => 'corporate_sme',
                'value' => 'Product::getCorporateSME($data->corporate_sme)',
                'footer' => LeadElcMeter::getCorporateDropdown()
            ),
            'utility' => array(
                'type' => 'raw',
                'filter' => false, //CHtml::listData(Users::getAllSuppliers(), 'id', 'fullname'), 'type' => 'html',
                'name' => 'utility',
                'value' => 'Product::getUtilites($data->utility)',
                'footer' => LeadElcMeter::getUtilityDropdown()
            ),
            'meter_name' => array(
                'filter' => false,
                'name' => 'meter_name',
                'footer' => '<input id="LeadElcMeter_meter_name_" class="span5 form-control" maxlength="255" name="LeadElcMeter[meter_name]" type="text">'
            ),
            'mpan' => array(
                'filter' => false,
                'name' => 'mpan',
                'footer' => '<input id="LeadElcMeter_mpan_" class="span5 form-control" maxlength="255" name="LeadElcMeter[mpan]" type="text">'
            ),
            'total_eac' => array(
                'filter' => false,
                'name' => 'total_eac',
                'footer' => '<input id="LeadElcMeter_total_eac_" class="span5 form-control" maxlength="255" name="LeadElcMeter[total_eac]" type="text">'
            ),
            'contract_end_date' => array(
                'filter' => false,
                'name' => 'contract_end_date',
                'footer' => '<input id="LeadElcMeter_contract_end_date_" class="form-control " name="LeadElcMeter[contract_end_date]" type="text">'
            ),
            'meter_status' => array(
                'filter' => false,
                'name' => 'meter_status',
                'value' => 'MeterStatus::getStatus($data->meter_status)',
                'footer' => LeadElcMeter::getStatusDropdown()
            ),
//            'meter_pricing' => array(
//                'filter' => false,
//                'name' => 'meter_pricing',
//                'value' => '($data->meter_pricing == 0) ?  "No" : "Yes"',
//                
//            ), /**/
            //  'created',
            array(
                'class' => 'bootstrap.widgets.TbButtonColumn',
                'template' => '{update}{delete}',
                'header' => 'Actions',
                'footer' => '<button class="btn btn-success" id="elec_meter_submit">Add</button>',
                'buttons' => array(
                    'update' => array(
                        'label' => '<button class="btn btn-primary">Update</button>',
                        'url' => 'Yii::app()->createAbsoluteUrl("admin/leadElcMeter/meterupdate", array("id"=>$data->id))',
                        'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Update'),
                        'imageUrl' => false,
                        'click' => "function(e){
                               e.preventDefault();
                               var url = $(this).attr('href');
                               $.ajax({
                                 type: 'POST',
                                 url: url,
                                 success: function(res){
                                 res = $.parseJSON(res);
                              
                                for (var property in res) {
                                        if (res.hasOwnProperty(property)) { 
                                        
                                         $('#LeadElcMeter_'+property).val(res[property]);
                                    }
                                }
                                    var current = $('#lead-elc-meter-form').attr('action');
                                    $('#lead-elc-meter-form').attr('action',current+'&id='+res.id);
                                 },
                                });
                                
                           
                            }",
                    ),
                ),
            ),
        ),
    ));
    ?>

</form>

<script>
    
    $(document).ready(function (e) {
        $("#elec_meter_submit").click(function (e) {
            e.preventDefault();
            var data = $("#elec_meter_frm").serialize();
            console.log(data);
            var business_url = '<?php echo Yii::app()->createAbsoluteUrl("admin/leadElcMeter/create") ?>';
            $.ajax({
                url: business_url,
                data: data,
                type: "POST",
                success: function (res) {
                    $.fn.yiiGridView.update("lead-elc-meter-grid");
                }
            });
        });
    });

</script>