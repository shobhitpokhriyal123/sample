<?php
$this->breadcrumbs=array(
	'Meter Bases',
);

$this->menu=array(
	array('label'=>'Create MeterBase','url'=>array('create')),
	array('label'=>'Manage MeterBase','url'=>array('admin')),
);
?>

<h1>Meter Bases</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
