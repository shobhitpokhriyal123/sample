<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 style="color:gary;"><span class="glyphicon glyphicon-bell"></span> Agency Details</h4>
    </div>
    <div class="modal-bodyw">

        <div  class="clearfix"></div>
        <hr>

        <div class="col-md-12">
            <div  class="col-md-4">
                <img src="<?php echo Yii::app()->createAbsoluteUrl('admin/users/thumbnail/file/' . $user->image); ?>" />
            </div>
            <div  class="col-md-4">
                Username:  <b><?php echo $user->username; ?></b>
            </div>
            <div  class="col-md-4">
                Email:  <b><?php echo $user->email; ?></b>
            </div>
            <hr>
        </div>

        <div  class="clearfix"></div>
        <hr>

        <div class="col-md-12">
            <?php
            if ($user->agencies) {
                foreach ($user->agencies as $agency) {
                    ?>
                    <h3>Agency Details</h3>

                    <div  class="col-md-4">Logo: <img src="<?php echo Yii::app()->createAbsoluteUrl('admin/users/thumbnail/file/' . $agency->agency_logo); ?>" /></div> 
                    <div  class="col-md-4"> File: <img src="<?php echo Yii::app()->createAbsoluteUrl('admin/users/thumbnail/file/' . $agency->agency_file); ?>" /> </div> 
                    <div  class="col-md-4"> Letter Authority : <img src="<?php echo Yii::app()->createAbsoluteUrl('admin/users/thumbnail/file/' . $agency->letter_authority); ?>" /></div> 

                      <?php echo $form->fileFieldRow($agency, 'csv_file', array('disabled' => 'disabled')); ?>
                    <a class="btn btn-primary" href="<?php echo Yii::app()->createAbsoluteUrl('admin/product/download/file/' . $model->csv_file) ?>"><?php echo $model->csv_file ?><i class="icon-download"></i></a>
                    <div  class="col-md-4">
                        <b>Current users:</b> <?php echo $agency->current_users ?>
                    </div>
                    <div  class="col-md-4">
                        <b>Max users:</b> <?php echo $agency->max_users ?>
                    </div>
                    <div  class="col-md-4">
                        <b>Tranding Type:</b> <?php echo Agency::getTrendingType($agency->trending_type) ?>  
                    </div>
                    <div  class="col-md-4">
                        <b>Principal :</b> <?php echo $agency->principal; ?>
                    </div>
                    <div  class="col-md-4">
                        <b>Current users:</b> <?php echo $agency->vat_no; ?>
                    </div>
                    <div  class="col-md-4">
                        <b>Agent Split:</b> <?php echo $agency->agent_split . '%'; ?>
                    </div>
                    <div  class="col-md-4">
                        <b>OD Split:</b> <?php echo $agency->od_split . '%'; ?>
                    </div>





                    <div  class="clearfix"></div>
                    <hr>
                    <?php
                    $agency_address = AgencyAddress::model()->findByAttributes(array('agency_id' => $agency->id));
                    if ($agency_address) {
                        ?>
                        <h3>Agency Address</h3>
                        <div  class="col-md-4">
                            <b>Address No:</b> <?php echo ($agency_address->address_number) ? $agency_address->address_number : 'N/A'; ?>
                        </div>
                        <div  class="col-md-4">
                            <b>Town :</b> <?php echo($agency_address->town) ? $agency_address->town : 'N/A'; ?>
                        </div>
                        <div  class="col-md-4">
                            <b>Address Number:</b> <?php echo ($agency_address->address1) ? $agency_address->address1 : 'N/A'; ?>
                        </div>
                        <div  class="col-md-4">
                            <b>Town:</b> <?php echo ($agency_address->country) ? $agency_address->country : 'N/A'; ?>
                        </div>
                        <div  class="col-md-4">
                            <b>Telephone:</b> <?php echo ($agency_address->telephone) ? $agency_address->telephone : 'N/A'; ?>
                        </div>
                        <div  class="col-md-4">
                            <b>Website:</b> <?php echo ($agency_address->website) ? $agency_address->website : 'N/A'; ?>
                        </div>
                        <div  class="col-md-4">
                            <b>Email:</b> <?php echo ($agency_address->email) ? $agency_address->email : 'N/A'; ?>
                        </div>
                        <div  class="col-md-4">
                            <b>Fax:</b> <?php echo ($agency_address->fax) ? $agency_address->fax : 'N/A'; ?>
                        </div>
                        <?php
                    } else {
                        echo 'Address not found...!';
                    }
                }
            } else {
                echo 'No agency found..!';
            }
            ?>
        </div>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal">
            <span class="glyphicon glyphicon-remove"></span> Close
        </button>

    </div>
</div>

<style>
    .modal-dialog {
        margin: 30px auto;
        width: 855px;
    }
    .col-md-4{
        padding: 6px;
    }
</style>