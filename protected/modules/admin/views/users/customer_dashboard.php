<section id="view_suplier">
    <div class="container">
        <div class="row">

            <div class="card">
                <div class="card-header">
                    <div class="card-title text-primary">
                        <strong>Hello <?php echo $model->fullname; ?>, Welcome to Utility Energy..!</strong>
                    </div>
                    <?php
                    if (Yii::app()->user->isCustomer) {
                        //  echo CHtml::link('Manage Quote', array('/admin/leadQuote/admin'), array("class" => 'btn btn-success'));
                        //.  echo CHtml::link('Manage Site', array('/admin/customerSite/admin','cid'=>Yii::app()->user->id), array("class" => 'btn btn-success'));
                        //echo CHtml::link('Manage Quote', array('/admin/customerQuote/quote'), array("class" => 'btn btn-success'));
                    }
                    ?>
                    <?php //echo CHtml::link('Update', array('users/update/id/' . $model->id), array("class" => 'btn btn-success')); ?>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 text-left">
                            <div class="col-sm-2">
                                <img src="<?php echo Yii::app()->createAbsoluteUrl('users/thumbnail/file/' . $model->image); ?>" class="img-thumbnail" width="100%" style="margin-bottom:20px;">
                            </div>
                            <div class="col-sm-4 text-left">
                                <h4>My Profile</h4>
                                <?php
                                //  echo '<pre>'; print_r($model->broker->broker); die;
                                $this->widget('bootstrap.widgets.TbDetailView', array(
                                    'data' => $model,
                                    'attributes' => array(
                                        //'id',
                                        'fullname',
                                        'email',
                                        'username',
                                        'role_id' => array(
                                            'name' => 'role_id',
                                            'value' => Role::getRoles($model->role_id),
                                        ),
                                    // 'created',
                                    ),
                                ));
//                                echo '<pre>'; print_r($model->broker->broker); die;
                                $model1 = ($model->broker) ? $model->broker->broker : "";
                                
                                ?>
                            </div>
                            <?php if($model1){ ?>
                            <div class="col-sm-2">
                                <img src="<?php echo Yii::app()->createAbsoluteUrl('users/thumbnail/file/' . $model->image); ?>" class="img-thumbnail" width="100%" style="margin-bottom:20px;">
                            </div>
                            <div class="col-sm-4 text-left">
                                <h4>Broker Account Manager</h4>
                                <?php
                                $this->widget('bootstrap.widgets.TbDetailView', array(
                                    'data' => $model1,
                                    'attributes' => array(
                                        //'id',
                                        'fullname',
                                        'email',
                                        'username',
                                        'role_id' => array(
                                            'name' => 'role_id',
                                            'value' => Role::getRoles($model1->role_id),
                                        ),
                                    // 'created',
                                    ),
                                ));
                                ?>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="row">
                        <hr>
                        <h4>Your Upcomming contracts</h4>
                        <div class="col-sm-12 text-left">
                            <div class="col-sm-6 text-left border">
                                <hr>
                                <h2>My Contracts </h2>

                                <a class="btn btn-success" href="" id="electricity_contracts" >View Your Contracts  (N/A)</a>
                            </div>
                            <!--                            <div class="col-sm-6 text-left border">
                                                            <hr>
                                                            <h2>Gas </h2>
                                                            <a class="btn btn-success"  href="" id="gas_contracts" >View Your Contracts (N/A)</a>
                                                        </div>-->

                        </div>
                        <div class="" id="load_data">


                        </div>
                    </div>


                    <hr>
                </div>
            </div>

        </div>
    </div>
</section>

<script>
    $(document).ready(function () {
        $("#electricity_contracts").click(function (e) {
            e.preventDefault();
            var url = '<?php echo Yii::app()->createAbsoluteUrl("admin/leadQuote/customerAdmin") ?>';
            $("#load_data").load(url);
            //$("#contract").html('Manage Contract : Electricity');
        });

    });
</script>


