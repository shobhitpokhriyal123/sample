<section id="manage_supliers">
    <div class="container-fluid">
        <div class="row">

            <div class="card">
                <div class="card-header">
                    <div class="card-title text-primary">
                        <strong>Manage Agencies</strong>
                    </div>
                    <?php
                    if (Yii::app()->user->isAdmin) {
                        echo CHtml::link('New Agency', array('/admin/users/newAgency'), array("class" => 'btn btn-success'));
                        // echo CHtml::link('Map CSV', array('/admin/product/csvMapping'), array("class" => 'btn btn-default'));
                        //  echo CHtml::link('Product Utilities', array('/admin/users/types'), array("class" => 'btn btn-success'));
                    }
                    ?>
                </div>
                <div class="card-body .table-responsive">

                    <?php
                    //print_r($model); die;
                    $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => 'agency-grid',
                        'dataProvider' => $model->search(),
                        'afterAjaxUpdate' => 'function(id,options){ paginationControls(); }',
                        'filter' => $model,
                        'columns' => array(
                            //  'id',
                            // 
                            //  'agency_logo',
                            //  'agency_file',
                            'agency_id' => array(
                                'name' => 'agency_id',
                              //  'filter' => false,
                            ),
                            'user_id' => array(
                                'name' => 'user_id',
                                'header' => 'Agency Name',
                                'filter' => $model->getAgency(),
                                'value' => '$data->user->username '
                            ),
//                             'password_plain' => array(
//                                'name' => 'password_plain',
//                                'header' => 'Password',
//                                'filter' => false,
//                                 'value'=>'$data->user->password_plain'
//                            ),
                            //     'company_pic',
                            //   'letter_authority',
                            // 'current_users',
                            // 'max_users',
//                            'trending_type'=> array(
//                                'name' => 'trending_type',
//                                'filter' => false,
//                                'value' => '$data->trending_type'
//                            ),
                            'trending_type' => array(
                                'filter' => false,
                                'name' => 'trending_type',
                                'id' => 'trending_type',
                                'value' => 'Agency::getTrendingType($data->trending_type)'
                            ),
                            'telephone' => array(
                                'filter' => false,
                                'name' => 'telephone',
                                'header' => 'Mobile',
                                'value' => '($data->agencyAddress1) ? $data->agencyAddress1->telephone : "N/A"'
                            ),
                            'principal',
                            'vat_no',
                            'date_created',
                            'date_modified',
                            //    'self_billing',
                            'self_billing' => array(
                                'filter' => false,
                                'name' => 'self_billing',
                                'id' => 'self_billing',
                                'value' => '($data->self_billing == 1) ? "Yes" : "No"'
                            ),
//                            
                            'od_split',
                            /* */
                            array(
                                'class' => 'bootstrap.widgets.TbButtonColumn',
                                'template' => '{view}{update}',
                                'buttons' => array(
                                    'view' => array(
                                        // 'label' => '<i class="fa fa-icon-pencil"></i>',
                                        'url' => 'Yii::app()->createUrl("admin/users/view", array("id"=>$data->user->id))',
                                        'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'View'),
                                    ),
                                    'update' => array(
                                        // 'label' => '<i class="fa fa-icon-pencil"></i>',
                                        'url' => 'Yii::app()->createUrl("admin/users/updateAgency", array("id"=>$data->user->id))',
                                        'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Update'),
                                    ),
                                ),
                            ),
                        ),
                    ));
                    ?>
                </div>
            </div>    
        </div>
    </div>
</section>
<!-- Modal -->
<div class="modal fade" id="myModal3434" role="dialog">
    <div class="modal-dialog" id="response123">

        <!-- Modal content-->

    </div>
</div>