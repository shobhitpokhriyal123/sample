<style>
    label {
        margin-top: 10px;
    }
</style>

<section id="create_suplier">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <strong>Create Supplier</strong>
                    </div>
                    <?php
                    if (Yii::app()->user->isAdmin) {
                        echo CHtml::link('Manage Suppliers', array('/admin/users/admin'), array("class" => 'btn btn-success'));
                    }
                    ?>
                </div>
                <div class="card-body">
                    <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $('label[for="Users_image"]').append(' Supplier Image');
    $('.btn').each(function () {
        if ($(this).text() == 'Create') {
            $(this).addClass('longBtn').css('margin-top', '10px');
        }
    });
</script>
