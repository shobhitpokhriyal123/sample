<section id="manage_supliers">
    <div class="container">
        <div class="row">

            <div class="card">
                <div class="card-header">
                    <div class="card-title text-primary">
                        <strong>Manage Agents</strong>
                    </div>
                    <?php
                    if (Yii::app()->user->isAdmin) {
                        echo CHtml::link('New User', array('/admin/users/newUser'), array("class" => 'btn btn-success'));
                    }
                    ?>
                </div>
                <div class="card-body .table-responsive">
                    <?php
                    $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => 'users-grid',
                        'dataProvider' => $model->search($role),
                        'afterAjaxUpdate' => 'function(id,options){ paginationControls(); }',
                        'filter' => $model,
                        'columns' => array(
                            'image' => array(
                                'filter' => false,
                                'type' => 'html',
                                'name' => 'image',
                                'value' => 'CHtml::image(Yii::app()->createAbsoluteUrl("/admin/users/thumbnail/file/".$data->image))',
                                'htmlOptions' => array('class' => 'table-images')
                            ),
                            'email' => array(
                                'filter' => false,
                                'name' => 'email'
                            ),
                            'agency_id' => array(
                                'filter' => false,
                                'name' => 'agency_id',
                                'header' => 'Agency',
                                'value' => '($data->agency_id) ? Users::getAgencyName($data->agency_id) : "N/A"'
                            ),
//                            'password_plain' => array(
//                                'name' => 'password_plain',
//                                'header' => 'Password',
//                                'filter' => false,
//                                'value' => '$data->password_plain'
//                            ),
                            'username' => array(
                                'filter' => false,
                                'name' => 'username'
                            ),
                            'fullname' => array(
                                'filter' => false,
                                'name' => 'fullname'
                            ),
                            'role_id' => array(
                                'filter' => false, //CHtml::listData(Role::model()->getRoles(), 'id', 'title'),
                                'name' => 'role_id',
                                'id' => 'role_id',
                                'value' => '$data->rolename->title'
                            ),
                            /*   'agency_id' => array(
                              'filter' => false, //CHtml::listData(Role::model()->getRoles(), 'id', 'title'),
                              'header' => 'Agency',
                              'name' => 'agency_id',
                              'id' => 'agency_id',
                              'value' => '$data->agency->user->username'
                              ),
                             */
                            'active' => array(
                                'filter' => false, //'filter' => $model->getStatus(),
                                'name' => 'active',
                                'id' => 'active',
                                'value' => '$data->getStatus($data->active)'
                            ),
                            //  'password',
                            //'role_id'=>$model->role,
                            /*
                              'image',
                              'role_id',
                              ,
                              'created',
                             */
                            array(
                                'class' => 'bootstrap.widgets.TbButtonColumn',
                                'template' => '{view}{update}',
                                'buttons' => array(
                                    'view' => array(
                                        // 'label' => '<i class="fa fa-icon-pencil"></i>',
                                        'url' => 'Yii::app()->createUrl("admin/users/view", array("id"=>$data->id))',
                                        'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'View'),
                                    ),
                                    'update' => array(
                                        //  'label' => '<i class="fa fa-pencil"></i>',
                                        'url' => 'Yii::app()->createUrl("admin/users/updateAgent", array("id"=>$data->id))',
                                        'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Update'),
                                    ),
                                ),
                            ),
                        ),
                    ));
                    ?>
                </div>
            </div>    
        </div>
    </div>
</section>
