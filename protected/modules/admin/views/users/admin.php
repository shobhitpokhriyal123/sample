<section id="manage_supliers">
    <div class="container">
        <div class="row">

            <div class="card">
                <div class="card-header">
                    <div class="card-title text-primary">
                        <strong>Manage Suppliers </strong>
                    </div>
                    <?php
                    if (Yii::app()->user->isAdmin) {
                        echo CHtml::link('New Supplier', array('/admin/users/create'), array("class" => 'btn btn-success'));
                        echo CHtml::link('Map CSV', array('/admin/product/csvMapping'), array("class" => 'btn btn-default'));
                        echo CHtml::link('Product Utilities', array('/admin/users/types'), array("class" => 'btn btn-success'));
                    }
                    ?>
                </div>
                <div class="card-body .table-responsive">
                    <?php
                    $this->widget('bootstrap.widgets.TbGridView', array(
                        'id' => 'users-grid',
                        'dataProvider' => $model->search($role),
                        'afterAjaxUpdate' => 'function(id,options){ paginationControls(); }',
                        'filter' => $model,
                        'columns' => array(
                            //  'fullname',
                            'image' => array(
                                'filter' => false,
                                'type' => 'html',
                                'name' => 'image',
                                'value' => '$data->username', //'CHtml::image(Yii::app()->createAbsoluteUrl("/admin/users/thumbnail/file/".$data->image))',
                                'htmlOptions' => array('class' => 'table-images')
                            ),
                            'email',
                            // 'mobile',
                            'fullname',
                            'role_id' => array(
                                'filter' => CHtml::listData(Role::model()->getRoles(), 'id', 'title'),
                                'name' => 'role_id',
                                'id' => 'role_id',
                                'value' => '$data->rolename->title'
                            ),
                            'active' => array(
                                'filter' => $model->getStatus(),
                                'name' => 'active',
                                'id' => 'active',
                                'value' => '$data->getStatus($data->active)'
                            ),
                            //  'password',
                            //'role_id'=>$model->role,
                            /*
                              'image',
                              'role_id',
                              ,
                              'created',
                             */
                            array(
                                'class' => 'bootstrap.widgets.TbButtonColumn',
                                'template' => '{view}{update}{delete}{map_meter}',
                                'buttons' => array(
                                    'map_meter' => array(
                                        'label' => '<button class="btn btn-primary">Map Meter</button>',
                                        'url' => 'Yii::app()->createAbsoluteUrl("admin/meterType/admin", array("sid"=>$data->id))',
                                        'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Meter'),
                                        'imageUrl' => false,
                                    ),
                                ),
                            ),
                        ),
                    ));
                    ?>
                </div>
            </div>    
        </div>
    </div>
</section>
