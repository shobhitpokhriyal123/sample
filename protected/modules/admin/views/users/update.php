<section id="update_suppliers">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <strong>Update Supplier <?php echo $model->username; ?></strong>
                    </div>
                    <?php
                        if (Yii::app()->user->isAdmin ) {
                            echo CHtml::link('Manage Suppliers', array('/admin/users/admin'), array("class" => 'btn btn-success'));
                        } else {
                          //  echo CHtml::link('Profile', array('/admin/users/view'), array("class" => 'btn btn-success'));
                        }
                    ?>
                </div>
                <div class="card-body">
                    <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $('label[for="Users_image"]').append(' Supplier Image');
    $('.btn').each(function () {
        if($(this).text() == 'Save') {
            $(this).addClass('longBtn').css('margin-top','10px');
        }
    });
</script>
