<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'users-form',
    'enableAjaxValidation' => false,
    'enableClientValidation' => false,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php //echo $form->errorSummary($model);  ?>

<div class="row">
    <div class="col-sm-6">
        <!--    <div class="control-group">
                <div id="check_boxes">
        <?php
//            $model->sub_type_id = explode(",", $model->sub_type_id);
//            echo $form->checkBoxListRow($model, 'sub_type_id', CHtml::listData(
//                            $model->getSubProductTypes(), 'id', 'title')
//            );
        ?>
                </div>
            </div>-->
        <div class="control-group">
            <?php echo $form->textFieldRow($model, 'username', array('class' => 'span5', 'maxlength' => 255)); ?>
            <?php echo $form->textFieldRow($model, 'fullname', array('class' => 'span5', 'maxlength' => 255)); ?>
            <?php echo $form->textFieldRow($model, 'email', array('class' => 'span5', 'maxlength' => 255)); ?>
            <?php echo $form->textAreaRow($model, 'description', array('class' => 'form-control')); ?>
        </div>

        <!--        <div class="control-group">
        
                        <div class="span4">
                    
                        </div>
                            <div class="span6">
        <?php //echo $form->textFieldRow($model, 'username', array('class' => 'span12', 'maxlength' => 255)); ?>
                            </div>
                    
                            <div class="span6">
        <?php //echo $form->passwordFieldRow($model, 'password', array('class' => 'span12', 'maxlength' => 255)); ?>
                            </div>
        
        <?php /* if (Yii::app()->user->isAdmin) { ?>
          <div class="span3">
          <?php echo $form->dropdownListRow($model, 'role_id', CHtml::listData(Role::getRoles(), 'id', 'title'), array('class' => 'span10', 'empty' => '(Select Role)')); ?>
          </div>
          <?php } */ ?>
        
                </div>-->

        <div class="control-group">
            <div class="span6">
                <?php echo $form->fileFieldRow($model, 'image', array('class' => 'form-control')); ?>
            </div>
        </div>

    </div>
</div>

<!--<div class="control-group">
   
    <div class="span4">
<?php //echo $form->textAreaRow($model, 'current_address', array('class' => 'span12', 'maxlength' => 255));  ?>
    </div>

    <div class="span4">
<?php //echo $form->textAreaRow($model, 'permanent_address', array('class' => 'span12', 'maxlength' => 255));  ?>
    </div>
</div>-->

<div class="control-group">
    <div class="form-actions">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type' => 'primary',
            'label' => $model->isNewRecord ? 'Create' : 'Save',
        ));
        ?>
    </div>
</div>


<?php $this->endWidget(); ?>
