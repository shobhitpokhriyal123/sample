<section id="user_utilities">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <strong><?php echo $model->fullname; ?>  : <span id="span_id"> Product Types  </span></strong>
                    </div>
                    <?php
                        if (Yii::app()->user->isAdmin) {
                            echo CHtml::link('Product Types', array('users/utilities', array('id' => $model->id)), array("class" => 'btn btn-primary', 'id' => 'product_type_link'));

                            echo '<br>' . CHtml::link('Products', array('product/admin', 'sid' => $model->id), array("class" => 'btn btn-primary', 'id' => 'product_link'));
                            //echo CHtml::link('Products', array('product/admin', array('sid' => $model->id)), array("class" => 'btn btn-success'));
                        }
                    ?>
                </div>
                <div class="card-body">
                    <?php
                        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                            'id' => 'users-form1',
                            'enableAjaxValidation' => false,
                            'action' => $this->createAbsoluteUrl("users/types"),
                            'enableClientValidation' => true,
                            'clientOptions' => array(
                                'validateOnSubmit' => true,
                            ),
                            'htmlOptions' => array('enctype' => 'multipart/form-data'),
                        ));
                    ?>

                    <div class="row">
                        <div id="partial_part">

                        </div>

                        <div class="control-group">
                            <div class="form-actionss">
                                <button type="button" class="longBtn btn btn-success" name="" id="fadsgfaskjfd" >Save</button>
                            </div>
                        </div>
                    </div>

                    <?php $this->endWidget(); ?>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function () {
        $("#fadsgfaskjfd").click(function (e) {
        //   var  u = $("#p_utility").val();
            e.preventDefault();
            var url = '<?php echo Yii::app()->createAbsoluteUrl("admin/product/products/sid/" . $model->id); ?>';
            $("#span_id").html('Products');
            $("#partial_part").load(url);

        });
        $("#product_link").click(function (e) {
            e.preventDefault();
            var url = '<?php echo Yii::app()->createAbsoluteUrl("admin/product/products/sid/" . $model->id); ?>';
            $("#span_id").html('Products');
            $("#partial_part").load(url);

        });
        $("#product_type_link").click(function (e) {
            e.preventDefault();
            var url = '<?php echo Yii::app()->createAbsoluteUrl("admin/users/getUtilities/id/" . $model->id); ?>';
            $("#span_id").html('Product Types');
            $("#partial_part").load(url);
            return false;
        });
    });
    $(window).load(function () {
        var url = '<?php echo Yii::app()->createAbsoluteUrl("admin/users/getUtilities/id/" . $model->id); ?>';
        $("#span_id").html('Product Types');
        $("#partial_part").load(url);
        return false;
    });


</script>
