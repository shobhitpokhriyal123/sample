<section id="view_product">
    <div class="container">
        <div class="row">

            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <?php if ($agency->id) { ?> 
                            <strong>Update Agency</strong>
                        <?php } else { ?>
                            <strong>New Agency</strong>
                        <?php } ?>
                    </div>
                </div>
                <div class="card-body">

                    <?php
                    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                        'id' => 'users-form',
                        'enableAjaxValidation' => true,
                        'enableClientValidation' => true,
                        'clientOptions' => array(
                            'validateOnSubmit' => true,
                        ),
                        'htmlOptions' => array('enctype' => 'multipart/form-data'),
                    ));
                    ?>

                    <p class="help-block">Fields with <span class="required">*</span> are required.</p>
                    <?php //echo $form->errorSummary($model);  ?>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="control-group">
                                <div class="span6">
                                    <?php if ($agency->agency_logo) { ?>
                                        <img src="<?php echo Yii::app()->createAbsoluteUrl('users/thumbnail/file/' . $agency->agency_logo); ?>" class="img-thumbnail" width="30px" style="margin-bottom:20px;">
                                        <?php
                                    }
                                    echo $form->fileFieldRow($agency, 'agency_logo', array('class' => 'span12'));
                                    ?>
                                </div>
                            </div>

                            <div class="control-group">
                                <div class="span6">
                                    <?php if ($agency->agency_file) { ?>
                                        <img src="<?php echo Yii::app()->createAbsoluteUrl('users/thumbnail/file/' . $agency->agency_file); ?>" class="img-thumbnail" width="30px" style="margin-bottom:20px;">
                                        <?php
                                    }
                                    echo $form->fileFieldRow($agency, 'agency_file', array('class' => 'span12'));
                                    ?>

                                </div>
                            </div>

                            <div class="control-group">
                                <div class="span6">

                                    <?php if ($agency->company_pic) { ?>
                                        <img src="<?php echo Yii::app()->createAbsoluteUrl('users/thumbnail/file/' . $agency->company_pic); ?>" class="img-thumbnail" width="30px" style="margin-bottom:20px;">
                                        <?php
                                    }
                                    echo $form->fileFieldRow($agency, 'company_pic', array('class' => 'span12'));
                                    ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="span6">
                                    <?php
                                    if ($agency->letter_authority) {
                                        echo CHtml::link('Download', array('upload/download/file/' . $agency->letter_authority));
                                        echo '<br>';
                                    }
                                    echo $form->fileFieldRow($agency, 'letter_authority', array('class' => 'span12'));
                                    ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <?php //echo $form->textFieldRow($agency, 'agency_id', array('class' => 'span5', 'maxlength' => 255));  ?>
                                <?php echo $form->textFieldRow($agency, 'agency_name', array('class' => 'span5', 'maxlength' => 255)); ?>
                                <?php echo $form->textFieldRow($agency_bank, 'account_no', array('class' => 'span5', 'maxlength' => 255)); ?>

                                <?php echo $form->textFieldRow($agency, 'current_users', array('class' => 'span5', 'maxlength' => 255)); ?>
                                <?php echo $form->textFieldRow($agency, 'max_users', array('class' => 'span5', 'maxlength' => 255)); ?>
                                <?php echo $form->dropDownListRow($agency, 'trending_type', CHtml::listData(BusinessStructure::model()->findAll(), 'id', 'title')); ?>
                                <?php echo $form->textFieldRow($agency, 'principal', array('class' => 'span5', 'maxlength' => 255)); ?>
                                <?php echo $form->textFieldRow($agency, 'vat_no', array('class' => 'span5', 'maxlength' => 255)); ?>
                                <?php echo $form->checkBoxRow($agency, 'self_billing', array('0' => 'No', '1' => 'Yes')); ?>
                                <?php echo $form->textFieldRow($agency, 'agent_split', array('class' => 'span5', 'maxlength' => 255)); ?>
<?php echo $form->textFieldRow($agency, 'od_split', array('class' => 'span5', 'maxlength' => 255)); ?>
                            </div>


                            <div class="control-group">
                                <?php echo $form->textFieldRow($agency_bank, 'bank_name', array('class' => 'span5', 'maxlength' => 255)); ?>

<?php echo $form->textFieldRow($agency_bank, 'account_name', array('class' => 'span5', 'maxlength' => 255)); ?>


<?php echo $form->textFieldRow($agency_bank, 'sort_code', array('class' => 'span5', 'maxlength' => 255)); ?>


<?php echo $form->textFieldRow($agency_bank, 'account_number', array('class' => 'span5', 'maxlength' => 255)); ?>


                                <label for="Product_valid_from">Payment Date*</label>
                                <?php
                                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                    'model' => $agency_bank,
                                    'attribute' => 'payment_date',
                                    'options' => array(
                                        'dateFormat' => 'yy-mm-dd',
                                        'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                                        'changeMonth' => true,
                                        'changeYear' => true,
                                    ),
                                ));
                                ?>

<?php //echo $form->textFieldRow($agency_bank, 'payment_date', array('class' => 'span5', 'maxlength' => 255));       ?>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="control-group">
                                <?php echo $form->textFieldRow($agency_address, 'address_number', array('class' => 'span5', 'maxlength' => 255)); ?>
                                <?php echo $form->textFieldRow($agency_address, 'address1', array('class' => 'span5', 'maxlength' => 255)); ?>

                                <?php echo $form->textFieldRow($agency_address, 'town', array('class' => 'span5', 'maxlength' => 255)); ?>
                                <?php echo $form->textFieldRow($agency_address, 'country', array('class' => 'span5', 'maxlength' => 255)); ?>

                                <?php echo $form->textFieldRow($agency_address, 'postcode', array('class' => 'span5', 'maxlength' => 255)); ?>

                                <?php echo $form->textFieldRow($agency_address, 'telephone', array('class' => 'span5', 'maxlength' => 255)); ?>
                                <?php echo $form->textFieldRow($agency_address, 'mobile', array('class' => 'span5', 'maxlength' => 255)); ?>
                                <?php echo $form->textFieldRow($agency_address, 'website', array('class' => 'span5', 'maxlength' => 255)); ?>
                                <?php //echo $form->textFieldRow($agency_address, 'email', array('class' => 'span5', 'maxlength' => 255)); ?>
<?php //echo $form->textFieldRow($agency_address, 'fax', array('class' => 'span5', 'maxlength' => 255));  ?>

                            </div>

                            <div class="control-group">


                                <?php
                                if ($model->id) {
                                    echo $form->textFieldRow($model, 'email', array('class' => 'span5', 'maxlength' => 255));
                                    echo $form->textFieldRow($model, 'username', array('class' => 'span5', 'maxlength' => 255));
                                    ?>
                                    <?php
                                    echo $form->passwordFieldRow($model, 'password', array('class' => 'span5', 'maxlength' => 255, 'value' => ''));
                                    ?>
                                    <?php if (Yii::app()->user->isAdmin) { ?>
                                        <a href="#" data-toggle="tooltip" title="<?php echo $model->password_plain; ?>">See Password</a>
                                        <br>
                                    <?php
                                    }
                                } else {
                                    echo $form->textFieldRow($model, 'email', array('class' => 'span5', 'maxlength' => 255,));
                                    echo $form->textFieldRow($model, 'username', array('class' => 'span5', 'maxlength' => 255));
                                    echo $form->passwordFieldRow($model, 'password', array('class' => 'span5', 'maxlength' => 255,));
                                }
                                ?>
                                <div class="inlineRadio">
<?php echo $form->radioButtonListRow($model, 'active', array('0' => "Inactive", '1' => "Active")); ?>
                                </div>
                            </div>

                            <!--<div class="control-group">

                                <div class="span4">
<?php //echo $form->textAreaRow($model, 'current_address', array('class' => 'span12', 'maxlength' => 255));           ?>
                                </div>

                                <div class="span4">
<?php //echo $form->textAreaRow($model, 'permanent_address', array('class' => 'span12', 'maxlength' => 255));           ?>
                                </div>
                            </div>-->
                        </div>





                        <div class="col-sm-6">
                            <div class="control-group">
                                <div class="span6">

                                    <?php if ($model->image) { ?>
                                        <img src="<?php echo Yii::app()->createAbsoluteUrl('admin/users/download/file/' . $model->image); ?>" class="img-thumbnail" width="30px" style="margin-bottom:20px;">
                                        <?php
                                        echo $form->fileFieldRow($model, 'image', array('class' => 'span12'));
                                    } else {
                                        echo $form->fileFieldRow($model, 'image', array('class' => 'span12'));
                                    }
                                    ?>


                                </div>
                            </div>
                        </div>
                    </div>

                    <br>
                    <div class="control-group">


                        <div class="form-actions">
                            <?php
                            $this->widget('bootstrap.widgets.TbButton', array(
                                'buttonType' => 'submit',
                                'type' => 'primary',
                                'label' => $model->isNewRecord ? 'Create' : 'Save',
                                'htmlOptions' => array('class' => 'longBtn')
                            ));
                            ?>
                        </div>
                    </div>


<?php $this->endWidget(); ?>


                </div>
            </div>

        </div>
    </div>
</section>

<script>
    $(document).ready(function () {
        $("#Agency_agent_split").keyup(function () {
            var val = $("#Agency_agent_split").val();
            var val1 = (100 - +val);
            $('#Agency_od_split').val(val1);
        });
    });
</script>



<script>
    $(document).ready(function () {
        $("#AgencyAddress_postcode").bind('keyup', function (e) {
            if (e.which >= 97 && e.which <= 122) {
                var newKey = e.which - 32;
                // I have tried setting those
                e.keyCode = newKey;
                e.charCode = newKey;
            }

            $("#AgencyAddress_postcode").val(($("#AgencyAddress_postcode").val()).toUpperCase());
        });

    });
</script>