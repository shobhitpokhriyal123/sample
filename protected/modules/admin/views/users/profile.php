<div class="row-fluid">
    <div class="span12 well">

        <h1>View User #<?php echo $model->username; ?></h1>
        <div class="right">
            <?php echo CHtml::link('Manage Users', array('/admin/users/admin'), array("class" => 'btn btn-success')); ?>
         <?php echo CHtml::link('Update', array('/admin/users/update/id/'.$model->id), array("class" => 'btn btn-success')); ?>
        </div>
        <br>
        <br>
        <?php
        $this->widget('bootstrap.widgets.TbDetailView', array(
            'data' => $model,
            'attributes' => array(
               'id',
                'emp_code',
                'fullname',
                'email',
                'username',
                'mobile',
                'designation',
                'last_company',
                'permanent_address:html',
                'current_address:html',
                'joining_date',
                // 'password',
                // 'image',
                'role_id' => array(
                    'name' => 'role_id',
                    'value' => Role::getRoles($model->role_id),
                ),
                'active' => array(
                    'name' => 'active',
                    'value' => $model->getStatus($model->active),
                ),
                'created',
            ),
        ));
        ?>
    </div>
</div>
