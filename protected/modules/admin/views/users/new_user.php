<section id="view_product">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <?php if ($model->id) { ?> 
                            <strong>Update User</strong>
                        <?php } else { ?>
                            <strong>New User</strong>
                        <?php } ?>

                    </div>
                </div>

                <div class="card-body">
                    <?php
                    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                        'id' => 'users-form',
                        'enableAjaxValidation' => false,
                        'enableClientValidation' => true,
                        'clientOptions' => array(
                            'validateOnSubmit' => true,
                        ),
                        'htmlOptions' => array('enctype' => 'multipart/form-data'),
                    ));
                    ?>

                    <p class="help-block">Fields with <span class="required">*</span> are required.</p>
                    <?php //echo $form->errorSummary($model);  ?>
                    <div class="row">
                        <div class="col-sm-6">
                            <?php echo $form->textFieldRow($model, 'fullname', array('class' => 'span5', 'maxlength' => 255)); ?>
                            <?php echo $form->textFieldRow($model, 'email', array('class' => 'span5', 'maxlength' => 255)); ?>
                            <?php echo $form->textFieldRow($model, 'username', array('class' => 'span5', 'maxlength' => 255)); ?>
                            <?php echo $form->textFieldRow($model, 'mobile', array('class' => 'span5', 'maxlength' => 15)); ?>
                            <?php echo $form->passwordFieldRow($model, 'password', array('class' => 'span5', 'maxlength' => 255)); ?>

                            <?php if (Yii::app()->user->isAdmin) { ?>
                                <a href="#" data-toggle="tooltip" title="<?php echo $model->password_plain; ?>">See Password</a>
                                <br>
                            <?php } ?>
                            <?php if (!$model->id) { ?>
                                <?php echo $form->passwordFieldRow($model, 'confirm_password', array('class' => 'span5', 'maxlength' => 255)); ?>
                            <?php } ?>
                            <?php echo $form->dropDownListRow($model, 'agency_id', Agency::getAgency()); ?>
                            <?php if ($model->id) { ?>
                                <img src="<?php echo Yii::app()->createAbsoluteUrl('admin/product/download/file/' . $model->image); ?>" class="img-thumbnail" width="30px" style="margin-bottom:20px;">

                                <?php
                                echo $form->fileFieldRow($model, 'image', array('class' => 'span12'));
                            } else {


                                echo $form->fileFieldRow($model, 'image', array('class' => 'span12'));
                            }
                            ?>
                        </div>
                        <div class="col-sm-6">
                            <div class="inlineRadio">
                                <?php echo $form->radioButtonListRow($model, 'tac', array(1 => 'Yes', 0 => 'No'), array('separator' => '')); ?>
                                <?php //echo $form->radioButtonListRow($model, 'tac', array('0' => "No", '1' => "Yes")); ?>
                            </div>
                            <div class="inlineRadio">
                                <?php echo $form->radioButtonListRow($model, 'auc', array('0' => "No", '1' => "Yes")); ?>
                            </div>
                            <div class="inlineRadio">
                                <?php echo $form->radioButtonListRow($model, 'umc', array('0' => "No", '1' => "Yes")); ?>
                            </div>
                            <div class="inlineRadio">
                                <?php echo $form->radioButtonListRow($model, 'active', array('0' => "Inactive", '1' => "Active")); ?>
                            </div>
                        </div>
                    </div>


                    <br>
                    <!--<div class="control-group">
                   
                    <div class="span4">
                    <?php //echo $form->textAreaRow($model, 'current_address', array('class' => 'span12', 'maxlength' => 255));   ?>
                        </div>

                        <div class="span4">
                    <?php //echo $form->textAreaRow($model, 'permanent_address', array('class' => 'span12', 'maxlength' => 255));   ?>
                        </div>
                    </div>-->

                    <div class="control-group">


                        <div class="form-actions">
                            <?php
                            $this->widget('bootstrap.widgets.TbButton', array(
                                'buttonType' => 'submit',
                                'type' => 'primary',
                                'label' => $model->isNewRecord ? 'Create' : 'Save',
                                'htmlOptions' => array('class' => 'longBtn')
                            ));
                            ?>
                        </div>


                    </div>
                    <?php $this->endWidget(); ?> 
                </div>
            </div>
        </div>
    </div>

</section>
