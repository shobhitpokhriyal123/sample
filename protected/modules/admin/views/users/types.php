<section id="supplier_type">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <strong>Supplier Details</strong>
                    </div>
                    <?php echo CHtml::link('Manage Suppliers', array('/admin/users/admin'), array("class" => 'btn btn-success')); ?>
                </div>
                <div class="card-body">
                    <?php
                    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                        'id' => 'users-form',
                        'enableAjaxValidation' => false,
                        //'action' => $this->createAbsoluteUrl("users/types"),
                        'enableClientValidation' => true,
                        'clientOptions' => array(
                            'validateOnSubmit' => true,
                        ),
                        'htmlOptions' => array('enctype' => 'multipart/form-data'),
                    ));
                    ?>

                    <div class="form-group">
                        <?php
                        $users = Users::getAllSuppliers();
                        echo $form->dropdownListRow($model, 'supplier_id', CHtml::listData(
                                        $users, 'id', 'fullname'), array('class' => 'form-control',
                            'empty' => '(Select Supplier)', 'onchange' => 'getUtilities($(this).val())')
                        );
                        ?>
                    </div>
                    <div class="form-group" id="check_boxes">
                        <?php
                        $model->sub_type_id = explode(",", $model->sub_type_id);
                        echo $form->checkBoxListRow($model, 'sub_type_id', CHtml::listData(
                                        $model->getSubProductTypes(), 'id', 'title')
                        );
                        ?>
                    </div>
                    <div class="form-group">
                        <button type="submit" disabled="disabled" class = "longBtn btn btn-primary" >Filter</button>

                    </div>
                    <?php $this->endWidget(); ?>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    function getUtilities(id) {
        var url = '<?php echo Yii::app()->createAbsoluteUrl("admin/users/getUtilities/id") ?>/' + id;
        if (id) {
            $.ajax({
                url: url,
                type: 'POST',
                success: function (res) {
                    if(res == '1'){
                         $("#check_boxes").html("No product found for that supplier..!");
                           $(".longBtn").attr('disabled','disabled');
                    }else{
                        $("#check_boxes").html(res);
                        $(".longBtn").removeAttr('disabled');
                    }
                  
                }
            });
        }
    }
</script>
