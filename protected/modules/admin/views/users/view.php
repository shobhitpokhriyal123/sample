<section id="view_suplier">
    <div class="container">
        <div class="row">

            <div class="card">
                <div class="card-header">
                    <div class="card-title text-primary">
                        <strong>View  #<?php echo $model->fullname;  ?></strong>
                    </div>
                    <?php
                    $agency = Agency::model()->findByAttributes(array('user_id' => Yii::app()->user->id));
                    if (Yii::app()->user->isAdmin ) {
                        //echo $model->role_id;
                        if($model->role_id == 4){
                        echo CHtml::link('Manage ', array('users/allUsers'), array("class" => 'btn btn-success'));
                        }
                        if($model->role_id == 3){
                             echo CHtml::link('Manage ', array('users/agencies'), array("class" => 'btn btn-success'));
                        }
                        //echo CHtml::link('Update Utility Ageny Details', array('users/updateAgency', 'id' => Yii::app()->user->id), array("class" => 'btn btn-success'));
                    }
                    ?>
                    <?php //echo CHtml::link('Update', array('users/update/id/' . $model->id), array("class" => 'btn btn-success')); ?>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-2">
                            <img src="<?php echo Yii::app()->createAbsoluteUrl('users/thumbnail/file/' . $model->image); ?>" class="img-thumbnail" width="100%" style="margin-bottom:20px;">
                        </div>
                        <div class="col-sm-10 text-left">
                            <?php
                            $this->widget('bootstrap.widgets.TbDetailView', array(
                                'data' => $model,
                                'attributes' => array(
                                    // 'id'=>,
                                    'fullname',
                                    'email',
                                    'username',
                                    'mobile',
//                                   'agency_name' , 
//                                    'agency_file' ,
//                                    'agency_id' ,
//                                    'company_pic',
//                                    'letter_authority',
//                                    'current_users',
//                                    'max_users',
//                                    'trending_type',
//                                    'principal',
//                                    'vat_no',
//                                    'date_modified',
//                                    'self_billing',
//                                    'od_split',
//                                    'agent_split',
//                                    'agency_logo' ,
                                    //  'permanent_address:html',
                                    //  'current_address:html',
//                                    'sub_type_id'=>array(
//                                        'name'=>'sub_type_id',
//                                        'value'=> Type::getUtilityTitle($model->sub_type_id)
//                                    ),
                                    'role_id' => array(
                                        'name' => 'role_id',
                                        'value' => Role::getRoles($model->role_id),
                                    ),
                                    'active' => array(
                                        'name' => 'active',
                                        'value' => $model->getStatus($model->active),
                                    ),
                                    'created',
                                ),
                            ));


                            $model1 = Agency::model()->findByAttributes(array('user_id' => $model->id));
                          // echo '<pre>'; print_r( $model1); die;
                            if ($model1)
                                $this->widget('bootstrap.widgets.TbDetailView', array(
                                    'data' => $model1,
                                    'attributes' => array(
                                        // 'id',
//                                    'fullname',
//                                    'email',
//                                    'username',
//                                    'mobile', 
                                        'agency_name',
                                        //  'agency_file' ,
                                        'agency_id',
                                        // 'company_pic',
                                        //  'letter_authority',
                                        'agency_logo' => array(
                                            'type' => 'image',
                                            'name' => 'agency_logo',
                                            'type'=>'raw',
                                            'value' => CHtml::image(Yii::app()->createAbsoluteUrl("/admin/users/thumbnail/file/" . $model1->agency_logo)),
                                            'htmlOptions' => array('class' => 'table-images')
                                        ),
                                        'company_pic' => array(
                                            'filter' => false,
                                           // 'type' => 'image',
                                             'type'=>'raw',
                                            'name' => 'company_pic',
                                            'value' => CHtml::image(Yii::app()->createAbsoluteUrl("/admin/users/thumbnail/file/" . $model1->company_pic)),
                                            'htmlOptions' => array('class' => 'table-images')
                                        ),
                                        'agency_file' => array(
                                            'filter' => false,
                                            //'type' => 'image',
                                            'name' => 'agency_file',
                                            'type'=>'raw',
                                            'value' => CHtml::image(Yii::app()->createAbsoluteUrl("/admin/users/thumbnail/file/" . $model1->agency_file)),
                                            'htmlOptions' => array('class' => 'table-images')
                                        ),
                                        'letter_authority' => array(
                                            'filter' => false,
                                            'type' => 'image',
                                             'type'=>'raw',
                                            'name' => 'letter_authority',
                                            'value' => CHtml::link('Download',array('upload/download/file/'.$model1->letter_authority)),//CHtml::link("Download",array(Yii::app()->createAbsoluteUrl("admin/users/download/file/" . $model1->letter_authority))),
                                            'htmlOptions' => array('class' => 'table-images')
                                        ),
                                        'current_users',
                                        'max_users',
                                        'trending_type' => array(
                                            'filter' => false,
                                            'name' => 'trending_type',
                                            'id' => 'trending_type',
                                            'value' => Agency::getTrendingType($model1->trending_type)
                                        ),
                                        'principal',
                                        'vat_no',
                                        'date_modified',
                                        'self_billing',
                                        'od_split',
                                        'agent_split',
                                    ),
                                ));

//                            //$model12 = Bank::model()->findByAttributes(array('user_id' => $model->id));
                            $model12 = '';
                            if($agency)
                         $model12 = AgencyBank::model()->findByAttributes(array('agency_id' => $agency->id));
                           // echo '<pre>'; print_r( $model->agency1); die;
                            if ($model12)
                                $this->widget('bootstrap.widgets.TbDetailView', array(
                                    'data' => $model12,
                                    'attributes' => array(
                                        //'id' , 
                                        'user_id',
                                        'contract_id',
                                        'account_no',
                                        'sort_code',
                                        'bank_name',
                                        'account_number',
                                        'payment_date',
                                    ),
                                ));



//if (Yii::app()->user->isAgency) {
//                            if ($agency && ($agency->user->role_id == Users::ROLEAGENCY )) {
//                               // $this->renderPartial('application.modules.admin.views.agency.view', array('model' => ($agency) ? $agency : new Agency), null, true);
//                            }
                            // }
                            ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
