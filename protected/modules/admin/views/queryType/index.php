<?php
$this->breadcrumbs=array(
	'Query Types',
);

$this->menu=array(
	array('label'=>'Create QueryType','url'=>array('create')),
	array('label'=>'Manage QueryType','url'=>array('admin')),
);
?>

<h1>Query Types</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
